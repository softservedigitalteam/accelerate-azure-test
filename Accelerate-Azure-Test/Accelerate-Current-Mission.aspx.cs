﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class Accelerate_Current_Mission : System.Web.UI.Page
{
    private clsAccountUsers clsAccountUsers;
    public clsMissions clsMissions;
    public clsAccountUserTrackings clsTracking;
    clsAccountUserTrackings clsAccountUserTracking;
    clsGroups clsGroup;
    string cacheGroup = "";
    string cacheGroupPhase1 = "";
    string cacheGroupPhase2 = "";
    string cacheGroupPhase3 = "";

    public DataTable dtPhase1Missions;
    public DataTable dtPhase2Missions;
    public DataTable dtPhase3Missions;
    int iNextMissionID = 0;
    
    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["action"] == "syslogout")
        {
            //### Clear all seesion variables
            Session.Clear();
        }
        
        //### Check if session clsUser exists
        if (Session["clsAccountUsers"] == null)
        {
            //### Redirect back to login
            Response.Redirect("Accelerate-Login.aspx");
        }
        try
        {
            clsAccountUsers = (clsAccountUsers)Session["clsAccountUsers"];
            clsTracking = new clsAccountUserTrackings(clsAccountUsers.iAccountUserTrackingID);
            clsAccountUserTracking = new clsAccountUserTrackings(clsAccountUsers.iAccountUserTrackingID);

            //### Caching Group
            cacheGroup = "cacheGroup" + clsAccountUsers.iGroupID.ToString();
            if (Cache[cacheGroup] == null)
            {
                clsGroup = new clsGroups(clsAccountUsers.iGroupID);
                Cache.Add(cacheGroup, clsGroup, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, 5, 0), System.Web.Caching.CacheItemPriority.Default, null);
            }
            else
            {
                clsGroup = (clsGroups)Cache[cacheGroup];
            }

            //## Caching GroupPhases
            cacheGroupPhase1 = cacheGroup + "Phase1";
            cacheGroupPhase2 = cacheGroup + "Phase2";
            cacheGroupPhase3 = cacheGroup + "Phase3";


            checkCompletedPhasesUserTracking();
            litPoints.Text = clsAccountUsers.iCurrentPoints.ToString();
            //Check Team Leader Mission
            //int iCheckMissionID = Convert.ToInt32(Request.QueryString["iMissionID"]);
            //clsMissions newclsMissions = new clsMissions(iCheckMissionID);
            //int iCheckMissionTypeID = Convert.ToInt32(newclsMissions.iMissionTypeID);
            //if (iCheckMissionTypeID == 2)
            //{
            ScriptManager.RegisterStartupScript(this, this.GetType(), "InitialiseDrag", "<script lang='JavaScript'>REDIPS.drag.init();</script>", false);
            //    checkTeamLeadershipDragDropCorrectAnswers(iCheckMissionID);
            //}

}
        catch(Exception ex)
        {
            Response.Redirect("Accelerate-Error404.aspx");
        }

        if (!IsPostBack)
        {
            //### If the iMissionID is passed through then we want to instantiate the object with that iMissionID
            if ((Request.QueryString["iMissionID"] != "") && (Request.QueryString["iMissionID"] != null))
            {
                try
                {
                    int iMissionID = Convert.ToInt32(Request.QueryString["iMissionID"]);
                    clsMissions = new clsMissions(iMissionID);

                    int iMissionTypeID = clsMissions.iMissionTypeID;

                    if (iMissionTypeID == 1)
                    {
                        //Execute Javascript to display teamshipMission (hiddden as to not clash with and displace other html elements)
                        Page.ClientScript.RegisterStartupScript(GetType(), "hideTeamshipMission", "hideTeamshipMission();", true);
                        //### Populate the form
                        popMissionQuestions(iMissionID);
                        //popCurrentMissionInfo(iMissionID);
                        popPreviouslyAnsweredQuestions(iMissionID);
                        popCurrentMissionBanner(iMissionID);
                        popMissionProgressBarPercent(iCurrentPercentage(iMissionID), iMissionID);
                        //popCurrentMissionInfoFromCurrentMission(iMissionID);

                        litBootstrap.Text = "<script src='assets/js/bootstrap/bootstrap.min.js'></script>";
                    }
                    else if (iMissionTypeID == 2)
                    {
                        //### Populate the form
                        //popTeamShips(iMissionID);
                        //### Populate the form
                        popTeamShipsDragDrop(iMissionID);
                        popTeamshipQuestionsDragDropInfo(iMissionID);
                        //### Nout Used
                        //popCurrentMissionInfo(iMissionID);
                        popPreviouslyAnsweredTeamshipDragDropQuestions(iMissionID);
                        //popPreviouslyAnsweredTeamshipQuestions(iMissionID);
                        popCurrentMissionBanner(iMissionID);
                        //popMissionProgressBarPercent(iCurrentPercentageTeamship(iMissionID), iMissionID);
                        popMissionProgressBarPercentDragDropTeamshipMission(iMissionID);
                        //popCurrentMissionInfoFromCurrentMission(iMissionID);

                        litBootstrap.Text = "<script src='assets/js/bootstrap/bootstrap.min.js'></script>";
                    }
                    else if (iMissionTypeID == 4)
                    {
                        //Execute Javascript to display teamshipMission (hiddden as to not clash with and displace other html elements)
                        Page.ClientScript.RegisterStartupScript(GetType(), "hideTeamshipMission", "hideTeamshipMission();", true);
                        //### Populate the form
                        popCurrentMissionBanner(iMissionID);

                        popMissionDropDownQuestions(iMissionID);
                        popDropDowns(iMissionID);
                        popPreviouslyAnsweredDropDownQuestions(iMissionID);

                        popMissionProgressBarPercent(iCurrentPercentageDropDown(iMissionID), iMissionID);

                        //litBootstrap.Text = "<script src='assets/js/bootstrap/bootstrap.min.js'></script>";
                    }

                    StringBuilder strbVideoPages = new StringBuilder();

                    DataTable dtMissionVideos = clsMissionVideos.GetMissionVideosList();
                    foreach (DataRow dr in dtMissionVideos.Rows)
                    {
                        if (dr["iMissionVideoID"].ToString() == clsMissions.iMissionVideoID.ToString())
                        {
                            string strVideoLink = getList(dr["strPathToVideo"].ToString());

                            if (dr["strPathToVideo"].ToString() != "" && dr["strPathToVideo"].ToString() != null)
                            {
                                strbVideoPages.AppendLine("<br /><br />");
                                strbVideoPages.AppendLine("<div class='well-short' style='background: #878a8f; margin-top:8px'>");
                                strbVideoPages.AppendLine("<img src='images/dummy.png' alt='citybg' data-lazyload='images/citybg.jpg' data-bgposition='center top' data-bgfit='cover' data-bgrepeat='no-repeat'>");
                                strbVideoPages.AppendLine("<video controls='' autostart='false' name='media' style='width:100%; height: 360px; margin-bottom:22px;'>");
                                strbVideoPages.AppendLine("<source src='" + strVideoLink + "' type='video/mp4'></video>");
                                strbVideoPages.AppendLine("</div>");
                            }
                            else
                            {
                                DataTable dtVideo = clsMissionVideos.GetMissionVideosList("iMissionVideoID = " + clsMissions.iMissionVideoID, "");
                                foreach (DataRow drs in dtVideo.Rows)
                                {
                                    strVideoLink = drs["strVideoLink"].ToString();
                                }
                                strbVideoPages.AppendLine("<div class='well-short' style='background: #878a8f; '>");
                                strbVideoPages.AppendLine("<iframe id='ifrmMissionVideo' src='" + strVideoLink + "' runat='server' width='100%' height='360px'></iframe>");
                                strbVideoPages.AppendLine("</div>");
                            }

                            litVideo.Text = strbVideoPages.ToString();
                            break;
                        }
                    }
                }
                catch(Exception ex)
                {
                    Response.Redirect("Accelerate-Error404.aspx");
                }
            }
        }
        //TEST
        //SendPhaseCompletionEmail();
        //SendFinalEmail();
    }

    protected void checkTeamLeadershipDragDropCorrectAnswers(int iMissionTypeID)
    {
        if (Session["lstCorrectList"] != null)
        {
            List<int> lstCorrectList = new List<int>();
            lstCorrectList = (List<int>)Session["lstCorrectList"];
            string strCorrectMissionLeaderMatches = "";
            foreach (int value in lstCorrectList)
            {
                if (strCorrectMissionLeaderMatches == "")
                {
                    strCorrectMissionLeaderMatches = strCorrectMissionLeaderMatches + value.ToString();
                }
                else
                {
                    strCorrectMissionLeaderMatches = strCorrectMissionLeaderMatches + "," + value.ToString();
                }
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "CheckTeamShipMission", "setCorrectElements('" + strCorrectMissionLeaderMatches + "')", true);
        }
    }

    protected void btnPhase1_Click(object sender, EventArgs e)
    {
        try
        {
            popUserPhaseAchievements(1);
        }
        catch
        {
            Response.Redirect("Accelerate-Error404.aspx");
        }
    }

    protected void btnPhase2_Click(object sender, EventArgs e)
    {
        try
        {
            popUserPhaseAchievements(2);
        }
        catch
        {
            Response.Redirect("Accelerate-Error404.aspx");
        }
    }

    protected void btnPhase3_Click(object sender, EventArgs e)
    {
        try
        {
            popUserPhaseAchievements(3);
        }
        catch
        {
            Response.Redirect("Accelerate-Error404.aspx");
        }
    }

    #endregion

    #region ACTION FUNCTIONS

    protected void popPercentageBar(int iCurrentPoints, int iTotalPoints)
    {
        StringBuilder strbCurrentMission = new StringBuilder();
        strbCurrentMission.AppendLine("<div class='percentageGreen' style='width:" + iCurrentPercentage(iCurrentPoints, iTotalPoints) + "px'></div>");
    }

    protected string strCurrentRank(int iCurrentPoints)
    {
        DataTable dtRank = clsRanks.GetRanksList("", "");

        int iRankID = 1;
        int iPointLevel;
        string strCurrentRank = "Noob";

        int iCount = 0;

        foreach (DataRow dtrRank in dtRank.Rows)
        {
            ++iCount;

            iRankID = Convert.ToInt32(dtrRank["iRankID"].ToString());
            iPointLevel = Convert.ToInt32(dtrRank["iPointLevel"].ToString());
            strCurrentRank = dtrRank["strTitle"].ToString();

            if (iCurrentPoints < iPointLevel)
            {
                break;
            }
        }

        return strCurrentRank;
    }

    //### Get the total number of  answers
    protected int iTotalQuestions(int iCurrentMissionID)
    {
        DataTable dtMissionQuestions = clsMissionQuestions.GetMissionQuestionsList("iMissionID=" + iCurrentMissionID, "");

        int iCurrentQs = dtMissionQuestions.Rows.Count;

        return iCurrentQs;
    }

    //### Get the total number of questions answered
    protected int iTotalQuestionsAnswered(int iCurrentMissionID)
    {
        DataTable dtMissionQuestionsAnswered = clsUserAnswers.GetUserAnswersList("iMissionID=" + iCurrentMissionID + " AND iAccountUserID=" + clsAccountUsers.iAccountUserID, "");

        int iTotalQuestionsAnswered = dtMissionQuestionsAnswered.Rows.Count;

        return iTotalQuestionsAnswered;
    }

    //### Returns Current Percentage
    protected int iCurrentPercentage(int iCurrentPoints, int iTotalPoints)
    {
        double dblCurrentPercentage = Convert.ToDouble(iCurrentPoints) / Convert.ToDouble(iTotalPoints);

        int iCurrentPercentage = Convert.ToInt32(dblCurrentPercentage * 200);

        return iCurrentPercentage;
    }

    //### Returns if the question has a correct answer
    private bool bDoesACorrectAnswerExist(int iMissionQuestionID)
    {
        bool bIsThereAnAnswer;

        DataTable dtAnswers = clsMissionAnswers.GetMissionAnswersList("iMissionQuestionID=" + iMissionQuestionID + "AND bIsCorrectAnswer=" + true, "");

        if ((dtAnswers.Rows.Count) > 0)
        {
            bIsThereAnAnswer = true;
        }
        else
            bIsThereAnAnswer = false;

        return bIsThereAnAnswer;
    }

    //### Returns the correct answer
    private string strAnswerValue(int iMissionQuestionID)
    {
        string strAnswer = "Not Set";

        DataTable dtAnswers = clsMissionAnswers.GetMissionAnswersList("iMissionQuestionID=" + iMissionQuestionID + "AND bIsCorrectAnswer=" + true, "");

        foreach (DataRow dtrAnswers in dtAnswers.Rows)
        {
            strAnswer = dtrAnswers["strAnswer"].ToString();
        }

        return strAnswer;
    }

    protected int iCurrentPercentage(int iCurrentMissionID)
    {
        DataTable dtMissionQuestions = clsMissionQuestions.GetMissionQuestionsList("iMissionID=" + iCurrentMissionID, "");
        int iTotalQuestionCount = dtMissionQuestions.Rows.Count;

        DataTable dtCompletedAnswersToQuestions = clsUserAnswers.GetUserAnswersList("iMissionID=" + iCurrentMissionID + " AND iAccountUserID=" + clsAccountUsers.iAccountUserID, "");
        int iTotalAnsweredCount = dtCompletedAnswersToQuestions.Rows.Count;
        int iCurrentPercentage = 0;

        //### Check if undefined
        if ((iTotalQuestionCount != 0))
        {
            double dblCurrentPercentage = Convert.ToDouble(iTotalAnsweredCount) / Convert.ToDouble(iTotalQuestionCount);
            iCurrentPercentage = Convert.ToInt32(dblCurrentPercentage * 200);
        }

        return iCurrentPercentage;
    }

    //### Returns Answers Completed Percentage for Teamship mission
    protected int iCurrentPercentageTeamship(int iCurrentMissionID)
    {
        DataTable dtMissionQuestions = clsTeamShips.GetTeamShipsList("iMissionID=" + iCurrentMissionID, "");
        int iTotalQuestionCount = dtMissionQuestions.Rows.Count;

        DataTable dtCompletedAnswersToQuestions = clsTeamShipAnswers.GetTeamShipAnswersList("iMissionID=" + iCurrentMissionID + " AND iAddedby=" + clsAccountUsers.iAccountUserID, "");
        int iTotalAnsweredCount = dtCompletedAnswersToQuestions.Rows.Count;
        int iCurrentPercentage = 0;

        //### Check if undefined
        if ((iTotalQuestionCount != 0))
        {
            double dblCurrentPercentage = Convert.ToDouble(iTotalAnsweredCount) / Convert.ToDouble(iTotalQuestionCount);
            iCurrentPercentage = Convert.ToInt32(dblCurrentPercentage * 200);
        }

        return iCurrentPercentage;
    }

    protected int iCurrentPercentageDropDown(int iCurrentMissionID)
    {
        DataTable dtMissionQuestions = clsMissionDropDownQuestions.GetMissionQuestionsList("iMissionID=" + iCurrentMissionID, "");
        int iTotalQuestionCount = dtMissionQuestions.Rows.Count;

        DataTable dtCompletedAnswersToQuestions = clsUserDropDownAnswers.GetDropDownAnswersList("iMissionID=" + iCurrentMissionID + " AND iAddedby=" + clsAccountUsers.iAccountUserID, "");
        int iTotalAnsweredCount = dtCompletedAnswersToQuestions.Rows.Count;
        int iCurrentPercentage = 0;

        //### Check if undefined
        if ((iTotalQuestionCount != 0))
        {
            double dblCurrentPercentage = Convert.ToDouble(iTotalAnsweredCount) / Convert.ToDouble(iTotalQuestionCount);
            iCurrentPercentage = Convert.ToInt32(dblCurrentPercentage * 200);
        }

        return iCurrentPercentage;
    }

    protected int iTotalTeamshipQuestions(int iCurrentMissionID)
    {
        DataTable dtMissionQuestions = clsTeamShips.GetTeamShipsList("iMissionID=" + iCurrentMissionID, "");

        int iCurrentQs = dtMissionQuestions.Rows.Count;

        return iCurrentQs;
    }

    protected int iTotalDropDownQuestions(int iCurrentMissionID)
    {
        DataTable dtMissionQuestions = clsMissionDropDownQuestions.GetMissionQuestionsList("iMissionID=" + iCurrentMissionID, "");

        int iCurrentQs = dtMissionQuestions.Rows.Count;

        return iCurrentQs;
    }

    //### Get the total number of questions answered
    protected int iTotalTeamshipQuestionsAnswered(int iCurrentMissionID)
    {
        DataTable dtMissionQuestionsAnswered = clsTeamShipAnswers.GetTeamShipAnswersList("iMissionID=" + iCurrentMissionID + " AND iAddedBy=" + clsAccountUsers.iAccountUserID, "");

        int iTotalQuestionsAnswered = dtMissionQuestionsAnswered.Rows.Count;

        return iTotalQuestionsAnswered;
    }

    protected int iTotalDropDownQuestionsAnswered(int iCurrentMissionID)
    {
        DataTable dtMissionQuestionsAnswered = clsUserDropDownAnswers.GetDropDownAnswersList("iMissionID=" + iCurrentMissionID + " AND iAddedBy=" + clsAccountUsers.iAccountUserID, "");

        int iTotalQuestionsAnswered = dtMissionQuestionsAnswered.Rows.Count;

        return iTotalQuestionsAnswered;
    }

    private string strTeamNameAnswerValue(int iTeamShipID)
    {
        string strAnswer = "Not Set";

        DataTable dtAnswers = clsTeamShips.GetTeamShipsList("iTeamShipID=" + iTeamShipID, "");

        foreach (DataRow dtrAnswers in dtAnswers.Rows)
        {
            strAnswer = dtrAnswers["strName"].ToString();
        }

        return strAnswer;
    }

    private string strDropDownAnswerValue(int iDropDownID)
    {
        string strAnswer = "Not Set";

        DataTable dtAnswers = clsMissionDropDownAnswers.GetMissionAnswersList("iMissionDropDownQuestionID=" + iDropDownID + "AND bIsCorrectAnswer=1", "");

        foreach (DataRow dtrAnswers in dtAnswers.Rows)
        {
            strAnswer = dtrAnswers["strAnswer"].ToString();
        }

        return strAnswer;
    }

    private string strTeamTitleAnswerValue(int iTeamShipID)
    {
        string strAnswer = "Not Set";

        DataTable dtAnswers = clsTeamShips.GetTeamShipsList("iTeamShipID=" + iTeamShipID, "");

        foreach (DataRow dtrAnswers in dtAnswers.Rows)
        {
            strAnswer = dtrAnswers["strTitle"].ToString();
        }

        return strAnswer;
    }

    #endregion

    #region POPULATION METHODS

    protected void checkCompletedPhasesUserTracking()
    {
        clsPhases clsPhase1 = new clsPhases(clsTracking.iPhase1ID);
        litPhase1Text.Text = clsPhase1.strTitle;
        clsPhases clsPhase2 = new clsPhases(clsTracking.iPhase2ID);
        litPhase2Text.Text = clsPhase2.strTitle;
        clsPhases clsPhase3 = new clsPhases(clsTracking.iPhase3ID);
        litPhase3Text.Text = clsPhase3.strTitle;
        //litPhase3Text.Text = "<span><span class='hideSmall'>Phase </span>3</span><span style='font-size:8px;color:#222;position:relative;top:-10px;width: 70%; text-overflow: ellipsis;'><br />" + clsPhase3.strTitle + "</span>";

        //dtPhase1Missions = clsPhaseMissions.GetPhaseMissionsList("iPhaseID=" + clsTracking.iPhase1ID, "");
        //dtPhase2Missions = clsPhaseMissions.GetPhaseMissionsList("iPhaseID=" + clsTracking.iPhase2ID, "");
        //dtPhase3Missions = clsPhaseMissions.GetPhaseMissionsList("iPhaseID=" + clsTracking.iPhase3ID, "");

        if (Cache[cacheGroupPhase1] == null)
        {
            dtPhase1Missions = clsPhaseMissions.GetPhaseMissionsList("iPhaseID=" + clsTracking.iPhase1ID, "");
            Cache.Add(cacheGroupPhase1, dtPhase1Missions, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, 5, 0), System.Web.Caching.CacheItemPriority.Default, null);
        }
        else
        {
            dtPhase1Missions = (DataTable)Cache[cacheGroupPhase1];
        }

        if (Cache[cacheGroupPhase2] == null)
        {
            dtPhase2Missions = clsPhaseMissions.GetPhaseMissionsList("iPhaseID=" + clsTracking.iPhase2ID, "");
            Cache.Add(cacheGroupPhase2, dtPhase2Missions, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, 5, 0), System.Web.Caching.CacheItemPriority.Default, null);
        }
        else
        {
            dtPhase2Missions = (DataTable)Cache[cacheGroupPhase2];
        }

        if (Cache[cacheGroupPhase3] == null)
        {
            dtPhase3Missions = clsPhaseMissions.GetPhaseMissionsList("iPhaseID=" + clsTracking.iPhase3ID, "");
            Cache.Add(cacheGroupPhase3, dtPhase3Missions, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, 5, 0), System.Web.Caching.CacheItemPriority.Default, null);
        }
        else
        {
            dtPhase3Missions = (DataTable)Cache[cacheGroupPhase3];
        }

        int iCurrentPhaseLevel = clsTracking.iCurrentPhaseLevel;

        if (iCurrentPhaseLevel == 1)
        {
            popUserPhaseAchievements(1);
            btnPhase2.Text = "<div class='showcaseSection Grey'><span class='hidden-laptop'>Phase</span> 2: " + clsPhase2.strTitle + "</div>";
            btnPhase3.Text = "<div class='showcaseSection Grey'><span class='hidden-laptop'>Phase</span> 3: " + clsPhase3.strTitle + "</div>";
        }
        else if (iCurrentPhaseLevel == 2)
        {
            btnPhase2.Enabled = true;
            btnPhase2.Text = "<div class='showcaseSection Blue2'><span class='hidden-laptop'>Phase</span> 2: " + clsPhase2.strTitle + "</div>";
            btnPhase3.Text = "<div class='showcaseSection Grey'><span class='hidden-laptop'>Phase</span> 3: " + clsPhase3.strTitle + "</div>";
            popUserPhaseAchievements(2);
        }
        else if (iCurrentPhaseLevel == 3)
        {
            btnPhase2.Enabled = true;
            btnPhase3.Enabled = true;
            btnPhase2.Text = "<div class='showcaseSection Blue2'><span class='hidden-laptop'>Phase</span> 2: " + clsPhase2.strTitle + "</div>";
            btnPhase3.Text = "<div class='showcaseSection Blue3'><span class='hidden-laptop'>Phase</span> 3: " + clsPhase3.strTitle + "</div>";
            popUserPhaseAchievements(3);
        }
    }

    protected void popUserPhaseAchievements(int iPhaseLevel)
    {
        DataTable dtUserAchievements = clsUserAchievements.GetUserAchievementsRecords(clsAccountUsers.iAccountUserID);

        DataTable dtUserDisplayAchievements = new DataTable();
        dtUserDisplayAchievements.Clear();
        dtUserDisplayAchievements.Columns.Add("iMissionID");
        dtUserDisplayAchievements.Columns.Add("strLink");
        dtUserDisplayAchievements.Columns.Add("strTitle");
        dtUserDisplayAchievements.Columns.Add("strNewRow");
        dtUserDisplayAchievements.Columns.Add("FullPathForImage");

        List<int> lstAchievements = new List<int>();
        List<int> lstMissions = new List<int>();
        //T
        List<int> lstNextMission = new List<int>();

        int iCount = 1;
        int iAchieveTotalCount = 0;

        if (iPhaseLevel == 1)
        {
            foreach (DataRow dr in dtPhase1Missions.Rows)
            {
                lstMissions.Add(Convert.ToInt32(dr["iMissionID"]));
            }
            foreach (DataRow dr in dtUserAchievements.Rows)
            {
                lstAchievements.Add(Convert.ToInt32(dr["iAchievementID"]));
            }
            //T
            lstNextMission = lstMissions;

            foreach (int iMissionIDNum in lstMissions)
            {
                clsMissions currentMission = new clsMissions(iMissionIDNum);
                clsAchievements currentAchievements = new clsAchievements(currentMission.iAchievementID);
                DataRow drUserDisplayAchievements = dtUserDisplayAchievements.NewRow();

                drUserDisplayAchievements["iMissionID"] = iMissionIDNum;
                drUserDisplayAchievements["strTitle"] = currentAchievements.strTitle;

                if ((lstAchievements.Contains(currentMission.iAchievementID)) && (currentMission.iAchievementID == 1))
                {
                    drUserDisplayAchievements["FullPathForImage"] = "Achievements/" + currentAchievements.strPathToImages + "/" + Path.GetFileNameWithoutExtension(currentAchievements.strMasterImage) + "" + Path.GetExtension(currentAchievements.strMasterImage);
                    drUserDisplayAchievements["strLink"] = "#";
                    dtUserDisplayAchievements.Rows.Add(drUserDisplayAchievements);
                }
                else if (lstAchievements.Contains(currentMission.iAchievementID))
                {
                    drUserDisplayAchievements["FullPathForImage"] = "Achievements/" + currentAchievements.strPathToImages + "/" + Path.GetFileNameWithoutExtension(currentAchievements.strMasterImage) + "" + Path.GetExtension(currentAchievements.strMasterImage);
                    drUserDisplayAchievements["strLink"] = "Accelerate-Current-Mission.aspx?iMissionID=" + iMissionIDNum;
                    dtUserDisplayAchievements.Rows.Add(drUserDisplayAchievements);
                }
                else
                {
                    drUserDisplayAchievements["FullPathForImage"] = "img/Lock.png";
                    drUserDisplayAchievements["strLink"] = "Accelerate-Current-Mission.aspx?iMissionID=" + iMissionIDNum;
                    dtUserDisplayAchievements.Rows.Add(drUserDisplayAchievements);

                    //### SET THE NEXT MISSION
                    if (iCount == 1)
                    {
                        ++iCount;
                        iNextMissionID = iMissionIDNum;
                    }
                }

                ++iAchieveTotalCount;

                if (iAchieveTotalCount == 1)
                    drUserDisplayAchievements["strNewRow"] = "<div class='row-fluid'>";
                else if (iAchieveTotalCount == 9)
                    drUserDisplayAchievements["strNewRow"] = "</div><div class='row-fluid'>";
                else
                    drUserDisplayAchievements["strNewRow"] = "";

                drUserDisplayAchievements["iMissionID"] = iMissionIDNum;
                drUserDisplayAchievements["strTitle"] = currentAchievements.strTitle;
            }
        }
        else if (iPhaseLevel == 2)
        {
            foreach (DataRow dr in dtPhase2Missions.Rows)
            {
                lstMissions.Add(Convert.ToInt32(dr["iMissionID"]));
            }
            foreach (DataRow dr in dtUserAchievements.Rows)
            {
                lstAchievements.Add(Convert.ToInt32(dr["iAchievementID"]));
            }

            foreach (int iMissionIDNum in lstMissions)
            {
                clsMissions currentMission = new clsMissions(iMissionIDNum);
                clsAchievements currentAchievements = new clsAchievements(currentMission.iAchievementID);
                DataRow drUserDisplayAchievements = dtUserDisplayAchievements.NewRow();

                drUserDisplayAchievements["iMissionID"] = iMissionIDNum;
                drUserDisplayAchievements["strTitle"] = currentAchievements.strTitle;

                if ((lstAchievements.Contains(currentMission.iAchievementID)) && (currentMission.iAchievementID == 1))
                {
                    drUserDisplayAchievements["FullPathForImage"] = "Achievements/" + currentAchievements.strPathToImages + "/" + Path.GetFileNameWithoutExtension(currentAchievements.strMasterImage) + "" + Path.GetExtension(currentAchievements.strMasterImage);
                    drUserDisplayAchievements["strLink"] = "#";
                    dtUserDisplayAchievements.Rows.Add(drUserDisplayAchievements);
                }
                else if (lstAchievements.Contains(currentMission.iAchievementID))
                {
                    drUserDisplayAchievements["FullPathForImage"] = "Achievements/" + currentAchievements.strPathToImages + "/" + Path.GetFileNameWithoutExtension(currentAchievements.strMasterImage) + "" + Path.GetExtension(currentAchievements.strMasterImage);
                    drUserDisplayAchievements["strLink"] = "Accelerate-Current-Mission.aspx?iMissionID=" + iMissionIDNum;
                    dtUserDisplayAchievements.Rows.Add(drUserDisplayAchievements);
                }
                else
                {
                    drUserDisplayAchievements["FullPathForImage"] = "img/Lock.png";
                    drUserDisplayAchievements["strLink"] = "Accelerate-Current-Mission.aspx?iMissionID=" + iMissionIDNum;
                    dtUserDisplayAchievements.Rows.Add(drUserDisplayAchievements);

                    //### SET THE NEXT MISSION
                    if (iCount == 1)
                    {
                        ++iCount;
                        iNextMissionID = iMissionIDNum;
                    }
                }

                ++iAchieveTotalCount;

                if (iAchieveTotalCount == 1)
                    drUserDisplayAchievements["strNewRow"] = "<div class='row-fluid'>";
                else if (iAchieveTotalCount == 9)
                    drUserDisplayAchievements["strNewRow"] = "</div><div class='row-fluid'>";
                else
                    drUserDisplayAchievements["strNewRow"] = "";

                drUserDisplayAchievements["iMissionID"] = iMissionIDNum;
                drUserDisplayAchievements["strTitle"] = currentAchievements.strTitle;
            }
        }
        else if (iPhaseLevel == 3)
        {
            foreach (DataRow dr in dtPhase3Missions.Rows)
            {
                lstMissions.Add(Convert.ToInt32(dr["iMissionID"]));
            }
            foreach (DataRow dr in dtUserAchievements.Rows)
            {
                lstAchievements.Add(Convert.ToInt32(dr["iAchievementID"]));
            }

            foreach (int iMissionIDNum in lstMissions)
            {
                clsMissions currentMission = new clsMissions(iMissionIDNum);
                clsAchievements currentAchievements = new clsAchievements(currentMission.iAchievementID);
                DataRow drUserDisplayAchievements = dtUserDisplayAchievements.NewRow();

                drUserDisplayAchievements["iMissionID"] = iMissionIDNum;
                drUserDisplayAchievements["strTitle"] = currentAchievements.strTitle;

                if ((lstAchievements.Contains(currentMission.iAchievementID)) && (currentMission.iAchievementID == 1))
                {
                    drUserDisplayAchievements["FullPathForImage"] = "Achievements/" + currentAchievements.strPathToImages + "/" + Path.GetFileNameWithoutExtension(currentAchievements.strMasterImage) + "" + Path.GetExtension(currentAchievements.strMasterImage);
                    drUserDisplayAchievements["strLink"] = "#";
                    dtUserDisplayAchievements.Rows.Add(drUserDisplayAchievements);
                }
                else if (lstAchievements.Contains(currentMission.iAchievementID))
                {
                    drUserDisplayAchievements["FullPathForImage"] = "Achievements/" + currentAchievements.strPathToImages + "/" + Path.GetFileNameWithoutExtension(currentAchievements.strMasterImage) + "" + Path.GetExtension(currentAchievements.strMasterImage);
                    drUserDisplayAchievements["strLink"] = "Accelerate-Current-Mission.aspx?iMissionID=" + iMissionIDNum;
                    dtUserDisplayAchievements.Rows.Add(drUserDisplayAchievements);
                }
                else
                {
                    drUserDisplayAchievements["FullPathForImage"] = "img/Lock.png";
                    drUserDisplayAchievements["strLink"] = "Accelerate-Current-Mission.aspx?iMissionID=" + iMissionIDNum;
                    dtUserDisplayAchievements.Rows.Add(drUserDisplayAchievements);

                    //### SET THE NEXT MISSION
                    if (iCount == 1)
                    {
                        ++iCount;
                        iNextMissionID = iMissionIDNum;
                    }
                }

                ++iAchieveTotalCount;

                if (iAchieveTotalCount == 1)
                    drUserDisplayAchievements["strNewRow"] = "<div class='row-fluid'>";
                else if (iAchieveTotalCount == 9)
                    drUserDisplayAchievements["strNewRow"] = "</div><div class='row-fluid'>";
                else
                    drUserDisplayAchievements["strNewRow"] = "";

                drUserDisplayAchievements["iMissionID"] = iMissionIDNum;
                drUserDisplayAchievements["strTitle"] = currentAchievements.strTitle;
            }
        }

        rpAchievements.DataSource = dtUserDisplayAchievements;
        rpAchievements.DataBind();
    }

    protected void popCurrentMissionBanner(int iCurrentMissionID)
    {
        string strFullPathForMissionImage = "";
        clsMissions clsMissions = new clsMissions(iCurrentMissionID);

        if (clsMissions.iMissionVideoID != 0)
        {
            DataTable dtMissionVideos = clsMissionVideos.GetMissionVideosList("iMissionVideoID = " + clsMissions.iMissionVideoID, "");

            foreach (DataRow dr in dtMissionVideos.Rows)
            {
                strFullPathForMissionImage = "Missions/" + dr["strPathToImages"] + "/" + Path.GetFileNameWithoutExtension(dr["strMasterImage"].ToString()) + "" + Path.GetExtension(dr["strMasterImage"].ToString());
            }
        }
        else
        {
            strFullPathForMissionImage = "Missions/" + clsMissions.strPathToImages + "/" + Path.GetFileNameWithoutExtension(clsMissions.strMasterImage) + "" + Path.GetExtension(clsMissions.strMasterImage);
        }

        StringBuilder strbCurrentMission = new StringBuilder();

        strbCurrentMission.AppendLine("<div class='row-fluid'>");
        strbCurrentMission.AppendLine("<div class='span1'><center>");
        strbCurrentMission.AppendLine("<img src='" + strFullPathForMissionImage + "' align='center' />"); // width='60%;'
        strbCurrentMission.AppendLine("</center></div>");
        strbCurrentMission.AppendLine("<div class='span9' style='font-size: 12px; color: #000;'>");
        strbCurrentMission.AppendLine(clsMissions.strDescription.Replace("/n", "<br /><br />"));
        strbCurrentMission.AppendLine("</div>");
        strbCurrentMission.AppendLine("<div id='missionPoints' class='span2' style='padding-left:15px;'>");
        strbCurrentMission.AppendLine("<span style='font-size: 2.5em; color: #FFF; line-height: 1.2em; font-weight: 100;'>" + clsMissions.iBonusPoints + "</span><br />");
        strbCurrentMission.AppendLine("<span style='font-size: 1.5em; color: #FFF; line-height: 1em; font-weight: 600;'>MISSION</span><br />");
        strbCurrentMission.AppendLine("<span style='font-size: 1.5em; color: #FFF; line-height: 1em; font-weight: 600;'>POINTS</span><br />");
        strbCurrentMission.AppendLine("</div>");
        strbCurrentMission.AppendLine("</div><br style='clear:both;'/>");

        litBanner.Text = strbCurrentMission.ToString();

        litTitle.Text = clsMissions.strTitle;

        litDescription.Text = clsMissions.strTagLine.Replace("/n", "<br />");

        if ((clsMissions.strLink != null) && (clsMissions.strLink != ""))
        {
            lnkbtnDisplayPDF.Visible = true;
            lnkbtnDisplayPDF.InnerText = clsMissions.strLinkTitle;
            
            //lnkbtnDisplayPDF.Text = clsMissions.strLinkTitle;
            if (clsMissions.strLink.Contains(".pdf"))
            {
                string pathZoomLink = clsMissions.strLink.ToString() + "#zoom=75";
                ifrmItemViewer.Attributes["src"] = pathZoomLink;
                //ifrmViewer.Attributes["src"] = clsMissions.strLink.ToString();
                //ifrmViewer.Attributes["src"] = "http://www.pitt.edu/~kmram/132/lectures/registers+counters.pdf";
                //ModalPopupExtenderViewer.Show();
            }
            else
            {
                ifrmItemViewer.Attributes["src"] = clsMissions.strLink.ToString();
                //ModalPopupExtenderViewer.Show();
                //lnkbtnDisplayPDF.Attributes.Add("href", clsMissions.strLink);
                //lnkbtnDisplayPDF.Attributes.Add("target", "_blank");
            }
        }
        
    }
    protected void lnkbtnDisplayPDF_Click(object sender, EventArgs e)
    {
        //pdfDiv.Style["display"] = "block";
    }


    protected void popMissionProgressBarPercent(int iCurrentDoublePercent, int iCurrentMissionID)
    {
        int iDoublePercent = iCurrentDoublePercent;
        int iPercent = 0;

        if (iDoublePercent != 0)
        {
            iPercent = iDoublePercent / 2;
        }

        if (iDoublePercent == 200)
        {
            btnSave.Visible = false;
        }
    }

    protected void popMissionProgressBarPercentDragDropTeamshipMission(int iCurrentMissionID)
    {
        clsMissions clsCurrentMission = new clsMissions(iCurrentMissionID);
        bool bUserAchievementExists = clsCommonFunctions.DoesRecordExist("tblUserAchievements", "iAchievementID='" + clsCurrentMission.iAchievementID + "' AND iAccountUserID='" + clsAccountUsers.iAccountUserID + "'");

        if (bUserAchievementExists == true)
        {
            btnSave.Visible = false;
        }
    }

    protected void popMissionQuestions(int iCurrentMissionID)
    {
        DataTable dtMission = clsMissionQuestions.GetMissionQuestionsList("iMissionID=" + iCurrentMissionID, "");
        dtMission.Columns.Add("strAnswer");

        string strQuestion;
        int iMissionQuestionID;

        int iCount = 0;

        foreach (DataRow dtrAccountMember in dtMission.Rows)
        {
            ++iCount;

            iMissionQuestionID = Convert.ToInt32(dtrAccountMember["iMissionQuestionID"].ToString());
            strQuestion = dtrAccountMember["strQuestion"].ToString();
        }
        
        rpMisionQuestions.DataSource = dtMission;
        rpMisionQuestions.DataBind();
    }

    protected void popMissionDropDownQuestions(int iCurrentMissionID)
    {
        DataTable dtDropDownMission = clsMissionDropDownQuestions.GetMissionQuestionsList("iMissionID=" + iCurrentMissionID, "");

        dtDropDownMission.Columns.Add("strAnswer");

        string strQuestion;
        int iMissionDropDownQuestionID;

        int iCount = 0;

        foreach (DataRow dtrAccountMember in dtDropDownMission.Rows)
        {
            ++iCount;

            iMissionDropDownQuestionID = Convert.ToInt32(dtrAccountMember["iMissionDropDownQuestionID"].ToString());
            strQuestion = dtrAccountMember["strQuestion"].ToString();
        }

        rpMissionDropDownQuestions.DataSource = dtDropDownMission;
        rpMissionDropDownQuestions.DataBind();
    }

    protected void popDropDowns(int iCurrentMissionID)
    {
        if (rpMissionDropDownQuestions.Controls.Count > 0)
        {
            foreach (Control item in rpMissionDropDownQuestions.Controls)
            {
                foreach (Control c in item.Controls)
                {
                    if (c is DropDownList)
                    {
                        DropDownList lstQuestionOptions = (DropDownList)c.FindControl("lstQuestionAnswer");

                        string strMisionQuestionID = lstQuestionOptions.Attributes["CommandArgument"].ToString();
                        int iCurrentMissionQuestionID = Convert.ToInt32(strMisionQuestionID);

                        DataTable dtDropDownOptions = clsMissionDropDownAnswers.GetMissionAnswersList("iMissionDropDownQuestionID=" + iCurrentMissionQuestionID, "");
                        foreach (DataRow dr in dtDropDownOptions.Rows)
                        {
                            lstQuestionOptions.Items.Add(dr["strAnswer"].ToString());
                        }

                        //### Add default select option;
                        lstQuestionOptions.Items.Insert(0, new ListItem("--Not Selected--", "0"));

                    }
                }
            }
        }
    }

    protected void popPreviouslyAnsweredQuestions(int iCurrentMissionID)
    {
        if (rpMisionQuestions.Controls.Count > 0)
        {
            foreach (Control ctr in Page.Controls)
            {
                if (ctr is TextBox)
                {
                    TextBox t = (TextBox)ctr;
                    t.BackColor = Color.AliceBlue;
                    t.ReadOnly = false;
                }
            }

            foreach (Control item in rpMisionQuestions.Controls)
            {
                foreach (Control c in item.Controls)
                {
                    if (c is TextBox)
                    {
                        TextBox txt = new TextBox();
                        txt = (TextBox)c.FindControl("txtQuestionAnswer");
                        string strAnswerFromUser = txt.Text.ToString();
                        string strMisionQuestionID = txt.Attributes["CommandArgument"].ToString();
                        int iCurrentMissionQuestionID = Convert.ToInt32(strMisionQuestionID);

                        int iCurrentUserAnswerID = clsCommonFunctions.returnRecordID("iUserAnswerID", "tblUserAnswers", "iMissionQuestionID=" + iCurrentMissionQuestionID + " AND iAccountUserID=" + clsAccountUsers.iAccountUserID + " AND bIsDeleted=0");

                        if (iCurrentUserAnswerID != 0)
                        {
                            clsUserAnswers clsUserAnswers = new clsUserAnswers(iCurrentUserAnswerID);
                            txt.Text = clsUserAnswers.strAnswer;
                        }
                    }
                }
            }
        }
    }

    /// <summary>
    /// ### POP TEAMSHIP QUESTIONS DYNAMICALLY
    /// </summary>
    /// <param name="iCurrentMissionID"></param>
    //protected void popTeamShips(int iCurrentMissionID)
    //{
    //    DataTable dtMission = clsTeamShips.GetTeamShipsList("iMissionID=" + iCurrentMissionID, "");

    //    dtMission.Columns.Add("strNameAnswer");
    //    dtMission.Columns.Add("strTitleAnswer");
    //    dtMission.Columns.Add("FullPathForImage");

    //    string strName;
    //    string strTitle;
    //    int iTeamShipID;

    //    int iCount = 0;

    //    foreach (DataRow dtrAccountMember in dtMission.Rows)
    //    {
    //        ++iCount;

    //        iTeamShipID = Convert.ToInt32(dtrAccountMember["iTeamShipID"].ToString());
    //        strName = dtrAccountMember["strName"].ToString();
    //        strTitle = dtrAccountMember["strTitle"].ToString();

    //        clsTeamShips clsCurrentTeamShips = new clsTeamShips(iTeamShipID);

    //        if ((clsCurrentTeamShips.strMasterImage == "") || (clsCurrentTeamShips.strMasterImage == null))
    //        {
    //            dtrAccountMember["FullPathForImage"] = "images/imgNoImage.png";
    //        }
    //        else
    //        {
    //            dtrAccountMember["FullPathForImage"] = "/TeamShips/" + clsCurrentTeamShips.strPathToImages + "/" + Path.GetFileNameWithoutExtension(clsCurrentTeamShips.strMasterImage) + "" + Path.GetExtension(clsCurrentTeamShips.strMasterImage);
    //        }
    //    }

    //    rpMissionQuestions2.DataSource = dtMission;
    //    rpMissionQuestions2.DataBind();
    //}

    protected void popTeamShipsDragDrop(int iCurrentMissionID)
    {
        Session["lstCorrectList"] = null;
        DataTable dtMission = clsTeamShips.GetTeamShipsList("iMissionID=" + iCurrentMissionID, "");
        string strFullPath = "";

        dtMission.Columns.Add("strNameAnswer");
        dtMission.Columns.Add("strTitleAnswer");
        dtMission.Columns.Add("FullPathForImage");
        dtMission.Columns.Add("litLeaderInfo");

        string strName;
        string strTitle;
        int iTeamShipID;

        int iCount = 0;

        litLeadershipInfo.Text = "Drag and drop the pictures on the right to the relevant placeholders.<br /><br />";

        foreach (DataRow dtrAccountMember in dtMission.Rows)
        {
            ++iCount;

            iTeamShipID = Convert.ToInt32(dtrAccountMember["iTeamShipID"].ToString());
            strName = dtrAccountMember["strName"].ToString();
            strTitle = dtrAccountMember["strTitle"].ToString();

            clsTeamShips clsCurrentTeamShips = new clsTeamShips(iTeamShipID);

            if ((clsCurrentTeamShips.strMasterImage == "") || (clsCurrentTeamShips.strMasterImage == null))
            {
                dtrAccountMember["FullPathForImage"] = "images/imgNoImage.png";
                strFullPath = "images/imgNoImage.png";
            }
            else
            {
                dtrAccountMember["FullPathForImage"] = "/TeamShips/" + clsCurrentTeamShips.strPathToImages + "/" + Path.GetFileNameWithoutExtension(clsCurrentTeamShips.strMasterImage) + "" + Path.GetExtension(clsCurrentTeamShips.strMasterImage);
                strFullPath = "/TeamShips/" + clsCurrentTeamShips.strPathToImages + "/" + Path.GetFileNameWithoutExtension(clsCurrentTeamShips.strMasterImage) + "" + Path.GetExtension(clsCurrentTeamShips.strMasterImage);
            }

            StringBuilder strbLeaderInfo = new StringBuilder();

            //strbLeaderInfo.AppendLine("<div class='well ProfileSection' style='background: #00aae7; color: #fff;'>");
            //strbLeaderInfo.AppendLine("<center><img onerror=\"this.src='img/DefaultNoImageProfile.png'\" src='" + strFullPath + "' /></center><br /><br />");
            strbLeaderInfo.AppendLine("<b>" + strName + "</b><br />");
            strbLeaderInfo.AppendLine(strTitle);

            dtrAccountMember["litLeaderInfo"] = strbLeaderInfo;
        }
        rpTeamShipDragDropNames.DataSource = dtMission;
        rpTeamShipDragDropNames.DataBind();

        //Tesing Randomize
        Random rndRandomNumber = new Random();
        List<int> lstRandomNumbers = new List<int>();
        int iRowCount = dtMission.Rows.Count;

        for (int i=0; i<iRowCount; i++)
        {
            int iRandom = rndRandomNumber.Next(0, iRowCount);
            while (lstRandomNumbers.Contains(iRandom))
            {
                iRandom = rndRandomNumber.Next(0, iRowCount);
            }

            lstRandomNumbers.Add(iRandom);
        }

        DataTable dtRandomizeItems = dtMission.Clone();
        foreach (int x in lstRandomNumbers)
        {
            DataRow drNewRandomRow = dtRandomizeItems.NewRow();
            drNewRandomRow.ItemArray = dtMission.Rows[x].ItemArray;
            dtRandomizeItems.Rows.Add(drNewRandomRow);
        }

        //rpTeamShipDragDropImages.DataSource = dtMission;
        //rpTeamShipDragDropImages.DataBind();

        rpTeamShipDragDropImages.DataSource = dtRandomizeItems;
        rpTeamShipDragDropImages.DataBind();

    }

    /// <summary>
    /// POP TEAMSHIP ANSWERED QUESTIONS INTO DYNAMICALLY CREATED TEXTBOXES
    /// </summary>
    /// <param name="iCurrentMissionID"></param>
    //protected void popPreviouslyAnsweredTeamshipQuestions(int iCurrentMissionID)
    //{
    //    if (rpMissionQuestions2.Controls.Count > 0)
    //    {
    //        foreach (Control ctr in Page.Controls)
    //        {
    //            if (ctr is TextBox)
    //            {
    //                TextBox t = (TextBox)ctr;
    //                t.BackColor = Color.AliceBlue;
    //                t.ReadOnly = false;
    //            }
    //        }

    //        foreach (Control item in rpMissionQuestions2.Controls)
    //        {
    //            foreach (Control c in item.Controls)
    //            {
    //                if (c is TextBox)
    //                {
    //                    TextBox txtN = new TextBox();
    //                    txtN = (TextBox)c.FindControl("txtName");

    //                    string strNameAnswerFromUser = "";

    //                    //if (!String.IsNullOrEmpty(txtN.Text))
    //                    strNameAnswerFromUser = txtN.Text.ToString();

    //                    TextBox txtT = new TextBox();
    //                    txtT = (TextBox)c.FindControl("txtTitle");

    //                    string strTitleAnswerFromUser = "";

    //                    //if (!String.IsNullOrEmpty(txtT.Text))
    //                    strTitleAnswerFromUser = txtT.Text.ToString();

    //                    string strTeamshipID = txtT.Attributes["CommandArgument"].ToString();

    //                    //int iCurrentMissionQuestionID = Convert.ToInt32(strTeamshipQuestionID);

    //                    int iCurrentTeamshipAnswerID = clsCommonFunctions.returnRecordID("iTeamShipAnswerID", "tblTeamShipAnswers", "iMissionID=" + iCurrentMissionID + " AND iAddedBy=" + clsAccountUsers.iAccountUserID + " AND iTeamshipID=" + strTeamshipID + " AND bIsDeleted=0");

    //                    if (iCurrentTeamshipAnswerID != 0)
    //                    {
    //                        clsTeamShipAnswers clsTeamShipAnswers = new clsTeamShipAnswers(iCurrentTeamshipAnswerID);
    //                        txtN.Text = clsTeamShipAnswers.strNameAnswer;
    //                        txtT.Text = clsTeamShipAnswers.strTitleAnswer;
    //                    }
    //                }
    //            }
    //        }
    //    }
    //}

    protected void popPreviouslyAnsweredTeamshipDragDropQuestions(int iCurrentMissionID)
    {
        clsMissions clsCurrentMission = new clsMissions(iCurrentMissionID);
        bool bUserAchievementExists = clsCommonFunctions.DoesRecordExist("tblUserAchievements", "iAchievementID='" + clsCurrentMission.iAchievementID + "' AND iAccountUserID='" + clsAccountUsers.iAccountUserID + "'");
        if (bUserAchievementExists)
        {
            //Page.ClientScript.RegisterStartupScript(GetType(), "sayHello", "sayHello();", true);
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "sayHello", "sayHello();", true);
        }
    }

    protected void popTeamshipQuestionsDragDropInfo(int iCurrentMissionID)
    {
        if (rpTeamShipDragDropNames.Controls.Count > 0)
        {
            foreach (Control ctr in Page.Controls)
            {
                if (ctr is TextBox)
                {
                    TextBox t = (TextBox)ctr;
                    t.BackColor = Color.AliceBlue;
                    t.ReadOnly = false;
                }
            }

            foreach (Control item in rpTeamShipDragDropNames.Controls)
            {
                foreach (Control c in item.Controls)
                {
                    if (c is TextBox)
                    {
                        TextBox txtN = new TextBox();
                        txtN = (TextBox)c.FindControl("txtName");

                        string strNameAnswerFromUser = "";

                        //if (!String.IsNullOrEmpty(txtN.Text))
                        strNameAnswerFromUser = txtN.Text.ToString();

                        TextBox txtT = new TextBox();
                        txtT = (TextBox)c.FindControl("txtTitle");

                        string strTitleAnswerFromUser = "";

                        //if (!String.IsNullOrEmpty(txtT.Text))
                        strTitleAnswerFromUser = txtT.Text.ToString();

                        int strTeamshipID = Convert.ToInt32(txtT.Attributes["CommandArgument"].ToString());

                        //int iCurrentMissionQuestionID = Convert.ToInt32(strTeamshipQuestionID);

                        //int iCurrentTeamshipAnswerID = clsCommonFunctions.returnRecordID("iTeamShipAnswerID", "tblTeamShipAnswers", "iMissionID=" + iCurrentMissionID + " AND iAddedBy=" + clsAccountUsers.iAccountUserID + " AND iTeamshipID=" + strTeamshipID + " AND bIsDeleted=0");
                        if (strTeamshipID != 0)
                        {
                            clsTeamShips clsTeamShips = new clsTeamShips(strTeamshipID);
                            txtN.Text = clsTeamShips.strName;
                            txtT.Text = clsTeamShips.strTitle;
                        }
                    }
                }
            }
        }
    }



    protected void popPreviouslyAnsweredDropDownQuestions(int iCurrentMissionID)
    {
        if (rpMissionDropDownQuestions.Controls.Count > 0)
        {
            foreach (Control ctr in Page.Controls)
            {
                if (ctr is DropDownList)
                {
                    DropDownList ddl = (DropDownList)ctr;
                    ddl.BackColor = Color.AliceBlue;
                }
            }

            foreach (Control item in rpMissionDropDownQuestions.Controls)
            {
                foreach (Control c in item.Controls)
                {
                    if (c is DropDownList)
                    {
                        DropDownList ddl = new DropDownList();
                        ddl = (DropDownList)c.FindControl("lstQuestionAnswer");

                        string strNameAnswerFromUser = "";

                        //if (!String.IsNullOrEmpty(txtN.Text))
                        strNameAnswerFromUser = ddl.SelectedValue.ToString();

                        string strDropDownID = ddl.Attributes["CommandArgument"].ToString();

                        //int iCurrentMissionQuestionID = Convert.ToInt32(strTeamshipQuestionID);

                        int iCurrentDropDownAnswerID = clsCommonFunctions.returnRecordID("iUserDropDownAnswerID", "tblUserDropDownAnswers", "iMissionID=" + iCurrentMissionID + " AND iAddedBy=" + clsAccountUsers.iAccountUserID + " AND iMissionDropDownQuestionID=" + strDropDownID + " AND bIsDeleted=0");

                        if (iCurrentDropDownAnswerID != 0)
                        {
                            clsUserDropDownAnswers clsUserDropDownAnswers = new clsUserDropDownAnswers(iCurrentDropDownAnswerID);
                            ddl.SelectedValue = clsUserDropDownAnswers.strAnswer;
                            ddl.BackColor = Color.AliceBlue;
                            ddl.Enabled = false;
                        }
                        else
                        {
                            ddl.Enabled = true;
                        }
                    }
                }
            }
        }
    }

    

    #endregion

    #region SAVE METHODS

    protected void btnSave_Click(object sender, EventArgs e)
    {
        //try
        //{
            int iMissionID = Convert.ToInt32(Request.QueryString["iMissionID"]);
            clsMissions clsMissions = new clsMissions(iMissionID);

            if (clsMissions.iMissionTypeID == 1)
            {
                SaveTextMission(iMissionID);
            }
            else if (clsMissions.iMissionTypeID == 2)
            {
                //SaveTeamLeadershipMission(iMissionID);
                SaveTeamLeadershipDragDropMission(iMissionID);
            }
            else if (clsMissions.iMissionTypeID == 4)
            {
                SaveDropDownMission(iMissionID);
            }
        //}
        //catch(Exception ex)
        //{
        //    Response.Redirect("Accelerate-Error404.aspx");
        //}
    }

    private void SaveTextMission(int iMissionID)
    {
        if (rpMisionQuestions.Controls.Count > 0)
        {
            foreach (Control ctr in Page.Controls)
            {
                if (ctr is TextBox)
                {
                    TextBox t = (TextBox)ctr;
                    t.BackColor = Color.AliceBlue;
                    t.ReadOnly = false;
                }
            }

            foreach (Control item in rpMisionQuestions.Controls)
            {
                foreach (Control c in item.Controls)
                {
                    if (c is TextBox)
                    {
                        TextBox txt = new TextBox();
                        txt = (TextBox)c.FindControl("txtQuestionAnswer");
                        string strAnswerFromUser = txt.Text.ToString();

                        string strMisionQuestionID = txt.Attributes["CommandArgument"].ToString();

                        int iMissionQuestionID = Convert.ToInt32(strMisionQuestionID);
                        bool bDoesAnswerExist = bDoesACorrectAnswerExist(iMissionQuestionID);
                        TextBox t = (TextBox)c;

                        if (strAnswerFromUser == "")
                        {
                            //### This answer is INCOMPLETE
                            Color myColourRed = ColorTranslator.FromHtml("#f09ca2");
                            t.BackColor = myColourRed;
                            t.ReadOnly = false;
                        }
                        else if (strAnswerFromUser != "")
                        {
                            if (bDoesAnswerExist)
                            {
                                if (strAnswerValue(iMissionQuestionID).ToLower() == strAnswerFromUser.ToLower())
                                {
                                    //### CORRECT
                                    t.BackColor = Color.Transparent;
                                    t.ReadOnly = false;

                                    int iCurrentUserAnswerID = clsCommonFunctions.returnRecordID("iUserAnswerID", "tblUserAnswers", "iMissionQuestionID=" + iMissionQuestionID + " AND iAccountUserID=" + clsAccountUsers.iAccountUserID + " AND bIsDeleted=0");

                                    clsUserAnswers clsUserAnswers = new clsUserAnswers();
                                    clsMissionQuestions clsMissionQuestions = new clsMissionQuestions(iMissionQuestionID);

                                    if (iCurrentUserAnswerID != 0)
                                    {
                                        clsUserAnswers = new clsUserAnswers(iCurrentUserAnswerID);
                                        clsUserAnswers.iEditedBy = clsAccountUsers.iAccountUserID;
                                        clsUserAnswers.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
                                    }
                                    else
                                    { }

                                    clsUserAnswers.iAccountUserID = clsAccountUsers.iAccountUserID;
                                    clsUserAnswers.iAddedBy = clsAccountUsers.iAccountUserID;
                                    clsUserAnswers.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
                                    clsUserAnswers.iMissionQuestionID = iMissionQuestionID;
                                    clsUserAnswers.strAnswer = strAnswerFromUser;
                                    clsUserAnswers.iMissionID = iMissionID;
                                    clsUserAnswers.Update();
                                    string userAnswer = "";
                                    userAnswer = strAnswerFromUser;
                                    if (clsMissionQuestions.bIsCertificateQuestion == true)
                                    {
                                        SendMissionQuestionCertificate(userAnswer);
                                    }
                                }
                                else
                                {
                                    //### This answer is INCORRECT
                                    Color myColourRed = ColorTranslator.FromHtml("#f09ca2");
                                    t.BackColor = myColourRed;
                                    t.ReadOnly = false;
                                }
                            }
                            else
                            {
                                //### CORRECT
                                t.BackColor = Color.Transparent;
                                t.ReadOnly = false;

                                int iCurrentUserAnswerID = clsCommonFunctions.returnRecordID("iUserAnswerID", "tblUserAnswers", "iMissionQuestionID=" + iMissionQuestionID + " AND iAccountUserID=" + clsAccountUsers.iAccountUserID + " AND bIsDeleted=0");
                                clsUserAnswers clsUserAnswers = new clsUserAnswers();
                                clsMissionQuestions clsMissionQuestions = new clsMissionQuestions(iMissionQuestionID);

                                if (iCurrentUserAnswerID != 0)
                                {
                                    clsUserAnswers = new clsUserAnswers(iCurrentUserAnswerID);
                                    clsUserAnswers.iEditedBy = clsAccountUsers.iAccountUserID;
                                    clsUserAnswers.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
                                }
                                else
                                { }

                                clsUserAnswers.iAccountUserID = clsAccountUsers.iAccountUserID;
                                clsUserAnswers.iAddedBy = clsAccountUsers.iAccountUserID;
                                clsUserAnswers.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));

                                clsUserAnswers.iMissionQuestionID = iMissionQuestionID;
                                clsUserAnswers.strAnswer = strAnswerFromUser;
                                clsUserAnswers.iMissionID = iMissionID;
                                clsUserAnswers.Update();
                                //Send mission question certificate
                                string userAnswer = "";
                                userAnswer = strAnswerFromUser;
                                if (clsMissionQuestions.bIsCertificateQuestion == true)
                                {
                                    SendMissionQuestionCertificate(userAnswer);
                                }
                            }
                        }
                    }
                }
            }
        }

        ///### Check that all answers are complete
        if (iTotalQuestions(iMissionID) == iTotalQuestionsAnswered(iMissionID))
        {
            saveMission(iMissionID);
            checkIfPhaseIsComplete(clsTracking.iCurrentPhaseLevel);
            //Test
            clsMissions clsMissions = new clsMissions(iMissionID);
            if (clsMissions.iAdminEmailSettingsID != 0)
            {
                clsAdminEmailSettings clsAdminEmailSettings = new clsAdminEmailSettings(clsMissions.iAdminEmailSettingsID);
                SendMissionReport();
            }
            Response.Redirect("Accelerate-Mission-Success.aspx?iMissionID=" + iMissionID);
        }
        else {
            
        }
        //Page.ClientScript.RegisterStartupScript(GetType(), "hideTeamshipMission2", "hideTeamshipMission();", true);
    }

    private void SaveTeamLeadershipDragDropMission(int iMissionID)
    {
        Session["lstCorrectList"] = null;

        string strTeamshipAllCorrect = hfTeamshipAllCorrect.Value;
        string strIncorrectCount = hfTeamshipIncorrect.Value;
        string strCorrectCount = hfTeamshipCorrect.Value;
        //string strAllIDs = hfTeamshipFullIDCount.Value;
        string[] incorrectList;
        string[] correctList;
        //string[] AllIDs;
        if (strIncorrectCount != null && strIncorrectCount != "")
        {
            incorrectList = strIncorrectCount.Split(',');
        }
        else
        {
            incorrectList = null;
        }

        if (strCorrectCount != null && strCorrectCount != "")
        {
            correctList = strCorrectCount.Split(',');
        }
        else
        {
            correctList = null;
        }

        //if (strAllIDs != null && strAllIDs != "")
        //{
        //    AllIDs = strAllIDs.Split(',');
        //}
        //else
        //{
        //    AllIDs = null;
        //}

        List<int> lstIncorrectList = new List<int>();
        List<int> lstCorrectList = new List<int>();
        //List<int> lstAllIDs = new List<int>();

        if (incorrectList != null && incorrectList.Length != 0)
        {
            foreach (string value in incorrectList)
            {
                int convertValue = Convert.ToInt32(value);
                lstIncorrectList.Add(convertValue);
            }
        }

        if (correctList != null && correctList.Length != 0)
        {
            foreach (string value in correctList)
            {
                int convertValue = Convert.ToInt32(value);
                lstCorrectList.Add(convertValue);
            }
        }

        //if (AllIDs != null && AllIDs.Length != 0)
        //{
        //    foreach (string value in AllIDs)
        //    {
        //        int convertValue = Convert.ToInt32(value);
        //        lstAllIDs.Add(convertValue);
        //    }
        //}

        Session["lstCorrectList"] = lstCorrectList;
        //### Check that all answers are complete

        if (strTeamshipAllCorrect == "true")
        {
                saveMission(iMissionID);
                checkIfPhaseIsComplete(clsTracking.iCurrentPhaseLevel);
                //Test
                //clsMissions clsMissions = new clsMissions(iMissionID);
                //if (clsMissions.iAdminEmailSettingsID != 0)
                //{
                //    clsAdminEmailSettings clsAdminEmailSettings = new clsAdminEmailSettings(clsMissions.iAdminEmailSettingsID);
                //    SendMissionReport();
                //}
            Session["lstCorrectList"] = null;
            Response.Redirect("Accelerate-Mission-Success.aspx?iMissionID=" + iMissionID);
        }
        //else { Page.Response.Redirect(Page.Request.Url.ToString(), false); }
        else
        {
            foreach (Control item in rpTeamShipDragDropNames.Controls)
            {
                foreach (Control c in item.Controls)
                {
                    if (c is TextBox)
                    {
                        TextBox txtN = new TextBox();
                        txtN = (TextBox)c.FindControl("txtName");

                        string strNameAnswerFromUser = "";

                        //if (!String.IsNullOrEmpty(txtN.Text))
                        strNameAnswerFromUser = txtN.Text.ToString();

                        TextBox txtT = new TextBox();
                        txtT = (TextBox)c.FindControl("txtTitle");

                        string strTitleAnswerFromUser = "";

                        //if (!String.IsNullOrEmpty(txtT.Text))
                        strTitleAnswerFromUser = txtT.Text.ToString();

                        int strTeamshipID = Convert.ToInt32(txtT.Attributes["CommandArgument"].ToString());
                        string divID = "infoDiv" + strTeamshipID.ToString();

                        if (lstIncorrectList.Contains(strTeamshipID))
                        {
                            Color myColourRed = ColorTranslator.FromHtml("#f09ca2");
                            //txtN.Attributes["background-color"] = "none";
                            //txtT.Attributes["background-color"] = "none";
                            //txtN.BackColor = myColourRed;
                            //txtT.BackColor = myColourRed;
                            //txtN.Style["background-color"] = "#f09ca2";
                            //txtT.Style["background-color"] = "#f09ca2";
                        }
                        else if(lstCorrectList.Contains(strTeamshipID) == false)
                        {
                            Color myColourRed = ColorTranslator.FromHtml("#f09ca2");
                            //txtN.Attributes["background-color"] = "none";
                            //txtT.Attributes["background-color"] = "none";
                            //txtN.BackColor = "#f09ca2";
                            //txtT.BackColor = myColourRed;
                            //txtN.Style["background-color"] = "#f09ca2";
                            //txtT.Style["background-color"] = "#f09ca2";
                        }
                        else
                        {
                            Color myColourRed = ColorTranslator.FromHtml("#ebebea");
                            //txtN.Attributes["background-color"] = "none";
                            //txtT.Attributes["background-color"] = "none";
                        }
                        //int iCurrentMissionQuestionID = Convert.ToInt32(strTeamshipQuestionID);

                        //int iCurrentTeamshipAnswerID = clsCommonFunctions.returnRecordID("iTeamShipAnswerID", "tblTeamShipAnswers", "iMissionID=" + iCurrentMissionID + " AND iAddedBy=" + clsAccountUsers.iAccountUserID + " AND iTeamshipID=" + strTeamshipID + " AND bIsDeleted=0");

                    }
                }
            }
            clsMissions newclsMissions = new clsMissions(iMissionID);
            checkTeamLeadershipDragDropCorrectAnswers(newclsMissions.iMissionTypeID);
        }
    }

    private void SaveDropDownMission(int iMissionID)
    {
        int iTotalCorrectCount = 0;
        int iTotalIncorrectCount = 0;

        if (rpMissionDropDownQuestions.Controls.Count > 0)
        {
            foreach (Control ctr in Page.Controls)
            {
                if (ctr is DropDownList)
                {
                    DropDownList t = (DropDownList)ctr;
                    t.BackColor = Color.AliceBlue;
                }
            }

            foreach (Control item in rpMissionDropDownQuestions.Controls)
            {
                foreach (Control c in item.Controls)
                {
                    if (c is DropDownList)
                    {
                        ///### For Name
                        DropDownList lstList = new DropDownList();
                        lstList = (DropDownList)c.FindControl("lstQuestionAnswer");

                        string strAnswerFromDropDown = lstList.SelectedValue.ToString();
                        DropDownList lstFinal = (DropDownList)c;

                        //### Common for both name and title
                        string strDropDownID = lstList.Attributes["CommandArgument"].ToString();

                        //### iDropDownID is i DROP DOWN QUESITON ID
                        int iDropDownID = Convert.ToInt32(strDropDownID);

                        if (strAnswerFromDropDown == "")
                        {
                            //### This answer is INCOMPLETE
                            Color myColourRed = ColorTranslator.FromHtml("#f09ca2");
                            lstFinal.BackColor = myColourRed;
                        }
                        else if (strAnswerFromDropDown != "")
                        {
                            if (true)
                            {
                                bool bIsAnswerCorrect = false;

                                //### Check Answer
                                string strCorrectAnswer = strDropDownAnswerValue(iDropDownID).ToLower();
                                if (strCorrectAnswer == strAnswerFromDropDown.ToLower())
                                {
                                    bIsAnswerCorrect = true;
                                }
                                else { }

                                int iAnswerFromUser = 0;
                                DataTable dtCorrectID = clsMissionDropDownAnswers.GetMissionAnswersList("iMissionDropDownQuestionID= " + iDropDownID + " AND strAnswer= '" + strAnswerFromDropDown + "'", "");

                                foreach (DataRow dr in dtCorrectID.Rows)
                                {
                                    iAnswerFromUser = Int32.Parse(dr["iMissionDropDownAnswerID"].ToString());
                                    break;
                                }

                                if (bIsAnswerCorrect)
                                {
                                    int iCurrentDropDownAnswerID = clsCommonFunctions.returnRecordID("iUserDropDownAnswerID", "tblUserDropDownAnswers", "iMissionDropDownQuestionID=" + iDropDownID + " AND iAddedBy=" + clsAccountUsers.iAccountUserID + " AND bIsDeleted=0");
                                    clsUserDropDownAnswers clsUserDropDownAnswers = new clsUserDropDownAnswers();

                                    if (iCurrentDropDownAnswerID != 0)
                                    {
                                        clsUserDropDownAnswers = new clsUserDropDownAnswers(iCurrentDropDownAnswerID);
                                        clsUserDropDownAnswers.iEditedBy = clsAccountUsers.iAccountUserID;
                                        clsUserDropDownAnswers.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
                                    }
                                    else
                                    {
                                        clsUserDropDownAnswers.iAddedBy = clsAccountUsers.iAccountUserID;
                                        clsUserDropDownAnswers.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
                                        clsUserDropDownAnswers.iMissionDropDownAnswerID = iAnswerFromUser;
                                        clsUserDropDownAnswers.iMissionDropDownQuestionID = iDropDownID;
                                        clsUserDropDownAnswers.strAnswer = strAnswerFromDropDown;
                                        clsUserDropDownAnswers.iMissionID = iMissionID;
                                    }
                                    clsUserDropDownAnswers.Update();

                                    //### CORRECT
                                    lstFinal.BackColor = Color.Transparent;
                                    ++iTotalCorrectCount;

                                }
                                else if (!bIsAnswerCorrect)
                                {
                                    //### RED FOR WARNING
                                    Color myColourRed = ColorTranslator.FromHtml("#f09ca2");

                                    //### This answer is INCORRECT
                                    lstFinal.BackColor = myColourRed;
                                    ++iTotalIncorrectCount;
                                }
                            }
                        }
                    }
                }
            }
        }

        ///### Check that all answers are complete
        //if ((iTotalIncorrectCount==0) && (iTotalCorrectCount/2)==iTotalTeamshipQuestionsAnswered(2))        

        //f (iTotalDropDownQuestions(iMissionID) == iTotalCorrectCount)
        if (iTotalDropDownQuestions(iMissionID) == iTotalDropDownQuestionsAnswered(iMissionID))
        {
            saveMission(iMissionID);
            checkIfPhaseIsComplete(clsTracking.iCurrentPhaseLevel);
            //Test
            //clsMissions clsMissions = new clsMissions(iMissionID);
            //if (clsMissions.iAdminEmailSettingsID != 0)
            //{
            //    clsAdminEmailSettings clsAdminEmailSettings = new clsAdminEmailSettings(clsMissions.iAdminEmailSettingsID);
            //    SendMissionReport();
            //}

            Response.Redirect("Accelerate-Mission-Success.aspx?iMissionID=" + iMissionID);
        }
        else {
            Page.ClientScript.RegisterStartupScript(GetType(), "hideTeamshipMission", "hideTeamshipMission();", true);
        }
    }

    protected void checkIfPhaseIsComplete(int iUserCurrentPhaseLevel)
    {
        int numMissions;

        DataTable dtUserCompletedMissions = clsUserAchievements.GetUserAchievementsRecords(clsAccountUsers.iAccountUserID);

        if (iUserCurrentPhaseLevel == 1)
        {
            numMissions = 0;

            //DataTable dtPhase1Missions = clsPhaseMissions.GetPhaseMissionsList("iPhaseID=" + clsTracking.iPhase1ID, "");
            DataTable dtUserAchievements = clsUserAchievements.GetUserAchievementsRecords(clsAccountUsers.iAccountUserID);

            numMissions += dtPhase1Missions.Rows.Count;

            if (numMissions == dtUserAchievements.Rows.Count)
            {
                SendPhaseCompletionEmail();
                clsTracking.bIsPhase1Complete = true;
                clsTracking.iCurrentPhaseLevel = 2;
                clsTracking.Update();

                Response.Redirect("Accelerate-Phase-Complete.aspx");
            }
        }

        if (iUserCurrentPhaseLevel == 2)
        {
            numMissions = 0;

            //DataTable dtPhase1Missions = clsPhaseMissions.GetPhaseMissionsList("iPhaseID=" + clsTracking.iPhase1ID, "");
            //DataTable dtPhase2Missions = clsPhaseMissions.GetPhaseMissionsList("iPhaseID=" + clsTracking.iPhase2ID, "");
            DataTable dtUserAchievements = clsUserAchievements.GetUserAchievementsRecords(clsAccountUsers.iAccountUserID);

            numMissions += dtPhase1Missions.Rows.Count;
            numMissions += dtPhase2Missions.Rows.Count;

            if (numMissions == dtUserAchievements.Rows.Count)
            {
                SendPhaseCompletionEmail();
                clsTracking.bIsPhase2Complete = true;
                clsTracking.iCurrentPhaseLevel = 3;
                clsTracking.Update();

                Response.Redirect("Accelerate-Phase-Complete.aspx");
            }
        }

        if (iUserCurrentPhaseLevel == 3)
        {
            numMissions = 0;

            //dtPhase1Missions = clsPhaseMissions.GetPhaseMissionsList("iPhaseID=" + clsTracking.iPhase1ID, "");
            //dtPhase2Missions = clsPhaseMissions.GetPhaseMissionsList("iPhaseID=" + clsTracking.iPhase2ID, "");
            //dtPhase3Missions = clsPhaseMissions.GetPhaseMissionsList("iPhaseID=" + clsTracking.iPhase3ID, "");
            DataTable dtUserAchievements = clsUserAchievements.GetUserAchievementsRecords(clsAccountUsers.iAccountUserID);

            numMissions += dtPhase1Missions.Rows.Count;
            numMissions += dtPhase2Missions.Rows.Count;
            numMissions += dtPhase3Missions.Rows.Count;

            if (numMissions == dtUserAchievements.Rows.Count)
            {
                clsTracking.bIsPhase3Complete = true;
                clsTracking.Update();
                SendFinalEmail();
                Response.Redirect("Accelerate-Certificate-Page.aspx");
            }
        }
    }

    protected void saveMission(int iCurrentMissionID)
    {
        clsMissions clsCurrentMission = new clsMissions(iCurrentMissionID);

        #region EMAIL CONTENT START

        Attachment[] emptyVariable = new Attachment[] { };
        StringBuilder strbEmailContent = new StringBuilder();

        strbEmailContent.AppendLine("<!-- Start of textbanner -->");
        strbEmailContent.AppendLine("<table width='100%' bgcolor='#ffffff' cellpadding='0' cellspacing='0' border='0' id='backgroundTable' movable=''>");
        strbEmailContent.AppendLine("<tbody>");
        strbEmailContent.AppendLine("<tr>");
        strbEmailContent.AppendLine("<td>");
        strbEmailContent.AppendLine("<table bgcolor='#ffffff' width='650' cellpadding='0' cellspacing='0' border='0' align='center' class='devicewidth' options=''>");
        strbEmailContent.AppendLine("<tbody>");
        strbEmailContent.AppendLine("<!-- Spacing -->");
        strbEmailContent.AppendLine("<tr>");
        strbEmailContent.AppendLine("<td height='50'></td>");
        strbEmailContent.AppendLine("</tr>");
        strbEmailContent.AppendLine("<!-- End of Spacing -->");
        strbEmailContent.AppendLine("<tr>");
        strbEmailContent.AppendLine("<td>");
        strbEmailContent.AppendLine("<table width='100%' cellspacing='0' cellpadding='0'>");
        strbEmailContent.AppendLine("<tbody>");

        strbEmailContent.AppendLine("<!-- Content -->");
        strbEmailContent.AppendLine("<tr>");
        strbEmailContent.AppendLine("<td width='60'></td>");
        strbEmailContent.AppendLine("<td valign='top' style='font-family: Helvetica, Arial, Arial;font-size: 14px; color: #919191; text-align:left;line-height: 22px;' text=''>");

        #endregion

        string strMissionTitle = clsCurrentMission.strTitle;
        int iMissionPoints = clsCurrentMission.iBonusPoints;
        string strReportName = "Mission Complete";
        bool bAlreadyExists = clsCommonFunctions.DoesRecordExist("tblNotifications", "iMissionID='" + iCurrentMissionID + "' AND iAccountUserID='" + clsAccountUsers.iAccountUserID + "' AND bIsDeleted=0");

        if (!bAlreadyExists)
        {
            clsNotifications clsNotifications = new clsNotifications();

            //### Add / Update
            clsNotifications.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            clsNotifications.iAddedBy = -10;
            clsNotifications.iAccountUserID = Convert.ToInt32(clsAccountUsers.iAccountUserID);
            clsNotifications.iGroupID = Convert.ToInt32(clsAccountUsers.iGroupID);
            clsNotifications.iMissionID = iCurrentMissionID;
            clsNotifications.strMessage = clsAccountUsers.strFirstName + " has just completed the mission: " + strMissionTitle + "";

            clsNotifications.Update();

            clsAccountUsers.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            clsAccountUsers.iEditedBy = Convert.ToInt32(clsAccountUsers.iAccountUserID);
            clsAccountUsers.iCurrentPoints = clsAccountUsers.iCurrentPoints + iMissionPoints;
            clsAccountUsers.Update();

            try
            {
                bool bUserAchievementExists = clsCommonFunctions.DoesRecordExist("tblUserAchievements", "iAchievementID='" + clsCurrentMission.iAchievementID + "' AND iAccountUserID='" + clsAccountUsers.iAccountUserID + "'");

                if (!bUserAchievementExists)
                {
                    clsUserAchievements clsUserAchievements = new clsUserAchievements();
                    clsUserAchievements.iAccountUserID = clsAccountUsers.iAccountUserID;
                    clsUserAchievements.iAchievementID = clsCurrentMission.iAchievementID;
                    clsUserAchievements.Update();

                    clsAchievements clsAchieve = new clsAchievements(clsCurrentMission.iAchievementID);

                    DataTable dtPhaseMission = clsPhaseMissions.GetPhaseMissionsList();

                    List<int> lstGroupsPhases = new List<int>();
                    DataTable dtGroupPhases = clsGroupPhases.GetGroupPhasesList("iGroupID =" + clsAccountUsers.iGroupID, "");
                    foreach (DataRow dr in dtGroupPhases.Rows)
                    {
                        lstGroupsPhases.Add(Convert.ToInt32(dr["iPhaseID"]));
                    }
                }

                //### SEND EMAIL NOTIFICATION
                strbEmailContent.AppendLine("<!-- Start of textbanner -->");
                strbEmailContent.AppendLine("<table width='100%' bgcolor='#efefef' cellpadding='0' cellspacing='0' border='0' id='backgroundTable' movable=''>");
                strbEmailContent.AppendLine("<tbody>");
                strbEmailContent.AppendLine("<tr>");
                strbEmailContent.AppendLine("<td>");
                strbEmailContent.AppendLine("<table bgcolor='#ffffff' width='650' cellpadding='0' cellspacing='0' border='0' align='center' class='devicewidth' options=''>"); //style='border-left:1px solid #333; border-right:1px solid #333;'
                strbEmailContent.AppendLine("<tbody>");
                strbEmailContent.AppendLine("<!-- Spacing -->");
                strbEmailContent.AppendLine("<tr>");
                strbEmailContent.AppendLine("<td height='50'></td>");
                strbEmailContent.AppendLine("</tr>");
                strbEmailContent.AppendLine("<!-- End of Spacing -->");
                strbEmailContent.AppendLine("<tr>");
                strbEmailContent.AppendLine("<td>");
                strbEmailContent.AppendLine("<table width='100%' cellspacing='0' cellpadding='0'>");
                strbEmailContent.AppendLine("<tbody>");

                strbEmailContent.AppendLine("<!-- Content -->");
                strbEmailContent.AppendLine("<tr>");
                strbEmailContent.AppendLine("<td width='60'></td>");
                strbEmailContent.AppendLine("<td valign='top' style='font-family: Helvetica, Arial, Arial;font-size: 14px; color: #919191; text-align:left;line-height: 22px;' text=''>");
                strbEmailContent.AppendLine("Dear " + clsAccountUsers.strFirstName + "<br /><br />");
                strbEmailContent.AppendLine("You have just completed the mission: " + strMissionTitle + "<br /><br />");
                strbEmailContent.AppendLine("<br/>");
                strbEmailContent.AppendLine("</td>");
                strbEmailContent.AppendLine("<td width='60'></td>");
                strbEmailContent.AppendLine("</tr>");
                strbEmailContent.AppendLine("<!-- End of Content -->");

                strbEmailContent.AppendLine("</td>");
                strbEmailContent.AppendLine("<td width='20'></td>");
                strbEmailContent.AppendLine("</tr>");
                strbEmailContent.AppendLine("</tbody>");
                strbEmailContent.AppendLine("</table>");
                strbEmailContent.AppendLine("</td>");
                strbEmailContent.AppendLine("</tr>");
                strbEmailContent.AppendLine("<!-- Spacing -->");
                strbEmailContent.AppendLine("<tr>");
                strbEmailContent.AppendLine("<td height='50'></td>");
                strbEmailContent.AppendLine("</tr>");
                strbEmailContent.AppendLine("<!-- End of Spacing -->");
                strbEmailContent.AppendLine("</tbody>");
                strbEmailContent.AppendLine("</table>");
                strbEmailContent.AppendLine("</td>");
                strbEmailContent.AppendLine("</tr>");
                strbEmailContent.AppendLine("</tbody>");
                strbEmailContent.AppendLine("</table>");
                strbEmailContent.AppendLine("<!-- End of textbanner -->");

                Attachment[] empty = new Attachment[] { };

                //try
                //{
                //notificationEmail.SendMail("no-reply@deloitteactivation.co.za", clsAccountUsers.strEmailAddress, "", "andrew@softservedigital.co.za", strReportName, strbMailBuilder.ToString(), "", "", empty, true);

                //clsTracking.Update();
                //}
                //    mandatoryDiv.Visible = false;

                //}
                //catch { }
            }
            catch
            {
                Exception ex;
            }
        }
    }

    //Get List of Ranks/ Get rank
    private List<string> strCustomGroupRank(int iCustomRankID, int iCurrentPoints)
    {
        List<string> lstRanks = new List<string>();
        string strRank = "";
        string strNextRank = "";
        string strPointLevel = "";
        string strNextPointLevel = "";

        clsCustomGroupRanks clsCustomRank = new clsCustomGroupRanks(iCustomRankID);

        if (iCurrentPoints <= clsCustomRank.iRank1Points)
        {
            if (iCurrentPoints == clsCustomRank.iRank1Points)
            {
                strRank = clsCustomRank.strRank2Title;
                strNextRank = clsCustomRank.strRank3Title;
                strPointLevel = clsCustomRank.iRank2Points.ToString();
                strNextPointLevel = clsCustomRank.iRank3Points.ToString();
                lstRanks.Add(strRank);
                lstRanks.Add(strNextRank);
                lstRanks.Add(strPointLevel);
                lstRanks.Add(strNextPointLevel);
            }
            else if (iCurrentPoints < clsCustomRank.iRank1Points)
            {
                strRank = clsCustomRank.strRank1Title;
                strNextRank = clsCustomRank.strRank2Title;
                strPointLevel = clsCustomRank.iRank1Points.ToString();
                strNextPointLevel = clsCustomRank.iRank2Points.ToString();
                lstRanks.Add(strRank);
                lstRanks.Add(strNextRank);
                lstRanks.Add(strPointLevel);
                lstRanks.Add(strNextPointLevel);
            }
        }
        else if (iCurrentPoints <= clsCustomRank.iRank2Points)
        {
            if (iCurrentPoints == clsCustomRank.iRank2Points)
            {
                strRank = clsCustomRank.strRank3Title;
                strNextRank = clsCustomRank.strRank4Title;
                strPointLevel = clsCustomRank.iRank3Points.ToString();
                strNextPointLevel = clsCustomRank.iRank4Points.ToString();
                lstRanks.Add(strRank);
                lstRanks.Add(strNextRank);
                lstRanks.Add(strPointLevel);
                lstRanks.Add(strNextPointLevel);
            }
            else if (iCurrentPoints < clsCustomRank.iRank2Points)
            {
                strRank = clsCustomRank.strRank2Title;
                strNextRank = clsCustomRank.strRank3Title;
                strPointLevel = clsCustomRank.iRank2Points.ToString();
                strNextPointLevel = clsCustomRank.iRank3Points.ToString();
                lstRanks.Add(strRank);
                lstRanks.Add(strNextRank);
                lstRanks.Add(strPointLevel);
                lstRanks.Add(strNextPointLevel);
            }
        }
        else if (iCurrentPoints <= clsCustomRank.iRank3Points)
        {

            if (iCurrentPoints == clsCustomRank.iRank3Points)
            {
                strRank = clsCustomRank.strRank4Title;
                strNextRank = clsCustomRank.strRank5Title;
                strPointLevel = clsCustomRank.iRank4Points.ToString();
                strNextPointLevel = clsCustomRank.iRank5Points.ToString();
                lstRanks.Add(strRank);
                lstRanks.Add(strNextRank);
                lstRanks.Add(strPointLevel);
                lstRanks.Add(strNextPointLevel);
            }
            else if (iCurrentPoints < clsCustomRank.iRank3Points)
            {
                strRank = clsCustomRank.strRank3Title;
                strNextRank = clsCustomRank.strRank4Title;
                strPointLevel = clsCustomRank.iRank3Points.ToString();
                strNextPointLevel = clsCustomRank.iRank4Points.ToString();
                lstRanks.Add(strRank);
                lstRanks.Add(strNextRank);
                lstRanks.Add(strPointLevel);
                lstRanks.Add(strNextPointLevel);
            }
        }
        else if (iCurrentPoints <= clsCustomRank.iRank4Points)
        {

            if (iCurrentPoints == clsCustomRank.iRank4Points)
            {
                strRank = clsCustomRank.strRank5Title;
                strNextRank = clsCustomRank.strRank6Title;
                strPointLevel = clsCustomRank.iRank5Points.ToString();
                strNextPointLevel = clsCustomRank.iRank6Points.ToString();
                lstRanks.Add(strRank);
                lstRanks.Add(strNextRank);
                lstRanks.Add(strPointLevel);
                lstRanks.Add(strNextPointLevel);
            }

            else if (iCurrentPoints < clsCustomRank.iRank4Points)
            {
                strRank = clsCustomRank.strRank4Title;
                strNextRank = clsCustomRank.strRank5Title;
                strPointLevel = clsCustomRank.iRank4Points.ToString();
                strNextPointLevel = clsCustomRank.iRank5Points.ToString();
                lstRanks.Add(strRank);
                lstRanks.Add(strNextRank);
                lstRanks.Add(strPointLevel);
                lstRanks.Add(strNextPointLevel);
            }
        }
        else if (iCurrentPoints <= clsCustomRank.iRank5Points)
        {
            if (iCurrentPoints == clsCustomRank.iRank5Points)
            {
                strRank = clsCustomRank.strRank6Title;
                strNextRank = clsCustomRank.strRank7Title;
                strPointLevel = clsCustomRank.iRank6Points.ToString();
                strNextPointLevel = clsCustomRank.iRank7Points.ToString();
                lstRanks.Add(strRank);
                lstRanks.Add(strNextRank);
                lstRanks.Add(strPointLevel);
                lstRanks.Add(strNextPointLevel);
            }
            else if (iCurrentPoints < clsCustomRank.iRank5Points)
            {
                strRank = clsCustomRank.strRank5Title;
                strNextRank = clsCustomRank.strRank6Title;
                strPointLevel = clsCustomRank.iRank5Points.ToString();
                strNextPointLevel = clsCustomRank.iRank6Points.ToString();
                lstRanks.Add(strRank);
                lstRanks.Add(strNextRank);
                lstRanks.Add(strPointLevel);
                lstRanks.Add(strNextPointLevel);
            }
        }
        else if (iCurrentPoints <= clsCustomRank.iRank6Points)
        {

            if (iCurrentPoints == clsCustomRank.iRank6Points)
            {
                strRank = clsCustomRank.strRank7Title;
                strNextRank = "None";
                strPointLevel = clsCustomRank.iRank7Points.ToString();
                strNextPointLevel = "0";
                lstRanks.Add(strRank);
                lstRanks.Add(strNextRank);
                lstRanks.Add(strPointLevel);
                lstRanks.Add(strNextPointLevel);
            }
            else if (iCurrentPoints < clsCustomRank.iRank6Points)
            {
                strRank = clsCustomRank.strRank6Title;
                strNextRank = clsCustomRank.strRank7Title;
                strPointLevel = clsCustomRank.iRank6Points.ToString();
                strNextPointLevel = clsCustomRank.iRank7Points.ToString();
                lstRanks.Add(strRank);
                lstRanks.Add(strNextRank);
                lstRanks.Add(strPointLevel);
                lstRanks.Add(strNextPointLevel);
            }
        }
        else if (iCurrentPoints <= clsCustomRank.iRank7Points)
        {
            strRank = clsCustomRank.strRank7Title;
            strNextRank = "None";
            strPointLevel = clsCustomRank.iRank7Points.ToString();
            strNextPointLevel = "0";
            lstRanks.Add(strRank);
            lstRanks.Add(strNextRank);
            lstRanks.Add(strPointLevel);
            lstRanks.Add(strNextPointLevel);
        }
        else
        {
            strRank = clsCustomRank.strRank7Title;
            strNextRank = "None";
            strPointLevel = clsCustomRank.iRank7Points.ToString();
            strNextPointLevel = "0";
            lstRanks.Add(strRank);
            lstRanks.Add(strNextRank);
            lstRanks.Add(strPointLevel);
            lstRanks.Add(strNextPointLevel);
        }

        string strMaxLevel = "";

        bool bMaxFound = false;

        if (bMaxFound == false)
        {
            if (clsCustomRank.iRank7Points != 0)
            {
                strMaxLevel = clsCustomRank.iRank7Points.ToString();
                if (iCurrentPoints.ToString() == strMaxLevel)
                {
                    strRank = clsCustomRank.strRank7Title;
                    lstRanks[0] = strRank;
                }
                bMaxFound = true;
            }
            else if (clsCustomRank.iRank6Points != 0)
            {
                strMaxLevel = clsCustomRank.iRank6Points.ToString();
                if (iCurrentPoints.ToString() == strMaxLevel)
                {
                    strRank = clsCustomRank.strRank6Title;
                    lstRanks[0] = strRank;
                }
                bMaxFound = true;
            }
            else if (clsCustomRank.iRank5Points != 0)
            {
                strMaxLevel = clsCustomRank.iRank5Points.ToString();
                if (iCurrentPoints.ToString() == strMaxLevel)
                {
                    strRank = clsCustomRank.strRank5Title;
                    lstRanks[0] = strRank;
                }
                bMaxFound = true;
            }
            else if (clsCustomRank.iRank4Points != 0)
            {
                strMaxLevel = clsCustomRank.iRank4Points.ToString();
                if (iCurrentPoints.ToString() == strMaxLevel)
                {
                    strRank = clsCustomRank.strRank4Title;
                    lstRanks[0] = strRank;
                }
                bMaxFound = true;
            }
            else if (clsCustomRank.iRank3Points != 0)
            {
                strMaxLevel = clsCustomRank.iRank3Points.ToString();
                if (iCurrentPoints.ToString() == strMaxLevel)
                {
                    strRank = clsCustomRank.strRank3Title;
                    lstRanks[0] = strRank;
                }
                bMaxFound = true;
            }
            else if (clsCustomRank.iRank2Points != 0)
            {
                strMaxLevel = clsCustomRank.iRank2Points.ToString();
                if (iCurrentPoints.ToString() == strMaxLevel)
                {
                    strRank = clsCustomRank.strRank2Title;
                    lstRanks[0] = strRank;
                }
                bMaxFound = true;
            }
        }

        lstRanks.Add(strMaxLevel);

        return lstRanks;

    }

    #endregion

    #region EMAIL METHODS
    
    protected void SendPhaseCompletionEmail()
    {
        clsGroups clsGroup = new clsGroups(clsAccountUsers.iGroupID);
        List<string> lstRanks = strCustomGroupRank(clsGroup.iCustomGroupRankID, clsAccountUsers.iCurrentPoints);
        string strRank = "";
        string strNextRank = "";
        int iCurrentRankPoints = 0;
        int iNextRankPoints = 0;
        int iCounter = 1;
        int iMaxPoints = 0;
        foreach (string strRankItem in lstRanks)
        {
            switch (iCounter)
            {
                case 1:
                    strRank = strRankItem;
                    break;
                case 2:
                    strNextRank = strRankItem;
                    break;
                case 3:
                    iCurrentRankPoints = Convert.ToInt32(strRankItem);
                    break;
                case 4:
                    iNextRankPoints = Convert.ToInt32(strRankItem);
                    break;
                case 5:
                    iMaxPoints = Convert.ToInt32(strRankItem);
                    break;
            }

            iCounter++;
        }

        StringBuilder strbEmailContent = new StringBuilder();
        int nextPhase = clsTracking.iCurrentPhaseLevel + 1;
        strbEmailContent.AppendLine("<!-- Start of textbanner -->");
        strbEmailContent.AppendLine("<table width='650' bgcolor='#FFF' cellpadding='30' cellspacing='0' border='0'  border='none' align='center' class='devicewidth' options=''>"); //style='border-left:1px solid #333; border-right:1px solid #333;'
        strbEmailContent.AppendLine("<tbody>");

        strbEmailContent.AppendLine("<!-- Content -->");
        strbEmailContent.AppendLine("<tr>");
        //strbEmailContent.AppendLine("<td width='60'></td>");
        strbEmailContent.AppendLine("<td valign='top' style='color:black; font-family:Verdana; font-size:12px; text-align:left;' text=''>");
        strbEmailContent.AppendLine("Hi " + clsAccountUsers.strFirstName.ToString() + "<br /><br />");

        strbEmailContent.AppendLine("Congratulations you have unlocked Phase: " + nextPhase.ToString() + ".<br /><br />");
        strbEmailContent.AppendLine("You have earned " + clsAccountUsers.iCurrentPoints.ToString() + " points and are getting closer to becoming a Deloitte Citizen. <br/><br/>");
        strbEmailContent.AppendLine("Got some questions? Feel free to contact our friendly support team on <a href='mailto:info@stratagc.co.za'>info@stratagc.co.za</a>, or call us on 084 2809 206. <br/><br/>");
        strbEmailContent.AppendLine("Learn, network and become a Deloitte Citizen.<br/><br/>");
        strbEmailContent.AppendLine("Regards <br/><br/>");
        strbEmailContent.AppendLine("--<br/>");
        strbEmailContent.AppendLine("<p style='font-family:Verdana; font-size:11px;'><b>The Onboarding Team</b></p>");
        strbEmailContent.AppendLine("<p style='font-family:Verdana; font-size:11px;'>D: +27 (0)84 280 9206</p>");
        strbEmailContent.AppendLine("<a style='font-family:Verdana; font-size:11px;' href='info@stratagc.co.za'>info@stratagc.co.za</a> | <a style='font-family:Verdana; font-size:11px;' href='www.deloitte.com'>www.deloitte.com</a><br/>");
        strbEmailContent.AppendLine("</td>");
        strbEmailContent.AppendLine("</tr>");
        strbEmailContent.AppendLine("<!-- End of Content -->");

        strbEmailContent.AppendLine("</tbody>");
        strbEmailContent.AppendLine("</table>");
        strbEmailContent.AppendLine("<!-- End of textbanner -->");

        Attachment[] emptyAttach = new Attachment[] { };

        try
        {
            PhaseCompletionEmailDeloitte.SendMail("noreply@deloitte.co.za", clsAccountUsers.strEmailAddress, "", "", "Completed Phase", strbEmailContent.ToString(), "", "", emptyAttach, true);
            //###TEST###
            //PhaseCompletionEmailDeloitte.SendMail("jamie@softservedigital.co.za", "jamie@softservedigital.co.za", "", "", "Completed Phase", strbEmailContent.ToString(), "", "", emptyAttach, true);
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
    }

    protected void SendMissionQuestionCertificate(String userAnswer)
    {

        StringBuilder strbEmailContent = new StringBuilder();

        strbEmailContent.AppendLine("<!-- Start of textbanner -->");
        strbEmailContent.AppendLine("<table width='650' bgcolor='#FFF' cellpadding='30' cellspacing='0' border='0'  border='none' align='center' class='devicewidth' options=''>"); //style='border-left:1px solid #333; border-right:1px solid #333;'
        strbEmailContent.AppendLine("<tbody>");

        strbEmailContent.AppendLine("<!-- Content -->");
        strbEmailContent.AppendLine("<tr>");
        //strbEmailContent.AppendLine("<td width='60'></td>");
        strbEmailContent.AppendLine("<td valign='top' style='color:black; font-family:Verdana; font-size:12px; text-align:left;' text=''>");

        strbEmailContent.AppendLine("Dear " + clsAccountUsers.strFirstName + "<br /><br />");
        strbEmailContent.AppendLine("Here is Your Mission Certificate! Congratulations!<br /><br />");
        strbEmailContent.AppendLine("Please see attached file for your certificate!<br /><br />");
        strbEmailContent.AppendLine("--<br/>");
        strbEmailContent.AppendLine("<p style='font-family:Verdana; font-size:11px;'><b>The Onboarding Team</b></p>");
        strbEmailContent.AppendLine("<p style='font-family:Verdana; font-size:11px;'>D: +27 (0)84 280 9206</p>");
        strbEmailContent.AppendLine("<a style='font-family:Verdana; font-size:11px;' href='info@stratagc.co.za'>info@stratagc.co.za</a> | <a style='font-family:Verdana; font-size:11px;' href='www.deloitte.com'>www.deloitte.com</a><br/>");
        strbEmailContent.AppendLine("</td>");
        strbEmailContent.AppendLine("</tr>");
        strbEmailContent.AppendLine("<!-- End of Content -->");

        strbEmailContent.AppendLine("</tbody>");
        strbEmailContent.AppendLine("</table>");
        strbEmailContent.AppendLine("<!-- End of textbanner -->");


        string strName = clsAccountUsers.strFirstName + " " + clsAccountUsers.strSurname;

        string strCertificate = File.ReadAllText(HttpContext.Current.Server.MapPath("~/templates/MissionCertificate.html"))
        .Replace("{AccountUserName}", strName.ToString())
        .Replace("{Answer}", userAnswer.ToString());

        string strQuoteTempPath = AppDomain.CurrentDomain.BaseDirectory + "\\PDF\\CompleteCertificate\\" + clsAccountUsers.iAccountUserID.ToString() + "-" + clsAccountUsers.strSurname + "-" + clsAccountUsers.strFirstName + ".pdf";

        //New
        using (FileStream fs = new FileStream(HttpContext.Current.Server.MapPath("~/templates/MissionCertificateTemp.html"), FileMode.Create))
        {
            using (StreamWriter w = new StreamWriter(fs, Encoding.UTF8))
            {
                w.WriteLine(strCertificate);
            }
        }
        //New

        var htmlContent = strCertificate;
        var htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter();
        var pdfBytes = htmlToPdf.GeneratePdfFromFile(HttpContext.Current.Server.MapPath("~/templates/MissionCertificateTemp.html"), null);

        using (FileStream fstream = new FileStream(strQuoteTempPath, FileMode.OpenOrCreate))
        {
            fstream.Write(pdfBytes, 0, pdfBytes.Length);
        }

        StringBuilder strContent = new StringBuilder();
        System.Net.Mail.Attachment[] attachments = new System.Net.Mail.Attachment[1];
        System.Net.Mail.Attachment attachment = new System.Net.Mail.Attachment(strQuoteTempPath);
        attachments[0] = attachment;

        try
        {
            emailComponentNew.SendMail("noreply@deloitte.co.za", clsAccountUsers.strEmailAddress, "", "", "Certificate", strbEmailContent.ToString(), "", "", attachments, true);
            //###TEST###
            //emailComponentNew.SendMail("jamie@softservedigital.co.za", "jamie@softservedigital.co.za", "", "", "Completed All Missions", strbEmailContent.ToString(), "", "", attachments, true);
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
    }

    protected void SendFinalEmail()
    {

        StringBuilder strbEmailContent = new StringBuilder();

        strbEmailContent.AppendLine("<!-- Start of textbanner -->");
        strbEmailContent.AppendLine("<table width='650' bgcolor='#FFF' cellpadding='30' cellspacing='0' border='0'  border='none' align='center' class='devicewidth' options=''>"); //style='border-left:1px solid #333; border-right:1px solid #333;'
        strbEmailContent.AppendLine("<tbody>");

        strbEmailContent.AppendLine("<!-- Content -->");
        strbEmailContent.AppendLine("<tr>");
        //strbEmailContent.AppendLine("<td width='60'></td>");
        strbEmailContent.AppendLine("<td valign='top' style='color:black; font-family:Verdana; font-size:12px; text-align:left;' text=''>");

        strbEmailContent.AppendLine("Dear " + clsAccountUsers.strFirstName + "<br /><br />");
        strbEmailContent.AppendLine("You have just completed all missions! Congratulations!<br /><br />");
        strbEmailContent.AppendLine("--<br/>");
        strbEmailContent.AppendLine("<p style='font-family:Verdana; font-size:11px;'><b>The Onboarding Team</b></p>");
        strbEmailContent.AppendLine("<p style='font-family:Verdana; font-size:11px;'>D: +27 (0)84 280 9206</p>");
        strbEmailContent.AppendLine("<a style='font-family:Verdana; font-size:11px;' href='info@stratagc.co.za'>info@stratagc.co.za</a> | <a style='font-family:Verdana; font-size:11px;' href='www.deloitte.com'>www.deloitte.com</a><br/>");
        strbEmailContent.AppendLine("</td>");
        strbEmailContent.AppendLine("</tr>");
        strbEmailContent.AppendLine("<!-- End of Content -->");

        strbEmailContent.AppendLine("</tbody>");
        strbEmailContent.AppendLine("</table>");
        strbEmailContent.AppendLine("<!-- End of textbanner -->");

        Attachment[] emptyAttach = new Attachment[] { };

        try
        {
            finalEmailDeloitte.SendMail("noreply@deloitte.co.za", clsAccountUsers.strEmailAddress, "", "", "Completed All Missions", strbEmailContent.ToString(), "", "", emptyAttach, true);
            //###TEST###
            //finalEmailDeloitte.SendMail("jamie@softservedigital.co.za", "jamie@softservedigital.co.za", "", "", "Completed All Missions", strbEmailContent.ToString(), "", "", emptyAttach, true);
        }
        catch (Exception ex)
        {
            ex.ToString();
        }

    }

    protected void SendMissionReport()
    {
        //string strEmailAddress;
        //if (clsCommonFunctions.DoesRecordExist("tblUsers", "strEmailAddress = '" + strEmailAddress + "' AND bIsDeleted = 0") == true)
        //### Settings
        string strReportName = "Q and A Report";
        //bool isThereUsersInThisGroup = false;
        StringBuilder strbMailBuilder = new StringBuilder();
        int iGroupID = clsAccountUsers.iGroupID;
        int iMissionID = Convert.ToInt32(Request.QueryString["iMissionID"]);
        string emailTo = "";
        string cc = "";

        //clsMissions clsMissions = new clsMissions(iMissionID);

        strbMailBuilder.AppendLine("<!-- Start of textbanner -->");
        strbMailBuilder.AppendLine("<table width='100%' bgcolor='#ffffff' cellpadding='0' cellspacing='0' border='0' id='backgroundTable' movable=''>");
        strbMailBuilder.AppendLine("<tbody>");
        strbMailBuilder.AppendLine("<tr>");
        strbMailBuilder.AppendLine("<td>");
        strbMailBuilder.AppendLine("<table bgcolor='#ffffff' width='650' cellpadding='0' cellspacing='0' border='0' align='center' class='devicewidth' options=''>");
        strbMailBuilder.AppendLine("<tbody>");
        //strbMailBuilder.AppendLine("<!-- Spacing -->");
        //strbMailBuilder.AppendLine("<tr>");
        //strbMailBuilder.AppendLine("<td height='50'></td>");
        //strbMailBuilder.AppendLine("</tr>");
        //strbMailBuilder.AppendLine("<!-- End of Spacing -->");
        strbMailBuilder.AppendLine("<tr>");
        strbMailBuilder.AppendLine("<td>");
        strbMailBuilder.AppendLine("<table width='100%' cellspacing='0' cellpadding='0'>");
        strbMailBuilder.AppendLine("<tbody>");

        strbMailBuilder.AppendLine("<!-- Content -->");
        strbMailBuilder.AppendLine("<tr>");
        strbMailBuilder.AppendLine("<td width='60'></td>");
        strbMailBuilder.AppendLine("<td valign='top' style='font-family: Helvetica, Arial, sans-serif;font-size: 14px; color: #919191; text-align:left;line-height: 22px;' text=''>");
        strbMailBuilder.AppendLine("Dear Admin<br /><br />");
        strbMailBuilder.AppendLine("Full Name: " + clsAccountUsers.strFirstName + " " + clsAccountUsers.strSurname + "<br />");
        strbMailBuilder.AppendLine("Email Address: " + clsAccountUsers.strEmailAddress + "<br/>");
        clsGroups clsGroups = new clsGroups(clsAccountUsers.iGroupID);
        strbMailBuilder.AppendLine("Group: " + clsGroups.strTitle + "<br/>");
        //strbMailBuilder.AppendLine("<span style='color: #1f497d !important;'>Group: " + clsGroups.strTitle + "</span><br/>");
        strbMailBuilder.AppendLine("<br/>");
        strbMailBuilder.AppendLine("<br/>");

        DataTable dtGroupPhases = clsGroupPhases.GetGroupPhasesList("iGroupID=" + iGroupID, "");
        int iPhaseID = 0;

        clsMissions clsMissions = new clsMissions(iMissionID);
        clsAdminEmailSettings clsAdminEmailSettings = new clsAdminEmailSettings(clsMissions.iAdminEmailSettingsID);
        int iMissionTypeID = clsMissions.iMissionTypeID;

        //strbMailBuilder.AppendLine("MISSION: " + clsMissions.strTitle + "<br />");
        strbMailBuilder.AppendLine("<b>MISSION: " + clsMissions.strTitle + "</b><br />");

        strbMailBuilder.AppendLine("" + clsMissions.strDescription + "<br/><br/>");
        strbMailBuilder.AppendLine("" + clsMissions.strTagLine + "<br/>");
        //strbMailBuilder.AppendLine("Points:" + clsMissions.iBonusPoints.ToString() + "<br/>");                        

        strbMailBuilder.AppendLine("<br/>");
        strbMailBuilder.AppendLine("<br/>");

        strbMailBuilder.AppendLine(getMissionQuestionAndAnswer(iMissionID, iMissionTypeID));


        strbMailBuilder.AppendLine("</td>");
        strbMailBuilder.AppendLine("<td width='60'></td>");
        strbMailBuilder.AppendLine("</tr>");
        strbMailBuilder.AppendLine("<!-- End of Content -->");

        strbMailBuilder.AppendLine("</td>");
        strbMailBuilder.AppendLine("<td width='20'></td>");
        strbMailBuilder.AppendLine("</tr>");
        strbMailBuilder.AppendLine("</tbody>");
        strbMailBuilder.AppendLine("</table>");
        strbMailBuilder.AppendLine("</td>");
        strbMailBuilder.AppendLine("</tr>");
        strbMailBuilder.AppendLine("<!-- Spacing -->");
        strbMailBuilder.AppendLine("<tr>");
        strbMailBuilder.AppendLine("<td height='50'></td>");
        strbMailBuilder.AppendLine("</tr>");
        strbMailBuilder.AppendLine("<!-- End of Spacing -->");
        strbMailBuilder.AppendLine("</tbody>");
        strbMailBuilder.AppendLine("</table>");
        strbMailBuilder.AppendLine("</td>");
        strbMailBuilder.AppendLine("</tr>");
        strbMailBuilder.AppendLine("</tbody>");
        strbMailBuilder.AppendLine("</table>");
        strbMailBuilder.AppendLine("<!-- End of textbanner -->");

        emailTo = clsAdminEmailSettings.strEmail1.ToString();
        cc = clsAdminEmailSettings.strEmail2.ToString();
        Attachment[] empty = new Attachment[] { };
        try
        {
            emailComponentNew.SendMail("noreply@deloitte.co.za", emailTo, cc, "", strReportName, strbMailBuilder.ToString(), "", "", empty, true);
            //notificationEmail.SendMail("noreply@softservedigital.co.za", "jamie@softservedigital.co.za", "", "", strReportName, strbMailBuilder.ToString(), "", "", empty, true);
            // notificationEmail.SendMail("no-reply@raonboarding.co.za", strEmailAddress, "", "andrew@softservedigital.co.za", strReportName, strbMailBuilder.ToString(), "", "", empty, true);
            //mandatoryDiv.Visible = false;

            //mandatoryDiv2.Visible = true;
            //lblValidationMessage2.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceHappy.png\" alt='' title=''/><div class=\"validationMessage\">The report has been sent</div></div>";
        }
        catch { }
        //### Redirect
    }

    protected string getMissionQuestionAndAnswer(int iCurrentMissionID, int iMissionTypeID)
    {
        string strMissionQuestionAndAnswer = "";
        DataTable dtMission = new DataTable();

        if (iMissionTypeID == 1)
        {
            dtMission = clsMissionQuestions.GetMissionQuestionsList("iMissionID=" + iCurrentMissionID, "");

            StringBuilder StrBQuestion = new StringBuilder();

            string strQuestion;
            int iMissionQuestionID;

            int iCount = 0;

            foreach (DataRow dtrAccountMember in dtMission.Rows)
            {
                ++iCount;

                iMissionQuestionID = Convert.ToInt32(dtrAccountMember["iMissionQuestionID"].ToString());
                strQuestion = dtrAccountMember["strQuestion"].ToString();

                StrBQuestion.AppendLine(iCount + ") " + strQuestion + ":");
                StrBQuestion.AppendLine("<br />");

                //int iCurrentUserAnswerID = clsCommonFunctions.returnRecordID("iUserAnswerID", "tblUserAnswers", "iMissionQuestionID=" + iMissionQuestionID + " AND bIsDeleted=0"); //" AND iAccountUserID=" + clsAccountUsers.iAccountUserID + 
                int iCurrentUserAnswerID = clsCommonFunctions.returnRecordID("iUserAnswerID", "tblUserAnswers", "iMissionQuestionID=" + iMissionQuestionID + " AND iAccountUserID=" + clsAccountUsers.iAccountUserID + " AND bIsDeleted=0");

                if (iCurrentUserAnswerID != 0)
                {
                    clsUserAnswers clsUserAnswers = new clsUserAnswers(iCurrentUserAnswerID);
                    //StrBQuestion.AppendLine("Answer: ");
                    StrBQuestion.AppendLine("<b>Answer:</b> ");
                    //StrBQuestion.AppendLine("<br />");
                    StrBQuestion.AppendLine(clsUserAnswers.strAnswer);
                    StrBQuestion.AppendLine("<br />");
                    StrBQuestion.AppendLine("<br />");
                }

                strMissionQuestionAndAnswer = StrBQuestion.ToString();
            }
        }
        else if (iMissionTypeID == 2)
        {
            //### TEAMSHIP
            dtMission = clsTeamShips.GetTeamShipsList("iMissionID=" + iCurrentMissionID, "");

            StringBuilder StrBQuestion = new StringBuilder();

            string strName;
            string strTitle;
            string strFullPathToImages;
            int iCount = 0;

            foreach (DataRow dtrAccountMember in dtMission.Rows)
            {
                ++iCount;

                int iTeamshipID = Convert.ToInt32(dtrAccountMember["iTeamShipID"].ToString());
                strName = dtrAccountMember["strName"].ToString();
                strTitle = dtrAccountMember["strTitle"].ToString();

                clsTeamShips clsCurrentTeamShips = new clsTeamShips(iTeamshipID);

                if ((clsCurrentTeamShips.strMasterImage == "") || (clsCurrentTeamShips.strMasterImage == null))
                {
                    strFullPathToImages = "images/imgNoImage.png";
                }
                else
                {
                    strFullPathToImages = AppDomain.CurrentDomain.BaseDirectory + "/TeamShips/" + clsCurrentTeamShips.strPathToImages + "/" + Path.GetFileNameWithoutExtension(clsCurrentTeamShips.strMasterImage) + "" + Path.GetExtension(clsCurrentTeamShips.strMasterImage);
                }

                StrBQuestion.AppendLine(iCount + ")  Full Name: " + strName);
                StrBQuestion.AppendLine("<br />");
                StrBQuestion.AppendLine("Job Title: " + strTitle);
                StrBQuestion.AppendLine("<br />");
                StrBQuestion.AppendLine("<img alt='' src='" + strFullPathToImages + "' />");
                StrBQuestion.AppendLine("<br />");
                StrBQuestion.AppendLine("<br />");

                int iCurrentUserAnswerID = clsCommonFunctions.returnRecordID("iUserAnswerID", "tblUserAnswers", "iTeamshipID=" + iTeamshipID + " AND bIsDeleted=0"); //" AND iAccountUserID=" + clsAccountUsers.iAccountUserID + 

                if (iCurrentUserAnswerID != 0)
                {
                    clsUserAnswers clsUserAnswers = new clsUserAnswers(iCurrentUserAnswerID);
                    StrBQuestion.AppendLine(clsUserAnswers.strAnswer);
                }

                strMissionQuestionAndAnswer = StrBQuestion.ToString();
            }

        }
        return strMissionQuestionAndAnswer;
    }
    #endregion

    #region VIDEO METHODS

    List<string> lstVideo;
    List<string> lstVideoFileNames;
    string strUniqueFullPath = AppDomain.CurrentDomain.BaseDirectory + "\\MissionVideos";

    public string getList(String strPathToFolder)
    {
        lstVideo = new List<string>();
        lstVideoFileNames = new List<string>();

        string strPath = strPathToFolder;

        string[] files = Directory.GetFiles(strUniqueFullPath + "\\" + strPath);

        string iMissionVideoID = "";
        if (!string.IsNullOrEmpty(Request.QueryString["iMissionVideoID"]))
            iMissionVideoID = Request.QueryString["iMissionVideoID"];

        string strVideoFile = "";
        foreach (string strName in files)
        {

            string strHTMLVideo = strName.Replace(AppDomain.CurrentDomain.BaseDirectory + "\\", "..\\");
            strHTMLVideo = strHTMLVideo.Replace("\\", "/");
            strVideoFile = strHTMLVideo;
            break;
        }

        return strVideoFile;
    }

    #endregion

    protected void rpMisionQuestions_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            int iCurrentMissionID = Convert.ToInt32(Request.QueryString["iMissionID"]);
            try
            {
                DataTable dtMission = clsMissionQuestions.GetMissionQuestionsList("iMissionID=" + iCurrentMissionID, "");
                foreach (DataRow dtrAccountMember in dtMission.Rows)
                {
                    if (Convert.ToBoolean(dtrAccountMember["bIsCertificateQuestion"]) == true)
                    {
                        TextBox textBox = (TextBox)e.Item.FindControl("txtQuestionAnswer");
                        textBox.MaxLength = 70;
                    }
                }
            }
            catch (Exception ex) { }
        }
    }

    //#region SHOWCASEPOPULATION

    //protected void checkCompletedPhasesUserTracking()
    //{
    //    clsPhases clsPhase1 = new clsPhases(clsAccountUserTracking.iPhase1ID);
    //    litPhase1Text.Text = clsPhase1.strTitle;
    //    clsPhases clsPhase2 = new clsPhases(clsAccountUserTracking.iPhase2ID);
    //    litPhase2Text.Text = clsPhase2.strTitle;
    //    clsPhases clsPhase3 = new clsPhases(clsAccountUserTracking.iPhase3ID);
    //    litPhase3Text.Text = clsPhase3.strTitle;

    //    int iCurrentPhaseLevel = clsAccountUserTracking.iCurrentPhaseLevel;

    //    if (iCurrentPhaseLevel == 1)
    //    {
    //        popUserPhaseAchievements(1);
    //        btnPhase2.Text = "<div class='showcaseSection Grey'><span class='hidden-laptop'>Phase</span> 2: " + clsPhase2.strTitle + "</div>";
    //        btnPhase3.Text = "<div class='showcaseSection Grey'><span class='hidden-laptop'>Phase</span> 3: " + clsPhase3.strTitle + "</div>";
    //    }
    //    else if (iCurrentPhaseLevel == 2)
    //    {
    //        btnPhase2.Enabled = true;
    //        btnPhase2.Text = "<div class='showcaseSection Blue2'><span class='hidden-laptop'>Phase</span> 2: " + clsPhase2.strTitle + "</div>";
    //        btnPhase3.Text = "<div class='showcaseSection Grey'><span class='hidden-laptop'>Phase</span> 3: " + clsPhase3.strTitle + "</div>";
    //        popUserPhaseAchievements(2);
    //    }
    //    else if (iCurrentPhaseLevel == 3)
    //    {
    //        btnPhase2.Enabled = true;
    //        btnPhase3.Enabled = true;
    //        btnPhase2.Text = "<div class='showcaseSection Blue2'><span class='hidden-laptop'>Phase</span> 2: " + clsPhase2.strTitle + "</div>";
    //        btnPhase3.Text = "<div class='showcaseSection Blue3'><span class='hidden-laptop'>Phase</span> 3: " + clsPhase3.strTitle + "</div>";
    //        popUserPhaseAchievements(3);
    //    }
    //}

    //protected void popUserPhaseAchievements(int iPhaseLevel)
    //{
    //    DataTable dtUserAchievements = clsUserAchievements.GetUserAchievementsRecords(clsAccountUsers.iAccountUserID);

    //    DataTable dtUserDisplayAchievements = new DataTable();
    //    dtUserDisplayAchievements.Clear();
    //    dtUserDisplayAchievements.Columns.Add("iMissionID");
    //    dtUserDisplayAchievements.Columns.Add("strLink");
    //    dtUserDisplayAchievements.Columns.Add("strTitle");
    //    dtUserDisplayAchievements.Columns.Add("strNewRow");
    //    dtUserDisplayAchievements.Columns.Add("FullPathForImage");

    //    List<int> lstAchievements = new List<int>();
    //    List<int> lstMissions = new List<int>();
    //    //T
    //    List<int> lstNextMission = new List<int>();

    //    int iCount = 1;
    //    int iAchieveTotalCount = 0;

    //    if (iPhaseLevel == 1)
    //    {
    //        foreach (DataRow dr in dtPhase1Missions.Rows)
    //        {
    //            lstMissions.Add(Convert.ToInt32(dr["iMissionID"]));
    //        }
    //        foreach (DataRow dr in dtUserAchievements.Rows)
    //        {
    //            lstAchievements.Add(Convert.ToInt32(dr["iAchievementID"]));
    //        }
    //        //T
    //        lstNextMission = lstMissions;

    //        foreach (int iMissionIDNum in lstMissions)
    //        {
    //            clsMissions currentMission = new clsMissions(iMissionIDNum);
    //            clsAchievements currentAchievements = new clsAchievements(currentMission.iAchievementID);
    //            DataRow drUserDisplayAchievements = dtUserDisplayAchievements.NewRow();

    //            drUserDisplayAchievements["iMissionID"] = iMissionIDNum;
    //            drUserDisplayAchievements["strTitle"] = currentAchievements.strTitle;

    //            if ((lstAchievements.Contains(currentMission.iAchievementID)) && (currentMission.iAchievementID == 1))
    //            {
    //                drUserDisplayAchievements["FullPathForImage"] = "Achievements/" + currentAchievements.strPathToImages + "/" + Path.GetFileNameWithoutExtension(currentAchievements.strMasterImage) + "" + Path.GetExtension(currentAchievements.strMasterImage);
    //                drUserDisplayAchievements["strLink"] = "#";
    //                dtUserDisplayAchievements.Rows.Add(drUserDisplayAchievements);
    //            }
    //            else if (lstAchievements.Contains(currentMission.iAchievementID))
    //            {
    //                drUserDisplayAchievements["FullPathForImage"] = "Achievements/" + currentAchievements.strPathToImages + "/" + Path.GetFileNameWithoutExtension(currentAchievements.strMasterImage) + "" + Path.GetExtension(currentAchievements.strMasterImage);
    //                drUserDisplayAchievements["strLink"] = "Accelerate-Current-Mission.aspx?iMissionID=" + iMissionIDNum;
    //                dtUserDisplayAchievements.Rows.Add(drUserDisplayAchievements);
    //            }
    //            else
    //            {
    //                drUserDisplayAchievements["FullPathForImage"] = "img/Lock.png";
    //                drUserDisplayAchievements["strLink"] = "Accelerate-Current-Mission.aspx?iMissionID=" + iMissionIDNum;
    //                dtUserDisplayAchievements.Rows.Add(drUserDisplayAchievements);

    //                //### SET THE NEXT MISSION
    //                if (iCount == 1)
    //                {
    //                    ++iCount;
    //                    iNextMissionID = iMissionIDNum;
    //                }
    //            }

    //            ++iAchieveTotalCount;

    //            if (iAchieveTotalCount == 1)
    //                drUserDisplayAchievements["strNewRow"] = "<div class='row-fluid'>";
    //            else if (iAchieveTotalCount == 9)
    //                drUserDisplayAchievements["strNewRow"] = "</div><div class='row-fluid'>";
    //            else
    //                drUserDisplayAchievements["strNewRow"] = "";

    //            drUserDisplayAchievements["iMissionID"] = iMissionIDNum;
    //            drUserDisplayAchievements["strTitle"] = currentAchievements.strTitle;
    //        }
    //    }
    //    else if (iPhaseLevel == 2)
    //    {
    //        foreach (DataRow dr in dtPhase2Missions.Rows)
    //        {
    //            lstMissions.Add(Convert.ToInt32(dr["iMissionID"]));
    //        }
    //        foreach (DataRow dr in dtUserAchievements.Rows)
    //        {
    //            lstAchievements.Add(Convert.ToInt32(dr["iAchievementID"]));
    //        }

    //        foreach (int iMissionIDNum in lstMissions)
    //        {
    //            clsMissions currentMission = new clsMissions(iMissionIDNum);
    //            clsAchievements currentAchievements = new clsAchievements(currentMission.iAchievementID);
    //            DataRow drUserDisplayAchievements = dtUserDisplayAchievements.NewRow();

    //            drUserDisplayAchievements["iMissionID"] = iMissionIDNum;
    //            drUserDisplayAchievements["strTitle"] = currentAchievements.strTitle;

    //            if ((lstAchievements.Contains(currentMission.iAchievementID)) && (currentMission.iAchievementID == 1))
    //            {
    //                drUserDisplayAchievements["FullPathForImage"] = "Achievements/" + currentAchievements.strPathToImages + "/" + Path.GetFileNameWithoutExtension(currentAchievements.strMasterImage) + "" + Path.GetExtension(currentAchievements.strMasterImage);
    //                drUserDisplayAchievements["strLink"] = "#";
    //                dtUserDisplayAchievements.Rows.Add(drUserDisplayAchievements);
    //            }
    //            else if (lstAchievements.Contains(currentMission.iAchievementID))
    //            {
    //                drUserDisplayAchievements["FullPathForImage"] = "Achievements/" + currentAchievements.strPathToImages + "/" + Path.GetFileNameWithoutExtension(currentAchievements.strMasterImage) + "" + Path.GetExtension(currentAchievements.strMasterImage);
    //                drUserDisplayAchievements["strLink"] = "Accelerate-Current-Mission.aspx?iMissionID=" + iMissionIDNum;
    //                dtUserDisplayAchievements.Rows.Add(drUserDisplayAchievements);
    //            }
    //            else
    //            {
    //                drUserDisplayAchievements["FullPathForImage"] = "img/Lock.png";
    //                drUserDisplayAchievements["strLink"] = "Accelerate-Current-Mission.aspx?iMissionID=" + iMissionIDNum;
    //                dtUserDisplayAchievements.Rows.Add(drUserDisplayAchievements);

    //                //### SET THE NEXT MISSION
    //                if (iCount == 1)
    //                {
    //                    ++iCount;
    //                    iNextMissionID = iMissionIDNum;
    //                }
    //            }

    //            ++iAchieveTotalCount;

    //            if (iAchieveTotalCount == 1)
    //                drUserDisplayAchievements["strNewRow"] = "<div class='row-fluid'>";
    //            else if (iAchieveTotalCount == 9)
    //                drUserDisplayAchievements["strNewRow"] = "</div><div class='row-fluid'>";
    //            else
    //                drUserDisplayAchievements["strNewRow"] = "";

    //            drUserDisplayAchievements["iMissionID"] = iMissionIDNum;
    //            drUserDisplayAchievements["strTitle"] = currentAchievements.strTitle;
    //        }
    //    }
    //    else if (iPhaseLevel == 3)
    //    {
    //        foreach (DataRow dr in dtPhase3Missions.Rows)
    //        {
    //            lstMissions.Add(Convert.ToInt32(dr["iMissionID"]));
    //        }
    //        foreach (DataRow dr in dtUserAchievements.Rows)
    //        {
    //            lstAchievements.Add(Convert.ToInt32(dr["iAchievementID"]));
    //        }

    //        foreach (int iMissionIDNum in lstMissions)
    //        {
    //            clsMissions currentMission = new clsMissions(iMissionIDNum);
    //            clsAchievements currentAchievements = new clsAchievements(currentMission.iAchievementID);
    //            DataRow drUserDisplayAchievements = dtUserDisplayAchievements.NewRow();

    //            drUserDisplayAchievements["iMissionID"] = iMissionIDNum;
    //            drUserDisplayAchievements["strTitle"] = currentAchievements.strTitle;

    //            if ((lstAchievements.Contains(currentMission.iAchievementID)) && (currentMission.iAchievementID == 1))
    //            {
    //                drUserDisplayAchievements["FullPathForImage"] = "Achievements/" + currentAchievements.strPathToImages + "/" + Path.GetFileNameWithoutExtension(currentAchievements.strMasterImage) + "" + Path.GetExtension(currentAchievements.strMasterImage);
    //                drUserDisplayAchievements["strLink"] = "#";
    //                dtUserDisplayAchievements.Rows.Add(drUserDisplayAchievements);
    //            }
    //            else if (lstAchievements.Contains(currentMission.iAchievementID))
    //            {
    //                drUserDisplayAchievements["FullPathForImage"] = "Achievements/" + currentAchievements.strPathToImages + "/" + Path.GetFileNameWithoutExtension(currentAchievements.strMasterImage) + "" + Path.GetExtension(currentAchievements.strMasterImage);
    //                drUserDisplayAchievements["strLink"] = "Accelerate-Current-Mission.aspx?iMissionID=" + iMissionIDNum;
    //                dtUserDisplayAchievements.Rows.Add(drUserDisplayAchievements);
    //            }
    //            else
    //            {
    //                drUserDisplayAchievements["FullPathForImage"] = "img/Lock.png";
    //                drUserDisplayAchievements["strLink"] = "Accelerate-Current-Mission.aspx?iMissionID=" + iMissionIDNum;
    //                dtUserDisplayAchievements.Rows.Add(drUserDisplayAchievements);

    //                //### SET THE NEXT MISSION
    //                if (iCount == 1)
    //                {
    //                    ++iCount;
    //                    iNextMissionID = iMissionIDNum;
    //                }
    //            }

    //            ++iAchieveTotalCount;

    //            if (iAchieveTotalCount == 1)
    //                drUserDisplayAchievements["strNewRow"] = "<div class='row-fluid'>";
    //            else if (iAchieveTotalCount == 9)
    //                drUserDisplayAchievements["strNewRow"] = "</div><div class='row-fluid'>";
    //            else
    //                drUserDisplayAchievements["strNewRow"] = "";

    //            drUserDisplayAchievements["iMissionID"] = iMissionIDNum;
    //            drUserDisplayAchievements["strTitle"] = currentAchievements.strTitle;
    //        }
    //    }

    //    rpAchievements.DataSource = dtUserDisplayAchievements;
    //    rpAchievements.DataBind();
    //}

    //protected void btnPhase1_Click(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        popUserPhaseAchievements(1);
    //    }
    //    catch (Exception ex)
    //    {
    //        Response.Redirect("Accelerate-Error404.aspx");
    //    }
    //}

    //protected void btnPhase2_Click(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        popUserPhaseAchievements(2);
    //    }
    //    catch (Exception ex)
    //    {
    //        Response.Redirect("Accelerate-Error404.aspx");
    //    }
    //}

    //protected void btnPhase3_Click(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        popUserPhaseAchievements(3);
    //    }
    //    catch (Exception ex)
    //    {
    //        Response.Redirect("Accelerate-Error404.aspx");
    //    }
    //}

    //#endregion
}

