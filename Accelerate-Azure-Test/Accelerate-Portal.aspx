﻿<%@ Page Title="Accelerate - Portal" Language="C#" MasterPageFile="~/Accelerate.master" AutoEventWireup="true" Inherits="Accelerate_Portal" Codebehind="Accelerate-Portal.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .rightMenuContainer {
            position: relative;
        }

        @media (max-width: 1024px) {
            .span3 {
                float: left;
                width: 30%;
            }

            .divKnowledgeBaseBottom {
                position: relative;
                top: -20px;
                margin-bottom: 260px;
                left: 92px;
            }

            .divKnowledgeBaseTop {
                width: 837px;
                float: left;
                left: 92px;
                position: relative;
            }
        }

        @media (max-width: 769px) {
            .divKnowledgeBaseBottom {
                position: relative;
                top: 0px;
                margin-bottom: 260px;
            }

            .divKnowledgeBaseTop {
                position: relative;
                left: -35px;
            }

            .span3 {
                width: 33% !important;
                float: left !important;
            }
        }

        @media (max-width: 738px) {
            .divKnowledgeBaseBottom {
                position: relative;
                top: -10px;
                left: 90px;
                margin-bottom: 260px;
            }

            .divKnowledgeBaseTop {
                position: relative;
                left: -65px;
            }
        }


        @media (max-width: 668px) {
            .divKnowledgeBaseBottom {
                position: relative;
                top: 0px;
                left: 89px;
                margin-bottom: 0px;
            }

            .divKnowledgeBaseTop {
                position: relative;
                left: -81px;
            }
        }

        @media (max-width: 568px) {
            .divKnowledgeBaseBottom {
                position: relative;
                top: 0px;
                left: 87px;
                margin-bottom: 0px;
            }

            .divKnowledgeBaseTop {
                position: relative;
                left: -144px;
            }
        }

        @media (max-width: 481px) {
            .divKnowledgeBaseBottom {
                position: relative;
                top: 0px;
                left: 92px;
                margin-bottom: 0px;
            }

            .divKnowledgeBaseTop {
                position: relative;
                left: -200px;
            }
        }

        @media (max-width: 415px) {
            .divKnowledgeBaseBottom {
                position: relative;
                top: 0px;
                left: 93px;
                margin-bottom: 0px;
            }

            .divKnowledgeBaseTop {
                position: relative;
                left: -230px;
            }
        }



        @media (max-width: 376px) {
            .divKnowledgeBaseBottom {
                position: relative;
                top: 0px;
                left: 92px;
                margin-bottom: 0px;
            }

            .divKnowledgeBaseTop {
                position: relative;
                left: -246px;
            }
        }

        @media (max-width: 321px) {
            .divKnowledgeBaseBottom {
                position: relative;
                top: 0px;
                left: 90px;
                margin-bottom: 0px;
            }

            .divKnowledgeBaseTop {
                position: relative;
                left: -262px;
            }
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="infobox">
        <div class="top-bar-alternative">
            <h3>Knowledge Base</h3>
        </div>
        <div class="infobox" style="color: #000;">
            <asp:Literal runat="server" ID="litPortalFeatures"></asp:Literal>
        </div>
        <%--<div class="infobox">
            <div class="top-bar">
                <h3>Business Units</h3>
            </div>
            <div class="well" style="color: #000;">
                <div style="margin-bottom: 20px;">
                    The member firms of Deloitte offer clients a broad range of Audit, Business-Process-as-a-Service (BPaaS), Consulting, Financial Advisory, and Tax services. 
                    Deloitte SA under the leadership of Lead Client Service Partners and Lead Client Service Directors help create powerful business solutions 
                    for organizations operating anywhere in the world.
                <br />
                </div>
                <div style="margin-bottom: 20px;">
                    <b class="media-heading">Assurance</b>
                    <br />
                    Best practices, guidance, tools and solutions to support the delivery of a quality audit service in providing the standard of excellence to our clients.
                    <br />
                </div>
                <div style="margin-bottom: 20px;">
                    <b class="media-heading">BPaaS (Business-Process-as-a-Service)</b>
                    <br />
                    Deloitte’s BPaaS provides clients with a unique and comprehensive approach to assess and deliver operational effectiveness through a 
                    solution that is enabled by technology, giving clients the freedom to grow their business.
                    <br />
                </div>
                <div style="margin-bottom: 20px;">
                    <b class="media-heading">Consulting</b>
                    <br />
                    The powerful teams at Deloitte Consulting Africa partner with clients to unlock opportunities, adding advantage at every level in order to change the game.
                    <br />
                </div>
                <div style="margin-bottom: 20px;">
                    <b class="media-heading">Risk Advisory </b>
                    <br />
                    Combining the skills of the experts in Internal Audit, IT Audit, Forensics, Governance and Risk Management, Technology Risk Advisory, Tip-offs Anonymous, Data Analytics.
                    <br />
                </div>
                <div style="margin-bottom: 20px;">
                    <b class="media-heading">Tax</b>
                    <br />
                    Our team of highly specialised lawyers and accountants, who comprise the Tax and Legal practice of Deloitte in South Africa, provide our 
                    clients with access to a synergistic combination of technical skills.
                    <br />
                </div>
                <div style="margin-bottom: 20px;">
                    <b class="media-heading">Corporate Finance </b>
                    <br />
                    Deloitte South Africa’s Corporate Finance division consists of Due Diligence, Valuations, Mergers and Acquisition Advisory, Sponsor Services, Debt Advisory, 
                    Private Equity , and operates as a national practice from its offices in Woodmead, Johannesburg and Brooklyn, Pretoria.
                    <br />
                </div>
            </div>
            <div class="infobox">
                <a href="https://za.deloitteresources.com/research/SiteAssets/Pages/africa-organograms/Deloitte%20Africa%20Organisational%20Structure%20February%202016.pdf" target="_blank">
                    <div class="well" style="background: #00aae7; color: #FFF; font-size: 20px;">
                        DELOITTE AFRICA ORGANISATIONAL STRUCTURE
                    </div>
                </a>
            </div>
        </div>--%>
    </div>
</asp:Content>
