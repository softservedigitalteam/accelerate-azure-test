﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data.SqlClient;
using System.Net.Mail;

public partial class CMS_frmCMSLogin : System.Web.UI.Page
{
    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        txtUsername.Focus();

        //### Logout User
        if (Request.QueryString["action"] == "logout")
        {
            //### Clear all session variables
            Session.Clear();
        }
    }

    protected void lnkbtnForgottenPassword_Click(object sender, EventArgs e)
    {
        lnkbtnForgottenPassword.Visible = false;
        lnkbtnBackToLogin.Visible = true;
        divPassword.Visible = false;
        lnkbtnLogin.Visible = false;
        lnkbtnSubmit.Visible = true;
    }

    protected void lnkbtnBackToLogin_Click(object sender, EventArgs e)
    {
        lnkbtnForgottenPassword.Visible = true;
        lnkbtnBackToLogin.Visible = false;
        divPassword.Visible = true;
        lnkbtnLogin.Visible = true;
        lnkbtnSubmit.Visible = false;
    }

    protected void lnkbtnLogin_Click(object sender, EventArgs e)
    {
        try
        {
            clsUsers clsUsers = new clsUsers(txtUsername.Text, clsCommonFunctions.GetMd5Sum(txtPassword.Text));

            Session["clsUsers"] = clsUsers;
            
            Response.Redirect("CMS/frmLandingZone.aspx");
        }
        catch
        {
            litValidationMessage.Text = "<div class=\"validationImageIncorrectLogin\"></div><div style=\"position:relative; top:16px; left:5px; font-size:13px\">Invalid Login Credentials.</div>";

            if ((txtPassword.Text == "") || (txtPassword.Text == null))
            {
                litValidationMessage.Text = "<div class=\"validationImageIncorrectLogin\"></div><div class=\"validationLabel\">Login requires a Password.</div>";
                divPasswordLeft.Attributes["class"] = "roundedTextboxLeftSideRed";
                divPasswordTxt.Attributes["class"] = "roundedTextboxBackgroundDivRed";
                divPasswordRight.Attributes["class"] = "roundedTextboxRightSideRed";
            }
            else if ((txtUsername.Text == "") || (txtUsername.Text == null))
            {
                litValidationMessage.Text = "<div class=\"validationImageIncorrectLogin\"></div><div class=\"validationLabel\">Login requires an Email.</div>";
                divUsernameLeft.Attributes["class"] = "roundedTextboxLeftSideRed";
                divUsernameTxt.Attributes["class"] = "roundedTextboxBackgroundDivRed";
                divUsernameRight.Attributes["class"] = "roundedTextboxRightSideRed";
            }

            if ((txtUsername.Text == "") && (txtPassword.Text == ""))
            {
                litValidationMessage.Text = "<div class=\"validationImageIncorrectLogin\"></div><div class=\"validationLabel\">Login requires an Email and Password.</div>";
                divUsernameLeft.Attributes["class"] = "roundedTextboxLeftSideRed";
                divUsernameTxt.Attributes["class"] = "roundedTextboxBackgroundDivRed";
                divUsernameRight.Attributes["class"] = "roundedTextboxRightSideRed";

                divPasswordLeft.Attributes["class"] = "roundedTextboxLeftSideRed";
                divPasswordTxt.Attributes["class"] = "roundedTextboxBackgroundDivRed";
                divPasswordRight.Attributes["class"] = "roundedTextboxRightSideRed";
            }
        }
    }

    protected void lnkbtnSubmit_Click(object sender, EventArgs e)
    {
        //### Validate email
        if (txtUsername.Text == "")
        {
            litValidationMessage.Text = "<div class=\"validationImageIncorrectLogin\"></div><div class=\"validationLabel\">Please enter your email address.</div>";
        }
        else
        {
            //### Check if email address exists
            if (clsCommonFunctions.DoesRecordExist("tblUsers", "strEmailAddress = '" + txtUsername.Text + "' AND bIsDeleted = 0") == true)
            {
                //### Change password & Send mail
                string strRandomPassword = "";
                string strPassword = "";

                strRandomPassword = clsCommonFunctions.strCreateRandomPassword(8);
                strPassword = strRandomPassword;

                //### Hash random password
                strRandomPassword = clsCommonFunctions.GetMd5Sum(strRandomPassword);

                ForgottenPassword(txtUsername.Text, strRandomPassword);

                StringBuilder strbMailBuilder = new StringBuilder();

                strbMailBuilder.AppendLine("Dear User<br /><br />");
                strbMailBuilder.AppendLine("Your password has been reset.<br /><br />");
                strbMailBuilder.AppendLine("Your temporary password is shown below, please log into your account and change your password to something more suitable and easier to remember:<br /><br />");
                strbMailBuilder.AppendLine("Password: " + strPassword + "<br/>");

                string strContent = strbMailBuilder.ToString();

                //DataConnection.GetDataObject().ExecuteScalar("spForgottenPassword '" + txtUsername.Text + "', '" + strRandomPassword + "'");
                //emailComponent.SendMail("noreply@gobundu.co.za", txtUsername.Text, "", "", "Forgotten Password", strContent, true);

                Attachment[] empty = new Attachment[] { };

                try
                {
                    emailComponent.SendMail("andrew@softservedigital.co.za", txtUsername.Text, "", "", "CMS Forgotten Password", strContent, "Heading", "Subheading", empty, true);
                }
                catch { }

                //### Redirect
                litValidationMessage.Text = "<div class=\"validationImageCorrectLogin\"></div><div class=\"validationLabel\">Your temporary password has been sent.</div>";
            }
            else
            {
                //### If no email address found
                litValidationMessage.Text = "<div class=\"validationImageIncorrectLogin\"></div><div class=\"validationLabel\">The email you have entered is not a registered <br />email address.</div>";
            }
        }
    }

    #endregion

    #region PASSWORD METHODS

    public static void ForgottenPassword(string strEmailAddress, string strRandomPassword)
    {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
            {
                new SqlParameter("@strEmailAddress", strEmailAddress),
                new SqlParameter("@strRandomPassword", strRandomPassword)
            };
        //### Executes Password Reset sp
        clsDataAccess.Execute("spForgottenPassword", sqlParameter);
    }

    #endregion
}