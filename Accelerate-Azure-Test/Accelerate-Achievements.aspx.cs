﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Accelerate_Achievements : System.Web.UI.Page
{
    private clsAccountUsers clsAccountUsers;

    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Logout User
        if (Request.QueryString["action"] == "syslogout")
        {
            //### Clear all seesion variables
            Session.Clear();
        }

        //### Check if session clsUser exists
        if (Session["clsAccountUsers"] == null)
        {
            //### Redirect back to login
            Response.Redirect("Accelerate-Login.aspx");
        }
        clsAccountUsers = (clsAccountUsers)Session["clsAccountUsers"];

        try
        {
            string strAchievementsList = popAvailablePhasesMissionsAchievements();
            DataTable dtAchievementsToDisplay = getAchievementsToDisplayDT(strAchievementsList);
            //DataTable dtAchievementsToDisplay = getAchievementsToDisplayDT(hdfAvailableAchiements.Value.ToString());
            //DataTable dtAllPossibleBadgesList = getDtAllPossibleAchievements(clsAccountUsers.iAccountUserID, dtAchievementsToDisplay);
            string strCompletedBadgeIDList = popCompletedAchievements(clsAccountUsers.iAccountUserID);

            //popAllAchievements(strCompletedBadgeIDList, dtAllPossibleBadgesList, clsAccountUsers.iAccountUserID);
            popAllAchievements(strCompletedBadgeIDList, dtAchievementsToDisplay, clsAccountUsers.iAccountUserID);

            //string strBadgeIdList = popAchievements(clsAccountUsers.iAccountUserID);
            //DataTable dtAllPossibleBadgesList = getDtAllPossibleAchievements(clsAccountUsers.iAccountUserID);
            //popAllAchievements(strBadgeIdList, dtAllPossibleBadgesList, clsAccountUsers.iAccountUserID);

            //TEMP REMOVE
            //popAchievementStages();

            litPoints.Text = clsAccountUsers.iCurrentPoints.ToString();
        }
        catch(Exception ex)
        {
            Response.Redirect("Accelerate-Error404.aspx");
        }
        
    }

    protected string strCurrentRank(int iCurrentPoints)
    {
        DataTable dtRank = clsRanks.GetRanksList("", "");

        int iRankID = 1;
        int iPointLevel;
        string strCurrentRank = "Noob";

        int iCount = 0;

        foreach (DataRow dtrRank in dtRank.Rows)
        {
            ++iCount;

            iRankID = Convert.ToInt32(dtrRank["iRankID"].ToString());
            iPointLevel = Convert.ToInt32(dtrRank["iPointLevel"].ToString());
            strCurrentRank = dtrRank["strTitle"].ToString();

            if (iCurrentPoints < iPointLevel)
            {
                break;
            }
        }

        return strCurrentRank;
    }

    //### Returns Current Rank ID
    protected int iRankID(int iCurrentPoints)
    {
        DataTable dtRank = clsRanks.GetRanksList("", "");

        int iRankID = 1;
        int iPointLevel;

        int iCount = 0;

        foreach (DataRow dtrRank in dtRank.Rows)
        {
            ++iCount;

            iRankID = Convert.ToInt32(dtrRank["iRankID"].ToString());
            iPointLevel = Convert.ToInt32(dtrRank["iPointLevel"].ToString());

            if (iCurrentPoints < iPointLevel)
            {
                break;
            }
        }

        return iRankID;
    }

    protected int iRankCount()
    {
        DataTable dtRank = clsRanks.GetRanksList();

        int iRowCount = dtRank.Rows.Count;

        return iRowCount;
    }

    //### Get the Missions from that Phase
    protected DataTable getAllMissionsForCurrentUserPhase(int iCurrentPhase)
    {
        DataTable dtMissionList = clsPhaseMissions.GetPhaseMissionsList("iPhaseID=" + iCurrentPhase, ""); //+ " AND iAccountUserID=" + clsAccountUsers.iAccountUserID

        int iTotalMissions = dtMissionList.Rows.Count;

        return dtMissionList;
    }

    protected DataTable getAllPhasesForCurrentUserGroup(int iCurrentGroup)
    {
        DataTable dtPhaseList = clsGroupPhases.GetGroupPhasesList("iGroupID=" + iCurrentGroup, ""); //+ " AND iAccountUserID=" + clsAccountUsers.iAccountUserID

        //int iTotalPhases = dtPhaseList.Rows.Count;

        return dtPhaseList;
    }

    protected string getAchievementList(int iPhaseID)
    {
        //StringBuilder strbAchievementList = new StringBuilder();
        string strAchievements = "";

        //### GET MISSIONS FOR CURRENT PHASE
        DataTable dtMissionList = getAllMissionsForCurrentUserPhase(iPhaseID);

        foreach (DataRow dtrMissionItems in dtMissionList.Rows)
        {
            int iMissionID = Convert.ToInt32(dtrMissionItems["iMissionID"]);

            clsMissions clsMissions = new clsMissions(iMissionID);
            int iMissionTypeID = clsMissions.iMissionTypeID;

            if (strAchievements != "")
                strAchievements += ",";

            strAchievements += clsMissions.iAchievementID;
        }

        return strAchievements;
    }

    protected string popCompletedAchievements(int iAccelerateUserID)
    {
        DataTable dtUserAchievement = clsUserAchievements.GetUserAchievementsList("iAccountUserID=" + iAccelerateUserID, "");

        string strListOfIDs = "";
        int iAchievementID;
        int iCount = 0;

        foreach (DataRow dtrAccountMember in dtUserAchievement.Rows)
        {
            ++iCount;

            iAchievementID = Convert.ToInt32(dtrAccountMember["iAchievementID"].ToString());
            strListOfIDs += iAchievementID;

            //clsAchievements clsAchievements = new clsAchievements(iAchievementID);

            if (dtUserAchievement.Rows.Count != iCount)
                strListOfIDs += ",";
        }

        return strListOfIDs;
    }

    protected void popAllAchievements(string strAlreadyDisplayedBadges, DataTable dtAllPossibleBadgesList, int iAccelerateUserID)
    {
        string[] idsList = strAlreadyDisplayedBadges.Split(',');
        int iCount = 0;
        int iAchieveTotalCount = 0;
        dtAllPossibleBadgesList.Columns.Add("iMissionID");
        dtAllPossibleBadgesList.Columns.Add("strLink");
        dtAllPossibleBadgesList.Columns.Add("strNewRow");

        foreach (DataRow dtrAccountMember in dtAllPossibleBadgesList.Rows)
        {
            ++iCount;
            int iAchievementID = Convert.ToInt32(dtrAccountMember["iAchievementID"].ToString());

            //### Gets the MissionID and adds it in a new datable for the href
            DataTable dtCurrentMission = clsMissions.GetMissionsList("iAchievementID=" + iAchievementID, "");
            foreach (DataRow dtrMissionList in dtCurrentMission.Rows)
            {
                int iMissionID = Convert.ToInt32(dtrMissionList["iMissionID"]);
                dtrAccountMember["iMissionID"] = iMissionID;
                clsAchievements clsCurrentAchievement = new clsAchievements(iAchievementID);

                foreach (DataRow dtrPossibleAchievements in dtAllPossibleBadgesList.Rows)
                {
                    if (Convert.ToInt16(dtrPossibleAchievements["iAchievementID"].ToString()) == iAchievementID)
                    {
                        foreach (string strID in idsList)
                        {
                            if (strID != "")
                            {
                                if (Convert.ToInt16(strID) == iAchievementID)
                                {

                                    if ((clsCurrentAchievement.strMasterImage == "") || (clsCurrentAchievement.strMasterImage == null))
                                    {
                                        dtrAccountMember["FullPathForImage"] = "images/imgNoImage.png";
                                    }
                                    else
                                    {
                                        dtrAccountMember["FullPathForImage"] = "Achievements/" + clsCurrentAchievement.strPathToImages + "/" + Path.GetFileNameWithoutExtension(clsCurrentAchievement.strMasterImage) + "" + Path.GetExtension(clsCurrentAchievement.strMasterImage);
                                        break;
                                    }
                                }
                                else
                                {
                                    dtrAccountMember["FullPathForImage"] = "img/Lock.png";
                                }
                            }
                            else
                            {
                                dtrAccountMember["FullPathForImage"] = "img/Lock.png";
                            }


                            if (iMissionID != 1)
                                dtrAccountMember["strLink"] = "Accelerate-Current-Mission.aspx?iMissionID=" + iMissionID;
                            else
                                dtrAccountMember["strLink"] = "#";
                        }
                        //++iAchieveTotalCount;

                        //if (iAchieveTotalCount == 1)
                        //    dtrAccountMember["strNewRow"] = "<div class='row-fluid'>";
                        //else if (iAchieveTotalCount == 9)
                        //    dtrAccountMember["strNewRow"] = "</div><div class='row-fluid'>";
                        //else
                        //    dtrAccountMember["strNewRow"] = "";
                    }
                }
            }
        }

        rpAchievements.DataSource = dtAllPossibleBadgesList;
        rpAchievements.DataBind();
    }

    protected DataTable getDtAllPossibleAchievements(int iAccountUserID)
    {
        string strListOfAllPossibleAchievements = "";
        int iCount = 0;
        int iCountGroupPhases = 0;

        DataTable dtAchievement = clsAchievements.GetAchievementsList();
        dtAchievement.Columns.Add("FullPathForImage");
        //dtAchievement.Columns.Add("FullPathForImage");

        DataTable dtPossibleAchievements = new DataTable();
        dtPossibleAchievements = dtAchievement.Clone();
        dtPossibleAchievements.Clear();

        clsAccountUsers clsCurrentAccountUser = new clsAccountUsers(iAccountUserID);
        int iGroupID = clsCurrentAccountUser.iGroupID;

        DataTable dtGroupPhases = clsGroupPhases.GetGroupPhasesList("iGroupID=" + iGroupID, "");

        foreach (DataRow dtrGrouphases in dtGroupPhases.Rows)
        {
            iCountGroupPhases++;

            int iPhaseID = Convert.ToInt32(dtrGrouphases["iPhaseID"]);
            DataTable dtPhaseMission = clsPhaseMissions.GetPhaseMissionsList("iPhaseID=" + iPhaseID, "");

            foreach (DataRow dtrPhaseMission in dtPhaseMission.Rows)
            {
                ++iCount;

                int iMissionID = Convert.ToInt32(dtrPhaseMission["iMissionID"]);
                clsMissions clsMissions = new clsMissions(iMissionID);
                int iAchievementID = clsMissions.iAchievementID;
                //strListOfAllPossibleAchievements += iAchievementID;

                foreach (DataRow dtrAvailableAchievements in dtAchievement.Rows)
                {
                    if (iAchievementID == Convert.ToInt32(dtrAvailableAchievements["iAchievementID"].ToString()))
                        dtPossibleAchievements.Rows.Add(dtrAvailableAchievements.ItemArray);
                }
            }
        }

        dtPossibleAchievements.DefaultView.Sort = "strTitle ASC";
        dtPossibleAchievements = dtPossibleAchievements.DefaultView.ToTable();

        return dtPossibleAchievements;
    }

    protected string popAchievements(int iAccelerateUserID)
    {
        DataTable dtUserAchievement = clsUserAchievements.GetUserAchievementsList("iAccountUserID=" + iAccelerateUserID, "");

        string strListOfIDs = "";
        int iAchievementID;
        int iCount = 0;

        foreach (DataRow dtrAccountMember in dtUserAchievement.Rows)
        {
            ++iCount;

            iAchievementID = Convert.ToInt32(dtrAccountMember["iAchievementID"].ToString());

            strListOfIDs += iAchievementID;

            clsAchievements clsAchievements = new clsAchievements(iAchievementID);

            if (dtUserAchievement.Rows.Count != iCount)
                strListOfIDs += ",";
        }

        return strListOfIDs;
    }

    protected DataTable getAchievementsToDisplayDT(string strAchievementsList)
    {
        DataTable dtAchievement = clsAchievements.GetAchievementsList();
        dtAchievement.Columns.Add("FullPathForImage");

        DataTable dtAchievementsToDisplay = new DataTable();
        dtAchievementsToDisplay = dtAchievement.Clone();
        dtAchievementsToDisplay.Clear();

        string[] AchievementIDs = strAchievementsList.Split(',');

        foreach (string ID in AchievementIDs)
        {
            foreach (DataRow dtrAvailableAchievements in dtAchievement.Rows)
            {
                if (Convert.ToInt32(ID) == Convert.ToInt32(dtrAvailableAchievements["iAchievementID"].ToString()))
                    dtAchievementsToDisplay.Rows.Add(dtrAvailableAchievements.ItemArray);
            }
        }

        return dtAchievementsToDisplay;
    }

    protected string popAvailablePhasesMissionsAchievements()
    {
        int iPhasesAvailable = canWeUnlockNextPhase();

        clsPhases clsCurrentPhase = new clsPhases();
        DataTable dtPhase = getAllPhasesForCurrentUserGroup(clsAccountUsers.iGroupID);

        int iCount = 0;
        string strMissions = "";
        string strAchievementsList = "";

        DateTime dtCurrentDate = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        DateTime dtStartTime = new DateTime();
        DateTime dtEndTime = new DateTime();

        foreach (DataRow dtrCurrentPhase in dtPhase.Rows)
        {
            ++iCount;

            int iPhaseID = Convert.ToInt32(dtrCurrentPhase["iPhaseID"].ToString());
            clsCurrentPhase = new clsPhases(iPhaseID);

            dtStartTime = Convert.ToDateTime(clsCurrentPhase.dtStartDate);
            dtEndTime = Convert.ToDateTime(clsCurrentPhase.dtEndDate);

            if (dtStartTime <= dtCurrentDate && dtEndTime >= dtCurrentDate)
            {
                if (iPhasesAvailable >= iCount)
                {
                    if (strAchievementsList != "")
                        strAchievementsList += ",";

                    //### Get list of possible achievements
                    strAchievementsList = strAchievementsList.ToString() + getAchievementList(iPhaseID);
                }
            }
        }

        return strAchievementsList;
    }

    private int canWeUnlockNextPhase() //###return 1 2 or 3 depending what should be available
    {
        int iGroupID = clsAccountUsers.iGroupID;
        clsGroups clsGroups = new clsGroups(clsAccountUsers.iGroupID);
        DataTable dtGroupPhases = clsGroupPhases.GetGroupPhasesList("iGroupID=" + iGroupID, "");
        int iPhaseID = 1;
        int iPhaseCount = 1;

        foreach (DataRow dtrGroupPhasesList in dtGroupPhases.Rows)
        {
            iPhaseID = Convert.ToInt32(dtrGroupPhasesList["iPhaseID"]);
            DataTable dtPhaseMissions = clsPhaseMissions.GetPhaseMissionsList("iPhaseID=" + iPhaseID, "");

            bool bIsThisPhaseComplete = true;

            //break;
            int iMissionID = 0;
            foreach (DataRow dtrPhaseMissionsList in dtPhaseMissions.Rows)
            {
                iMissionID = Convert.ToInt32(dtrPhaseMissionsList["iMissionID"]);

                clsMissions clsMissions = new clsMissions(iMissionID);
                int iMissionTypeID = clsMissions.iMissionTypeID;

                if (iMissionTypeID == 1)
                {
                    if (!bIsTextMissionComplete(iMissionID))
                        bIsThisPhaseComplete = false;
                }
                else if (iMissionTypeID == 2)
                {
                    if (!bIsLeadershipMissionComplete(iMissionID))
                        bIsThisPhaseComplete = false;
                }
            }

            if (bIsThisPhaseComplete)
                ++iPhaseCount;
        }

        return iPhaseCount;
    }

    protected void popAchievementStages()
    {
        clsGroups clsGroup = new clsGroups(clsAccountUsers.iGroupID);

        StringBuilder strbAchievementStages = new StringBuilder();
        strbAchievementStages.AppendLine("<div class='achievementStageContainer'>");
        strbAchievementStages.AppendLine("<div class='ovalAchiementStage'>");
        strbAchievementStages.AppendLine("Current Status<br />");
        List<string> lstRanks = strCustomGroupRank(clsGroup.iCustomGroupRankID, clsAccountUsers.iCurrentPoints);
        string strRank = "";
        string strNextRank = "";
        int iCurrentRankPoints = 0;
        int iNextRankPoints = 0;
        int iCounter = 1;
        int iMaxPoints = 0;
        foreach (string strRankItem in lstRanks)
        {
            switch (iCounter)
            {
                case 1: strRank = strRankItem;
                    break;
                case 2: strNextRank = strRankItem;
                    break;
                case 3: iCurrentRankPoints = Convert.ToInt32(strRankItem);
                    break;
                case 4: iNextRankPoints = Convert.ToInt32(strRankItem);
                    break;
                case 5: iMaxPoints = Convert.ToInt32(strRankItem);
                    break;
            }

            iCounter++;
        }
        strbAchievementStages.AppendLine("<span>" + strRank + "</span>");
        strbAchievementStages.AppendLine("</div>");

        //int iCurrentRank = iRankID(clsAccountUsers.iCurrentPoints);
        //clsRanks clsCurrentRank = new clsRanks(iCurrentRank);
        //clsRanks clsNextRank = new clsRanks();
        //clsRanks clsFinalRank = new clsRanks();

        //if (iRankCount() > iCurrentRank)
        //{
        //clsNextRank = new clsRanks(iCurrentRank + 1);
        //clsFinalRank = new clsRanks(iRankCount());
        //}

        clsCustomGroupRanks clsCustomRanks = new clsCustomGroupRanks(clsGroup.iCustomGroupRankID);

        string strFinalRank = "";

        if (clsCustomRanks.iRank7Points == 0)
        {
            if (clsCustomRanks.iRank6Points == 0)
            {
                if (clsCustomRanks.iRank5Points == 0)
                {
                    if (clsCustomRanks.iRank4Points == 0)
                    {
                        if (clsCustomRanks.iRank3Points == 0)
                        {
                            if (clsCustomRanks.iRank2Points == 0)
                            {
                                strFinalRank = clsCustomRanks.strRank1Title;
                            }
                            else
                            {
                                strFinalRank = clsCustomRanks.strRank2Title;
                            }
                        }
                        else
                        {
                            strFinalRank = clsCustomRanks.strRank3Title;
                        }
                    }
                    else
                    {
                        strFinalRank = clsCustomRanks.strRank4Title;
                    }
                }
                else
                {
                    strFinalRank = clsCustomRanks.strRank5Title;
                }
            }
            else
            {
                strFinalRank = clsCustomRanks.strRank6Title;
            }
        }
        else
        {
            strFinalRank = clsCustomRanks.strRank7Title;
        }

        strbAchievementStages.AppendLine("<div class='ovalAchiementStage marginleftfifty'>");
        strbAchievementStages.AppendLine("Next Status<br />");
        strbAchievementStages.AppendLine("<span>" + strNextRank + "</span>");
        strbAchievementStages.AppendLine("</div>");
        strbAchievementStages.AppendLine("<div class='ovalAchiementStage marginleftfifty'>");
        strbAchievementStages.AppendLine("Final Status<br />");
        strbAchievementStages.AppendLine("<span>" + strFinalRank + "</span>");
        strbAchievementStages.AppendLine("</div></div>");

        litAchievementStages.Text = strbAchievementStages.ToString();
    }

    private List<string> strCustomGroupRank(int iCustomRankID, int iCurrentPoints)
    {
        List<string> lstRanks = new List<string>();
        string strRank = "";
        string strNextRank = "";
        string strPointLevel = "";
        string strNextPointLevel = "";

        clsCustomGroupRanks clsCustomRank = new clsCustomGroupRanks(iCustomRankID);
        DataTable dtRank = clsRanks.GetRanksList("", "");

        if (iCurrentPoints <= clsCustomRank.iRank1Points)
        {

            if (iCurrentPoints == clsCustomRank.iRank1Points)
            {
                strRank = clsCustomRank.strRank2Title;
                strNextRank = clsCustomRank.strRank3Title;
                strPointLevel = clsCustomRank.iRank2Points.ToString();
                strNextPointLevel = clsCustomRank.iRank3Points.ToString();
                lstRanks.Add(strRank);
                lstRanks.Add(strNextRank);
                lstRanks.Add(strPointLevel);
                lstRanks.Add(strNextPointLevel);
            }

            else if (iCurrentPoints < clsCustomRank.iRank1Points)
            {
                strRank = clsCustomRank.strRank1Title;
                strNextRank = clsCustomRank.strRank2Title;
                strPointLevel = clsCustomRank.iRank1Points.ToString();
                strNextPointLevel = clsCustomRank.iRank2Points.ToString();
                lstRanks.Add(strRank);
                lstRanks.Add(strNextRank);
                lstRanks.Add(strPointLevel);
                lstRanks.Add(strNextPointLevel);
            }
        }
        else if (iCurrentPoints <= clsCustomRank.iRank2Points)
        {
            if (iCurrentPoints == clsCustomRank.iRank2Points)
            {
                strRank = clsCustomRank.strRank3Title;
                strNextRank = clsCustomRank.strRank4Title;
                strPointLevel = clsCustomRank.iRank3Points.ToString();
                strNextPointLevel = clsCustomRank.iRank4Points.ToString();
                lstRanks.Add(strRank);
                lstRanks.Add(strNextRank);
                lstRanks.Add(strPointLevel);
                lstRanks.Add(strNextPointLevel);
            }

            else if (iCurrentPoints < clsCustomRank.iRank2Points)
            {
                strRank = clsCustomRank.strRank2Title;
                strNextRank = clsCustomRank.strRank3Title;
                strPointLevel = clsCustomRank.iRank2Points.ToString();
                strNextPointLevel = clsCustomRank.iRank3Points.ToString();
                lstRanks.Add(strRank);
                lstRanks.Add(strNextRank);
                lstRanks.Add(strPointLevel);
                lstRanks.Add(strNextPointLevel);
            }
        }

        else if (iCurrentPoints <= clsCustomRank.iRank3Points)
        {

            if (iCurrentPoints == clsCustomRank.iRank3Points)
            {
                strRank = clsCustomRank.strRank4Title;
                strNextRank = clsCustomRank.strRank5Title;
                strPointLevel = clsCustomRank.iRank4Points.ToString();
                strNextPointLevel = clsCustomRank.iRank5Points.ToString();
                lstRanks.Add(strRank);
                lstRanks.Add(strNextRank);
                lstRanks.Add(strPointLevel);
                lstRanks.Add(strNextPointLevel);
            }

            else if (iCurrentPoints < clsCustomRank.iRank3Points)
            {
                strRank = clsCustomRank.strRank3Title;
                strNextRank = clsCustomRank.strRank4Title;
                strPointLevel = clsCustomRank.iRank3Points.ToString();
                strNextPointLevel = clsCustomRank.iRank4Points.ToString();
                lstRanks.Add(strRank);
                lstRanks.Add(strNextRank);
                lstRanks.Add(strPointLevel);
                lstRanks.Add(strNextPointLevel);
            }
        }

        else if (iCurrentPoints <= clsCustomRank.iRank4Points)
        {

            if (iCurrentPoints == clsCustomRank.iRank4Points)
            {
                strRank = clsCustomRank.strRank5Title;
                strNextRank = clsCustomRank.strRank6Title;
                strPointLevel = clsCustomRank.iRank5Points.ToString();
                strNextPointLevel = clsCustomRank.iRank6Points.ToString();
                lstRanks.Add(strRank);
                lstRanks.Add(strNextRank);
                lstRanks.Add(strPointLevel);
                lstRanks.Add(strNextPointLevel);
            }

            else if (iCurrentPoints < clsCustomRank.iRank4Points)
            {
                strRank = clsCustomRank.strRank4Title;
                strNextRank = clsCustomRank.strRank5Title;
                strPointLevel = clsCustomRank.iRank4Points.ToString();
                strNextPointLevel = clsCustomRank.iRank5Points.ToString();
                lstRanks.Add(strRank);
                lstRanks.Add(strNextRank);
                lstRanks.Add(strPointLevel);
                lstRanks.Add(strNextPointLevel);
            }
        }

        else if (iCurrentPoints <= clsCustomRank.iRank5Points)
        {

            if (iCurrentPoints == clsCustomRank.iRank5Points)
            {
                strRank = clsCustomRank.strRank6Title;
                strNextRank = clsCustomRank.strRank7Title;
                strPointLevel = clsCustomRank.iRank6Points.ToString();
                strNextPointLevel = clsCustomRank.iRank7Points.ToString();
                lstRanks.Add(strRank);
                lstRanks.Add(strNextRank);
                lstRanks.Add(strPointLevel);
                lstRanks.Add(strNextPointLevel);
            }

            else if (iCurrentPoints < clsCustomRank.iRank5Points)
            {
                strRank = clsCustomRank.strRank5Title;
                strNextRank = clsCustomRank.strRank6Title;
                strPointLevel = clsCustomRank.iRank5Points.ToString();
                strNextPointLevel = clsCustomRank.iRank6Points.ToString();
                lstRanks.Add(strRank);
                lstRanks.Add(strNextRank);
                lstRanks.Add(strPointLevel);
                lstRanks.Add(strNextPointLevel);
            }
        }

        else if (iCurrentPoints <= clsCustomRank.iRank6Points)
        {

            if (iCurrentPoints == clsCustomRank.iRank6Points)
            {
                strRank = clsCustomRank.strRank7Title;
                strNextRank = "None";
                strPointLevel = clsCustomRank.iRank7Points.ToString();
                strNextPointLevel = "0";
                lstRanks.Add(strRank);
                lstRanks.Add(strNextRank);
                lstRanks.Add(strPointLevel);
                lstRanks.Add(strNextPointLevel);
            }

            else if (iCurrentPoints < clsCustomRank.iRank6Points && iCurrentPoints <= clsCustomRank.iRank7Points)
            {
                strRank = clsCustomRank.strRank6Title;
                strNextRank = clsCustomRank.strRank7Title;
                strPointLevel = clsCustomRank.iRank6Points.ToString();
                strNextPointLevel = clsCustomRank.iRank7Points.ToString();
                lstRanks.Add(strRank);
                lstRanks.Add(strNextRank);
                lstRanks.Add(strPointLevel);
                lstRanks.Add(strNextPointLevel);
            }
        }

        else if (iCurrentPoints <= clsCustomRank.iRank7Points)
        {
            strRank = clsCustomRank.strRank7Title;
            strNextRank = "None";
            strPointLevel = clsCustomRank.iRank7Points.ToString();
            strNextPointLevel = "0";
            lstRanks.Add(strRank);
            lstRanks.Add(strNextRank);
            lstRanks.Add(strPointLevel);
            lstRanks.Add(strNextPointLevel);
        }

        else
        {
            strRank = clsCustomRank.strRank7Title;
            strNextRank = "None";
            strPointLevel = clsCustomRank.iRank7Points.ToString();
            strNextPointLevel = "0";
            lstRanks.Add(strRank);
            lstRanks.Add(strNextRank);
            lstRanks.Add(strPointLevel);
            lstRanks.Add(strNextPointLevel);
        }

        string strMaxLevel = "";
        bool bMaxFound = false;

        if (bMaxFound == false)
        {
            if (clsCustomRank.iRank7Points != 0)
            {
                strMaxLevel = clsCustomRank.iRank7Points.ToString();
                bMaxFound = true;
            }
        }
        if (bMaxFound == false)
        {
            if (clsCustomRank.iRank6Points != 0)
            {
                strMaxLevel = clsCustomRank.iRank6Points.ToString();
                bMaxFound = true;
            }
        }
        if (bMaxFound == false)
        {
            if (clsCustomRank.iRank5Points != 0)
            {
                strMaxLevel = clsCustomRank.iRank5Points.ToString();
                bMaxFound = true;
            }
        }
        if (bMaxFound == false)
        {
            if (clsCustomRank.iRank4Points != 0)
            {
                strMaxLevel = clsCustomRank.iRank4Points.ToString();
                bMaxFound = true;
            }
        }
        if (bMaxFound == false)
        {
            if (clsCustomRank.iRank3Points != 0)
            {
                strMaxLevel = clsCustomRank.iRank3Points.ToString();
                bMaxFound = true;
            }
        }
        if (bMaxFound == false)
        {
            if (clsCustomRank.iRank2Points != 0)
            {
                strMaxLevel = clsCustomRank.iRank2Points.ToString();
                bMaxFound = true;
            }
        }

        lstRanks.Add(strMaxLevel);

        return lstRanks;

    }

    //### Returns if Mission is Complete for Text mission
    protected bool bIsTextMissionComplete(int iCurrentMissionID)
    {
        bool bIsTextMissionComplete = false;

        DataTable dtMissionQuestions = clsMissionQuestions.GetMissionQuestionsList("iMissionID=" + iCurrentMissionID, "");
        int iTotalQuestionCount = dtMissionQuestions.Rows.Count;

        DataTable dtCompletedAnswersToQuestions = clsUserAnswers.GetUserAnswersList("iMissionID=" + iCurrentMissionID + " AND iAccountUserID=" + clsAccountUsers.iAccountUserID, "");
        int iTotalAnsweredCount = dtCompletedAnswersToQuestions.Rows.Count;
        int iCurrentPercentage = 0;

        //### Check if undefined
        if ((iTotalQuestionCount != 0))
        {
            double dblCurrentPercentage = Convert.ToDouble(iTotalAnsweredCount) / Convert.ToDouble(iTotalQuestionCount);
            iCurrentPercentage = Convert.ToInt32(dblCurrentPercentage * 200);
        }

        if (iCurrentPercentage == 200)
            bIsTextMissionComplete = true;

        return bIsTextMissionComplete;
    }

    //### Returns if Mission is Complete for Teamship mission
    protected bool bIsLeadershipMissionComplete(int iCurrentMissionID)
    {
        bool bIsLeadershipMissionComplete = false;

        DataTable dtMissionQuestions = clsTeamShips.GetTeamShipsList("iMissionID=" + iCurrentMissionID, "");
        int iTotalQuestionCount = dtMissionQuestions.Rows.Count;

        DataTable dtCompletedAnswersToQuestions = clsTeamShipAnswers.GetTeamShipAnswersList("iMissionID=" + iCurrentMissionID + " AND iAddedby=" + clsAccountUsers.iAccountUserID, "");
        int iTotalAnsweredCount = dtCompletedAnswersToQuestions.Rows.Count;
        int iCurrentPercentage = 0;

        //### Check if undefined
        if ((iTotalQuestionCount != 0))
        {
            double dblCurrentPercentage = Convert.ToDouble(iTotalAnsweredCount) / Convert.ToDouble(iTotalQuestionCount);
            iCurrentPercentage = Convert.ToInt32(dblCurrentPercentage * 200);
        }

        if (iCurrentPercentage == 200)
            bIsLeadershipMissionComplete = true;

        return bIsLeadershipMissionComplete;
    }

    #endregion
}