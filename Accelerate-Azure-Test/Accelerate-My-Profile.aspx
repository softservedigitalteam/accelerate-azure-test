﻿<%@ Page Title="Accelerate - My Profile" Language="C#" MasterPageFile="~/Accelerate.master" AutoEventWireup="true" Inherits="Accelerate_My_Profile" Codebehind="Accelerate-My-Profile.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <%--  <link href="css/base.css" rel="stylesheet" />
    <link href="css/skeleton.css" rel="stylesheet" />--%>
 <%--<link href="css/style.css" rel="stylesheet" type="text/css" />--%>

    <%--<link href="CMS/css/styles.css" rel="stylesheet" />--%>
    <script src="CMS/scripts/jquery.min.js"></script>
<script src="CMS/scripts/jquery.Jcrop.min.js"></script>
<link rel="stylesheet" href="CMS/css/jquery.Jcrop.css" type="text/css" />
    <script lang="javascript" type="text/javascript">
        function jCrop() {
            jQuery('#<%=imgCrop.ClientID%>').Jcrop({
                onSelect: storeCoords,
                onChange: storeCoords,
                aspectRatio: 1 / 1
            });
        }

        jQuery(window).load(jCrop);

       function storeCoords(c) {
            jQuery('#<%=X.ClientID%>').val(c.x);
           jQuery('#<%=Y.ClientID%>').val(c.y);
            jQuery('#<%=W.ClientID%>').val(c.w);
           jQuery('#<%=H.ClientID%>').val(c.h);
       };    

    </script>
  </asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="updMain" runat="server">
        <ContentTemplate>

            <div class="infobox">
                <div class="top-bar-alternative">
                    <h3>My Profile</h3>
                </div>
                <div id="mandatoryDiv" class="mandatoryInvalidDiv" runat="server" visible="false">
                    <asp:Label ID="lblValidationMessage" runat="server"></asp:Label>
                </div>
                <div class="infobox well-short" style="color: #000; background: #FFF; position: relative;">
                    <b style="font-size: 13px;">Edit your profile below</b>
                    <br />
                    <br />
                    <asp:FileUpload ID="FileUpload" runat="server" CssClass="Green3 btn" onblur="setValid(this);" onchange="this.form.submit()" />
                    <br />
                    <br />
                    <asp:Label ID="lblUniquePath" runat="Server" Visible="false"></asp:Label><br style="clear: both" />
                    <div style="position: absolute; top: 35px; right: 40px;">
                        <asp:UpdatePanel ID="udpImages" runat="server" ChildrenAsTriggers="true">
                            <ContentTemplate>
                                <asp:DataList ID="dlImages" runat="server" RepeatColumns="3" ItemStyle-CssClass="listImages">
                                    <ItemTemplate>
                                        <table cellspacing="5">
                                            <%# Container.DataItem %>
                                        </table>
                                        <div runat="server" visible="false" align="center" style="padding-top: 5px; display: none;">
                                            <asp:RadioButton ID="rdbMainImage" runat="server" GroupName="MainImages" Checked="true" Text="Main Image" onclick="javascript:CheckOnOff(this.id,'dlImages');" /><br />
                                        </div>
                                    </ItemTemplate>
                                </asp:DataList>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <asp:TextBox runat="server" ID="txtFirstName" type="text" class="textboxAlt" placeholder="Name" />
                    <asp:TextBox runat="server" ID="txtSurname" type="text" class="textboxAlt" placeholder="Surname" />
                    <asp:TextBox runat="server" ID="txtPhoneNumber" type="text" class="textboxAlt" placeholder="Contact Number" />

                    <asp:TextBox runat="server" ID="txtEmailAddress" type="text" class="textboxAlt" placeholder="Email" />
                    <br />
                    <br />
                    <b>
                        <asp:Label ID="lblUploadError" runat="server"></asp:Label></b>
                    <asp:Button runat="server" ID="btnSave" Text="Save" CssClass="Green3 btn" OnClick="btnSave_Click" />
                </div>

            </div>
            <asp:UpdateProgress ID="updateProgress" runat="server">
                <ProgressTemplate>
                    <div style="position: fixed; height: 100%; width: 100%; overflow: show; margin: auto; top: 0; left: 0; bottom: 0;right: 0; z-index: 9999999; background-color: #fff; opacity: 0.8;">
                        <center><asp:Image ID="imgUpdateProgress4" runat="server" ImageUrl="img/Loader.gif" AlternateText="Loading ..." ToolTip="Loading ..." style="opacity:1;position:fixed;top: 43%;"/></center>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>

        </ContentTemplate>

        <%--        <Triggers>
            <asp:PostBackTrigger ControlID="btnUpload" />
        </Triggers>--%>
    </asp:UpdatePanel>

  
  
    
<cc1:ModalPopupExtender ID="ModalPopupExtenderCrop" runat="server" TargetControlID="btnPopupCrop"
        PopupControlID="pnlCrop" BackgroundCssClass="modalBackground" CancelControlID="btnCancel"
        RepositionMode="RepositionOnWindowResize" Y="0">
    </cc1:ModalPopupExtender>

 
    <asp:Panel ID="pnlCrop" runat="server">
        <div class="top-bar">
           <h3>Select area to crop.</h3> 
        </div>
        <div class="well">
            <asp:Image ID="imgCrop" runat="server" />
        <div>
        <asp:HiddenField ID="X" runat="server" />
        <asp:HiddenField ID="Y" runat="server" />
        <asp:HiddenField ID="W" runat="server" />
        <asp:HiddenField ID="H" runat="server" />
            <%--<img src="CMS/images/imgDivider.png" alt="" title="" />--%>
            <hr style="height:1px;border:none;color:#333;background-color:transparent;width:600px" />
        </div>
        <div class="buttonsRightDiv" style="padding-right: 10px">
            <asp:LinkButton ID="btnCrop" runat="server" CssClass="Green3 btn" Text="Crop" AlternateText="Crop" OnClick="btnCrop_Click"  />
            <asp:LinkButton ID="btnCancel" runat="server" CssClass="Green3 btn" Text="Back" AlternateText="Back" />
        </div>
            </div>
    </asp:Panel>
        <asp:Button ID="btnPopupCrop" runat="server" Style="display: none" />
     
    
    
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ScrMang" Runat="Server">
    <script src='assets/js/jquery.hotkeys.js'></script>
        <script src='assets/js/calendar/fullcalendar.min.js'></script>
        <script src="assets/js/jquery-ui-1.10.2.custom.min.js"></script>
        <script src="assets/js/jquery.pajinate.js"></script>
        <script src="assets/js/jquery.prism.min.js"></script>
        <script src="assets/js/jquery.dataTables.min.js"></script>
        <script src="assets/js/charts/jquery.flot.time.js"></script>
        <script src="assets/js/charts/jquery.flot.pie.js"></script>
        <script src="assets/js/charts/jquery.flot.resize.js"></script>
        <script src="assets/js/bootstrap/bootstrap-wysiwyg.js"></script>
        <script src="assets/js/bootstrap/bootstrap-typeahead.js"></script>
        <script src="assets/js/jquery.easing.min.js"></script>
        <script src="assets/js/jquery.chosen.min.js"></script>
        <script src="assets/js/avocado-custom.js"></script>
</asp:Content>