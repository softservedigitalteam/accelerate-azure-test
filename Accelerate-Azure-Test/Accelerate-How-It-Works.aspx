﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Accelerate.master" AutoEventWireup="true" Inherits="Accelerate_How_It_Works" Codebehind="Accelerate-How-It-Works.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script src="js/jquery.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="genericMainHeading">
        <h2>How it works</h2>
    </div>
    <div>
    
    </div>
    <img src="images/Accelerate-Tutorial.jpg" width="98%" style="float:left"/>

    <script type="text/javascript">
        function balanceHelp() {
            var lhs = $('#leftHandSideSection').height();
            var rhs = $('#rightHandSideSection').height();
            if (lhs > rhs)
            {
                $('#rightHandSideSection').css({ 'height': lhs + 'px' });
                $('#leftHandSideSection').css({ 'height': lhs + 'px' });
            }
            else
            {
                $('#rightHandSideSection').css({ 'height': rhs + 'px' });
                $('#leftHandSideSection').css({ 'height': rhs + 'px' });
            }
        }
    </script>
</asp:Content>

