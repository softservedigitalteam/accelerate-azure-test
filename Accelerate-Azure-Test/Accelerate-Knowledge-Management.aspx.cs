using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Accelerate_Knowledge_Management : System.Web.UI.Page
{
    clsAccountUsers clsAccountUsers;
    clsKnowledgeManagements clsKnowledgeManagements;

    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsAccountUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("Accelerate-Login.aspx");
            }
        }
        try
        {
            clsAccountUsers = (clsAccountUsers)Session["clsAccountUsers"];
        }
        catch(Exception ex)
        {
            Response.Redirect("Accelerate-Error404.aspx");
        }

        //### Determines if a javascript delete has been called
        if (!String.IsNullOrEmpty(Page.Request["__EVENTARGUMENT"]) && (Page.Request["__EVENTARGUMENT"].Split(':')[0] == "iRemoveKnowledgeManagement"))
            DeleteKnowledgeManagement(Convert.ToInt32(Page.Request["__EVENTARGUMENT"].Split(':')[1]));

        if (!IsPostBack)
        {
            //### If the iKnowledgeManagementID is passed through then we want to instantiate the object with that iKnowledgeManagementID
            if ((Request.QueryString["iKnowledgeManagementID"] != "") && (Request.QueryString["iKnowledgeManagementID"] != null))
            {
                try
                {
                    clsKnowledgeManagements = new clsKnowledgeManagements(Convert.ToInt32(Request.QueryString["iKnowledgeManagementID"]));
                    //### Populate the form
                    popFormData();
                }
                catch(Exception ex)
                {
                    Response.Redirect("Accelerate-Error404.aspx");
                }
            }
            else
            {
                clsKnowledgeManagements = new clsKnowledgeManagements();
            }
            Session["clsKnowledgeManagements"] = clsKnowledgeManagements;
        }
        else
        {
            clsKnowledgeManagements = (clsKnowledgeManagements)Session["clsKnowledgeManagements"];
        }
    }

    //protected void lnkbtnBack_Click(object sender, EventArgs e)
    //{
    //    //### Go back to previous page
    //    Response.Redirect("frmKnowledgeManagementsView.aspx");
    //}

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        //### Validate registration process
        bool bCanSave = true;

        //bCanSave = clsValidation.IsNullOrEmpty(txtTitle, bCanSave);

        if (bCanSave == true)
        {
            //mandatoryDiv.Visible = false;
            //lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceHappy.png\" alt='' title=''/><div class=\"validationMessage\">KnowledgeManagement added successfully</div></div>";
            try
            {
                SaveData();
            }
            catch(Exception ex)
            {
                Response.Redirect("Accelerate-Error404.aspx");
            }
        }
        else
        {
            //mandatoryDiv.Visible = true;
            //lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">Please fill out all mandatory fields - KnowledgeManagement not added</div></div>";
        }
    }

    //protected void lnkbtnClear_Click(object sender, EventArgs e)
    //{
    //    mandatoryDiv.Visible = false;

    //    txtTitle.Text = "";
    //    clsValidation.SetValid(txtTitle);

    //    txtDescription.Text = "";
    //    clsValidation.SetValid(txtDescription);
    //}

    #endregion

    #region POPULATE DATA METHODS

    private void popFormData()
    {
        //txtTitle.Text = clsKnowledgeManagements.strTitle;
        //txtDescription.Text = clsKnowledgeManagements.strDescription;

        //### Populates images
        if (!string.IsNullOrEmpty(clsKnowledgeManagements.strPathToKnowledgeManagement))
        {
            lblUniquePath.Text = clsKnowledgeManagements.strPathToKnowledgeManagement;
            lblFileSize.Text = clsKnowledgeManagements.strFileSize;
            getList(clsKnowledgeManagements.strPathToKnowledgeManagement);
        }

        //clsCommonFunctions.setValidationImageValid(this);
    }

    #endregion

    #region SAVE DATA METHODS

    private void SaveData()
    {
        //### Add / Update
        clsKnowledgeManagements.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsKnowledgeManagements.iAddedBy = clsAccountUsers.iAccountUserID;
        clsKnowledgeManagements.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsKnowledgeManagements.iEditedBy = clsAccountUsers.iAccountUserID;

        clsKnowledgeManagements.strTitle = "";//txtTitle.Text;
        clsKnowledgeManagements.strDescription = "";//txtDescription.Text;

        //### Images related items
        clsKnowledgeManagements.strPathToKnowledgeManagement = lblUniquePath.Text;
        clsKnowledgeManagements.strFileSize = lblFileSize.Text;

        clsKnowledgeManagements.Update();

        Session["dtKnowledgeManagementsList"] = null;

        //### Go Congratulations PageS
        Response.Redirect("Accelerate-Mission-Success.aspx");
    }

    #endregion

    #region IMAGE METHODS

    List<string> lstKnowledgeManagement;
    List<string> lstKnowledgeManagementFileNames;
    int iMaxKnowledgeManagement = 1;

    string strUniqueFullPath = AppDomain.CurrentDomain.BaseDirectory + "\\KnowledgeManagements";

    protected void btnUpload_Click(object sender, EventArgs e)
    {
        if (Session["lstKnowledgeManagement"] == null)
        {
            lstKnowledgeManagement = new List<string>();
            Session["lstKnowledgeManagement"] = lstKnowledgeManagement;
        }
        //### Check that they have ONLY HAVE MAX NUMBER OF KnowledgeManagement in the datalist
        if (dlKnowledgeManagement.Items.Count == iMaxKnowledgeManagement)
        {
            //mandatoryDiv.Visible = true;
            //lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">You can only upload " + iMaxKnowledgeManagement.ToString() + " KnowledgeManagement per entry.</div></div>";
            getList(lblUniquePath.Text);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "setValid", "setValidFile('" + FileUpload.ClientID + "',true)", true);
        }
        else
        {
            //mandatoryDiv.Visible = false;

            string strUniquePath;
            if (lblUniquePath.Text == "")
            {
                strUniquePath = GetUniquePath();
                lblUniquePath.Text = strUniquePath;
            }
            else
            {
                strUniquePath = lblUniquePath.Text;
            }

            UploadKnowledgeManagement(strUniquePath);
            getList(strUniquePath);
        }
    }

    private string GetUniquePath()
    {
        int iCount = 1;
        //### First we need to get the path
        while (System.IO.Directory.Exists(strUniqueFullPath + "\\KnowledgeManagements" + iCount) == true)
        {
            iCount++;
        }
        return "KnowledgeManagements" + iCount;
    }

    protected void UploadKnowledgeManagement(String strUniquePath)
    {
        if (FileUpload.PostedFile.ContentLength > 0 && FileUpload.PostedFile.ContentLength < 5242880)
        {
            //### Upload files to unique folder
            string strUploadFileName = "";
            strUploadFileName = System.IO.Path.GetFileName(FileUpload.PostedFile.FileName);

            string strSaveLocation = "";
            strSaveLocation = strUniquePath + "\\" + strUploadFileName;

            if (!System.IO.Directory.Exists(strUniqueFullPath + "\\" + strUniquePath))
                System.IO.Directory.CreateDirectory(strUniqueFullPath + "\\" + strUniquePath);

            if (File.Exists(strSaveLocation))
                File.Delete(strSaveLocation);

            FileUpload.PostedFile.SaveAs(strUniqueFullPath + "\\" + strSaveLocation);

            lblFileSize.Text = FileUpload.PostedFile.ContentLength.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "setValid", "setValidFile('" + FileUpload.ClientID + "',true)", true);
        }
        else
        {
            lblUploadError.Text = "The file should be between 0 and 3MB.";
            lblUploadError.Visible = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "setValid", "setValidFile('" + FileUpload.ClientID + "',false)", true);
        }   
    }

    public void getList(String strPathToFolder)
    {
        lstKnowledgeManagement = new List<string>();
        lstKnowledgeManagementFileNames = new List<string>();

        try
        {
            String[] straFileList = Directory.GetFiles(strUniqueFullPath + "\\" + strPathToFolder);
            foreach (String strFile in straFileList)
            {
                int iImageCount = 0;
                if (strFile.Contains(".pdf"))
                {
                    //### Generates a javascript postback for the delete method
                    String strPostBack = Page.ClientScript.GetPostBackEventReference(this, "iRemoveKnowledgeManagement:" + iImageCount);

                    string strDocPath = strFile.Replace(AppDomain.CurrentDomain.BaseDirectory, System.Configuration.ConfigurationManager.AppSettings["WebRoot"]).Replace("\\", "/");

                    if (File.Exists(strFile))
                    {
                        lstKnowledgeManagement.Add(@"<tr style='text-align: centre;'>
                                        <td style='text-align: centre;'>
                                            <center>
                                            <a href='" + strDocPath + @"' target='_blank' style='color: #000000; text-decoration: none; text-decoration: none;'>
                                            <img src='images/imgPdf.png' alt='' title='' style='border: none;' /><br /><br />" +
                                                    "<div>" + Path.GetFileNameWithoutExtension(strFile) + "</div></a></center>" +
                                                        "<center><div class='buttonStyle' style='padding-top: 5px;'><a href=\"javascript:" + strPostBack + "\" style='color: #ffffff;'><div class='btnStart'>Delete</div></a></div></center>" + @"
                                        </td>
                                    </tr>");
                        lstKnowledgeManagementFileNames.Add(Path.GetFileName(strFile));
                        iImageCount++;
                    }
                }
                else if (strFile.Contains(".doc") || strFile.Contains(".docx"))
                {
                    //### Generates a javascript postback for the delete method
                    String strPostBack = Page.ClientScript.GetPostBackEventReference(this, "iRemoveKnowledgeManagement:" + iImageCount);

                    string strDocPath = strFile.Replace(AppDomain.CurrentDomain.BaseDirectory, System.Configuration.ConfigurationManager.AppSettings["WebRoot"]);

                    if (File.Exists(strFile))
                    {
                        lstKnowledgeManagement.Add(@"<tr style='text-align: centre;'>
                                        <td style='text-align: centre;'>
                                            <center><a href='" + strDocPath + @"' target='_blank' style='color: #000000; text-decoration: none; text-decoration: none;'>
                                            <img src='images/imgWord.png' alt='' title='' style='border: none;' /><br /><br />" +
                                                    "<div>" + Path.GetFileNameWithoutExtension(strFile) + "</div></a></center>" +
                                                        "<center><div class='buttonStyle' style='padding-top: 5px;'><a href=\"javascript:" + strPostBack + "\" style='color: #ffffff;'><div class='btnStart'>Delete</div></a></div></center>" + @"
                                        </td>
                                    </tr>");
                        lstKnowledgeManagementFileNames.Add(Path.GetFileName(strFile));
                        iImageCount++;
                    }
                }
                else if (strFile.Contains(".xls") || strFile.Contains(".xlsx"))
                {
                    //### Generates a javascript postback for the delete method
                    String strPostBack = Page.ClientScript.GetPostBackEventReference(this, "iRemoveKnowledgeManagement:" + iImageCount);

                    string strDocPath = strFile.Replace(AppDomain.CurrentDomain.BaseDirectory, System.Configuration.ConfigurationManager.AppSettings["WebRoot"]);

                    if (File.Exists(strFile))
                    {
                        lstKnowledgeManagement.Add(@"<tr style='text-align: centre;'>
                                        <td style='text-align: centre;'>
                                            <center><a href='" + strDocPath + @"' target='_blank' style='color: #000000; text-decoration: none; text-decoration: none;'>
                                            <img src='images/imgExcel.png' alt='' title='' style='border: none;' /><br /><br />" +
                                                    "<div>" + Path.GetFileNameWithoutExtension(strFile) + "</div></a></center>" +
                                                        "<center><div class='buttonStyle' style='padding-top: 5px;'><a href=\"javascript:" + strPostBack + "\" style='color: #ffffff;'><div class='btnStart'>Delete</div></a></div></center>" + @"
                                        </td>
                                    </tr>");
                        lstKnowledgeManagementFileNames.Add(Path.GetFileName(strFile));
                        iImageCount++;
                    }
                }
                else if (strFile.Contains(".ppt") || strFile.Contains(".pptx"))
                {
                    //### Generates a javascript postback for the delete method
                    String strPostBack = Page.ClientScript.GetPostBackEventReference(this, "iRemoveKnowledgeManagement:" + iImageCount);

                    string strDocPath = strFile.Replace(AppDomain.CurrentDomain.BaseDirectory, System.Configuration.ConfigurationManager.AppSettings["WebRoot"]);

                    if (File.Exists(strFile))
                    {
                        lstKnowledgeManagement.Add(@"<tr style='text-align: centre;'>
                                        <td style='text-align: centre;'>
                                            <center><a href='" + strDocPath + @"' target='_blank' style='color: #000000; text-decoration: none; text-decoration: none;'>
                                            <img src='images/imgPowerPoint.png' alt='' title='' style='border: none;' /><br /><br />" +
                                                    "<div>" + Path.GetFileNameWithoutExtension(strFile) + "</div></a></center>" +
                                                        "<center><div class='buttonStyle' style='padding-top: 5px;'><a href=\"javascript:" + strPostBack + "\" style='color: #ffffff;'><div class='btnStart'>Delete</div></a></div></center>" + @"
                                        </td>
                                    </tr>");
                        lstKnowledgeManagementFileNames.Add(Path.GetFileName(strFile));
                        iImageCount++;
                    }
                }
            }

            dlKnowledgeManagement.DataSource = lstKnowledgeManagement;
            dlKnowledgeManagement.DataBind();

            Session["lstKnowledgeManagement"] = lstKnowledgeManagement;
            Session["lstKnowledgeManagementFileNames"] = lstKnowledgeManagementFileNames;
        }
        catch (Exception ex)
        { }
    }

    private string GetMainImagePath(DataList dtlTarget)
    {
        string strReturn = "";

        foreach (DataListItem dliTarget in dtlTarget.Items)
        {
            RadioButton rdbMainImage = (RadioButton)dliTarget.FindControl("rdbMainImage");
            if (rdbMainImage.Checked)
            {
                lstKnowledgeManagementFileNames = (List<string>)Session["lstKnowledgeManagementFileNames"];
                strReturn = lstKnowledgeManagementFileNames[dliTarget.ItemIndex];
                break;
            }
        }
        return strReturn;
    }

    private void DeleteKnowledgeManagement(int iKnowledgeManagementIndex)
    {

        //### Deletes all KnowledgeManagement related to the target KnowledgeManagement.
        lstKnowledgeManagement = (List<string>)Session["lstKnowledgeManagement"];
        lstKnowledgeManagementFileNames = (List<string>)Session["lstKnowledgeManagementFileNames"];

        lstKnowledgeManagement.RemoveAt(iKnowledgeManagementIndex);
        Session["lstKnowledgeManagement"] = lstKnowledgeManagement;

        string[] files = Directory.GetFiles(strUniqueFullPath + "\\" + lblUniquePath.Text);

        foreach (string file in files)
        {
            if (Path.GetFileName(file) == lstKnowledgeManagementFileNames[iKnowledgeManagementIndex].ToString())
            {
                //### Remove all KnowledgeManagement
                File.Delete(file);
                break;
            }
        }

        lstKnowledgeManagementFileNames.RemoveAt(iKnowledgeManagementIndex);
        ViewState["lstKnowledgeManagementFileNames"] = lstKnowledgeManagementFileNames;
        getList(lblUniquePath.Text);
    }

    #endregion
}