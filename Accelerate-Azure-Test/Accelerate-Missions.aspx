﻿<%@ Page Title="Accelerate-Missions" Language="C#" MasterPageFile="~/Accelerate.master" AutoEventWireup="true" Inherits="Accelerate_Missions" Codebehind="Accelerate-Missions.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <link href="css/component.css" rel="stylesheet" />
    <script src="js/jquery.js"></script>
    <script src="js/modernizr.custom.js"></script>
    <%--<script src="js/toucheffects.js"></script>--%>

    <style>
        .rightMenuContainer {
            position: relative;
        }

        @media (max-width: 1024px) {
            .span3{
                float: left;
                width: 30%;
            }

            .divKnowledgeBaseBottom {
                position: relative;
                top: -170px;
                margin-bottom: 260px;
                left: 92px;
            }

            .divKnowledgeBaseTop {
                width: 837px;
                float: left;
                left: 92px;
                position: relative;
            }
        }

        @media (max-width: 769px) {
            .divKnowledgeBaseBottom {
                position: relative;
                top: 10px;
                margin-bottom: 260px;
            }

            .divKnowledgeBaseTop {
                position: relative;
                left: -35px;
            }
            .span3
            {
                width: 33% !important;
                float: left !important;
            }
        }

        @media (max-width: 738px) {
            .divKnowledgeBaseBottom {
                position: relative;
                top: 60px;
                left: 90px;
                margin-bottom: 260px;
            }

            .divKnowledgeBaseTop {
                position: relative;
                left: -65px;
            }
        }

        
        @media (max-width: 668px) {
            .divKnowledgeBaseBottom {
                position: relative;
                top: 0px;
                left: 89px;
                margin-bottom: 0px;
            }

            .divKnowledgeBaseTop {
                position: relative;
                left: -81px;
            }
        }
        @media (max-width: 568px) {
            .divKnowledgeBaseBottom {
                position: relative;
                top: 0px;
                left: 89px;
                margin-bottom: 0px;
            }

            .divKnowledgeBaseTop {
                position: relative;
                left: -144px;
            }
        }

        @media (max-width: 481px) {
            .divKnowledgeBaseBottom {
                position: relative;
                top: 0px;
                left: 92px;
                margin-bottom: 0px;
            }

            .divKnowledgeBaseTop {
                position: relative;
                left: -200px;
            }
        }

        @media (max-width: 415px) {
            .divKnowledgeBaseBottom {
                position: relative;
                top: 0px;
                left: 93px;
                margin-bottom: 0px;
            }

            .divKnowledgeBaseTop {
                position: relative;
                left: -221px;
            }
        }

            

        @media (max-width: 376px) {
            .divKnowledgeBaseBottom {
                position: relative;
                top: 0px;
                left: 92px;
                margin-bottom: 0px;
            }

            .divKnowledgeBaseTop {
                position: relative;
                left: -232px;
            }
        }
        @media (max-width: 321px) {
            .divKnowledgeBaseBottom {
                position: relative;
                top: 0px;
                left: 90px !important;
                margin-bottom: 0px;
            }

            .divKnowledgeBaseTop {
                position: relative;
                left: -246px;
            }
        }

       

    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server" onload="balanceSectionsMission()">
    <div class="genericMainHeading">
        <h2>Missions</h2>
    </div>
    <%--<span>Complete and unlock missions</span>--%>

    <div class="missionHeaderImages">
        <ul class="grid cs-style-3">
            <asp:Repeater runat="server" ID='rpPhases'>
                <ItemTemplate>
                    <li class="one-third">
                        <figure>
                            <img src='<%#Eval ("FullPathForImage") %>' title='<%#Eval ("strPhaseTitle") %>' alt='<%#Eval ("strPhaseTitle") %>' style='<%#Eval ("strStyle") %>' />
                            <figcaption>
                                <h3 class="missionHeader"><%#Eval ("strPhaseTitle") %></h3>
                            </figcaption>
                        </figure>
                    </li>
                </ItemTemplate>
            </asp:Repeater>
        </ul>
    </div>
    <br class="clr" />

    <div class="sectionContainerLeft innerBoxWithShadow margintopforty">
        <h2 class="genericHeading">My Current Missions</h2>
        <div>
            <br />
            <div class="innerMissionText">
                <asp:Literal runat="server" ID="litMissionsList"></asp:Literal>
            </div>
        </div>
        <br class="clr"/>
        <br class="clr"/>
    </div>

    <script type="text/javascript">
        function balanceSectionsMission() {
            var lhs = $('#leftHandSideSection').height();
            var rhs = $('#rightHandSideSection').height();
            if (lhs > rhs) {
                $('#rightHandSideSection').css({ 'height': lhs + 'px' });
                $('#leftHandSideSection').css({ 'height': lhs + 'px' });
            }
            else {
                $('#rightHandSideSection').css({ 'height': rhs + 'px' });
                $('#leftHandSideSection').css({ 'height': rhs + 'px' });
            }
        }
    </script>
</asp:Content>
