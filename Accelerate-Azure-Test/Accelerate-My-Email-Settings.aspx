﻿<%@ Page Title="Accelerate - My Profile" Language="C#" MasterPageFile="~/Accelerate.master" AutoEventWireup="true" Inherits="Accelerate_My_Email_Settings" Codebehind="Accelerate-My-Email-Settings.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="css/base.css" rel="stylesheet" />
    <link href="css/skeleton.css" rel="stylesheet" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <%--<link href="CMS/css/styles.css" rel="stylesheet" />--%>
    <script src="js/switchery.js"></script>
    <link href="css/switchery.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="updMain" runat="server">
        <ContentTemplate>
            <asp:Literal ID="litProfile" runat="server"></asp:Literal>
            <center><div class="arrow-down"></div></center>
            <div class="sectionContainerLeft innerBoxWithShadow margintopten">
                <h2 class="genericHeading">My Profile</h2>
                <div class="ProfileMissionHead">
                    <div class="sectionContainerLeft ProfileInnerBoxWithShadow margintopten" style="width: 90%; margin-left: 40px; position: relative;">
                        <h2 class="InsideGenericHeading">Edit your profile below</h2>
                        <div class="controlDiv">
                            <div id="mandatoryDiv" class="mandatoryInvalidDiv" runat="server" visible="false">
                                <asp:Label ID="lblValidationMessage" runat="server"></asp:Label>
                            </div>
                            <br />

                            <div class="settingItem">
                                
                                <div style="float: left; width: 50%; padding: 0%; text-align: center;">
                                    <input runat="server" type="checkbox" class="js-switch" checked="checked" id="cbMission" /></div>
                                <div style="float: left; width: 50%; padding: 0%; color: #333;">Mission E-Mails</div>
                            </div>

                            <div class="settingItem">
                                
                                <div style="float: left; width: 50%; padding: 0%; text-align: center;">
                                    <input runat="server" type="checkbox" class="js-switch" checked="checked" id="cbReminders"/></div>
                                <div style="float: left; width: 50%; padding: 0%; color: #333;">Reminder E-Mails</div>
                            </div>

                            <div class="settingItem">                                
                                <div style="float: left; width: 50%; padding: 0%; text-align: center;">
                                    <input runat="server" type="checkbox" class="js-switch" checked="checked" id="cbNotification" /></div>
                                <div style="float: left; width: 50%; padding: 0%; color: #333;">Notification E-Mails</div>
                            </div>
                            <br class="clearingSpacer" />
                        </div>
                    </div>
                    <div class="imageHolkderCommonDiv">
                    </div> 
                </div>
                <asp:Button runat="server" ID="btnSave" Style="margin: 15px 30px 19px 40px;" Width="110px" Text="Save Changes" OnClick="btnSave_Click" />
            </div>
            

            <script type="text/javascript">
                function populateSwitch() {
                    var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));

                    elems.forEach(function (html) {
                        var switchery = new Switchery(html);
                    });
                }
            </script>
        </ContentTemplate>
        <Triggers>
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

