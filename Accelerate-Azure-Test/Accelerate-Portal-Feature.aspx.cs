﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.IO;
using System.Configuration;
using System.Data;

public partial class Accelerate_Portal_Feature : System.Web.UI.Page
{
    clsAccountUsers clsAccountUsers;
    clsPortalFeatures clsPortalFeatures;

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Logout User
        if (Request.QueryString["action"] == "syslogout")
        {
            //### Clear all seesion variables
            Session.Clear();
        }

        //### Check if session clsUser exists
        if (Session["clsAccountUsers"] == null)
        {
            //### Redirect back to login
            Response.Redirect("Accelerate-Login.aspx");
        }
        clsAccountUsers = (clsAccountUsers)Session["clsAccountUsers"];

        if (!IsPostBack)
        {
            //### If the iPortalFeatureID is passed through then we want to instantiate the object with that iPortalFeatureID
            if ((Request.QueryString["iFeatureID"] != "") && (Request.QueryString["iFeatureID"] != null))
            {
                clsPortalFeatures = new clsPortalFeatures(Convert.ToInt32(Request.QueryString["iFeatureID"]));

                //### Populate the form
                try
                {
                    popPortalFeatureDetails();
                }
                catch(Exception ex)
                {
                    Response.Redirect("Accelerate-Error404.aspx");
                }
                ScriptManager.RegisterStartupScript(this, this.GetType(), "loopFormCheck", "loopFormCheck();", true);
            }
            else
            {
                clsPortalFeatures = new clsPortalFeatures();
            }
            Session["clsPortalFeatures"] = clsPortalFeatures;
        }
        else
        {
            clsPortalFeatures = (clsPortalFeatures)Session["clsPortalFeatures"];
        }
    }

    #region POP METHODS

    private void popPortalFeatureDetails()
    {
        string strTitle = clsPortalFeatures.strTitle;
        string strYouTubeLink = clsPortalFeatures.strYouTubeLink.ToString();
        string strDescription = clsPortalFeatures.strDescription;
        string strHeader = "";
        string strIconUrl = "../images/imgPdf.png";
        string strDocumentName = "";

        string strDocumentPath = AppDomain.CurrentDomain.BaseDirectory + "\\Documents\\" + clsPortalFeatures.strPathToDocument;
        strDocumentPath = strDocumentPath.Replace("\\", "/");
        StringBuilder strbPortal = new StringBuilder();

        //## Checks if the document path is empty
        if (!string.IsNullOrEmpty(clsPortalFeatures.strPathToDocument))
        {
            strbPortal.AppendLine("<ul style='list-style:none;padding:0;margin:0;'>");
            if (!string.IsNullOrEmpty(clsPortalFeatures.strMasterDocument))
            {
                string[] files = Directory.GetFiles(strDocumentPath);

                if (files.Count() > 0)
                {
                    if (clsPortalFeatures.bIsMultiple)
                    {
                        strbPortal.AppendLine("<div class='row-fluid'>");
                        strbPortal.AppendLine("<div class='span9' style='text-align: left;'><strong>File Name</strong></div>");
                        strbPortal.AppendLine("<div class='span3' style='float:left; text-align: left; color:#000;'><strong>View/Download Link</strong></div>");
                        strbPortal.AppendLine("</div>");
                        strbPortal.AppendLine("<br/>");
                        for (int i = 0; i < files.Count(); i++)
                        {
                            if (files[i].Contains("pdf") || files[i].Contains("doc") || files[i].Contains("docx") || files[i].Contains("xls") || files[i].Contains("xlsx") || files[i].Contains("ppt") || files[i].Contains("pptx") || files[i].Contains("zip") || files[i].Contains("zipx"))
                            {
                                strbPortal.AppendLine("<li style=''>");
                                strDocumentName = files[i];
                                strDocumentName = strDocumentName.Replace("/", "\\");
                                strDocumentPath = strDocumentName.Replace(AppDomain.CurrentDomain.BaseDirectory + "\\", ConfigurationManager.AppSettings["WebRoot"]);
                                strDocumentPath = strDocumentPath.Replace("Accelerate/", "");
                                strDocumentPath = strDocumentPath.Replace("\\", "/");

                                if (strDocumentPath.Contains(".pdf"))
                                {
                                    strIconUrl = "../images/imgPdf.png";
                                }
                                else if (strDocumentPath.Contains(".doc") || strDocumentPath.Contains(".docx"))
                                {
                                    strIconUrl = "../images/imgWord.png";
                                }
                                else if (strDocumentPath.Contains(".xls") || strDocumentPath.Contains(".xlsx"))
                                {
                                    strIconUrl = "../images/imgExcel.png";
                                }
                                else if (strDocumentPath.Contains(".ppt") || strDocumentPath.Contains(".pptx"))
                                {
                                    strIconUrl = "../images/imgPowerPoint.png";
                                }
                                else if (strDocumentPath.Contains(".zip") || strDocumentPath.Contains(".zipx"))
                                {
                                    strIconUrl = "../images/imgZipIcon.png";
                                }

                                strHeader = Path.GetFileNameWithoutExtension(strDocumentPath);
                                strHeader = strHeader.Replace("-", " ");

                                string strDownloadName = System.Text.RegularExpressions.Regex.Replace(strHeader, @"\s+", " ");
                                strDownloadName = strDownloadName.Replace(" ", "-");

                                litPortalHeading.Text = clsPortalFeatures.strTitle;

                                strbPortal.AppendLine("<div style='width:100%;'>");

                                if (strDocumentPath.Contains(".pdf"))
                                {
                                    if ((strDocumentPath != null) && (strDocumentPath != ""))
                                    {
                                        strbPortal.AppendLine("<div class='span9' style='color:#000; text-align: left; float: left; margin-top: -20px;'>" + strHeader + "</div>");
                                        strbPortal.AppendLine("<div class='span3' style='color:#000; text-align: left; float: left; margin-top: -20px;'><a style=' color:#000;' href='" + strDocumentPath + "' target='_blank'>");
                                        strbPortal.AppendLine("Click to open");
                                        strbPortal.AppendLine("</a>");
                                    }
                                }
                                else if (strDocumentPath.Contains(".zip"))
                                {
                                    if ((strDocumentPath != null) && (strDocumentPath != ""))
                                    {
                                        strbPortal.AppendLine("<div class='span9' style=' color:#000; text-align: left; float: left; margin-top: -20px;'>" + strHeader + "</div>");
                                        strbPortal.AppendLine("<div class='span3' style=' color:#000; text-align: left; float: left; margin-top: -20px;'><a style=' color:#000;' href='" + strDocumentPath + "' target='_blank'>");
                                        strbPortal.AppendLine("Click to open");
                                        strbPortal.AppendLine("</a>");
                                    }
                                }
                                else
                                {
                                    if ((strDocumentPath != null) && (strDocumentPath != ""))
                                    {
                                        strbPortal.AppendLine("<div class='span9' style='color:#000; text-align: left; float: left; margin-top: -20px;'>" + strHeader + "</div>");
                                        strbPortal.AppendLine("<div class='span3' style='color:#000; text-align: left; float: left; margin-top: -20px;'><a style=' color:#000;' href='" + strDocumentPath + "' target='_blank'>");
                                        strbPortal.AppendLine("Click to open");
                                        strbPortal.AppendLine("</a>");
                                    }
                                }

                                if ((clsPortalFeatures.strYouTubeLink != null) && (clsPortalFeatures.strYouTubeLink != ""))
                                {;
                                    strbPortal.AppendLine("<br class='clr'/>");
                                    strbPortal.AppendLine("<div style='margin:0px;'><iframe width='100%' height='360' src='" + strYouTubeLink + "' frameborder='0' allowfullscreen></iframe></div>");
                                }
                                strbPortal.AppendLine("</div>");
                                strbPortal.AppendLine("</li>");

                                strbPortal.AppendLine("<br class='clr'/>");
                            }
                        }
                    }
                    else if (clsPortalFeatures.bIsChecklist)
                    {
                        string strCheckboxes = "";

                        try
                        {
                            DataTable dtFeaturedUser = clsFeatureUsers.GetPortalFeaturesList("iAccountUserID = " + clsAccountUsers.iAccountUserID + " AND + iFeatureID = " + clsPortalFeatures.iPortalFeatureID, "");
                            foreach (DataRow dr in dtFeaturedUser.Rows)
                            {
                                strCheckboxes = dr["strFeatureChecklist"].ToString();
                                break;
                            }
                        }
                        catch {}

                        lblCheckboxes.Value = strCheckboxes;

                            strbPortal.AppendLine("<div class='row-fluid'>");
                            strbPortal.AppendLine("<div class='span3' style='float:left; text-align: left;'><strong>Tick When Done</strong></div>");
                            strbPortal.AppendLine("<div class='span6' style='float:left; text-align: left;'><strong>File Name</strong></div>");
                            strbPortal.AppendLine("<div class='span3' style='float:left; text-align: left;'><strong>View/Download Link</strong></div>");
                            strbPortal.AppendLine("</div>");
                            strbPortal.AppendLine("<br/>");

                            for (int i = 0; i < files.Count(); i++)
                            {
                                if (files[i].Contains("pdf") || files[i].Contains("doc") || files[i].Contains("docx") || files[i].Contains("xls") || files[i].Contains("xlsx") || files[i].Contains("ppt") || files[i].Contains("pptx") || files[i].Contains("zip") || files[i].Contains("zipx"))
                                {
                                    int iCheckbox = i + 1;
                                    //checked='checked'
                                    strbPortal.AppendLine("<input id='" + Path.GetFileName(files[i].ToString()) + "' type='checkbox' onclick=\"loopForm();\" style='margin-left: 35px; width:40px; height:40px; position: absolute; left: 0px; margin-top: 18px;' />");
                                    strbPortal.AppendLine("<li style='margin-top: 20px;list-style:none;'>");
                                    strDocumentName = files[i];
                                    strDocumentName = strDocumentName.Replace("/", "\\");
                                    strDocumentPath = strDocumentName.Replace(AppDomain.CurrentDomain.BaseDirectory + "\\", ConfigurationManager.AppSettings["WebRoot"]);
                                    strDocumentPath = strDocumentPath.Replace("Accelerate/", "");
                                    strDocumentPath = strDocumentPath.Replace("\\", "/");

                                    if (strDocumentPath.Contains(".pdf"))
                                    {
                                        strIconUrl = "../images/imgPdf.png";
                                    }
                                    else if (strDocumentPath.Contains(".doc") || strDocumentPath.Contains(".docx"))
                                    {
                                        strIconUrl = "../images/imgWord.png";
                                    }
                                    else if (strDocumentPath.Contains(".xls") || strDocumentPath.Contains(".xlsx"))
                                    {
                                        strIconUrl = "../images/imgExcel.png";
                                    }
                                    else if (strDocumentPath.Contains(".ppt") || strDocumentPath.Contains(".pptx"))
                                    {
                                        strIconUrl = "../images/imgPowerPoint.png";
                                    }
                                    else if (strDocumentPath.Contains(".zip") || strDocumentPath.Contains(".zipx"))
                                    {
                                        strIconUrl = "../images/imgZipIcon.png";
                                    }

                                    strHeader = Path.GetFileNameWithoutExtension(strDocumentPath);
                                    strHeader = strHeader.Replace("-", " ");

                                    string strDownloadName = System.Text.RegularExpressions.Regex.Replace(strHeader, @"\s+", " ");
                                    strDownloadName = strDownloadName.Replace(" ", "-");

                                    litPortalHeading.Text = clsPortalFeatures.strTitle;

                                    strbPortal.AppendLine("<div style='width:100%;'>");

                                    if (strDocumentPath.Contains(".pdf"))
                                    {
                                        if ((strDocumentPath != null) && (strDocumentPath != ""))
                                        {
                                            strbPortal.AppendLine("<div class='span6' style='color:#000; text-align: left; float: left; margin-top: -20px;'>" + strHeader + "</div>");
                                            strbPortal.AppendLine("<div class='span3' style='color:#000; text-align: left; float: left; margin-top: -20px; margin-left: 15px;' ><a style='color:#000;' href='" + strDocumentPath + "' target='_blank'>");
                                            strbPortal.AppendLine("Click to open");
                                            strbPortal.AppendLine("</a>");
                                        }
                                    }
                                    else if (strDocumentPath.Contains(".zip"))
                                    {
                                        if ((strDocumentPath != null) && (strDocumentPath != ""))
                                        {
                                            strbPortal.AppendLine("<div class='span6' style='color:#000; text-align: left; float: left; margin-top: -20px;'>" + strHeader + "</div>");
                                            strbPortal.AppendLine("<div class='span3' style='color:#000; text-align: left; float: left; margin-top: -20px; margin-left:15px;'><a style='color:#000;' href='" + strDocumentPath + "' target='_blank'>");
                                            strbPortal.AppendLine("Click to open");
                                            strbPortal.AppendLine("</a>");
                                        }
                                    }
                                    else
                                    {
                                        if ((strDocumentPath != null) && (strDocumentPath != ""))
                                        {
                                            strbPortal.AppendLine("<div class='span6' style='color:#000; text-align: left; float: left; margin-top: -20px;'>" + strHeader + "</div>");
                                            strbPortal.AppendLine("<div class='span3' style='color:#000; text-align: left; float: left; margin-top: -20px; margin-left:15px;'><a style='color:#000;' href='" + strDocumentPath + "' target='_blank'>");
                                            strbPortal.AppendLine("Click to open");
                                            strbPortal.AppendLine("</a>");
                                        }
                                    }

                                    if ((clsPortalFeatures.strYouTubeLink != null) && (clsPortalFeatures.strYouTubeLink != ""))
                                    {
                                        strbPortal.AppendLine("<br class='clr'/>");
                                        strbPortal.AppendLine("<br class='clr'/>");
                                        strbPortal.AppendLine("<div style='margin:0px;'><iframe width='100%' height='360' src='" + strYouTubeLink + "' frameborder='0' allowfullscreen></iframe></div>");
                                        strbPortal.AppendLine("<div class='span3' style=' text-align: left; float: left; margin-top: -20px;'>" + strHeader + "</div>");
                                        strbPortal.AppendLine("<div class='span3' style='text-align: left; float: left; margin-top: -20px;'><a style='color:#000;' href='" + strDocumentPath + "' target='_blank'>");
                                    }
                                    strbPortal.AppendLine("</div>");
                                    strbPortal.AppendLine("</li>");

                                    strbPortal.AppendLine("<br class='clr'/>");
                                }
                            }
                        //}
                    }

                    else
                    {
                        strTitle = clsPortalFeatures.strTitle;
                        strYouTubeLink = clsPortalFeatures.strYouTubeLink.ToString();
                        strDescription = clsPortalFeatures.strDescription;
                        strIconUrl = "../images/imgPdf.png";
                        strDocumentName = "";

                        strDocumentPath = AppDomain.CurrentDomain.BaseDirectory + "\\Documents\\" + clsPortalFeatures.strPathToDocument;
                        strDocumentPath = strDocumentPath.Replace("\\", "/");

                        //## Checks if the document path is empty
                        if (!string.IsNullOrEmpty(clsPortalFeatures.strPathToDocument))
                        {
                            if (!string.IsNullOrEmpty(clsPortalFeatures.strMasterDocument))
                            {
                                files = Directory.GetFiles(strDocumentPath);
                                if (files.Count() > 0)
                                {
                                    for (int i = 0; i < files.Count(); i++)
                                    {
                                        if (files[i].Contains("pdf") || files[i].Contains("doc") || files[i].Contains("xls") || files[i].Contains("ppt"))
                                        {
                                            strDocumentName = files[i];
                                        }
                                    }
                                    strDocumentName = strDocumentName.Replace("/", "\\");
                                    strDocumentPath = strDocumentName.Replace(AppDomain.CurrentDomain.BaseDirectory + "\\", ConfigurationManager.AppSettings["WebRoot"]);
                                    strDocumentPath = strDocumentPath.Replace("Accelerate/", "");
                                }
                            }
                        }
                        else
                        {
                            strIconUrl = "";
                            strDocumentPath = "";
                        }

                        strDocumentPath = strDocumentPath.Replace("\\", "/");

                        if (strDocumentPath.Contains(".pdf"))
                        {
                            strIconUrl = "../images/imgPdf.png";
                        }
                        else if (strDocumentPath.Contains(".doc") || strDocumentPath.Contains(".docx"))
                        {
                            strIconUrl = "../images/imgWord.png";
                        }
                        else if (strDocumentPath.Contains(".xls") || strDocumentPath.Contains(".xlsx"))
                        {
                            strIconUrl = "../images/imgExcel.png";
                        }
                        else if (strDocumentPath.Contains(".ppt") || strDocumentPath.Contains(".pptx"))
                        {
                            strIconUrl = "../images/imgPowerPoint.png";
                        }

                        strbPortal = new StringBuilder();

                        litPortalHeading.Text = clsPortalFeatures.strTitle;
                        
                        strbPortal.AppendLine("<br class='clr'/>");
                        strbPortal.AppendLine(strDescription);

                        //if ((strDocumentPath != null) && (strDocumentPath != ""))
                        {
                            strbPortal.AppendLine("<br class='clr'/>");
                            strbPortal.AppendLine("<br class='clr'/>");
                            strbPortal.AppendLine("<a href='" + strDocumentPath + "' target='_blank'><img src='" + strIconUrl + "' style='margin: 0px 12px; color:#000;' /><br/>");
                            strbPortal.AppendLine("Click to open");
                            strbPortal.AppendLine("</a>");
                            strbPortal.AppendLine("<br class='clr'/>");
                            strbPortal.AppendLine("<br class='clr'/>");
                        }

                        if ((clsPortalFeatures.strYouTubeLink != null) && (clsPortalFeatures.strYouTubeLink != ""))
                        {
                            strbPortal.AppendLine("<br class='clr'/>");
                            strbPortal.AppendLine("<br class='clr'/>");
                            strbPortal.AppendLine("<div style='margin:0px;'><iframe width='100%' height='360' src='" + strYouTubeLink + "' frameborder='0' allowfullscreen></iframe></div>");
                        }

                        litPortalFeatures.Text = strbPortal.ToString();
                    }
                }
            }
            else
            {
                strIconUrl = "";
                strDocumentPath = "";
            }
            strbPortal.AppendLine("</ul>");
            litPortalFeatures.Text = strbPortal.ToString();
        }

    }

    #endregion
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable dtFeaturedUser = clsFeatureUsers.GetPortalFeaturesList("iAccountUserID = " + clsAccountUsers.iAccountUserID + " AND + iFeatureID = " + clsPortalFeatures.iPortalFeatureID, "");
            if (dtFeaturedUser.Rows.Count > 0)
            {
                foreach (DataRow dr in dtFeaturedUser.Rows)
                {
                    string strCheckboxes = lblCheckboxes.Value.ToString();
                    clsFeatureUsers clsFeaturedUser = new clsFeatureUsers(Convert.ToInt32(dr["iFeatureUserID"]));
                    clsFeaturedUser.dtEdited = DateTime.Now;
                    clsFeaturedUser.iEditedBy = clsAccountUsers.iAccountUserID;
                    clsFeaturedUser.strFeatureChecklist = strCheckboxes;
                    clsFeaturedUser.Update();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "loopFormCheck", "loopFormCheck();", true);
                }
            }
            else
            {
                string strCheckboxes = lblCheckboxes.Value.ToString();
                //List<string> lstCheckboxes = strCheckboxes.Split('|').ToList();
                clsFeatureUsers clsFeaturedUser = new clsFeatureUsers();
                clsFeaturedUser.strFeatureChecklist = strCheckboxes;
                clsFeaturedUser.dtAdded = DateTime.Now;
                clsFeaturedUser.iAddedBy = clsAccountUsers.iAccountUserID;
                clsFeaturedUser.iAccountUserID = clsAccountUsers.iAccountUserID;
                clsFeaturedUser.iFeatureID = clsPortalFeatures.iPortalFeatureID;
                clsFeaturedUser.Update();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "loopFormCheck", "loopFormCheck();", true);
            }
        }
        catch(Exception ex)
        {
            Response.Redirect("Accelerate-Error404.aspx");
        }
        
    }
}
