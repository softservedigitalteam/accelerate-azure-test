﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using SD = System.Drawing;
using System.Drawing.Drawing2D;
using System.Net.Mail;
using System.DirectoryServices;

public partial class Accelerate_My_Profile : System.Web.UI.Page
{
    clsUsers clsUsers;
    clsAccountUsers clsAccountUsers;

    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsAccountUsers"] == null)
        {
            //### Redirect back to login
            Response.Redirect("Accelerate-Login.aspx");
        }
        clsAccountUsers = (clsAccountUsers)Session["clsAccountUsers"];

        //popProfile(clsAccountUsers.iCurrentPoints);

        //### Determines if a javascript delete has been called
        if (!String.IsNullOrEmpty(Page.Request["__EVENTARGUMENT"]) && (Page.Request["__EVENTARGUMENT"].Split(':')[0] == "iRemoveImages"))
            DeleteImages(Convert.ToInt32(Page.Request["__EVENTARGUMENT"].Split(':')[1]));

        if (!IsPostBack)
        {
            //popGroup();
            //popTitle();

            //clsAccountUsers = new clsAccountUsers(Convert.ToInt32(Request.QueryString["iAccountUserID"]));

            //### Populate the form
            try
            {
                popFormData();
            }
            catch(Exception ex)
            {
                Response.Redirect("Accelerate-Error404.aspx");
            }
            //lstTitle.Enabled = false;

            //### If the iAccountUserID is passed through then we want to instantiate the object with that iAccountUserID
            //if ((Request.QueryString["iAccountUserID"] != "") && (Request.QueryString["iAccountUserID"] != null))
            //{
            //    clsAccountUsers = new clsAccountUsers(Convert.ToInt32(Request.QueryString["iAccountUserID"]));
            //    //### Populate the form
            //    lstTitle.Enabled = false;
            //}

        }
        else
        {
            if (IsPostBack && FileUpload.PostedFile != null)
            {
                if (FileUpload.PostedFile.FileName.Length > 0)
                {
                    if (Session["lstImages"] == null)
                    {
                        lstImages = new List<string>();
                        Session["lstImages"] = lstImages;
                    }
                    //### Check that they have ONLY HAVE MAX NUMBER OF Images in the datalist
                    if (dlImages.Items.Count == iMaxImages)
                    {
                        mandatoryDiv.Visible = true;
                        lblValidationMessage.Text = "You can only have " + iMaxImages.ToString() + " Images.";
                        getList(lblUniquePath.Text);
                    }
                    else
                    {
                        mandatoryDiv.Visible = false;

                        string strUniquePath;
                        if (lblUniquePath.Text == "")
                        {
                            strUniquePath = GetUniquePath();
                            lblUniquePath.Text = strUniquePath;
                        }
                        else
                        {
                            strUniquePath = lblUniquePath.Text;
                        }
                        UploadImages(strUniquePath);
                        getList(strUniquePath);
                    }
                }
            }
            clsAccountUsers = (clsAccountUsers)Session["clsAccountUsers"];
        }
    }


    protected void btnSave_Click(object sender, EventArgs e)
    {
        //### Validate registration process
        bool bCanSave = true;

        bCanSave = clsValidation.IsNullOrEmpty(txtFirstName, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtSurname, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtEmailAddress, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtPhoneNumber, bCanSave);

        if (bCanSave == true)
        {
            mandatoryDiv.Visible = false;
            try
            {
                SaveData();
            }
            catch(Exception ex)
            {
                Response.Redirect("Accelerate-Error404.aspx");
            }
            
        }
        else
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><div class=\"validationMessage\">Please fill out all the fields</div></div>";
        }
        //### Validate registration process
    }

    protected void lnkbtnClear_Click(object sender, EventArgs e)
    {
        mandatoryDiv.Visible = false;

        //lstTitle.SelectedValue = "0";
        //clsValidation.SetValid(lstTitle);
        txtFirstName.Text = "";
        clsValidation.SetValid(txtFirstName);
        txtSurname.Text = "";
        clsValidation.SetValid(txtSurname);
        txtEmailAddress.Text = "";
        clsValidation.SetValid(txtEmailAddress);
        txtPhoneNumber.Text = "";
        clsValidation.SetValid(txtPhoneNumber);
        //lstGroup.SelectedValue = "0";
        //clsValidation.SetValid(lstGroup);
    }

    #endregion

    #region POPULATE DATA METHODS

    private void popFormData()
    {
        txtFirstName.Text = clsAccountUsers.strFirstName;
        txtSurname.Text = clsAccountUsers.strSurname;
        txtEmailAddress.Text = clsAccountUsers.strEmailAddress;
        txtPhoneNumber.Text = clsAccountUsers.strPhoneNumber;
        //lstGroup.SelectedValue = clsAccountUsers.iGroupID.ToString();

        //### Populates images
        if (!string.IsNullOrEmpty(clsAccountUsers.strPathToImages))
        {
            lblUniquePath.Text = clsAccountUsers.strPathToImages;
            getList(clsAccountUsers.strPathToImages);
            //### Set Current Master Image
            List<string> lstImagesFileNames = (List<string>)Session["lstImagesFileNames"];
            try
            {
                foreach (string strImageFileName in lstImagesFileNames)
                {
                    if (strImageFileName == clsAccountUsers.strMasterImage)
                    {
                        RadioButton rdbMasterImage = dlImages.Items[lstImagesFileNames.IndexOf(strImageFileName)].FindControl("rdbMainImage") as RadioButton;
                        rdbMasterImage.Checked = true;
                        break;
                    }
                }
            }
            catch { }
        }
    }

    //private void popTitle()
    //{
    //    DataTable dtTitlesList = new DataTable();
    //    lstTitle.DataSource = clsMissions.GetMissionsList();

    //    //### Populates the drop down list with PK and TITLE;
    //    lstTitle.DataValueField = "iMissionID";
    //    lstTitle.DataTextField = "strTitle";

    //    //### Bind the data to the list;
    //    lstTitle.DataBind();

    //    //### Add default select option;
    //    lstTitle.Items.Insert(0, new ListItem("--Not Selected--", "0"));
    //}

    //private void popGroup()
    //{
    //    DataTable dtGroupsList = new DataTable();
    //    lstGroup.DataSource = clsGroups.GetGroupsList();

    //    //### Populates the drop down list with PK and TITLE;
    //    lstGroup.DataValueField = "iGroupID";
    //    lstGroup.DataTextField = "strTitle";

    //    //### Bind the data to the list;
    //    lstGroup.DataBind();

    //    //### Add default select option;
    //    lstGroup.Items.Insert(0, new ListItem("--Not Selected--", "0"));
    //}

    #endregion

    #region ACTION METHODS

    protected int iRankID(int iCurrentPoints)
    {
        DataTable dtRank = clsRanks.GetRanksList("", "");

        int iRankID = 1;
        int iPointLevel;

        int iCount = 0;

        foreach (DataRow dtrRank in dtRank.Rows)
        {
            ++iCount;

            iRankID = Convert.ToInt32(dtrRank["iRankID"].ToString());
            iPointLevel = Convert.ToInt32(dtrRank["iPointLevel"].ToString());

            if (iCurrentPoints < iPointLevel)
            {
                break;
            }
        }

        return iRankID;
    }

    protected int iRankCount()
    {
        DataTable dtRank = clsRanks.GetRanksList();

        int iRowCount = dtRank.Rows.Count;

        return iRowCount;
    }

    protected int iCurrentPercentage(int iCurrentPoints, int iTotalPoints)
    {
        double dblCurrentPercentage = Convert.ToDouble(iCurrentPoints) / Convert.ToDouble(iTotalPoints);

        int iCurrentPercentage = Convert.ToInt32(dblCurrentPercentage * 200);

        return iCurrentPercentage;
    }

    //### Returns Answers Completed Percentage for current mission
    protected int iCurrentPercentage(int iCurrentMissionID)
    {
        DataTable dtMissionQuestions = clsMissionQuestions.GetMissionQuestionsList("iMissionID=" + iCurrentMissionID, "");
        int iTotalQuestionCount = dtMissionQuestions.Rows.Count;

        DataTable dtCompletedAnswersToQuestions = clsUserAnswers.GetUserAnswersList("iMissionID=" + iCurrentMissionID + " AND iAccountUserID=" + clsAccountUsers.iAccountUserID, "");
        int iTotalAnsweredCount = dtCompletedAnswersToQuestions.Rows.Count;
        int iCurrentPercentage = 0;

        //### Check if undefined
        if ((iTotalQuestionCount != 0))
        {
            double dblCurrentPercentage = Convert.ToDouble(iTotalAnsweredCount) / Convert.ToDouble(iTotalQuestionCount);
            iCurrentPercentage = Convert.ToInt32(dblCurrentPercentage * 200);
        }

        return iCurrentPercentage;
    }

    //### Returns Answers Completed Percentage for Teamship mission
    protected int iCurrentPercentageTeamship(int iCurrentMissionID)
    {
        DataTable dtMissionQuestions = clsTeamShips.GetTeamShipsList("iMissionID=" + iCurrentMissionID, "");
        int iTotalQuestionCount = dtMissionQuestions.Rows.Count;

        DataTable dtCompletedAnswersToQuestions = clsTeamShipAnswers.GetTeamShipAnswersList("iMissionID=" + iCurrentMissionID + " AND iAddedby=" + clsAccountUsers.iAccountUserID, "");
        int iTotalAnsweredCount = dtCompletedAnswersToQuestions.Rows.Count;
        int iCurrentPercentage = 0;

        //### Check if undefined
        if ((iTotalQuestionCount != 0))
        {
            double dblCurrentPercentage = Convert.ToDouble(iTotalAnsweredCount) / Convert.ToDouble(iTotalQuestionCount);
            iCurrentPercentage = Convert.ToInt32(dblCurrentPercentage * 200);
        }

        return iCurrentPercentage;
    }
    #endregion

    #region SAVE DATA METHODS

    private void SaveData()
    {
        //### Add / Update
        //clsAccountUsers clsCurrentsAccountUsers = new clsAccountUsers(clsAccountUsers.iAccountUserID);
        clsAccountUsers.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsAccountUsers.iEditedBy = clsAccountUsers.iAccountUserID;
        clsAccountUsers.strFirstName = txtFirstName.Text;
        clsAccountUsers.strSurname = txtSurname.Text;
        clsAccountUsers.strPhoneNumber = txtPhoneNumber.Text;
        clsAccountUsers.strEmailAddress = txtEmailAddress.Text;
        
        //clsAccountUsers.iGroupID = Convert.ToInt32(lstGroup.SelectedValue.ToString());

        //### Images related items
        clsAccountUsers.strPathToImages = lblUniquePath.Text;
        clsAccountUsers.strMasterImage = "crop_" + GetMainImagePath(dlImages);

        //string strFullPathToImage = clsAccountUsers.strPathToImages + clsAccountUsers.strMasterImage;  // WHY??

        clsAccountUsers.Update();

        Session["clsAccountUsers"] = clsAccountUsers;
        Session["dtAccountUsersList"] = null;

        //### Go back to view page
        Response.Redirect("Accelerate-Home.aspx");
    }

    #endregion

    #region IMAGE METHODS

    List<string> lstImages;
    List<string> lstImagesFileNames;
    int iMaxImages = 1;
    string strUniqueFullPath = AppDomain.CurrentDomain.BaseDirectory + "\\AccountUsers";

    protected void btnUpload_Click(object sender, EventArgs e)
    {
        if (Session["lstImages"] == null)
        {
            lstImages = new List<string>();
            Session["lstImages"] = lstImages;
        }
        if (dlImages.Items.Count == iMaxImages)
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "You can only have " + iMaxImages.ToString() + " Images.";
            getList(lblUniquePath.Text);
        }
        else
        {
            mandatoryDiv.Visible = false;

            string strUniquePath;
            if (lblUniquePath.Text == "")
            {
                strUniquePath = GetUniquePath();
                lblUniquePath.Text = strUniquePath;
            }
            else
            {
                strUniquePath = lblUniquePath.Text;
            }
            UploadImages(strUniquePath);
            getList(strUniquePath);
        }
    }

    protected void btnCrop_Click(object sender, EventArgs e)
    {
        string strImageName = Session["WorkingImage"].ToString();

        //### Validate a cropped area has been selected
        if (W.Value != "" && W.Value != "0" && H.Value != "" && H.Value != "0" && X.Value != "" && Y.Value != "")
        {
            int w = Convert.ToInt32(W.Value);
            int h = Convert.ToInt32(H.Value);
            int x = Convert.ToInt32(X.Value);
            int y = Convert.ToInt32(Y.Value);

            byte[] CropImage = Crop(strUniqueFullPath + "\\" + lblUniquePath.Text + "\\" + strImageName, w, h, x, y);

            using (MemoryStream ms = new MemoryStream(CropImage, 0, CropImage.Length))
            {
                ms.Write(CropImage, 0, CropImage.Length);
                using (SD.Image CroppedImage = SD.Image.FromStream(ms, true))
                {
                    string SaveTo = strUniqueFullPath + "\\" + lblUniquePath.Text + "\\" + "crop_" + strImageName;
                    CroppedImage.Save(SaveTo, CroppedImage.RawFormat);
                }
            }

            CopyAndResizePic(strUniqueFullPath + "\\" + lblUniquePath.Text + "\\" + strImageName);
            getList(lblUniquePath.Text);
        }
        else
        {
            //### Show message
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "Select an area you wish to crop.";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "jCropUnique", "jCrop();", true);
        }
    }

    static byte[] Crop(string Img, int Width, int Height, int X, int Y)
    {
        try
        {
            using (SD.Image OriginalImage = SD.Image.FromFile(Img))
            {
                using (SD.Bitmap bmp = new SD.Bitmap(Width, Height))
                {
                    bmp.SetResolution(OriginalImage.HorizontalResolution, OriginalImage.VerticalResolution);

                    using (SD.Graphics Graphic = SD.Graphics.FromImage(bmp))
                    {
                        Graphic.SmoothingMode = SmoothingMode.AntiAlias;
                        Graphic.InterpolationMode = InterpolationMode.HighQualityBicubic;
                        Graphic.PixelOffsetMode = PixelOffsetMode.HighQuality;
                        Graphic.DrawImage(OriginalImage, new SD.Rectangle(0, 0, Width, Height), X, Y, Width, Height, SD.GraphicsUnit.Pixel);

                        MemoryStream ms = new MemoryStream();
                        bmp.Save(ms, OriginalImage.RawFormat);

                        return ms.GetBuffer();
                    }
                }
            }
        }
        catch (Exception Ex)
        {
            throw (Ex);
        }
    }

    private string GetUniquePath()
    {
        int iCount = 1;
        //### First we need to get the path
        while (System.IO.Directory.Exists(strUniqueFullPath + "\\AccountUsers" + iCount) == true)
        {
            iCount++;
        }
        return "AccountUsers" + iCount;
    }

    protected void UploadImages(String strUniquePath)
    {
        if (FileUpload.PostedFile.ContentLength > 0 && FileUpload.PostedFile.ContentLength < 1073741824)
        {
            //### Upload files to unique folder
            string strUploadFileName = "";
            strUploadFileName = System.IO.Path.GetFileName(FileUpload.PostedFile.FileName);
            string strSaveLocation = "";
            strSaveLocation = strUniqueFullPath + "\\" + strUniquePath + "\\" + strUploadFileName;

            if (!System.IO.Directory.Exists(strUniqueFullPath + "\\" + strUniquePath))
            {
                System.IO.Directory.CreateDirectory(strUniqueFullPath + "\\" + strUniquePath);
            }
            FileUpload.PostedFile.SaveAs(strSaveLocation);

            Session["WorkingImage"] = FileUpload.FileName;

            clsAccountUsers.strPathToImages = strUniquePath;

            clsCommonFunctions.ResizeImage(strUniqueFullPath + "\\" + lblUniquePath.Text + "\\" + Session["WorkingImage"], strUniqueFullPath + "\\" + lblUniquePath.Text + "\\" +
                Session["WorkingImage"], 620, 620, true);

            imgCrop.ImageUrl = "../AccountUsers" + "/" + strUniquePath + "/" + strUploadFileName;
            ModalPopupExtenderCrop.Show();
            Session["WorkingImage"] = FileUpload.FileName;
        }
        else
        {
            lblValidationMessage.Text = "The file should be between 0 and 1Mb.";
            //lblUploadError.Text = "The file should be between 0 and 1Mb.";
        }
    }

    private void CopyAndResizePic(String strFullPath)
    {
        try
        {
            String strFileName;
            String strNewFilePath;

            //### Main Images
            String strLrgFileName;

            strFileName = Path.GetFileName(strFullPath);
            strNewFilePath = strFullPath.Replace(strFileName, "");
            strNewFilePath = strNewFilePath + Path.GetFileNameWithoutExtension(strFullPath);
            strNewFilePath = strNewFilePath + "_lrg";
            strNewFilePath = strNewFilePath + Path.GetExtension(strFullPath);

            File.Copy(strFullPath, strNewFilePath);
            strLrgFileName = Path.GetFileName(strNewFilePath);

            clsCommonFunctions.ResizeImage(strNewFilePath, strNewFilePath, 600, 600, false);

            //### Thumbnail
            String strSmlFileName;

            strFileName = Path.GetFileName(strFullPath);
            strNewFilePath = strFullPath.Replace(strFileName, "");
            strNewFilePath = strNewFilePath + Path.GetFileNameWithoutExtension(strFullPath);
            strNewFilePath = strNewFilePath + "_sml";
            strNewFilePath = strNewFilePath + Path.GetExtension(strFullPath);

            File.Copy(strFullPath.Replace(strFileName, "crop_") + strFileName, strNewFilePath);
            strSmlFileName = Path.GetFileName(strNewFilePath);

            clsCommonFunctions.ResizeImage(strNewFilePath, strNewFilePath, 134, 134, false);

        }
        catch (Exception ex) { }
    }

    public void getList(String strPathToFolder)
    {
        lstImages = new List<string>();
        lstImagesFileNames = new List<string>();
        try
        {
            string strPath = strPathToFolder;
            string[] files = Directory.GetFiles(strUniqueFullPath + "\\" + strPath);

            string iAccountUserID = "";
            if (!string.IsNullOrEmpty(Request.QueryString["iAccountUserID"]))
                iAccountUserID = Request.QueryString["iAccountUserID"];

            int iImagesCount = 0;

            foreach (string strName in files)
            {
                if (strName.IndexOf("_sml") != -1)
                {
                    string strHTMLImages = strName.Replace(AppDomain.CurrentDomain.BaseDirectory + "\\", "..\\");
                    strHTMLImages = strHTMLImages.Replace("\\", "/");

                    //### Generates a javascript postback for the delete method
                    String strPostBack = Page.ClientScript.GetPostBackEventReference(this, "iRemoveImages:" + iImagesCount);

                    lstImages.Add(@"<tr style='text-align: centre;'>
                                        <td style='text-align:centre;'>
                                           <center><img src='" + strHTMLImages + @"' alt='' title='' style='border: solid 5px #ffffff;' /><br class='clr'/></center>" +
                                             "<center><div class='Green3 btn' style='padding-top: 3px;'><a href=\"javascript:" + strPostBack + "\" style='color: #000;font-size:12px;'><div class='deleteButton' style='height:20px; width:100px;'>Delete</div></div></center>" + @"
                                        </td>
                                    </tr>");
                    lstImagesFileNames.Add(Path.GetFileName(strName).Replace("_sml", ""));
                    iImagesCount++;
                }
            }
            dlImages.DataSource = lstImages;
            dlImages.DataBind();

            Session["lstImages"] = lstImages;
            Session["lstImagesFileNames"] = lstImagesFileNames;
        }
        catch (Exception ex) { }
    }

    private string GetMainImagePath(DataList dtlTarget)
    {
        string strReturn = "";

        foreach (DataListItem dliTarget in dtlTarget.Items)
        {
            RadioButton rdbMainImage = (RadioButton)dliTarget.FindControl("rdbMainImage");
            if (rdbMainImage.Checked)
            {
                lstImagesFileNames = (List<string>)Session["lstImagesFileNames"];
                strReturn = lstImagesFileNames[dliTarget.ItemIndex];
                break;
            }
        }
        return strReturn;
    }

    private void DeleteImages(int iImagesIndex)
    {
        //### Deletes all Images related to the target Images.
        //lstImages = (List<string>)Session["lstImages"];
        lstImagesFileNames = (List<string>)Session["lstImagesFileNames"];

        //lstImages.RemoveAt(iImagesIndex);
        //Session["lstImages"] = lstImages;

        string[] files = Directory.GetFiles(strUniqueFullPath + "\\" + lblUniquePath.Text);
        foreach (string file in files)
        {
            if (Path.GetFileName(file) == lstImagesFileNames[iImagesIndex].ToString())
            {
                //### Remove all Images
                File.Delete(file);
                File.Delete(file.Replace(Path.GetExtension(file), "_sml" + Path.GetExtension(file)));
                File.Delete(file.Replace(Path.GetExtension(file), "_med" + Path.GetExtension(file)));
                File.Delete(file.Replace(Path.GetExtension(file), "_lrg" + Path.GetExtension(file)));
                break;
            }
        }
        lstImagesFileNames.RemoveAt(iImagesIndex);
        ViewState["lstImagesFileNames"] = lstImagesFileNames;
        getList(lblUniquePath.Text);

        if (lstImagesFileNames.Count == 0)
        {
            clsAccountUsers.iEditedBy = clsAccountUsers.iAccountUserID;
            clsAccountUsers.dtEdited = Convert.ToDateTime(DateTime.Now);
            clsAccountUsers.strMasterImage = "";
            clsAccountUsers.Update();
            Session["clsAccountUsers"] = clsAccountUsers;

            //popProfile(clsAccountUsers.iCurrentPoints);
        }
    }

    #endregion

    //#region SINGLE SIGN ON

    ///// <summary>
    ///// Utility function to string away the domain name E.G: zadeloitte\jsmith => jsmith
    ///// </summary>
    ///// <param name="username">Full username with domain</param>
    ///// <returns></returns>
    //private string ExtractUserName(string username)
    //{
    //    //username will be passed with the escape character \. example zadeloitte\\jsmith
    //    string[] userPath = username.Split(new char[] { '\\' });
    //    return userPath[userPath.Length - 1];
    //}

    ///// <summary>
    ///// Check if the person using the browser is in AD
    ///// </summary>
    ///// <param name="username">the name of the logged/browser user</param>
    ///// <param name="err">Populates the error reference variable with the error message</param>
    ///// <returns>Returns a string with all the required pieces of information</returns>
    //private string isUserInAD(string username, ref string err)
    //{
    //    //put your fully qualified Domain name here
    //    string strLDAPPath = "LDAP://zajnb0000.za.deloitte.com/DC=za,DC=deloitte,DC=com";
    //    string result = string.Empty;
    //    SearchResult srResult = null;

    //    try
    //    {
    //        //initiate AD connection on the supplied Domain connection string
    //        DirectoryEntry de = new DirectoryEntry(strLDAPPath); //Adds the domain path as a directory entry

    //        //initiate the AD search object and add items to be returned
    //        DirectorySearcher ds = new DirectorySearcher(de); // Creates a new DirectorySearch instance with the path given by de

    //        //filter the AD object tree by searching only the USERS/PERSON classes
    //        ds.Filter = "(&(objectClass=user)(objectCategory=person)(sAMAccountName=" + ExtractUserName(username) + "))"; // Filters through the DirectorySearch instance with the username given checking for a match

    //        //If the value is found the fields below are used to retrieve certain information about the user.

    //        ds.PropertiesToLoad.Add("samaccountname");
    //        ds.PropertiesToLoad.Add("givenname"); //This is an optional Line and is only used as a filter for the current search result.
    //        ds.PropertiesToLoad.Add("mail"); //This is an optional Line and is only used as a filter for the current search result.
    //        ds.PropertiesToLoad.Add("telephoneNumber"); //This is an optional Line and is only used as a filter for the current search result.
    //        ds.PropertiesToLoad.Add("mobile"); //This is an optional Line and is only used as a filter for the current search result.
    //        ds.PropertiesToLoad.Add("sn"); //This is an optional Line and is only used as a filter for the current search result.

    //        //check if user is in AD and return just one record even if there is multiple matches, just one
    //        srResult = ds.FindOne();

    //        //if user has been found, get the attributes/data from object
    //        //if the user is not in AD, the result will be NULL
    //        if (srResult != null)
    //        {
    //            string samaccountname = (string)srResult.Properties["samaccountname"][0];
    //            string givenname = (string)srResult.Properties["givenname"][0];
    //            string email = (string)srResult.Properties["mail"][0];
    //            string phone = (string)srResult.Properties["telephoneNumber"][0];
    //            string mobile = (string)srResult.Properties["mobile"][0];
    //            string surname = (string)srResult.Properties["sn"][0];

    //            //return result and set error to null
    //            result = string.Format("Name : {0} <br/> Surname : {1} <br/> Username : {2} <br/>  Email : {3}<br/> Tel: {4} <br/> Cell: {5}", givenname, surname, samaccountname, email, phone, mobile);
    //            err = null;
    //        }
    //        else
    //        {
    //            result = "Invalid user - stop fooling around...";
    //            err = result;
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        err = ex.Message;
    //    }

    //    //return result to calling function
    //    return result;
    //}

    //#endregion
}