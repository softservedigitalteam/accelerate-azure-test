﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Accelerate_Completed : System.Web.UI.Page
{
    clsAccountUsers clsAccountUsers;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["clsAccountUsers"] == null)
        {
            //### Redirect back to login
            Response.Redirect("Accelerate-Login.aspx");
        }
        try
        {
            clsAccountUsers = (clsAccountUsers)Session["clsAccountUsers"];

            clsGroups clsGroup = new clsGroups(clsAccountUsers.iGroupID);
            popMessage(clsGroup);
        }
        catch(Exception ex)
        {
            Response.Redirect("Accelerate-Error404.aspx");
        }

        //string strPath = AppDomain.CurrentDomain.BaseDirectory + "\\PDF\\CompleteCertificate\\" + clsAccountUsers.iAccountUserID.ToString() + "-" + clsAccountUsers.strSurname + "-" + clsAccountUsers.strFirstName + ".pdf";
        //string pdfPath = "PDF/CompleteCertificate/" + clsAccountUsers.strSurname + "-" + clsAccountUsers.strFirstName + ".pdf";
        //if (File.Exists(strPath))
        //{
        //    if (strPath.Contains(".pdf"))
        //    {
        //        Response.Redirect("Accelerate-Certificate-Page.aspx");
        //    }
        //}
    }

    protected void btnSmallProceed_Click(object sender, EventArgs e)
    {
        //Response.Redirect("Accelerate-Home.aspx");
        Response.Redirect("Accelerate-Home.aspx");
    }

    protected void btnProceedLarge_Click(object sender, EventArgs e)
    {
        //Response.Redirect("Accelerate-Home.aspx");
        Response.Redirect("Accelerate-Home.aspx");
    }

    protected void popMessage(clsGroups clsGroup)
    {
        int iFinalMessageID = clsGroup.iFinalMessageID;

        clsFinalMessages clsFinalMessage = new clsFinalMessages(iFinalMessageID);

        StringBuilder strbImage = new StringBuilder();

        //strbImage.AppendLine("<img id='imgFinal' src='/FinalMessages/" + clsFinalMessage.strPathToImages + "/" + clsFinalMessage.strMasterImage + "'/>");

        strbImage.AppendLine("<style>body {");
        strbImage.AppendLine("background: url(img/imgCongratsPage.png) no-repeat center center fixed;");
        
        //strbImage.AppendLine("background: url(/FinalMessages/" + clsFinalMessage.strPathToImages + "/" + clsFinalMessage.strMasterImage + "') no-repeat center center fixed;");
        strbImage.AppendLine("-webkit-background-size: cover;");
        strbImage.AppendLine("-moz-background-size: cover;");
        strbImage.AppendLine("-o-background-size: cover;");
        strbImage.AppendLine("background-size: cover;");
        strbImage.AppendLine("}</style>");

        litImage.Text = strbImage.ToString();
    }
}