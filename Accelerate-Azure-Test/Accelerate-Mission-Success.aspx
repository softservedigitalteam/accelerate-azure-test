﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Accelerate.master" AutoEventWireup="true" Inherits="Accelerate_Success" Codebehind="Accelerate-Mission-Success.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <%--<div class="infobox">
        <div class="row-fluid">
            <div class="span2 PointsSection">
                        <div class="PointNumbersText">
                            <asp:Literal runat="server" ID="litPoints"></asp:Literal>
                        </div>
                        <br />
                        <br />
                        <div class="points-text-display">POINTS</div>
                    </div>
            <div class="span10">
                <div class="row-fluid">
                    <div class="showcaseSection Black">My showcase</div>
                    <asp:LinkButton runat="server" OnClick="btnPhase1_Click" ID="btnPhase1">
                        <div class="showcaseSection Blue1">
                            Phase 1:
                                    <asp:Literal runat="server" ID="litPhase1Text"></asp:Literal>
                        </div>
                    </asp:LinkButton>
                    <asp:LinkButton runat="server" ID="btnPhase2" OnClick="btnPhase2_Click" Enabled="false">
                        <div class="showcaseSection Blue2">
                            Phase 2:
                                    <asp:Literal runat="server" ID="litPhase2Text"></asp:Literal>
                        </div>
                    </asp:LinkButton>
                    <asp:LinkButton runat="server" ID="btnPhase3" OnClick="btnPhase3_Click" Enabled="false" Style="display: block;">
                        <div class="showcaseSection Blue3">
                            Phase 3:
                                    <asp:Literal runat="server" ID="litPhase3Text"></asp:Literal>
                        </div>
                    </asp:LinkButton>
                </div>
                <div class="well">
                    <asp:Repeater ID="rpAchievements" runat="server">
                        <ItemTemplate>
                            <%#Eval ("strNewRow") %>
                            <div class="achievement" style="text-align: center; margin: 0 1.2%;">
                                <a href='<%#Eval ("strLink") %>'>
                                    <img onerror="this.src='images/imgAchievementEmpty.png'" src='<%#Eval ("FullPathForImage") %>' title='<%#Eval ("strTitle") %>' alt='<%#Eval ("strTitle") %>' width='70%' /></a>
                                <br />
                                <%#Eval ("strTitle") %>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </div>
        </div>
    </div>
    </div>--%>

    <div class="infobox">
        <div class="well" style="color: #FFF;background: #00abb8;">
            <center>
                <asp:Literal ID="litCurrentMission" runat="server"></asp:Literal>
                <br /><br /><br />
            </center>
        </div>
    </div>

    <asp:Button ID="btnContinue" runat="server" Text="Continue" CssClass="Green3 btn" OnClick="btnContinue_Click" />
</asp:Content>

