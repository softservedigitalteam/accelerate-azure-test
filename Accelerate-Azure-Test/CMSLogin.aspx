﻿<%@ Page Title="CMS Login" Language="C#" AutoEventWireup="true" Inherits="CMS_frmCMSLogin" Codebehind="CMSLogin.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>CMS Login</title>

    <!--Stylesheet-->
    <link type="text/css" href="CMS/css/styles.css" rel="Stylesheet" />
   
    <link rel="shortcut icon" href="CMS/images/favicon.ico"/>

    <script language="javascript" type="text/javascript">

        function changeFocusStylingEmail(target) {
            document.getElementById("<%= divUsernameLeft.ClientID %>").className = 'roundedTextboxLeftSideWhite';
            document.getElementById("<%= divUsernameTxt.ClientID %>").className = 'roundedTextboxBackgroundDivWhite';
            document.getElementById("<%= divUsernameRight.ClientID %>").className = 'roundedTextboxRightSideWhite';
        }

        function clearStylingEmail(target) {
            document.getElementById("<%= divUsernameLeft.ClientID %>").className = 'roundedTextboxLeftSide';
            document.getElementById("<%= divUsernameTxt.ClientID %>").className = 'roundedTextboxBackgroundDiv';
            document.getElementById("<%= divUsernameRight.ClientID %>").className = 'roundedTextboxRightSide';
            if (target.value != '' || target.value != null) {
                var emailExpression = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
                if (target.value.length > 0) {
                    if (target.value.match(emailExpression)) {
                        document.getElementById("<%= divImageHolderLogin.ClientID %>").className = 'validationImageCorrectLogin';
                    }
                    else {
                        document.getElementById("<%= divImageHolderLogin.ClientID %>").className = 'validationImageIncorrectLogin';
                    }
                }
                else {
                    document.getElementById("<%= divImageHolderLogin.ClientID %>").className = 'validationImageIncorrectLogin';
                }
            }
            else {
                document.getElementById("<%= divImageHolderLogin.ClientID %>").className = 'validationImageIncorrectLogin';
            }
        }

        function changeFocusStylingPassword(target) {
            document.getElementById("<%= divPasswordLeft.ClientID %>").className = 'roundedTextboxLeftSideWhite';
            document.getElementById("<%= divPasswordTxt.ClientID %>").className = 'roundedTextboxBackgroundDivWhite';
            document.getElementById("<%= divPasswordRight.ClientID %>").className = 'roundedTextboxRightSideWhite';
        }

        function clearStylingPassword(target) {
            document.getElementById("<%= divPasswordLeft.ClientID %>").className = 'roundedTextboxLeftSide';
            document.getElementById("<%= divPasswordTxt.ClientID %>").className = 'roundedTextboxBackgroundDiv';
            document.getElementById("<%= divPasswordRight.ClientID %>").className = 'roundedTextboxRightSide';
        }
    
</script>
</head>
<body>
    <form id="form1" runat="server">

        <asp:ScriptManager ID="smgLogin" runat="server" ScriptMode="Release" />
        <center>
            <div class="loginContainerDiv">

                <!-- Left Nav -->
                <div class="loginLeftNav">
                    <img src="CMS/images/login/imgCMSBanner.png" alt="CMS Banner" title="CMS Banner" />
                    <img src="CMS/images/login/imgCMSBannerSide.png" alt="" title="" class="loginSideBannerImg" />
                    
                    <div class="loginFooterDiv">
                        <img src="CMS/images/login/imgLoginDivider.png" alt="" title="" />
                        <a href="http://www.softservedigital.co.za" target="_blank" >
                            <img src="CMS/images/login/imgPoweredBy.png" alt="Softserve Digital Development" title="Softserve Digital Development" class="loginFooterImg" />
                        </a>
                        <img src="CMS/images/login/imgLoginDivider.png" alt="" title="" />
                    </div>
                </div>

                <!-- Body -->
                <div class="loginBodyContent">
                    <img src="images/logo.png" alt="Accelerate :: Accelerate" title="Accelerate :: Accelerate" class="loginLogoImg"  width="300px" height="auto"/>
                    <br style="clear:both;" />
                    <h1 class="loginHeaderH1">LOGIN BELOW</h1>
                    <span>Please provide us with your email and password to sign in:</span>

                    <asp:UpdatePanel ID="udpLogin" runat="server" UpdateMode="Conditional" >
                        <ContentTemplate>                            
                            <asp:Panel ID="panelLogin" runat="server" DefaultButton="lnkbtnLogin">
                            <div class="controlDiv">
                                <div class="labelLoginDiv">Username:</div>
                                <div id="divImageHolderLogin" runat="server" class="divImageHolderLogin"></div>
                                <div class="fieldLoginDiv">
                                    <div class="roundedTextboxContainerDiv">
                                        <div id="divUsernameLeft" runat="server" class="roundedTextboxLeftSide">&nbsp;</div>
                                        <div id="divUsernameTxt" runat="server" class="roundedTextboxBackgroundDiv"><asp:TextBox ID="txtUsername" runat="server" onfocus="javascript:changeFocusStylingEmail(this)" onblur="javascript:clearStylingEmail(this)" CssClass="roundedTextbox"/></div>
                                        <div id="divUsernameRight" runat="server" class="roundedTextboxRightSide">&nbsp;</div>
                                    </div>
                                </div>
                                <br class="clearingSpacer" />
                            </div>

                            <div id="divPassword" runat="server" class="controlDiv">
                                <div class="labelLoginDiv">Password:</div>
                                <div class="divImageHolderLogin"></div>
                                <div class="fieldLoginDiv">
                                    <div class="roundedTextboxContainerDiv">
                                        <div id="divPasswordLeft" runat="server" class="roundedTextboxLeftSide">&nbsp;</div>
                                        <div id="divPasswordTxt" runat="server" class="roundedTextboxBackgroundDiv"><asp:TextBox ID="txtPassword" runat="server" onfocus="javascript:changeFocusStylingPassword(this)" onblur="javascript:clearStylingPassword(this)" CssClass="roundedTextboxPassword" TextMode="Password"/></div>
                                        <div id="divPasswordRight" runat="server" class="roundedTextboxRightSide">&nbsp;</div>
                                    </div>
                                </div>
                                <br class="clearingSpacer" />
                            </div>

                            <div class="loginButtonsRightDiv">
                                <asp:LinkButton ID="lnkbtnForgottenPassword" runat="server" CssClass="linkGreyUnderlined" Text="I forgot my password" OnClick="lnkbtnForgottenPassword_Click" />
                                <asp:LinkButton ID="lnkbtnBackToLogin" runat="server" CssClass="linkGreyUnderlined" Text="Back to login" OnClick="lnkbtnBackToLogin_Click" Visible="false" />
                                &nbsp;&nbsp;<img src="CMS/images/imgAnchorSeparator.png" alt="" title="" />&nbsp;&nbsp;
                                <a href="Accelerate-Home.aspx" target="_blank" class="linkGreyUnderlined">Visit website</a>
                            </div>

                            <img src="CMS/images/imgDivider.png" alt="" title="" />
                            
                            <div style="padding:10px;float:left;width:50%;">
                                <asp:Literal ID="litValidationMessage" runat="server" />
                            </div>

                            <div class="loginButtonsRightDiv">
                                <asp:LinkButton ID="lnkbtnLogin" runat="server" CssClass="loginButton" OnClick="lnkbtnLogin_Click" />
                                <asp:LinkButton ID="lnkbtnSubmit" runat="server" CssClass="submitButton" Visible="false" onClick="lnkbtnSubmit_Click" />
                            </div>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </center>
    </form>
</body>
</html>