
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text;

/// <summary>
/// Summary description for clsTeamShipsView
/// </summary>
public partial class CMS_clsTeamShipsView : System.Web.UI.Page
{
    #region CLASSES AND VARIABLES

    clsUsers clsUsers;
    DataTable dtTeamShipsList;

    List<clsTeamShips> glstTeamShips;

    #endregion

    #region EVENT MODULES

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

dgrGrid.ItemCreated += new DataGridItemEventHandler(clsCommonFunctions.dgrGrid_PagerPopulate);

        if (!Page.IsPostBack)
        {
            Session["dtTeamShipsList"] = null;
            PopulateFormData();
        }
        else
        {
            if (Session["glstTeamShips"] == null)
            {
                glstTeamShips = new List<clsTeamShips>();
                Session["glstTeamShips"] = glstTeamShips;
            }
            else
                glstTeamShips = (List<clsTeamShips>)Session["glstTeamShips"];
        }
            PopulateFormData();

    }

    protected string EscapeLikeValue(string valueWithoutWildcards)
    {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < valueWithoutWildcards.Length; i++)
        {
            char c = valueWithoutWildcards[i];
            if (c == '*' || c == '%' || c == '[' || c == ']')
                sb.Append("[").Append(c).Append("]");
            else if (c == '\'')
                sb.Append("''");
            else
                sb.Append(c);
        }
        return sb.ToString();
    }

    protected void lnkbtnSearch_Click(object sender, EventArgs e)
    {
        string EscapedString = EscapeLikeValue(txtSearch.Text.ToString());
        string FilterExpression ="(strName +' '+ strTitle +' '+ strMasterImage) LIKE '%" + EscapedString + "%'";
        DataTable dtTeamShipsList = clsTeamShips.GetTeamShipsList(FilterExpression, "");

        Session["dtTeamShipsList"] = dtTeamShipsList;

        PopulateFormData();
    }

    protected void lnkbtnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmTeamShipsAddModify.aspx");
    }

    #endregion

    #region DATA GRID METHODS

    private void PopulateFormData()
    {
        //### Populate datagrid using clsTeamShipsList object
        try
        {
            dtTeamShipsList = new DataTable();

            if (Session["dtTeamShipsList"] == null)
                dtTeamShipsList = clsTeamShips.GetTeamShipsList("", "dtAdded DESC");
            else
                dtTeamShipsList = (DataTable)Session["dtTeamShipsList"];

            dgrGrid.DataSource = dtTeamShipsList;

            //### Add this check to trap if the last item on the page is deleted
            if (Convert.ToInt16(dgrGrid.CurrentPageIndex) < Convert.ToInt16(Session["TeamShipsView_CurrentPageIndex"]))
            {
                dgrGrid.CurrentPageIndex = 0;
            }
            else
            {
                if (Session["TeamShipsView_CurrentPageIndex"] != null)
                {
                    dgrGrid.CurrentPageIndex = (int)Session["TeamShipsView_CurrentPageIndex"];
                }
            }

            dgrGrid.DataBind();

        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void lnkDeleteItem_Click(object sender, EventArgs e)
    {
        int iTeamShipID = int.Parse((sender as LinkButton).CommandArgument);

        clsTeamShips.Delete(iTeamShipID);

        DataTable dtTeamShipsList = new DataTable();

        if (((txtSearch.Text == "SEARCH FOR A TEAMSHIP") == false) && ((txtSearch.Text == "") == false))
        {
            string FilterExpression = "(strName +' '+ strTitle +' '+ strMasterImage) LIKE '%" + txtSearch.Text + "%'";
            dtTeamShipsList = clsTeamShips.GetTeamShipsList(FilterExpression, "");
        }
        else
        {
            dtTeamShipsList = clsTeamShips.GetTeamShipsList();
        }

        Session["dtTeamShipsList"] = dtTeamShipsList;

        PopulateFormData();
    }

    protected void dgrGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //### Method is used when the page index is changed.
        try
        {
            Session["TeamShipsView_CurrentPageIndex"] = e.NewPageIndex;
            dgrGrid.CurrentPageIndex = e.NewPageIndex;
            PopulateFormData();
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgrGrid_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        //### Sort Method
        dtTeamShipsList = new DataTable();

        if (Session["dtTeamShipsList"] == null)
            dtTeamShipsList = clsTeamShips.GetTeamShipsList();
        else
            dtTeamShipsList = (DataTable)Session["dtTeamShipsList"];

        DataView dvTemp = dtTeamShipsList.DefaultView;

        dvTemp.Sort = e.SortExpression;
        dtTeamShipsList = dvTemp.ToTable();
        Session["dtTeamShipsList"] = dtTeamShipsList;

        PopulateFormData();
    }

    public void dgrGrid_PopEditLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkEditItem = (HyperLink)sender;
            DataGridItem dgrItemEdit = (DataGridItem)lnkEditItem.Parent.Parent;

            //### Get the iTeamShipID
            int iTeamShipID = 0;
            iTeamShipID = int.Parse(dgrItemEdit.Cells[0].Text);

            //### Add attributes to edit link
            lnkEditItem.Attributes.Add("href", "frmTeamShipsAddModify.aspx?action=edit&iTeamShipID=" + iTeamShipID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

     public void dgrGrid_PopDeleteLink(object sender, EventArgs e)
     {
          try
          {
             //### Add hyperlink to datagrid item
             HyperLink lnkDeleteItem = (HyperLink)sender;
             DataGridItem dgrItemDelete = (DataGridItem)lnkDeleteItem.Parent.Parent;

             //### Get the iTeamShipID
             int iTeamShipID = 0;
             iTeamShipID = int.Parse(dgrItemDelete.Cells[0].Text);

             //### Add attributes to delete link
             lnkDeleteItem.CssClass = "dgrDeleteLink";
             lnkDeleteItem.Attributes.Add("href", "frmTeamShipsView.aspx?action=delete&iTeamShipID=" + iTeamShipID);
           }
           catch (Exception ex)
           {
                 Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
           }
     }

    #endregion

    #region TeamShips FILTERING

    /// <summary>
    /// Enables the AutoCompleteExtender
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    /// 
    [WebMethod]
    [ScriptMethod]
    public static string[] FilterBusinessRule(string prefixText, int count)
    {
        string[] strReturnList = new string[] { };

        string FilterExpression = "(strName +' '+ strTitle +' '+ strMasterImage) LIKE '%" + prefixText + "%'";
        DataTable dtTeamShips = clsTeamShips.GetTeamShipsList(FilterExpression, "");
        List<string> glstTeamShips = new List<string>();

        if (dtTeamShips.Rows.Count > 0)
        {
            foreach (DataRow dtrTeamShips in dtTeamShips.Rows)
            {
                glstTeamShips.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem("" + dtrTeamShips["strName"].ToString() +' '+"" + dtrTeamShips["strTitle"].ToString() +' '+"" + dtrTeamShips["strMasterImage"].ToString(), dtrTeamShips["iTeamShipID"].ToString()));
            }
        }
        else
            glstTeamShips.Add("No TeamShips Available.");
        strReturnList = glstTeamShips.ToArray();
        return strReturnList;
    }

    #endregion
}
