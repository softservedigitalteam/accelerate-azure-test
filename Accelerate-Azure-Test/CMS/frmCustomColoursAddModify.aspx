<%@ Page Title="CustomColours" Language="C#" MasterPageFile="~/CMS/CMS.master" AutoEventWireup="true" Inherits="CMS_frmCustomColoursAddModify" Codebehind="frmCustomColoursAddModify.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div id="mandatoryDiv" class="mandatoryInvalidDiv" runat="server" visible="false">
        <asp:Label ID="lblValidationMessage" runat="server"></asp:Label>
    </div>

    <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Title: (Please use one word titles)</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtTitle" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,50)" />
         </div>
         <br class="clearingSpacer" />
     </div>

    <div class="controlDiv" style="display:none;">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Additional2:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtLink" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,100)" />
         </div>
         <br class="clearingSpacer" />
     </div>

    <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">User Name:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtUserName" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

    <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Profile Header BG:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtProfileHeader" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

    <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Current Rank:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtCurrentRank" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

    <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Next Rank:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtNextRank" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

    <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Achievement Text:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtAchievementText" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

    <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Top Progress Bar:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtTopProgressBar" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Top Bar BG:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtTopBarBG" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Top Bar Text:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtTopBarText" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

    <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Top Bar Hover BG:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtTopBarHoverBG" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

    <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Top Bar Hover Text:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtTopBarHoverText" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Side Bar Headers:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtSideBarHeaders" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Side Bar Header Text:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtSideBarHeaderText" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Side Bar Sub Areas:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtSideBarSubAreas" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Side Bar Sub Area Text:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtSideBarSubAreaText" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

    <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Mission Header:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtMissionHeader" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

    <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Notification Area Border:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtNotificationAreaBorder" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Notification Header Text:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtNotificationHeaderText" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Notification Sub Text:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtNotificationSubText" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Achievement Rank Header:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtAchievementRankHeader" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

    <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Achievement Rank:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtAchievementRank" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Mission Progress Bar:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtMissionProgressBar" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Page Header Text:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtPageHeaderText" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Phase Header B G:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtPhaseHeaderBG" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

    <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Phase Header Text:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtPhaseHeaderText" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

    <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Email Header Banner:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtEmailHeaderBanner" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

    <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Congratulations Background:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtCongratulationsBackground" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

    <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Congratulations Achievement Image Container:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtCongratulationsAchievementImageContainer" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv" style="display:none;">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Additional3:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtAdditional3" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" />
         </div>
         <br class="clearingSpacer" />
     </div>


    
    <div class="Line"></div>
    <div class="buttonsRightDiv">
        <asp:LinkButton ID="lnkbtnBack" runat="server" CssClass="backButton" onclick="lnkbtnBack_Click" OnClientClick='history.go(-1);return false;' />
        <asp:LinkButton ID="lnkbtnSave" runat="server" CssClass="saveButton" onclick="lnkbtnSave_Click" />
        <asp:LinkButton ID="lnkbtnClear" runat="server" CssClass="clearButton" onclick="lnkbtnClear_Click" />
    </div>
</asp:Content>
