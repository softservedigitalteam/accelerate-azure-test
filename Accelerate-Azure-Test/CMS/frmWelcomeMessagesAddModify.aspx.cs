using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class CMS_frmWelcomeMessagesAddModify : System.Web.UI.Page
{
    clsUsers clsUsers;
    clsWelcomeMessages clsWelcomeMessages;
    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

             //### Determines if a javascript delete has been called
             if (!String.IsNullOrEmpty(Page.Request["__EVENTARGUMENT"]) && (Page.Request["__EVENTARGUMENT"].Split(':')[0] == "iRemoveImages"))
             DeleteImages(Convert.ToInt32(Page.Request["__EVENTARGUMENT"].Split(':')[1]));
        if (!IsPostBack)
        {

            //### If the iWelcomeMessageID is passed through then we want to instantiate the object with that iWelcomeMessageID
            if ((Request.QueryString["iWelcomeMessageID"] != "") && (Request.QueryString["iWelcomeMessageID"] != null))
            {
                clsWelcomeMessages = new clsWelcomeMessages(Convert.ToInt32(Request.QueryString["iWelcomeMessageID"]));

                //### Populate the form
                popFormData();
            }
            else
            {
                clsWelcomeMessages = new clsWelcomeMessages();
            }
            Session["clsWelcomeMessages"] = clsWelcomeMessages;
        }
        else
        {
            clsWelcomeMessages = (clsWelcomeMessages)Session["clsWelcomeMessages"];
        }
    }

    protected void lnkbtnBack_Click(object sender, EventArgs e)
    {
        //### Go back to previous page
        Response.Redirect("frmWelcomeMessagesView.aspx");
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        //### Validate registration process
        bool bCanSave = true;

       bCanSave = clsValidation.IsNullOrEmpty(txtHeaderText, bCanSave);
       bCanSave = clsValidation.IsNullOrEmpty(txtParagraphText, bCanSave);
       //bCanSave = clsValidation.IsNullOrEmpty(txtPathToImages, bCanSave);
       //bCanSave = clsValidation.IsNullOrEmpty(txtMasterImage, bCanSave);

        if (bCanSave == true)
        {
            mandatoryDiv.Visible = false;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceHappy.png\" alt='' title=''/><div class=\"validationMessage\">WelcomeMessage added successfully</div></div>";
            SaveData();
        }
        else
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">Please fill out all mandatory fields - WelcomeMessage not added</div></div>";
        }
    }

    protected void lnkbtnClear_Click(object sender, EventArgs e)
    {
        mandatoryDiv.Visible = false;

        txtHeaderText.Text = "";
        clsValidation.SetValid(txtHeaderText);
        txtParagraphText.Text = "";
        clsValidation.SetValid(txtParagraphText);
    }

    #endregion 

    #region POPULATE DATA METHODS

    private void popFormData()
    {
         txtHeaderText.Text = clsWelcomeMessages.strHeaderText;
         txtParagraphText.Text = clsWelcomeMessages.strParagraphText;

         //### Populates images
         if (!string.IsNullOrEmpty(clsWelcomeMessages.strPathToImages))
         {
             lblUniquePath.Text = clsWelcomeMessages.strPathToImages;
             getList(clsWelcomeMessages.strPathToImages);
             //### Set Current Master Image
             List<string> lstImagesFileNames = (List<string>)Session["lstImagesFileNames"];
             try
             {
                 foreach (string strImageFileName in lstImagesFileNames)
                 {
                     if (strImageFileName == clsWelcomeMessages.strMasterImage)
                     {
                         RadioButton rdbMasterImage = dlImages.Items[lstImagesFileNames.IndexOf(strImageFileName)].FindControl("rdbMainImage") as RadioButton;
                         rdbMasterImage.Checked = true;
                         break;
                     }
                 }
             }
             catch { }
         }
    }
    
    #endregion

    #region SAVE DATA METHODS

    private void SaveData()
    {
        //### Add / Update
        clsWelcomeMessages.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsWelcomeMessages.iAddedBy = clsUsers.iUserID;
        clsWelcomeMessages.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));

        string strTester = txtParagraphText.Text;
        //string strTemp = strTester.Replace("\n", "<br />");

        clsWelcomeMessages.strHeaderText = txtHeaderText.Text;
        clsWelcomeMessages.strParagraphText = strTester;

        //### Images related items
        clsWelcomeMessages.strPathToImages = lblUniquePath.Text;
        clsWelcomeMessages.strMasterImage = GetMainImagePath(dlImages);

        clsWelcomeMessages.Update();

        Session["dtWelcomeMessagesList"] = null;

        //### Go back to view page
        Response.Redirect("frmWelcomeMessagesView.aspx");
    }

    #endregion
    #region IMAGE METHODS

    List<string> lstImages;
    List<string> lstImagesFileNames;
    int iMaxImages = 1;
    string strUniqueFullPath = AppDomain.CurrentDomain.BaseDirectory + "\\WelcomeMessages";

    protected void btnUpload_Click(object sender, EventArgs e)
    {
        if (Session["lstImages"] == null)
        {
            lstImages = new List<string>();
            Session["lstImages"] = lstImages;
        }
        //### Check that they have ONLY HAVE MAX NUMBER OF Images in the datalist
        if (dlImages.Items.Count == iMaxImages)
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "You can only have " + iMaxImages.ToString() + " Images.";
            getList(lblUniquePath.Text);
        }
        else
        {
            mandatoryDiv.Visible = false;

            string strUniquePath;
            if (lblUniquePath.Text == "")
            {
                strUniquePath = GetUniquePath();
                lblUniquePath.Text = strUniquePath;
            }
            else
            {
                strUniquePath = lblUniquePath.Text;
            }
            UploadImages(strUniquePath);
            getList(strUniquePath);
        }
    }
    private string GetUniquePath()
    {
        int iCount = 1;
        //### First we need to get the path
        while (System.IO.Directory.Exists(strUniqueFullPath + "\\WelcomeMessages" + iCount) == true)
        {
            iCount++;
        }
        return "WelcomeMessages" + iCount;
    }

    protected void UploadImages(String strUniquePath)
    {
        if (FileUpload.PostedFile.ContentLength > 0 && FileUpload.PostedFile.ContentLength < 1073741824)
        {

            //### Upload files to unique folder
            string strUploadFileName = "";
            strUploadFileName = System.IO.Path.GetFileName(FileUpload.PostedFile.FileName);
            string strSaveLocation = "";
            strSaveLocation = strUniqueFullPath + "\\" + strUniquePath + "\\" + strUploadFileName;

            if (!System.IO.Directory.Exists(strUniqueFullPath + "\\" + strUniquePath))
            {
                System.IO.Directory.CreateDirectory(strUniqueFullPath + "\\" + strUniquePath);
            }
            FileUpload.PostedFile.SaveAs(strSaveLocation);

            CopyAndResizePic(strSaveLocation);
            getList(strUniqueFullPath + "\\" + strUniquePath);
        }
        else
        {
            lblValidationMessage.Text = "The file should be between 0 and 1Mb.";
            lblUploadError.Text = "The file should be between 0 and 1Mb.";
        }
    }

    private void CopyAndResizePic(String strFullPath)
    {
        try
        {

            String strFileName;
            String strNewFilePath;

            //### Main Images
            String strLrgFileName;

            strFileName = Path.GetFileName(strFullPath);
            strNewFilePath = strFullPath.Replace(strFileName, "");
            strNewFilePath = strNewFilePath + Path.GetFileNameWithoutExtension(strFullPath);
            strNewFilePath = strNewFilePath + "_lrg";
            strNewFilePath = strNewFilePath + Path.GetExtension(strFullPath);

            File.Copy(strFullPath, strNewFilePath);
            strLrgFileName = Path.GetFileName(strNewFilePath);

            clsCommonFunctions.ResizeImage(strNewFilePath, strNewFilePath, 600, 600, false);

            //### Thumbnail
            String strSmlFileName;

            strFileName = Path.GetFileName(strFullPath);
            strNewFilePath = strFullPath.Replace(strFileName, "");
            strNewFilePath = strNewFilePath + Path.GetFileNameWithoutExtension(strFullPath);
            strNewFilePath = strNewFilePath + "_sml";
            strNewFilePath = strNewFilePath + Path.GetExtension(strFullPath);

            File.Copy(strFullPath, strNewFilePath);
            strSmlFileName = Path.GetFileName(strNewFilePath);

            clsCommonFunctions.ResizeImage(strNewFilePath, strNewFilePath, 140, 140, false);
            
        }
        catch (Exception ex){}
    }

    public void getList(String strPathToFolder)
    {
        lstImages = new List<string>();
        lstImagesFileNames = new List<string>();
        try
        {
            string strPath = strPathToFolder;
            string[] files = Directory.GetFiles(strUniqueFullPath + "\\" + strPath);

            string iWelcomeMessageID = "";
            if (!string.IsNullOrEmpty(Request.QueryString["iWelcomeMessageID"]))
                iWelcomeMessageID = Request.QueryString["iWelcomeMessageID"];

            int iImagesCount = 0;

            foreach (string strName in files)
            {
                if (strName.IndexOf("_sml") != -1)
                {
                    string strHTMLImages = strName.Replace(AppDomain.CurrentDomain.BaseDirectory + "\\", "..\\");
                    strHTMLImages = strHTMLImages.Replace("\\", "/");

                    //### Generates a javascript postback for the delete method
                    String strPostBack = Page.ClientScript.GetPostBackEventReference(this, "iRemoveImages:" + iImagesCount);

                    lstImages.Add(@"<tr style='text-align: centre;'>
                                        <td style='text-align: centre;'>
                                            <a class='ImagesColorBox' href='" + strHTMLImages.Replace("_sml", "_lrg") + "'><img src='" + strHTMLImages + @"' alt='' title='' style='border: solid 5px #ffffff;' /></a><br /><br />" +
                                            "<center><div class='buttonStyle' style='padding-top: 5px;'><a href=\"javascript:" + strPostBack + "\" style='color: #ffffff;'><div class='deleteButton'></div></a></div></center>" + @"
                                        </td>
                                    </tr>");
                    lstImagesFileNames.Add(Path.GetFileName(strName).Replace("_sml", ""));
                    iImagesCount++;
                }
            }
            dlImages.DataSource = lstImages;
            dlImages.DataBind();

            Session["lstImages"] = lstImages;
            Session["lstImagesFileNames"] = lstImagesFileNames;
        }
        catch (Exception ex) {}
    }

    private string GetMainImagePath(DataList dtlTarget)
    {
        string strReturn = "";

        foreach (DataListItem dliTarget in dtlTarget.Items)
        {
            RadioButton rdbMainImage = (RadioButton)dliTarget.FindControl("rdbMainImage");
            if (rdbMainImage.Checked)
            {
                lstImagesFileNames = (List<string>)Session["lstImagesFileNames"];
                strReturn = lstImagesFileNames[dliTarget.ItemIndex];
                break;
            }
        }
        return strReturn;
    }

    private void DeleteImages(int iImagesIndex)
    {

        //### Deletes all Images related to the target Images.
        lstImages = (List<string>)Session["lstImages"];
        lstImagesFileNames = (List<string>)Session["lstImagesFileNames"];

        lstImages.RemoveAt(iImagesIndex);
        Session["lstImages"] = lstImages;

        string[] files = Directory.GetFiles(strUniqueFullPath + "\\" + lblUniquePath.Text);
        foreach (string file in files)
        {
            if (Path.GetFileName(file) == lstImagesFileNames[iImagesIndex].ToString())
            {
                //### Remove all Images
                File.Delete(file);
                File.Delete(file.Replace(Path.GetExtension(file), "_sml" + Path.GetExtension(file)));
                File.Delete(file.Replace(Path.GetExtension(file), "_med" + Path.GetExtension(file)));
                File.Delete(file.Replace(Path.GetExtension(file), "_lrg" + Path.GetExtension(file)));
                break;
            }
        }
        lstImagesFileNames.RemoveAt(iImagesIndex);
        ViewState["lstImagesFileNames"] = lstImagesFileNames;
        getList(lblUniquePath.Text);
    }

    #endregion
}