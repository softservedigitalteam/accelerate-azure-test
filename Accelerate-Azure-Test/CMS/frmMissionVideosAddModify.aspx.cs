using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class CMS_frmMissionVideosAddModify : System.Web.UI.Page
{
    clsUsers clsUsers;
    clsMissionVideos clsMissionVideos;
    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

        //### Determines if a javascript delete has been called
        if (!String.IsNullOrEmpty(Page.Request["__EVENTARGUMENT"]) && (Page.Request["__EVENTARGUMENT"].Split(':')[0] == "iRemoveImages"))
            DeleteImage(Convert.ToInt32(Page.Request["__EVENTARGUMENT"].Split(':')[1]));
        if (!String.IsNullOrEmpty(Page.Request["__EVENTARGUMENT"]) && (Page.Request["__EVENTARGUMENT"].Split(':')[0] == "iRemoveVideo"))
            DeleteVideo(Convert.ToInt32(Page.Request["__EVENTARGUMENT"].Split(':')[1]));

        if ((Request.QueryString["iMissionVideoID"] != "") && (Request.QueryString["iMissionVideoID"] != null))
        {
            clsMissionVideos = new clsMissionVideos(Convert.ToInt32(Request.QueryString["iMissionVideoID"]));
            if (dlVideo.Items.Count > 0)
            {
                txtVideoLink.Enabled = false;
            }
        }
        else
        {
            clsMissionVideos = new clsMissionVideos();
        }
        Session["clsMissionVideos"] = clsMissionVideos;

        if (!IsPostBack)
        {
            //### If the iMissionVideoID is passed through then we want to instantiate the object with that iMissionVideoID
            //### Populate the form
            popFormData();
        }
        else
        {
            if (IsPostBack && FileUpload1.PostedFile != null && FileUpload.PostedFile == null)
            {
                if (FileUpload1.PostedFile.FileName.Length > 0)
                {
                    if (Session["lstImages"] == null)
                    {
                        lstImages = new List<string>();
                        Session["lstImages"] = lstImages;
                    }
                    //### Check that they have ONLY HAVE MAX NUMBER OF Images in the datalist
                    if (dlImages.Items.Count == iMaxImages)
                    {
                        mandatoryDiv.Visible = true;
                        lblValidationMessage.Text = "You can only have " + iMaxImages.ToString() + " Images.";
                        getImageList(lblUniqueImagePath.Text);
                    }
                    else
                    {
                        mandatoryDiv.Visible = false;

                        string strUniquePath;
                        if (lblUniqueImagePath.Text == "")
                        {
                            strUniquePath = GetUniqueImagePath();
                            lblUniqueImagePath.Text = strUniquePath;
                        }
                        else
                        {
                            strUniquePath = lblUniqueImagePath.Text;
                        }
                        UploadImages(strUniquePath);
                    }
                }
            }

            else if (IsPostBack && FileUpload.PostedFile != null && FileUpload1.PostedFile != null)
            {
                if (FileUpload.PostedFile.FileName.Length > 0)
                {

                    if (Session["lstVideo"] == null)
                    {
                        lstVideo = new List<string>();
                        Session["lstVideo"] = lstVideo;
                    }
                    //### Check that they have ONLY HAVE MAX NUMBER OF Image in the datalist
                    //if (dlImage.Items.Count == iMaxImage)
                    //{
                    //    mandatoryDiv.Visible = true;
                    //    lblValidationMessage.Text = "You can only have " + iMaxImage.ToString() + " Image.";
                    //    getList(lblUniquePath.Text);
                    //}
                    //else
                    //{
                    mandatoryDiv.Visible = false;

                    string strUniquePath;
                    if (lblUniquePath.Text == "")
                    {
                        strUniquePath = GetUniquePath();
                        lblUniquePath.Text = strUniquePath;
                    }
                    else
                    {
                        strUniquePath = lblUniquePath.Text;
                    }
                    UploadVideo(strUniquePath);
                    getList(strUniquePath);
                    if (dlVideo.Items.Count > 0)
                    {
                        txtVideoLink.Text = " ";
                        txtVideoLink.Enabled = false;
                    }
                }

                if (FileUpload1.PostedFile.FileName.Length > 0)
                {
                    if (Session["lstImages"] == null)
                    {
                        lstImages = new List<string>();
                        Session["lstImages"] = lstImages;
                    }
                    //### Check that they have ONLY HAVE MAX NUMBER OF Images in the datalist
                    if (dlImages.Items.Count == iMaxImages)
                    {
                        mandatoryDiv.Visible = true;
                        lblValidationMessage.Text = "You can only have " + iMaxImages.ToString() + " Images.";
                        getImageList(lblUniqueImagePath.Text);
                    }
                    else
                    {
                        mandatoryDiv.Visible = false;

                        string strUniquePath;
                        if (lblUniqueImagePath.Text == "")
                        {
                            strUniquePath = GetUniqueImagePath();
                            lblUniqueImagePath.Text = strUniquePath;
                        }
                        else
                        {
                            strUniquePath = lblUniqueImagePath.Text;
                        }
                        UploadImages(strUniquePath);
                    }
                }

                clsMissionVideos = (clsMissionVideos)Session["clsMissionVideos"];
            }

            else if (IsPostBack && FileUpload1.PostedFile == null && FileUpload.PostedFile != null)
            {
                if (FileUpload.PostedFile.FileName.Length > 0)
                {

                    if (Session["lstVideo"] == null)
                    {
                        lstVideo = new List<string>();
                        Session["lstVideo"] = lstVideo;
                    }
                    //### Check that they have ONLY HAVE MAX NUMBER OF Image in the datalist
                    //if (dlImage.Items.Count == iMaxImage)
                    //{
                    //    mandatoryDiv.Visible = true;
                    //    lblValidationMessage.Text = "You can only have " + iMaxImage.ToString() + " Image.";
                    //    getList(lblUniquePath.Text);
                    //}
                    //else
                    //{
                    mandatoryDiv.Visible = false;

                    string strUniquePath;
                    if (lblUniquePath.Text == "")
                    {
                        strUniquePath = GetUniquePath();
                        lblUniquePath.Text = strUniquePath;
                    }
                    else
                    {
                        strUniquePath = lblUniquePath.Text;
                    }
                    UploadVideo(strUniquePath);
                    getList(strUniquePath);
                    if (dlVideo.Items.Count > 0)
                    {
                        txtVideoLink.Text = " ";
                        txtVideoLink.Enabled = false;
                    }
                }

                if (FileUpload1.PostedFile.FileName.Length > 0)
                {
                    if (Session["lstImages"] == null)
                    {
                        lstImages = new List<string>();
                        Session["lstImages"] = lstImages;
                    }
                    //### Check that they have ONLY HAVE MAX NUMBER OF Images in the datalist
                    if (dlImages.Items.Count == iMaxImages)
                    {
                        mandatoryDiv.Visible = true;
                        lblValidationMessage.Text = "You can only have " + iMaxImages.ToString() + " Images.";
                        getImageList(lblUniqueImagePath.Text);
                    }
                    else
                    {
                        mandatoryDiv.Visible = false;

                        string strUniquePath;
                        if (lblUniqueImagePath.Text == "")
                        {
                            strUniquePath = GetUniqueImagePath();
                            lblUniqueImagePath.Text = strUniquePath;
                        }
                        else
                        {
                            strUniquePath = lblUniqueImagePath.Text;
                        }
                        UploadImages(strUniquePath);
                    }
                }

                clsMissionVideos = (clsMissionVideos)Session["clsMissionVideos"];
            }
        }
    }

    protected void lnkbtnBack_Click(object sender, EventArgs e)
    {
        //### Go back to previous page
        Response.Redirect("frmMissionVideosView.aspx");
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        //### Validate registration process
        bool bCanSave = true;

        bCanSave = clsValidation.IsNullOrEmpty(txtTitle, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtColor, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtVideoLink, bCanSave);
        //bCanSave = clsValidation.IsNullOrEmpty(txtPathToImage, bCanSave);
        //bCanSave = clsValidation.IsNullOrEmpty(txtMasterImage, bCanSave);

        if (bCanSave == true)
        {
            mandatoryDiv.Visible = false;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceHappy.png\" alt='' title=''/><div class=\"validationMessage\">MissionVideo added successfully</div></div>";
            SaveData();
        }
        else
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">Please fill out all mandatory fields - MissionVideo not added</div></div>";
        }
    }

    protected void lnkbtnClear_Click(object sender, EventArgs e)
    {
        mandatoryDiv.Visible = false;

        txtColor.Text = "";
        clsValidation.SetValid(txtColor);
        txtVideoLink.Text = "";
        clsValidation.SetValid(txtVideoLink);
    }

    #endregion

    #region POPULATE DATA METHODS

    private void popFormData()
    {
        txtColor.Text = clsMissionVideos.strColor;
        txtVideoLink.Text = clsMissionVideos.strVideoLink;
        txtTitle.Text = clsMissionVideos.strTitle;

        //### Populates images
        if (!string.IsNullOrEmpty(clsMissionVideos.strPathToVideo))
        {
            lblUniquePath.Text = clsMissionVideos.strPathToVideo;
            lblUniqueImagePath.Text = clsMissionVideos.strPathToImages;
            txtVideoLink.Enabled = false;
            getList(clsMissionVideos.strPathToVideo);
        }

        getImageReturnList(clsMissionVideos.strPathToImages);
    }

    #endregion

    #region SAVE DATA METHODS

    private void SaveData()
    {
        //### Add / Update
        clsMissionVideos.iAddedBy = 0;
        clsMissionVideos.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsMissionVideos.iEditedBy = 0;
        clsMissionVideos.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsMissionVideos.strColor = txtColor.Text.ToUpper();
        if (dlVideo.Items.Count > 0)
        {
            clsMissionVideos.strVideoLink = lblUniquePath.Text;
        }
        else
        {
            clsMissionVideos.strVideoLink = txtVideoLink.Text;
        }

        //### Images related items
        clsMissionVideos.strPathToVideo = lblUniquePath.Text;
        clsMissionVideos.strMasterVideo = GetMainVideoPath(dlVideo);

        clsMissionVideos.strPathToImages = lblUniqueImagePath.Text;
        clsMissionVideos.strMasterImage = GetMainImagePath(dlImages);
        clsMissionVideos.strTitle = txtTitle.Text;

        clsMissionVideos.Update();

        Session["dtMissionVideosList"] = null;

        //### Go back to view page
        Response.Redirect("frmMissionVideosView.aspx");
    }

    #endregion

    #region VIDEO METHODS

    List<string> lstVideo;
    List<string> lstVideoFileNames;
    string strUniqueFullPath = AppDomain.CurrentDomain.BaseDirectory + "\\MissionVideos";

    protected void btnUpload_Click(object sender, EventArgs e)
    {
        if (Session["lstVideo"] == null)
        {
            lstVideo = new List<string>();
            Session["lstVideo"] = lstVideo;
        }
        //### Check that they have ONLY HAVE MAX NUMBER OF Image in the datalist
        //if (dlImage.Items.Count == iMaxImage)
        //{
        //    mandatoryDiv.Visible = true;
        //    lblValidationMessage.Text = "You can only have " + iMaxImage.ToString() + " Image.";
        //    getList(lblUniquePath.Text);
        //}
        //else
        //{
        mandatoryDiv.Visible = false;

        string strUniquePath;
        if (lblUniquePath.Text == "")
        {
            strUniquePath = GetUniquePath();
            lblUniquePath.Text = strUniquePath;
        }
        else
        {
            strUniquePath = lblUniquePath.Text;
        }
        UploadVideo(strUniquePath);
        getList(strUniquePath);
        if (dlVideo.Items.Count > 0)
        {
            txtVideoLink.Text = " ";
            txtVideoLink.Enabled = false;
        }
    }

    private string GetUniquePath()
    {
        int iCount = 1;
        //### First we need to get the path
        while (System.IO.Directory.Exists(strUniqueFullPath + "\\MissionVideos" + iCount) == true)
        {
            iCount++;
        }
        return "MissionVideos" + iCount;
    }

    protected void UploadVideo(String strUniquePath)
    {
        if (FileUpload.PostedFile.ContentLength > 0 && long.Parse(FileUpload.PostedFile.ContentLength.ToString()) < 8589934592)
        {

            string temp = FileUpload.PostedFile.ContentType.ToLower();
            if (FileUpload.PostedFile.ContentType.ToLower() == "video/mp4")
            {

                //### Upload files to unique folder
                string strUploadFileName = "";
                strUploadFileName = System.IO.Path.GetFileName(FileUpload.PostedFile.FileName);
                string strSaveLocation = "";
                strSaveLocation = strUniqueFullPath + "\\" + strUniquePath + "\\" + strUploadFileName;

                if (!System.IO.Directory.Exists(strUniqueFullPath + "\\" + strUniquePath))
                {
                    System.IO.Directory.CreateDirectory(strUniqueFullPath + "\\" + strUniquePath);
                }
                FileUpload.PostedFile.SaveAs(strSaveLocation);
                lblUploadError.Text = "";
                lblValidationMessage.Text = "";
            }
            else
            {
                lblValidationMessage.Text = "The file should be .mp4 format.";
                lblUploadError.Text = "The file should be .mp4 format.";
            }
        }
        else
        {
            lblValidationMessage.Text = "The file should be between 0 and 8Mb.";
            lblUploadError.Text = "The file should be between 0 and 8Mb.";
        }
    }

    //private void CopyAndResizePic(String strFullPath)
    //{
    //    try
    //    {

    //        String strFileName;
    //        String strNewFilePath;

    //        //### Main Image
    //        String strLrgFileName;

    //        strFileName = Path.GetFileName(strFullPath);
    //        strNewFilePath = strFullPath.Replace(strFileName, "");
    //        strNewFilePath = strNewFilePath + Path.GetFileNameWithoutExtension(strFullPath);
    //        strNewFilePath = strNewFilePath + "_lrg";
    //        strNewFilePath = strNewFilePath + Path.GetExtension(strFullPath);

    //        File.Copy(strFullPath, strNewFilePath);
    //        strLrgFileName = Path.GetFileName(strNewFilePath);

    //        clsCommonFunctions.ResizeImage(strNewFilePath, strNewFilePath, 600, 600, false);

    //        //### Thumbnail
    //        String strSmlFileName;

    //        strFileName = Path.GetFileName(strFullPath);
    //        strNewFilePath = strFullPath.Replace(strFileName, "");
    //        strNewFilePath = strNewFilePath + Path.GetFileNameWithoutExtension(strFullPath);
    //        strNewFilePath = strNewFilePath + "_sml";
    //        strNewFilePath = strNewFilePath + Path.GetExtension(strFullPath);

    //        File.Copy(strFullPath, strNewFilePath);
    //        strSmlFileName = Path.GetFileName(strNewFilePath);

    //        clsCommonFunctions.ResizeImage(strNewFilePath, strNewFilePath, 140, 140, false);

    //    }
    //    catch (Exception ex){}
    //}

    private string GetMainVideoPath(DataList dtlTarget)
    {
        string strReturn = "";

        foreach (DataListItem dliTarget in dtlTarget.Items)
        {
            lstVideoFileNames = (List<string>)Session["lstVideoFileNames"];
            strReturn = lstVideoFileNames[dliTarget.ItemIndex];
            break;
        }
        return strReturn;
    }

    public void getList(String strPathToFolder)
    {
        lstVideo = new List<string>();
        lstVideoFileNames = new List<string>();
        try
        {
            string strPath = strPathToFolder;
            string[] files = Directory.GetFiles(strUniqueFullPath + "\\" + strPath);

            string iMissionVideoID = "";
            if (!string.IsNullOrEmpty(Request.QueryString["iMissionVideoID"]))
                iMissionVideoID = Request.QueryString["iMissionVideoID"];

            int iVideoCount = 0;

            foreach (string strName in files)
            {

                string strHTMLVideo = strName.Replace(AppDomain.CurrentDomain.BaseDirectory + "\\", "..\\");
                strHTMLVideo = strHTMLVideo.Replace("\\", "/");

                //### Generates a javascript postback for the delete method
                String strPostBack = Page.ClientScript.GetPostBackEventReference(this, "iRemoveVideo:" + iVideoCount);

                lstVideo.Add(@"<tr style='text-align: centre;'>
                                            <td style='text-align: centre;'>
                                                <a class='ImageColorBox' href='" + "'><video src='" + strHTMLVideo + @"' alt='' title='' style='border: solid 5px #ffffff;  width: 150px;' /></a><br /><br />" +
                                        "<center><div class='buttonStyle' style='padding-top: 5px;'><a href=\"javascript:" + strPostBack + "\" style='color: #ffffff;'><div class='deleteButton'></div></a></div></center>" + @"
                                            </td>
                                        </tr>");
                lstVideoFileNames.Add(Path.GetFileName(strName));
                iVideoCount++;
            }
            dlVideo.DataSource = lstVideo;
            dlVideo.DataBind();

            Session["lstVideo"] = lstVideo;
            Session["lstVideoFileNames"] = lstVideoFileNames;
        }
        catch (Exception ex) { }
    }

    private void DeleteVideo(int iVideoIndex)
    {

        //### Deletes all Image related to the target Image.
        lstVideo = (List<string>)Session["lstVideo"];
        lstVideoFileNames = (List<string>)Session["lstVideoFileNames"];

        lstVideo.RemoveAt(iVideoIndex);
        Session["lstVideo"] = lstVideo;

        string[] files = Directory.GetFiles(strUniqueFullPath + "\\" + lblUniquePath.Text);
        foreach (string file in files)
        {
            if (Path.GetFileName(file) == lstVideoFileNames[iVideoIndex].ToString())
            {
                //### Remove all Image
                File.Delete(file);
                break;
            }
        }
        lstVideoFileNames.RemoveAt(iVideoIndex);
        ViewState["lstVideoFileNames"] = lstVideoFileNames;
        getList(lblUniquePath.Text);

        txtVideoLink.Enabled = true;
    }

    #endregion

    #region IMAGE METHODS

    List<string> lstImages;
    List<string> lstImagesFileNames;
    int iMaxImages = 1;
    string strUniqueImageFullPath = AppDomain.CurrentDomain.BaseDirectory + "\\Missions";
    protected void btnImageUpload_Click(object sender, EventArgs e)
    {
        if (Session["lstImages"] == null)
        {
            lstImages = new List<string>();
            Session["lstImages"] = lstImages;
        }
        //### Check that they have ONLY HAVE MAX NUMBER OF Images in the datalist
        if (dlImages.Items.Count == iMaxImages)
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "You can only have " + iMaxImages.ToString() + " Images.";
            getImageList(lblUniqueImagePath.Text);
        }
        else
        {
            mandatoryDiv.Visible = false;

            string strUniquePath;
            if (lblUniqueImagePath.Text == "")
            {
                strUniquePath = GetUniqueImagePath();
                lblUniqueImagePath.Text = strUniquePath;
            }
            else
            {
                strUniquePath = lblUniqueImagePath.Text;
            }
            UploadImages(strUniquePath);
        }
    }

    private string GetUniqueImagePath()
    {
        int iCount = 1;
        //### First we need to get the path
        while (System.IO.Directory.Exists(strUniqueImageFullPath + "\\Missions" + iCount) == true)
        {
            iCount++;
        }
        return "Missions" + iCount;
    }

    protected void UploadImages(String strUniquePath)
    {
        if (FileUpload1.PostedFile.ContentLength > 0 && FileUpload1.PostedFile.ContentLength < 1073741824)
        {

            //### Upload files to unique folder
            string strUploadFileName = "";
            strUploadFileName = System.IO.Path.GetFileName(FileUpload1.PostedFile.FileName);
            string strSaveLocation = "";
            strSaveLocation = strUniqueImageFullPath + "\\" + strUniquePath + "\\" + strUploadFileName;

            if (!System.IO.Directory.Exists(strUniqueImageFullPath + "\\" + strUniquePath))
            {
                System.IO.Directory.CreateDirectory(strUniqueImageFullPath + "\\" + strUniquePath);
            }
            FileUpload1.PostedFile.SaveAs(strSaveLocation);

            CopyAndResizePic(strSaveLocation);
            getImageList(strUniqueImageFullPath + "\\" + strUniquePath);
        }
        else
        {
            lblValidationMessage.Text = "The file should be between 0 and 1Mb.";
            lblUploadError.Text = "The file should be between 0 and 1Mb.";
        }
    }

    private void CopyAndResizePic(String strFullPath)
    {
        try
        {

            String strFileName;
            String strNewFilePath;

            //### Main Images
            String strLrgFileName;

            strFileName = Path.GetFileName(strFullPath);
            strNewFilePath = strFullPath.Replace(strFileName, "");
            strNewFilePath = strNewFilePath + Path.GetFileNameWithoutExtension(strFullPath);
            strNewFilePath = strNewFilePath + "_lrg";
            strNewFilePath = strNewFilePath + Path.GetExtension(strFullPath);

            File.Copy(strFullPath, strNewFilePath);
            strLrgFileName = Path.GetFileName(strNewFilePath);

            clsCommonFunctions.ResizeImage(strNewFilePath, strNewFilePath, 600, 600, false);

            //### Thumbnail
            String strSmlFileName;

            strFileName = Path.GetFileName(strFullPath);
            strNewFilePath = strFullPath.Replace(strFileName, "");
            strNewFilePath = strNewFilePath + Path.GetFileNameWithoutExtension(strFullPath);
            strNewFilePath = strNewFilePath + "_sml";
            strNewFilePath = strNewFilePath + Path.GetExtension(strFullPath);

            File.Copy(strFullPath, strNewFilePath);
            strSmlFileName = Path.GetFileName(strNewFilePath);

            clsCommonFunctions.ResizeImage(strNewFilePath, strNewFilePath, 140, 140, false);

        }
        catch (Exception ex) { }
    }

    public void getImageList(String strPathToFolder)
    {
        lstImages = new List<string>();
        lstImagesFileNames = new List<string>();
        try
        {
            string strPath = strPathToFolder;
            string[] files = Directory.GetFiles(strPath);

            string iMissionID = "";
            if (!string.IsNullOrEmpty(Request.QueryString["iMissionID"]))
                iMissionID = Request.QueryString["iMissionID"];

            int iImagesCount = 0;

            foreach (string strName in files)
            {
                if (strName.IndexOf("_sml") != -1)
                {
                    string strHTMLImages = strName.Replace(AppDomain.CurrentDomain.BaseDirectory + "\\", "..\\");
                    strHTMLImages = strHTMLImages.Replace("\\", "/");

                    //### Generates a javascript postback for the delete method
                    String strPostBack = Page.ClientScript.GetPostBackEventReference(this, "iRemoveImages:" + iImagesCount);

                    lstImages.Add(@"<tr style='text-align: centre;'>
                                        <td style='text-align: centre;'>
                                            <a class='ImagesColorBox' href='" + strHTMLImages.Replace("_sml", "_lrg") + "'><img src='" + strHTMLImages + @"' alt='' title='' style='border: solid 5px #ffffff;' /></a><br /><br />" +
                                            "<center><div class='buttonStyle' style='padding-top: 5px;'><a href=\"javascript:" + strPostBack + "\" style='color: #ffffff;'><div class='deleteButton'></div></a></div></center>" + @"
                                        </td>
                                    </tr>");
                    lstImagesFileNames.Add(Path.GetFileName(strName).Replace("_sml", ""));
                    iImagesCount++;
                }
            }
            dlImages.DataSource = lstImages;
            dlImages.DataBind();

            Session["lstImages"] = lstImages;
            Session["lstImagesFileNames"] = lstImagesFileNames;
        }
        catch (Exception ex) { }
    }

    public void getImageReturnList(String strPathToFolder)
    {
        lstImages = new List<string>();
        lstImagesFileNames = new List<string>();
        try
        {
            string strPath = strPathToFolder;
            string[] files = Directory.GetFiles(strUniqueImageFullPath + "\\" + strPath);

            string iMissionID = "";
            if (!string.IsNullOrEmpty(Request.QueryString["iMissionID"]))
                iMissionID = Request.QueryString["iMissionID"];

            int iImagesCount = 0;

            foreach (string strName in files)
            {
                if (strName.IndexOf("_sml") != -1)
                {
                    string strHTMLImages = strName.Replace(AppDomain.CurrentDomain.BaseDirectory + "\\", "..\\");
                    strHTMLImages = strHTMLImages.Replace("\\", "/");

                    //### Generates a javascript postback for the delete method
                    String strPostBack = Page.ClientScript.GetPostBackEventReference(this, "iRemoveImages:" + iImagesCount);

                    lstImages.Add(@"<tr style='text-align: centre;'>
                                        <td style='text-align: centre;'>
                                            <a class='ImagesColorBox' href='" + strHTMLImages.Replace("_sml", "_lrg") + "'><img src='" + strHTMLImages + @"' alt='' title='' style='border: solid 5px #ffffff;' /></a><br /><br />" +
                                            "<center><div class='buttonStyle' style='padding-top: 5px;'><a href=\"javascript:" + strPostBack + "\" style='color: #ffffff;'><div class='deleteButton'></div></a></div></center>" + @"
                                        </td>
                                    </tr>");
                    lstImagesFileNames.Add(Path.GetFileName(strName).Replace("_sml", ""));
                    iImagesCount++;
                }
            }
            dlImages.DataSource = lstImages;
            dlImages.DataBind();

            Session["lstImages"] = lstImages;
            Session["lstImagesFileNames"] = lstImagesFileNames;
        }
        catch (Exception ex) { }
    }

    private string GetMainImagePath(DataList dtlTarget)
    {
        string strReturn = "";

        foreach (DataListItem dliTarget in dtlTarget.Items)
        {

            lstImagesFileNames = (List<string>)Session["lstImagesFileNames"];
            strReturn = lstImagesFileNames[dliTarget.ItemIndex];
            break;
        }
        return strReturn;
    }

    private void DeleteImage(int iImagesIndex)
    {

        //### Deletes all Images related to the target Images.
        lstImages = (List<string>)Session["lstImages"];
        lstImagesFileNames = (List<string>)Session["lstImagesFileNames"];

        lstImages.RemoveAt(iImagesIndex);
        Session["lstImages"] = lstImages;

        string[] files = Directory.GetFiles(strUniqueImageFullPath + "\\" + lblUniqueImagePath.Text);
        foreach (string file in files)
        {
            if (Path.GetFileName(file) == lstImagesFileNames[iImagesIndex].ToString())
            {
                //### Remove all Images
                File.Delete(file);
                File.Delete(file.Replace(Path.GetExtension(file), "_sml" + Path.GetExtension(file)));
                File.Delete(file.Replace(Path.GetExtension(file), "_med" + Path.GetExtension(file)));
                File.Delete(file.Replace(Path.GetExtension(file), "_lrg" + Path.GetExtension(file)));
                break;
            }
        }
        lstImagesFileNames.RemoveAt(iImagesIndex);
        ViewState["lstImagesFileNames"] = lstImagesFileNames;
        getImageReturnList(lblUniqueImagePath.Text);
    }

    #endregion

}