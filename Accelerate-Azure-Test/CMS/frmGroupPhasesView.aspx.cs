using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;
using System.Net.Mail;
using System.Text;
using System.Configuration;
using System.IO;

/// <summary>
/// Summary description for clsPhasesView
/// </summary>
public partial class CMS_frmGroupPhasesView : System.Web.UI.Page
{
    DataTable dtOrderItems = new DataTable();

    #region CLASSES AND VARIABLES

    clsUsers clsUsers;
    DataTable dtPhasesList;

    List<clsPhases> glstPhases;

    #endregion

    #region EVENT MODULES

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

        dgrGrid.ItemCreated += new DataGridItemEventHandler(clsCommonFunctions.dgrGrid_PagerPopulate);
        dgOrder.ItemCreated += new DataGridItemEventHandler(clsCommonFunctions.dgrGrid_PagerPopulate);

        if (!Page.IsPostBack)
        {
            Session["dtPhasesList"] = null;
            PopulateFormData();
            PopulateOrderItems();
        }
        else
        {
            if (Session["glstPhases"] == null)
            {
                glstPhases = new List<clsPhases>();
                Session["glstPhases"] = glstPhases;
            }
            else
                glstPhases = (List<clsPhases>)Session["glstPhases"];
        }
    }

    protected string EscapeLikeValue(string valueWithoutWildcards)
    {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < valueWithoutWildcards.Length; i++)
        {
            char c = valueWithoutWildcards[i];
            if (c == '*' || c == '%' || c == '[' || c == ']')
                sb.Append("[").Append(c).Append("]");
            else if (c == '\'')
                sb.Append("''");
            else
                sb.Append(c);
        }
        return sb.ToString();
    }

    protected void lnkbtnSearch_Click(object sender, EventArgs e)
    {
        string EscapedString = EscapeLikeValue(txtSearch.Text.ToString());
        string FilterExpression ="(strPhaseName +' '+ strStockCode +' '+ strDescription) LIKE '%" + EscapedString + "%'";
        DataTable dtPhasesList = clsPhases.GetPhasesList(FilterExpression, "");

        Session["dtPhasesList"] = dtPhasesList;

        PopulateFormData();
        PopulateOrderItems();
    }
    
    protected void lnkbtnSubmit_Click(object sender, EventArgs e)
    {
        if ((dtOrderItems != null))
        {
            try
            {
                PopulateFormData();
                PopulateOrderItems();
            }
            catch { }
        }
        else
        {
            PopulateFormData();
            PopulateOrderItems();
        }
        
    }

    #endregion

    #region DATA GRID METHODS

    private void PopulateFormData()
    {
        //### Populate datagrid using clsPhasesList object
        try
        {
            clsGroups clsGroups = new clsGroups(Convert.ToInt32(Request.QueryString["iGroupID"]));
            lblGroupName.Text = clsGroups.strTitle;
            dtPhasesList = new DataTable();

            if (Session["dtPhasesList"] == null)
                dtPhasesList = clsPhases.GetPhasesList();
            else
                dtPhasesList = (DataTable)Session["dtPhasesList"];

            dgrGrid.DataSource = dtPhasesList;

            //### Add this check to trap if the last item on the page is deleted
            if (Convert.ToInt16(dgrGrid.CurrentPageIndex) < Convert.ToInt16(Session["PhasesView_CurrentPageIndex"]))
            {
                dgrGrid.CurrentPageIndex = 0;
            }
            else
            {
                if (Session["PhasesView_CurrentPageIndex"] != null)
                {
                    dgrGrid.CurrentPageIndex = (int)Session["PhasesView_CurrentPageIndex"];
                }
            }

            dgrGrid.DataBind();

        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void lnkAddItem_Click(object sender, EventArgs e)
    {
        DataTable dtCurrentGroupPhaseList = clsGroupPhases.GetGroupPhasesList("iGroupID=" + Convert.ToInt32(Request.QueryString["iGroupID"]), "");
        mandatoryDiv.Visible = false;
        if(dtCurrentGroupPhaseList.Rows.Count < 3)
        {
            mandatoryDiv.Visible = false;
            int iPhaseID = int.Parse((sender as LinkButton).CommandArgument);

            AddItemToOrderList(iPhaseID);

            PopulateFormData();
            PopulateOrderItems();
        }
        else
        {
            //A group cannot have more than 3 Phases
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">A group cannot have more than 3 Phases</div></div>";
        }
    }

    protected void lnkDeleteItem_Click(object sender, EventArgs e)
    {
        mandatoryDiv.Visible = false;
        int iGroupPhaseID = int.Parse((sender as LinkButton).CommandArgument);

        clsGroupPhases.Delete(iGroupPhaseID);

        DataTable dtPhasesList = new DataTable();

        if (((txtSearch.Text == "SEARCH FOR A PHASE TO ADD") == false) && ((txtSearch.Text == "") == false))
        {
            string FilterExpression = "(strPhaseName +' '+ strStockCode +' '+ strDescription) LIKE '%" + txtSearch.Text + "%'";
            dtPhasesList = clsPhases.GetPhasesList(FilterExpression, "");
        }
        else
        {
            dtPhasesList = clsPhases.GetPhasesList();
        }

        Session["dtPhasesList"] = dtPhasesList;

        PopulateFormData();
        PopulateOrderItems();
    }

    protected void AddItemToOrderList(int iPhaseID)
    {
        clsPhases clsPhases = new clsPhases(iPhaseID);

        clsGroupPhases clsGroupPhases = new clsGroupPhases();

        //clsGroupPhases.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        //clsGroupPhases.iAddedBy = clsUsers.iUserID;
        clsGroupPhases.iGroupID = Convert.ToInt32(Request.QueryString["iGroupID"]);
        clsGroupPhases.iPhaseID = iPhaseID;
        clsGroupPhases.strTitle = clsPhases.strTitle;
        clsGroupPhases.Update();
    }

    private void PopulateOrderItems()
    {
        //### Populate datagrid using clsMembersList object
        try
        {
            dtOrderItems = clsGroupPhases.GetGroupPhasesList("iGroupID=" + Convert.ToInt32(Request.QueryString["iGroupID"]), "");
            Session["dtOrderItems"] = dtOrderItems;

            if (Session["dtOrderItems"] == null)
                dtOrderItems = clsGroupPhases.GetGroupPhasesList("iGroupID=" + Convert.ToInt32(Request.QueryString["iGroupID"]), "");
            else
                dtOrderItems = (DataTable)Session["dtOrderItems"];

            dgOrder.DataSource = dtOrderItems;

            //### Add this check to trap if the last item on the page is deleted
            if (Convert.ToInt16(dgOrder.CurrentPageIndex) < Convert.ToInt16(Session["ManualOrdersView_CurrentPageIndex"]))
            {
                dgOrder.CurrentPageIndex = 0;
            }
            else
            {
                if (Session["ManualOrdersView_CurrentPageIndex"] != null)
                {
                    dgOrder.CurrentPageIndex = (int)Session["ManualOrdersView_CurrentPageIndex"];
                }
            }

            dgOrder.DataBind();

            //litTotal.Text = "<br style='clear:both;' /><div style='clear:both;float:right;margin-right:25px;'>ORDER TOTAL: <b>R " + calculateTotal().ToString("N2") + "</b></div>";

        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgOrder_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //### Method is used when the page index is changed.
        try
        {
            Session["ManualOrdersView_CurrentPageIndex"] = e.NewPageIndex;
            dgOrder.CurrentPageIndex = e.NewPageIndex;

            PopulateFormData();
            PopulateOrderItems();
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgrGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //### Method is used when the page index is changed.
        try
        {
            Session["PhasesView_CurrentPageIndex"] = e.NewPageIndex;
            dgrGrid.CurrentPageIndex = e.NewPageIndex;

            PopulateFormData();
            PopulateOrderItems();
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgrGrid_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        //### Sort Method
        dtPhasesList = new DataTable();

        if (Session["dtPhasesList"] == null)
            dtPhasesList = clsPhases.GetPhasesList();
        else
            dtPhasesList = (DataTable)Session["dtPhasesList"];

        DataView dvTemp = dtPhasesList.DefaultView;

        dvTemp.Sort = e.SortExpression;
        dtPhasesList = dvTemp.ToTable();
        Session["dtPhasesList"] = dtPhasesList;

        PopulateFormData();
        PopulateOrderItems();
    }

    #endregion

    #region Phases FILTERING

    /// <summary>
    /// Enables the AutoCompleteExtender
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    /// 
    [WebMethod]
    [ScriptMethod]
    public static string[] FilterBusinessRule(string prefixText, int count)
    {
        string[] strReturnList = new string[] { };

        string FilterExpression = "(strPhaseName +' '+ strStockCode +' '+ strDescription) LIKE '%" + prefixText + "%'";
        DataTable dtPhases = clsPhases.GetPhasesList(FilterExpression, "");
        List<string> glstPhases = new List<string>();

        if (dtPhases.Rows.Count > 0)
        {
            foreach (DataRow dtrPhases in dtPhases.Rows)
            {
                glstPhases.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem("" + dtrPhases["strPhaseName"].ToString() +' '+"" + dtrPhases["strStockCode"].ToString() +' '+"" + dtrPhases["strDescription"].ToString(), dtrPhases["iPhaseID"].ToString()));
            }
        }
        else
            glstPhases.Add("No Phases Available.");
        strReturnList = glstPhases.ToArray();
        return strReturnList;
    }

    #endregion
}
