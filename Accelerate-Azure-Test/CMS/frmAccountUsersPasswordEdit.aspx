﻿<%@ Page Title="CMS Users" Language="C#" MasterPageFile="~/CMS/CMS.master" AutoEventWireup="true" Inherits="CMS_frmAccountUsersPasswordEdit" Codebehind="frmAccountUsersPasswordEdit.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<script lang="javascript" type="text/javascript">

    //### Password check
    function passwordCheck() {
        if (document.getElementById("<%=txtPassword.ClientID%>").value != document.getElementById("<%=txtConfirmPassword.ClientID%>").value) {
            alert('Your passwords do not match.\nYour passwords have been reset.')
            document.getElementById("<%=txtPassword.ClientID%>").value = ""
            document.getElementById("<%=txtConfirmPassword.ClientID%>").value = ""
            document.getElementById("<%=txtPassword.ClientID%>").focus()
        }
    }
    
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div id="mandatoryDiv" class="mandatoryInvalidDiv" runat="server" visible="false">
        <asp:Label ID="lblValidationMessage" runat="server"></asp:Label>
        <b class="spacer"></b>
    </div>

    <div id="divPassword" runat="server" class="controlDiv">
        <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
        <div id="passwordLabel" class="labelDiv">Password:</div>
        <div id="passwordField" class="fieldDiv">
            <asp:TextBox ID="txtPassword" runat="server" CssClass="roundedCornerTextBox" TextMode="Password" onblur="setValid(this)" />
        </div>
        <br class="clearingSpacer" />
    </div>

    <div id="divConfirmPassword" runat="server" class="controlDiv">
        <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
        <div id="confirmPasswordLabel" class="labelDiv">Confirm Password:</div>
        <div id="confirmPasswordField" class="fieldDiv">
            <asp:TextBox ID="txtConfirmPassword" runat="server" CssClass="roundedCornerTextBox" TextMode="Password" onblur="passwordCheck(); setValid(this);" />
        </div>
        <br class="clearingSpacer" />
    </div>

    <br />
    <div>
        <img src="images/imgDgrFooter.png" alt="" />
    </div>

    <div class="buttonsRightDiv">
        <asp:LinkButton ID="lnkbtnBack" runat="server" CssClass="backButton" onclick="lnkbtnBack_Click" />
        <asp:LinkButton ID="lnkbtnSave" runat="server" CssClass="saveButton" onclick="lnkbtnSave_Click" />
        <asp:LinkButton ID="lnkbtnClear" runat="server" CssClass="clearButton" onclick="lnkbtnClear_Click" />        
    </div>

</asp:Content>