
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text;

/// <summary>
/// Summary description for clsUserAnswersView
/// </summary>
public partial class CMS_clsUserAnswersView : System.Web.UI.Page
{
    #region CLASSES AND VARIABLES

    clsUsers clsUsers;
    DataTable dtUserAnswersList;

    List<clsUserAnswers> glstUserAnswers;

    #endregion

    #region EVENT MODULES

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

dgrGrid.ItemCreated += new DataGridItemEventHandler(clsCommonFunctions.dgrGrid_PagerPopulate);

        if (!Page.IsPostBack)
        {
            Session["dtUserAnswersList"] = null;
            PopulateFormData();
        }
        else
        {
            if (Session["glstUserAnswers"] == null)
            {
                glstUserAnswers = new List<clsUserAnswers>();
                Session["glstUserAnswers"] = glstUserAnswers;
            }
            else
                glstUserAnswers = (List<clsUserAnswers>)Session["glstUserAnswers"];
        }
            PopulateFormData();

    }

    protected string EscapeLikeValue(string valueWithoutWildcards)
    {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < valueWithoutWildcards.Length; i++)
        {
            char c = valueWithoutWildcards[i];
            if (c == '*' || c == '%' || c == '[' || c == ']')
                sb.Append("[").Append(c).Append("]");
            else if (c == '\'')
                sb.Append("''");
            else
                sb.Append(c);
        }
        return sb.ToString();
    }

    protected void lnkbtnSearch_Click(object sender, EventArgs e)
    {
        string EscapedString = EscapeLikeValue(txtSearch.Text.ToString());
        string FilterExpression ="(strAnswer) LIKE '%" + EscapedString + "%'";
        DataTable dtUserAnswersList = clsUserAnswers.GetUserAnswersList(FilterExpression, "");

        Session["dtUserAnswersList"] = dtUserAnswersList;

        PopulateFormData();
    }

    protected void lnkbtnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmUserAnswersAddModify.aspx");
    }

    #endregion

    #region DATA GRID METHODS

    private void PopulateFormData()
    {
        //### Populate datagrid using clsUserAnswersList object
        try
        {
            dtUserAnswersList = new DataTable();

            if (Session["dtUserAnswersList"] == null)
                dtUserAnswersList = clsUserAnswers.GetUserAnswersList();
            else
                dtUserAnswersList = (DataTable)Session["dtUserAnswersList"];

            dgrGrid.DataSource = dtUserAnswersList;

            //### Add this check to trap if the last item on the page is deleted
            if (Convert.ToInt16(dgrGrid.CurrentPageIndex) < Convert.ToInt16(Session["UserAnswersView_CurrentPageIndex"]))
            {
                dgrGrid.CurrentPageIndex = 0;
            }
            else
            {
                if (Session["UserAnswersView_CurrentPageIndex"] != null)
                {
                    dgrGrid.CurrentPageIndex = (int)Session["UserAnswersView_CurrentPageIndex"];
                }
            }

            dgrGrid.DataBind();

        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void lnkDeleteItem_Click(object sender, EventArgs e)
    {
        int iUserAnswerID = int.Parse((sender as LinkButton).CommandArgument);

        clsUserAnswers.Delete(iUserAnswerID);

        DataTable dtUserAnswersList = new DataTable();

        if (((txtSearch.Text == "SEARCH FOR A USER ANSWER") == false) && ((txtSearch.Text == "") == false))
        {
            string FilterExpression = "(strAnswer) LIKE '%" + txtSearch.Text + "%'";
            dtUserAnswersList = clsUserAnswers.GetUserAnswersList(FilterExpression, "");
        }
        else
        {
            dtUserAnswersList = clsUserAnswers.GetUserAnswersList();
        }

        Session["dtUserAnswersList"] = dtUserAnswersList;

        PopulateFormData();
    }

    protected void dgrGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //### Method is used when the page index is changed.
        try
        {
            Session["UserAnswersView_CurrentPageIndex"] = e.NewPageIndex;
            dgrGrid.CurrentPageIndex = e.NewPageIndex;
            PopulateFormData();
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgrGrid_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        //### Sort Method
        dtUserAnswersList = new DataTable();

        if (Session["dtUserAnswersList"] == null)
            dtUserAnswersList = clsUserAnswers.GetUserAnswersList();
        else
            dtUserAnswersList = (DataTable)Session["dtUserAnswersList"];

        DataView dvTemp = dtUserAnswersList.DefaultView;

        dvTemp.Sort = e.SortExpression;
        dtUserAnswersList = dvTemp.ToTable();
        Session["dtUserAnswersList"] = dtUserAnswersList;

        PopulateFormData();
    }

    public void dgrGrid_PopEditLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkEditItem = (HyperLink)sender;
            DataGridItem dgrItemEdit = (DataGridItem)lnkEditItem.Parent.Parent;

            //### Get the iUserAnswerID
            int iUserAnswerID = 0;
            iUserAnswerID = int.Parse(dgrItemEdit.Cells[0].Text);

            //### Add attributes to edit link
            lnkEditItem.Attributes.Add("href", "frmUserAnswersAddModify.aspx?action=edit&iUserAnswerID=" + iUserAnswerID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

     public void dgrGrid_PopDeleteLink(object sender, EventArgs e)
     {
          try
          {
             //### Add hyperlink to datagrid item
             HyperLink lnkDeleteItem = (HyperLink)sender;
             DataGridItem dgrItemDelete = (DataGridItem)lnkDeleteItem.Parent.Parent;

             //### Get the iUserAnswerID
             int iUserAnswerID = 0;
             iUserAnswerID = int.Parse(dgrItemDelete.Cells[0].Text);

             //### Add attributes to delete link
             lnkDeleteItem.CssClass = "dgrDeleteLink";
             lnkDeleteItem.Attributes.Add("href", "frmUserAnswersView.aspx?action=delete&iUserAnswerID=" + iUserAnswerID);
           }
           catch (Exception ex)
           {
                 Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
           }
     }

    #endregion

    #region UserAnswers FILTERING

    /// <summary>
    /// Enables the AutoCompleteExtender
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    /// 
    [WebMethod]
    [ScriptMethod]
    public static string[] FilterBusinessRule(string prefixText, int count)
    {
        string[] strReturnList = new string[] { };

        string FilterExpression = "(strAnswer) LIKE '%" + prefixText + "%'";
        DataTable dtUserAnswers = clsUserAnswers.GetUserAnswersList(FilterExpression, "");
        List<string> glstUserAnswers = new List<string>();

        if (dtUserAnswers.Rows.Count > 0)
        {
            foreach (DataRow dtrUserAnswers in dtUserAnswers.Rows)
            {
                glstUserAnswers.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem("" + dtrUserAnswers["strAnswer"].ToString(), dtrUserAnswers["iUserAnswerID"].ToString()));
            }
        }
        else
            glstUserAnswers.Add("No UserAnswers Available.");
        strReturnList = glstUserAnswers.ToArray();
        return strReturnList;
    }

    #endregion
}
