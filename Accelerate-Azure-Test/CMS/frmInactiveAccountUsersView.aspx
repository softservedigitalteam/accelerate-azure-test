<%@ Page Title="InactiveAccountUsers" Language="C#" MasterPageFile="~/CMS/CMS.master" AutoEventWireup="true" Inherits="CMS_clsInactiveAccountUsersView" Codebehind="frmInactiveAccountUsersView.aspx.cs" %>

<%@ Register TagPrefix="ajax" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<script lang="javascript" type="text/javascript">
    //### Password check
    function clearText() {

            document.getElementById("<%=txtSearch.ClientID%>").value = ""

    }
</script>
<script type="text/javascript">
    function RefreshUpdatePanel(sender) {
        __doPostBack(sender, '');
    }
</script>

<!-- Event Manager for selection of item from auto-complete list -->
<script lang="javascript" type="text/javascript">
    function ClientItemSelected(sender, e) {
        $get("<%=hfInactiveAccountUserID.ClientID %>").value = e.get_value();
        $get('<%=lnkbtnSearch.ClientID %>').click();
    }

    function toggleDiv(divID) {
        $(divID).slideToggle();
    }

    function CheckAll(oCheckbox)
 {
        var dgrGrid = document.getElementById("<%=dgrGrid.ClientID %>");
        for (i = 1; i < dgrGrid.rows.length; i++)
     {
            dgrGrid.rows[i].cells[0].getElementsByTagName("INPUT")[0].checked = oCheckbox.checked;
     }
    }

    //function CheckChanged()                                   
    //    {                                                                   
    //      var frm = document.form1;                              
    //      var boolAllChecked;                                         
    //      boolAllChecked=true;                                       
    //      for(i=0;i< frm.length;i++)                                 
    //      {                                                                 
    //        e=frm.elements[i];                                        
    //      if ( e.type=='checkbox' && e.name.indexOf('Id') != -1 )
    //          if(e.checked== false)                                  
    //          {                                                             
    //             boolAllChecked=false;                               
    //             break;                                                    
    //          }                                                              
    //      }                                                                  
    //      for(i=0;i< frm.length;i++)                                  
    //      {                                                                  
    //        e=frm.elements[i];                                         
    //        if ( e.type=='checkbox' && e.name.indexOf('checkAll') != -1 )
    //        {                                                            
    //          if( boolAllChecked==false)                         
    //             e.checked= false ;                                
    //             else                                                    
    //             e.checked= true;                                  
    //          break;                                                    
    //        }                                                             
    //       }                                                              
    //     }
</script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <asp:Panel ID="pSearch" runat="server" DefaultButton="lnkbtnSearch">
        <div class="controlDiv" style="margin-top:15px;">
            <div class="fieldSearchDiv">
                <asp:TextBox ID="txtSearch" runat="server" CssClass="roundedCornerTextBox" Text="SEARCH FOR A INACTIVE ACCOUNT USER" onfocus="clearText();"/>
                <ajax:AutoCompleteExtender ID="aceInactiveAccountUsers" runat="server" TargetControlID="txtSearch" ServiceMethod="FilterBusinessRule"
                        EnableCaching="true" MinimumPrefixLength="1" CompletionInterval="0" CompletionSetCount="3" 
                        CompletionListCssClass="popupCompletionList" CompletionListItemCssClass="popupCompletionListItem" CompletionListHighlightedItemCssClass="popupCompletionListItemHighlight"
                        OnClientItemSelected="ClientItemSelected" FirstRowSelected="true" />
                        <asp:HiddenField ID="hfInactiveAccountUserID" runat="server" Value="" />
            </div>
            <div class="buttonsSearchDiv"><asp:LinkButton ID="lnkbtnSearch" runat="server" CssClass="searchButton" onclick="lnkbtnSearch_Click" /></div>
        </div>
        <br class="clearingSpacer"/>
    </asp:Panel>

    <br />

    <asp:UpdatePanel ID="udpInactiveAccountUsersView" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:DataGrid ID="dgrGrid" Runat="server" AutoGenerateColumns="False" AllowSorting="true" OnSelectedIndexChanged="dgrGrid_SelectedIndexChanged"
                AllowPaging="true" PageSize="15" PagerStyle-Mode="NumericPages" OnPageIndexChanged="dgrGrid_PageIndexChanged" 
                OnSortCommand="dgrGrid_SortCommand" CellPadding="3" CellSpacing="0" PagerStyle-NextPageText="Next" PagerStyle-PrevPageText="Prev"
                HeaderStyle-CssClass="dgrHeader"  ItemStyle-CssClass="dgrItem" AlternatingItemStyle-CssClass="dgrAltItem" PagerStyle-CssClass="dgrPg" CssClass="dgr">
                <Columns>
                    <asp:BoundColumn DataField="iInactiveAccountUserID" Visible="false" />
                    <asp:TemplateColumn HeaderText="" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="50px" HeaderStyle-CssClass="dgrHeaderLeft">
                        <HeaderTemplate>
                            <asp:CheckBox id="chkbcHeader" runat="server" AutoPostBack="true" onclick="CheckAll(this)"/>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkbxActivate" runat="server" AutoPostBack="true" OnCheckedChanged="chkbxActivate_CheckedChanged" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:BoundColumn DataField="strFirstName" ReadOnly="false" SortExpression="strFirstName" HeaderText="First"  HeaderStyle-HorizontalAlign="Left"/>
                    <asp:BoundColumn DataField="strSurname" SortExpression="strSurname" HeaderText="Surname" HeaderStyle-HorizontalAlign="Left"/>
                    <asp:BoundColumn DataField="strEmailAddress" SortExpression="strEmailAddress" HeaderText="Email Address" HeaderStyle-HorizontalAlign="Left"/>
                    <asp:BoundColumn DataField="strJobTitle" SortExpression="strJobTitle" HeaderText="Job Title" HeaderStyle-HorizontalAlign="Left"/>

                    <asp:TemplateColumn HeaderText="Group" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="100px" >
                        <ItemTemplate>
                            <asp:DropDownList Width="200" ID="lstGroup" runat="server" OnDataBinding="lstGroup_DataBinding"/>
                        </ItemTemplate>
                    </asp:TemplateColumn>

                    <asp:TemplateColumn HeaderText="Edit" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50px" >
                        <ItemTemplate>
                            <asp:HyperLink ID="lnkEditItem" runat="Server" CssClass="dgrLinkEdit" OnDataBinding="dgrGrid_PopEditLink" />
                        </ItemTemplate>
                    </asp:TemplateColumn>

                    <asp:TemplateColumn HeaderText="Delete" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50px"  HeaderStyle-CssClass="dgrHeaderRight" >
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkDeleteItem" runat="server" CssClass="dgrLinkDelete" CommandArgument='<%# Eval("iInactiveAccountUserID") %>' OnClick="lnkDeleteItem_Click" OnClientClick="return jsDeleteConfirm()"/>
                        </ItemTemplate>
                    </asp:TemplateColumn>

                </Columns>
            </asp:DataGrid>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div class="buttonsRightDiv">
        <asp:LinkButton ID="lnkbtnActivate" runat="server" CssClass="saveButton" OnClick="lnkbtnActivate_Click" />
    </div>

    <div class="buttonsRightDiv">
        <asp:LinkButton ID="lnkbtnAdd" runat="server" CssClass="addButton" onclick="lnkbtnAdd_Click" />
    </div>

    <div class="buttonsRightDiv">
        <asp:LinkButton ID="lnkbtnImport" runat="server" CssClass="importButton" OnClick="lnkbtnImport_Click" />
    </div>

</asp:Content>
