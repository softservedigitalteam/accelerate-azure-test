
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text;

/// <summary>
/// Summary description for clsRanksView
/// </summary>
public partial class CMS_clsRanksView : System.Web.UI.Page
{
    #region CLASSES AND VARIABLES

    clsUsers clsUsers;
    DataTable dtRanksList;

    List<clsRanks> glstRanks;

    #endregion

    #region EVENT MODULES

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

dgrGrid.ItemCreated += new DataGridItemEventHandler(clsCommonFunctions.dgrGrid_PagerPopulate);

        if (!Page.IsPostBack)
        {
            PopulateFormData();
        }
        else
        {
            if (Session["glstRanks"] == null)
            {
                glstRanks = new List<clsRanks>();
                Session["glstRanks"] = glstRanks;
            }
            else
                glstRanks = (List<clsRanks>)Session["glstRanks"];
        }
            PopulateFormData();

    }

    protected string EscapeLikeValue(string valueWithoutWildcards)
    {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < valueWithoutWildcards.Length; i++)
        {
            char c = valueWithoutWildcards[i];
            if (c == '*' || c == '%' || c == '[' || c == ']')
                sb.Append("[").Append(c).Append("]");
            else if (c == '\'')
                sb.Append("''");
            else
                sb.Append(c);
        }
        return sb.ToString();
    }

    protected void lnkbtnSearch_Click(object sender, EventArgs e)
    {
        string EscapedString = EscapeLikeValue(txtSearch.Text.ToString());
        string FilterExpression ="(strTitle) LIKE '%" + EscapedString + "%'";
        DataTable dtRanksList = clsRanks.GetRanksList(FilterExpression, "");

        Session["dtRanksList"] = dtRanksList;

        PopulateFormData();
    }

    protected void lnkbtnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmRanksAddModify.aspx");
    }

    #endregion

    #region DATA GRID METHODS

    private void PopulateFormData()
    {
        //### Populate datagrid using clsRanksList object
        try
        {
            dtRanksList = new DataTable();

            if (Session["dtRanksList"] == null)
                dtRanksList = clsRanks.GetRanksList();
            else
                dtRanksList = (DataTable)Session["dtRanksList"];

            dgrGrid.DataSource = dtRanksList;

            //### Add this check to trap if the last item on the page is deleted
            if (Convert.ToInt16(dgrGrid.CurrentPageIndex) < Convert.ToInt16(Session["RanksView_CurrentPageIndex"]))
            {
                dgrGrid.CurrentPageIndex = 0;
            }
            else
            {
                if (Session["RanksView_CurrentPageIndex"] != null)
                {
                    dgrGrid.CurrentPageIndex = (int)Session["RanksView_CurrentPageIndex"];
                }
            }

            dgrGrid.DataBind();

        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    //protected void lnkDeleteItem_Click(object sender, EventArgs e)
    //{
    //    int iRankID = int.Parse((sender as LinkButton).CommandArgument);

    //    clsRanks.Delete(iRankID);

    //    PopulateFormData();
    //}

    protected void dgrGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //### Method is used when the page index is changed.
        try
        {
            Session["RanksView_CurrentPageIndex"] = e.NewPageIndex;
            dgrGrid.CurrentPageIndex = e.NewPageIndex;
            PopulateFormData();
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgrGrid_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        //### Sort Method
        dtRanksList = new DataTable();

        if (Session["dtRanksList"] == null)
            dtRanksList = clsRanks.GetRanksList();
        else
            dtRanksList = (DataTable)Session["dtRanksList"];

        DataView dvTemp = dtRanksList.DefaultView;

        dvTemp.Sort = e.SortExpression;
        dtRanksList = dvTemp.ToTable();
        Session["dtRanksList"] = dtRanksList;

        PopulateFormData();
    }

    public void dgrGrid_PopEditLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkEditItem = (HyperLink)sender;
            DataGridItem dgrItemEdit = (DataGridItem)lnkEditItem.Parent.Parent;

            //### Get the iRankID
            int iRankID = 0;
            iRankID = int.Parse(dgrItemEdit.Cells[0].Text);

            //### Add attributes to edit link
            lnkEditItem.Attributes.Add("href", "frmRanksAddModify.aspx?action=edit&iRankID=" + iRankID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

     //public void dgrGrid_PopDeleteLink(object sender, EventArgs e)
     //{
     //     try
     //     {
     //        //### Add hyperlink to datagrid item
     //        HyperLink lnkDeleteItem = (HyperLink)sender;
     //        DataGridItem dgrItemDelete = (DataGridItem)lnkDeleteItem.Parent.Parent;

     //        //### Get the iRankID
     //        int iRankID = 0;
     //        iRankID = int.Parse(dgrItemDelete.Cells[0].Text);

     //        //### Add attributes to delete link
     //        lnkDeleteItem.CssClass = "dgrDeleteLink";
     //        lnkDeleteItem.Attributes.Add("href", "frmRanksView.aspx?action=delete&iRankID=" + iRankID);
     //      }
     //      catch (Exception ex)
     //      {
     //            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
     //      }
     //}

    #endregion

    #region Ranks FILTERING

    /// <summary>
    /// Enables the AutoCompleteExtender
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    /// 
    [WebMethod]
    [ScriptMethod]
    public static string[] FilterBusinessRule(string prefixText, int count)
    {
        string[] strReturnList = new string[] { };

        string FilterExpression = "(strTitle) LIKE '%" + prefixText + "%'";
        DataTable dtRanks = clsRanks.GetRanksList(FilterExpression, "");
        List<string> glstRanks = new List<string>();

        if (dtRanks.Rows.Count > 0)
        {
            foreach (DataRow dtrRanks in dtRanks.Rows)
            {
                glstRanks.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem("" + dtrRanks["strTitle"].ToString(), dtrRanks["iRankID"].ToString()));
            }
        }
        else
            glstRanks.Add("No Ranks Available.");
        strReturnList = glstRanks.ToArray();
        return strReturnList;
    }

    #endregion
}
