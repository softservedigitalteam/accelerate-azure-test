﻿<%@ Page Title="Form Reporting" Language="C#" MasterPageFile="~/CMS/CMS.master" AutoEventWireup="true" Inherits="CMS_Reporting" Codebehind="frmReporting.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="mandatoryDiv" class="mandatoryInvalidDiv" runat="server" visible="false">
        <asp:Label ID="lblValidationMessage" Text="" runat="server"></asp:Label>
    </div>
     <div id="mandatoryDiv2" class="mandatoryValidDiv" runat="server" visible="false">
        <asp:Label ID="lblValidationMessage2" Text="" runat="server"></asp:Label>
    </div>
    <div class="controlDiv">
        <div class="imageHolderCommonDiv">
            <div class="dummyHolder"></div>
        </div>
        <div class="labelDiv leftAlign">
            <asp:RadioButton ID="rbUser" Text="User" runat="server" GroupName="FilterBy" OnCheckedChanged="rbUser_CheckedChanged" AutoPostBack="true" />
            <br /><br />
            <asp:RadioButton ID="rbGroup" Text="Group" runat="server" GroupName="FilterBy" OnCheckedChanged="rbGroup_CheckedChanged" AutoPostBack="true" />
        </div>
        <div class="fieldDiv">
        </div>
        <br class="clearingSpacer" />
    </div>
    
    <div runat="server" id="divUser" visible="false" class="controlDiv">
        <div class="imageHolderCommonDiv">
            <div class="dummyHolder"></div>
        </div>
        <div class="labelDiv">User:</div>
        <div class="fieldDiv">
            <asp:DropDownList ID="lstAccountUser" runat="server" CssClass="roundedCornerDropDownList" onblur="setValid(this, 0);"></asp:DropDownList>
        </div>
        <br class="clearingSpacer" />
    </div>
    <div runat="server" id="divGroup" visible="false" class="controlDiv">
        <div class="imageHolderCommonDiv">
            <div class="dummyHolder"></div>
        </div>
        <div class="labelDiv">Group:</div>
        <div class="fieldDiv">
            <asp:DropDownList ID="lstGroup" runat="server" CssClass="roundedCornerDropDownList" onblur="setValid(this, 0);"></asp:DropDownList>
        </div>
        <br class="clearingSpacer" />
    </div>

    <div class="Line"></div>
    <div class="buttonsRightDiv">
        <asp:LinkButton ID="lnkbtnBack" runat="server" CssClass="backButton" OnClick="lnkbtnBack_Click" OnClientClick='history.go(-1);return false;' />
        <asp:LinkButton ID="lnkbtnSave" runat="server" CssClass="saveButton" OnClick="lnkbtnSave_Click" />
        <asp:LinkButton ID="lnkbtnClear" runat="server" CssClass="clearButton" OnClick="lnkbtnClear_Click" />
    </div>
</asp:Content>

