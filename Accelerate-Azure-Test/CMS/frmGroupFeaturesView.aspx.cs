using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;
using System.Net.Mail;
using System.Text;
using System.Configuration;
using System.IO;

/// <summary>
/// Summary description for clsPortalFeaturesView
/// </summary>
public partial class CMS_clsFeaturesView : System.Web.UI.Page
{
    DataTable dtOrderItems = new DataTable();

    #region CLASSES AND VARIABLES

    clsUsers clsUsers;
    DataTable dtFeaturesList;

    List<clsPortalFeatures> glstFeatures;

    #endregion

    #region EVENT MODULES

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

        dgrGrid.ItemCreated += new DataGridItemEventHandler(clsCommonFunctions.dgrGrid_PagerPopulate);
        dgOrder.ItemCreated += new DataGridItemEventHandler(clsCommonFunctions.dgrGrid_PagerPopulate);

        if (!Page.IsPostBack)
        {
            Session["dtFeaturesList"] = null;
            PopulateFormData();
            PopulateOrderItems();
        }
        else
        {
            if (Session["glstFeatures"] == null)
            {
                glstFeatures = new List<clsPortalFeatures>();
                Session["glstFeatures"] = glstFeatures;
            }
            else
                glstFeatures = (List<clsPortalFeatures>)Session["glstFeatures"];
        }
    }

    protected string EscapeLikeValue(string valueWithoutWildcards)
    {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < valueWithoutWildcards.Length; i++)
        {
            char c = valueWithoutWildcards[i];
            if (c == '*' || c == '%' || c == '[' || c == ']')
                sb.Append("[").Append(c).Append("]");
            else if (c == '\'')
                sb.Append("''");
            else
                sb.Append(c);
        }
        return sb.ToString();
    }

    protected void lnkbtnSearch_Click(object sender, EventArgs e)
    {
        string EscapedString = EscapeLikeValue(txtSearch.Text.ToString());
        string FilterExpression ="(strTitle +' '+ strDescription) LIKE '%" + EscapedString + "%'";
        DataTable dtFeaturesList = clsPortalFeatures.GetPortalFeaturesList(FilterExpression, "");

        Session["dtFeaturesList"] = dtFeaturesList;

        PopulateFormData();
        PopulateOrderItems();
    }
    
    protected void lnkbtnSubmit_Click(object sender, EventArgs e)
    {
        if ((dtOrderItems != null))
        {
            try
            {
                PopulateFormData();
                PopulateOrderItems();
            }
            catch { }
        }
        else
        {
            PopulateFormData();
            PopulateOrderItems();
        }
        
    }

    #endregion

    #region DATA GRID METHODS

    private void PopulateFormData()
    {
        //### Populate datagrid using clsPortalFeaturesList object
        try
        {
            clsGroups clsGroups = new clsGroups(Convert.ToInt32(Request.QueryString["iGroupID"]));
            lblGroupName.Text = clsGroups.strTitle;
            dtFeaturesList = new DataTable();

            if (Session["dtFeaturesList"] == null)
                dtFeaturesList = clsPortalFeatures.GetPortalFeaturesList();
            else
                dtFeaturesList = (DataTable)Session["dtFeaturesList"];

            dgrGrid.DataSource = dtFeaturesList;

            //### Add this check to trap if the last item on the page is deleted
            if (Convert.ToInt16(dgrGrid.CurrentPageIndex) < Convert.ToInt16(Session["FeaturesView_CurrentPageIndex"]))
            {
                dgrGrid.CurrentPageIndex = 0;
            }
            else
            {
                if (Session["FeaturesView_CurrentPageIndex"] != null)
                {
                    dgrGrid.CurrentPageIndex = (int)Session["FeaturesView_CurrentPageIndex"];
                }
            }

            dgrGrid.DataBind();

        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void lnkAddItem_Click(object sender, EventArgs e)
    {
        DataTable dtCurrentGroupFeatureList = clsGroupFeatures.GetGroupFeaturesList("iGroupID=" + Convert.ToInt32(Request.QueryString["iGroupID"]), "");
        mandatoryDiv.Visible = false;
        //if(dtCurrentGroupFeatureList.Rows.Count <= 10)
        //{
            mandatoryDiv.Visible = false;
            int iPortalFeatureID = int.Parse((sender as LinkButton).CommandArgument);

            AddItemToOrderList(iPortalFeatureID);

            PopulateFormData();
            PopulateOrderItems();
        //}
        //else
        //{
        //    //A group cannot have more than 10 Features
        //    mandatoryDiv.Visible = true;
        //    lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">A group cannot have more than 10 Features</div></div>";
        //}
    }

    protected void lnkDeleteItem_Click(object sender, EventArgs e)
    {
        mandatoryDiv.Visible = false;
        int iGroupFeatureID = int.Parse((sender as LinkButton).CommandArgument);

        clsGroupFeatures.Delete(iGroupFeatureID);

        DataTable dtFeaturesList = new DataTable();

        if (((txtSearch.Text == "SEARCH FOR A FEATURE TO ADD") == false) && ((txtSearch.Text == "") == false))
        {
            string FilterExpression = "(strTitle +' '+ strDescription) LIKE '%" + txtSearch.Text + "%'";
            dtFeaturesList = clsPortalFeatures.GetPortalFeaturesList(FilterExpression, "");
        }
        else
        {
            dtFeaturesList = clsPortalFeatures.GetPortalFeaturesList();
        }

        Session["dtFeaturesList"] = dtFeaturesList;

        PopulateFormData();
        PopulateOrderItems();
    }

    protected void AddItemToOrderList(int iPortalFeatureID)
    {
        clsPortalFeatures clsPortalFeatures = new clsPortalFeatures(iPortalFeatureID);

        clsGroupFeatures clsGroupFeatures = new clsGroupFeatures();

        //clsGroupFeatures.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        //clsGroupFeatures.iAddedBy = clsUsers.iUserID;
        clsGroupFeatures.iGroupID = Convert.ToInt32(Request.QueryString["iGroupID"]);
        clsGroupFeatures.iPortalFeatureID = iPortalFeatureID;
        clsGroupFeatures.strTitle = clsPortalFeatures.strTitle;
        clsGroupFeatures.Update();
    }

    private void PopulateOrderItems()
    {
        //### Populate datagrid using clsMembersList object
        try
        {
            dtOrderItems = clsGroupFeatures.GetGroupFeaturesList("iGroupID=" + Convert.ToInt32(Request.QueryString["iGroupID"]), "");
            Session["dtOrderItems"] = dtOrderItems;

            if (Session["dtOrderItems"] == null)
                dtOrderItems = clsGroupFeatures.GetGroupFeaturesList("iGroupID=" + Convert.ToInt32(Request.QueryString["iGroupID"]), "");
            else
                dtOrderItems = (DataTable)Session["dtOrderItems"];

            dgOrder.DataSource = dtOrderItems;

            //### Add this check to trap if the last item on the page is deleted
            if (Convert.ToInt16(dgOrder.CurrentPageIndex) < Convert.ToInt16(Session["ManualOrdersView_CurrentPageIndex"]))
            {
                dgOrder.CurrentPageIndex = 0;
            }
            else
            {
                if (Session["ManualOrdersView_CurrentPageIndex"] != null)
                {
                    dgOrder.CurrentPageIndex = (int)Session["ManualOrdersView_CurrentPageIndex"];
                }
            }

            dgOrder.DataBind();
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgOrder_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //### Method is used when the page index is changed.
        try
        {
            Session["ManualOrdersView_CurrentPageIndex"] = e.NewPageIndex;
            dgOrder.CurrentPageIndex = e.NewPageIndex;

            PopulateFormData();
            PopulateOrderItems();
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgrGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //### Method is used when the page index is changed.
        try
        {
            Session["FeaturesView_CurrentPageIndex"] = e.NewPageIndex;
            dgrGrid.CurrentPageIndex = e.NewPageIndex;

            PopulateFormData();
            PopulateOrderItems();
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgrGrid_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        //### Sort Method
        dtFeaturesList = new DataTable();

        if (Session["dtFeaturesList"] == null)
            dtFeaturesList = clsPortalFeatures.GetPortalFeaturesList();
        else
            dtFeaturesList = (DataTable)Session["dtFeaturesList"];

        DataView dvTemp = dtFeaturesList.DefaultView;

        dvTemp.Sort = e.SortExpression;
        dtFeaturesList = dvTemp.ToTable();
        Session["dtFeaturesList"] = dtFeaturesList;

        PopulateFormData();
        PopulateOrderItems();
    }

    #endregion

    #region Features FILTERING

    /// <summary>
    /// Enables the AutoCompleteExtender
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    /// 
    [WebMethod]
    [ScriptMethod]
    public static string[] FilterBusinessRule(string prefixText, int count)
    {
        string[] strReturnList = new string[] { };

        string FilterExpression = "(strTitle +' '+ strDescription) LIKE '%" + prefixText + "%'";
        DataTable dtFeatures = clsPortalFeatures.GetPortalFeaturesList(FilterExpression, "");
        List<string> glstFeatures = new List<string>();

        if (dtFeatures.Rows.Count > 0)
        {
            foreach (DataRow dtrFeatures in dtFeatures.Rows)
            {
                glstFeatures.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem("" + dtrFeatures["strTitle"].ToString() +' '+"" + dtrFeatures["strDescription"].ToString(), dtrFeatures["iPortalFeatureID"].ToString()));
            }
        }
        else
            glstFeatures.Add("No Features Available.");
        strReturnList = glstFeatures.ToArray();
        return strReturnList;
    }

    #endregion
}
