using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Net.Mail;
using SD = System.Drawing;
using System.Drawing.Drawing2D;
using System.Text.RegularExpressions;

public partial class CMS_frmAccountUsersAddModify : System.Web.UI.Page
{
    clsUsers clsUsers;
    clsAccountUsers clsAccountUsers;

    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

        //### Determines if a javascript delete has been called
        if (!String.IsNullOrEmpty(Page.Request["__EVENTARGUMENT"]) && (Page.Request["__EVENTARGUMENT"].Split(':')[0] == "iRemoveImages"))
            DeleteImages(Convert.ToInt32(Page.Request["__EVENTARGUMENT"].Split(':')[1]));

        if (!IsPostBack)
        {
            popGroup();

            //### If the iAccountUserID is passed through then we want to instantiate the object with that iAccountUserID
            if ((Request.QueryString["iAccountUserID"] != "") && (Request.QueryString["iAccountUserID"] != null))
            {
                clsAccountUsers = new clsAccountUsers(Convert.ToInt32(Request.QueryString["iAccountUserID"]));

                //### Populate the form
                popFormData();
               // lstTitle.Enabled = true;
            }
            else
            {
                clsAccountUsers = new clsAccountUsers();
            }
            Session["clsAccountUsers"] = clsAccountUsers;
            
        }
        else
        {
            if (IsPostBack && FileUpload.PostedFile != null)
            {
                if (FileUpload.PostedFile.FileName.Length > 0)
                {
                    if (Session["lstImages"] == null)
                    {
                        lstImages = new List<string>();
                        Session["lstImages"] = lstImages;
                    }
                    //### Check that they have ONLY HAVE MAX NUMBER OF Images in the datalist
                    if (dlImages.Items.Count == iMaxImages)
                    {
                        mandatoryDiv.Visible = true;
                        lblValidationMessage.Text = "You can only have " + iMaxImages.ToString() + " Images.";
                        getList(lblUniquePath.Text);
                    }
                    else
                    {
                        mandatoryDiv.Visible = false;

                        string strUniquePath;
                        if (lblUniquePath.Text == "")
                        {
                            strUniquePath = GetUniquePath();
                            lblUniquePath.Text = strUniquePath;
                        }
                        else
                        {
                            strUniquePath = lblUniquePath.Text;
                        }
                        UploadImages(strUniquePath);
                        getList(strUniquePath);
                    }
                }
            }

            clsAccountUsers = (clsAccountUsers)Session["clsAccountUsers"];
        }
        //TEST
        //SendReport();
    }

    protected void lnkbtnBack_Click(object sender, EventArgs e)
    {
        //### Go back to previous page
        Response.Redirect("frmAccountUsersView.aspx");
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        //### Validate registration process
        bool bCanSave = true;

        bCanSave = clsValidation.IsNullOrEmpty(lstGroup, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtFirstName, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtSurname, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtEmailAddress, bCanSave);

        if (bCanSave == true)
        {
            mandatoryDiv.Visible = false;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceHappy.png\" alt='' title=''/><div class=\"validationMessage\">User added successfully</div></div>";
            SaveData();
        }
        else
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">Please fill out all mandatory fields - User not added</div></div>";
        }
    }

    protected void lnkbtnClear_Click(object sender, EventArgs e)
    {
        mandatoryDiv.Visible = false;

        txtFirstName.Text = "";
        clsValidation.SetValid(txtFirstName);
        txtSurname.Text = "";
        clsValidation.SetValid(txtSurname);
        txtEmailAddress.Text = "";
        clsValidation.SetValid(txtEmailAddress);
    }

    #endregion

    #region POPULATE DATA METHODS

    private void popFormData()
    {
        txtFirstName.Text = clsAccountUsers.strFirstName;
        txtSurname.Text = clsAccountUsers.strSurname;
        txtEmailAddress.Text = clsAccountUsers.strEmailAddress;
        txtPhoneNumber.Text = clsAccountUsers.strPhoneNumber;
        txtJobTitle.Text = clsAccountUsers.strJobTitle;
        lstGroup.SelectedValue = clsAccountUsers.iGroupID.ToString();

        //### Populates images
        if (!string.IsNullOrEmpty(clsAccountUsers.strPathToImages))
        {
            lblUniquePath.Text = clsAccountUsers.strPathToImages;
            getList(clsAccountUsers.strPathToImages);
            //### Set Current Master Image
            List<string> lstImagesFileNames = (List<string>)Session["lstImagesFileNames"];
            
                foreach (string strImageFileName in lstImagesFileNames)
                {
                    if (strImageFileName == clsAccountUsers.strMasterImage)
                    {
                        RadioButton rdbMasterImage = dlImages.Items[lstImagesFileNames.IndexOf(strImageFileName)].FindControl("rdbMainImage") as RadioButton;
                        rdbMasterImage.Checked = true;
                        break;
                    }
                }
        }
    }

    private void popGroup()
    {
        DataTable dtGroupsList = new DataTable();
        lstGroup.DataSource = clsGroups.GetGroupsList();

        //### Populates the drop down list with PK and TITLE;
        lstGroup.DataValueField = "iGroupID";
        lstGroup.DataTextField = "strTitle";

        //### Bind the data to the list;
        lstGroup.DataBind();

        //### Add default select option;
        lstGroup.Items.Insert(0, new ListItem("--Not Selected--", "0"));
    }

    #endregion

    #region SAVE DATA METHODS

    private void SaveData()
    {
      
        bool bIsNewUser = false;

        //removing white space from email address
        string strEmailAddress = txtEmailAddress.Text.ToString();
        strEmailAddress = strEmailAddress.Trim();
        strEmailAddress = Regex.Replace(strEmailAddress, @"\s", "");
        strEmailAddress = strEmailAddress.ToLower();

        if (clsAccountUsers.iAccountUserID == 0)
            bIsNewUser = true;

        if ((clsCommonFunctions.DoesRecordExist("tblAccountUsers", "strEmailAddress='" + strEmailAddress + "' AND bIsDeleted = 0") == true) && bIsNewUser)
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">This Email already exist, please choose a new Email</div></div>";
        }
        else
        {
                //### Add / Update
                mandatoryDiv.Visible = false;

                clsAccountUsers.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
                clsAccountUsers.iAddedBy = clsUsers.iUserID;
                clsAccountUsers.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
                clsAccountUsers.iEditedBy = clsUsers.iUserID;
                clsAccountUsers.strFirstName = txtFirstName.Text;
                clsAccountUsers.strSurname = txtSurname.Text;
                clsAccountUsers.strEmailAddress = strEmailAddress;
                clsAccountUsers.iGroupID = Convert.ToInt32(lstGroup.SelectedValue.ToString());
                int iGroupID = Convert.ToInt32(lstGroup.SelectedValue.ToString());
                clsAccountUsers.strPhoneNumber = txtPhoneNumber.Text;
                clsAccountUsers.strJobTitle = txtJobTitle.Text;

                if (bIsNewUser)
                    clsAccountUsers.bHasCompletedProfileMission = false;

                //string strHashPassword = clsCommonFunctions.GetMd5Sum("accelerate");
                string strHashPassword = clsCommonFunctions.GetMd5Sum(txtEmailAddress.Text.ToLower());
                clsAccountUsers.strPassword = strHashPassword;

                //### Images related items
                clsAccountUsers.strPathToImages = lblUniquePath.Text;
                clsAccountUsers.strMasterImage = "crop_" + GetMainImagePath(dlImages);

                string strFullPathToImage = clsAccountUsers.strPathToImages + clsAccountUsers.strMasterImage;
                clsAccountUsers.iCurrentMissionID = 0;
                clsAccountUsers.iCurrentPoints = 0;

                clsAccountUsers.Update();

                if (bIsNewUser)
                {
                    SendReport();
                }

                if (bIsNewUser)
                {
                    clsAccountUserTrackings clsAccountUserTrackings = new clsAccountUserTrackings();
                    clsAccountUserTrackings.dtAdded = DateTime.Now;
                    clsAccountUserTrackings.bHasEmailBeenSent = false;
                    clsAccountUserTrackings.iCurrentPhaseLevel = 1;
                    clsAccountUserTrackings.bIsPhase1Complete = false;
                    clsAccountUserTrackings.bIsPhase2Complete = false;
                    clsAccountUserTrackings.bIsPhase3Complete = false;

                    DataTable dtAllPhasesForThatGroup = clsGroupPhases.GetGroupPhasesList("iGroupID=" + clsAccountUsers.iGroupID, "");

                    int iCountID = 0;

                    foreach (DataRow dtrPhaseInGroup in dtAllPhasesForThatGroup.Rows)
                    {
                        ++iCountID;

                        int iCurrentPhaseID;

                        if (iCountID == 1)
                        {
                            iCurrentPhaseID = Convert.ToInt32(dtrPhaseInGroup["iPhaseID"]);
                            clsAccountUserTrackings.iPhase1ID = iCurrentPhaseID;
                            clsAccountUserTrackings.iCurrentPhaseID = iCurrentPhaseID;
                        }
                        else if (iCountID == 2)
                        {
                            iCurrentPhaseID = Convert.ToInt32(dtrPhaseInGroup["iPhaseID"]);
                            clsAccountUserTrackings.iPhase2ID = iCurrentPhaseID;
                        }
                        else if (iCountID == 3)
                        {
                            iCurrentPhaseID = Convert.ToInt32(dtrPhaseInGroup["iPhaseID"]);
                            clsAccountUserTrackings.iPhase3ID = iCurrentPhaseID;
                        }
                    }

                    DataTable dtPhase1Missions = clsPhaseMissions.GetPhaseMissionsList("iPhaseID=" + clsAccountUserTrackings.iPhase1ID, "");

                    foreach (DataRow dtrMissionsInPhase in dtPhase1Missions.Rows)
                    {
                        clsAccountUsers.iCurrentMissionID = Convert.ToInt32(dtrMissionsInPhase["iMissionID"]);
                        break;
                    }
                    //DataTable dtPhase2Missions = clsPhaseMissions.GetPhaseMissionsList("iPhaseID=" + clsAccountUserTrackings.iPhase2ID, "");
                    //DataTable dtPhase3Missions = clsPhaseMissions.GetPhaseMissionsList("iPhaseID=" + clsAccountUserTrackings.iPhase3ID, "");



                    //int iPhase1Total = dtPhase1Missions.Rows.Count;
                    //int iPhase2Total = dtPhase2Missions.Rows.Count;
                    //int iPhase3Total = dtPhase3Missions.Rows.Count;
                    //int iMissionTotal = iPhase1Total + iPhase2Total + iPhase3Total;

                    //clsAccountUserTrackings.iTotalMissionsAvailable = iMissionTotal;
                    //clsAccountUsers.Update();
                    clsAccountUserTrackings.Update();
                    //clsAccountUsers.iAccountUserTrackingID = clsAccountUserTrackings.iAccountUserTrackingID;
                    //clsAccountUsers.Update();
                    clsAccountUsers.iAccountUserTrackingID = clsAccountUserTrackings.iAccountUserTrackingID;
                    clsAccountUsers.iCurrentPhaseID = clsAccountUserTrackings.iCurrentPhaseID;
                    clsAccountUsers.Update();
                }

                Session["dtAccountUsersList"] = null;

                //### Go back to view page
                Response.Redirect("frmAccountUsersView.aspx");
            
        }
    }

    #endregion

    #region IMAGE METHODS

    List<string> lstImages;
    List<string> lstImagesFileNames;
    int iMaxImages = 3;

    string strUniqueFullPath = AppDomain.CurrentDomain.BaseDirectory + "\\AccountUsers";

    protected void btnUpload_Click(object sender, EventArgs e)
    {
        if (Session["lstImages"] == null)
        {
            lstImages = new List<string>();
            Session["lstImages"] = lstImages;
        }
        //### Check that they have ONLY HAVE MAX NUMBER OF Images in the datalist
        if (dlImages.Items.Count == iMaxImages)
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "You can only have " + iMaxImages.ToString() + " Images.";
            getList(lblUniquePath.Text);
        }
        else
        {
            mandatoryDiv.Visible = false;

            string strUniquePath;
            if (lblUniquePath.Text == "")
            {
                strUniquePath = GetUniquePath();
                lblUniquePath.Text = strUniquePath;
            }
            else
            {
                strUniquePath = lblUniquePath.Text;
            }
            UploadImages(strUniquePath);
            getList(strUniquePath);
        }
    }

    protected void btnCrop_Click(object sender, EventArgs e)
    {
        string strImageName = Session["WorkingImage"].ToString();

        //### Validate a cropped area has been selected
        if (W.Value != "" && W.Value != "0" && H.Value != "" && H.Value != "0" && X.Value != "" && Y.Value != "")
        {
            int w = Convert.ToInt32(W.Value);
            int h = Convert.ToInt32(H.Value);
            int x = Convert.ToInt32(X.Value);
            int y = Convert.ToInt32(Y.Value);

            byte[] CropImage = Crop(strUniqueFullPath + "\\" + lblUniquePath.Text + "\\" + strImageName, w, h, x, y);

            using (MemoryStream ms = new MemoryStream(CropImage, 0, CropImage.Length))
            {
                ms.Write(CropImage, 0, CropImage.Length);
                using (SD.Image CroppedImage = SD.Image.FromStream(ms, true))
                {
                    string SaveTo = strUniqueFullPath + "\\" + lblUniquePath.Text + "\\" + "crop_" + strImageName;
                    CroppedImage.Save(SaveTo, CroppedImage.RawFormat);
                }
            }

            CopyAndResizePic(strUniqueFullPath + "\\" + lblUniquePath.Text + "\\" + strImageName);
            getList(lblUniquePath.Text);
        }
        else
        {
            //### Show message
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "Select an area you wish to crop.";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "jCropUnique", "jCrop();", true);
        }
    }

    static byte[] Crop(string Img, int Width, int Height, int X, int Y)
    {
        try
        {
            using (SD.Image OriginalImage = SD.Image.FromFile(Img))
            {
                using (SD.Bitmap bmp = new SD.Bitmap(Width, Height))
                {
                    bmp.SetResolution(OriginalImage.HorizontalResolution, OriginalImage.VerticalResolution);

                    using (SD.Graphics Graphic = SD.Graphics.FromImage(bmp))
                    {
                        Graphic.SmoothingMode = SmoothingMode.AntiAlias;
                        Graphic.InterpolationMode = InterpolationMode.HighQualityBicubic;
                        Graphic.PixelOffsetMode = PixelOffsetMode.HighQuality;
                        Graphic.DrawImage(OriginalImage, new SD.Rectangle(0, 0, Width, Height), X, Y, Width, Height, SD.GraphicsUnit.Pixel);

                        MemoryStream ms = new MemoryStream();
                        bmp.Save(ms, OriginalImage.RawFormat);

                        return ms.GetBuffer();
                    }
                }
            }
        }
        catch (Exception Ex)
        {
            throw (Ex);
        }
    }

    private string GetUniquePath()
    {
        int iCount = 1;
        //### First we need to get the path
        while (System.IO.Directory.Exists(strUniqueFullPath + "\\AccountUsers" + iCount) == true)
        {
            iCount++;
        }
        return "AccountUsers" + iCount;
    }

    protected void UploadImages(String strUniquePath)
    {
        if (FileUpload.PostedFile.ContentLength > 0 && FileUpload.PostedFile.ContentLength < 1073741824)
        {

            //### Upload files to unique folder
            string strUploadFileName = "";
            strUploadFileName = System.IO.Path.GetFileName(FileUpload.PostedFile.FileName);
            string strSaveLocation = "";
            strSaveLocation = strUniqueFullPath + "\\" + strUniquePath + "\\" + strUploadFileName;

            if (!System.IO.Directory.Exists(strUniqueFullPath + "\\" + strUniquePath))
            {
                System.IO.Directory.CreateDirectory(strUniqueFullPath + "\\" + strUniquePath);
            }
            FileUpload.PostedFile.SaveAs(strSaveLocation);

            Session["WorkingImage"] = FileUpload.FileName;

            clsCommonFunctions.ResizeImage(strUniqueFullPath + "\\" + lblUniquePath.Text + "\\" + Session["WorkingImage"], strUniqueFullPath + "\\" + lblUniquePath.Text + "\\" +
                Session["WorkingImage"], 620, 620, true);

            imgCrop.ImageUrl = "../AccountUsers" + "/" + strUniquePath + "/" + strUploadFileName;
            ModalPopupExtenderCrop.Show();
            Session["WorkingImage"] = FileUpload.FileName;
        }
        else
        {
            lblValidationMessage.Text = "The file should be between 0 and 1Mb.";
            //lblUploadError.Text = "The file should be between 0 and 1Mb.";
        }
    }

    private void CopyAndResizePic(String strFullPath)
    {
        try
        {
            String strFileName;
            String strNewFilePath;

            //### Main Images
            String strLrgFileName;

            strFileName = Path.GetFileName(strFullPath);
            strNewFilePath = strFullPath.Replace(strFileName, "");
            strNewFilePath = strNewFilePath + Path.GetFileNameWithoutExtension(strFullPath);
            strNewFilePath = strNewFilePath + "_lrg";
            strNewFilePath = strNewFilePath + Path.GetExtension(strFullPath);

            File.Copy(strFullPath, strNewFilePath);
            strLrgFileName = Path.GetFileName(strNewFilePath);

            clsCommonFunctions.ResizeImage(strNewFilePath, strNewFilePath, 600, 600, false);

            //### Thumbnail
            String strSmlFileName;

            strFileName = Path.GetFileName(strFullPath);
            strNewFilePath = strFullPath.Replace(strFileName, "");
            strNewFilePath = strNewFilePath + Path.GetFileNameWithoutExtension(strFullPath);
            strNewFilePath = strNewFilePath + "_sml";
            strNewFilePath = strNewFilePath + Path.GetExtension(strFullPath);

            File.Copy(strFullPath.Replace(strFileName, "crop_") + strFileName, strNewFilePath);
            strSmlFileName = Path.GetFileName(strNewFilePath);

            clsCommonFunctions.ResizeImage(strNewFilePath, strNewFilePath, 134, 134, false);

        }
        catch (Exception ex) { }
    }

    public void getList(String strPathToFolder)
    {
        lstImages = new List<string>();
        lstImagesFileNames = new List<string>();
        try
        {
            string strPath = strPathToFolder;
            string[] files = Directory.GetFiles(strUniqueFullPath + "\\" + strPath);

            string iAccountUserID = "";
            if (!string.IsNullOrEmpty(Request.QueryString["iAccountUserID"]))
                iAccountUserID = Request.QueryString["iAccountUserID"];

            int iImagesCount = 0;

            foreach (string strName in files)
            {
                if (strName.IndexOf("_sml") != -1)
                {
                    string strHTMLImages = strName.Replace(AppDomain.CurrentDomain.BaseDirectory + "\\", "..\\");
                    strHTMLImages = strHTMLImages.Replace("\\", "/");

                    //### Generates a javascript postback for the delete method
                    String strPostBack = Page.ClientScript.GetPostBackEventReference(this, "iRemoveImages:" + iImagesCount);

                    //<a class='ImagesColorBox' href='" + strHTMLImages.Replace("_sml", "_lrg") + "'></a>

                    lstImages.Add(@"<tr style='text-align: centre;'>
                                        <td style='text-align:centre;'>
                                           <center><img src='" + strHTMLImages + @"' alt='' title='' style='border: solid 5px #ffffff;' /><br class='clr'/></center>" +
                                             "<center><div class='buttonStyle' style='padding-top: 5px;'><a href=\"javascript:" + strPostBack + "\" style='color: #ffffff;'><div class='deleteButton' style='height:25px; width:100px;'>Delete</div></div></center>" + @"
                                        </td>
                                    </tr>");
                    lstImagesFileNames.Add(Path.GetFileName(strName).Replace("_sml", ""));
                    iImagesCount++;
                }
            }
            dlImages.DataSource = lstImages;
            dlImages.DataBind();

            Session["lstImages"] = lstImages;
            Session["lstImagesFileNames"] = lstImagesFileNames;
        }
        catch (Exception ex) { }
    }

    private string GetMainImagePath(DataList dtlTarget)
    {
        string strReturn = "";

        foreach (DataListItem dliTarget in dtlTarget.Items)
        {
            RadioButton rdbMainImage = (RadioButton)dliTarget.FindControl("rdbMainImage");
            if (rdbMainImage.Checked)
            {
                lstImagesFileNames = (List<string>)Session["lstImagesFileNames"];
                strReturn = lstImagesFileNames[dliTarget.ItemIndex];
                break;
            }
        }
        return strReturn;
    }

    private void DeleteImages(int iImagesIndex)
    {

        //### Deletes all Images related to the target Images.
        lstImages = (List<string>)Session["lstImages"];
        lstImagesFileNames = (List<string>)Session["lstImagesFileNames"];

        lstImages.RemoveAt(iImagesIndex);
        Session["lstImages"] = lstImages;

        string[] files = Directory.GetFiles(strUniqueFullPath + "\\" + lblUniquePath.Text);
        foreach (string file in files)
        {
            if (Path.GetFileName(file) == lstImagesFileNames[iImagesIndex].ToString())
            {
                //### Remove all Images
                File.Delete(file);
                File.Delete(file.Replace(Path.GetExtension(file), "_sml" + Path.GetExtension(file)));
                File.Delete(file.Replace(Path.GetExtension(file), "_med" + Path.GetExtension(file)));
                File.Delete(file.Replace(Path.GetExtension(file), "_lrg" + Path.GetExtension(file)));
                break;
            }
        }
        lstImagesFileNames.RemoveAt(iImagesIndex);
        ViewState["lstImagesFileNames"] = lstImagesFileNames;
        getList(lblUniquePath.Text);
    }

    #endregion

    #region REPORT SECTION

    protected void SendReport()
    {
        string strReportName = "Welcome";

        StringBuilder strbMailBuilder = new StringBuilder();
        string strName = txtFirstName.Text.ToString();

        //removing white space from email address
        string strEmailAddress = txtEmailAddress.Text;
        strEmailAddress = strEmailAddress.Trim();
        strEmailAddress = Regex.Replace(strEmailAddress, @"\s", "");
        strEmailAddress = strEmailAddress.ToLower();

        strbMailBuilder.AppendLine("<!-- Start of Text -->");
        strbMailBuilder.AppendLine("<table width='650' bgcolor='#FFF' cellpadding='30' cellspacing='0' border='0'  border='none' align='center' class='devicewidth' options=''>"); //style='border-left:1px solid #333; border-right:1px solid #333;'
        strbMailBuilder.AppendLine("<tbody>");

        strbMailBuilder.AppendLine("<!-- Content -->");
        strbMailBuilder.AppendLine("<tr>");
        //strbMailBuilder.AppendLine("<td width='60'></td>");

        //strbMailBuilder.AppendLine("<td valign='top' style='font-family:Arial;font-size: 15px; color: #3b3b3b; text-align:left;line-height: 22px;' text=''>");
        strbMailBuilder.AppendLine("<td valign='top' style='color:black; font-family:Verdana; font-size:12px; text-align:left;' text=''>");
        strbMailBuilder.AppendLine("<p style='font-family:Verdana; font-size:12px; font-weight:bold'>Welcome to Accelerate</p>");
        strbMailBuilder.AppendLine("<br/><br/>");
        strbMailBuilder.AppendLine("Hi " + strName + ", ");
        strbMailBuilder.AppendLine("<br/><br/>");
        strbMailBuilder.AppendLine("Welcome to Deloitte. ");//strbMailBuilder.AppendLine("Congratulation on joining Deloitte Risk Advisory.As part of your onboarding experience, you will be introduced to Risk Advisory through an interactive, engaging <b>gamified</b> experience that is guaranteed tochallenge, engage and excite you.<br /><br/>");
        strbMailBuilder.AppendLine("As part of your onboarding experience, you will be introduced to Deloitte through an interactive, gamified experience that is guaranteed to challenge, engage and excite you.<br /><br/>");
        strbMailBuilder.AppendLine("Log on to the Accelerate website using the following credentials to kick-off your first mission.<br /><br/>");

        strbMailBuilder.AppendLine("Username: " + "<span style='color: #6c7480 !important; text-decoration: underline;'>" + strEmailAddress + "</span><br/>");
        strbMailBuilder.AppendLine("Password: " + "<span style='color: #6c7480 !important; text-decoration: underline;'>" + strEmailAddress + "</span><br/>");
        strbMailBuilder.AppendLine("Link: <a href='" + System.Configuration.ConfigurationManager.AppSettings["WebRoot"] + "Accelerate-Login.aspx" + "'>" + System.Configuration.ConfigurationManager.AppSettings["WebRoot"] + "</a><br/><br/>");

        strbMailBuilder.AppendLine("<p style='color:gray; font-family:Verdana; font-size:12px; font-weight:bold'>Moving forward</p>");
        //strbMailBuilder.AppendLine("<h2>Why Onboarding?</h2>");//strbMailBuilder.AppendLine("<h2>Why Gamification?</h2>");
        strbMailBuilder.AppendLine("We are committed to you and your success at Deloitte, which is why we have created a tailor-made solution that helps you jump start your career!<br/><br/>");//strbMailBuilder.AppendLine("We are committed to you and your success at Deloitte, which is why we have created a tailor made solution that you help you get a jump start on your career!Think happier, more engaged, integrated employees.<br/>");
        strbMailBuilder.AppendLine("<p style='color:gray; font-family:Verdana; font-size:12px; font-weight:bold'>What will you learn?</p>");

        strbMailBuilder.AppendLine("This programme will form the foundation for your specific service-line onboarding experience. Ultimately, you will be empowered to hit the ground running as a knowledgeable, happy and productive member of your team.<br/><br/>");
        strbMailBuilder.AppendLine("By completing missions on the system, you will earn points and ultimately will get to know your Deloitte, your business unit and your leadership better.  Learn, network and become a Deloitte Citizen.<br/><br/>");
        strbMailBuilder.AppendLine("So again, welcome to the Deloitte family. ");
        strbMailBuilder.AppendLine("We hope that your journey will be a rewarding one.<br/><br/>");
        //strbMailBuilder.AppendLine("By completing missions on the system, you will earn points and ultimately will get to know your Deloitte, your Consulting and your leadership better.<br/>");
        //strbMailBuilder.AppendLine("<i>*Completing all missions also earns you Challenger Series points, increasing your chances of winning iPads, gift vouchers and other great prizes.</i><br/>");
        //strbMailBuilder.AppendLine("info@stratagc.co.za<br/>");

        //strbMailBuilder.AppendLine("Learn,�network�and become a Deloitte Citizen.<br/>");
        //strbMailBuilder.AppendLine("The Onboarding Team<br/>");
        //strbMailBuilder.AppendLine("0725967186");
        strbMailBuilder.AppendLine("--<br/>");
        strbMailBuilder.AppendLine("<p style='font-family:Verdana; font-size:11px;'><b>The Onboarding Team</b></p>");
        strbMailBuilder.AppendLine("<p style='font-family:Verdana; font-size:11px;'>D: +27 (0)84 280 9206</p>");
        strbMailBuilder.AppendLine("<a style='font-family:Verdana; font-size:11px;' href='info@stratagc.co.za'>info@stratagc.co.za</a> | <a style='font-family:Verdana; font-size:11px;' href='www.deloitte.com'>www.deloitte.com</a><br/>");
        strbMailBuilder.AppendLine("</td>");
        strbMailBuilder.AppendLine("</tr>");

        //###END OF EMAIL
        //strbMailBuilder.AppendLine("<td valign='top' style='color:black; font-family:Verdana; font-size:12px; text-align:left;' text=''>");
        //strbMailBuilder.AppendLine("<p style='font-family:Verdana; font-size:11px;'><b>The On-boarding Team</b></p>");
        //strbMailBuilder.AppendLine("<p style='font-family:Verdana; font-size:11px;'>D: +27 (0)84 280 9206</p>");
        //strbMailBuilder.AppendLine("<p style='font-family:Verdana; font-size:11px;'>D: +27 (0)84 280 9206</p>");
        //strbMailBuilder.AppendLine("<a style='font-family:Verdana; font-size:11px;' href='info@stratagc.co.za'>info@stratagc.co.za</a> | <a style='font-family:Verdana; font-size:11px;' href='www.deloitte.com'>www.deloitte.com</a><br/><br/><br/>");
        //strbMailBuilder.AppendLine("<a href='mailto:info@stratagc.co.za'><img src=\"cid:getintouch\" alt='getintouch' /></a><br/><br/><br/>");

        //strbMailBuilder.AppendLine("</td>");
        //strbMailBuilder.AppendLine("<td width='60'></td>");
        //strbMailBuilder.AppendLine("</tr>");
        //strbMailBuilder.AppendLine("<tr>");
        //strbMailBuilder.AppendLine("<td valign='top' style='text-align:center'>");
        //strbMailBuilder.AppendLine("<a style='display:block; margin:0px 20px 0px 20px' href='https://www.facebook.com/deloitte'><img src=\"cid:FacebookEmailLinkThumb\" alt='Facebook' /></a>");
        //strbMailBuilder.AppendLine("<a style='display:block; margin:0px 5px 0px 5px' href='https://twitter.com/Deloitte'><img src=\"cid:TwitterEmailLinkThumb\" alt='Twitter' /></a>");
        //strbMailBuilder.AppendLine("<a style='display:block; margin:0px 5px 0px 5px' href='http://www.linkedin.com/company/deloitte'><img src=\"cid:LinkedInEmailLinkThumb\" alt='LinkedIn' /></a>");
        //strbMailBuilder.AppendLine("<a style='display:block; margin:0px 5px 0px 5px' href='https://plus.google.com/+Deloitte/posts'><img src=\"cid:GooglePEmailLinkThumb\" alt='Google' /></a>");
        //strbMailBuilder.AppendLine("<a style='display:block; margin:0px 5px 0px 5px' href='http://www.pinterest.com/deloitte/'><img src=\"cid:PinInEmailLinkThumb\" alt='PinInterest' /></a>");
        //strbMailBuilder.AppendLine("<a href='#'><img src=\"cid:MailEmailLinkThumb\" alt='Mail' /></a>");
        //strbMailBuilder.AppendLine("<br/><br/><br/>");
        //strbMailBuilder.AppendLine("</td>");

        //strbMailBuilder.AppendLine("</tr>");

        //strbMailBuilder.AppendLine("</br>");

        //strbMailBuilder.AppendLine("<tr>");
        //strbMailBuilder.AppendLine("<td valign='top' style='color:gray; font-family:Verdana; font-size:10px; text-align:left;' text=''>");
        //strbMailBuilder.AppendLine("Deloitte refers to one or more of Deloitte Touche Tohmatsu Limited, a UK private ");
        //strbMailBuilder.AppendLine("company limited by guarantee (DTTL), its network of member firms and their related ");
        //strbMailBuilder.AppendLine("entities. DTTL and each of its member firms are legally separate and independent ");
        //strbMailBuilder.AppendLine("entities. DTTL (also referred to as �Deloitte Global�) does not provide services to ");
        //strbMailBuilder.AppendLine("clients. Please see www.deloitte.com/about for a more detailed description of DTTL and its member firms.<br/><br/>");

        //strbMailBuilder.AppendLine("This communication is for internal distribution and use only among personnel of ");
        //strbMailBuilder.AppendLine("Deloitte Touche Tohmatsu Limited, its member firms, and their related entities ");
        //strbMailBuilder.AppendLine("(collectively, the �Deloitte network�). None of the Deloitte network shall be responsible ");
        //strbMailBuilder.AppendLine("for any loss whatsoever sustained by any person who relies on this communication.<br/><br/>");

        //strbMailBuilder.AppendLine("� 2016. For information, contact Deloitte Touche Tohmatsu Limited<br/><br/>");

        //strbMailBuilder.AppendLine("<p style='color:black; font-family:Verdana; font-size:9px;'>To no longer receive emails about this topic please send a return email to the sender with the word �Unsubscribe� in the subject line.</p>");

        //strbMailBuilder.AppendLine("</td>");
        //strbMailBuilder.AppendLine("</tr>");



        strbMailBuilder.AppendLine("<!-- End of Content -->");

        strbMailBuilder.AppendLine("</tbody>");
        strbMailBuilder.AppendLine("</table>");
        strbMailBuilder.AppendLine("<!-- End of Text -->");

        //strbMailBuilder.AppendLine("Hi " + txtFirstName.Text + ",<br /><br />");
        //strbMailBuilder.AppendLine("Congratulations on joining Deloitte.<br/>As part of your onboarding experience, you will be introduced to Deloitte through an interactive, engaging gamified experience that is guaranteed tochallenge, engage and excite you.<br /><br/>");//strbMailBuilder.AppendLine("Congratulation on joining Deloitte Risk Advisory.As part of your onboarding experience, you will be introduced to Risk Advisory through an interactive, engaging <b>gamified</b> experience that is guaranteed tochallenge, engage and excite you.<br /><br/>");
        //strbMailBuilder.AppendLine("Login to the Accelerate website using the following credentials to kick-off your first mission.<br /><br/>");//strbMailBuilder.AppendLine("Login to the Accelerate website using the following credentials to kick-off your first mission.<br /><br/>");

        //strbMailBuilder.AppendLine("Username: " + txtEmailAddress.Text + "<br/>");
        //strbMailBuilder.AppendLine("Password: " + txtEmailAddress.Text + "<br/>");
        //strbMailBuilder.AppendLine("Link: <a href='" + System.Configuration.ConfigurationManager.AppSettings["WebRoot"] + "Accelerate-Login.aspx" + "'>" + System.Configuration.ConfigurationManager.AppSettings["WebRoot"] + "</a><br/><br/>");

        //strbMailBuilder.AppendLine("<h2>Why Onboarding?</h2>");//strbMailBuilder.AppendLine("<h2>Why Gamification?</h2>");
        //strbMailBuilder.AppendLine("We are committed to you and your success at Deloitte, which is why we have created a tailor made solution that helps you jump start on your career!<br/>");//strbMailBuilder.AppendLine("We are committed to you and your success at Deloitte, which is why we have created a tailor made solution that you help you get a jump start on your career!Think happier, more engaged, integrated employees.<br/>");
        //strbMailBuilder.AppendLine("<h2>What will you learn?</h2>");
        //strbMailBuilder.AppendLine("By completing missions on the system, you will earn points and ultimately will get to know your Deloitte, your Consulting and your leadership better.<br/>");
        ////strbMailBuilder.AppendLine("<i>*Completing all missions also earns you Challenger Series points, increasing your chances of winning iPads, gift vouchers and other great prizes.</i><br/>");

        //strbMailBuilder.AppendLine("Learn,�network�and become a Deloitte Citizen.<br/>");
        //strbMailBuilder.AppendLine("Regards,<br/>");
        //strbMailBuilder.AppendLine("The On-boarding Team<br/>");
        //strbMailBuilder.AppendLine("info@stratagc.co.za<br/>");
        //strbMailBuilder.AppendLine("0842809206");
        //strbMailBuilder.AppendLine("</td>");
        //strbMailBuilder.AppendLine("<td width='60'></td>");
        //strbMailBuilder.AppendLine("</tr>");
        //strbMailBuilder.AppendLine("<!-- End of Content -->");

        //strbMailBuilder.AppendLine("</td>");
        //strbMailBuilder.AppendLine("<td width='20'></td>");
        //strbMailBuilder.AppendLine("</tr>");
        //strbMailBuilder.AppendLine("</tbody>");
        //strbMailBuilder.AppendLine("</table>");
        //strbMailBuilder.AppendLine("</td>");
        //strbMailBuilder.AppendLine("</tr>");
        //strbMailBuilder.AppendLine("<!-- Spacing -->");
        //strbMailBuilder.AppendLine("<tr>");
        //strbMailBuilder.AppendLine("<td height='50'></td>");
        //strbMailBuilder.AppendLine("</tr>");
        //strbMailBuilder.AppendLine("<!-- End of Spacing -->");
        //strbMailBuilder.AppendLine("</tbody>");
        //strbMailBuilder.AppendLine("</table>");
        //strbMailBuilder.AppendLine("</td>");
        //strbMailBuilder.AppendLine("</tr>");
        //strbMailBuilder.AppendLine("</tbody>");
        //strbMailBuilder.AppendLine("</table>");
        //strbMailBuilder.AppendLine("<!-- End of textbanner -->");

        Attachment[] empty = new Attachment[] { };

        
        // onboarding@deloitteactivate.co.za
        //testing all email sending

        //emailComponent.SendMail("andrew@softservedigital.co.za", txtEmailAddress.Text, "", "", strReportName, strbMailBuilder.ToString(), "Heading", "Subheading", empty, true);
        //finalEmail.SendMail("andrew@softservedigital.co.za", txtEmailAddress.Text, "", "", strReportName, strbMailBuilder.ToString(), "Heading", "Subheading", empty, true);
        //missionCompletionEmail.SendMail("andrew@softservedigital.co.za", txtEmailAddress.Text, "", "", strReportName, strbMailBuilder.ToString(), "Heading", "Subheading", empty, true);
        //notificationEmail.SendMail("andrew@softservedigital.co.za", txtEmailAddress.Text, "", "", strReportName, strbMailBuilder.ToString(), "Heading", "Subheading", empty, true);
        //passwordReminder.SendMail("andrew@softservedigital.co.za", txtEmailAddress.Text, "", "", strReportName, strbMailBuilder.ToString(), "Heading", "Subheading", empty, true);
        //PhaseCompletionEmail.SendMail("andrew@softservedigital.co.za", txtEmailAddress.Text, "", "", strReportName, strbMailBuilder.ToString(), "Heading", "Subheading", empty, true);
        //reminderEmail.SendMail("andrew@softservedigital.co.za", txtEmailAddress.Text, "", "", strReportName, strbMailBuilder.ToString(), "Heading", "Subheading", empty, true);
        string sendMailTo = txtEmailAddress.Text.ToString();
        try
        {
            // ### TEST ###
            //emailComponentNew.SendMail("jamie@softservedigital.co.za", "jamie@softservedigital.co.za", "", "", strReportName, strbMailBuilder.ToString(), "Heading", "Subheading", empty, true);
            emailComponentNew.SendMail("noreply@deloitte.co.za", strEmailAddress, "", "", strReportName, strbMailBuilder.ToString(), "Heading", "Subheading", empty, true);
        }
        catch(Exception ex)
        {
            lblValidationMessage.Text = "<div class=\"validationImageCorrectLogin\"></div><div class=\"validationLabel\">Your email could not be sent to " + sendMailTo.ToString() +". </div>";
            lblValidationMessage.Text += ex.ToString();
        }
        //### Redirect
        lblValidationMessage.Text = "<div class=\"validationImageCorrectLogin\"></div><div class=\"validationLabel\">Your email has been sent.</div>";
    }

    #endregion


}
