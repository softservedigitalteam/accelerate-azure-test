
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text;

/// <summary>
/// Summary description for clsForumCommentsView
/// </summary>
public partial class CMS_clsForumCommentsView : System.Web.UI.Page
{
    #region CLASSES AND VARIABLES

    clsUsers clsUsers;
    DataTable dtForumCommentsList;

    List<clsForumComments> glstForumComments;

    #endregion

    #region EVENT MODULES

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

dgrGrid.ItemCreated += new DataGridItemEventHandler(clsCommonFunctions.dgrGrid_PagerPopulate);

        if (!Page.IsPostBack)
        {
            Session["dtForumCommentsList"] = null;
            PopulateFormData();
        }
        else
        {
            if (Session["glstForumComments"] == null)
            {
                glstForumComments = new List<clsForumComments>();
                Session["glstForumComments"] = glstForumComments;
            }
            else
                glstForumComments = (List<clsForumComments>)Session["glstForumComments"];
        }
            PopulateFormData();

    }

    protected string EscapeLikeValue(string valueWithoutWildcards)
    {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < valueWithoutWildcards.Length; i++)
        {
            char c = valueWithoutWildcards[i];
            if (c == '*' || c == '%' || c == '[' || c == ']')
                sb.Append("[").Append(c).Append("]");
            else if (c == '\'')
                sb.Append("''");
            else
                sb.Append(c);
        }
        return sb.ToString();
    }

    protected void lnkbtnSearch_Click(object sender, EventArgs e)
    {
        string EscapedString = EscapeLikeValue(txtSearch.Text.ToString());
        string FilterExpression ="(strComment) LIKE '%" + EscapedString + "%'";
        DataTable dtForumCommentsList = clsForumComments.GetForumCommentsList(FilterExpression, "");

        Session["dtForumCommentsList"] = dtForumCommentsList;

        PopulateFormData();
    }

    protected void lnkbtnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmForumCommentsAddModify.aspx");
    }

    #endregion

    #region DATA GRID METHODS

    private void PopulateFormData()
    {
        //### Populate datagrid using clsForumCommentsList object
        try
        {
            dtForumCommentsList = new DataTable();

            if (Session["dtForumCommentsList"] == null)
                dtForumCommentsList = clsForumComments.GetForumCommentsList();
            else
                dtForumCommentsList = (DataTable)Session["dtForumCommentsList"];

            dgrGrid.DataSource = dtForumCommentsList;

            //### Add this check to trap if the last item on the page is deleted
            if (Convert.ToInt16(dgrGrid.CurrentPageIndex) < Convert.ToInt16(Session["ForumCommentsView_CurrentPageIndex"]))
            {
                dgrGrid.CurrentPageIndex = 0;
            }
            else
            {
                if (Session["ForumCommentsView_CurrentPageIndex"] != null)
                {
                    dgrGrid.CurrentPageIndex = (int)Session["ForumCommentsView_CurrentPageIndex"];
                }
            }

            dgrGrid.DataBind();

        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void lnkDeleteItem_Click(object sender, EventArgs e)
    {
        int iForumCommentID = int.Parse((sender as LinkButton).CommandArgument);

        clsForumComments.Delete(iForumCommentID);

        DataTable dtForumCommentsList = new DataTable();

        if (((txtSearch.Text == "SEARCH FOR A FORUM COMMENT") == false) && ((txtSearch.Text == "") == false))
        {
            string FilterExpression = "(strComment) LIKE '%" + txtSearch.Text + "%'";
            dtForumCommentsList = clsForumComments.GetForumCommentsList(FilterExpression, "");
        }
        else
        {
            dtForumCommentsList = clsForumComments.GetForumCommentsList();
        }

        Session["dtForumCommentsList"] = dtForumCommentsList;

        PopulateFormData();
    }

    protected void dgrGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //### Method is used when the page index is changed.
        try
        {
            Session["ForumCommentsView_CurrentPageIndex"] = e.NewPageIndex;
            dgrGrid.CurrentPageIndex = e.NewPageIndex;
            PopulateFormData();
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgrGrid_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        //### Sort Method
        dtForumCommentsList = new DataTable();

        if (Session["dtForumCommentsList"] == null)
            dtForumCommentsList = clsForumComments.GetForumCommentsList();
        else
            dtForumCommentsList = (DataTable)Session["dtForumCommentsList"];

        DataView dvTemp = dtForumCommentsList.DefaultView;

        dvTemp.Sort = e.SortExpression;
        dtForumCommentsList = dvTemp.ToTable();
        Session["dtForumCommentsList"] = dtForumCommentsList;

        PopulateFormData();
    }

    public void dgrGrid_PopEditLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkEditItem = (HyperLink)sender;
            DataGridItem dgrItemEdit = (DataGridItem)lnkEditItem.Parent.Parent;

            //### Get the iForumCommentID
            int iForumCommentID = 0;
            iForumCommentID = int.Parse(dgrItemEdit.Cells[0].Text);

            //### Add attributes to edit link
            lnkEditItem.Attributes.Add("href", "frmForumCommentsAddModify.aspx?action=edit&iForumCommentID=" + iForumCommentID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

     public void dgrGrid_PopDeleteLink(object sender, EventArgs e)
     {
          try
          {
             //### Add hyperlink to datagrid item
             HyperLink lnkDeleteItem = (HyperLink)sender;
             DataGridItem dgrItemDelete = (DataGridItem)lnkDeleteItem.Parent.Parent;

             //### Get the iForumCommentID
             int iForumCommentID = 0;
             iForumCommentID = int.Parse(dgrItemDelete.Cells[0].Text);

             //### Add attributes to delete link
             lnkDeleteItem.CssClass = "dgrDeleteLink";
             lnkDeleteItem.Attributes.Add("href", "frmForumCommentsView.aspx?action=delete&iForumCommentID=" + iForumCommentID);
           }
           catch (Exception ex)
           {
                 Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
           }
     }

    #endregion

    #region ForumComments FILTERING

    /// <summary>
    /// Enables the AutoCompleteExtender
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    /// 
    [WebMethod]
    [ScriptMethod]
    public static string[] FilterBusinessRule(string prefixText, int count)
    {
        string[] strReturnList = new string[] { };

        string FilterExpression = "(strComment) LIKE '%" + prefixText + "%'";
        DataTable dtForumComments = clsForumComments.GetForumCommentsList(FilterExpression, "");
        List<string> glstForumComments = new List<string>();

        if (dtForumComments.Rows.Count > 0)
        {
            foreach (DataRow dtrForumComments in dtForumComments.Rows)
            {
                glstForumComments.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem("" + dtrForumComments["strComment"].ToString(), dtrForumComments["iForumCommentID"].ToString()));
            }
        }
        else
            glstForumComments.Add("No ForumComments Available.");
        strReturnList = glstForumComments.ToArray();
        return strReturnList;
    }

    #endregion
}
