using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class CMS_frmDefaultImagesAddModify : System.Web.UI.Page
{
    clsUsers clsUsers;
    clsDefaultImages clsDefaultImages;

    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

             //### Determines if a javascript delete has been called
             if (!String.IsNullOrEmpty(Page.Request["__EVENTARGUMENT"]) && (Page.Request["__EVENTARGUMENT"].Split(':')[0] == "iRemoveImages"))
             DeleteImages(Convert.ToInt32(Page.Request["__EVENTARGUMENT"].Split(':')[1]));
        if (!IsPostBack)
        {

            //### If the iDefaultImageID is passed through then we want to instantiate the object with that iDefaultImageID
            if ((Request.QueryString["iDefaultImageID"] != "") && (Request.QueryString["iDefaultImageID"] != null))
            {
                clsDefaultImages = new clsDefaultImages(Convert.ToInt32(Request.QueryString["iDefaultImageID"]));

                //### Populate the form
                popFormData();
            }
            else
            {
                clsDefaultImages = new clsDefaultImages();
            }
            Session["clsDefaultImages"] = clsDefaultImages;
        }
        else
        {
            if (IsPostBack && FileUpload.PostedFile != null)
            {
                if (FileUpload.PostedFile.FileName.Length > 0)
                {
                    if (Session["lstImages"] == null)
                    {
                        lstImages = new List<string>();
                        Session["lstImages"] = lstImages;
                    }
                    //### Check that they have ONLY HAVE MAX NUMBER OF Images in the datalist
                    if (dlImages.Items.Count == iMaxImages)
                    {
                        mandatoryDiv.Visible = true;
                        lblValidationMessage.Text = "You can only have " + iMaxImages.ToString() + " Images.";
                        getList(lblUniquePath.Text);
                    }
                    else
                    {
                        mandatoryDiv.Visible = false;

                        string strUniquePath;
                        if (lblUniquePath.Text == "")
                        {
                            strUniquePath = GetUniquePath();
                            lblUniquePath.Text = strUniquePath;
                        }
                        else
                        {
                            strUniquePath = lblUniquePath.Text;
                        }
                        UploadImages(strUniquePath);
                        getList(strUniquePath);
                    }
                }
            }

            clsDefaultImages = (clsDefaultImages)Session["clsDefaultImages"];
        }
    }

    protected void lnkbtnBack_Click(object sender, EventArgs e)
    {
        //### Go back to previous page
        Response.Redirect("frmDefaultImagesView.aspx");
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        //### Validate registration process
        bool bCanSave = true;

       bCanSave = clsValidation.IsNullOrEmpty(txtTitle, bCanSave);
       bCanSave = clsValidation.IsNullOrEmpty(txtDescription, bCanSave);

        if (bCanSave == true)
        {
            mandatoryDiv.Visible = false;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceHappy.png\" alt='' title=''/><div class=\"validationMessage\">Achievement added successfully</div></div>";
            SaveData();
        }
        else
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">Please fill out all mandatory fields - Achievement not added</div></div>";
        }
    }

    protected void lnkbtnClear_Click(object sender, EventArgs e)
    {
        mandatoryDiv.Visible = false;

        txtTitle.Text = "";
        clsValidation.SetValid(txtTitle);
        txtDescription.Text = "";
        clsValidation.SetValid(txtDescription);
    }

    #endregion 

    #region POPULATE DATA METHODS

    private void popFormData()
    {
         txtTitle.Text = clsDefaultImages.strTitle;
         txtDescription.Text = clsDefaultImages.strDescription;

         //### Populates images
             lblUniquePath.Text = clsDefaultImages.strPathToImages;
             getList(lblUniquePath.Text);
             //### Set Current Master Image
             List<string> lstImagesFileNames = (List<string>)Session["lstImagesFileNames"];
             try
             {
                 foreach (string strImageFileName in lstImagesFileNames)
                 {
                     if (strImageFileName == clsDefaultImages.strMasterImage)
                     {
                         RadioButton rdbMasterImage = dlImages.Items[lstImagesFileNames.IndexOf(strImageFileName)].FindControl("rdbMainImage") as RadioButton;
                         rdbMasterImage.Checked = true;
                         break;
                     }
                 }
             }
             catch { }
    }
    
    #endregion

    #region SAVE DATA METHODS

    private void SaveData()
    {
        //### Add / Update
        clsDefaultImages.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsDefaultImages.iAddedBy = clsUsers.iUserID;
        clsDefaultImages.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsDefaultImages.iEditedBy = clsUsers.iUserID;
        clsDefaultImages.strTitle = txtTitle.Text;
        clsDefaultImages.strDescription = txtDescription.Text;

        //### Images related items
        clsDefaultImages.strMasterImage = GetMainImagePath(dlImages);
        clsDefaultImages.strPathToImages = lblUniquePath.Text;

        clsDefaultImages.Update();

        Session["dtDefaultImagesList"] = null;

        //### Go back to view page
        Response.Redirect("frmDefaultImagesView.aspx");
    }

    #endregion
    #region IMAGE METHODS

    List<string> lstImages;
    List<string> lstImagesFileNames;
    int iMaxImages = 1;
    string strUniqueFullPath = AppDomain.CurrentDomain.BaseDirectory + "\\DefaultImages";

    protected void btnUpload_Click(object sender, EventArgs e)
    {
        if (Session["lstImages"] == null)
        {
            lstImages = new List<string>();
            Session["lstImages"] = lstImages;
        }

        //### Check that they have ONLY HAVE MAX NUMBER OF Images in the datalist
        if (dlImages.Items.Count == iMaxImages)
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "You can only have " + iMaxImages.ToString() + " Images.";
            getList(lblUniquePath.Text);
        }
        else
        {
            mandatoryDiv.Visible = false;

            string strUniquePath;
            if (lblUniquePath.Text == "")
            {
                strUniquePath = GetUniquePath();
                lblUniquePath.Text = strUniquePath;
            }
            else
            {
                strUniquePath = lblUniquePath.Text;
            }
            UploadImages(strUniquePath);
            getList(strUniquePath);
        }

        //if (dlImages.Items.Count == iMaxImages)
        //{
        //    mandatoryDiv.Visible = true;
        //    lblValidationMessage.Text = "You can only have " + iMaxImages.ToString() + " Images.";
        //    getList();
        //}
        //else
        //{
        //    mandatoryDiv.Visible = false;

        //    UploadImages();
        //    getList();
        //}
    }
    private string GetUniquePath()
    {
        int iCount = 1;
        //### First we need to get the path
        while (System.IO.Directory.Exists(strUniqueFullPath + "\\DefaultImages" + iCount) == true)
        {
            iCount++;
        }
        return "DefaultImages" + iCount;
    }

    protected void UploadImages(String strUniquePath)
    {
        if (FileUpload.PostedFile.ContentLength > 0 && FileUpload.PostedFile.ContentLength < 1073741824)
        {

            //### Upload files to unique folder
            string strUploadFileName = "";
            strUploadFileName = System.IO.Path.GetFileName(FileUpload.PostedFile.FileName);
            string strSaveLocation = "";
            strSaveLocation = strUniqueFullPath + "\\" + strUniquePath + "\\" + strUploadFileName;

            if (!System.IO.Directory.Exists(strUniqueFullPath + "\\" + strUniquePath))
            {
                System.IO.Directory.CreateDirectory(strUniqueFullPath + "\\" + strUniquePath);
            }
            FileUpload.PostedFile.SaveAs(strSaveLocation);

            CopyAndResizePic(strSaveLocation);
            getList(strUniqueFullPath + "\\" + strUniquePath);
        }
        else
        {
            lblValidationMessage.Text = "The file should be between 0 and 1Mb.";
            lblUploadError.Text = "The file should be between 0 and 1Mb.";
        }
    }

    public void getList(String strPathToFolder)
    {
        lstImages = new List<string>();
        lstImagesFileNames = new List<string>();
        try
        {
            string strPath = strPathToFolder;
            string[] files = Directory.GetFiles(strUniqueFullPath + "\\" + strPath);

            string iDefaultImageID = "";
            if (!string.IsNullOrEmpty(Request.QueryString["iDefaultImageID"]))
                iDefaultImageID = Request.QueryString["iDefaultImageID"];

            int iImagesCount = 0;

            foreach (string strName in files)
            {
                if (strName.IndexOf("_sml") != -1)
                {
                    string strHTMLImages = strName.Replace(AppDomain.CurrentDomain.BaseDirectory + "\\", "..\\");
                    strHTMLImages = strHTMLImages.Replace("\\", "/");

                    //### Generates a javascript postback for the delete method
                    String strPostBack = Page.ClientScript.GetPostBackEventReference(this, "iRemoveImages:" + iImagesCount);

                    lstImages.Add(@"<tr style='text-align: centre;'>
                                        <td style='text-align: centre;'>
                                            <a class='ImagesColorBox' href='" + strHTMLImages.Replace("_sml", "_lrg") + "'><img src='" + strHTMLImages + @"' alt='' title='' style='border: solid 5px #ffffff;' /></a><br /><br />" +
                                            "<center><div class='buttonStyle' style='padding-top: 5px;'><a href=\"javascript:" + strPostBack + "\" style='color: #ffffff;'><div class='deleteButton'></div></a></div></center>" + @"
                                        </td>
                                    </tr>");
                    lstImagesFileNames.Add(Path.GetFileName(strName).Replace("_sml", ""));
                    iImagesCount++;
                }
            }
            dlImages.DataSource = lstImages;
            dlImages.DataBind();

            Session["lstImages"] = lstImages;
            Session["lstImagesFileNames"] = lstImagesFileNames;
        }
        catch (Exception ex) { }
    }

    //protected void UploadImages()
    //{      
    //    if (FileUpload.PostedFile.ContentLength > 0 && FileUpload.PostedFile.ContentLength < 1073741824)
    //    {
    //        //### Upload files to unique folder
    //        string strUploadFileName = "";
    //        strUploadFileName = System.IO.Path.GetFileName(FileUpload.PostedFile.FileName);
    //        string strSaveLocation = "";
    //        lblUniquePath.Text = GetUniquePath();
    //        strSaveLocation = strUniqueFullPath + "\\" + lblUniquePath.Text;

    //        if (!System.IO.Directory.Exists(strUniqueFullPath + "\\" + lblUniquePath.Text))
    //        {
    //            System.IO.Directory.CreateDirectory(strUniqueFullPath + "\\" + lblUniquePath.Text);
    //        }
    //        FileUpload.PostedFile.SaveAs(strSaveLocation);

    //        CopyAndResizePic(strSaveLocation);

    //        getList();
    //    }
    //    else
    //    {
    //        lblValidationMessage.Text = "The file should be between 0 and 1Mb.";
    //        lblUploadError.Text = "The file should be between 0 and 1Mb.";
    //    }
    //}

    private void CopyAndResizePic(String strFullPath)
    {
        try
        {

            String strFileName;
            String strNewFilePath;

            //### Main Images
            String strLrgFileName;

            strFileName = Path.GetFileName(strFullPath);
            strNewFilePath = strFullPath.Replace(strFileName, "");
            strNewFilePath = strNewFilePath + Path.GetFileNameWithoutExtension(strFullPath);
            strNewFilePath = strNewFilePath + "_lrg";
            strNewFilePath = strNewFilePath + Path.GetExtension(strFullPath);

            File.Copy(strFullPath, strNewFilePath);
            strLrgFileName = Path.GetFileName(strNewFilePath);

            clsCommonFunctions.ResizeImage(strNewFilePath, strNewFilePath, 600, 600, false);

            //### Thumbnail
            String strSmlFileName;

            strFileName = Path.GetFileName(strFullPath);
            strNewFilePath = strFullPath.Replace(strFileName, "");
            strNewFilePath = strNewFilePath + Path.GetFileNameWithoutExtension(strFullPath);
            strNewFilePath = strNewFilePath + "_sml";
            strNewFilePath = strNewFilePath + Path.GetExtension(strFullPath);

            File.Copy(strFullPath, strNewFilePath);
            strSmlFileName = Path.GetFileName(strNewFilePath);

            clsCommonFunctions.ResizeImage(strNewFilePath, strNewFilePath, 140, 140, false);
            
        }
        catch (Exception ex){}
    }


//    public void getList()
//    {
//        lstImages = new List<string>();
//        lstImagesFileNames = new List<string>();
//        try
//        {
//            string[] files = Directory.GetFiles(strUniqueFullPath + "//" + clsDefaultImages.strPathToImages);

//            string iDefaultImageID = "";
//            if (!string.IsNullOrEmpty(Request.QueryString["iDefaultImageID"]))
//                iDefaultImageID = Request.QueryString["iDefaultImageID"];

//            int iImagesCount = 0;

//            foreach (string strName in files)
//            {
//                if (strName.IndexOf("_sml") != -1)
//                {
//                    string strHTMLImages = strName.Replace(AppDomain.CurrentDomain.BaseDirectory + "\\", "..\\");
//                    strHTMLImages = strHTMLImages.Replace("\\", "/");

//                    //### Generates a javascript postback for the delete method
//                    String strPostBack = Page.ClientScript.GetPostBackEventReference(this, "iRemoveImages:" + iImagesCount);

//                    lstImages.Add(@"<tr style='text-align: centre;'>
//                                        <td style='text-align: centre;'>
//                                            <a class='ImagesColorBox' href='" + strHTMLImages.Replace("_sml", "_lrg") + "'><img src='" + strHTMLImages + @"' alt='' title='' style='border: solid 5px #ffffff;' /></a><br /><br />" +
//                                            "<center><div class='buttonStyle' style='padding-top: 5px;'><a href=\"javascript:" + strPostBack + "\" style='color: #ffffff;'><div class='deleteButton'></div></a></div></center>" + @"
//                                        </td>
//                                    </tr>");
//                    lstImagesFileNames.Add(Path.GetFileName(strName).Replace("_sml", ""));
//                    iImagesCount++;
//                }
//            }
//            dlImages.DataSource = lstImages;
//            dlImages.DataBind();

//            Session["lstImages"] = lstImages;
//            Session["lstImagesFileNames"] = lstImagesFileNames;
//        }
//        catch (Exception ex) {}
//    }

    private string GetMainImagePath(DataList dtlTarget)
    {
        string strReturn = "";

        foreach (DataListItem dliTarget in dtlTarget.Items)
        {
            RadioButton rdbMainImage = (RadioButton)dliTarget.FindControl("rdbMainImage");
            if (rdbMainImage.Checked)
            {
                lstImagesFileNames = (List<string>)Session["lstImagesFileNames"];
                strReturn = lstImagesFileNames[dliTarget.ItemIndex];
                break;
            }
        }
        return strReturn;
    }

    private void DeleteImages(int iImagesIndex)
    {

        //### Deletes all Images related to the target Images.
        lstImages = (List<string>)Session["lstImages"];
        lstImagesFileNames = (List<string>)Session["lstImagesFileNames"];

        lstImages.RemoveAt(iImagesIndex);
        Session["lstImages"] = lstImages;

        string[] files = Directory.GetFiles(strUniqueFullPath + "\\" + lblUniquePath.Text);
        foreach (string file in files)
        {
            if (Path.GetFileName(file) == lstImagesFileNames[iImagesIndex].ToString())
            {
                //### Remove all Images
                File.Delete(file);
                File.Delete(file.Replace(Path.GetExtension(file), "_sml" + Path.GetExtension(file)));
                File.Delete(file.Replace(Path.GetExtension(file), "_med" + Path.GetExtension(file)));
                File.Delete(file.Replace(Path.GetExtension(file), "_lrg" + Path.GetExtension(file)));
                break;
            }
        }
        lstImagesFileNames.RemoveAt(iImagesIndex);
        ViewState["lstImagesFileNames"] = lstImagesFileNames;
        getList(lblUniquePath.Text);
    }

    #endregion
}