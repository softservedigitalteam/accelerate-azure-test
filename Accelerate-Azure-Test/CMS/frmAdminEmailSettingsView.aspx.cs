
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text;

/// <summary>
/// Summary description for clsAdminEmailSettingsView
/// </summary>
public partial class CMS_clsAdminEmailSettingsView : System.Web.UI.Page
{
    #region CLASSES AND VARIABLES

    clsUsers clsUsers;
    DataTable dtAdminEmailSettingsList;

    List<clsAdminEmailSettings> glstAdminEmailSettings;

    #endregion

    #region EVENT MODULES

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

dgrGrid.ItemCreated += new DataGridItemEventHandler(clsCommonFunctions.dgrGrid_PagerPopulate);

        if (!Page.IsPostBack)
        {
            Session["dtAdminEmailSettingsList"] = null;
            PopulateFormData();
        }
            PopulateFormData();

    }

    protected string EscapeLikeValue(string valueWithoutWildcards)
    {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < valueWithoutWildcards.Length; i++)
        {
            char c = valueWithoutWildcards[i];
            if (c == '*' || c == '%' || c == '[' || c == ']')
                sb.Append("[").Append(c).Append("]");
            else if (c == '\'')
                sb.Append("''");
            else
                sb.Append(c);
        }
        return sb.ToString();
    }

    protected void lnkbtnSearch_Click(object sender, EventArgs e)
    {
        string EscapedString = EscapeLikeValue(txtSearch.Text.ToString());
        string FilterExpression ="() LIKE '%" + EscapedString + "%'";
        DataTable dtAdminEmailSettingsList = clsAdminEmailSettings.GetAdminEmailSettingsList(FilterExpression, "");

        Session["dtAdminEmailSettingsList"] = dtAdminEmailSettingsList;

        PopulateFormData();
    }

    protected void lnkbtnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmAdminEmailSettingsAddModify.aspx");
    }

    #endregion

    #region DATA GRID METHODS

    private void PopulateFormData()
    {
        //### Populate datagrid using clsAdminEmailSettingsList object
        try
        {
            dtAdminEmailSettingsList = new DataTable();

            if (Session["dtAdminEmailSettingsList"] == null)
                dtAdminEmailSettingsList = clsAdminEmailSettings.GetAdminEmailSettingsList();
            else
                dtAdminEmailSettingsList = (DataTable)Session["dtAdminEmailSettingsList"];

            dgrGrid.DataSource = dtAdminEmailSettingsList;

            //### Add this check to trap if the last item on the page is deleted
            if (Convert.ToInt16(dgrGrid.CurrentPageIndex) < Convert.ToInt16(Session["AdminEmailSettingsView_CurrentPageIndex"]))
            {
                dgrGrid.CurrentPageIndex = 0;
            }
            else
            {
                if (Session["AdminEmailSettingsView_CurrentPageIndex"] != null)
                {
                    dgrGrid.CurrentPageIndex = (int)Session["AdminEmailSettingsView_CurrentPageIndex"];
                }
            }

            dgrGrid.DataBind();

        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void lnkDeleteItem_Click(object sender, EventArgs e)
    {
        int iAdminEmailSettingID = int.Parse((sender as LinkButton).CommandArgument);

        clsAdminEmailSettings.Delete(iAdminEmailSettingID);

        DataTable dtAdminEmailSettingsList = new DataTable();

        if (((txtSearch.Text == "SEARCH FOR A ADMIN EMAIL SETTING") == false) && ((txtSearch.Text == "") == false))
        {
            string FilterExpression = "() LIKE '%" + txtSearch.Text + "%'";
            dtAdminEmailSettingsList = clsAdminEmailSettings.GetAdminEmailSettingsList(FilterExpression, "");
        }
        else
        {
            dtAdminEmailSettingsList = clsAdminEmailSettings.GetAdminEmailSettingsList();
        }

        Session["dtAdminEmailSettingsList"] = dtAdminEmailSettingsList;

        PopulateFormData();
        //Session["dtAdminEmailSettingsList"] = null;
    }

    protected void dgrGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //### Method is used when the page index is changed.
        try
        {
            Session["AdminEmailSettingsView_CurrentPageIndex"] = e.NewPageIndex;
            dgrGrid.CurrentPageIndex = e.NewPageIndex;
            PopulateFormData();
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgrGrid_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        //### Sort Method
        dtAdminEmailSettingsList = new DataTable();

        if (Session["dtAdminEmailSettingsList"] == null)
            dtAdminEmailSettingsList = clsAdminEmailSettings.GetAdminEmailSettingsList();
        else
            dtAdminEmailSettingsList = (DataTable)Session["dtAdminEmailSettingsList"];

        DataView dvTemp = dtAdminEmailSettingsList.DefaultView;

        dvTemp.Sort = e.SortExpression;
        dtAdminEmailSettingsList = dvTemp.ToTable();
        Session["dtAdminEmailSettingsList"] = dtAdminEmailSettingsList;

        PopulateFormData();
    }

    public void dgrGrid_PopEditLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkEditItem = (HyperLink)sender;
            DataGridItem dgrItemEdit = (DataGridItem)lnkEditItem.Parent.Parent;

            //### Get the iAdminEmailSettingID
            int iAdminEmailSettingID = 0;
            iAdminEmailSettingID = int.Parse(dgrItemEdit.Cells[0].Text);

            //### Add attributes to edit link
            lnkEditItem.Attributes.Add("href", "frmAdminEmailSettingsAddModify.aspx?action=edit&iAdminEmailSettingID=" + iAdminEmailSettingID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    #endregion

    #region AdminEmailSettings FILTERING

    /// <summary>
    /// Enables the AutoCompleteExtender
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    /// 
    [WebMethod]
    [ScriptMethod]
    public static string[] FilterBusinessRule(string prefixText, int count)
    {
        string[] strReturnList = new string[] { };

        string FilterExpression = "() LIKE '%" + prefixText + "%'";
        DataTable dtAdminEmailSettings = clsAdminEmailSettings.GetAdminEmailSettingsList(FilterExpression, "");
        List<string> glstAdminEmailSettings = new List<string>();

        if (dtAdminEmailSettings.Rows.Count > 0)
        {
            foreach (DataRow dtrAdminEmailSettings in dtAdminEmailSettings.Rows)
            {
                glstAdminEmailSettings.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem("", dtrAdminEmailSettings["iAdminEmailSettingID"].ToString()));
            }
        }
        else
            glstAdminEmailSettings.Add("No AdminEmailSettings Available.");
        strReturnList = glstAdminEmailSettings.ToArray();
        return strReturnList;
    }

    #endregion
}
