
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text;

/// <summary>
/// Summary description for clsAccountUsersView
/// </summary>
public partial class CMS_clsCompaniesView : System.Web.UI.Page
{
    #region CLASSES AND VARIABLES

    clsUsers clsUsers;
    DataTable dtCompaniesList;
    List<clsCompanies> glstCompanies;

    #endregion

    #region EVENT MODULES

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

        dgrGrid.ItemCreated += new DataGridItemEventHandler(clsCommonFunctions.dgrGrid_PagerPopulate);

        if (!Page.IsPostBack)
        {
            Session["dtCompaniesList"] = null;
            PopulateFormData();
        }
        else
        {
            if (Session["glstCompanies"] == null)
            {
                glstCompanies = new List<clsCompanies>();
                Session["glstCompanies"] = glstCompanies;
            }
            else
                glstCompanies = (List<clsCompanies>)Session["glstCompanies"];
        }
            PopulateFormData();
    }

    protected string EscapeLikeValue(string valueWithoutWildcards)
    {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < valueWithoutWildcards.Length; i++)
        {
            char c = valueWithoutWildcards[i];
            if (c == '*' || c == '%' || c == '[' || c == ']')
                sb.Append("[").Append(c).Append("]");
            else if (c == '\'')
                sb.Append("''");
            else
                sb.Append(c);
        }
        return sb.ToString();
    }

    protected void lnkbtnSearch_Click(object sender, EventArgs e)
    {
        string EscapedString = EscapeLikeValue(txtSearch.Text.ToString());
        string FilterExpression ="(strTitle +' '+ strPrimaryEmail) LIKE '%" + EscapedString + "%'";
        DataTable dtCompaniesList = clsCompanies.GetCompaniesList(FilterExpression, "");

        Session["dtCompaniesList"] = dtCompaniesList;

        PopulateFormData();
    }

    protected void lnkbtnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmCompaniesAddModify.aspx");
    }

    #endregion

    #region DATA GRID METHODS

    private void PopulateFormData()
    {
        //### Populate datagrid using clsAccountUsersList object
        try
        {
            dtCompaniesList = new DataTable();

            if (Session["dtCompaniesList"] == null)
                dtCompaniesList = clsCompanies.GetCompaniesList();
            else
                dtCompaniesList = (DataTable)Session["dtCompaniesList"];

            dgrGrid.DataSource = dtCompaniesList;

            //### Add this check to trap if the last item on the page is deleted
            if (Convert.ToInt16(dgrGrid.CurrentPageIndex) < Convert.ToInt16(Session["CompaniesView_CurrentPageIndex"]))
            {
                dgrGrid.CurrentPageIndex = 0;
            }
            else
            {
                if (Session["CompaniesView_CurrentPageIndex"] != null)
                {
                    dgrGrid.CurrentPageIndex = (int)Session["CompaniesView_CurrentPageIndex"];
                }
            }

            dgrGrid.DataBind();

        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void lnkDeleteItem_Click(object sender, EventArgs e)
    {
        int iCompanyID = int.Parse((sender as LinkButton).CommandArgument);

        clsCompanies.Delete(iCompanyID);

        DataTable dtCompaniesList = new DataTable();

        if (((txtSearch.Text == "SEARCH FOR A COMPANY") == false) && ((txtSearch.Text == "") == false))
        {
            string FilterExpression = "(strTitle +' '+ strPrimaryEmail) LIKE '%" + txtSearch.Text + "%'";
            dtCompaniesList = clsCompanies.GetCompaniesList(FilterExpression, "");
        }
        else
        {
            dtCompaniesList = clsCompanies.GetCompaniesList();
        }

        Session["dtCompaniesList"] = dtCompaniesList;

        PopulateFormData();
    }

    protected void dgrGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //### Method is used when the page index is changed.
        try
        {
            Session["CompaniesView_CurrentPageIndex"] = e.NewPageIndex;
            dgrGrid.CurrentPageIndex = e.NewPageIndex;
            PopulateFormData();
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgrGrid_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        //### Sort Method
        dtCompaniesList = new DataTable();

        if (Session["dtCompaniesList"] == null)
            dtCompaniesList = clsCompanies.GetCompaniesList();
        else
            dtCompaniesList = (DataTable)Session["dtCompaniesList"];

        DataView dvTemp = dtCompaniesList.DefaultView;

        dvTemp.Sort = e.SortExpression;
        dtCompaniesList = dvTemp.ToTable();
        Session["dtCompaniesList"] = dtCompaniesList;

        PopulateFormData();
    }

    public void dgrGrid_PopEditLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkEditItem = (HyperLink)sender;
            DataGridItem dgrItemEdit = (DataGridItem)lnkEditItem.Parent.Parent;

            //### Get the iCompanyID
            int iCompanyID = 0;
            iCompanyID = int.Parse(dgrItemEdit.Cells[0].Text);

            //### Add attributes to edit link
            lnkEditItem.Attributes.Add("href", "frmCompaniesAddModify.aspx?action=edit&iCompanyID=" + iCompanyID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

     public void dgrGrid_PopDeleteLink(object sender, EventArgs e)
     {
          try
          {
             //### Add hyperlink to datagrid item
             HyperLink lnkDeleteItem = (HyperLink)sender;
             DataGridItem dgrItemDelete = (DataGridItem)lnkDeleteItem.Parent.Parent;

             //### Get the iCompanyID
             int iCompanyID = 0;
             iCompanyID = int.Parse(dgrItemDelete.Cells[0].Text);

             //### Add attributes to delete link
             lnkDeleteItem.CssClass = "dgrDeleteLink";
             lnkDeleteItem.Attributes.Add("href", "frmCompaniesView.aspx?action=delete&iCompanyID=" + iCompanyID);
           }
           catch (Exception ex)
           {
                 Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
           }
     }

    #endregion

    #region AccountUsers FILTERING

    /// <summary>
    /// Enables the AutoCompleteExtender
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    /// 
    [WebMethod]
    [ScriptMethod]
    public static string[] FilterBusinessRule(string prefixText, int count)
    {
        string[] strReturnList = new string[] { };

        string FilterExpression = "(strCompanyName +' '+ strPrimaryEmail) LIKE '%" + prefixText + "%'";
        DataTable dtCompanies = clsCompanies.GetCompaniesList(FilterExpression, "");
        List<string> glstCompanies = new List<string>();

        if (dtCompanies.Rows.Count > 0)
        {
            foreach (DataRow dtrAccountUsers in dtCompanies.Rows)
            {
                glstCompanies.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem("" + dtrAccountUsers["strCompanyName"].ToString()+' '+"" + dtrAccountUsers["strPrimaryEmail"].ToString(), dtrAccountUsers["iCompanyID"].ToString()));
            }
        }
        else
            glstCompanies.Add("No Companies Available.");
        strReturnList = glstCompanies.ToArray();
        return strReturnList;
    }

    #endregion
   
}
