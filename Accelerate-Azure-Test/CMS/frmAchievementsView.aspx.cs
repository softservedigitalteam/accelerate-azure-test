
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text;

/// <summary>
/// Summary description for clsAchievementsView
/// </summary>
public partial class CMS_clsAchievementsView : System.Web.UI.Page
{
    #region CLASSES AND VARIABLES

    clsUsers clsUsers;
    DataTable dtAchievementsList;

    List<clsAchievements> glstAchievements;

    #endregion

    #region EVENT MODULES

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

dgrGrid.ItemCreated += new DataGridItemEventHandler(clsCommonFunctions.dgrGrid_PagerPopulate);

        if (!Page.IsPostBack)
        {
            Session["dtAchievementsList"] = null;
            PopulateFormData();
        }
        //else
        //{
        //    if (Session["glstAchievements"] == null)
        //    {
        //        glstAchievements = new List<clsAchievements>();
        //        Session["glstAchievements"] = glstAchievements;
        //    }
        //    else
        //        glstAchievements = (List<clsAchievements>)Session["glstAchievements"];
        //}
        PopulateFormData();

    }

    protected string EscapeLikeValue(string valueWithoutWildcards)
    {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < valueWithoutWildcards.Length; i++)
        {
            char c = valueWithoutWildcards[i];
            if (c == '*' || c == '%' || c == '[' || c == ']')
                sb.Append("[").Append(c).Append("]");
            else if (c == '\'')
                sb.Append("''");
            else
                sb.Append(c);
        }
        return sb.ToString();
    }

    protected void lnkbtnSearch_Click(object sender, EventArgs e)
    {
        string EscapedString = EscapeLikeValue(txtSearch.Text.ToString());
        string FilterExpression ="(strTitle +' '+ strDescription) LIKE '%" + EscapedString + "%'";
        DataTable dtAchievementsList = clsAchievements.GetAchievementsList(FilterExpression, "");

        Session["dtAchievementsList"] = dtAchievementsList;

        PopulateFormData();
    }

    protected void lnkbtnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmAchievementsAddModify.aspx");
    }

    #endregion

    #region DATA GRID METHODS

    private void PopulateFormData()
    {
        //### Populate datagrid using clsAchievementsList object
        try
        {
            dtAchievementsList = new DataTable();

            if (Session["dtAchievementsList"] == null)
                dtAchievementsList = clsAchievements.GetAchievementsList("", "dtAdded DESC");
            else
                dtAchievementsList = (DataTable)Session["dtAchievementsList"];

            dgrGrid.DataSource = dtAchievementsList;

            //### Add this check to trap if the last item on the page is deleted
            if (Convert.ToInt16(dgrGrid.CurrentPageIndex) < Convert.ToInt16(Session["AchievementsView_CurrentPageIndex"]))
            {
                dgrGrid.CurrentPageIndex = 0;
            }
            else
            {
                if (Session["AchievementsView_CurrentPageIndex"] != null)
                {
                    dgrGrid.CurrentPageIndex = (int)Session["AchievementsView_CurrentPageIndex"];
                }
            }

            dgrGrid.DataBind();

        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void lnkDeleteItem_Click(object sender, EventArgs e)
    {
        int iAchievementID = int.Parse((sender as LinkButton).CommandArgument);

        clsAchievements.Delete(iAchievementID);

        DataTable dtAchievementsList = new DataTable();

        if (((txtSearch.Text == "SEARCH FOR AN ACHIEVEMENT") == false) && ((txtSearch.Text == "") == false))
        {
            string FilterExpression = "(strTitle +' '+ strDescription) LIKE '%" + txtSearch.Text + "%'";
            dtAchievementsList = clsAchievements.GetAchievementsList(FilterExpression, "");
        }
        else
        {
            dtAchievementsList = clsAchievements.GetAchievementsList();
        }

        Session["dtAchievementsList"] = dtAchievementsList;

        PopulateFormData();
    }

    protected void dgrGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //### Method is used when the page index is changed.
        try
        {
            Session["AchievementsView_CurrentPageIndex"] = e.NewPageIndex;
            dgrGrid.CurrentPageIndex = e.NewPageIndex;
            PopulateFormData();
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgrGrid_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        //### Sort Method
        dtAchievementsList = new DataTable();

        if (Session["dtAchievementsList"] == null)
            dtAchievementsList = clsAchievements.GetAchievementsList();
        else
            dtAchievementsList = (DataTable)Session["dtAchievementsList"];

        DataView dvTemp = dtAchievementsList.DefaultView;

        dvTemp.Sort = e.SortExpression;
        dtAchievementsList = dvTemp.ToTable();
        Session["dtAchievementsList"] = dtAchievementsList;

        PopulateFormData();
    }

    public void dgrGrid_PopEditLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkEditItem = (HyperLink)sender;
            DataGridItem dgrItemEdit = (DataGridItem)lnkEditItem.Parent.Parent;

            //### Get the iAchievementID
            int iAchievementID = 0;
            iAchievementID = int.Parse(dgrItemEdit.Cells[0].Text);

            //### Add attributes to edit link
            lnkEditItem.Attributes.Add("href", "frmAchievementsAddModify.aspx?action=edit&iAchievementID=" + iAchievementID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

     public void dgrGrid_PopDeleteLink(object sender, EventArgs e)
     {
          try
          {
             //### Add hyperlink to datagrid item
             HyperLink lnkDeleteItem = (HyperLink)sender;
             DataGridItem dgrItemDelete = (DataGridItem)lnkDeleteItem.Parent.Parent;

             //### Get the iAchievementID
             int iAchievementID = 0;
             iAchievementID = int.Parse(dgrItemDelete.Cells[0].Text);

             //### Add attributes to delete link
             lnkDeleteItem.CssClass = "dgrDeleteLink";
             lnkDeleteItem.Attributes.Add("href", "frmAchievementsView.aspx?action=delete&iAchievementID=" + iAchievementID);
           }
           catch (Exception ex)
           {
                 Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
           }
     }

    #endregion

    #region Achievements FILTERING

    /// <summary>
    /// Enables the AutoCompleteExtender
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    /// 
    [WebMethod]
    [ScriptMethod]
    public static string[] FilterBusinessRule(string prefixText, int count)
    {
        string[] strReturnList = new string[] { };

        string FilterExpression = "(strTitle +' '+ strDescription) LIKE '%" + prefixText + "%'";
        DataTable dtAchievements = clsAchievements.GetAchievementsList(FilterExpression, "");
        List<string> glstAchievements = new List<string>();

        if (dtAchievements.Rows.Count > 0)
        {
            foreach (DataRow dtrAchievements in dtAchievements.Rows)
            {
                glstAchievements.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem("" + dtrAchievements["strTitle"].ToString() +' '+"" + dtrAchievements["strDescription"].ToString(), dtrAchievements["iAchievementID"].ToString()));
            }
        }
        else
            glstAchievements.Add("No Achievements Available.");
        strReturnList = glstAchievements.ToArray();
        return strReturnList;
    }

    #endregion
}
