﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.UI.HtmlControls;
using System.Text;

public partial class CMS_frmUsersView : System.Web.UI.Page
{
    clsUsers clsUsers;
    DataTable dtUsersList;

    List<clsUsers> glstUsers;

    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            //### Redirect back to login
            Response.Redirect("../CMSLogin.aspx");
        }
        clsUsers = (clsUsers)Session["clsUsers"];

        dgrGrid.ItemCreated += new DataGridItemEventHandler(dgrGrid_PagerPopulate);

        if (!Page.IsPostBack)
        {
            Session["dtUsersList"] = null;
            PopulateFormData();
        }
        else
        {
            if (Session["glstUsers"] == null)
            {
                glstUsers = new List<clsUsers>();
                Session["glstUsers"] = glstUsers;
            }
            else
                glstUsers = (List<clsUsers>)Session["glstUsers"];

            PopulateFormData();
        }
    }

    protected string EscapeLikeValue(string valueWithoutWildcards)
    {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < valueWithoutWildcards.Length; i++)
        {
            char c = valueWithoutWildcards[i];
            if (c == '*' || c == '%' || c == '[' || c == ']')
                sb.Append("[").Append(c).Append("]");
            else if (c == '\'')
                sb.Append("''");
            else
                sb.Append(c);
        }
        return sb.ToString();
    }

    protected void lnkbtnSearch_Click(object sender, EventArgs e)
    {
        string EscapedString = EscapeLikeValue(txtSearch.Text.ToString());
        string FilterExpression = " (strFirstName + ' ' + strSurname + ' ' + strEmailAddress) LIKE '%" + EscapedString + "%'";

        DataTable dtUsersList = clsUsers.GetUsersList(FilterExpression, "");

        Session["dtUsersList"] = dtUsersList;
        PopulateFormData();
    }
    
    protected void lnkbtnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmUsersAddModify.aspx");
    }

    #endregion

    #region DATA GRID METHODS

    private void PopulateFormData()
    {
        //### Populate datagrid using clsUserList object
        try
        {
            dtUsersList = new DataTable();

            if (Session["dtUsersList"] == null)
                dtUsersList = clsUsers.GetUsersList();
            else
                dtUsersList = (DataTable)Session["dtUsersList"];

            dgrGrid.DataSource = dtUsersList;

            //### Add this check to trap if the last item on the page is deleted
            if (Convert.ToInt16(dgrGrid.CurrentPageIndex) < Convert.ToInt16(Session["UserView_CurrentPageIndex"]))
            {
                dgrGrid.CurrentPageIndex = 0;
            }
            else
            {
                if (Session["UserView_CurrentPageIndex"] != null)
                {
                    dgrGrid.CurrentPageIndex = (int)Session["UserView_CurrentPageIndex"];
                }
            }

            dgrGrid.DataBind();

        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void lnkDeleteItem_Click(object sender, EventArgs e)
    {
        int iUserID = int.Parse((sender as LinkButton).CommandArgument);

        clsUsers.Delete(iUserID);

        DataTable dtUsersList = new DataTable();

        if (((txtSearch.Text == "SEARCH FOR A USER") == false) && ((txtSearch.Text == "") == false))
        {
            string FilterExpression = " (strFirstName + ' ' + strSurname + ' ' + strEmailAddress) LIKE '%" + txtSearch.Text + "%'";
            dtUsersList = clsUsers.GetUsersList(FilterExpression, "");
        }
        else
        {
            dtUsersList = clsUsers.GetUsersList();
        }

        Session["dtUsersList"] = dtUsersList;

        PopulateFormData();
    }

    protected void dgrGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //### Method is used when the page index is changed.
        try
        {
            Session["UserView_CurrentPageIndex"] = e.NewPageIndex;
            dgrGrid.CurrentPageIndex = e.NewPageIndex;
            PopulateFormData();
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgrGrid_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        //### Sort Method
        dtUsersList = new DataTable();

        if (Session["dtUsersList"] == null)
            dtUsersList = clsUsers.GetUsersList();
        else
            dtUsersList = (DataTable)Session["dtUsersList"];

        DataView dvTemp = dtUsersList.DefaultView;

        dvTemp.Sort = e.SortExpression;
        dtUsersList = dvTemp.ToTable();
        Session["dtUsersList"] = dtUsersList;

        PopulateFormData();
    }

    public void dgrGrid_PopEditPassLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkEditPass = (HyperLink)sender;
            DataGridItem dgrItemEditPass = (DataGridItem)lnkEditPass.Parent.Parent;

            //### Get the iUserID
            int iUserID = 0;
            iUserID = int.Parse(dgrItemEditPass.Cells[0].Text);

            //### Add attributes to edit link
            lnkEditPass.Attributes.Add("href", "frmUsersPasswordEdit.aspx?iUserID=" + iUserID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    public void dgrGrid_PopEditLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkEditItem = (HyperLink)sender;
            DataGridItem dgrItemEdit = (DataGridItem)lnkEditItem.Parent.Parent;

            //### Get the iUserID
            int iUserID = 0;
            iUserID = int.Parse(dgrItemEdit.Cells[0].Text);

            //### Add attributes to edit link
            lnkEditItem.Attributes.Add("href", "frmUsersAddModify.aspx?action=edit&iCMSUserID=" + iUserID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    public void dgrGrid_PopDeleteLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkDeleteItem = (HyperLink)sender;
            DataGridItem dgrItemDelete = (DataGridItem)lnkDeleteItem.Parent.Parent;

            //### Get the iUserID
            int iUserID = 0;
            iUserID = int.Parse(dgrItemDelete.Cells[0].Text);

            //### Add attributes to delete link
            lnkDeleteItem.CssClass = "dgrDeleteLink";
            lnkDeleteItem.Attributes.Add("href", "frmUsersView.aspx?action=delete&iUserID=" + iUserID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    public static void dgrGrid_PagerPopulate(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Pager)
        {
            Control[] arrayControls = new Control[e.Item.Controls[0].Controls.Count];

            for (int i = 0; i < e.Item.Controls[0].Controls.Count; i++)
            {
                arrayControls[i] = e.Item.Controls[0].Controls[i];

                //### Check if the first is a previous page button
                if (e.Item.Controls[0].Controls[i] is LinkButton)
                {
                }
            }

            //### Remove the crappy built in span used for paging
            for (int i = 0; i < e.Item.Controls[0].Controls.Count; i++)
            {
                e.Item.Controls[0].Controls.RemoveAt(i);
            }

            for (int i = 0; i < arrayControls.Length; i++)
            {
                if (arrayControls[i] is Label)
                {
                    //### Current page
                    LinkButton lblPager = new LinkButton();
                    HtmlGenericControl divPager = new HtmlGenericControl("div");
                    lblPager.Width = 22;
                    divPager.Attributes.Add("style", "margin-top:10px");
                    lblPager.Attributes.Add("style", "margin-right:2px");
                    divPager.Attributes.Add("style", "height:20px");
                    divPager.InnerText = ((Label)arrayControls[i]).Text;
                    lblPager.Controls.Add(divPager);
                    e.Item.Controls[0].Controls.Add(lblPager);

                }
                if (arrayControls[i] is LinkButton)
                {
                    //### All the other pages
                    LinkButton btnPager = new LinkButton();
                    HtmlGenericControl divPager = new HtmlGenericControl("p");
                    btnPager.Width = 22;
                    btnPager.CommandArgument = ((LinkButton)arrayControls[i]).CommandArgument;
                    btnPager.CommandName = ((LinkButton)arrayControls[i]).CommandName;
                    divPager.Attributes.Add("style", "margin-top:3px");
                    btnPager.Attributes.Add("style", "margin-right:2px");
                    divPager.InnerText = ((LinkButton)arrayControls[i]).Text;
                    btnPager.Controls.Add(divPager);
                    e.Item.Controls[0].Controls.Add(btnPager);
                }
            }
        }
    }

    #endregion

    #region USER FILTERING
    
    /// <summary>
    /// Enables the AutoCompleteExtender
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    /// 
    [WebMethod]
    [ScriptMethod]
    public static string[] FilterBusinessRule(string prefixText, int count)
    {
        string[] strReturnList = new string[] { };

        string FilterExpression = " (strFirstName + ' ' + strSurname + ' ' + strEmailAddress) LIKE '%" + prefixText + "%'";
        DataTable dtUsers = clsUsers.GetUsersList(FilterExpression, "");
        List<string> glstUsers = new List<string>();

        if (dtUsers.Rows.Count > 0)
        {
            foreach (DataRow dtrUsers in dtUsers.Rows)
            {
                glstUsers.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem("" + dtrUsers["strFirstName"].ToString() + " " + dtrUsers["strSurname"].ToString(), dtrUsers["iUserID"].ToString()));
            }
        }
        else
            glstUsers.Add("No Users Available.");

        strReturnList = glstUsers.ToArray();

        return strReturnList;
    }

    #endregion
}