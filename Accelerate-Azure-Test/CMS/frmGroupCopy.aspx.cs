﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using SD = System.Drawing;
using System.Drawing.Drawing2D;
using System.Data.SqlClient;

public partial class CMS_frmGroupCopy : System.Web.UI.Page
{
    clsUsers clsUsers;
    clsGroups clsGroups;

    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

        if (!IsPostBack)
        {
            //### If the iCategorieID is passed through then we want to instantiate the object with that iCategorieID
            if ((Request.QueryString["iGroupID"] != "") && (Request.QueryString["iGroupID"] != null))
            {
                clsGroups = new clsGroups(Convert.ToInt32(Request.QueryString["iGroupID"]));

                //### Populate the form
                popCompanies();
                popWelcomes();
                popCompletions();
                popCustomRanks();
                popColorSchemes();
                popFormData();
                popValidFields();

            }
            else
            {
                PopGroups(0);
                popWelcomes();
                popCompletions();
                popCompanies();
                popCustomRanks();
                popColorSchemes();

                clsGroups = new clsGroups();
            }
            Session["clsGroups"] = clsGroups;
        }
        else
        {
            clsGroups = (clsGroups)Session["clsGroups"];
        }
    }

    protected void lnkbtnBack_Click(object sender, EventArgs e)
    {
        //### Go back to previous page
        Response.Redirect("frmGroupsView.aspx");
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        //### Validate registration process
        bool bCanSave = true;

        bCanSave = clsValidation.IsNullOrEmpty(lstWelcome, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(lstComplete, bCanSave);
        //bCanSave = clsValidation.IsNullOrEmpty(lstTheme, bCanSave);
        //bCanSave = clsValidation.IsNullOrEmpty(lstGroupType, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(lstCompany, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(lstRankSettings, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtTitle, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtDescription, bCanSave);

        if (treeGroups.SelectedNode == null)
        {
            bCanSave = false;
        }

        if (bCanSave == true)
        {
            mandatoryDiv.Visible = false;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceHappy.png\" alt='' title=''/><div class=\"validationMessage\">Categorie added successfully</div></div>";
            SaveData();
        }
        else
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">Please fill out all mandatory fields - Categorie not added</div></div>";
        }

        popValidFields();
    }

    protected void lnkbtnClear_Click(object sender, EventArgs e)
    {
        mandatoryDiv.Visible = false;
        txtTitle.Text = "";
        clsValidation.SetValid(txtTitle);
        txtDescription.Text = "";
        clsValidation.SetValid(txtDescription);
    }

    #endregion

    #region POPULATE DATA METHODS

    private void popFormData()
    {
        //lstGroupType.SelectedValue = clsGroups.iGroupTypeID.ToString();
        txtTitle.Text = clsGroups.strTitle + " - Copy";
        txtDescription.Text = clsGroups.strDescription + " - Copy";
        lstCompany.SelectedValue = clsGroups.iCompanyID.ToString();
        lstTheme.SelectedValue = clsGroups.iCustomGeneralColourID.ToString();
        lstRankSettings.SelectedValue = clsGroups.iCustomGroupRankID.ToString();
        lstWelcome.SelectedValue = clsGroups.iWelcomeMessageID.ToString();
        lstComplete.SelectedValue = clsGroups.iFinalMessageID.ToString();
        PopGroups(clsGroups.iLevelID);
        hdniGroupID.Value = clsGroups.iLevelID.ToString();
    }

    private void popValidFields()
    {
        //### Make the relevant mandatory fields green
        string strScript = "";

        if (!String.IsNullOrEmpty(txtTitle.Text))
        {
            strScript += "setValidFile('" + txtTitle.ClientID + "',true);";
        }

        ScriptManager.RegisterStartupScript(this, this.GetType(), "mandatoryFields", strScript + strScript, true);
    }

    /// <summary>
    /// Populate the Tree view with the Group types
    /// </summary>
    private void PopGroups(int clsGroupsiGroupID)
    {
        treeGroups.Nodes.Clear();
        TreeNode Mainnode = new TreeNode();
        bool bIsSelected = false;
        Mainnode.Value = "0";
        Mainnode.Text = "Master Group - (Select this if you want to create a Master Group)";
        if (clsGroupsiGroupID == 0)
        {
            Mainnode.Selected = true;
        }
        treeGroups.Nodes.Add(Mainnode);

        DataTable DT = new DataTable();
        DT = clsGroups.GetGroupsList("iLevelID = 0", "strTitle");

        foreach (DataRow r in DT.Rows)
        {
            int iGroupID = 0;
            iGroupID = int.Parse(r["iGroupID"].ToString());
            TreeNode node = new TreeNode(r["strTitle"].ToString(), r["iGroupID"].ToString());
            if (clsGroupsiGroupID == int.Parse(r["iGroupID"].ToString()))
            {
                node.Selected = true;
            }

            //### Now we call the PopChildGroups method to build the sub Groups
            PopChildGroups(ref node, int.Parse(r["iGroupID"].ToString()), clsGroupsiGroupID, ref bIsSelected);
            if (bIsSelected)
            {
                node.Expanded = true;
            }
            else
            {
                node.Expanded = false;
            }
            treeGroups.Nodes.Add(node);
        }
    }

    private void popCompanies()
    {
        lstCompany.DataSource = clsCompanies.GetCompaniesList();

        //### Populates the drop down list with PK and TITLE;
        lstCompany.DataValueField = "iCompanyID";
        lstCompany.DataTextField = "strTitle";

        //### Bind the data to the list;
        lstCompany.DataBind();

        //### Add default select option;
        lstCompany.Items.Insert(0, new ListItem("--Not Selected--", "0"));
    }

    private void popWelcomes()
    {
        lstWelcome.DataSource = clsWelcomeMessages.GetWelcomeMessagesList();

        //### Populates the drop down list with PK and TITLE;
        lstWelcome.DataValueField = "iWelcomeMessageID";
        lstWelcome.DataTextField = "strHeaderText";

        //### Bind the data to the list;
        lstWelcome.DataBind();

        //### Add default select option;
        lstWelcome.Items.Insert(0, new ListItem("--Not Selected--", "0"));
    }

    private void popCompletions()
    {
        lstComplete.DataSource = clsFinalMessages.GetFinalMessagesList();

        //### Populates the drop down list with PK and TITLE;
        lstComplete.DataValueField = "iFinalMessageID";
        lstComplete.DataTextField = "strHeaderText";

        //### Bind the data to the list;
        lstComplete.DataBind();

        //### Add default select option;
        lstComplete.Items.Insert(0, new ListItem("--Not Selected--", "0"));
    }

    private void popCustomRanks()
    {
        lstRankSettings.DataSource = clsCustomGroupRanks.GetRanksList();

        //### Populates the drop down list with PK and TITLE;
        lstRankSettings.DataValueField = "iCustomGroupRankID";
        lstRankSettings.DataTextField = "strTag";

        //### Bind the data to the list;
        lstRankSettings.DataBind();

        //### Add default select option;
        lstRankSettings.Items.Insert(0, new ListItem("--Not Selected--", "0"));
    }

    private void popColorSchemes()
    {
        lstTheme.Items.Clear();
        lstTheme.DataSource = clsCustomGeneralColours.GetCustomGeneralColoursList();

        //### Populates the drop down list with PK and TITLE;
        lstTheme.DataValueField = "iCustomGeneralColourID";
        lstTheme.DataTextField = "strTitle";

        //### Bind the data to the list
        lstTheme.DataBind();

        //### Add default select option;
        lstTheme.Items.Insert(0, new ListItem("Default Theme", "0"));
    }

    /// <summary>
    /// Populate the SubGroups for the Group Treeview.
    /// This Method will call itself to build the SubGroup of the current SubGroup
    /// </summary>
    /// <param name="ChildNode">The current Top level Group</param>
    /// <param name="iGroupID">The ID of the Group</param>
    /// <param name="clsGroupsiGroupID">The ID of the clsGroups.iGroupID</param>
    private void PopChildGroups(ref TreeNode ChildNode, int iGroupID, int clsGroupsiGroupID, ref bool bIsSelected)
    {
        DataTable DT = new DataTable();
        DT = clsGroups.GetGroupsList("iLevelID = " + iGroupID.ToString(), "strTitle");

        foreach (DataRow r in DT.Rows)
        {
            TreeNode node = new TreeNode(r["strTitle"].ToString(), r["iGroupID"].ToString());
            if (clsGroupsiGroupID == int.Parse(r["iGroupID"].ToString()))
            {
                node.Selected = true;
                bIsSelected = true;
            }
            if (bIsSelected)
            {
                node.Expanded = true;
            }
            ChildNode.ChildNodes.Add(node);

            //### Now we call this methode again to build the subsubGroups
            PopChildGroups(ref node, int.Parse(r["iGroupID"].ToString()), clsGroupsiGroupID, ref bIsSelected);
        }
    }

    #endregion

    #region SAVE DATA METHODS

    private void SaveData()
    {
        //### Add new Group
        int iOldGroupID = (Convert.ToInt32(Request.QueryString["iGroupID"]));
        DataTable dtGroupPhases = clsGroupPhases.GetGroupPhasesList("iGroupID = '" + iOldGroupID + "'","");
        DataTable dtPortalFeatureGroups = clsGroupFeatures.GetGroupFeaturesList("iGroupID = '" + iOldGroupID + "'", "");
        clsGroups clsGroups = new clsGroups();

        clsGroups.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsGroups.iAddedBy = clsUsers.iUserID;
        clsGroups.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsGroups.iEditedBy = clsUsers.iUserID;

        clsGroups.iCompanyID = Convert.ToInt32(lstCompany.SelectedValue.ToString());
        clsGroups.iWelcomeMessageID = Convert.ToInt32(lstWelcome.SelectedValue.ToString());
        clsGroups.iFinalMessageID = Convert.ToInt32(lstComplete.SelectedValue.ToString());

        if (lstTheme.SelectedValue.ToString() != "0")
        {
            clsGroups.iCustomGeneralColourID = Convert.ToInt32(lstTheme.SelectedValue.ToString());
        }
        else
        {
            clsGroups.iCustomGeneralColourID = 0;
        }

        int iLevelID;

        if (treeGroups.Nodes.Count != 0)
        {
            if (treeGroups.SelectedNode != null)
            {

                iLevelID = int.Parse(treeGroups.SelectedNode.Value);
            }
            else
            {
                iLevelID = 0;
            }
        }
        else
        {
            iLevelID = 0;
        }

        clsGroups.iLevelID = iLevelID;
        clsGroups.iCustomGroupRankID = Convert.ToInt32(lstRankSettings.SelectedValue);
        //clsGroups.iGroupTypeID = Convert.ToInt32(lstGroupType.SelectedValue.ToString());
        clsGroups.strTitle = txtTitle.Text;
        clsGroups.strDescription = txtDescription.Text;

        clsGroups.Update();

        if (iLevelID == 0)
        {
            DataTable dtMasterGroupsList = clsGroups.GetGroupsList("iLevelID = '" + iOldGroupID + "'", "");
            foreach (DataRow drMasterGroup in dtMasterGroupsList.Rows)
            {
                int iOldGroupCopyID = Convert.ToInt32(drMasterGroup["iGroupID"]);
                clsGroups clsOldGroup = new clsGroups(iOldGroupCopyID);
                clsGroups clsNewGroup = new clsGroups();

                clsNewGroup.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
                clsNewGroup.iAddedBy = clsUsers.iUserID;
                clsNewGroup.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
                clsNewGroup.iEditedBy = clsUsers.iUserID;
                clsNewGroup.iCompanyID = clsOldGroup.iCompanyID;
                clsNewGroup.iLevelID = clsGroups.iGroupID;
                clsNewGroup.strTitle = clsOldGroup.strTitle + " - Copy";
                clsNewGroup.strDescription = clsOldGroup.strDescription;
                clsNewGroup.iCustomGroupRankID = clsOldGroup.iCustomGroupRankID;
                clsNewGroup.iCustomGeneralColourID = clsOldGroup.iCustomGeneralColourID;
                clsNewGroup.iWelcomeMessageID = clsOldGroup.iWelcomeMessageID;
                clsNewGroup.iFinalMessageID = clsOldGroup.iFinalMessageID;
                clsNewGroup.Update();

                DataTable dtOldGroupPhases = clsGroupPhases.GetGroupPhasesList("iGroupID = '" + iOldGroupCopyID + "'", "");
                DataTable dtOldPortalFeatureGroups = clsGroupFeatures.GetGroupFeaturesList("iGroupID = '" + iOldGroupCopyID + "'", "");

                if (dtOldGroupPhases != null)
                {
                    foreach (DataRow drGroupPhases in dtOldGroupPhases.Rows)
                    {
                        int phaseID = Convert.ToInt32(drGroupPhases["iPhaseID"]);
                        int groupID = clsNewGroup.iGroupID;
                        AddPhasesToGroup(phaseID, groupID);
                    }
                }
                if (dtOldPortalFeatureGroups != null)
                {
                    foreach (DataRow drPortalFeatureGroups in dtOldPortalFeatureGroups.Rows)
                    {
                        int portalFeatureID = Convert.ToInt32(drPortalFeatureGroups["iPortalFeatureID"]);
                        int groupID = clsNewGroup.iGroupID;
                        AddFeaturesToGroup(portalFeatureID, groupID);
                    }
                }
            }
        }

        if (dtGroupPhases != null)
        {
            foreach (DataRow drGroupPhases in dtGroupPhases.Rows)
            {
                int phaseID = Convert.ToInt32(drGroupPhases["iPhaseID"]);
                int groupID = clsGroups.iGroupID;
                AddPhasesToGroup(phaseID, groupID);
            }
        }
        if (dtPortalFeatureGroups != null)
        {
            foreach (DataRow drPortalFeatureGroups in dtPortalFeatureGroups.Rows)
            {
                int portalFeatureID = Convert.ToInt32(drPortalFeatureGroups["iPortalFeatureID"]);
                int groupID = clsGroups.iGroupID;
                AddFeaturesToGroup(portalFeatureID, groupID);
            }
        }

        //### Go back to view page
        Response.Redirect("frmGroupsView.aspx");
    }

    #endregion
    protected void AddPhasesToGroup(int iPhaseID, int iGroupID)
    {
        clsPhases clsPhases = new clsPhases(iPhaseID);

        clsGroupPhases clsGroupPhases = new clsGroupPhases();

        //clsGroupPhases.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        //clsGroupPhases.iAddedBy = clsUsers.iUserID;
        clsGroupPhases.iGroupID = iGroupID;
        clsGroupPhases.iPhaseID = iPhaseID;
        if (clsPhases.strTitle != null)
        {
            clsGroupPhases.strTitle = clsPhases.strTitle;
        }
        else
        {
            clsGroupPhases.strTitle = "";
        }
        clsGroupPhases.Update();
    }

    protected void AddFeaturesToGroup(int iPortalFeatureID, int iGroupID)
    {
        clsPortalFeatures clsPortalFeatures = new clsPortalFeatures(iPortalFeatureID);

        clsGroupFeatures clsGroupFeatures = new clsGroupFeatures();

        //clsGroupFeatures.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        //clsGroupFeatures.iAddedBy = clsUsers.iUserID;
        clsGroupFeatures.iGroupID = iGroupID;
        clsGroupFeatures.iPortalFeatureID = iPortalFeatureID;
        if (clsPortalFeatures.strTitle != null)
        {
            clsGroupFeatures.strTitle = clsPortalFeatures.strTitle;
        }
        else
        {
            clsGroupFeatures.strTitle = "";
        }
        clsGroupFeatures.Update();
    }
}