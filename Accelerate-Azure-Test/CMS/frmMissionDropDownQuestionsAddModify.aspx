<%@ Page Title="Mission Questions" Language="C#" MasterPageFile="~/CMS/CMS.master" AutoEventWireup="true" Inherits="CMS_frmMissionDropDownQuestionsAddModify" Codebehind="frmMissionDropDownQuestionsAddModify.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div id="mandatoryDiv" class="mandatoryInvalidDiv" runat="server" visible="false">
        <asp:Label ID="lblValidationMessage" runat="server"></asp:Label>
    </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Mission:</div>
         <div class="fieldDiv">
             <asp:DropDownList ID="lstMission" runat="server" CssClass="roundedCornerDropDownList" onblur="setValid(this, 0);"></asp:DropDownList>
         </div>
         <br class="clearingSpacer" />
     </div>
     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Question:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtQuestion" runat="server" CssClass="roundedCornerTextBoxMultiLine8" onKeyUp="return SetMaxLength(this,1000)" TextMode="MultiLine" Rows="8" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv" style="display:none;">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Is the answer text?</div>
         <div class="fieldDiv">
             <asp:CheckBox ID="cbIsText" runat="server" Text="(Tick if yes - only one may be chosen)" style="float:left;margin-top:15px;"/>
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv" style="display:none;">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Is the answer a drop down list?</div>
         <div class="fieldDiv">
             <asp:CheckBox ID="cbIsDropDownList" runat="server" Text="(Tick if yes - only one may be chosen)" style="float:left;margin-top:20px;"/>
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv" style="display:none;">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Is the answer a Leader's Hip?</div>
         <div class="fieldDiv">
             <asp:CheckBox ID="cbIsMultipleChoice" runat="server" Text="(Tick if yes - only one may be chosen)" style="float:left;margin-top:20px;"/>
         </div>
         <br class="clearingSpacer" />
     </div>

    <div class="controlDiv" style="display:none;">
        <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
        <div class="labelDiv">Is the answer an upload?</div>
        <div class="fieldDiv">
            <asp:CheckBox ID="cbIsTeamStructure" runat="server" Text="(Tick if yes - only one may be chosen)" style="float:left;margin-top:20px;"/>
        </div>
        <br class="clearingSpacer" />
    </div>
    
    <div class="Line"></div>
    <div class="buttonsRightDiv">
        <asp:LinkButton ID="lnkbtnBack" runat="server" CssClass="backButton" onclick="lnkbtnBack_Click" OnClientClick='history.go(-1);return false;' />
        <asp:LinkButton ID="lnkbtnSave" runat="server" CssClass="saveButton" onclick="lnkbtnSave_Click" />
        <asp:LinkButton ID="lnkbtnClear" runat="server" CssClass="clearButton" onclick="lnkbtnClear_Click" />
    </div>
</asp:Content>
