<%@ Page Title="Groups" Language="C#" MasterPageFile="~/CMS/CMS.master" AutoEventWireup="true" Inherits="CMS_clsGroupsView" Codebehind="frmGroupsView.aspx.cs" %>

<%@ Register TagPrefix="ajax" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script lang="javascript" type="text/javascript">
        //### Password check
        function clearText() {
            if (document.getElementById("<%=txtSearch.ClientID%>").value == "SEARCH FOR A BUSINESS UNIT") {
                document.getElementById("<%=txtSearch.ClientID%>").value = "SEARCH FOR A BUSINESS UNIT"
            }
        }
    </script>
    <script type="text/javascript">
        function RefreshUpdatePanel(sender) {
            __doPostBack(sender, '');
        }
    </script>
    <!-- Event Manager for selection of item from auto-complete list -->
    <script lang="javascript" type="text/javascript">
        function ClientItemSelected(sender, e) {
            $get("<%=hfGroupID.ClientID %>").value = e.get_value();
            $get('<%=lnkbtnSearch.ClientID %>').click();
        }

        function toggleDiv(divID) {
            $(divID).slideToggle();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="pSearch" runat="server" DefaultButton="lnkbtnSearch">
        <div class="controlDiv" style="margin-top: 15px;">
            <div class="fieldSearchDiv">
                <asp:TextBox ID="txtSearch" runat="server" CssClass="roundedCornerTextBox" Width="350px" />
                <asp:DropDownList ID="lstGroupFilter" runat="server" Width="200px" CssClass="roundedCornerDropDownList" onselectedindexchanged="lstGroupFilter_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                <ajax:TextBoxWatermarkExtender ID="txtSearch_TextBoxWatermarkExtender" runat="server"
                    TargetControlID="txtSearch" WatermarkText="SEARCH FOR A GROUP">
                </ajax:TextBoxWatermarkExtender>
                <ajax:AutoCompleteExtender ID="aceGroups" runat="server" TargetControlID="txtSearch"
                    ServiceMethod="FilterBusinessRule" EnableCaching="true" MinimumPrefixLength="1"
                    CompletionInterval="0" CompletionSetCount="3" CompletionListCssClass="popupCompletionList"
                    CompletionListItemCssClass="popupCompletionListItem" CompletionListHighlightedItemCssClass="popupCompletionListItemHighlight"
                    OnClientItemSelected="ClientItemSelected" FirstRowSelected="true" />
                <asp:HiddenField ID="hfGroupID" runat="server" Value="" />
            </div>
            <div class="buttonsSearchDiv">
                <asp:LinkButton ID="lnkbtnSearch" runat="server" CssClass="searchButton" OnClick="lnkbtnSearch_Click" />
            </div>
        </div>
        <br class="clearingSpacer" />
    </asp:Panel>
    <br />
    <asp:UpdatePanel ID="udpGroupsView" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:DataGrid ID="dgrGrid" runat="server" AutoGenerateColumns="False" AllowSorting="true"
                AllowPaging="true" PageSize="15" PagerStyle-Mode="NumericPages" OnPageIndexChanged="dgrGrid_PageIndexChanged"
                OnSortCommand="dgrGrid_SortCommand" CellPadding="3" CellSpacing="0" PagerStyle-NextPageText="Next"
                PagerStyle-PrevPageText="Prev" HeaderStyle-CssClass="dgrHeader" ItemStyle-CssClass="dgrItem"
                AlternatingItemStyle-CssClass="dgrAltItem" PagerStyle-CssClass="dgrPg" CssClass="dgr">
                <Columns>
                    <asp:BoundColumn DataField="iGroupID" Visible="false" />
                    <asp:BoundColumn DataField="strTitle" SortExpression="strTitle" HeaderText="Title" HeaderStyle-CssClass="dgrHeaderLeft" HeaderStyle-HorizontalAlign="Left" />   
                    
                    <asp:TemplateColumn HeaderText="Copy Group" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50px" >
                        <ItemTemplate>
                            <asp:HyperLink ID="lnkCopyGroup" runat="Server" CssClass="dgrLinkAdd" OnDataBinding="dgrGrid_PoplnkCopyGroups" />
                        </ItemTemplate>
                    </asp:TemplateColumn>

                    <asp:TemplateColumn HeaderText="Add Features" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50px" >
                        <ItemTemplate>
                            <asp:HyperLink ID="lnkAddFeaturesToGroups" runat="Server" CssClass="dgrLinkAdd" OnDataBinding="dgrGrid_PoplnkAddFeaturesToGroups" />
                        </ItemTemplate>
                    </asp:TemplateColumn>              
                    
                    <asp:TemplateColumn HeaderText="Add Phases" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50px" >
                        <ItemTemplate>
                            <asp:HyperLink ID="lnkAddPhasesToGroups" runat="Server" CssClass="dgrLinkAdd" OnDataBinding="dgrGrid_PoplnkAddPhasesToGroups" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    
                    <asp:TemplateColumn HeaderText="Edit" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50px">
                        <ItemTemplate>
                            <asp:HyperLink ID="lnkEditItem" runat="Server" CssClass="dgrLinkEdit" OnDataBinding="dgrGrid_PopEditLink" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    
                    <asp:TemplateColumn HeaderText="Delete" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50px" HeaderStyle-CssClass="dgrHeaderRight">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkDeleteItem" runat="server" CssClass="dgrLinkDelete" CommandArgument='<%# Eval("iGroupID") %>' OnClick="lnkDeleteItem_Click" OnClientClick="return jsDeleteConfirm()" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                </Columns>
            </asp:DataGrid>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div class="buttonsRightDiv" >
        <asp:LinkButton ID="lnkbtnAdd" runat="server" CssClass="addButton" OnClick="lnkbtnAdd_Click" PostBackUrl="frmGroupsAddModify.aspx" />
    </div>
</asp:Content>
