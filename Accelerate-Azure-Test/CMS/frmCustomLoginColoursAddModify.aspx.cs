using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;

public partial class CMS_frmCustomLoginColoursAddModify : System.Web.UI.Page
{
    clsUsers clsUsers;
    clsCustomLoginColours clsCustomLoginColours;
    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

             //### Determines if a javascript delete has been called
             if (!String.IsNullOrEmpty(Page.Request["__EVENTARGUMENT"]) && (Page.Request["__EVENTARGUMENT"].Split(':')[0] == "iRemoveImages"))
             DeleteImages(Convert.ToInt32(Page.Request["__EVENTARGUMENT"].Split(':')[1]));
        if (!IsPostBack)
        {

            //### If the iCustomLoginColourID is passed through then we want to instantiate the object with that iCustomLoginColourID
            if ((Request.QueryString["iCustomLoginColourID"] != "") && (Request.QueryString["iCustomLoginColourID"] != null))
            {
                clsCustomLoginColours = new clsCustomLoginColours(Convert.ToInt32(Request.QueryString["iCustomLoginColourID"]));

                //### Populate the form
                popFormData();
            }
            else
            {
                clsCustomLoginColours = new clsCustomLoginColours();
            }
            Session["clsCustomLoginColours"] = clsCustomLoginColours;
        }
        else
        {
            clsCustomLoginColours = (clsCustomLoginColours)Session["clsCustomLoginColours"];
        }
    }

    protected void lnkbtnBack_Click(object sender, EventArgs e)
    {
        //### Go back to previous page
        Response.Redirect("frmCustomLoginColoursView.aspx");
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        //### Validate registration process
        bool bCanSave = true;

        //bCanSave = clsValidation.IsNullOrEmpty(txtPathToImages, bCanSave);
        //bCanSave = clsValidation.IsNullOrEmpty(txtMasterImage, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtTitle, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtPanelBackgroundColour, bCanSave);
       bCanSave = clsValidation.IsNullOrEmpty(txtRememberMeTextColour, bCanSave);
       bCanSave = clsValidation.IsNullOrEmpty(txtButtonBackgroundColour, bCanSave);
       bCanSave = clsValidation.IsNullOrEmpty(txtButtonTextColour, bCanSave);
       bCanSave = clsValidation.IsNullOrEmpty(txtTextBelowButtonColour, bCanSave);

        if (bCanSave == true)
        {
            mandatoryDiv.Visible = false;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceHappy.png\" alt='' title=''/><div class=\"validationMessage\">CustomLoginColour added successfully</div></div>";
            SaveData();
        }
        else
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">Please fill out all mandatory fields - CustomLoginColour not added</div></div>";
        }
    }

    protected void lnkbtnClear_Click(object sender, EventArgs e)
    {
        mandatoryDiv.Visible = false;
        txtTitle.Text = "";
        clsValidation.SetValid(txtTitle);
        txtPanelBackgroundColour.Text = "";
        clsValidation.SetValid(txtPanelBackgroundColour);
        txtRememberMeTextColour.Text = "";
        clsValidation.SetValid(txtRememberMeTextColour);
        txtButtonBackgroundColour.Text = "";
        clsValidation.SetValid(txtButtonBackgroundColour);
        txtButtonTextColour.Text = "";
        clsValidation.SetValid(txtButtonTextColour);
        txtTextBelowButtonColour.Text = "";
        clsValidation.SetValid(txtTextBelowButtonColour);
    }

    #endregion 

    #region POPULATE DATA METHODS

    private void popFormData()
    {

         //### Populates images
         if (!string.IsNullOrEmpty(clsCustomLoginColours.strPathToImages))
         {
             lblUniquePath.Text = clsCustomLoginColours.strPathToImages;
             getList(clsCustomLoginColours.strPathToImages);
             //### Set Current Master Image
             List<string> lstImagesFileNames = (List<string>)Session["lstImagesFileNames"];
             try
             {
                 foreach (string strImageFileName in lstImagesFileNames)
                 {
                     if (strImageFileName == clsCustomLoginColours.strMasterImage)
                     {
                         RadioButton rdbMasterImage = dlImages.Items[lstImagesFileNames.IndexOf(strImageFileName)].FindControl("rdbMainImage") as RadioButton;
                         rdbMasterImage.Checked = true;
                         break;
                     }
                 }
             }
             catch { }
         }
        txtTitle.Text = clsCustomLoginColours.strTitle;
         txtPanelBackgroundColour.Text = clsCustomLoginColours.strPanelBackgroundColour;
         txtRememberMeTextColour.Text = clsCustomLoginColours.strRememberMeTextColour;
         txtButtonBackgroundColour.Text = clsCustomLoginColours.strButtonBackgroundColour;
         txtButtonTextColour.Text = clsCustomLoginColours.strButtonTextColour;
         txtTextBelowButtonColour.Text = clsCustomLoginColours.strTextBelowButtonColour;
    }
    
    #endregion

    #region SAVE DATA METHODS

    private void SaveData()
    {
        //### Add / Update
        clsCustomLoginColours.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsCustomLoginColours.iAddedBy = clsUsers.iUserID;
        clsCustomLoginColours.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsCustomLoginColours.iEditedBy = clsUsers.iUserID;

        //### Images related items
        clsCustomLoginColours.strTitle = txtTitle.Text;
        clsCustomLoginColours.strPathToImages = lblUniquePath.Text;
        clsCustomLoginColours.strMasterImage = GetMainImagePath(dlImages);
        clsCustomLoginColours.strPanelBackgroundColour = txtPanelBackgroundColour.Text;
        clsCustomLoginColours.strRememberMeTextColour = txtRememberMeTextColour.Text;
        clsCustomLoginColours.strButtonBackgroundColour = txtButtonBackgroundColour.Text;
        clsCustomLoginColours.strButtonTextColour = txtButtonTextColour.Text;
        clsCustomLoginColours.strTextBelowButtonColour = txtTextBelowButtonColour.Text;

        clsCustomLoginColours.strLink = createCSSFileLink();

        clsCustomLoginColours.Update();

        Session["dtCustomLoginColoursList"] = null;

        //### Go back to view page
        Response.Redirect("frmCustomLoginColoursView.aspx");
    }

    private string createCSSFileLink()
    {
        string strFileDirectory = AppDomain.CurrentDomain.BaseDirectory + "\\CustomLoginColourSchemes";

        if (!System.IO.Directory.Exists(strFileDirectory))
            System.IO.Directory.CreateDirectory(strFileDirectory);

        string strFullFilePath = strFileDirectory + "\\" + GetUniqueDocumentPath(strFileDirectory) + "\\" + txtTitle.Text + ".css";

        if (File.Exists(strFullFilePath))
            File.Delete(strFullFilePath);

        string strDirectoryForStyle = strFullFilePath.Replace(strFileDirectory + "\\" + GetUniqueDocumentPath(strFileDirectory) + "\\" + txtTitle.Text + ".css", strFileDirectory + "\\" + GetUniqueDocumentPath(strFileDirectory));

        if (!System.IO.Directory.Exists(strDirectoryForStyle))
            System.IO.Directory.CreateDirectory(strDirectoryForStyle);

        using (FileStream fstream = new FileStream(strFullFilePath, FileMode.Create))
        {
            using (StreamWriter sWriter = new StreamWriter(fstream, new ASCIIEncoding()))
            {
                sWriter.Write(createCSSFile());
            }
        }

        /*DocumentUpload.PostedFile.SaveAs(strUniqueDocumentFullPath + "\\" + strSaveLocation);*/
        strFullFilePath = strFullFilePath.Replace(AppDomain.CurrentDomain.BaseDirectory + "\\", "");

        return strFullFilePath.Replace(AppDomain.CurrentDomain.BaseDirectory, "");
    }

    private string createCSSFile()
    {
        StringBuilder strbCssFileContent = new StringBuilder();

        strbCssFileContent.AppendLine(".login-box.clearfix.animated.flipInY{background: " + txtPanelBackgroundColour.Text + " !important;}");
        strbCssFileContent.AppendLine(".buttonHolder{background: " + txtPanelBackgroundColour.Text + " !important;}");
        strbCssFileContent.AppendLine(".btn.btn-white{background: " + txtButtonBackgroundColour.Text + " !important;color: " + txtButtonTextColour.Text + " !important;}");
        strbCssFileContent.AppendLine("a{color: " + txtTextBelowButtonColour.Text + " !important;}");


        //strbCssFileContent.AppendLine(".well.ProfileSection{background: " + txtProfileBackgroundColour.Text + ";color: " + txtProfileColourText.Text + ";}");

        return strbCssFileContent.ToString();
    }

    private string GetUniqueDocumentPath(string strFileDirectory)
    {
        int iCount = 1;
        //### First we need to get the path
        while (System.IO.Directory.Exists(strFileDirectory + "\\LoginScheme" + iCount) == true)
        {
            iCount++;
        }
        return "Scheme" + iCount;

    }

    #endregion
    #region IMAGE METHODS

    List<string> lstImages;
    List<string> lstImagesFileNames;
    int iMaxImages = 3;
    string strUniqueFullPath = AppDomain.CurrentDomain.BaseDirectory + "\\CustomLoginColours";
    protected void btnUpload_Click(object sender, EventArgs e)
    {
        if (Session["lstImages"] == null)
        {
            lstImages = new List<string>();
            Session["lstImages"] = lstImages;
        }
        //### Check that they have ONLY HAVE MAX NUMBER OF Images in the datalist
        if (dlImages.Items.Count == iMaxImages)
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "You can only have " + iMaxImages.ToString() + " Images.";
            getList(lblUniquePath.Text);
        }
        else
        {
            mandatoryDiv.Visible = false;

            string strUniquePath;
            if (lblUniquePath.Text == "")
            {
                strUniquePath = GetUniquePath();
                lblUniquePath.Text = strUniquePath;
            }
            else
            {
                strUniquePath = lblUniquePath.Text;
            }
            UploadImages(strUniquePath);
            getList(strUniquePath);
        }
    }
    private string GetUniquePath()
    {
        int iCount = 1;
        //### First we need to get the path
        while (System.IO.Directory.Exists(strUniqueFullPath + "\\CustomLoginColours" + iCount) == true)
        {
            iCount++;
        }
        return "CustomLoginColours" + iCount;
    }

    protected void UploadImages(String strUniquePath)
    {
        if (FileUpload.PostedFile.ContentLength > 0 && FileUpload.PostedFile.ContentLength < 1073741824)
        {

            //### Upload files to unique folder
            string strUploadFileName = "";
            strUploadFileName = System.IO.Path.GetFileName(FileUpload.PostedFile.FileName);
            string strSaveLocation = "";
            strSaveLocation = strUniqueFullPath + "\\" + strUniquePath + "\\" + strUploadFileName;

            if (!System.IO.Directory.Exists(strUniqueFullPath + "\\" + strUniquePath))
            {
                System.IO.Directory.CreateDirectory(strUniqueFullPath + "\\" + strUniquePath);
            }
            FileUpload.PostedFile.SaveAs(strSaveLocation);

            CopyAndResizePic(strSaveLocation);
            getList(strUniqueFullPath + "\\" + strUniquePath);
        }
        else
        {
            lblValidationMessage.Text = "The file should be between 0 and 1Mb.";
            lblUploadError.Text = "The file should be between 0 and 1Mb.";
        }
    }

    private void CopyAndResizePic(String strFullPath)
    {
        try
        {

            String strFileName;
            String strNewFilePath;

            //### Main Images
            String strLrgFileName;

            strFileName = Path.GetFileName(strFullPath);
            strNewFilePath = strFullPath.Replace(strFileName, "");
            strNewFilePath = strNewFilePath + Path.GetFileNameWithoutExtension(strFullPath);
            strNewFilePath = strNewFilePath + "_lrg";
            strNewFilePath = strNewFilePath + Path.GetExtension(strFullPath);

            File.Copy(strFullPath, strNewFilePath);
            strLrgFileName = Path.GetFileName(strNewFilePath);

            clsCommonFunctions.ResizeImage(strNewFilePath, strNewFilePath, 600, 600, false);

            //### Thumbnail
            String strSmlFileName;

            strFileName = Path.GetFileName(strFullPath);
            strNewFilePath = strFullPath.Replace(strFileName, "");
            strNewFilePath = strNewFilePath + Path.GetFileNameWithoutExtension(strFullPath);
            strNewFilePath = strNewFilePath + "_sml";
            strNewFilePath = strNewFilePath + Path.GetExtension(strFullPath);

            File.Copy(strFullPath, strNewFilePath);
            strSmlFileName = Path.GetFileName(strNewFilePath);

            clsCommonFunctions.ResizeImage(strNewFilePath, strNewFilePath, 140, 140, false);
            
        }
        catch (Exception ex){}
    }

    public void getList(String strPathToFolder)
    {
        lstImages = new List<string>();
        lstImagesFileNames = new List<string>();
        try
        {
            string strPath = strPathToFolder;
            string[] files = Directory.GetFiles(strUniqueFullPath + "\\" + strPath);

            string iCustomLoginColourID = "";
            if (!string.IsNullOrEmpty(Request.QueryString["iCustomLoginColourID"]))
                iCustomLoginColourID = Request.QueryString["iCustomLoginColourID"];

            int iImagesCount = 0;

            foreach (string strName in files)
            {
                if (strName.IndexOf("_sml") != -1)
                {
                    string strHTMLImages = strName.Replace(AppDomain.CurrentDomain.BaseDirectory + "\\", "..\\");
                    strHTMLImages = strHTMLImages.Replace("\\", "/");

                    //### Generates a javascript postback for the delete method
                    String strPostBack = Page.ClientScript.GetPostBackEventReference(this, "iRemoveImages:" + iImagesCount);

                    lstImages.Add(@"<tr style='text-align: centre;'>
                                        <td style='text-align: centre;'>
                                            <a class='ImagesColorBox' href='" + strHTMLImages.Replace("_sml", "_lrg") + "'><img src='" + strHTMLImages + @"' alt='' title='' style='border: solid 5px #ffffff;' /></a><br /><br />" +
                                            "<center><div class='buttonStyle' style='padding-top: 5px;'><a href=\"javascript:" + strPostBack + "\" style='color: #ffffff;'><div class='deleteButton'></div></a></div></center>" + @"
                                        </td>
                                    </tr>");
                    lstImagesFileNames.Add(Path.GetFileName(strName).Replace("_sml", ""));
                    iImagesCount++;
                }
            }
            dlImages.DataSource = lstImages;
            dlImages.DataBind();

            Session["lstImages"] = lstImages;
            Session["lstImagesFileNames"] = lstImagesFileNames;
        }
        catch (Exception ex) {}
    }

    private string GetMainImagePath(DataList dtlTarget)
    {
        string strReturn = "";

        foreach (DataListItem dliTarget in dtlTarget.Items)
        {
            RadioButton rdbMainImage = (RadioButton)dliTarget.FindControl("rdbMainImage");
            if (rdbMainImage.Checked)
            {
                lstImagesFileNames = (List<string>)Session["lstImagesFileNames"];
                strReturn = lstImagesFileNames[dliTarget.ItemIndex];
                break;
            }
        }
        return strReturn;
    }

    private void DeleteImages(int iImagesIndex)
    {

        //### Deletes all Images related to the target Images.
        lstImages = (List<string>)Session["lstImages"];
        lstImagesFileNames = (List<string>)Session["lstImagesFileNames"];

        lstImages.RemoveAt(iImagesIndex);
        Session["lstImages"] = lstImages;

        string[] files = Directory.GetFiles(strUniqueFullPath + "\\" + lblUniquePath.Text);
        foreach (string file in files)
        {
            if (Path.GetFileName(file) == lstImagesFileNames[iImagesIndex].ToString())
            {
                //### Remove all Images
                File.Delete(file);
                File.Delete(file.Replace(Path.GetExtension(file), "_sml" + Path.GetExtension(file)));
                File.Delete(file.Replace(Path.GetExtension(file), "_med" + Path.GetExtension(file)));
                File.Delete(file.Replace(Path.GetExtension(file), "_lrg" + Path.GetExtension(file)));
                break;
            }
        }
        lstImagesFileNames.RemoveAt(iImagesIndex);
        ViewState["lstImagesFileNames"] = lstImagesFileNames;
        getList(lblUniquePath.Text);
    }

    #endregion
}