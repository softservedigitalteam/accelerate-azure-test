using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text.RegularExpressions;

public partial class CMS_frmInactiveAccountUsersAddModify : System.Web.UI.Page
{
    clsUsers clsUsers;
    clsInactiveAccountUsers clsInactiveAccountUsers;
    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

        if (!IsPostBack)
        {
             //popGroup();

            //### If the iInactiveAccountUserID is passed through then we want to instantiate the object with that iInactiveAccountUserID
            if ((Request.QueryString["iInactiveAccountUserID"] != "") && (Request.QueryString["iInactiveAccountUserID"] != null))
            {
                clsInactiveAccountUsers = new clsInactiveAccountUsers(Convert.ToInt32(Request.QueryString["iInactiveAccountUserID"]));

                //### Populate the form
                popFormData();
            }
            else
            {
                clsInactiveAccountUsers = new clsInactiveAccountUsers();
            }
            Session["clsInactiveAccountUsers"] = clsInactiveAccountUsers;
        }
        else
        {
            clsInactiveAccountUsers = (clsInactiveAccountUsers)Session["clsInactiveAccountUsers"];
        }
    }

    protected void lnkbtnBack_Click(object sender, EventArgs e)
    {
        //### Go back to previous page
        Response.Redirect("frmInactiveAccountUsersView.aspx");
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        //### Validate registration process
        bool bCanSave = true;

       bCanSave = clsValidation.IsNullOrEmpty(txtFirstName, bCanSave);
       bCanSave = clsValidation.IsNullOrEmpty(txtSurname, bCanSave);
       bCanSave = clsValidation.IsNullOrEmpty(txtEmailAddress, bCanSave);
       //bCanSave = clsValidation.IsNullOrEmpty(txtJobTitle, bCanSave);
       //bCanSave = clsValidation.IsNullOrEmpty(lstGroup, bCanSave);

        if (bCanSave == true)
        {
            mandatoryDiv.Visible = false;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceHappy.png\" alt='' title=''/><div class=\"validationMessage\">InactiveAccountUser added successfully</div></div>";
            SaveData();
        }
        else
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">Please fill out all mandatory fields - InactiveAccountUser not added</div></div>";
        }
    }

    protected void lnkbtnClear_Click(object sender, EventArgs e)
    {
        mandatoryDiv.Visible = false;

        txtFirstName.Text = "";
        clsValidation.SetValid(txtFirstName);
        txtSurname.Text = "";
        clsValidation.SetValid(txtSurname);
        txtEmailAddress.Text = "";
        clsValidation.SetValid(txtEmailAddress);
        txtPhoneNumber.Text = "";
        clsValidation.SetValid(txtPhoneNumber);
        txtJobTitle.Text = "";
        clsValidation.SetValid(txtJobTitle);
        //lstGroup.SelectedValue = "0";
        //clsValidation.SetValid(lstGroup);
    }

    #endregion 

    #region POPULATE DATA METHODS

    private void popFormData()
    {
         txtFirstName.Text = clsInactiveAccountUsers.strFirstName;
         txtSurname.Text = clsInactiveAccountUsers.strSurname;
         txtEmailAddress.Text = clsInactiveAccountUsers.strEmailAddress;
         txtPhoneNumber.Text = clsInactiveAccountUsers.strPhoneNumber;
         txtJobTitle.Text = clsInactiveAccountUsers.strJobTitle;
         //lstGroup.SelectedValue = clsInactiveAccountUsers.iGroupID.ToString();
    }
    
    //private void popGroup()
    //{
    //     DataTable dtGroupsList = new DataTable();
    //     lstGroup.DataSource = clsGroups.GetGroupsList();

    //     //### Populates the drop down list with PK and TITLE;
    //     lstGroup.DataValueField = "iGroupID";
    //     lstGroup.DataTextField = "strTitle";

    //     //### Bind the data to the list;
    //     lstGroup.DataBind();

    //     //### Add default select option;
    //     lstGroup.Items.Insert(0, new ListItem("--Not Selected--", "0"));
    //}
    
    #endregion

    #region SAVE DATA METHODS

    private void SaveData()
    {
        //### Add / Update
        clsInactiveAccountUsers.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsInactiveAccountUsers.iAddedBy = clsUsers.iUserID;
        clsInactiveAccountUsers.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsInactiveAccountUsers.iEditedBy = clsUsers.iUserID;
        clsInactiveAccountUsers.strFirstName = txtFirstName.Text;
        clsInactiveAccountUsers.strSurname = txtSurname.Text;

        //removing white space from email address
        string strEmailAddress = txtEmailAddress.Text;
        strEmailAddress = strEmailAddress.Trim();
        strEmailAddress = Regex.Replace(strEmailAddress, @"\s", "");
        strEmailAddress = strEmailAddress.ToLower();

        clsInactiveAccountUsers.strEmailAddress = strEmailAddress;
        clsInactiveAccountUsers.strPhoneNumber = txtPhoneNumber.Text;
        clsInactiveAccountUsers.strJobTitle = txtJobTitle.Text;
        //clsInactiveAccountUsers.iGroupID = Convert.ToInt32(lstGroup.SelectedValue.ToString());

        clsInactiveAccountUsers.Update();

        Session["dtInactiveAccountUsersList"] = null;

        //### Go back to view page
        Response.Redirect("frmInactiveAccountUsersView.aspx");
    }

    #endregion
}