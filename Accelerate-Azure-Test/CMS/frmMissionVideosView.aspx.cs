
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text;

/// <summary>
/// Summary description for clsMissionVideosView
/// </summary>
public partial class CMS_clsMissionVideosView : System.Web.UI.Page
{
    #region CLASSES AND VARIABLES

    clsUsers clsUsers;
    DataTable dtMissionVideosList;

    List<clsMissionVideos> glstMissionVideos;

    #endregion

    #region EVENT MODULES

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

dgrGrid.ItemCreated += new DataGridItemEventHandler(clsCommonFunctions.dgrGrid_PagerPopulate);

        if (!Page.IsPostBack)
        {
            Session["dtMissionVideosList"] = null;
            PopulateFormData();
        }
        else
        {
            if (Session["glstMissionVideos"] == null)
            {
                glstMissionVideos = new List<clsMissionVideos>();
                Session["glstMissionVideos"] = glstMissionVideos;
            }
            else
                glstMissionVideos = (List<clsMissionVideos>)Session["glstMissionVideos"];
        }
            PopulateFormData();

    }

    protected string EscapeLikeValue(string valueWithoutWildcards)
    {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < valueWithoutWildcards.Length; i++)
        {
            char c = valueWithoutWildcards[i];
            if (c == '*' || c == '%' || c == '[' || c == ']')
                sb.Append("[").Append(c).Append("]");
            else if (c == '\'')
                sb.Append("''");
            else
                sb.Append(c);
        }
        return sb.ToString();
    }

    protected void lnkbtnSearch_Click(object sender, EventArgs e)
    {
        string EscapedString = EscapeLikeValue(txtSearch.Text.ToString());
        string FilterExpression ="(strColor +' '+ strVideoLink +' '+ strMasterImage) LIKE '%" + EscapedString + "%'";
        DataTable dtMissionVideosList = clsMissionVideos.GetMissionVideosList(FilterExpression, "");

        Session["dtMissionVideosList"] = dtMissionVideosList;

        PopulateFormData();
    }

    protected void lnkbtnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmMissionVideosAddModify.aspx");
    }

    #endregion

    #region DATA GRID METHODS

    private void PopulateFormData()
    {
        //### Populate datagrid using clsMissionVideosList object
        try
        {
            dtMissionVideosList = new DataTable();

            if (Session["dtMissionVideosList"] == null)
                dtMissionVideosList = clsMissionVideos.GetMissionVideosList("", "dtAdded DESC");
            else
                dtMissionVideosList = (DataTable)Session["dtMissionVideosList"];

            dgrGrid.DataSource = dtMissionVideosList;

            //### Add this check to trap if the last item on the page is deleted
            if (Convert.ToInt16(dgrGrid.CurrentPageIndex) < Convert.ToInt16(Session["MissionVideosView_CurrentPageIndex"]))
            {
                dgrGrid.CurrentPageIndex = 0;
            }
            else
            {
                if (Session["MissionVideosView_CurrentPageIndex"] != null)
                {
                    dgrGrid.CurrentPageIndex = (int)Session["MissionVideosView_CurrentPageIndex"];
                }
            }

            dgrGrid.DataBind();

        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void lnkDeleteItem_Click(object sender, EventArgs e)
    {
        int iMissionVideoID = int.Parse((sender as LinkButton).CommandArgument);

        clsMissionVideos.Delete(iMissionVideoID);

        DataTable dtMissionVideosList = new DataTable();

        if (((txtSearch.Text == "SEARCH FOR A MISSION VIDEO") == false) && ((txtSearch.Text == "") == false))
        {
            string FilterExpression = "(strColor +' '+ strVideoLink +' '+ strMasterImage) LIKE '%" + txtSearch.Text + "%'";
            dtMissionVideosList = clsMissionVideos.GetMissionVideosList(FilterExpression, "");
        }
        else
        {
            dtMissionVideosList = clsMissionVideos.GetMissionVideosList();
        }

        Session["dtMissionVideosList"] = dtMissionVideosList;

        PopulateFormData();
    }

    protected void dgrGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //### Method is used when the page index is changed.
        try
        {
            Session["MissionVideosView_CurrentPageIndex"] = e.NewPageIndex;
            dgrGrid.CurrentPageIndex = e.NewPageIndex;
            PopulateFormData();
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgrGrid_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        //### Sort Method
        dtMissionVideosList = new DataTable();

        if (Session["dtMissionVideosList"] == null)
            dtMissionVideosList = clsMissionVideos.GetMissionVideosList();
        else
            dtMissionVideosList = (DataTable)Session["dtMissionVideosList"];

        DataView dvTemp = dtMissionVideosList.DefaultView;

        dvTemp.Sort = e.SortExpression;
        dtMissionVideosList = dvTemp.ToTable();
        Session["dtMissionVideosList"] = dtMissionVideosList;

        PopulateFormData();
    }

    public void dgrGrid_PopEditLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkEditItem = (HyperLink)sender;
            DataGridItem dgrItemEdit = (DataGridItem)lnkEditItem.Parent.Parent;

            //### Get the iMissionVideoID
            int iMissionVideoID = 0;
            iMissionVideoID = int.Parse(dgrItemEdit.Cells[0].Text);

            //### Add attributes to edit link
            lnkEditItem.Attributes.Add("href", "frmMissionVideosAddModify.aspx?action=edit&iMissionVideoID=" + iMissionVideoID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

     public void dgrGrid_PopDeleteLink(object sender, EventArgs e)
     {
          try
          {
             //### Add hyperlink to datagrid item
             HyperLink lnkDeleteItem = (HyperLink)sender;
             DataGridItem dgrItemDelete = (DataGridItem)lnkDeleteItem.Parent.Parent;

             //### Get the iMissionVideoID
             int iMissionVideoID = 0;
             iMissionVideoID = int.Parse(dgrItemDelete.Cells[0].Text);

             //### Add attributes to delete link
             lnkDeleteItem.CssClass = "dgrDeleteLink";
             lnkDeleteItem.Attributes.Add("href", "frmMissionVideosView.aspx?action=delete&iMissionVideoID=" + iMissionVideoID);
           }
           catch (Exception ex)
           {
                 Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
           }
     }

    #endregion

    #region MissionVideos FILTERING

    /// <summary>
    /// Enables the AutoCompleteExtender
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    /// 
    [WebMethod]
    [ScriptMethod]
    public static string[] FilterBusinessRule(string prefixText, int count)
    {
        string[] strReturnList = new string[] { };

        string FilterExpression = "(strColor +' '+ strVideoLink +' '+ strMasterImage) LIKE '%" + prefixText + "%'";
        DataTable dtMissionVideos = clsMissionVideos.GetMissionVideosList(FilterExpression, "");
        List<string> glstMissionVideos = new List<string>();

        if (dtMissionVideos.Rows.Count > 0)
        {
            foreach (DataRow dtrMissionVideos in dtMissionVideos.Rows)
            {
                glstMissionVideos.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem("" + dtrMissionVideos["strColor"].ToString() +' '+"" + dtrMissionVideos["strVideoLink"].ToString() +' '+"" + dtrMissionVideos["strMasterImage"].ToString(), dtrMissionVideos["iMissionVideoID"].ToString()));
            }
        }
        else
            glstMissionVideos.Add("No MissionVideos Available.");
        strReturnList = glstMissionVideos.ToArray();
        return strReturnList;
    }

    #endregion
}
