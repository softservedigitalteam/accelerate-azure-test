using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class CMS_frmPortalFeaturesAddModify : System.Web.UI.Page
{
    clsUsers clsUsers;
    clsPortalFeatures clsPortalFeatures;
    //clsDocuments clsDocuments;

    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

        //### Determines if a javascript delete has been called
        if (!String.IsNullOrEmpty(Page.Request["__EVENTARGUMENT"]) && (Page.Request["__EVENTARGUMENT"].Split(':')[0] == "iRemoveImages"))
            DeleteImages(Convert.ToInt32(Page.Request["__EVENTARGUMENT"].Split(':')[1]));
        if (!String.IsNullOrEmpty(Page.Request["__EVENTARGUMENT"]) && (Page.Request["__EVENTARGUMENT"].Split(':')[0] == "iRemoveDocument"))
            DeleteDocument(Convert.ToInt32(Page.Request["__EVENTARGUMENT"].Split(':')[1]));

        
        if (!IsPostBack)
        {

            //### If the iPortalFeatureID is passed through then we want to instantiate the object with that iPortalFeatureID
            if ((Request.QueryString["iPortalFeatureID"] != "") && (Request.QueryString["iPortalFeatureID"] != null))
            {
                clsPortalFeatures = new clsPortalFeatures(Convert.ToInt32(Request.QueryString["iPortalFeatureID"]));

                //### Populate the form
                popFormData();
            }
            else
            {
                clsPortalFeatures = new clsPortalFeatures();
            }
            Session["clsPortalFeatures"] = clsPortalFeatures;
        }
        else
        {
            if (IsPostBack && FileUpload.PostedFile != null && DocumentUpload.PostedFile == null)
            {
                if (FileUpload.PostedFile.FileName.Length > 0)
                {
                    if (Session["lstImages"] == null)
                    {
                        lstImages = new List<string>();
                        Session["lstImages"] = lstImages;
                    }
                    //### Check that they have ONLY HAVE MAX NUMBER OF Images in the datalist
                    if (dlImages.Items.Count == iMaxImages)
                    {
                        mandatoryDiv.Visible = true;
                        lblValidationMessage.Text = "You can only have " + iMaxImages.ToString() + " Images.";
                        getList(lblUniquePath.Text);
                    }
                    else
                    {
                        mandatoryDiv.Visible = false;

                        string strUniquePath;
                        if (lblUniquePath.Text == "")
                        {
                            strUniquePath = GetUniquePath();
                            lblUniquePath.Text = strUniquePath;
                        }
                        else
                        {
                            strUniquePath = lblUniquePath.Text;
                        }
                        UploadImages(strUniquePath);
                        getList(strUniquePath);
                    }
                }
            }

            else if (IsPostBack && DocumentUpload.PostedFile != null && FileUpload.PostedFile == null)
            {
                if (DocumentUpload.PostedFile.FileName.Length > 0)
                {
                    if (Session["lstDocument"] == null)
                    {
                        lstDocument = new List<string>();
                        Session["lstDocument"] = lstDocument;
                    }
                    //### Check that they have ONLY HAVE MAX NUMBER OF Document in the datalist
                    if (dlDocument.Items.Count == iMaxDocument)
                    {
                        mandatoryDiv.Visible = true;
                        lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">You can only upload " + iMaxDocument.ToString() + " document per entry.</div></div>";
                        getDocumentList(lblUniqueDocumentPath.Text);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "setValid", "setValidFile('" + DocumentUpload.ClientID + "',true)", true);
                    }
                    else
                    {
                        mandatoryDiv.Visible = false;

                        string strUniquePath;
                        if (lblUniqueDocumentPath.Text == "")
                        {
                            strUniquePath = GetUniqueDocumentPath();
                            lblUniqueDocumentPath.Text = strUniquePath;
                        }
                        else
                        {
                            strUniquePath = lblUniqueDocumentPath.Text;
                        }

                        UploadDocument(strUniquePath);
                        getDocumentList(strUniquePath);
                    }
                }
            }

            else if (IsPostBack && DocumentUpload.PostedFile != null && FileUpload.PostedFile != null)
            {
                if (DocumentUpload.PostedFile.FileName.Length > 0)
                {
                    if (Session["lstDocument"] == null)
                    {
                        lstDocument = new List<string>();
                        Session["lstDocument"] = lstDocument;
                    }
                    //### Check that they have ONLY HAVE MAX NUMBER OF Document in the datalist
                    if (dlDocument.Items.Count == iMaxDocument)
                    {
                        mandatoryDiv.Visible = true;
                        lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">You can only upload " + iMaxDocument.ToString() + " document per entry.</div></div>";
                        getDocumentList(lblUniqueDocumentPath.Text);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "setValid", "setValidFile('" + DocumentUpload.ClientID + "',true)", true);
                    }
                    else
                    {
                        mandatoryDiv.Visible = false;

                        string strUniquePath;
                        if (lblUniqueDocumentPath.Text == "")
                        {
                            strUniquePath = GetUniqueDocumentPath();
                            lblUniqueDocumentPath.Text = strUniquePath;
                        }
                        else
                        {
                            strUniquePath = lblUniqueDocumentPath.Text;
                        }

                        UploadDocument(strUniquePath);
                        getDocumentList(strUniquePath);
                    }
                }
                if (FileUpload.PostedFile.FileName.Length > 0)
                {
                    if (Session["lstImages"] == null)
                    {
                        lstImages = new List<string>();
                        Session["lstImages"] = lstImages;
                    }
                    //### Check that they have ONLY HAVE MAX NUMBER OF Images in the datalist
                    if (dlImages.Items.Count == iMaxImages)
                    {
                        mandatoryDiv.Visible = true;
                        lblValidationMessage.Text = "You can only have " + iMaxImages.ToString() + " Images.";
                        getList(lblUniquePath.Text);
                    }
                    else
                    {
                        mandatoryDiv.Visible = false;

                        string strUniquePath;
                        if (lblUniquePath.Text == "")
                        {
                            strUniquePath = GetUniquePath();
                            lblUniquePath.Text = strUniquePath;
                        }
                        else
                        {
                            strUniquePath = lblUniquePath.Text;
                        }
                        UploadImages(strUniquePath);
                        getList(strUniquePath);
                    }
                }
            }

            clsPortalFeatures = (clsPortalFeatures)Session["clsPortalFeatures"];
        }
    }

    protected void lnkbtnBack_Click(object sender, EventArgs e)
    {
        //### Go back to previous page
        Response.Redirect("frmPortalFeaturesView.aspx");
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        //### Validate registration process
        bool bCanSave = true;

       bCanSave = clsValidation.IsNullOrEmpty(txtTitle, bCanSave);
       bCanSave = clsValidation.IsNullOrEmpty(txtDescription, bCanSave);

       if (lblUniquePath.Text == "")
       {
           bCanSave = false;
           mandatoryDiv.Visible = true;
           lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">Please upload image and fill out all mandatory field - TeamShip not addeds</div></div>";

       }
       else
       {

           if (bCanSave == true)
           {
               mandatoryDiv.Visible = false;
               lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceHappy.png\" alt='' title=''/><div class=\"validationMessage\">PortalFeature added successfully</div></div>";
               SaveData();
           }
           else
           {
               mandatoryDiv.Visible = true;
               lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">Please fill out all mandatory fields - PortalFeature not added</div></div>";
           }
       }
    }

    protected void lnkbtnClear_Click(object sender, EventArgs e)
    {
        mandatoryDiv.Visible = false;

        txtTitle.Text = "";
        clsValidation.SetValid(txtTitle);
        txtDescription.Text = "";
        clsValidation.SetValid(txtDescription);
        txtYouTubeLink.Text = "";
        clsValidation.SetValid(txtYouTubeLink);
    }

    #endregion 

    #region POPULATE DATA METHODS

    private void popFormData()
    {
         txtTitle.Text = clsPortalFeatures.strTitle;
         txtDescription.Text = clsPortalFeatures.strDescription;
         txtYouTubeLink.Text = clsPortalFeatures.strYouTubeLink;

         if(clsPortalFeatures.bIsNormal == true)
         {
             lstFeatureType.SelectedIndex = 1;
         }
         else if(clsPortalFeatures.bIsChecklist == true)
         {
             lstFeatureType.SelectedIndex = 2;
         }
         else if (clsPortalFeatures.bIsMultiple == true)
         {
             lstFeatureType.SelectedIndex = 3;
         }

         //### Populates images
         if (!string.IsNullOrEmpty(clsPortalFeatures.strPathToImages))
         {

             lblUniquePath.Text = clsPortalFeatures.strPathToImages;
             getList(clsPortalFeatures.strPathToImages);
             //### Set Current Master Image
             List<string> lstImagesFileNames = (List<string>)Session["lstImagesFileNames"];
             try
             {
                 foreach (string strImageFileName in lstImagesFileNames)
                 {
                     if (strImageFileName == clsPortalFeatures.strMasterImage)
                     {
                         RadioButton rdbMasterImage = dlImages.Items[lstImagesFileNames.IndexOf(strImageFileName)].FindControl("rdbMainImage") as RadioButton;
                         rdbMasterImage.Checked = true;
                         break;
                     }
                 }
             }
             catch { }

             //### Populates images
             if (!string.IsNullOrEmpty(clsPortalFeatures.strPathToDocument))
             {
                 lblUniqueDocumentPath.Text = clsPortalFeatures.strPathToDocument;
                 //lblFileSize.Text = clsPortalFeatures.strFileSize;
                 getDocumentList(clsPortalFeatures.strPathToDocument);
             }

         }
    }
    
    #endregion

    #region SAVE DATA METHODS

    private void SaveData()
    {
        //### Add / Update
        clsPortalFeatures.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsPortalFeatures.iAddedBy = clsUsers.iUserID;
        clsPortalFeatures.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsPortalFeatures.iEditedBy = clsUsers.iUserID;
        clsPortalFeatures.iGroupID = 0;
        clsPortalFeatures.strTitle = txtTitle.Text;
        clsPortalFeatures.strDescription = txtDescription.Text;
        clsPortalFeatures.strYouTubeLink = txtYouTubeLink.Text;

        //### Images related items
        clsPortalFeatures.strPathToImages = lblUniquePath.Text;
        clsPortalFeatures.strMasterImage = GetMainImagePath(dlImages);
        clsPortalFeatures.strPathToDocument = lblUniqueDocumentPath.Text;
        clsPortalFeatures.strMasterDocument = GetMainDocumentPath(dlDocument);

        string strSelectedValue = lstFeatureType.SelectedValue.ToString();

        if(strSelectedValue == "Normal")
        {
            clsPortalFeatures.bIsNormal = true;
            clsPortalFeatures.bIsChecklist = false;
            clsPortalFeatures.bIsMultiple = false;
        }
        else if(strSelectedValue == "Checklist")
        {
            clsPortalFeatures.bIsNormal = false;
            clsPortalFeatures.bIsChecklist = true;
            clsPortalFeatures.bIsMultiple = false;
        }
        else if(strSelectedValue == "Multiple")
        {
            clsPortalFeatures.bIsNormal = false;
            clsPortalFeatures.bIsChecklist = false;
            clsPortalFeatures.bIsMultiple = true;
        }

        clsPortalFeatures.Update();

        Session["dtPortalFeaturesList"] = null;

        //### Go back to view page
        Response.Redirect("frmPortalFeaturesView.aspx");
    }

    #endregion

    #region IMAGE METHODS

    List<string> lstImages;
    List<string> lstImagesFileNames;
    int iMaxImages = 1;
    string strUniqueFullPath = AppDomain.CurrentDomain.BaseDirectory + "\\PortalFeatures";
    protected void btnUpload_Click(object sender, EventArgs e)
    {
        if (Session["lstImages"] == null)
        {
            lstImages = new List<string>();
            Session["lstImages"] = lstImages;
        }
        //### Check that they have ONLY HAVE MAX NUMBER OF Images in the datalist
        if (dlImages.Items.Count == iMaxImages)
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "You can only have " + iMaxImages.ToString() + " Images.";
            getList(lblUniquePath.Text);
        }
        else
        {
            mandatoryDiv.Visible = false;

            string strUniquePath;
            if (lblUniquePath.Text == "")
            {
                strUniquePath = GetUniquePath();
                lblUniquePath.Text = strUniquePath;
            }
            else
            {
                strUniquePath = lblUniquePath.Text;
            }
            UploadImages(strUniquePath);
            getList(strUniquePath);
        }
    }
    private string GetUniquePath()
    {
        int iCount = 1;
        //### First we need to get the path
        while (System.IO.Directory.Exists(strUniqueFullPath + "\\PortalFeatures" + iCount) == true)
        {
            iCount++;
        }
        return "PortalFeatures" + iCount;
    }

    protected void UploadImages(String strUniquePath)
    {
        if (FileUpload.PostedFile.ContentLength > 0 && FileUpload.PostedFile.ContentLength < 1073741824)
        {

            //### Upload files to unique folder
            string strUploadFileName = "";
            strUploadFileName = System.IO.Path.GetFileName(FileUpload.PostedFile.FileName);
            string strSaveLocation = "";
            strSaveLocation = strUniqueFullPath + "\\" + strUniquePath + "\\" + strUploadFileName;

            if (!System.IO.Directory.Exists(strUniqueFullPath + "\\" + strUniquePath))
            {
                System.IO.Directory.CreateDirectory(strUniqueFullPath + "\\" + strUniquePath);
            }
            FileUpload.PostedFile.SaveAs(strSaveLocation);

            CopyAndResizePic(strSaveLocation);
            getList(strUniqueFullPath + "\\" + strUniquePath);
        }
        else
        {
            lblValidationMessage.Text = "The file should be between 0 and 1Mb.";
            lblUploadError.Text = "The file should be between 0 and 1Mb.";
        }
    }

    private void CopyAndResizePic(String strFullPath)
    {
        try
        {

            String strFileName;
            String strNewFilePath;

            //### Main Images
            String strLrgFileName;

            strFileName = Path.GetFileName(strFullPath);
            strNewFilePath = strFullPath.Replace(strFileName, "");
            strNewFilePath = strNewFilePath + Path.GetFileNameWithoutExtension(strFullPath);
            strNewFilePath = strNewFilePath + "_lrg";
            strNewFilePath = strNewFilePath + Path.GetExtension(strFullPath);

            File.Copy(strFullPath, strNewFilePath);
            strLrgFileName = Path.GetFileName(strNewFilePath);

            clsCommonFunctions.ResizeImage(strNewFilePath, strNewFilePath, 600, 600, false);

            //### Thumbnail
            String strSmlFileName;

            strFileName = Path.GetFileName(strFullPath);
            strNewFilePath = strFullPath.Replace(strFileName, "");
            strNewFilePath = strNewFilePath + Path.GetFileNameWithoutExtension(strFullPath);
            strNewFilePath = strNewFilePath + "_sml";
            strNewFilePath = strNewFilePath + Path.GetExtension(strFullPath);

            File.Copy(strFullPath, strNewFilePath);
            strSmlFileName = Path.GetFileName(strNewFilePath);

            clsCommonFunctions.ResizeImage(strNewFilePath, strNewFilePath, 140, 140, false);
            
        }
        catch (Exception ex){}
    }

    public void getList(String strPathToFolder)
    {
        lstImages = new List<string>();
        lstImagesFileNames = new List<string>();
        try
        {
            string strPath = strPathToFolder;
            string[] files = Directory.GetFiles(strUniqueFullPath + "\\" + strPath);

            string iPortalFeatureID = "";
            if (!string.IsNullOrEmpty(Request.QueryString["iPortalFeatureID"]))
                iPortalFeatureID = Request.QueryString["iPortalFeatureID"];

            int iImagesCount = 0;

            foreach (string strName in files)
            {
                if (strName.IndexOf("_sml") != -1)
                {
                    string strHTMLImages = strName.Replace(AppDomain.CurrentDomain.BaseDirectory + "\\", "..\\");
                    strHTMLImages = strHTMLImages.Replace("\\", "/");

                    //### Generates a javascript postback for the delete method
                    String strPostBack = Page.ClientScript.GetPostBackEventReference(this, "iRemoveImages:" + iImagesCount);

                    lstImages.Add(@"<tr style='text-align: centre;'>
                                        <td style='text-align: centre;'>
                                            <a class='ImagesColorBox' href='" + strHTMLImages.Replace("_sml", "_lrg") + "'><img src='" + strHTMLImages + @"' alt='' title='' style='border: solid 5px #ffffff;' /></a><br /><br />" +
                                            "<center><div class='buttonStyle' style='padding-top: 5px;'><a href=\"javascript:" + strPostBack + "\" style='color: #ffffff;'><div class='deleteButton'></div></a></div></center>" + @"
                                        </td>
                                    </tr>");
                    lstImagesFileNames.Add(Path.GetFileName(strName).Replace("_sml", ""));
                    iImagesCount++;
                }
            }
            dlImages.DataSource = lstImages;
            dlImages.DataBind();

            Session["lstImages"] = lstImages;
            Session["lstImagesFileNames"] = lstImagesFileNames;
        }
        catch (Exception ex) {}
    }

    private string GetMainImagePath(DataList dtlTarget)
    {
        string strReturn = "";

        foreach (DataListItem dliTarget in dtlTarget.Items)
        {
            RadioButton rdbMainImage = (RadioButton)dliTarget.FindControl("rdbMainImage");
            if (rdbMainImage.Checked)
            {
                lstImagesFileNames = (List<string>)Session["lstImagesFileNames"];
                strReturn = lstImagesFileNames[dliTarget.ItemIndex];
                break;
            }
        }
        return strReturn;
    }

    private void DeleteImages(int iImagesIndex)
    {

        //### Deletes all Images related to the target Images.
        lstImages = (List<string>)Session["lstImages"];
        lstImagesFileNames = (List<string>)Session["lstImagesFileNames"];

        lstImages.RemoveAt(iImagesIndex);
        Session["lstImages"] = lstImages;

        string[] files = Directory.GetFiles(strUniqueFullPath + "\\" + lblUniquePath.Text);
        foreach (string file in files)
        {
            if (Path.GetFileName(file) == lstImagesFileNames[iImagesIndex].ToString())
            {
                //### Remove all Images
                File.Delete(file);
                File.Delete(file.Replace(Path.GetExtension(file), "_sml" + Path.GetExtension(file)));
                File.Delete(file.Replace(Path.GetExtension(file), "_med" + Path.GetExtension(file)));
                File.Delete(file.Replace(Path.GetExtension(file), "_lrg" + Path.GetExtension(file)));
                break;
            }
        }
        lstImagesFileNames.RemoveAt(iImagesIndex);
        ViewState["lstImagesFileNames"] = lstImagesFileNames;
        getList(lblUniquePath.Text);
    }


    List<string> lstDocument;
    List<string> lstDocumentFileNames;
    int iMaxDocument = 10;
    string strUniqueDocumentFullPath = AppDomain.CurrentDomain.BaseDirectory + "\\Documents";

    protected void btnDocumentUpload_Click(object sender, EventArgs e)
    {
        if (Session["lstDocument"] == null)
        {
            lstDocument = new List<string>();
            Session["lstDocument"] = lstDocument;
        }
        //### Check that they have ONLY HAVE MAX NUMBER OF Document in the datalist
        if (dlDocument.Items.Count == iMaxDocument)
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">You can only upload " + iMaxDocument.ToString() + " document per entry.</div></div>";
            getDocumentList(lblUniqueDocumentPath.Text);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "setValid", "setValidFile('" + DocumentUpload.ClientID + "',true)", true);
        }
        else
        {
            mandatoryDiv.Visible = false;

            string strUniquePath;
            if (lblUniqueDocumentPath.Text == "")
            {
                strUniquePath = GetUniqueDocumentPath();
                lblUniqueDocumentPath.Text = strUniquePath;
            }
            else
            {
                strUniquePath = lblUniqueDocumentPath.Text;
            }

            UploadDocument(strUniquePath);
            getDocumentList(strUniquePath);
        }
    }

    private string GetUniqueDocumentPath()
    {
        int iCount = 1;
        //### First we need to get the path
        while (System.IO.Directory.Exists(strUniqueDocumentFullPath + "\\Documents" + iCount) == true)
        {
            iCount++;
        }
        return "Documents" + iCount;
    }

    protected void UploadDocument(String strUniquePath)
    {
        try
        {
            if (DocumentUpload.PostedFile.ContentLength > 0 && DocumentUpload.PostedFile.ContentLength < 20971520)
            {
                //### Upload files to unique folder
                clsPortalFeatures clsPortalFeature = new clsPortalFeatures(Convert.ToInt32(Request.QueryString["iPortalFeatureID"]));

                string strUploadFileName = "";
                strUploadFileName = System.IO.Path.GetFileName(DocumentUpload.PostedFile.FileName);

                string strSaveLocation = "";
                strSaveLocation = strUniquePath + "\\" + strUploadFileName;

                if (!System.IO.Directory.Exists(strUniqueDocumentFullPath + "\\" + strUniquePath))
                    System.IO.Directory.CreateDirectory(strUniqueDocumentFullPath + "\\" + strUniquePath);

                if (File.Exists(strUniqueDocumentFullPath + "\\" + strSaveLocation))
                    File.Delete(strUniqueDocumentFullPath + "\\" + strSaveLocation);

                //if (File.Exists(strUniqueDocumentFullPath + "\\" + clsPortalFeature.strPathToDocument + "\\" + clsPortalFeature.strMasterDocument))
                    //File.Delete(strUniqueDocumentFullPath + "\\" + strSaveLocation);

                DocumentUpload.PostedFile.SaveAs(strUniqueDocumentFullPath + "\\" + strSaveLocation);

                lblFileSize.Text = DocumentUpload.PostedFile.ContentLength.ToString();

                ScriptManager.RegisterStartupScript(this, this.GetType(), "setValid", "setValidFile('" + DocumentUpload.ClientID + "',true)", true);
            }
            else
            {
                lblUploadDocError.Text = "The file should be between 0 and 20MB.";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "setValid", "setValidFile('" + DocumentUpload.ClientID + "',false)", true);
            }

            lblUploadDocError.Visible = true;
        }
        catch
        {
            Exception ex;
        }
    }

    public void getDocumentList(String strPathToFolder)
    {
        lstDocument = new List<string>();
        lstDocumentFileNames = new List<string>();

        try
        {
            String[] straFileList = Directory.GetFiles(strUniqueDocumentFullPath + "\\" + strPathToFolder);
            foreach (String strFile in straFileList)
            {
                int iImageCount = 0;
                if (strFile.Contains(".pdf"))
                {
                    //### Generates a javascript postback for the delete method
                    String strPostBack = Page.ClientScript.GetPostBackEventReference(this, "iRemoveDocument:" + iImageCount);

                    string strDocPath = strFile.Replace(AppDomain.CurrentDomain.BaseDirectory, System.Configuration.ConfigurationManager.AppSettings["WebRoot"]).Replace("\\", "/");


                    if (File.Exists(strFile))
                    {
                        string strNewFileName = Path.GetFileNameWithoutExtension(strFile);

                        strNewFileName = strNewFileName.Replace("-", " ");

                        lstDocument.Add(@"<tr style='text-align: centre;'>
                                        <td style='text-align: centre;'>
                                            <center>
                                            <a href='" + strDocPath + @"' target='_blank' style='color: #000000; text-decoration: none; text-decoration: none;'>
                                            <img src='images/icon_pdf.png' alt='' title='' style='border: none;' /><br /><br />" +
                                                    "<div>" + strNewFileName + "</div></a></center>" +
                                                        "<center><div class='buttonStyle' style='padding-top: 5px;'><a href=\"javascript:" + strPostBack + "\" style='color: #ffffff;'><div class='deleteButton'></div></a></div></center>" + @"
                                        </td>
                                    </tr>");
                        lstDocumentFileNames.Add(Path.GetFileName(strFile));
                        iImageCount++;
                    }
                }
                else if (strFile.Contains(".doc") || strFile.Contains(".docx"))
                {
                    //### Generates a javascript postback for the delete method
                    String strPostBack = Page.ClientScript.GetPostBackEventReference(this, "iRemoveDocument:" + iImageCount);

                    string strDocPath = strFile.Replace(AppDomain.CurrentDomain.BaseDirectory, System.Configuration.ConfigurationManager.AppSettings["WebRoot"]);

                    if (File.Exists(strFile))
                    {
                        lstDocument.Add(@"<tr style='text-align: centre;'>
                                        <td style='text-align: centre;'>
                                            <center><a href='" + strDocPath + @"' target='_blank' style='color: #000000; text-decoration: none; text-decoration: none;'>
                                            <img src='images/icon_word.png' alt='' title='' style='border: none;' /><br /><br />" +
                                                    "<div>" + Path.GetFileNameWithoutExtension(strFile) + "</div></a></center>" +
                                                        "<center><div class='buttonStyle' style='padding-top: 5px;'><a href=\"javascript:" + strPostBack + "\" style='color: #ffffff;'><div class='deleteButton'></div></a></div></center>" + @"
                                        </td>
                                    </tr>");
                        lstDocumentFileNames.Add(Path.GetFileName(strFile));
                        iImageCount++;
                    }
                }
                else if (strFile.Contains(".xls") || strFile.Contains(".xlsx"))
                {
                    //### Generates a javascript postback for the delete method
                    String strPostBack = Page.ClientScript.GetPostBackEventReference(this, "iRemoveDocument:" + iImageCount);

                    string strDocPath = strFile.Replace(AppDomain.CurrentDomain.BaseDirectory, System.Configuration.ConfigurationManager.AppSettings["WebRoot"]);

                    if (File.Exists(strFile))
                    {
                        lstDocument.Add(@"<tr style='text-align: centre;'>
                                        <td style='text-align: centre;'>
                                            <center><a href='" + strDocPath + @"' target='_blank' style='color: #000000; text-decoration: none; text-decoration: none;'>
                                            <img src='images/icon_excel.png' alt='' title='' style='border: none;' /><br /><br />" +
                                                    "<div>" + Path.GetFileNameWithoutExtension(strFile) + "</div></a></center>" +
                                                        "<center><div class='buttonStyle' style='padding-top: 5px;'><a href=\"javascript:" + strPostBack + "\" style='color: #ffffff;'><div class='deleteButton'></div></a></div></center>" + @"
                                        </td>
                                    </tr>");
                        lstDocumentFileNames.Add(Path.GetFileName(strFile));
                        iImageCount++;
                    }
                }
                else if (strFile.Contains(".ppt") || strFile.Contains(".pptx"))
                {
                    //### Generates a javascript postback for the delete method
                    String strPostBack = Page.ClientScript.GetPostBackEventReference(this, "iRemoveDocument:" + iImageCount);

                    string strDocPath = strFile.Replace(AppDomain.CurrentDomain.BaseDirectory, System.Configuration.ConfigurationManager.AppSettings["WebRoot"]);

                    if (File.Exists(strFile))
                    {
                        lstDocument.Add(@"<tr style='text-align: centre;'>
                                        <td style='text-align: centre;'>
                                            <center><a href='" + strDocPath + @"' target='_blank' style='color: #000000; text-decoration: none; text-decoration: none;'>
                                            <img src='images/icon_powerpoint.png' alt='' title='' style='border: none;' /><br /><br />" +
                                                    "<div>" + Path.GetFileNameWithoutExtension(strFile) + "</div></a></center>" +
                                                        "<center><div class='buttonStyle' style='padding-top: 5px;'><a href=\"javascript:" + strPostBack + "\" style='color: #ffffff;'><div class='deleteButton'></div></a></div></center>" + @"
                                        </td>
                                    </tr>");
                        lstDocumentFileNames.Add(Path.GetFileName(strFile));
                        iImageCount++;
                    }
                }
                else if (strFile.Contains(".zip") || strFile.Contains(".zipx"))
                {
                    //### Generates a javascript postback for the delete method
                    String strPostBack = Page.ClientScript.GetPostBackEventReference(this, "iRemoveDocument:" + iImageCount);

                    string strDocPath = strFile.Replace(AppDomain.CurrentDomain.BaseDirectory, System.Configuration.ConfigurationManager.AppSettings["WebRoot"]);

                    if (File.Exists(strFile))
                    {
                        lstDocument.Add(@"<tr style='text-align: centre;'>
                                        <td style='text-align: centre;'>
                                            <center><a href='" + strDocPath + @"' target='_blank' style='color: #000000; text-decoration: none; text-decoration: none;'>
                                            <img src='../images/imgZipIcon.png' alt='' title='' style='border: none;' /><br /><br />" +
                                                    "<div>" + Path.GetFileNameWithoutExtension(strFile) + "</div></a></center>" +
                                                        "<center><div class='buttonStyle' style='padding-top: 5px;'><a href=\"javascript:" + strPostBack + "\" style='color: #ffffff;'><div class='deleteButton'></div></a></div></center>" + @"
                                        </td>
                                    </tr>");
                        lstDocumentFileNames.Add(Path.GetFileName(strFile));
                        iImageCount++;
                    }
                }
            }

            dlDocument.DataSource = lstDocument;
            dlDocument.DataBind();

            Session["lstDocument"] = lstDocument;
            Session["lstDocumentFileNames"] = lstDocumentFileNames;
        }
        catch (Exception ex)
        { }
    }

    private string GetMainDocumentPath(DataList dtlTarget)
    {
        string strReturn = "";

        foreach (DataListItem dliTarget in dtlTarget.Items)
        {
            RadioButton rdbMainDocument = (RadioButton)dliTarget.FindControl("rdbMainDocument");
            if (rdbMainDocument.Checked)
            {
                lstDocumentFileNames = (List<string>)Session["lstDocumentFileNames"];
                strReturn = lstDocumentFileNames[dliTarget.ItemIndex];
                break;
            }
        }
        return strReturn;
    }

    private void DeleteDocument(int iDocumentIndex)
    {
        //### Deletes all Document related to the target Document.
        lstDocument = (List<string>)Session["lstDocument"];
        lstDocumentFileNames = (List<string>)Session["lstDocumentFileNames"];

        lstDocument.RemoveAt(iDocumentIndex);
        Session["lstDocument"] = lstDocument;

        string[] files = Directory.GetFiles(strUniqueDocumentFullPath + "\\" + lblUniqueDocumentPath.Text);

        foreach (string file in files)
        {
            if (Path.GetFileName(file) == lstDocumentFileNames[iDocumentIndex].ToString())
            {
                //### Remove all Document
                File.Delete(file);
                break;
            }
        }

        lstDocumentFileNames.RemoveAt(iDocumentIndex);
        ViewState["lstDocumentFileNames"] = lstDocumentFileNames;
        getDocumentList(lblUniqueDocumentPath.Text);
    }

    #endregion
}