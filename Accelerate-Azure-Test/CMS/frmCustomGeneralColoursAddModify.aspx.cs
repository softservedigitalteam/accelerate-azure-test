using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;

public partial class CMS_frmCustomGeneralColoursAddModify : System.Web.UI.Page
{
    clsUsers clsUsers;
    clsCustomGeneralColours clsCustomGeneralColours;
    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

        if (!IsPostBack)
        {
             //popCustomGeneralColour();

            //### If the iCustomGeneralColourID is passed through then we want to instantiate the object with that iCustomGeneralColourID
            if ((Request.QueryString["iCustomGeneralColourID"] != "") && (Request.QueryString["iCustomGeneralColourID"] != null))
            {
                clsCustomGeneralColours = new clsCustomGeneralColours(Convert.ToInt32(Request.QueryString["iCustomGeneralColourID"]));

                //### Populate the form
                popFormData();
            }
            else
            {
                clsCustomGeneralColours = new clsCustomGeneralColours();
            }
            Session["clsCustomGeneralColours"] = clsCustomGeneralColours;
        }
        else
        {
            clsCustomGeneralColours = (clsCustomGeneralColours)Session["clsCustomGeneralColours"];
        }
    }

    protected void lnkbtnBack_Click(object sender, EventArgs e)
    {
        //### Go back to previous page
        Response.Redirect("frmCustomGeneralColoursView.aspx");
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        //### Validate registration process
        bool bCanSave = true;

       //bCanSave = clsValidation.IsNullOrEmpty(lstCustomGeneralColour, bCanSave);
       bCanSave = clsValidation.IsNullOrEmpty(txtTitle, bCanSave);

        if (bCanSave == true)
        {
            mandatoryDiv.Visible = false;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceHappy.png\" alt='' title=''/><div class=\"validationMessage\">CustomGeneralColour added successfully</div></div>";
            SaveData();
        }
        else
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">Please fill out all mandatory fields - CustomGeneralColour not added</div></div>";
        }
    }

    protected void lnkbtnClear_Click(object sender, EventArgs e)
    {
        mandatoryDiv.Visible = false;

        //lstCustomGeneralColour.SelectedValue = "0";
        //clsValidation.SetValid(lstCustomGeneralColour);
        txtTitle.Text = "";
        clsValidation.SetValid(txtTitle);
        //txtLink.Text = "";
        //clsValidation.SetValid(txtLink);
        txtProfileBackgroundColour.Text = "";
        clsValidation.SetValid(txtProfileBackgroundColour);
        txtProfileColourText.Text = "";
        clsValidation.SetValid(txtProfileColourText);
        txtRankBackgroundColour.Text = "";
        clsValidation.SetValid(txtRankBackgroundColour);
        txtRankTextColour.Text = "";
        clsValidation.SetValid(txtRankTextColour);
        txtProgressBarSectionBackgroundColour.Text = "";
        clsValidation.SetValid(txtProgressBarSectionBackgroundColour);
        txtProgressBarFillerColour.Text = "";
        clsValidation.SetValid(txtProgressBarFillerColour);
        txtInformationBarTopBackgroundColour.Text = "";
        clsValidation.SetValid(txtInformationBarTopBackgroundColour);
        txtInformationBarTopTextColour.Text = "";
        clsValidation.SetValid(txtInformationBarTopTextColour);
        txtInformationBarBackgroundColour.Text = "";
        clsValidation.SetValid(txtInformationBarBackgroundColour);
        txtInformationBarTextColour.Text = "";
        clsValidation.SetValid(txtInformationBarTextColour);
        txtTipsBackgroundColour.Text = "";
        clsValidation.SetValid(txtTipsBackgroundColour);
        txtTipsTextColour.Text = "";
        clsValidation.SetValid(txtTipsTextColour);
        txtIntroductionBackgroundColour.Text = "";
        clsValidation.SetValid(txtIntroductionBackgroundColour);
        txtIntroductionTextColour.Text = "";
        clsValidation.SetValid(txtIntroductionTextColour);
        txtMissionInfoBackgroundColour.Text = "";
        clsValidation.SetValid(txtMissionInfoBackgroundColour);
        txtMissionInfoTitleTextColour.Text = "";
        clsValidation.SetValid(txtMissionInfoTitleTextColour);
        txtMissionInfoTextColour.Text = "";
        clsValidation.SetValid(txtMissionInfoTextColour);
        txtMissionInfoPointsColour.Text = "";
        clsValidation.SetValid(txtMissionInfoPointsColour);
        txtGeneralInfoBoxBackgroundColour.Text = "";
        clsValidation.SetValid(txtGeneralInfoBoxBackgroundColour);
        txtGeneralInfoBoxTextColour.Text = "";
        clsValidation.SetValid(txtGeneralInfoBoxTextColour);
        txtGeneralInfoBoxHeaderBackgroundColour.Text = "";
        clsValidation.SetValid(txtGeneralInfoBoxHeaderBackgroundColour);
        txtGeneralInfoBoxHeaderTextColour.Text = "";
        clsValidation.SetValid(txtGeneralInfoBoxHeaderTextColour);
        txtKnowledgeBaseBlockColour1.Text = "";
        clsValidation.SetValid(txtKnowledgeBaseBlockColour1);
        txtKnowledgeBaseBlockColour2.Text = "";
        clsValidation.SetValid(txtKnowledgeBaseBlockColour2);
        txtKnowledgeBaseBlockColour3.Text = "";
        clsValidation.SetValid(txtKnowledgeBaseBlockColour3);
        txtKnowledgeBaseBlockTextColour1.Text = "";
        clsValidation.SetValid(txtKnowledgeBaseBlockTextColour1);
        txtKnowledgeBaseBlockTextColour2.Text = "";
        clsValidation.SetValid(txtKnowledgeBaseBlockTextColour2);
        txtKnowledgeBaseBlockTextColour3.Text = "";
        clsValidation.SetValid(txtKnowledgeBaseBlockTextColour3);
        txtKnowledgeBaseBlockLinkColour.Text = "";
        clsValidation.SetValid(txtKnowledgeBaseBlockLinkColour);
        txtMandatoryeLearningLinkColour.Text = "";
        clsValidation.SetValid(txtMandatoryeLearningLinkColour);
        txtHomePointsBackgroundColour.Text = "";
        clsValidation.SetValid(txtHomePointsBackgroundColour);
        txtHomePointsTextColour.Text = "";
        clsValidation.SetValid(txtHomePointsTextColour);
        txtShowcaseBackgroundColour.Text = "";
        clsValidation.SetValid(txtShowcaseBackgroundColour);
        txtShowcaseTextColour.Text = "";
        clsValidation.SetValid(txtShowcaseTextColour);
        txtPhase1BackgroundColour.Text = "";
        clsValidation.SetValid(txtPhase1BackgroundColour);
        txtPhase1TextColour.Text = "";
        clsValidation.SetValid(txtPhase1TextColour);
        txtPhase2BackgroundColour.Text = "";
        clsValidation.SetValid(txtPhase2BackgroundColour);
        txtPhase2TextColour.Text = "";
        clsValidation.SetValid(txtPhase2TextColour);
        txtPhase3BackgroundColour.Text = "";
        clsValidation.SetValid(txtPhase3BackgroundColour);
        txtPhase3TextColour.Text = "";
        clsValidation.SetValid(txtPhase3TextColour);
        txtContentBoxBackgroundColour.Text = "";
        clsValidation.SetValid(txtContentBoxBackgroundColour);
        txtContentBoxTextColour.Text = "";
        clsValidation.SetValid(txtContentBoxTextColour);
        txtTopAchieversTextColour.Text = "";
        clsValidation.SetValid(txtTopAchieversTextColour);
        txtContentBoxHeaderBackgroundColour.Text = "";
        clsValidation.SetValid(txtContentBoxHeaderBackgroundColour);
        txtContentBoxHeaderTextColour.Text = "";
        clsValidation.SetValid(txtContentBoxHeaderTextColour);
        txtMissionsCompleteTextColour.Text = "";
        clsValidation.SetValid(txtMissionsCompleteTextColour);
        txtButtonBackgroundColour.Text = "";
        clsValidation.SetValid(txtButtonBackgroundColour);
        txtButtonTextColour.Text = "";
        clsValidation.SetValid(txtButtonTextColour);
        txtNavigationBarBackgroundColour.Text = "";
        clsValidation.SetValid(txtNavigationBarBackgroundColour);
        txtNavigationBarButtonsColour.Text = "";
        clsValidation.SetValid(txtNavigationBarButtonsColour);
        txtProfileTextBoxBackgroundColour.Text = "";
        clsValidation.SetValid(txtProfileTextBoxBackgroundColour);
        txtProfileTextBoxTextColour.Text = "";
        clsValidation.SetValid(txtProfileTextBoxTextColour);
        txtMissionTextBoxBackgroundColour.Text = "";
        clsValidation.SetValid(txtMissionTextBoxBackgroundColour);
        txtMissionTextBoxTextColour.Text = "";
        clsValidation.SetValid(txtMissionTextBoxTextColour);
    }

    #endregion 

    #region POPULATE DATA METHODS

    private void popFormData()
    {
         //lstCustomGeneralColour.SelectedValue = clsCustomGeneralColours.iCustomGeneralColourID.ToString();
         txtTitle.Text = clsCustomGeneralColours.strTitle;
         //txtLink.Text = clsCustomGeneralColours.strLink;
         txtProfileBackgroundColour.Text = clsCustomGeneralColours.strProfileBackgroundColour;
         txtProfileColourText.Text = clsCustomGeneralColours.strProfileColourText;
         txtRankBackgroundColour.Text = clsCustomGeneralColours.strRankBackgroundColour;
         txtRankTextColour.Text = clsCustomGeneralColours.strRankTextColour;
         txtProgressBarSectionBackgroundColour.Text = clsCustomGeneralColours.strProgressBarSectionBackgroundColour;
         txtProgressBarFillerColour.Text = clsCustomGeneralColours.strProgressBarFillerColour;
         txtInformationBarTopBackgroundColour.Text = clsCustomGeneralColours.strInformationBarTopBackgroundColour;
         txtInformationBarTopTextColour.Text = clsCustomGeneralColours.strInformationBarTopTextColour;
         txtInformationBarBackgroundColour.Text = clsCustomGeneralColours.strInformationBarBackgroundColour;
         txtInformationBarTextColour.Text = clsCustomGeneralColours.strInformationBarTextColour;
         txtTipsBackgroundColour.Text = clsCustomGeneralColours.strTipsBackgroundColour;
         txtTipsTextColour.Text = clsCustomGeneralColours.strTipsTextColour;
         txtIntroductionBackgroundColour.Text = clsCustomGeneralColours.strIntroductionBackgroundColour;
         txtIntroductionTextColour.Text = clsCustomGeneralColours.strIntroductionTextColour;
         txtMissionInfoBackgroundColour.Text = clsCustomGeneralColours.strMissionInfoBackgroundColour;
         txtMissionInfoTitleTextColour.Text = clsCustomGeneralColours.strMissionInfoTitleTextColour;
         txtMissionInfoTextColour.Text = clsCustomGeneralColours.strMissionInfoTextColour;
         txtMissionInfoPointsColour.Text = clsCustomGeneralColours.strMissionInfoPointsColour;
         txtGeneralInfoBoxBackgroundColour.Text = clsCustomGeneralColours.strGeneralInfoBoxBackgroundColour;
         txtGeneralInfoBoxTextColour.Text = clsCustomGeneralColours.strGeneralInfoBoxTextColour;
         txtGeneralInfoBoxHeaderBackgroundColour.Text = clsCustomGeneralColours.strGeneralInfoBoxHeaderBackgroundColour;
         txtGeneralInfoBoxHeaderTextColour.Text = clsCustomGeneralColours.strGeneralInfoBoxHeaderTextColour;
         txtKnowledgeBaseBlockColour1.Text = clsCustomGeneralColours.strKnowledgeBaseBlockColour1;
         txtKnowledgeBaseBlockColour2.Text = clsCustomGeneralColours.strKnowledgeBaseBlockColour2;
         txtKnowledgeBaseBlockColour3.Text = clsCustomGeneralColours.strKnowledgeBaseBlockColour3;
        txtKnowledgeBaseBlockTextColour1.Text = clsCustomGeneralColours.strKnowledgeBaseBlockTextColour1;
        txtKnowledgeBaseBlockTextColour2.Text = clsCustomGeneralColours.strKnowledgeBaseBlockTextColour2;
        txtKnowledgeBaseBlockTextColour3.Text = clsCustomGeneralColours.strKnowledgeBaseBlockTextColour3;
        txtKnowledgeBaseBlockLinkColour.Text = clsCustomGeneralColours.strKnowledgeBaseBlockLinkColour;
         txtMandatoryeLearningLinkColour.Text = clsCustomGeneralColours.strMandatoryeLearningLinkColour;
         txtHomePointsBackgroundColour.Text = clsCustomGeneralColours.strHomePointsBackgroundColour;
         txtHomePointsTextColour.Text = clsCustomGeneralColours.strHomePointsTextColour;
         txtShowcaseBackgroundColour.Text = clsCustomGeneralColours.strShowcaseBackgroundColour;
         txtShowcaseTextColour.Text = clsCustomGeneralColours.strShowcaseTextColour;
         txtPhase1BackgroundColour.Text = clsCustomGeneralColours.strPhase1BackgroundColour;
         txtPhase1TextColour.Text = clsCustomGeneralColours.strPhase1TextColour;
         txtPhase2BackgroundColour.Text = clsCustomGeneralColours.strPhase2BackgroundColour;
         txtPhase2TextColour.Text = clsCustomGeneralColours.strPhase2TextColour;
         txtPhase3BackgroundColour.Text = clsCustomGeneralColours.strPhase3BackgroundColour;
         txtPhase3TextColour.Text = clsCustomGeneralColours.strPhase3TextColour;
         txtContentBoxBackgroundColour.Text = clsCustomGeneralColours.strContentBoxBackgroundColour;
         txtContentBoxTextColour.Text = clsCustomGeneralColours.strContentBoxTextColour;
         txtTopAchieversTextColour.Text = clsCustomGeneralColours.strTopAchieversTextColour;
         txtContentBoxHeaderBackgroundColour.Text = clsCustomGeneralColours.strContentBoxHeaderBackgroundColour;
         txtContentBoxHeaderTextColour.Text = clsCustomGeneralColours.strContentBoxHeaderTextColour;
         txtMissionsCompleteTextColour.Text = clsCustomGeneralColours.strMissionsCompleteTextColour;
         txtButtonBackgroundColour.Text = clsCustomGeneralColours.strButtonBackgroundColour;
        txtButtonTextColour.Text = clsCustomGeneralColours.strButtonTextColour;
        txtNavigationBarBackgroundColour.Text = clsCustomGeneralColours.strNavigationBarBackgroundColour;
         txtNavigationBarButtonsColour.Text = clsCustomGeneralColours.strNavigationBarButtonsColour;
        txtProfileTextBoxBackgroundColour.Text = clsCustomGeneralColours.strProfileTextBoxBackgroundColour;
        txtProfileTextBoxTextColour.Text = clsCustomGeneralColours.strProfileTextBoxTextColour;
        txtMissionTextBoxBackgroundColour.Text = clsCustomGeneralColours.strMissionTextBoxBackgroundColour;
        txtMissionTextBoxTextColour.Text = clsCustomGeneralColours.strMissionTextBoxTextColour;
    }
    
    //private void popCustomGeneralColour()
    //{
    //     DataTable dtCustomGeneralColoursList = new DataTable();
    //     lstCustomGeneralColour.DataSource = clsCustomGeneralColours.GetCustomGeneralColoursList();

    //     //### Populates the drop down list with PK and TITLE;
    //     lstCustomGeneralColour.DataValueField = "iCustomGeneralColourID";
    //     lstCustomGeneralColour.DataTextField = "strTitle";

    //     //### Bind the data to the list;
    //     lstCustomGeneralColour.DataBind();

    //     //### Add default select option;
    //     lstCustomGeneralColour.Items.Insert(0, new ListItem("--Not Selected--", "0"));
    //}
    
    #endregion

    #region SAVE DATA METHODS

    private void SaveData()
    {
        //### Add / Update
        //clsCustomGeneralColours.iCustomGeneralColourID = Convert.ToInt32(lstCustomGeneralColour.SelectedValue.ToString());
        clsCustomGeneralColours.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsCustomGeneralColours.iAddedBy = clsUsers.iUserID;
        clsCustomGeneralColours.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsCustomGeneralColours.iEditedBy = clsUsers.iUserID;
        clsCustomGeneralColours.strTitle = txtTitle.Text;
        //clsCustomGeneralColours.strLink = txtLink.Text;
        clsCustomGeneralColours.strProfileBackgroundColour = txtProfileBackgroundColour.Text;
        clsCustomGeneralColours.strProfileColourText = txtProfileColourText.Text;
        clsCustomGeneralColours.strRankBackgroundColour = txtRankBackgroundColour.Text;
        clsCustomGeneralColours.strRankTextColour = txtRankTextColour.Text;
        clsCustomGeneralColours.strProgressBarSectionBackgroundColour = txtProgressBarSectionBackgroundColour.Text;
        clsCustomGeneralColours.strProgressBarFillerColour = txtProgressBarFillerColour.Text;
        clsCustomGeneralColours.strInformationBarTopBackgroundColour = txtInformationBarTopBackgroundColour.Text;
        clsCustomGeneralColours.strInformationBarTopTextColour = txtInformationBarTopTextColour.Text;
        clsCustomGeneralColours.strInformationBarBackgroundColour = txtInformationBarBackgroundColour.Text;
        clsCustomGeneralColours.strInformationBarTextColour = txtInformationBarTextColour.Text;
        clsCustomGeneralColours.strTipsBackgroundColour = txtTipsBackgroundColour.Text;
        clsCustomGeneralColours.strTipsTextColour = txtTipsTextColour.Text;
        clsCustomGeneralColours.strIntroductionBackgroundColour = txtIntroductionBackgroundColour.Text;
        clsCustomGeneralColours.strIntroductionTextColour = txtIntroductionTextColour.Text;
        clsCustomGeneralColours.strMissionInfoBackgroundColour = txtMissionInfoBackgroundColour.Text;
        clsCustomGeneralColours.strMissionInfoTitleTextColour = txtMissionInfoTitleTextColour.Text;
        clsCustomGeneralColours.strMissionInfoTextColour = txtMissionInfoTextColour.Text;
        clsCustomGeneralColours.strMissionInfoPointsColour = txtMissionInfoPointsColour.Text;
        clsCustomGeneralColours.strGeneralInfoBoxBackgroundColour = txtGeneralInfoBoxBackgroundColour.Text;
        clsCustomGeneralColours.strGeneralInfoBoxTextColour = txtGeneralInfoBoxTextColour.Text;
        clsCustomGeneralColours.strGeneralInfoBoxHeaderBackgroundColour = txtGeneralInfoBoxHeaderBackgroundColour.Text;
        clsCustomGeneralColours.strGeneralInfoBoxHeaderTextColour = txtGeneralInfoBoxHeaderTextColour.Text;
        clsCustomGeneralColours.strKnowledgeBaseBlockColour1 = txtKnowledgeBaseBlockColour1.Text;
        clsCustomGeneralColours.strKnowledgeBaseBlockColour2 = txtKnowledgeBaseBlockColour2.Text;
        clsCustomGeneralColours.strKnowledgeBaseBlockColour3 = txtKnowledgeBaseBlockColour3.Text;
        clsCustomGeneralColours.strKnowledgeBaseBlockTextColour1 = txtKnowledgeBaseBlockTextColour1.Text;
        clsCustomGeneralColours.strKnowledgeBaseBlockTextColour2 = txtKnowledgeBaseBlockTextColour2.Text;
        clsCustomGeneralColours.strKnowledgeBaseBlockTextColour3 = txtKnowledgeBaseBlockTextColour3.Text;
        clsCustomGeneralColours.strKnowledgeBaseBlockLinkColour = txtKnowledgeBaseBlockLinkColour.Text;
        clsCustomGeneralColours.strMandatoryeLearningLinkColour = txtMandatoryeLearningLinkColour.Text;
        clsCustomGeneralColours.strHomePointsBackgroundColour = txtHomePointsBackgroundColour.Text;
        clsCustomGeneralColours.strHomePointsTextColour = txtHomePointsTextColour.Text;
        clsCustomGeneralColours.strShowcaseBackgroundColour = txtShowcaseBackgroundColour.Text;
        clsCustomGeneralColours.strShowcaseTextColour = txtShowcaseTextColour.Text;
        clsCustomGeneralColours.strPhase1BackgroundColour = txtPhase1BackgroundColour.Text;
        clsCustomGeneralColours.strPhase1TextColour = txtPhase1TextColour.Text;
        clsCustomGeneralColours.strPhase2BackgroundColour = txtPhase2BackgroundColour.Text;
        clsCustomGeneralColours.strPhase2TextColour = txtPhase2TextColour.Text;
        clsCustomGeneralColours.strPhase3BackgroundColour = txtPhase3BackgroundColour.Text;
        clsCustomGeneralColours.strPhase3TextColour = txtPhase3TextColour.Text;
        clsCustomGeneralColours.strContentBoxBackgroundColour = txtContentBoxBackgroundColour.Text;
        clsCustomGeneralColours.strContentBoxTextColour = txtContentBoxTextColour.Text;
        clsCustomGeneralColours.strTopAchieversTextColour = txtTopAchieversTextColour.Text;
        clsCustomGeneralColours.strContentBoxHeaderBackgroundColour = txtContentBoxHeaderBackgroundColour.Text;
        clsCustomGeneralColours.strContentBoxHeaderTextColour = txtContentBoxHeaderTextColour.Text;
        clsCustomGeneralColours.strMissionsCompleteTextColour = txtMissionsCompleteTextColour.Text;
        clsCustomGeneralColours.strButtonBackgroundColour = txtButtonBackgroundColour.Text;
        clsCustomGeneralColours.strButtonTextColour = txtButtonTextColour.Text;
        clsCustomGeneralColours.strNavigationBarBackgroundColour = txtNavigationBarBackgroundColour.Text;
        clsCustomGeneralColours.strNavigationBarButtonsColour = txtNavigationBarButtonsColour.Text;
        clsCustomGeneralColours.strProfileTextBoxBackgroundColour = txtProfileTextBoxBackgroundColour.Text;
        clsCustomGeneralColours.strProfileTextBoxTextColour = txtProfileTextBoxTextColour.Text;
        clsCustomGeneralColours.strMissionTextBoxBackgroundColour = txtMissionTextBoxBackgroundColour.Text;
        clsCustomGeneralColours.strMissionTextBoxTextColour = txtMissionTextBoxTextColour.Text;

        clsCustomGeneralColours.strLink = createCSSFileLink();

        clsCustomGeneralColours.Update();

        Session["dtCustomGeneralColoursList"] = null;

        //### Go back to view page
        Response.Redirect("frmCustomGeneralColoursView.aspx");
    }

    private string createCSSFileLink()
    {
        string strFileDirectory = AppDomain.CurrentDomain.BaseDirectory + "\\CustomColourSchemes";

        if (!System.IO.Directory.Exists(strFileDirectory))
            System.IO.Directory.CreateDirectory(strFileDirectory);

        string strFullFilePath = strFileDirectory + "\\" + GetUniqueDocumentPath(strFileDirectory) + "\\" + txtTitle.Text + ".css";

        if (File.Exists(strFullFilePath))
            File.Delete(strFullFilePath);

        string strDirectoryForStyle = strFullFilePath.Replace(strFileDirectory + "\\" + GetUniqueDocumentPath(strFileDirectory) + "\\" + txtTitle.Text + ".css", strFileDirectory + "\\" + GetUniqueDocumentPath(strFileDirectory));

        if (!System.IO.Directory.Exists(strDirectoryForStyle))
            System.IO.Directory.CreateDirectory(strDirectoryForStyle);

        using (FileStream fstream = new FileStream(strFullFilePath, FileMode.Create))
        {
            using (StreamWriter sWriter = new StreamWriter(fstream, new ASCIIEncoding()))
            {
                sWriter.Write(createCSSFile());
            }
        }

        /*DocumentUpload.PostedFile.SaveAs(strUniqueDocumentFullPath + "\\" + strSaveLocation);*/
        strFullFilePath = strFullFilePath.Replace(AppDomain.CurrentDomain.BaseDirectory + "\\", "");

        return strFullFilePath.Replace(AppDomain.CurrentDomain.BaseDirectory, "");
    }

    private string createCSSFile()
    {
        StringBuilder strbCssFileContent = new StringBuilder();

        strbCssFileContent.AppendLine(".well.ProfileSection{background: " + txtProfileBackgroundColour.Text + " !important;color: " + txtProfileColourText.Text + " !important;}");
        strbCssFileContent.AppendLine(".rankStatus{background: " + txtRankBackgroundColour.Text + ";color: " + txtRankTextColour.Text + ";}");
        strbCssFileContent.AppendLine(".progressBarSection{background: " + txtProgressBarSectionBackgroundColour.Text + ";}");
        strbCssFileContent.AppendLine(".progressBar{background: " + txtProgressBarFillerColour.Text + ";}");
        strbCssFileContent.AppendLine(".top-link{background: " + txtInformationBarTopBackgroundColour.Text + ";color: " + txtInformationBarTopTextColour.Text + ";}");
        strbCssFileContent.AppendLine(".top-link-2{background: " + txtInformationBarBackgroundColour.Text + ";color: " + txtInformationBarTextColour.Text + ";}");
        strbCssFileContent.AppendLine(".well-small.Blue2{background: " + txtTipsBackgroundColour.Text + " !important;color: " + txtTipsTextColour.Text + " !important;}");
        strbCssFileContent.AppendLine(".infobox.well-small{background: " + txtIntroductionBackgroundColour.Text + " !important;color: " + txtIntroductionTextColour.Text + " !important;}");
        strbCssFileContent.AppendLine("#TopDiv.well-short{background: " + txtMissionInfoBackgroundColour.Text + " !important;}");
        strbCssFileContent.AppendLine(".missionInformation{color: " + txtMissionInfoTitleTextColour.Text + " !important;}");
        strbCssFileContent.AppendLine(".span9{color: " + txtMissionInfoTextColour.Text + " !important;}");
        strbCssFileContent.AppendLine("#missionPoints.span2 span{color: " + txtMissionInfoPointsColour.Text + " !important;}");
        strbCssFileContent.AppendLine(".well-short{background: " + txtGeneralInfoBoxBackgroundColour.Text + " !important;color: " + txtGeneralInfoBoxTextColour.Text + " !important;}");
        strbCssFileContent.AppendLine(".top-bar-alternative{background: " + txtGeneralInfoBoxHeaderBackgroundColour.Text + ";border-color: " + txtGeneralInfoBoxHeaderBackgroundColour.Text + ";}");
        strbCssFileContent.AppendLine(".Colour1.KnowledgeBaseBlock{background: " + txtKnowledgeBaseBlockColour1.Text + ";color: " + txtKnowledgeBaseBlockTextColour1.Text + ";}");
        strbCssFileContent.AppendLine(".Colour2.KnowledgeBaseBlock{background: " + txtKnowledgeBaseBlockColour2.Text + ";color: " + txtKnowledgeBaseBlockTextColour2.Text + ";}");
        strbCssFileContent.AppendLine(".Colour3.KnowledgeBaseBlock{background: " + txtKnowledgeBaseBlockColour3.Text + ";color: " + txtKnowledgeBaseBlockTextColour3.Text + ";}");
        strbCssFileContent.AppendLine("a{color: " + txtKnowledgeBaseBlockLinkColour.Text + ";}");
        strbCssFileContent.AppendLine(".anchorAlt{color: " + txtMandatoryeLearningLinkColour.Text + ";}");
        strbCssFileContent.AppendLine(".span2.PointsSection{background: " + txtHomePointsBackgroundColour.Text + ";color: " + txtHomePointsTextColour.Text + ";}");
        strbCssFileContent.AppendLine(".showcaseSection.Black{background: " + txtShowcaseBackgroundColour.Text + ";color: " + txtShowcaseTextColour.Text + ";}");
        strbCssFileContent.AppendLine(".showcaseSection.Blue1{background: " + txtPhase1BackgroundColour.Text + " !important;color: " + txtPhase1TextColour.Text + " !important;}");
        strbCssFileContent.AppendLine(".showcaseSection.Blue2{background: " + txtPhase2BackgroundColour.Text + " !important;color: " + txtPhase2TextColour.Text + " !important;}");
        strbCssFileContent.AppendLine(".showcaseSection.Blue3{background: " + txtPhase3BackgroundColour.Text + " !important;color: " + txtPhase3TextColour.Text + " !important;}");
        strbCssFileContent.AppendLine(".well{background: " + txtContentBoxBackgroundColour.Text + " !important;color: " + txtContentBoxTextColour.Text + " !important;}");
        strbCssFileContent.AppendLine(".AchieverSection{color: " + txtTopAchieversTextColour.Text + " !important;}");
        strbCssFileContent.AppendLine(".top-bar{background: " + txtContentBoxHeaderBackgroundColour.Text + ";border-color: " + txtContentBoxHeaderBackgroundColour.Text + ";}");
        strbCssFileContent.AppendLine(".TAAF{background: " + txtContentBoxHeaderBackgroundColour.Text + ";border-color: " + txtContentBoxHeaderBackgroundColour.Text + ";}");
        strbCssFileContent.AppendLine("h3{color: " + txtContentBoxHeaderTextColour.Text + " !important;text-shadow: 0 1px 0 " + txtContentBoxHeaderBackgroundColour.Text + " !important;}");
        strbCssFileContent.AppendLine(".missionHeading.black{color: " + txtMissionsCompleteTextColour.Text + " !important;}");
        strbCssFileContent.AppendLine(".Green3.btn{background: " + txtButtonBackgroundColour.Text + ";color: " + txtButtonTextColour.Text + ";}");
        strbCssFileContent.AppendLine("#MenuBar.navbar-inner{background: " + txtNavigationBarBackgroundColour.Text + " !important;}");
        strbCssFileContent.AppendLine(".nav{background: " + txtNavigationBarButtonsColour.Text + " !important;}");
        strbCssFileContent.AppendLine(".textboxAlt{background: " + txtProfileTextBoxBackgroundColour.Text + " !important;color: " + txtProfileTextBoxTextColour.Text + " !important;}");
        strbCssFileContent.AppendLine(".textbox{background: " + txtMissionTextBoxBackgroundColour.Text + " !important;color: " + txtMissionTextBoxTextColour.Text + " !important;}");

        //strbCssFileContent.AppendLine(".well.ProfileSection{background: " + txtProfileBackgroundColour.Text + ";color: " + txtProfileColourText.Text + ";}");

        return strbCssFileContent.ToString();
    }

    private string GetUniqueDocumentPath(string strFileDirectory)
    {
        int iCount = 1;
        //### First we need to get the path
        while (System.IO.Directory.Exists(strFileDirectory + "\\Scheme" + iCount) == true)
        {
            iCount++;
        }
        return "Scheme" + iCount;

    }
    #endregion
}