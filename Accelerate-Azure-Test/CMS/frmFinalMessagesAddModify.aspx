<%@ Page Title="FinalMessages" Language="C#" MasterPageFile="~/CMS/CMS.master" AutoEventWireup="true" Inherits="CMS_frmFinalMessagesAddModify" Codebehind="frmFinalMessagesAddModify.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <asp:UpdatePanel ID="updMain" runat="server">
    <ContentTemplate>

    <div id="mandatoryDiv" class="mandatoryInvalidDiv" runat="server" visible="false">
        <asp:Label ID="lblValidationMessage" runat="server"></asp:Label>
    </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Header Text:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtHeaderText" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,35)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Paragraph Text:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtParagraphText" runat="server" CssClass="roundedCornerTextBoxMultiLine8" onKeyUp="return SetMaxLength(this,1250)" TextMode="MultiLine" Rows="8" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Images:</div>
         <div class="fieldDiv">
             <asp:FileUpload ID="FileUpload" runat="server" CssClass="roundedCornerTextBoxUpload" style="float: left;" Width="350px"  onblur="setValid(this);"/>
             <div style="float:left;"><asp:LinkButton ID="btnUpload" runat="server" CssClass="uploadButton" onclick="btnUpload_Click" style="margin-left:5px;" /></div>
             <asp:Label ID="lblUniquePath" runat="Server" Visible="false"></asp:Label><br style="clear:both"/>
             <asp:UpdatePanel ID="udpImages" runat="server" ChildrenAsTriggers="true">
                 <ContentTemplate>
                     <asp:DataList ID="dlImages" runat="server" RepeatColumns="3" ItemStyle-CssClass="listImages">
                         <ItemTemplate>
                             <table cellspacing="5">
                                 <%# Container.DataItem %>
                             </table>
                             <div align="center" style="padding-top: 5px;">
                                 <asp:RadioButton ID="rdbMainImage" runat="server" GroupName="MainImages" Text="Main Image" onclick="javascript:CheckOnOff(this.id,'dlImages');" /><br />
                             </div>
                         </ItemTemplate>
                     </asp:DataList>
                 </ContentTemplate>
             </asp:UpdatePanel><br />
             <b><asp:Label ID="lblUploadError" runat="server"></asp:Label></b>
         </div>
         <br class="clearingSpacer" />
     </div>


    
    <div class="Line"></div>
    <div class="buttonsRightDiv">
        <asp:LinkButton ID="lnkbtnBack" runat="server" CssClass="backButton" onclick="lnkbtnBack_Click" OnClientClick='history.go(-1);return false;' />
        <asp:LinkButton ID="lnkbtnSave" runat="server" CssClass="saveButton" onclick="lnkbtnSave_Click" />
        <asp:LinkButton ID="lnkbtnClear" runat="server" CssClass="clearButton" onclick="lnkbtnClear_Click" />
    </div>

         </ContentTemplate>
         <Triggers>
             <asp:PostBackTrigger ControlID="btnUpload" />
         </Triggers>
     </asp:UpdatePanel>
</asp:Content>
