
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text;

/// <summary>
/// Summary description for clsWelcomeMessagesView
/// </summary>
public partial class CMS_frmWelcomeMessagesView : System.Web.UI.Page
{
    #region CLASSES AND VARIABLES

    clsUsers clsUsers;
    DataTable dtWelcomeMessagesList;

    List<clsWelcomeMessages> glstWelcomeMessages;

    #endregion

    #region EVENT MODULES

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

dgrGrid.ItemCreated += new DataGridItemEventHandler(clsCommonFunctions.dgrGrid_PagerPopulate);

        if (!Page.IsPostBack)
        {
            Session["dtWelcomeMessagesList"] = null;
            PopulateFormData();
        }
        else
        {
            if (Session["glstWelcomeMessages"] == null)
            {
                glstWelcomeMessages = new List<clsWelcomeMessages>();
                Session["glstWelcomeMessages"] = glstWelcomeMessages;
            }
            else
                glstWelcomeMessages = (List<clsWelcomeMessages>)Session["glstWelcomeMessages"];
        }
            PopulateFormData();

    }

    protected string EscapeLikeValue(string valueWithoutWildcards)
    {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < valueWithoutWildcards.Length; i++)
        {
            char c = valueWithoutWildcards[i];
            if (c == '*' || c == '%' || c == '[' || c == ']')
                sb.Append("[").Append(c).Append("]");
            else if (c == '\'')
                sb.Append("''");
            else
                sb.Append(c);
        }
        return sb.ToString();
    }

    protected void lnkbtnSearch_Click(object sender, EventArgs e)
    {
        string EscapedString = EscapeLikeValue(txtSearch.Text.ToString());
        string FilterExpression ="(strHeaderText +' '+ strParagraphText +' '+ strMasterImage) LIKE '%" + EscapedString + "%'";
        DataTable dtWelcomeMessagesList = clsWelcomeMessages.GetWelcomeMessagesList(FilterExpression, "");

        Session["dtWelcomeMessagesList"] = dtWelcomeMessagesList;

        PopulateFormData();
    }

    protected void lnkbtnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmWelcomeMessagesAddModify.aspx");
    }

    #endregion

    #region DATA GRID METHODS

    private void PopulateFormData()
    {
        //### Populate datagrid using clsWelcomeMessagesList object
        try
        {
            dtWelcomeMessagesList = new DataTable();

            if (Session["dtWelcomeMessagesList"] == null)
                dtWelcomeMessagesList = clsWelcomeMessages.GetWelcomeMessagesList();
            else
                dtWelcomeMessagesList = (DataTable)Session["dtWelcomeMessagesList"];

            dgrGrid.DataSource = dtWelcomeMessagesList;

            //### Add this check to trap if the last item on the page is deleted
            if (Convert.ToInt16(dgrGrid.CurrentPageIndex) < Convert.ToInt16(Session["WelcomeMessagesView_CurrentPageIndex"]))
            {
                dgrGrid.CurrentPageIndex = 0;
            }
            else
            {
                if (Session["WelcomeMessagesView_CurrentPageIndex"] != null)
                {
                    dgrGrid.CurrentPageIndex = (int)Session["WelcomeMessagesView_CurrentPageIndex"];
                }
            }

            dgrGrid.DataBind();

        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void lnkDeleteItem_Click(object sender, EventArgs e)
    {
        int iWelcomeMessageID = int.Parse((sender as LinkButton).CommandArgument);

        clsWelcomeMessages.Delete(iWelcomeMessageID);

        DataTable dtUsersList = new DataTable();

        if (((txtSearch.Text == "SEARCH FOR A WELCOME MESSAGE") == false) && ((txtSearch.Text == "") == false))
        {
            string FilterExpression = "(strHeaderText +' '+ strParagraphText +' '+ strMasterImage) LIKE '%" + txtSearch.Text + "%'";
            DataTable dtWelcomeMessagesList = clsWelcomeMessages.GetWelcomeMessagesList(FilterExpression, "");
        }
        else
        {
            dtWelcomeMessagesList = clsWelcomeMessages.GetWelcomeMessagesList();
        }

        Session["dtWelcomeMessagesList"] = dtWelcomeMessagesList;

        PopulateFormData();
    }

    protected void dgrGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //### Method is used when the page index is changed.
        try
        {
            Session["WelcomeMessagesView_CurrentPageIndex"] = e.NewPageIndex;
            dgrGrid.CurrentPageIndex = e.NewPageIndex;
            PopulateFormData();
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgrGrid_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        //### Sort Method
        dtWelcomeMessagesList = new DataTable();

        if (Session["dtWelcomeMessagesList"] == null)
            dtWelcomeMessagesList = clsWelcomeMessages.GetWelcomeMessagesList();
        else
            dtWelcomeMessagesList = (DataTable)Session["dtWelcomeMessagesList"];

        DataView dvTemp = dtWelcomeMessagesList.DefaultView;

        dvTemp.Sort = e.SortExpression;
        dtWelcomeMessagesList = dvTemp.ToTable();
        Session["dtWelcomeMessagesList"] = dtWelcomeMessagesList;

        PopulateFormData();
    }

    public void dgrGrid_PopEditLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkEditItem = (HyperLink)sender;
            DataGridItem dgrItemEdit = (DataGridItem)lnkEditItem.Parent.Parent;

            //### Get the iWelcomeMessageID
            int iWelcomeMessageID = 0;
            iWelcomeMessageID = int.Parse(dgrItemEdit.Cells[0].Text);

            //### Add attributes to edit link
            lnkEditItem.Attributes.Add("href", "frmWelcomeMessagesAddModify.aspx?action=edit&iWelcomeMessageID=" + iWelcomeMessageID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

     public void dgrGrid_PopDeleteLink(object sender, EventArgs e)
     {
          try
          {
             //### Add hyperlink to datagrid item
             HyperLink lnkDeleteItem = (HyperLink)sender;
             DataGridItem dgrItemDelete = (DataGridItem)lnkDeleteItem.Parent.Parent;

             //### Get the iWelcomeMessageID
             int iWelcomeMessageID = 0;
             iWelcomeMessageID = int.Parse(dgrItemDelete.Cells[0].Text);

             //### Add attributes to delete link
             lnkDeleteItem.CssClass = "dgrDeleteLink";
             lnkDeleteItem.Attributes.Add("href", "frmWelcomeMessagesView.aspx?action=delete&iWelcomeMessageID=" + iWelcomeMessageID);
           }
           catch (Exception ex)
           {
                 Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
           }
     }

    #endregion

    #region WelcomeMessages FILTERING

    /// <summary>
    /// Enables the AutoCompleteExtender
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    /// 
    [WebMethod]
    [ScriptMethod]
    public static string[] FilterBusinessRule(string prefixText, int count)
    {
        string[] strReturnList = new string[] { };

        string FilterExpression = "(strHeaderText +' '+ strParagraphText +' '+ strMasterImage) LIKE '%" + prefixText + "%'";
        DataTable dtWelcomeMessages = clsWelcomeMessages.GetWelcomeMessagesList(FilterExpression, "");
        List<string> glstWelcomeMessages = new List<string>();

        if (dtWelcomeMessages.Rows.Count > 0)
        {
            foreach (DataRow dtrWelcomeMessages in dtWelcomeMessages.Rows)
            {
                glstWelcomeMessages.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem("" + dtrWelcomeMessages["strHeaderText"].ToString() +' '+"" + dtrWelcomeMessages["strParagraphText"].ToString() +' '+"" + dtrWelcomeMessages["strMasterImage"].ToString(), dtrWelcomeMessages["iWelcomeMessageID"].ToString()));
            }
        }
        else
            glstWelcomeMessages.Add("No WelcomeMessages Available.");
        strReturnList = glstWelcomeMessages.ToArray();
        return strReturnList;
    }

    #endregion
}
