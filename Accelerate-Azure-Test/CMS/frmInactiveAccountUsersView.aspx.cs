
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text;
using System.Net.Mail;
using System.Text.RegularExpressions;

/// <summary>
/// Summary description for clsInactiveAccountUsersView
/// </summary>
public partial class CMS_clsInactiveAccountUsersView : System.Web.UI.Page
{
    #region CLASSES AND VARIABLES

    clsUsers clsUsers;
    DataTable dtInactiveAccountUsersList;

    List<clsInactiveAccountUsers> glstInactiveAccountUsers;

    #endregion

    #region EVENT MODULES

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

        dgrGrid.ItemCreated += new DataGridItemEventHandler(clsCommonFunctions.dgrGrid_PagerPopulate);

        if (!Page.IsPostBack)
        {
            Session["dtInactiveAccountUsersList"] = null;
            PopulateFormData();
            if (Session["iNumberOfInactiveUsers"] != null && Convert.ToInt32(Session["iNumberOfInactiveUsers"]) != 0)
            {
                int iNumberOfInactiveUsers = Convert.ToInt32(Session["iNumberOfInactiveUsers"]);
                Response.Write(clsCommonFunctions.JSAlert(iNumberOfInactiveUsers.ToString() + " Users Added"));
                Session["iNumberOfInactiveUsers"] = null;
            }
        }
        else
        {
            dgrGrid.ItemCreated += new DataGridItemEventHandler(clsCommonFunctions.dgrGrid_PagerPopulate);
        }
        //PopulateFormData();
    }

    protected string EscapeLikeValue(string valueWithoutWildcards)
    {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < valueWithoutWildcards.Length; i++)
        {
            char c = valueWithoutWildcards[i];
            if (c == '*' || c == '%' || c == '[' || c == ']')
                sb.Append("[").Append(c).Append("]");
            else if (c == '\'')
                sb.Append("''");
            else
                sb.Append(c);
        }
        return sb.ToString();
    }

    protected void lnkbtnSearch_Click(object sender, EventArgs e)
    {
        string EscapedString = EscapeLikeValue(txtSearch.Text.ToString());
        string FilterExpression = "(strFirstName +' '+ strSurname +' '+ strEmailAddress +' '+ strJobTitle) LIKE '%" + EscapedString + "%'";
        DataTable dtInactiveAccountUsersList = clsInactiveAccountUsers.GetInactiveAccountUsersList(FilterExpression, "");

        Session["dtInactiveAccountUsersList"] = dtInactiveAccountUsersList;

        PopulateFormData();
    }

    protected void lnkbtnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmInactiveAccountUsersAddModify.aspx");
    }

    #endregion

    #region DATA GRID METHODS

    private void PopulateFormData()
    {
        //### Populate datagrid using clsInactiveAccountUsersList object
        try
        {
            dtInactiveAccountUsersList = new DataTable();

            if (Session["dtInactiveAccountUsersList"] == null)
                dtInactiveAccountUsersList = clsInactiveAccountUsers.GetInactiveAccountUsersList();
            else
                dtInactiveAccountUsersList = (DataTable)Session["dtInactiveAccountUsersList"];

            dgrGrid.DataSource = dtInactiveAccountUsersList;

            //### Add this check to trap if the last item on the page is deleted
            if (Convert.ToInt16(dgrGrid.CurrentPageIndex) < Convert.ToInt16(Session["InactiveAccountUsersView_CurrentPageIndex"]))
            {
                dgrGrid.CurrentPageIndex = 0;
            }
            else
            {
                if (Session["InactiveAccountUsersView_CurrentPageIndex"] != null)
                {
                    dgrGrid.CurrentPageIndex = (int)Session["InactiveAccountUsersView_CurrentPageIndex"];
                }
            }

            dgrGrid.DataBind();

        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void lnkDeleteItem_Click(object sender, EventArgs e)
    {
        int iInactiveAccountUserID = int.Parse((sender as LinkButton).CommandArgument);

        clsInactiveAccountUsers.Delete(iInactiveAccountUserID);

        DataTable dtInactiveAccountUsersList = new DataTable();

        if (((txtSearch.Text == "SEARCH FOR A INACTIVE ACCOUNT USER") == false) && ((txtSearch.Text == "") == false))
        {
            string FilterExpression = "(strFirstName +' '+ strSurname +' '+ strEmailAddress +' '+ strJobTitle) LIKE '%" + txtSearch.Text + "%'";
            dtInactiveAccountUsersList = clsInactiveAccountUsers.GetInactiveAccountUsersList(FilterExpression, "");
        }
        else
        {
            dtInactiveAccountUsersList = clsInactiveAccountUsers.GetInactiveAccountUsersList();
        }

        Session["dtInactiveAccountUsersList"] = dtInactiveAccountUsersList;

        PopulateFormData();

    }

    protected void dgrGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //### Method is used when the page index is changed.
        try
        {
            Session["InactiveAccountUsersView_CurrentPageIndex"] = e.NewPageIndex;
            dgrGrid.CurrentPageIndex = e.NewPageIndex;
            PopulateFormData();
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgrGrid_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        //### Sort Method
        dtInactiveAccountUsersList = new DataTable();

        if (Session["dtInactiveAccountUsersList"] == null)
            dtInactiveAccountUsersList = clsInactiveAccountUsers.GetInactiveAccountUsersList();
        else
            dtInactiveAccountUsersList = (DataTable)Session["dtInactiveAccountUsersList"];

        DataView dvTemp = dtInactiveAccountUsersList.DefaultView;

        dvTemp.Sort = e.SortExpression;
        dtInactiveAccountUsersList = dvTemp.ToTable();
        Session["dtInactiveAccountUsersList"] = dtInactiveAccountUsersList;

        PopulateFormData();
    }

    public void dgrGrid_PopEditLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkEditItem = (HyperLink)sender;
            DataGridItem dgrItemEdit = (DataGridItem)lnkEditItem.Parent.Parent;

            //### Get the iInactiveAccountUserID
            int iInactiveAccountUserID = 0;
            iInactiveAccountUserID = int.Parse(dgrItemEdit.Cells[0].Text);

            //### Add attributes to edit link
            lnkEditItem.Attributes.Add("href", "frmInactiveAccountUsersAddModify.aspx?action=edit&iInactiveAccountUserID=" + iInactiveAccountUserID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void lnkbtnImport_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmImportAccountUsers.aspx");
    }

    protected void lstGroup_DataBinding(object sender, EventArgs e)
    {
        DropDownList lstGroup = (DropDownList)sender;
        DataGridItem dgrGroup = (DataGridItem)lstGroup.Parent.Parent;

        //clsGroups clsGroups = new clsGroups();
        //DataTable dtGroups = clsGroups.GetGroupsList();

        lstGroup.DataSource = clsGroups.GetGroupsList();

        //ddlGroup.DataSource = dtGroups;
        lstGroup.DataValueField = "iGroupID";
        lstGroup.DataTextField = "strTitle";

        //ddlGroup.DataBind();
        lstGroup.Items.Insert(0, new ListItem("--Not Selected--", "0"));
    }

    #endregion

    #region InactiveAccountUsers FILTERING

    /// <summary>
    /// Enables the AutoCompleteExtender
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    /// 
    [WebMethod]
    [ScriptMethod]
    public static string[] FilterBusinessRule(string prefixText, int count)
    {
        string[] strReturnList = new string[] { };

        string FilterExpression = "(strFirstName +' '+ strSurname +' '+ strEmailAddress +' '+ strJobTitle) LIKE '%" + prefixText + "%'";
        DataTable dtInactiveAccountUsers = clsInactiveAccountUsers.GetInactiveAccountUsersList(FilterExpression, "");
        List<string> glstInactiveAccountUsers = new List<string>();

        if (dtInactiveAccountUsers.Rows.Count > 0)
        {
            foreach (DataRow dtrInactiveAccountUsers in dtInactiveAccountUsers.Rows)
            {
                glstInactiveAccountUsers.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem("" + dtrInactiveAccountUsers["strFirstName"].ToString() + ' ' + "" + dtrInactiveAccountUsers["strSurname"].ToString() + ' ' + "" + dtrInactiveAccountUsers["strEmailAddress"].ToString() + ' ' + "" + dtrInactiveAccountUsers["strJobTitle"].ToString(), dtrInactiveAccountUsers["iInactiveAccountUserID"].ToString()));
            }
        }
        else
            glstInactiveAccountUsers.Add("No InactiveAccountUsers Available.");
        strReturnList = glstInactiveAccountUsers.ToArray();
        return strReturnList;
    }

    #endregion



    protected void chkbxAddUser_DataBinding(object sender, EventArgs e)
    {

    }

    

    protected void lnkbtnActivate_Click(object sender, EventArgs e)
    {
        string activeUsers = "";

        foreach (DataGridItem di in dgrGrid.Items)
        {
            CheckBox chkbxActive = di.FindControl("chkbxActivate") as CheckBox;
            if(chkbxActive.Checked == true)
            {
                if (clsCommonFunctions.DoesRecordExist("tblAccountUsers", "strEmailAddress = '" + di.Cells[4].Text.ToString() + "' AND bIsDeleted = 0") == true)
                {
                    string userEmail = di.Cells[4].Text.ToString();
                    activeUsers += "Account User " + di.Cells[4].Text.ToString() + " already exists.\n";

                    string id = di.Cells[0].Text.ToString();
                    int iInactiveAccountUserID = Convert.ToInt32(id.ToString());
                    clsInactiveAccountUsers.Delete(iInactiveAccountUserID);
                }
                else
                {
                    DropDownList lstGroupID = di.FindControl("lstGroup") as DropDownList;
                    string id = di.Cells[0].Text.ToString();
                    int iGroupID = Convert.ToInt32(lstGroupID.SelectedValue);
                    int iInactiveAccountUserID = Convert.ToInt32(id.ToString());

                    clsAccountUsers clsAccountUsers = new clsAccountUsers();
                    clsInactiveAccountUsers clsInactiveAccountUsers = new clsInactiveAccountUsers(iInactiveAccountUserID);

                    clsAccountUsers.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
                    clsAccountUsers.iAddedBy = clsUsers.iUserID;
                    clsAccountUsers.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
                    clsAccountUsers.iEditedBy = clsUsers.iUserID;
                    clsAccountUsers.strFirstName = clsInactiveAccountUsers.strFirstName.ToString();
                    clsAccountUsers.strSurname = clsInactiveAccountUsers.strSurname.ToString();
                    clsAccountUsers.strEmailAddress = clsInactiveAccountUsers.strEmailAddress.ToString();
                    clsAccountUsers.strPhoneNumber = clsInactiveAccountUsers.strPhoneNumber.ToString();
                    clsAccountUsers.strJobTitle = clsInactiveAccountUsers.strJobTitle.ToString();

                    string strHashPassword = clsCommonFunctions.GetMd5Sum(clsInactiveAccountUsers.strEmailAddress.ToString().ToLower());
                    clsAccountUsers.strPassword = strHashPassword;
                    //### Images related items
                    clsAccountUsers.strPathToImages = "";
                    clsAccountUsers.strMasterImage = "";

                    string strFullPathToImage = clsAccountUsers.strPathToImages + clsAccountUsers.strMasterImage;
                    clsAccountUsers.iCurrentMissionID = 0;
                    clsAccountUsers.iCurrentPoints = 0;

                    clsAccountUsers.iGroupID = iGroupID;
                    clsAccountUsers.bHasCompletedProfileMission = false;

                    clsAccountUsers.iCurrentMissionID = 0;
                    clsAccountUsers.iCurrentPoints = 0;

                    clsAccountUsers.Update();

                    clsAccountUserTrackings clsAccountUserTrackings = new clsAccountUserTrackings();
                    clsAccountUserTrackings.dtAdded = DateTime.Now;
                    clsAccountUserTrackings.bHasEmailBeenSent = false;
                    clsAccountUserTrackings.iCurrentPhaseLevel = 1;
                    clsAccountUserTrackings.bIsPhase1Complete = false;
                    clsAccountUserTrackings.bIsPhase2Complete = false;
                    clsAccountUserTrackings.bIsPhase3Complete = false;

                    DataTable dtAllPhasesForThatGroup = clsGroupPhases.GetGroupPhasesList("iGroupID=" + clsAccountUsers.iGroupID, "");

                    int iCountID = 0;

                    foreach (DataRow dtrPhaseInGroup in dtAllPhasesForThatGroup.Rows)
                    {
                        ++iCountID;

                        int iCurrentPhaseID;

                        if (iCountID == 1)
                        {
                            iCurrentPhaseID = Convert.ToInt32(dtrPhaseInGroup["iPhaseID"]);
                            clsAccountUserTrackings.iPhase1ID = iCurrentPhaseID;
                            clsAccountUserTrackings.iCurrentPhaseID = iCurrentPhaseID;
                        }
                        else if (iCountID == 2)
                        {
                            iCurrentPhaseID = Convert.ToInt32(dtrPhaseInGroup["iPhaseID"]);
                            clsAccountUserTrackings.iPhase2ID = iCurrentPhaseID;
                        }
                        else if (iCountID == 3)
                        {
                            iCurrentPhaseID = Convert.ToInt32(dtrPhaseInGroup["iPhaseID"]);
                            clsAccountUserTrackings.iPhase3ID = iCurrentPhaseID;
                        }
                    }

                    DataTable dtPhase1Missions = clsPhaseMissions.GetPhaseMissionsList("iPhaseID=" + clsAccountUserTrackings.iPhase1ID, "");

                    foreach (DataRow dtrMissionsInPhase in dtPhase1Missions.Rows)
                    {
                        clsAccountUsers.iCurrentMissionID = Convert.ToInt32(dtrMissionsInPhase["iMissionID"]);
                        break;
                    }

                    clsAccountUserTrackings.Update();
                    clsAccountUsers.iAccountUserTrackingID = clsAccountUserTrackings.iAccountUserTrackingID;
                    clsAccountUsers.iCurrentPhaseID = clsAccountUserTrackings.iCurrentPhaseID;
                    clsAccountUsers.Update();

                    //Send User Welcome Email
                    SendReport(clsAccountUsers.strFirstName, clsAccountUsers.strEmailAddress);

                    //Delete Inactive User
                    clsInactiveAccountUsers.Delete(iInactiveAccountUserID);
                    
                }
                
            }
        }
        if (activeUsers != "")
        {
            Response.Write(clsCommonFunctions.JSAlert("Account User " + activeUsers + "already exists"));
        }

        Response.Redirect("frmInactiveAccountUsersView.aspx");
    }

    protected void chkbxActivate_CheckedChanged(object sender, EventArgs e)
    {
        dgrGrid.ItemCreated += new DataGridItemEventHandler(clsCommonFunctions.dgrGrid_PagerPopulate);
    }

    protected void dgrGrid_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    #region REPORT SECTION

    protected void SendReport(string strFirstName, string strEmail)
    {
        string strReportName = "Welcome";

        StringBuilder strbMailBuilder = new StringBuilder();
        string strName = strFirstName;

        //removing white space from email address
        string strEmailAddress = strEmail;
        //strEmailAddress = strEmailAddress.Trim();
        //strEmailAddress = Regex.Replace(strEmailAddress, @"\s", "");
        //strEmailAddress = strEmailAddress.ToLower();

        strbMailBuilder.AppendLine("<!-- Start of Text -->");
        strbMailBuilder.AppendLine("<table width='650' bgcolor='#FFF' cellpadding='30' cellspacing='0' border='0'  border='none' align='center' class='devicewidth' options=''>"); //style='border-left:1px solid #333; border-right:1px solid #333;'
        strbMailBuilder.AppendLine("<tbody>");

        strbMailBuilder.AppendLine("<!-- Content -->");
        strbMailBuilder.AppendLine("<tr>");
        //strbMailBuilder.AppendLine("<td width='60'></td>");

        //strbMailBuilder.AppendLine("<td valign='top' style='font-family:Arial;font-size: 15px; color: #3b3b3b; text-align:left;line-height: 22px;' text=''>");
        strbMailBuilder.AppendLine("<td valign='top' style='color:black; font-family:Verdana; font-size:12px; text-align:left;' text=''>");
        strbMailBuilder.AppendLine("<p style='font-family:Verdana; font-size:12px; font-weight:bold'>Welcome to Accelerate</p>");
        strbMailBuilder.AppendLine("<br/><br/>");
        strbMailBuilder.AppendLine("Hi " + strName + ", ");
        strbMailBuilder.AppendLine("<br/><br/>");
        strbMailBuilder.AppendLine("Welcome to Deloitte. ");//strbMailBuilder.AppendLine("Congratulation on joining Deloitte Risk Advisory.As part of your onboarding experience, you will be introduced to Risk Advisory through an interactive, engaging <b>gamified</b> experience that is guaranteed tochallenge, engage and excite you.<br /><br/>");
        strbMailBuilder.AppendLine("As part of your onboarding experience, you will be introduced to Deloitte through an interactive, gamified experience that is guaranteed to challenge, engage and excite you.<br /><br/>");
        strbMailBuilder.AppendLine("Log on to the Accelerate website using the following credentials to kick-off your first mission.<br /><br/>");

        strbMailBuilder.AppendLine("Username: " + "<span style='color: #6c7480 !important; text-decoration: underline;'>" + strEmailAddress + "</span><br/>");
        strbMailBuilder.AppendLine("Password: " + "<span style='color: #6c7480 !important; text-decoration: underline;'>" + strEmailAddress + "</span><br/>");
        strbMailBuilder.AppendLine("Link: <a href='" + System.Configuration.ConfigurationManager.AppSettings["WebRoot"] + "Accelerate-Login.aspx" + "'>" + System.Configuration.ConfigurationManager.AppSettings["WebRoot"] + "</a><br/><br/>");

        strbMailBuilder.AppendLine("<p style='color:gray; font-family:Verdana; font-size:12px; font-weight:bold'>Moving forward</p>");
        //strbMailBuilder.AppendLine("<h2>Why Onboarding?</h2>");//strbMailBuilder.AppendLine("<h2>Why Gamification?</h2>");
        strbMailBuilder.AppendLine("We are committed to you and your success at Deloitte, which is why we have created a tailor-made solution that helps you jump start your career!<br/><br/>");//strbMailBuilder.AppendLine("We are committed to you and your success at Deloitte, which is why we have created a tailor made solution that you help you get a jump start on your career!Think happier, more engaged, integrated employees.<br/>");
        strbMailBuilder.AppendLine("<p style='color:gray; font-family:Verdana; font-size:12px; font-weight:bold'>What will you learn?</p>");

        strbMailBuilder.AppendLine("This programme will form the foundation for your specific service-line onboarding experience. Ultimately, you will be empowered to hit the ground running as a knowledgeable, happy and productive member of your team.<br/><br/>");
        strbMailBuilder.AppendLine("By completing missions on the system, you will earn points and ultimately will get to know your Deloitte, your business unit and your leadership better.  Learn, network and become a Deloitte Citizen.<br/><br/>");
        strbMailBuilder.AppendLine("So again, welcome to the Deloitte family. ");
        strbMailBuilder.AppendLine("We hope that your journey will be a rewarding one.<br/><br/>");
        //strbMailBuilder.AppendLine("By completing missions on the system, you will earn points and ultimately will get to know your Deloitte, your Consulting and your leadership better.<br/>");
        //strbMailBuilder.AppendLine("<i>*Completing all missions also earns you Challenger Series points, increasing your chances of winning iPads, gift vouchers and other great prizes.</i><br/>");
        //strbMailBuilder.AppendLine("info@stratagc.co.za<br/>");

        //strbMailBuilder.AppendLine("Learn,�network�and become a Deloitte Citizen.<br/>");
        //strbMailBuilder.AppendLine("The Onboarding Team<br/>");
        //strbMailBuilder.AppendLine("0725967186");
        strbMailBuilder.AppendLine("--<br/>");
        strbMailBuilder.AppendLine("<p style='font-family:Verdana; font-size:11px;'><b>The Onboarding Team</b></p>");
        strbMailBuilder.AppendLine("<p style='font-family:Verdana; font-size:11px;'>D: +27 (0)84 280 9206</p>");
        strbMailBuilder.AppendLine("<a style='font-family:Verdana; font-size:11px;' href='info@stratagc.co.za'>info@stratagc.co.za</a> | <a style='font-family:Verdana; font-size:11px;' href='www.deloitte.com'>www.deloitte.com</a><br/>");
        strbMailBuilder.AppendLine("</td>");
        strbMailBuilder.AppendLine("</tr>");

        //###END OF EMAIL
        //strbMailBuilder.AppendLine("<td valign='top' style='color:black; font-family:Verdana; font-size:12px; text-align:left;' text=''>");
        //strbMailBuilder.AppendLine("<p style='font-family:Verdana; font-size:11px;'><b>The On-boarding Team</b></p>");
        //strbMailBuilder.AppendLine("<p style='font-family:Verdana; font-size:11px;'>D: +27 (0)84 280 9206</p>");
        //strbMailBuilder.AppendLine("<p style='font-family:Verdana; font-size:11px;'>D: +27 (0)84 280 9206</p>");
        //strbMailBuilder.AppendLine("<a style='font-family:Verdana; font-size:11px;' href='info@stratagc.co.za'>info@stratagc.co.za</a> | <a style='font-family:Verdana; font-size:11px;' href='www.deloitte.com'>www.deloitte.com</a><br/><br/><br/>");
        //strbMailBuilder.AppendLine("<a href='mailto:info@stratagc.co.za'><img src=\"cid:getintouch\" alt='getintouch' /></a><br/><br/><br/>");

        //strbMailBuilder.AppendLine("</td>");
        //strbMailBuilder.AppendLine("<td width='60'></td>");
        //strbMailBuilder.AppendLine("</tr>");
        //strbMailBuilder.AppendLine("<tr>");
        //strbMailBuilder.AppendLine("<td valign='top' style='text-align:center'>");
        //strbMailBuilder.AppendLine("<a style='display:block; margin:0px 20px 0px 20px' href='https://www.facebook.com/deloitte'><img src=\"cid:FacebookEmailLinkThumb\" alt='Facebook' /></a>");
        //strbMailBuilder.AppendLine("<a style='display:block; margin:0px 5px 0px 5px' href='https://twitter.com/Deloitte'><img src=\"cid:TwitterEmailLinkThumb\" alt='Twitter' /></a>");
        //strbMailBuilder.AppendLine("<a style='display:block; margin:0px 5px 0px 5px' href='http://www.linkedin.com/company/deloitte'><img src=\"cid:LinkedInEmailLinkThumb\" alt='LinkedIn' /></a>");
        //strbMailBuilder.AppendLine("<a style='display:block; margin:0px 5px 0px 5px' href='https://plus.google.com/+Deloitte/posts'><img src=\"cid:GooglePEmailLinkThumb\" alt='Google' /></a>");
        //strbMailBuilder.AppendLine("<a style='display:block; margin:0px 5px 0px 5px' href='http://www.pinterest.com/deloitte/'><img src=\"cid:PinInEmailLinkThumb\" alt='PinInterest' /></a>");
        //strbMailBuilder.AppendLine("<a href='#'><img src=\"cid:MailEmailLinkThumb\" alt='Mail' /></a>");
        //strbMailBuilder.AppendLine("<br/><br/><br/>");
        //strbMailBuilder.AppendLine("</td>");

        //strbMailBuilder.AppendLine("</tr>");

        //strbMailBuilder.AppendLine("</br>");

        //strbMailBuilder.AppendLine("<tr>");
        //strbMailBuilder.AppendLine("<td valign='top' style='color:gray; font-family:Verdana; font-size:10px; text-align:left;' text=''>");
        //strbMailBuilder.AppendLine("Deloitte refers to one or more of Deloitte Touche Tohmatsu Limited, a UK private ");
        //strbMailBuilder.AppendLine("company limited by guarantee (DTTL), its network of member firms and their related ");
        //strbMailBuilder.AppendLine("entities. DTTL and each of its member firms are legally separate and independent ");
        //strbMailBuilder.AppendLine("entities. DTTL (also referred to as �Deloitte Global�) does not provide services to ");
        //strbMailBuilder.AppendLine("clients. Please see www.deloitte.com/about for a more detailed description of DTTL and its member firms.<br/><br/>");

        //strbMailBuilder.AppendLine("This communication is for internal distribution and use only among personnel of ");
        //strbMailBuilder.AppendLine("Deloitte Touche Tohmatsu Limited, its member firms, and their related entities ");
        //strbMailBuilder.AppendLine("(collectively, the �Deloitte network�). None of the Deloitte network shall be responsible ");
        //strbMailBuilder.AppendLine("for any loss whatsoever sustained by any person who relies on this communication.<br/><br/>");

        //strbMailBuilder.AppendLine("� 2016. For information, contact Deloitte Touche Tohmatsu Limited<br/><br/>");

        //strbMailBuilder.AppendLine("<p style='color:black; font-family:Verdana; font-size:9px;'>To no longer receive emails about this topic please send a return email to the sender with the word �Unsubscribe� in the subject line.</p>");

        //strbMailBuilder.AppendLine("</td>");
        //strbMailBuilder.AppendLine("</tr>");



        strbMailBuilder.AppendLine("<!-- End of Content -->");

        strbMailBuilder.AppendLine("</tbody>");
        strbMailBuilder.AppendLine("</table>");
        strbMailBuilder.AppendLine("<!-- End of Text -->");

        //strbMailBuilder.AppendLine("Hi " + txtFirstName.Text + ",<br /><br />");
        //strbMailBuilder.AppendLine("Congratulations on joining Deloitte.<br/>As part of your onboarding experience, you will be introduced to Deloitte through an interactive, engaging gamified experience that is guaranteed tochallenge, engage and excite you.<br /><br/>");//strbMailBuilder.AppendLine("Congratulation on joining Deloitte Risk Advisory.As part of your onboarding experience, you will be introduced to Risk Advisory through an interactive, engaging <b>gamified</b> experience that is guaranteed tochallenge, engage and excite you.<br /><br/>");
        //strbMailBuilder.AppendLine("Login to the Accelerate website using the following credentials to kick-off your first mission.<br /><br/>");//strbMailBuilder.AppendLine("Login to the Accelerate website using the following credentials to kick-off your first mission.<br /><br/>");

        //strbMailBuilder.AppendLine("Username: " + txtEmailAddress.Text + "<br/>");
        //strbMailBuilder.AppendLine("Password: " + txtEmailAddress.Text + "<br/>");
        //strbMailBuilder.AppendLine("Link: <a href='" + System.Configuration.ConfigurationManager.AppSettings["WebRoot"] + "Accelerate-Login.aspx" + "'>" + System.Configuration.ConfigurationManager.AppSettings["WebRoot"] + "</a><br/><br/>");

        //strbMailBuilder.AppendLine("<h2>Why Onboarding?</h2>");//strbMailBuilder.AppendLine("<h2>Why Gamification?</h2>");
        //strbMailBuilder.AppendLine("We are committed to you and your success at Deloitte, which is why we have created a tailor made solution that helps you jump start on your career!<br/>");//strbMailBuilder.AppendLine("We are committed to you and your success at Deloitte, which is why we have created a tailor made solution that you help you get a jump start on your career!Think happier, more engaged, integrated employees.<br/>");
        //strbMailBuilder.AppendLine("<h2>What will you learn?</h2>");
        //strbMailBuilder.AppendLine("By completing missions on the system, you will earn points and ultimately will get to know your Deloitte, your Consulting and your leadership better.<br/>");
        ////strbMailBuilder.AppendLine("<i>*Completing all missions also earns you Challenger Series points, increasing your chances of winning iPads, gift vouchers and other great prizes.</i><br/>");

        //strbMailBuilder.AppendLine("Learn,�network�and become a Deloitte Citizen.<br/>");
        //strbMailBuilder.AppendLine("Regards,<br/>");
        //strbMailBuilder.AppendLine("The On-boarding Team<br/>");
        //strbMailBuilder.AppendLine("info@stratagc.co.za<br/>");
        //strbMailBuilder.AppendLine("0842809206");
        //strbMailBuilder.AppendLine("</td>");
        //strbMailBuilder.AppendLine("<td width='60'></td>");
        //strbMailBuilder.AppendLine("</tr>");
        //strbMailBuilder.AppendLine("<!-- End of Content -->");

        //strbMailBuilder.AppendLine("</td>");
        //strbMailBuilder.AppendLine("<td width='20'></td>");
        //strbMailBuilder.AppendLine("</tr>");
        //strbMailBuilder.AppendLine("</tbody>");
        //strbMailBuilder.AppendLine("</table>");
        //strbMailBuilder.AppendLine("</td>");
        //strbMailBuilder.AppendLine("</tr>");
        //strbMailBuilder.AppendLine("<!-- Spacing -->");
        //strbMailBuilder.AppendLine("<tr>");
        //strbMailBuilder.AppendLine("<td height='50'></td>");
        //strbMailBuilder.AppendLine("</tr>");
        //strbMailBuilder.AppendLine("<!-- End of Spacing -->");
        //strbMailBuilder.AppendLine("</tbody>");
        //strbMailBuilder.AppendLine("</table>");
        //strbMailBuilder.AppendLine("</td>");
        //strbMailBuilder.AppendLine("</tr>");
        //strbMailBuilder.AppendLine("</tbody>");
        //strbMailBuilder.AppendLine("</table>");
        //strbMailBuilder.AppendLine("<!-- End of textbanner -->");

        Attachment[] empty = new Attachment[] { };


        // onboarding@deloitteactivate.co.za
        //testing all email sending

        //emailComponent.SendMail("andrew@softservedigital.co.za", txtEmailAddress.Text, "", "", strReportName, strbMailBuilder.ToString(), "Heading", "Subheading", empty, true);
        //finalEmail.SendMail("andrew@softservedigital.co.za", txtEmailAddress.Text, "", "", strReportName, strbMailBuilder.ToString(), "Heading", "Subheading", empty, true);
        //missionCompletionEmail.SendMail("andrew@softservedigital.co.za", txtEmailAddress.Text, "", "", strReportName, strbMailBuilder.ToString(), "Heading", "Subheading", empty, true);
        //notificationEmail.SendMail("andrew@softservedigital.co.za", txtEmailAddress.Text, "", "", strReportName, strbMailBuilder.ToString(), "Heading", "Subheading", empty, true);
        //passwordReminder.SendMail("andrew@softservedigital.co.za", txtEmailAddress.Text, "", "", strReportName, strbMailBuilder.ToString(), "Heading", "Subheading", empty, true);
        //PhaseCompletionEmail.SendMail("andrew@softservedigital.co.za", txtEmailAddress.Text, "", "", strReportName, strbMailBuilder.ToString(), "Heading", "Subheading", empty, true);
        //reminderEmail.SendMail("andrew@softservedigital.co.za", txtEmailAddress.Text, "", "", strReportName, strbMailBuilder.ToString(), "Heading", "Subheading", empty, true);
        //string sendMailTo = strEmailAddress.Text.ToString();
        try
        {
            // ### TEST ###
            //emailComponentNew.SendMail("jamie@softservedigital.co.za", "jamie@softservedigital.co.za", "", "", strReportName, strbMailBuilder.ToString(), "Heading", "Subheading", empty, true);
            emailComponentNew.SendMail("noreply@deloitte.co.za", strEmailAddress, "", "", strReportName, strbMailBuilder.ToString(), "Heading", "Subheading", empty, true);
        }
        catch (Exception ex)
        {
            //lblValidationMessage.Text = "<div class=\"validationImageCorrectLogin\"></div><div class=\"validationLabel\">Your email could not be sent to " + sendMailTo.ToString() + ". </div>";
            //lblValidationMessage.Text += ex.ToString();
        }
        //### Redirect
        //lblValidationMessage.Text = "<div class=\"validationImageCorrectLogin\"></div><div class=\"validationLabel\">Your email has been sent.</div>";
    }

    #endregion

}
