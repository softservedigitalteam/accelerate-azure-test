<%@ Page Title="Phases" Language="C#" MasterPageFile="~/CMS/CMS.master" AutoEventWireup="true" Inherits="CMS_clsPhasesView" Codebehind="frmPhasesView.aspx.cs" %>

<%@ Register TagPrefix="ajax" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<script lang="javascript" type="text/javascript">
    //### Password check
    function clearText() {
            document.getElementById("<%=txtSearch.ClientID%>").value = ""
    }
</script>
<script type="text/javascript">
    function RefreshUpdatePanel(sender) {
        __doPostBack(sender, '');
    }
</script>

<!-- Event Manager for selection of item from auto-complete list -->
<script lang="javascript" type="text/javascript">
    function ClientItemSelected(sender, e) {
        $get("<%=hfPhaseID.ClientID %>").value = e.get_value();
        $get('<%=lnkbtnSearch.ClientID %>').click();
    }

    function toggleDiv(divID) {
        $(divID).slideToggle();
    }
</script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <asp:Panel ID="pSearch" runat="server" DefaultButton="lnkbtnSearch">
        <div class="controlDiv" style="margin-top:15px;">
            <div class="fieldSearchDiv">
                <asp:TextBox ID="txtSearch" runat="server" CssClass="roundedCornerTextBox" Text="SEARCH FOR A PHASE" onfocus="clearText();"/>
                <ajax:AutoCompleteExtender ID="acePhases" runat="server" TargetControlID="txtSearch" ServiceMethod="FilterBusinessRule"
                        EnableCaching="true" MinimumPrefixLength="1" CompletionInterval="0" CompletionSetCount="3" 
                        CompletionListCssClass="popupCompletionList" CompletionListItemCssClass="popupCompletionListItem" CompletionListHighlightedItemCssClass="popupCompletionListItemHighlight"
                        OnClientItemSelected="ClientItemSelected" FirstRowSelected="true" />
                        <asp:HiddenField ID="hfPhaseID" runat="server" Value="" />
            </div>
            <div class="buttonsSearchDiv"><asp:LinkButton ID="lnkbtnSearch" runat="server" CssClass="searchButton" onclick="lnkbtnSearch_Click" /></div>
        </div>
        <br class="clearingSpacer"/>
    </asp:Panel>

    <br />

    <asp:UpdatePanel ID="udpPhasesView" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:DataGrid ID="dgrGrid" Runat="server" AutoGenerateColumns="False" AllowSorting="true" 
                AllowPaging="true" PageSize="15" PagerStyle-Mode="NumericPages" OnPageIndexChanged="dgrGrid_PageIndexChanged" 
                OnSortCommand="dgrGrid_SortCommand" CellPadding="3" CellSpacing="0" PagerStyle-NextPageText="Next" PagerStyle-PrevPageText="Prev"
                HeaderStyle-CssClass="dgrHeader"  ItemStyle-CssClass="dgrItem" AlternatingItemStyle-CssClass="dgrAltItem" PagerStyle-CssClass="dgrPg" CssClass="dgr">
                <Columns>
                    <asp:BoundColumn DataField="iPhaseID" Visible="false" />
                    <asp:BoundColumn DataField="strTitle" SortExpression="strTitle" HeaderText="Title" HeaderStyle-CssClass="dgrHeaderLeft" HeaderStyle-HorizontalAlign="Left"/>
                    <asp:BoundColumn DataField="strTag" SortExpression="strTag" HeaderText="Tag" HeaderStyle-HorizontalAlign="Left"/>
                    <asp:BoundColumn DataField="strMasterImage" SortExpression="strMasterImage" HeaderText="Master Image" HeaderStyle-HorizontalAlign="Left"/>

                    <asp:TemplateColumn HeaderText="Add Missions" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50px" >
                        <ItemTemplate>
                            <asp:HyperLink ID="lnkAddMissionsToShop" runat="Server" CssClass="dgrLinkAdd" OnDataBinding="dgrGrid_PoplnkAddMissionsToShopLink" />
                        </ItemTemplate>
                    </asp:TemplateColumn>

                    <asp:TemplateColumn HeaderText="Edit" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50px" >
                        <ItemTemplate>
                            <asp:HyperLink ID="lnkEditItem" runat="Server" CssClass="dgrLinkEdit" OnDataBinding="dgrGrid_PopEditLink" />
                        </ItemTemplate>
                    </asp:TemplateColumn>

                    <asp:TemplateColumn HeaderText="Delete" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50px"  HeaderStyle-CssClass="dgrHeaderRight" >
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkDeleteItem" runat="server" CssClass="dgrLinkDelete" CommandArgument='<%# Eval("iPhaseID") %>' OnClick="lnkDeleteItem_Click" OnClientClick="return jsDeleteConfirm()"/>
                        </ItemTemplate>
                    </asp:TemplateColumn>

                </Columns>
            </asp:DataGrid>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div class="buttonsRightDiv">
        <asp:LinkButton ID="lnkbtnAdd" runat="server" CssClass="addButton" onclick="lnkbtnAdd_Click" />
    </div>
</asp:Content>
