
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text;

/// <summary>
/// Summary description for clsMissionQuestionsView
/// </summary>
public partial class CMS_clsMissionQuestionsView : System.Web.UI.Page
{
    #region CLASSES AND VARIABLES

    clsUsers clsUsers;
    DataTable dtMissionQuestionsList;

    List<clsMissionQuestions> glstMissionQuestions;

    #endregion

    #region EVENT MODULES

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

dgrGrid.ItemCreated += new DataGridItemEventHandler(clsCommonFunctions.dgrGrid_PagerPopulate);

        if (!Page.IsPostBack)
        {
            Session["dtMissionQuestionsList"] = null;
            PopulateFormData();
        }
        else
        {
            if (Session["glstMissionQuestions"] == null)
            {
                glstMissionQuestions = new List<clsMissionQuestions>();
                Session["glstMissionQuestions"] = glstMissionQuestions;
            }
            else
                glstMissionQuestions = (List<clsMissionQuestions>)Session["glstMissionQuestions"];
        }
            PopulateFormData();

    }

    protected string EscapeLikeValue(string valueWithoutWildcards)
    {
        StringBuilder sb = new StringBuilder();
 
        for (int i = 0; i < valueWithoutWildcards.Length; i++)
        {
            char c = valueWithoutWildcards[i];
            if (c == '*' || c == '%' || c == '[' || c == ']')
                sb.Append("[").Append(c).Append("]");
            else if (c == '\'')
                sb.Append("''");
            else
                sb.Append(c);
        }
        return sb.ToString();
    }

    protected void lnkbtnSearch_Click(object sender, EventArgs e)
    {
        string EscapedString = EscapeLikeValue(txtSearch.Text.ToString());
        string FilterExpression ="(strQuestion) LIKE '%" + EscapedString + "%'";
        DataTable dtMissionQuestionsList = clsMissionQuestions.GetMissionQuestionsList(FilterExpression, "");

        Session["dtMissionQuestionsList"] = dtMissionQuestionsList;

        PopulateFormData();
    }

    protected void lnkbtnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmMissionQuestionsAddModify.aspx");
    }

    #endregion

    #region DATA GRID METHODS

    private void PopulateFormData()
    {
        //### Populate datagrid using clsMissionQuestionsList object
        try
        {
            dtMissionQuestionsList = new DataTable();

            if (Session["dtMissionQuestionsList"] == null)
                dtMissionQuestionsList = clsMissionQuestions.GetMissionQuestionsList("", "dtAdded DESC");
            else
                dtMissionQuestionsList = (DataTable)Session["dtMissionQuestionsList"];

            dgrGrid.DataSource = dtMissionQuestionsList;

            //### Add this check to trap if the last item on the page is deleted
            if (Convert.ToInt16(dgrGrid.CurrentPageIndex) < Convert.ToInt16(Session["MissionQuestionsView_CurrentPageIndex"]))
            {
                dgrGrid.CurrentPageIndex = 0;
            }
            else
            {
                if (Session["MissionQuestionsView_CurrentPageIndex"] != null)
                {
                    dgrGrid.CurrentPageIndex = (int)Session["MissionQuestionsView_CurrentPageIndex"];
                }
            }

            dgrGrid.DataBind();

        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void lnkDeleteItem_Click(object sender, EventArgs e)
    {
        int iMissionQuestionID = int.Parse((sender as LinkButton).CommandArgument);

        clsMissionQuestions.Delete(iMissionQuestionID);

        DataTable dtMissionQuestionsList = new DataTable();

        if (((txtSearch.Text == "SEARCH FOR A MISSION QUESTION") == false) && ((txtSearch.Text == "") == false))
        {
            string FilterExpression = "(strQuestion) LIKE '%" + txtSearch.Text + "%'";
            dtMissionQuestionsList = clsMissionQuestions.GetMissionQuestionsList(FilterExpression, "");
        }
        else
        {
            dtMissionQuestionsList = clsMissionQuestions.GetMissionQuestionsList();
        }

        Session["dtMissionQuestionsList"] = dtMissionQuestionsList;

        PopulateFormData();
    }

    protected void dgrGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //### Method is used when the page index is changed.
        try
        {
            Session["MissionQuestionsView_CurrentPageIndex"] = e.NewPageIndex;
            dgrGrid.CurrentPageIndex = e.NewPageIndex;
            PopulateFormData();
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgrGrid_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        //### Sort Method
        dtMissionQuestionsList = new DataTable();

        if (Session["dtMissionQuestionsList"] == null)
            dtMissionQuestionsList = clsMissionQuestions.GetMissionQuestionsList();
        else
            dtMissionQuestionsList = (DataTable)Session["dtMissionQuestionsList"];

        DataView dvTemp = dtMissionQuestionsList.DefaultView;

        dvTemp.Sort = e.SortExpression;
        dtMissionQuestionsList = dvTemp.ToTable();
        Session["dtMissionQuestionsList"] = dtMissionQuestionsList;

        PopulateFormData();
    }

    public void dgrGrid_PopEditLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkEditItem = (HyperLink)sender;
            DataGridItem dgrItemEdit = (DataGridItem)lnkEditItem.Parent.Parent;

            //### Get the iMissionQuestionID
            int iMissionQuestionID = 0;
            iMissionQuestionID = int.Parse(dgrItemEdit.Cells[0].Text);

            //### Add attributes to edit link
            lnkEditItem.Attributes.Add("href", "frmMissionQuestionsAddModify.aspx?action=edit&iMissionQuestionID=" + iMissionQuestionID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

     public void dgrGrid_PopDeleteLink(object sender, EventArgs e)
     {
          try
          {
             //### Add hyperlink to datagrid item
             HyperLink lnkDeleteItem = (HyperLink)sender;
             DataGridItem dgrItemDelete = (DataGridItem)lnkDeleteItem.Parent.Parent;

             //### Get the iMissionQuestionID
             int iMissionQuestionID = 0;
             iMissionQuestionID = int.Parse(dgrItemDelete.Cells[0].Text);

             //### Add attributes to delete link
             lnkDeleteItem.CssClass = "dgrDeleteLink";
             lnkDeleteItem.Attributes.Add("href", "frmMissionQuestionsView.aspx?action=delete&iMissionQuestionID=" + iMissionQuestionID);
           }
           catch (Exception ex)
           {
                 Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
           }
     }

    #endregion

    #region MissionQuestions FILTERING

    /// <summary>
    /// Enables the AutoCompleteExtender
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    /// 
    [WebMethod]
    [ScriptMethod]
    public static string[] FilterBusinessRule(string prefixText, int count)
    {
        string[] strReturnList = new string[] { };

        string FilterExpression = "(strQuestion) LIKE '%" + prefixText + "%'";
        DataTable dtMissionQuestions = clsMissionQuestions.GetMissionQuestionsList(FilterExpression, "");
        List<string> glstMissionQuestions = new List<string>();

        if (dtMissionQuestions.Rows.Count > 0)
        {
            foreach (DataRow dtrMissionQuestions in dtMissionQuestions.Rows)
            {
                glstMissionQuestions.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem("" + dtrMissionQuestions["strQuestion"].ToString(), dtrMissionQuestions["iMissionQuestionID"].ToString()));
            }
        }
        else
            glstMissionQuestions.Add("No MissionQuestions Available.");
        strReturnList = glstMissionQuestions.ToArray();
        return strReturnList;
    }

    #endregion
}
