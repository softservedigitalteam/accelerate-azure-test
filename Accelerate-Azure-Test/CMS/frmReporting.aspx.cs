﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class CMS_Reporting : System.Web.UI.Page
{
    clsAccountUsers clsAccountUsers;
    clsUsers clsUsers;

    #region EVENTS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            //### Redirect back to login
            Response.Redirect("../CMSLogin.aspx");
        }
        clsUsers = (clsUsers)Session["clsUsers"];

        if (!IsPostBack)
        {
            popAccountUsers();
            popGroup();
            //### If the iUserID is passed through then we want to instantiate the obect with that iUserID
            if (Request.QueryString["iUserID"] != "" && Request.QueryString["iUserID"] != null)
            {
                clsUsers = new clsUsers(Convert.ToInt32(Request.QueryString["iUserID"]));

                //### Populate the form


                //### Hide password rows if editing

            }
            else
            {
                clsUsers = new clsUsers();
            }

            Session["clsUsers"] = clsUsers;
        }
        else
        {
            clsUsers = (clsUsers)Session["clsUsers"];
        }

    }

    protected void lnkbtnBack_Click(object sender, EventArgs e)
    {

    }
    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        //SendReport("mishanth@stratagc.co.za");
        SendReport("jamie@softservedigital.co.za");
    }
    protected void lnkbtnClear_Click(object sender, EventArgs e)
    {

    }

    protected void rbUser_CheckedChanged(object sender, EventArgs e)
    {
        divUser.Visible = true;
        divGroup.Visible = false;
        mandatoryDiv.Visible = false;
        mandatoryDiv2.Visible = false;
    }

    protected void rbGroup_CheckedChanged(object sender, EventArgs e)
    {
        divUser.Visible = false;
        divGroup.Visible = true;
        mandatoryDiv.Visible = false;
        mandatoryDiv2.Visible = false;
    }

    #endregion

    #region POPULATE DATA METHODS

    private void popAccountUsers()
    {
        DataTable dtTitlesList = new DataTable();
        lstAccountUser.DataSource = clsAccountUsers.GetAccountUsersList();

        //### Populates the drop down list with PK and TITLE;
        lstAccountUser.DataValueField = "iAccountUserID";
        lstAccountUser.DataTextField = "strFirstName";

        //### Bind the data to the list;
        lstAccountUser.DataBind();

        //### Add default select option;
        lstAccountUser.Items.Insert(0, new ListItem("--Not Selected--", "0"));
    }

    private void popGroup()
    {
        DataTable dtGroupsList = new DataTable();
        lstGroup.DataSource = clsGroups.GetGroupsList();

        //### Populates the drop down list with PK and TITLE;
        lstGroup.DataValueField = "iGroupID";
        lstGroup.DataTextField = "strTitle";

        //### Bind the data to the list;
        lstGroup.DataBind();

        //### Add default select option;
        lstGroup.Items.Insert(0, new ListItem("--Not Selected--", "0"));
    }

    protected string getMissionQuestionAndAnswer(int iCurrentMissionID, int iMissionTypeID)
    {
        string strMissionQuestionAndAnswer = "";
        DataTable dtMission = new DataTable();

        if (iMissionTypeID == 1)
        {
            dtMission = clsMissionQuestions.GetMissionQuestionsList("iMissionID=" + iCurrentMissionID, "");

            StringBuilder StrBQuestion = new StringBuilder();

            string strQuestion;
            int iMissionQuestionID;

            int iCount = 0;

            foreach (DataRow dtrAccountMember in dtMission.Rows)
            {
                ++iCount;

                iMissionQuestionID = Convert.ToInt32(dtrAccountMember["iMissionQuestionID"].ToString());
                strQuestion = dtrAccountMember["strQuestion"].ToString();

                StrBQuestion.AppendLine(iCount + ") " + strQuestion + ":");
                StrBQuestion.AppendLine("<br />");

                int iCurrentUserAnswerID = clsCommonFunctions.returnRecordID("iUserAnswerID", "tblUserAnswers", "iMissionQuestionID=" + iMissionQuestionID + " AND bIsDeleted=0"); //" AND iAccountUserID=" + clsAccountUsers.iAccountUserID + 

                if (iCurrentUserAnswerID != 0)
                {
                    clsUserAnswers clsUserAnswers = new clsUserAnswers(iCurrentUserAnswerID);
                    //StrBQuestion.AppendLine("Answer");
                    //StrBQuestion.AppendLine("<br />");
                    StrBQuestion.AppendLine(clsUserAnswers.strAnswer);
                    StrBQuestion.AppendLine("<br />");
                    StrBQuestion.AppendLine("<br />");
                }

                strMissionQuestionAndAnswer = StrBQuestion.ToString();
            }
        }
        else if (iMissionTypeID == 2)
        {
            //### TEAMSHIP
            dtMission = clsTeamShips.GetTeamShipsList("iMissionID=" + iCurrentMissionID, "");

            StringBuilder StrBQuestion = new StringBuilder();

            string strName;
            string strTitle;
            string strFullPathToImages;
            int iCount = 0;

            foreach (DataRow dtrAccountMember in dtMission.Rows)
            {
                ++iCount;

                int iTeamshipID = Convert.ToInt32(dtrAccountMember["iTeamShipID"].ToString());
                strName = dtrAccountMember["strName"].ToString();
                strTitle = dtrAccountMember["strTitle"].ToString();

                clsTeamShips clsCurrentTeamShips = new clsTeamShips(iTeamshipID);

                if ((clsCurrentTeamShips.strMasterImage == "") || (clsCurrentTeamShips.strMasterImage == null))
                {
                    strFullPathToImages = "images/imgNoImage.png";
                }
                else
                {
                    strFullPathToImages = AppDomain.CurrentDomain.BaseDirectory + "/TeamShips/" + clsCurrentTeamShips.strPathToImages + "/" + Path.GetFileNameWithoutExtension(clsCurrentTeamShips.strMasterImage) + "" + Path.GetExtension(clsCurrentTeamShips.strMasterImage);
                }

                StrBQuestion.AppendLine(iCount + ")  Full Name: " + strName);
                StrBQuestion.AppendLine("<br />");
                StrBQuestion.AppendLine("Job Title: " + strTitle);
                StrBQuestion.AppendLine("<br />");
                StrBQuestion.AppendLine("<img alt='' src='" + strFullPathToImages + "' />");
                StrBQuestion.AppendLine("<br />");
                StrBQuestion.AppendLine("<br />");

                int iCurrentUserAnswerID = clsCommonFunctions.returnRecordID("iUserAnswerID", "tblUserAnswers", "iTeamshipID=" + iTeamshipID + " AND bIsDeleted=0"); //" AND iAccountUserID=" + clsAccountUsers.iAccountUserID + 

                if (iCurrentUserAnswerID != 0)
                {
                    clsUserAnswers clsUserAnswers = new clsUserAnswers(iCurrentUserAnswerID);
                    StrBQuestion.AppendLine(clsUserAnswers.strAnswer);
                }

                strMissionQuestionAndAnswer = StrBQuestion.ToString();
            }

        }
        return strMissionQuestionAndAnswer;
    }

    protected void popPreviouslyAnsweredQuestions(int iCurrentMissionID)
    {
        //if (rpMisionQuestions.Controls.Count > 0)
        //{
        //    foreach (Control ctr in Page.Controls)
        //    {
        //        if (ctr is TextBox)
        //        {
        //            TextBox t = (TextBox)ctr;
        //            t.BackColor = Color.AliceBlue;
        //            t.ReadOnly = false;
        //        }
        //    }

        //    foreach (Control item in rpMisionQuestions.Controls)
        //    {
        //        foreach (Control c in item.Controls)
        //        {
        //            if (c is TextBox)
        //            {
        //                TextBox txt = new TextBox();
        //                txt = (TextBox)c.FindControl("txtQuestionAnswer");
        //                string strAnswerFromUser = txt.Text.ToString();
        //                string strMisionQuestionID = txt.Attributes["CommandArgument"].ToString();
        //                int iCurrentMissionQuestionID = Convert.ToInt32(strMisionQuestionID);

        //                int iCurrentUserAnswerID = clsCommonFunctions.returnRecordID("iUserAnswerID", "tblUserAnswers", "iMissionQuestionID=" + iCurrentMissionQuestionID + " AND iAccountUserID=" + clsAccountUsers.iAccountUserID + " AND bIsDeleted=0");

        //                if (iCurrentUserAnswerID != 0)
        //                {
        //                    clsUserAnswers clsUserAnswers = new clsUserAnswers(iCurrentUserAnswerID);
        //                    txt.Text = clsUserAnswers.strAnswer;
        //                }
        //            }
        //        }
        //    }
        //}
    }

    #endregion

    #region EMAIL METHODS

    protected void SendReport(string strEmailAddress)
    {
        //if (clsCommonFunctions.DoesRecordExist("tblUsers", "strEmailAddress = '" + strEmailAddress + "' AND bIsDeleted = 0") == true)
        {
            //### Settings
            string strReportName = "Q and A Report";
            bool isThereUsersInThisGroup = false;
            StringBuilder strbMailBuilder = new StringBuilder();

            if (rbUser.Checked)
            {
                int iUserAccountID = Convert.ToInt32(lstAccountUser.SelectedValue);
                if (iUserAccountID != 0)
                {
                    clsAccountUsers = new clsAccountUsers(iUserAccountID);
                    int iGroupID = clsAccountUsers.iGroupID;

                    strbMailBuilder.AppendLine("<!-- Start of textbanner -->");
                    strbMailBuilder.AppendLine("<table width='100%' bgcolor='#efefef' cellpadding='0' cellspacing='0' border='0' id='backgroundTable' movable=''>");
                    strbMailBuilder.AppendLine("<tbody>");
                    strbMailBuilder.AppendLine("<tr>");
                    strbMailBuilder.AppendLine("<td>");
                    strbMailBuilder.AppendLine("<table bgcolor='#ffffff' width='650' cellpadding='0' cellspacing='0' border='0' align='center' class='devicewidth' options=''>"); //style='border-left:1px solid #333; border-right:1px solid #333;'
                    strbMailBuilder.AppendLine("<tbody>");
                    strbMailBuilder.AppendLine("<!-- Spacing -->");
                    strbMailBuilder.AppendLine("<tr>");
                    strbMailBuilder.AppendLine("<td height='50'></td>");
                    strbMailBuilder.AppendLine("</tr>");
                    strbMailBuilder.AppendLine("<!-- End of Spacing -->");
                    strbMailBuilder.AppendLine("<tr>");
                    strbMailBuilder.AppendLine("<td>");
                    strbMailBuilder.AppendLine("<table width='100%' cellspacing='0' cellpadding='0'>");
                    strbMailBuilder.AppendLine("<tbody>");

                    strbMailBuilder.AppendLine("<!-- Content -->");
                    strbMailBuilder.AppendLine("<tr>");
                    strbMailBuilder.AppendLine("<td width='60'></td>");
                    strbMailBuilder.AppendLine("<td valign='top' style='font-family: Helvetica, Arial, sans-serif;font-size: 14px; color: #919191; text-align:left;line-height: 22px;' text=''>");
                    strbMailBuilder.AppendLine("Dear Admin<br /><br />");
                    strbMailBuilder.AppendLine("Username: " + clsAccountUsers.strFirstName + " " + clsAccountUsers.strSurname + "<br /><br/>");
                    strbMailBuilder.AppendLine("Email Address: " + clsAccountUsers.strEmailAddress + "<br/>");
                    clsGroups clsGroups = new clsGroups(clsAccountUsers.iGroupID);
                    strbMailBuilder.AppendLine("Group: " + clsGroups.strTitle + "<br/>");
                    strbMailBuilder.AppendLine("<br/>");
                    strbMailBuilder.AppendLine("<br/>");

                    DataTable dtGroupPhases = clsGroupPhases.GetGroupPhasesList("iGroupID=" + iGroupID, "");
                    int iPhaseID = 0;

                    foreach (DataRow dtrGroupPhasesList in dtGroupPhases.Rows)
                    {
                        iPhaseID = Convert.ToInt32(dtrGroupPhasesList["iPhaseID"]);

                        DataTable dtPhaseMissions = clsPhaseMissions.GetPhaseMissionsList("iPhaseID=" + iPhaseID, "");

                        //break;
                        int iMissionID = 0;
                        foreach (DataRow dtrPhaseMissionsList in dtPhaseMissions.Rows)
                        {
                            iMissionID = Convert.ToInt32(dtrPhaseMissionsList["iMissionID"]);

                            clsMissions clsMissions = new clsMissions(iMissionID);
                            int iMissionTypeID = clsMissions.iMissionTypeID;

                            strbMailBuilder.AppendLine("MISSION: " + clsMissions.strTitle + "<br />");

                            strbMailBuilder.AppendLine("" + clsMissions.strDescription + "<br/><br/>");
                            strbMailBuilder.AppendLine("" + clsMissions.strTagLine + "<br/>");
                            //strbMailBuilder.AppendLine("Points:" + clsMissions.iBonusPoints.ToString() + "<br/>");                        

                            strbMailBuilder.AppendLine("<br/>");
                            strbMailBuilder.AppendLine("<br/>");

                            strbMailBuilder.AppendLine(getMissionQuestionAndAnswer(iMissionID, iMissionTypeID));
                        }

                        strbMailBuilder.AppendLine("</td>");
                        strbMailBuilder.AppendLine("<td width='60'></td>");
                        strbMailBuilder.AppendLine("</tr>");
                        strbMailBuilder.AppendLine("<!-- End of Content -->");

                        strbMailBuilder.AppendLine("</td>");
                        strbMailBuilder.AppendLine("<td width='20'></td>");
                        strbMailBuilder.AppendLine("</tr>");
                        strbMailBuilder.AppendLine("</tbody>");
                        strbMailBuilder.AppendLine("</table>");
                        strbMailBuilder.AppendLine("</td>");
                        strbMailBuilder.AppendLine("</tr>");
                        strbMailBuilder.AppendLine("<!-- Spacing -->");
                        strbMailBuilder.AppendLine("<tr>");
                        strbMailBuilder.AppendLine("<td height='50'></td>");
                        strbMailBuilder.AppendLine("</tr>");
                        strbMailBuilder.AppendLine("<!-- End of Spacing -->");
                        strbMailBuilder.AppendLine("</tbody>");
                        strbMailBuilder.AppendLine("</table>");
                        strbMailBuilder.AppendLine("</td>");
                        strbMailBuilder.AppendLine("</tr>");
                        strbMailBuilder.AppendLine("</tbody>");
                        strbMailBuilder.AppendLine("</table>");
                        strbMailBuilder.AppendLine("<!-- End of textbanner -->");
                    }


                    Attachment[] empty = new Attachment[] { };

                    try
                    {
                        notificationEmail.SendMail("jamie@softservedigital.co.za", strEmailAddress, "", "jamie@softservedigital.co.za", strReportName, strbMailBuilder.ToString(), "", "", empty, true);
                       // notificationEmail.SendMail("no-reply@raonboarding.co.za", strEmailAddress, "", "andrew@softservedigital.co.za", strReportName, strbMailBuilder.ToString(), "", "", empty, true);
                        mandatoryDiv.Visible = false;

                        mandatoryDiv2.Visible = true;
                        lblValidationMessage2.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceHappy.png\" alt='' title=''/><div class=\"validationMessage\">The report has been sent</div></div>";
                    }
                    catch { }
                }
                else
                {
                    mandatoryDiv.Visible = true;
                    mandatoryDiv2.Visible = false;
                    lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">Please make sure you select a User</div></div>";
                }
                //### Redirect

            }
            else if (rbGroup.Checked)
            {
                int iGroupID = Convert.ToInt32(lstGroup.SelectedValue);
                if (iGroupID != 0)
                {
                    clsGroups clsGroups = new clsGroups(iGroupID);

                    DataTable dtUserList = clsAccountUsers.GetAccountUsersList("iGroupID=" + iGroupID, "");

                    foreach (DataRow dtrGroupList in dtUserList.Rows)
                    {
                        clsAccountUsers = new clsAccountUsers(Convert.ToInt32(dtrGroupList["iAccountUserID"].ToString()));
                       

                        strbMailBuilder.AppendLine("<!-- Start of textbanner -->");
                        strbMailBuilder.AppendLine("<table width='100%' bgcolor='#efefef' cellpadding='0' cellspacing='0' border='0' id='backgroundTable' movable=''>");
                        strbMailBuilder.AppendLine("<tbody>");
                        strbMailBuilder.AppendLine("<tr>");
                        strbMailBuilder.AppendLine("<td>");
                        strbMailBuilder.AppendLine("<table bgcolor='#ffffff' width='650' cellpadding='0' cellspacing='0' border='0' align='center' class='devicewidth' options=''>"); //style='border-left:1px solid #333; border-right:1px solid #333;'
                        strbMailBuilder.AppendLine("<tbody>");
                        strbMailBuilder.AppendLine("<!-- Spacing -->");
                        strbMailBuilder.AppendLine("<tr>");
                        strbMailBuilder.AppendLine("<td height='50'></td>");
                        strbMailBuilder.AppendLine("</tr>");
                        strbMailBuilder.AppendLine("<!-- End of Spacing -->");
                        strbMailBuilder.AppendLine("<tr>");
                        strbMailBuilder.AppendLine("<td>");
                        strbMailBuilder.AppendLine("<table width='100%' cellspacing='0' cellpadding='0'>");
                        strbMailBuilder.AppendLine("<tbody>");

                        strbMailBuilder.AppendLine("<!-- Content -->");
                        strbMailBuilder.AppendLine("<tr>");
                        strbMailBuilder.AppendLine("<td width='60'></td>");
                        strbMailBuilder.AppendLine("<td valign='top' style='font-family: Helvetica, Arial, sans-serif;font-size: 14px; color: #919191; text-align:left;line-height: 22px;' text=''>");
                        strbMailBuilder.AppendLine("Dear Admin<br /><br />");
                        strbMailBuilder.AppendLine("Username: " + clsAccountUsers.strFirstName + " " + clsAccountUsers.strSurname + "<br /><br />");
                        strbMailBuilder.AppendLine("Questions and Answers<br/>");
                        strbMailBuilder.AppendLine("<br/>");
                        strbMailBuilder.AppendLine("<br/>");
                       

                        DataTable dtGroupPhases = clsGroupPhases.GetGroupPhasesList("iGroupID=" + iGroupID, "");
                        int iPhaseID = 0;

                        foreach (DataRow dtrGroupPhasesList in dtGroupPhases.Rows)
                        {
                            iPhaseID = Convert.ToInt32(dtrGroupPhasesList["iPhaseID"]);

                            DataTable dtPhaseMissions = clsPhaseMissions.GetPhaseMissionsList("iPhaseID=" + iPhaseID, "");

                            //break;
                            int iMissionID = 0;
                            foreach (DataRow dtrPhaseMissionsList in dtPhaseMissions.Rows)
                            {
                                iMissionID = Convert.ToInt32(dtrPhaseMissionsList["iMissionID"]);

                                clsMissions clsMissions = new clsMissions(iMissionID);
                                int iMissionTypeID = clsMissions.iMissionTypeID;

                                strbMailBuilder.AppendLine(getMissionQuestionAndAnswer(iMissionID, iMissionTypeID));
                            }

                            strbMailBuilder.AppendLine("</td>");
                            strbMailBuilder.AppendLine("<td width='60'></td>");
                            strbMailBuilder.AppendLine("</tr>");
                            strbMailBuilder.AppendLine("<!-- End of Content -->");

                            strbMailBuilder.AppendLine("</td>");
                            strbMailBuilder.AppendLine("<td width='20'></td>");
                            strbMailBuilder.AppendLine("</tr>");
                            strbMailBuilder.AppendLine("</tbody>");
                            strbMailBuilder.AppendLine("</table>");
                            strbMailBuilder.AppendLine("</td>");
                            strbMailBuilder.AppendLine("</tr>");
                            strbMailBuilder.AppendLine("<!-- Spacing -->");
                            strbMailBuilder.AppendLine("<tr>");
                            strbMailBuilder.AppendLine("<td height='50'></td>");
                            strbMailBuilder.AppendLine("</tr>");
                            strbMailBuilder.AppendLine("<!-- End of Spacing -->");
                            strbMailBuilder.AppendLine("</tbody>");
                            strbMailBuilder.AppendLine("</table>");
                            strbMailBuilder.AppendLine("</td>");
                            strbMailBuilder.AppendLine("</tr>");
                            strbMailBuilder.AppendLine("</tbody>");
                            strbMailBuilder.AppendLine("</table>");
                            strbMailBuilder.AppendLine("<!-- End of textbanner -->");
                        }
                        try
                        {
                            clsAccountUsers = new clsAccountUsers(iGroupID);
                            isThereUsersInThisGroup = true;
                        }
                        catch
                        {
                            isThereUsersInThisGroup = false;
                        }

                    }
                    if (isThereUsersInThisGroup == true)
                    {
                        Attachment[] empty = new Attachment[] { };

                        try
                        {
                            notificationEmail.SendMail("no-reply@raonboarding.co.za", strEmailAddress, "", "andrew@softservedigital.co.za", strReportName, strbMailBuilder.ToString(), "Heading", "Subheading", empty, true);
                            mandatoryDiv.Visible = false; ;
                            mandatoryDiv2.Visible = true;
                            lblValidationMessage2.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceHappy.png\" alt='' title=''/><div class=\"validationMessage\">The report has been sent</div></div>";
                        }
                        catch { }
                    }
                    else
                    {
                        mandatoryDiv.Visible = true;
                        mandatoryDiv2.Visible = false;
                        lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">There is no user(s) in this group</div></div>";
                    }
                }
                else
                {
                    mandatoryDiv.Visible = true;
                    mandatoryDiv2.Visible = false;
                    lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">Please make sure you select a Group</div></div>";

                }
            }

            else
            {
                mandatoryDiv.Visible = true;
                mandatoryDiv2.Visible = false;
                lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">Please make sure you select a radio button</div></div>";
            }

            //DataConnection.GetDataObject().ExecuteScalar("spForgottenPassword '" + txtUsername.Text + "', '" + strRandomPassword + "'");
            //emailComponent.SendMail("noreply@gobundu.co.za", txtUsername.Text, "", "", "Forgotten Password", strContent, true);
        }

        //lblValidationMessage.Text = "<div class=\"validationImageIncorrectLogin\"></div><div class=\"validationLabel\">There is no such user on the system.</div>";
    }

    #endregion
}