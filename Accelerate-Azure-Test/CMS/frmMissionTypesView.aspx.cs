
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text;

/// <summary>
/// Summary description for clsMissionTypesView
/// </summary>
public partial class CMS_clsMissionTypesView : System.Web.UI.Page
{
    #region CLASSES AND VARIABLES

    clsUsers clsUsers;
    DataTable dtMissionTypesList;

    List<clsMissionTypes> glstMissionTypes;

    #endregion

    #region EVENT MODULES

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

dgrGrid.ItemCreated += new DataGridItemEventHandler(clsCommonFunctions.dgrGrid_PagerPopulate);

        if (!Page.IsPostBack)
        {
            Session["dtMissionTypesList"] = null;
            PopulateFormData();
        }
        else
        {
            if (Session["glstMissionTypes"] == null)
            {
                glstMissionTypes = new List<clsMissionTypes>();
                Session["glstMissionTypes"] = glstMissionTypes;
            }
            else
                glstMissionTypes = (List<clsMissionTypes>)Session["glstMissionTypes"];
        }
            PopulateFormData();

    }

    protected string EscapeLikeValue(string valueWithoutWildcards)
    {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < valueWithoutWildcards.Length; i++)
        {
            char c = valueWithoutWildcards[i];
            if (c == '*' || c == '%' || c == '[' || c == ']')
                sb.Append("[").Append(c).Append("]");
            else if (c == '\'')
                sb.Append("''");
            else
                sb.Append(c);
        }
        return sb.ToString();
    }

    protected void lnkbtnSearch_Click(object sender, EventArgs e)
    {
        string EscapedString = EscapeLikeValue(txtSearch.Text.ToString());
        string FilterExpression ="(strTitle) LIKE '%" + EscapedString + "%'";
        DataTable dtMissionTypesList = clsMissionTypes.GetMissionTypesList(FilterExpression, "");

        Session["dtMissionTypesList"] = dtMissionTypesList;

        PopulateFormData();
    }

    protected void lnkbtnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmMissionTypesAddModify.aspx");
    }

    #endregion

    #region DATA GRID METHODS

    private void PopulateFormData()
    {
        //### Populate datagrid using clsMissionTypesList object
        try
        {
            dtMissionTypesList = new DataTable();

            if (Session["dtMissionTypesList"] == null)
                dtMissionTypesList = clsMissionTypes.GetMissionTypesList();
            else
                dtMissionTypesList = (DataTable)Session["dtMissionTypesList"];

            dgrGrid.DataSource = dtMissionTypesList;

            //### Add this check to trap if the last item on the page is deleted
            if (Convert.ToInt16(dgrGrid.CurrentPageIndex) < Convert.ToInt16(Session["MissionTypesView_CurrentPageIndex"]))
            {
                dgrGrid.CurrentPageIndex = 0;
            }
            else
            {
                if (Session["MissionTypesView_CurrentPageIndex"] != null)
                {
                    dgrGrid.CurrentPageIndex = (int)Session["MissionTypesView_CurrentPageIndex"];
                }
            }

            dgrGrid.DataBind();

        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void lnkDeleteItem_Click(object sender, EventArgs e)
    {
        int iMissionTypeID = int.Parse((sender as LinkButton).CommandArgument);

        clsMissionTypes.Delete(iMissionTypeID);

        DataTable dtMissionTypesList = new DataTable();

        if (((txtSearch.Text == "SEARCH FOR A MISSION TYPE") == false) && ((txtSearch.Text == "") == false))
        {
            string FilterExpression = "(strTitle) LIKE '%" + txtSearch.Text + "%'";
            dtMissionTypesList = clsMissionTypes.GetMissionTypesList(FilterExpression, "");
        }
        else
        {
            dtMissionTypesList = clsMissionTypes.GetMissionTypesList();
        }

        Session["dtMissionTypesList"] = dtMissionTypesList;

        PopulateFormData();
    }

    protected void dgrGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //### Method is used when the page index is changed.
        try
        {
            Session["MissionTypesView_CurrentPageIndex"] = e.NewPageIndex;
            dgrGrid.CurrentPageIndex = e.NewPageIndex;
            PopulateFormData();
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgrGrid_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        //### Sort Method
        dtMissionTypesList = new DataTable();

        if (Session["dtMissionTypesList"] == null)
            dtMissionTypesList = clsMissionTypes.GetMissionTypesList();
        else
            dtMissionTypesList = (DataTable)Session["dtMissionTypesList"];

        DataView dvTemp = dtMissionTypesList.DefaultView;

        dvTemp.Sort = e.SortExpression;
        dtMissionTypesList = dvTemp.ToTable();
        Session["dtMissionTypesList"] = dtMissionTypesList;

        PopulateFormData();
    }

    public void dgrGrid_PopEditLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkEditItem = (HyperLink)sender;
            DataGridItem dgrItemEdit = (DataGridItem)lnkEditItem.Parent.Parent;

            //### Get the iMissionTypeID
            int iMissionTypeID = 0;
            iMissionTypeID = int.Parse(dgrItemEdit.Cells[0].Text);

            //### Add attributes to edit link
            lnkEditItem.Attributes.Add("href", "frmMissionTypesAddModify.aspx?action=edit&iMissionTypeID=" + iMissionTypeID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

     public void dgrGrid_PopDeleteLink(object sender, EventArgs e)
     {
          try
          {
             //### Add hyperlink to datagrid item
             HyperLink lnkDeleteItem = (HyperLink)sender;
             DataGridItem dgrItemDelete = (DataGridItem)lnkDeleteItem.Parent.Parent;

             //### Get the iMissionTypeID
             int iMissionTypeID = 0;
             iMissionTypeID = int.Parse(dgrItemDelete.Cells[0].Text);

             //### Add attributes to delete link
             lnkDeleteItem.CssClass = "dgrDeleteLink";
             lnkDeleteItem.Attributes.Add("href", "frmMissionTypesView.aspx?action=delete&iMissionTypeID=" + iMissionTypeID);
           }
           catch (Exception ex)
           {
                 Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
           }
     }

    #endregion

    #region Titles FILTERING

    /// <summary>
    /// Enables the AutoCompleteExtender
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    /// 
    [WebMethod]
    [ScriptMethod]
    public static string[] FilterBusinessRule(string prefixText, int count)
    {
        string[] strReturnList = new string[] { };

        string FilterExpression = "(strTitle) LIKE '%" + prefixText + "%'";
        DataTable dtMissionTypes = clsMissionTypes.GetMissionTypesList(FilterExpression, "");
        List<string> glstMissionTypes = new List<string>();

        if (dtMissionTypes.Rows.Count > 0)
        {
            foreach (DataRow dtrMissionTypes in dtMissionTypes.Rows)
            {
                glstMissionTypes.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem("" + dtrMissionTypes["strTitle"].ToString(), dtrMissionTypes["iMissionTypeID"].ToString()));
            }
        }
        else
            glstMissionTypes.Add("No MissionTypes Available.");
        strReturnList = glstMissionTypes.ToArray();
        return strReturnList;
    }

    #endregion
}
