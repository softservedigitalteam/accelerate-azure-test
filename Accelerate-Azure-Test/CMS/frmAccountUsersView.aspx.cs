
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;
using SD = System.Drawing;
using System.Drawing.Drawing2D;
using System.Text;

/// <summary>
/// Summary description for clsAccountUsersView
/// </summary>
public partial class CMS_clsAccountUsersView : System.Web.UI.Page
{
    #region CLASSES AND VARIABLES

    clsUsers clsUsers;
    DataTable dtAccountUsersList;
    List<clsAccountUsers> glstAccountUsers;

    #endregion

    #region EVENT MODULES

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

        dgrGrid.ItemCreated += new DataGridItemEventHandler(clsCommonFunctions.dgrGrid_PagerPopulate);

        if (!Page.IsPostBack)
        {
            Session["dtAccountUsersList"] = null;
            PopulateFormData();
            if (Session["iNumberOfActiveUsers"] != null && Convert.ToInt32(Session["iNumberOfActiveUsers"]) != 0)
            {
                int iNumberOfActiveUsers = Convert.ToInt32(Session["iNumberOfActiveUsers"]);
                Response.Write(clsCommonFunctions.JSAlert(iNumberOfActiveUsers.ToString() + " Users Added"));
                Session["iNumberOfActiveUsers"] = null;
            }
        }
        //else
        //{
        //    if (Session["glstAccountUsers"] == null)
        //    {
        //        glstAccountUsers = new List<clsAccountUsers>();
        //        Session["glstAccountUsers"] = glstAccountUsers;
        //    }
        //    else
        //        glstAccountUsers = (List<clsAccountUsers>)Session["glstAccountUsers"];
        //}

        PopulateFormData();
    }

    protected string EscapeLikeValue(string valueWithoutWildcards)
    {
        StringBuilder sb = new StringBuilder();
 
        for (int i = 0; i < valueWithoutWildcards.Length; i++)
        {
            char c = valueWithoutWildcards[i];
            if (c == '*' || c == '%' || c == '[' || c == ']')
                sb.Append("[").Append(c).Append("]");
            else if (c == '\'')
                sb.Append("''");
            else
                sb.Append(c);
        }
        return sb.ToString();
    }

    protected void lnkbtnSearch_Click(object sender, EventArgs e)
    {
        string EscapedString = EscapeLikeValue(txtSearch.Text.ToString());
        string FilterExpression ="(strFirstName +' '+ strSurname +' '+ strEmailAddress) LIKE '%" + txtSearch.Text + "%'";
        DataTable dtAccountUsersList = clsAccountUsers.GetAccountUsersList(FilterExpression, "");

        Session["dtAccountUsersList"] = dtAccountUsersList;

        PopulateFormData();
    }

    protected void lnkbtnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmAccountUsersAddModify.aspx");
    }

    protected void lnkbtnImport_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmImportActiveAccountUsers.aspx");
    }

    #endregion

    #region DATA GRID METHODS

    private void PopulateFormData()
    {
        //### Populate datagrid using clsAccountUsersList object
        try
        {
            dtAccountUsersList = new DataTable();


            if (Session["dtAccountUsersList"] == null)
            {
                //dtAccountUsersList = clsAccountUsers.GetAccountUsersListWithGroupName();
                dtAccountUsersList = clsAccountUsers.GetAccountUsersListWithGroupName("dtAdded DESC");
                dtAccountUsersList.Columns.Add("bHasCompletedAllMissions");
            }
            else
            {
                dtAccountUsersList = (DataTable)Session["dtAccountUsersList"];
                if (dtAccountUsersList.Columns.Contains("bHasCompletedAllMissions") == false)
                {
                    dtAccountUsersList.Columns.Add("bHasCompletedAllMissions");
                }
            }

            foreach (DataRow dtrAccountUsersList in dtAccountUsersList.Rows)
            {
                int iAccountUserID;
                iAccountUserID = Convert.ToInt32(dtrAccountUsersList["iAccountUserID"]);
                //clsAccountUsers clsAccountUser = new clsAccountUsers(iAccountUserID);
                clsAccountUserTrackings clsAccountUserTrackings = new clsAccountUserTrackings(iAccountUserID);
                if (clsAccountUserTrackings.bIsPhase3Complete == true)
                {
                    //dtrAccountUsersList["bHasEmailBeenSent"] = Convert.ToBoolean(clsAccountUserTrackings.bHasEmailBeenSent);
                    dtrAccountUsersList["bHasCompletedAllMissions"] = true;
                }
                else
                {
                    dtrAccountUsersList["bHasCompletedAllMissions"] = false;
                }
            }

            dgrGrid.DataSource = dtAccountUsersList;

            //### Add this check to trap if the last item on the page is deleted
            if (Convert.ToInt16(dgrGrid.CurrentPageIndex) < Convert.ToInt16(Session["AccountUsersView_CurrentPageIndex"]))
            {
                dgrGrid.CurrentPageIndex = 0;
            }
            else
            {
                if (Session["AccountUsersView_CurrentPageIndex"] != null)
                {
                    dgrGrid.CurrentPageIndex = (int)Session["AccountUsersView_CurrentPageIndex"];
                }
            }

            dgrGrid.DataBind();

        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void lnkDeleteItem_Click(object sender, EventArgs e)
    {
        int iAccountUserID = int.Parse((sender as LinkButton).CommandArgument);
        clsAccountUsers.Delete(iAccountUserID);
        //Session["dtAccountUsersList"] = null;

        DataTable dtAccountUsersList = new DataTable();

        if (((txtSearch.Text == "SEARCH FOR AN ACCOUNT USER") == false) && ((txtSearch.Text == "") == false))
        {
            string FilterExpression = "(strFirstName +' '+ strSurname +' '+ strEmailAddress) LIKE '%" + txtSearch.Text + "%'";
            dtAccountUsersList = clsAccountUsers.GetAccountUsersList(FilterExpression, "");
        }
        else
        {
            dtAccountUsersList = clsAccountUsers.GetAccountUsersListWithGroupName("dtAdded DESC");
        }

        Session["dtAccountUsersList"] = dtAccountUsersList;
        PopulateFormData();
    }

    protected void dgrGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //### Method is used when the page index is changed.
        try
        {
            Session["AccountUsersView_CurrentPageIndex"] = e.NewPageIndex;
            dgrGrid.CurrentPageIndex = e.NewPageIndex;
            PopulateFormData();
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgrGrid_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        //### Sort Method
        dtAccountUsersList = new DataTable();

        if (Session["dtAccountUsersList"] == null)
            dtAccountUsersList = clsAccountUsers.GetAccountUsersList();
        else
            dtAccountUsersList = (DataTable)Session["dtAccountUsersList"];

        DataView dvTemp = dtAccountUsersList.DefaultView;

        dvTemp.Sort = e.SortExpression;
        dtAccountUsersList = dvTemp.ToTable();
        Session["dtAccountUsersList"] = dtAccountUsersList;

        PopulateFormData();
    }

    public void dgrGrid_PopEditPassLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkEditPass = (HyperLink)sender;
            DataGridItem dgrItemEditPass = (DataGridItem)lnkEditPass.Parent.Parent;

            //### Get the iUserID
            int iUserID = 0;
            iUserID = int.Parse(dgrItemEditPass.Cells[0].Text);

            //### Add attributes to edit link
            lnkEditPass.Attributes.Add("href", "frmAccountUsersPasswordEdit.aspx?iUserID=" + iUserID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    public void dgrGrid_PopEditLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkEditItem = (HyperLink)sender;
            DataGridItem dgrItemEdit = (DataGridItem)lnkEditItem.Parent.Parent;

            //### Get the iAccountUserID
            int iAccountUserID = 0;
            iAccountUserID = int.Parse(dgrItemEdit.Cells[0].Text);

            //### Add attributes to edit link
            lnkEditItem.Attributes.Add("href", "frmAccountUsersAddModify.aspx?action=edit&iAccountUserID=" + iAccountUserID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

     public void dgrGrid_PopDeleteLink(object sender, EventArgs e)
     {
          try
          {
             //### Add hyperlink to datagrid item
             HyperLink lnkDeleteItem = (HyperLink)sender;
             DataGridItem dgrItemDelete = (DataGridItem)lnkDeleteItem.Parent.Parent;

             //### Get the iAccountUserID
             int iAccountUserID = 0;
             iAccountUserID = int.Parse(dgrItemDelete.Cells[0].Text);

             //### Add attributes to delete link
             lnkDeleteItem.CssClass = "dgrDeleteLink";
             lnkDeleteItem.Attributes.Add("href", "frmAccountUsersView.aspx?action=delete&iAccountUserID=" + iAccountUserID);
           }
           catch (Exception ex)
           {
                 Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
           }
     }

    #endregion

    #region AccountUsers FILTERING

    /// <summary>
    /// Enables the AutoCompleteExtender
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    /// 
    [WebMethod]
    [ScriptMethod]
    public static string[] FilterBusinessRule(string prefixText, int count)
    {
        string[] strReturnList = new string[] { };

        string FilterExpression = "(strFirstName +' '+ strSurname +' '+ strEmailAddress) LIKE '%" + prefixText + "%'";
        DataTable dtAccountUsers = clsAccountUsers.GetAccountUsersList(FilterExpression, "");
        List<string> glstAccountUsers = new List<string>();

        if (dtAccountUsers.Rows.Count > 0)
        {
            foreach (DataRow dtrAccountUsers in dtAccountUsers.Rows)
            {
                glstAccountUsers.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem("" + dtrAccountUsers["strFirstName"].ToString() +' '+"" + dtrAccountUsers["strSurname"].ToString() +' '+"" + dtrAccountUsers["strEmailAddress"].ToString(), dtrAccountUsers["iAccountUserID"].ToString()));
            }
        }
        else
            glstAccountUsers.Add("No AccountUsers Available.");
        strReturnList = glstAccountUsers.ToArray();
        return strReturnList;
    }

    #endregion
}
