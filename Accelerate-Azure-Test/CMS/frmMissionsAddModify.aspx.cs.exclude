using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class CMS_frmMissionsAddModify : System.Web.UI.Page
{
    clsUsers clsUsers;
    clsMissions clsMissions;
    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

             //### Determines if a javascript delete has been called
             if (!String.IsNullOrEmpty(Page.Request["__EVENTARGUMENT"]) && (Page.Request["__EVENTARGUMENT"].Split(':')[0] == "iRemoveImages"))
             DeleteImages(Convert.ToInt32(Page.Request["__EVENTARGUMENT"].Split(':')[1]));
        if (!IsPostBack)
        {
             popMissionStatus();

            //### If the iMissionID is passed through then we want to instantiate the object with that iMissionID
            if ((Request.QueryString["iMissionID"] != "") && (Request.QueryString["iMissionID"] != null))
            {
                clsMissions = new clsMissions(Convert.ToInt32(Request.QueryString["iMissionID"]));

                //### Populate the form
                popFormData();
            }
            else
            {
                clsMissions = new clsMissions();
            }
            Session["clsMissions"] = clsMissions;
        }
        else
        {
            clsMissions = (clsMissions)Session["clsMissions"];
        }
    }

    protected void lnkbtnBack_Click(object sender, EventArgs e)
    {
        //### Go back to previous page
        Response.Redirect("frmMissionsView.aspx");
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        //### Validate registration process
        bool bCanSave = true;

       bCanSave = clsValidation.IsNullOrEmpty(lstMissionStatus, bCanSave);
       bCanSave = clsValidation.IsNullOrEmpty(txtBonusPoints, bCanSave);
       bCanSave = clsValidation.IsNullOrEmpty(txtTitle, bCanSave);
       bCanSave = clsValidation.IsNullOrEmpty(txtDescription, bCanSave);

        if (bCanSave == true)
        {
            mandatoryDiv.Visible = false;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceHappy.png\" alt='' title=''/><div class=\"validationMessage\">Mission added successfully</div></div>";
            SaveData();
        }
        else
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">Please fill out all mandatory fields - Mission not added</div></div>";
        }
    }

    protected void lnkbtnClear_Click(object sender, EventArgs e)
    {
        mandatoryDiv.Visible = false;

        lstMissionStatus.SelectedValue = "0";
        clsValidation.SetValid(lstMissionStatus);
        
        txtTagLine.Text = "";
        clsValidation.SetValid(txtTagLine);
        txtBonusPoints.Text = "";
        clsValidation.SetValid(txtBonusPoints);
        txtTitle.Text = "";
        clsValidation.SetValid(txtTitle);
        txtDescription.Text = "";
        clsValidation.SetValid(txtDescription);
    }

    #endregion 

    #region POPULATE DATA METHODS

    private void popFormData()
    {
         lstMissionStatus.SelectedValue = clsMissions.iMissionStatusID.ToString();
         txtBonusPoints.Text = clsMissions.iBonusPoints.ToString();
         txtTitle.Text = clsMissions.strTitle;
         txtTagLine.Text = clsMissions.strTagLine;

         //### Populates images
         if (!string.IsNullOrEmpty(clsMissions.strPathToImages))
         {
             lblUniquePath.Text = clsMissions.strPathToImages;
             getList(clsMissions.strPathToImages);
             //### Set Current Master Image
             List<string> lstImagesFileNames = (List<string>)Session["lstImagesFileNames"];
             try
             {
                 foreach (string strImageFileName in lstImagesFileNames)
                 {
                     if (strImageFileName == clsMissions.strMasterImage)
                     {
                         RadioButton rdbMasterImage = dlImages.Items[lstImagesFileNames.IndexOf(strImageFileName)].FindControl("rdbMainImage") as RadioButton;
                         rdbMasterImage.Checked = true;
                         break;
                     }
                 }
             }
             catch { }
         }

         //### Populates images
         if (!string.IsNullOrEmpty(clsMissions.strPathToImages))
         {
             lblUniquePathBadges.Text = clsMissions.strPathToBadges;
             getListBadges(clsMissions.strPathToBadges);
             //### Set Current Master Image
             List<string> lstBadgesFileNames = (List<string>)Session["lstBadgesFileNames"];
             try
             {
                 foreach (string strImageFileName in lstBadgesFileNames)
                 {
                     if (strImageFileName == clsMissions.strMasterBadgeImage)
                     {
                         RadioButton rdbMasterImage2 = dlBadges.Items[lstBadgesFileNames.IndexOf(strImageFileName)].FindControl("rdbMainImage") as RadioButton;
                         rdbMasterImage2.Checked = true;
                         break;
                     }
                 }
             }
             catch { }
         }
         txtDescription.Text = clsMissions.strDescription;
    }
    
    private void popMissionStatus()
    {
         DataTable dtMissionStatussList = new DataTable();
         lstMissionStatus.DataSource = clsMissionStatuss.GetMissionStatussList();

         //### Populates the drop down list with PK and TITLE;
         lstMissionStatus.DataValueField = "iMissionStatusID";
         lstMissionStatus.DataTextField = "strTitle";

         //### Bind the data to the list;
         lstMissionStatus.DataBind();

         //### Add default select option;
         lstMissionStatus.Items.Insert(0, new ListItem("--Not Selected--", "0"));
    }
    
    #endregion

    #region SAVE DATA METHODS

    private void SaveData()
    {
        //### Add / Update
        clsMissions.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsMissions.iAddedBy = clsUsers.iUserID;
        clsMissions.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsMissions.iEditedBy = clsUsers.iUserID;
        clsMissions.iMissionStatusID = Convert.ToInt32(lstMissionStatus.SelectedValue.ToString());
        clsMissions.iBonusPoints = Convert.ToInt32(txtBonusPoints.Text);
        clsMissions.strTitle = txtTitle.Text;
        clsMissions.strTagLine = txtTagLine.Text;

        //### Images related items
        clsMissions.strPathToImages = lblUniquePath.Text;
        clsMissions.strMasterImage = GetMainImagePath(dlImages);

        //### Images related items
        clsMissions.strPathToBadges = lblUniquePathBadges.Text;
        clsMissions.strMasterBadgeImage = GetMainBadgePath(dlBadges);
        clsMissions.strDescription = txtDescription.Text;

        clsMissions.Update();

        Session["dtMissionsList"] = null;

        //### Go back to view page
        Response.Redirect("frmMissionsView.aspx");
    }

    #endregion

    #region IMAGE METHODS

    List<string> lstImages;
    List<string> lstImagesFileNames;
    int iMaxImages = 1;
    string strUniqueFullPath = AppDomain.CurrentDomain.BaseDirectory + "\\Missions";
    protected void btnUpload_Click(object sender, EventArgs e)
    {
        if (Session["lstImages"] == null)
        {
            lstImages = new List<string>();
            Session["lstImages"] = lstImages;
        }
        //### Check that they have ONLY HAVE MAX NUMBER OF Images in the datalist
        if (dlImages.Items.Count == iMaxImages)
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "You can only have " + iMaxImages.ToString() + " Images.";
            getList(lblUniquePath.Text);
        }
        else
        {
            mandatoryDiv.Visible = false;

            string strUniquePath;
            if (lblUniquePath.Text == "")
            {
                strUniquePath = GetUniquePath();
                lblUniquePath.Text = strUniquePath;
            }
            else
            {
                strUniquePath = lblUniquePath.Text;
            }
            UploadImages(strUniquePath);
            getList(strUniquePath);
        }
    }
    private string GetUniquePath()
    {
        int iCount = 1;
        //### First we need to get the path
        while (System.IO.Directory.Exists(strUniqueFullPath + "\\Missions" + iCount) == true)
        {
            iCount++;
        }
        return "Missions" + iCount;
    }
    protected void UploadImages(String strUniquePath)
    {
        if (FileUpload.PostedFile.ContentLength > 0 && FileUpload.PostedFile.ContentLength < 1073741824)
        {

            //### Upload files to unique folder
            string strUploadFileName = "";
            strUploadFileName = System.IO.Path.GetFileName(FileUpload.PostedFile.FileName);
            string strSaveLocation = "";
            strSaveLocation = strUniqueFullPath + "\\" + strUniquePath + "\\" + strUploadFileName;

            if (!System.IO.Directory.Exists(strUniqueFullPath + "\\" + strUniquePath))
            {
                System.IO.Directory.CreateDirectory(strUniqueFullPath + "\\" + strUniquePath);
            }
            FileUpload.PostedFile.SaveAs(strSaveLocation);

            CopyAndResizePic(strSaveLocation);
            getList(strUniqueFullPath + "\\" + strUniquePath);
        }
        else
        {
            lblValidationMessage.Text = "The file should be between 0 and 1Mb.";
            lblUploadError.Text = "The file should be between 0 and 1Mb.";
        }
    }
    private void CopyAndResizePic(String strFullPath)
    {
        try
        {

            String strFileName;
            String strNewFilePath;

            //### Main Images
            String strLrgFileName;

            strFileName = Path.GetFileName(strFullPath);
            strNewFilePath = strFullPath.Replace(strFileName, "");
            strNewFilePath = strNewFilePath + Path.GetFileNameWithoutExtension(strFullPath);
            strNewFilePath = strNewFilePath + "_lrg";
            strNewFilePath = strNewFilePath + Path.GetExtension(strFullPath);

            File.Copy(strFullPath, strNewFilePath);
            strLrgFileName = Path.GetFileName(strNewFilePath);

            clsCommonFunctions.ResizeImage(strNewFilePath, strNewFilePath, 600, 600, false);

            //### Thumbnail
            String strSmlFileName;

            strFileName = Path.GetFileName(strFullPath);
            strNewFilePath = strFullPath.Replace(strFileName, "");
            strNewFilePath = strNewFilePath + Path.GetFileNameWithoutExtension(strFullPath);
            strNewFilePath = strNewFilePath + "_sml";
            strNewFilePath = strNewFilePath + Path.GetExtension(strFullPath);

            File.Copy(strFullPath, strNewFilePath);
            strSmlFileName = Path.GetFileName(strNewFilePath);

            clsCommonFunctions.ResizeImage(strNewFilePath, strNewFilePath, 140, 140, false);
            
        }
        catch (Exception ex){}
    }
    public void getList(String strPathToFolder)
    {
        lstImages = new List<string>();
        lstImagesFileNames = new List<string>();
        try
        {
            string strPath = strPathToFolder;
            string[] files = Directory.GetFiles(strUniqueFullPath + "\\" + strPath);

            string iMissionID = "";
            if (!string.IsNullOrEmpty(Request.QueryString["iMissionID"]))
                iMissionID = Request.QueryString["iMissionID"];

            int iImagesCount = 0;

            foreach (string strName in files)
            {
                if (strName.IndexOf("_sml") != -1)
                {
                    string strHTMLImages = strName.Replace(AppDomain.CurrentDomain.BaseDirectory + "\\", "..\\");
                    strHTMLImages = strHTMLImages.Replace("\\", "/");

                    //### Generates a javascript postback for the delete method
                    String strPostBack = Page.ClientScript.GetPostBackEventReference(this, "iRemoveImages:" + iImagesCount);

                    lstImages.Add(@"<tr style='text-align: centre;'>
                                        <td style='text-align: centre;'>
                                            <a class='ImagesColorBox' href='" + strHTMLImages.Replace("_sml", "_lrg") + "'><img src='" + strHTMLImages + @"' alt='' title='' style='border: solid 5px #ffffff;' /></a><br /><br />" +
                                            "<center><div class='buttonStyle' style='padding-top: 5px;'><a href=\"javascript:" + strPostBack + "\" style='color: #ffffff;'><div class='deleteButton'></div></a></div></center>" + @"
                                        </td>
                                    </tr>");
                    lstImagesFileNames.Add(Path.GetFileName(strName).Replace("_sml", ""));
                    iImagesCount++;
                }
            }
            dlImages.DataSource = lstImages;
            dlImages.DataBind();

            Session["lstImages"] = lstImages;
            Session["lstImagesFileNames"] = lstImagesFileNames;
        }
        catch (Exception ex) {}
    }
    private string GetMainImagePath(DataList dtlTarget)
    {
        string strReturn = "";

        foreach (DataListItem dliTarget in dtlTarget.Items)
        {
            RadioButton rdbMainImage = (RadioButton)dliTarget.FindControl("rdbMainImage");
            if (rdbMainImage.Checked)
            {
                lstImagesFileNames = (List<string>)Session["lstImagesFileNames"];
                strReturn = lstImagesFileNames[dliTarget.ItemIndex];
                break;
            }
        }
        return strReturn;
    }
    private void DeleteImages(int iImagesIndex)
    {

        //### Deletes all Images related to the target Images.
        lstImages = (List<string>)Session["lstImages"];
        lstImagesFileNames = (List<string>)Session["lstImagesFileNames"];

        lstImages.RemoveAt(iImagesIndex);
        Session["lstImages"] = lstImages;

        string[] files = Directory.GetFiles(strUniqueFullPath + "\\" + lblUniquePath.Text);
        foreach (string file in files)
        {
            if (Path.GetFileName(file) == lstImagesFileNames[iImagesIndex].ToString())
            {
                //### Remove all Images
                File.Delete(file);
                File.Delete(file.Replace(Path.GetExtension(file), "_sml" + Path.GetExtension(file)));
                File.Delete(file.Replace(Path.GetExtension(file), "_med" + Path.GetExtension(file)));
                File.Delete(file.Replace(Path.GetExtension(file), "_lrg" + Path.GetExtension(file)));
                break;
            }
        }
        lstImagesFileNames.RemoveAt(iImagesIndex);
        ViewState["lstImagesFileNames"] = lstImagesFileNames;
        getList(lblUniquePath.Text);
    }

    #endregion

    #region BADGE METHODS

    List<string> lstBadges;
    List<string> lstBadgesFileNames;
    int iMaxBadges = 1;
    string strUniqueFullPathBadgesBadges = AppDomain.CurrentDomain.BaseDirectory + "\\MissionBadges";

    protected void btnUploadBadges_Click(object sender, EventArgs e)
    {
        if (Session["lstBadges"] == null)
        {
            lstBadges = new List<string>();
            Session["lstBadges"] = lstBadges;
        }

        //### Check that they have ONLY HAVE MAX NUMBER OF Badges in the datalist
        if (dlBadges.Items.Count == iMaxBadges)
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "You can only have " + iMaxBadges.ToString() + " Badges.";
            getListBadges(lblUniquePathBadges.Text);
        }
        else
        {
            mandatoryDiv.Visible = false;

            string strUniquePath;

            if (lblUniquePathBadges.Text == "")
            {
                strUniquePath = GetUniquePathBadges();
                lblUniquePathBadges.Text = strUniquePath;
            }
            else
            {
                strUniquePath = lblUniquePathBadges.Text;
            }
            UploadBadges(strUniquePath);
            getListBadges(strUniquePath);
        }
    }
    private string GetUniquePathBadges()
    {
        int iCount = 1;
        //### First we need to get the path
        while (System.IO.Directory.Exists(strUniqueFullPathBadgesBadges + "\\MissionBadges" + iCount) == true)
        {
            iCount++;
        }
        return "MissionBadges" + iCount;
    }

    protected void UploadBadges(String strUniquePath)
    {
        if (FileUploadBadges.PostedFile.ContentLength > 0 && FileUploadBadges.PostedFile.ContentLength < 1073741824)
        {

            //### Upload files to unique folder
            string strUploadFileName = "";
            strUploadFileName = System.IO.Path.GetFileName(FileUploadBadges.PostedFile.FileName);
            string strSaveLocation = "";
            strSaveLocation = strUniqueFullPathBadgesBadges + "\\" + strUniquePath + "\\" + strUploadFileName;

            if (!System.IO.Directory.Exists(strUniqueFullPathBadgesBadges + "\\" + strUniquePath))
            {
                System.IO.Directory.CreateDirectory(strUniqueFullPathBadgesBadges + "\\" + strUniquePath);
            }
            FileUploadBadges.PostedFile.SaveAs(strSaveLocation);

            CopyAndResizePicBadge(strSaveLocation);
            getListBadges(strUniqueFullPathBadgesBadges + "\\" + strUniquePath);
        }
        else
        {
            lblValidationMessage.Text = "The file should be between 0 and 1Mb.";
            lblUploadErrorBadges.Text = "The file should be between 0 and 1Mb.";
        }
    }

    private void CopyAndResizePicBadge(String strFullPath)
    {
        try
        {

            String strFileName;
            String strNewFilePath;

            //### Main Badges
            String strLrgFileName;

            strFileName = Path.GetFileName(strFullPath);
            strNewFilePath = strFullPath.Replace(strFileName, "");
            strNewFilePath = strNewFilePath + Path.GetFileNameWithoutExtension(strFullPath);
            strNewFilePath = strNewFilePath + "_lrg";
            strNewFilePath = strNewFilePath + Path.GetExtension(strFullPath);

            File.Copy(strFullPath, strNewFilePath);
            strLrgFileName = Path.GetFileName(strNewFilePath);

            clsCommonFunctions.ResizeImage(strNewFilePath, strNewFilePath, 600, 600, false);

            //### Thumbnail
            String strSmlFileName;

            strFileName = Path.GetFileName(strFullPath);
            strNewFilePath = strFullPath.Replace(strFileName, "");
            strNewFilePath = strNewFilePath + Path.GetFileNameWithoutExtension(strFullPath);
            strNewFilePath = strNewFilePath + "_sml";
            strNewFilePath = strNewFilePath + Path.GetExtension(strFullPath);

            File.Copy(strFullPath, strNewFilePath);
            strSmlFileName = Path.GetFileName(strNewFilePath);

            clsCommonFunctions.ResizeImage(strNewFilePath, strNewFilePath, 140, 140, false);
            
        }
        catch (Exception ex){}
    }

    public void getListBadges(String strPathToFolder)
    {
        lstBadges = new List<string>();
        lstBadgesFileNames = new List<string>();
        try
        {
            string strPath = strPathToFolder;
            string[] files = Directory.GetFiles(strUniqueFullPathBadgesBadges + "\\" + strPath);

            string iMissionID = "";
            if (!string.IsNullOrEmpty(Request.QueryString["iMissionID"]))
                iMissionID = Request.QueryString["iMissionID"];

            int iBadgesCount = 0;

            foreach (string strName in files)
            {
                if (strName.IndexOf("_sml") != -1)
                {
                    string strHTMLBadges = strName.Replace(AppDomain.CurrentDomain.BaseDirectory + "\\", "..\\");
                    strHTMLBadges = strHTMLBadges.Replace("\\", "/");

                    //### Generates a javascript postback for the delete method
                    String strPostBack = Page.ClientScript.GetPostBackEventReference(this, "iRemoveBadges:" + iBadgesCount);

                    lstBadges.Add(@"<tr style='text-align: centre;'>
                                        <td style='text-align: centre;'>
                                            <a class='BadgesColorBox' href='" + strHTMLBadges.Replace("_sml", "_lrg") + "'><img src='" + strHTMLBadges + @"' alt='' title='' style='border: solid 5px #ffffff;' /></a><br /><br />" +
                                            "<center><div class='buttonStyle' style='padding-top: 5px;'><a href=\"javascript:" + strPostBack + "\" style='color: #ffffff;'><div class='deleteButton'></div></a></div></center>" + @"
                                        </td>
                                    </tr>");
                    lstBadgesFileNames.Add(Path.GetFileName(strName).Replace("_sml", ""));
                    iBadgesCount++;
                }
            }
            dlBadges.DataSource = lstBadges;
            dlBadges.DataBind();

            Session["lstBadges"] = lstBadges;
            Session["lstBadgesFileNames"] = lstBadgesFileNames;
        }
        catch (Exception ex) {}
    }

    private string GetMainBadgePath(DataList dtlTarget)
    {
        string strReturn = "";

        foreach (DataListItem dliTarget in dtlTarget.Items)
        {
            RadioButton rdbMainImage2 = (RadioButton)dliTarget.FindControl("rdbMainImage2");
            if (rdbMainImage2.Checked)
            {
                lstBadgesFileNames = (List<string>)Session["lstBadgesFileNames"];
                strReturn = lstBadgesFileNames[dliTarget.ItemIndex];
                break;
            }
        }
        return strReturn;
    }

    private void DeleteBadges(int iBadgesIndex)
    {

        //### Deletes all Badges related to the target Badges.
        lstBadges = (List<string>)Session["lstBadges"];
        lstBadgesFileNames = (List<string>)Session["lstBadgesFileNames"];

        lstBadges.RemoveAt(iBadgesIndex);
        Session["lstBadges"] = lstBadges;

        string[] files = Directory.GetFiles(strUniqueFullPathBadgesBadges + "\\" + lblUniquePathBadges.Text);
        foreach (string file in files)
        {
            if (Path.GetFileName(file) == lstBadgesFileNames[iBadgesIndex].ToString())
            {
                //### Remove all Badges
                File.Delete(file);
                File.Delete(file.Replace(Path.GetExtension(file), "_sml" + Path.GetExtension(file)));
                File.Delete(file.Replace(Path.GetExtension(file), "_med" + Path.GetExtension(file)));
                File.Delete(file.Replace(Path.GetExtension(file), "_lrg" + Path.GetExtension(file)));
                break;
            }
        }
        lstBadgesFileNames.RemoveAt(iBadgesIndex);
        ViewState["lstBadgesFileNames"] = lstBadgesFileNames;
        getList(lblUniquePathBadges.Text);
    }

    #endregion
}