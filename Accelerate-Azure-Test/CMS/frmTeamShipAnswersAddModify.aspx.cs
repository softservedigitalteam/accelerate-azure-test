using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class CMS_frmTeamShipAnswersAddModify : System.Web.UI.Page
{
    clsUsers clsUsers;
    clsTeamShipAnswers clsTeamShipAnswers;
    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

        if (!IsPostBack)
        {
             popTeamShip();
             popMission();

            //### If the iTeamShipAnswerID is passed through then we want to instantiate the object with that iTeamShipAnswerID
            if ((Request.QueryString["iTeamShipAnswerID"] != "") && (Request.QueryString["iTeamShipAnswerID"] != null))
            {
                clsTeamShipAnswers = new clsTeamShipAnswers(Convert.ToInt32(Request.QueryString["iTeamShipAnswerID"]));

                //### Populate the form
                popFormData();
            }
            else
            {
                clsTeamShipAnswers = new clsTeamShipAnswers();
            }
            Session["clsTeamShipAnswers"] = clsTeamShipAnswers;
        }
        else
        {
            clsTeamShipAnswers = (clsTeamShipAnswers)Session["clsTeamShipAnswers"];
        }
    }

    protected void lnkbtnBack_Click(object sender, EventArgs e)
    {
        //### Go back to previous page
        Response.Redirect("frmTeamShipAnswersView.aspx");
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        //### Validate registration process
        bool bCanSave = true;

       bCanSave = clsValidation.IsNullOrEmpty(lstTeamShip, bCanSave);
       bCanSave = clsValidation.IsNullOrEmpty(lstMission, bCanSave);
       bCanSave = clsValidation.IsNullOrEmpty(txtNameAnswer, bCanSave);
       bCanSave = clsValidation.IsNullOrEmpty(txtTitleAnswer, bCanSave);

        if (bCanSave == true)
        {
            mandatoryDiv.Visible = false;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceHappy.png\" alt='' title=''/><div class=\"validationMessage\">TeamShipAnswer added successfully</div></div>";
            SaveData();
        }
        else
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">Please fill out all mandatory fields - TeamShipAnswer not added</div></div>";
        }
    }

    protected void lnkbtnClear_Click(object sender, EventArgs e)
    {
        mandatoryDiv.Visible = false;

        lstTeamShip.SelectedValue = "0";
        clsValidation.SetValid(lstTeamShip);
        lstMission.SelectedValue = "0";
        clsValidation.SetValid(lstMission);
        txtNameAnswer.Text = "";
        clsValidation.SetValid(txtNameAnswer);
        txtTitleAnswer.Text = "";
        clsValidation.SetValid(txtTitleAnswer);
    }

    #endregion 

    #region POPULATE DATA METHODS

    private void popFormData()
    {
         lstTeamShip.SelectedValue = clsTeamShipAnswers.iTeamShipID.ToString();
         lstMission.SelectedValue = clsTeamShipAnswers.iMissionID.ToString();
         txtNameAnswer.Text = clsTeamShipAnswers.strNameAnswer;
         txtTitleAnswer.Text = clsTeamShipAnswers.strTitleAnswer;
    }
    
    private void popTeamShip()
    {
         DataTable dtTeamShipsList = new DataTable();
         lstTeamShip.DataSource = clsTeamShips.GetTeamShipsList();

         //### Populates the drop down list with PK and TITLE;
         lstTeamShip.DataValueField = "iTeamShipID";
         lstTeamShip.DataTextField = "strTitle";

         //### Bind the data to the list;
         lstTeamShip.DataBind();

         //### Add default select option;
         lstTeamShip.Items.Insert(0, new ListItem("--Not Selected--", "0"));
    }
    
    private void popMission()
    {
         DataTable dtMissionsList = new DataTable();
         lstMission.DataSource = clsMissions.GetMissionsList("iMissionTypeID=2", "");

         //### Populates the drop down list with PK and TITLE;
         lstMission.DataValueField = "iMissionID";
         lstMission.DataTextField = "strTitle";

         //### Bind the data to the list;
         lstMission.DataBind();

         //### Add default select option;
         lstMission.Items.Insert(0, new ListItem("--Not Selected--", "0"));
    }
    
    #endregion

    #region SAVE DATA METHODS

    private void SaveData()
    {
        //### Add / Update
        clsTeamShipAnswers.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsTeamShipAnswers.iAddedBy = clsUsers.iUserID;
        clsTeamShipAnswers.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsTeamShipAnswers.iEditedBy = clsUsers.iUserID;
        clsTeamShipAnswers.iTeamShipID = Convert.ToInt32(lstTeamShip.SelectedValue.ToString());
        clsTeamShipAnswers.iMissionID = Convert.ToInt32(lstMission.SelectedValue.ToString());
        clsTeamShipAnswers.strNameAnswer = txtNameAnswer.Text;
        clsTeamShipAnswers.strTitleAnswer = txtTitleAnswer.Text;

        clsTeamShipAnswers.Update();

        Session["dtTeamShipAnswersList"] = null;

        //### Go back to view page
        Response.Redirect("frmTeamShipAnswersView.aspx");
    }

    #endregion
}