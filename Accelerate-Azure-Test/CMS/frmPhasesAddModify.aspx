<%@ Page Title="Phases" Language="C#" MasterPageFile="~/CMS/CMS.master" AutoEventWireup="true" Inherits="CMS_frmPhasesAddModify" Codebehind="frmPhasesAddModify.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" language="javascript">
        function CheckOnOff(rdoId, gridName) {
            var rdo = document.getElementById(rdoId);
            /* Getting an array of all the INPUT controls on the form.*/
            var rdo = document.getElementById(rdoId);
            var all = document.getElementsByTagName("input");
            for (i = 0; i < all.length; i++) {
                /*Checking if it is a radio button, and also checking if the id of that radio button is different than rdoId */
                if (all[i].type == "radio" && all[i].id != rdo.id) {
                    var count = all[i].id.indexOf(gridName);
                    if (count != -1) {
                        all[i].checked = false;
                    }
                }
            }
            rdo.checked = true; /* Finally making the clicked radio button CHECKED */
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:UpdatePanel ID="updMain" runat="server">
        <ContentTemplate>

            <div id="mandatoryDiv" class="mandatoryInvalidDiv" runat="server" visible="false">
                <asp:Label ID="lblValidationMessage" runat="server"></asp:Label>
            </div>

            <div class="controlDiv">
                <div class="imageHolderCommonDiv">
                    <div class="validationImageMandatory"></div>
                </div>
                <div class="labelDiv">Title:</div>
                <div class="fieldDiv">
                    <asp:TextBox ID="txtTitle" runat="server" CssClass="roundedCornerTextBoxMultiLine4" onKeyUp="return SetMaxLength(this,300)" TextMode="MultiLine" Rows="4" onblur="setValid(this);" />
                </div>
                <br class="clearingSpacer" />
            </div>

            <div class="controlDiv">
                <div class="imageHolderCommonDiv">
                    <div class="validationImageMandatory"></div>
                </div>
                <div class="labelDiv">Tag:</div>
                <div class="fieldDiv">
                    <asp:TextBox ID="txtTag" runat="server" CssClass="roundedCornerTextBoxMultiLine4" onKeyUp="return SetMaxLength(this,300)" TextMode="MultiLine" Rows="4" onblur="setValid(this);" />
                </div>
                <br class="clearingSpacer" />
            </div>

            <div class="controlDiv">
                <div class="imageHolderCommonDiv">
                    <div class="validationImageMandatory"></div>
                </div>
                <div class="labelDiv">Start Date:</div>
                <div class="fieldDiv">
                    <img src="images/calendar.png" id="StartDateDateImg" alt="Select a date" title="Select a date" runat="server" class="calendarPosition" />
                    <asp:TextBox ID="txtStartDate" runat="server" CssClass="roundedCornerTextBoxMini"></asp:TextBox>
                    <cc1:CalendarExtender ID="StartDateDateCal" runat="server" PopupButtonID="StartDateDateImg" TargetControlID="txtStartDate" Format="dd MMM yyyy"></cc1:CalendarExtender>
                </div>
                <br class="clearingSpacer" />
            </div>

            <div class="controlDiv">
                <div class="imageHolderCommonDiv">
                    <div class="validationImageMandatory"></div>
                </div>
                <div class="labelDiv">End Date:</div>
                <div class="fieldDiv">
                    <img src="images/calendar.png" id="EndDateDateImg" alt="Select a date" title="Select a date" runat="server" class="calendarPosition" />
                    <asp:TextBox ID="txtEndDate" runat="server" CssClass="roundedCornerTextBoxMini"></asp:TextBox>
                    <cc1:CalendarExtender ID="EndDateDateCal" runat="server" PopupButtonID="EndDateDateImg" TargetControlID="txtEndDate" Format="dd MMM yyyy"></cc1:CalendarExtender>
                </div>
                <br class="clearingSpacer" />
            </div>

            <div class="controlDiv">
                <div class="imageHolderCommonDiv">
                    <div class="validationImageMandatory"></div>
                </div>
                <div class="labelDiv">Images:<br />Minimum 300 x 300</div>
                 <p style="text-align: left;"></p>
                <div class="fieldDiv">
                   
                    <asp:FileUpload ID="FileUpload" runat="server" CssClass="roundedCornerTextBoxUpload" Style="float: left;" Width="100%" onblur="setValid(this);" onchange="this.form.submit()" />
                    <div style="float: left;">
                        <%--<asp:LinkButton ID="btnUpload" runat="server" CssClass="uploadButton" OnClick="btnUpload_Click" Style="margin-left: 5px;" /></div>--%>
                    <asp:Label ID="lblUniquePath" runat="Server" Visible="false"></asp:Label><br style="clear: both" />

                    <asp:UpdatePanel ID="udpImages" runat="server" ChildrenAsTriggers="true">
                        <ContentTemplate>
                            <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                        <ProgressTemplate>
                            <asp:Image ID="imgLoader" runat="server" ImageUrl="images/imgLoader.gif" />Loading. Please wait.
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                            <asp:DataList ID="dlImages" runat="server" RepeatColumns="3" ItemStyle-CssClass="listImages">
                                <ItemTemplate>
                                    <table cellspacing="5">
                                        <%# Container.DataItem %>
                                    </table>
                                    <div align="center" style="padding-top: 5px; display: none;">
                                        <asp:RadioButton ID="rdbMainImage" runat="server" Checked="true" GroupName="MainImages" Text="Main Image" onclick="javascript:CheckOnOff(this.id,'dlImages');" /><br />
                                    </div>
                                </ItemTemplate>
                            </asp:DataList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <br />
                    <b>
                        <asp:Label ID="lblUploadError" runat="server"></asp:Label></b>
                </div>
                <br class="clearingSpacer" />
            </div>

            <div class="Line"></div>
            <div class="buttonsRightDiv">
                <asp:LinkButton ID="lnkbtnBack" runat="server" CssClass="backButton" OnClick="lnkbtnBack_Click" OnClientClick='history.go(-1);return false;' />
                <asp:LinkButton ID="lnkbtnSave" runat="server" CssClass="saveButton" OnClick="lnkbtnSave_Click" />
                <asp:LinkButton ID="lnkbtnClear" runat="server" CssClass="clearButton" OnClick="lnkbtnClear_Click" />
            </div>

        </ContentTemplate>
<%--        <Triggers>
            <asp:PostBackTrigger ControlID="btnUpload" />
        </Triggers>--%>
    </asp:UpdatePanel>
</asp:Content>
