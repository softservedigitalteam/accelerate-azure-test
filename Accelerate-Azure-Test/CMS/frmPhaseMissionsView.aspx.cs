using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;
using System.Net.Mail;
using System.Text;
using System.Configuration;
using System.IO;

/// <summary>
/// Summary description for clsMissionsView
/// </summary>
public partial class CMS_frmPhaseMissionsView : System.Web.UI.Page
{
    DataTable dtOrderItems = new DataTable();

    #region CLASSES AND VARIABLES

    clsUsers clsUsers;
    DataTable dtMissionsList;

    List<clsMissions> glstMissions;

    #endregion

    #region EVENT MODULES

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

        dgrGrid.ItemCreated += new DataGridItemEventHandler(clsCommonFunctions.dgrGrid_PagerPopulate);
        dgOrder.ItemCreated += new DataGridItemEventHandler(clsCommonFunctions.dgrGrid_PagerPopulate);

        if (!Page.IsPostBack)
        {
            Session["dtMissionsList"] = null;
            PopulateFormData();
            PopulateOrderItems();
        }
        else
        {
            if (Session["glstMissions"] == null)
            {
                glstMissions = new List<clsMissions>();
                Session["glstMissions"] = glstMissions;
            }
            else
                glstMissions = (List<clsMissions>)Session["glstMissions"];
        }
    }

    protected string EscapeLikeValue(string valueWithoutWildcards)
    {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < valueWithoutWildcards.Length; i++)
        {
            char c = valueWithoutWildcards[i];
            if (c == '*' || c == '%' || c == '[' || c == ']')
                sb.Append("[").Append(c).Append("]");
            else if (c == '\'')
                sb.Append("''");
            else
                sb.Append(c);
        }
        return sb.ToString();
    }

    protected void lnkbtnSearch_Click(object sender, EventArgs e)
    {
        string EscapedString = EscapeLikeValue(txtSearch.Text.ToString());
        string FilterExpression ="(strMissionName +' '+ strStockCode +' '+ strDescription) LIKE '%" + EscapedString + "%'";
        DataTable dtMissionsList = clsMissions.GetMissionsList(FilterExpression, "");

        Session["dtMissionsList"] = dtMissionsList;

        PopulateFormData();
        PopulateOrderItems();
    }

    protected void lnkbtnSubmit_Click(object sender, EventArgs e)
    {
        if ((dtOrderItems != null))
        {
            try
            {
                //SendAdminMail();
                //###
                //clsManualOrders.ClearManualOrdersList();
                PopulateFormData();
                PopulateOrderItems();
            }
            catch { }
        }
        else
        {
            PopulateFormData();
            PopulateOrderItems();
        }
        
    }

    #endregion

    #region DATA GRID METHODS

    private void PopulateFormData()
    {
        clsPhases clsPhases = new clsPhases(Convert.ToInt32(Request.QueryString["iPhaseID"]));
        litPhaseName.Text = clsPhases.strTitle + " - " + clsPhases.strTag;
        lblPhaseName.Text = clsPhases.strTitle + " - " + clsPhases.strTag;
        //### Populate datagrid using clsMissionsList object
        try
        {
            dtMissionsList = new DataTable();

            if (Session["dtMissionsList"] == null)
                dtMissionsList = clsMissions.GetMissionsList();
            else
                dtMissionsList = (DataTable)Session["dtMissionsList"];

            dgrGrid.DataSource = dtMissionsList;

            //### Add this check to trap if the last item on the page is deleted
            if (Convert.ToInt16(dgrGrid.CurrentPageIndex) < Convert.ToInt16(Session["MissionsView_CurrentPageIndex"]))
            {
                dgrGrid.CurrentPageIndex = 0;
            }
            else
            {
                if (Session["MissionsView_CurrentPageIndex"] != null)
                {
                    dgrGrid.CurrentPageIndex = (int)Session["MissionsView_CurrentPageIndex"];
                }
            }

            dgrGrid.DataBind();

        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void lnkAddItem_Click(object sender, EventArgs e)
    {
        int iMissionID = int.Parse((sender as LinkButton).CommandArgument);

        AddItemToOrderList(iMissionID);

        PopulateFormData();
        PopulateOrderItems();
    }

    protected void lnkDeleteItem_Click(object sender, EventArgs e)
    {
        int iPhaseMissionID = int.Parse((sender as LinkButton).CommandArgument);

        clsPhaseMissions.Delete(iPhaseMissionID);

        DataTable dtMissionsList = new DataTable();

        if (((txtSearch.Text == "SEARCH FOR A MISSION TO ADD") == false) && ((txtSearch.Text == "") == false))
        {
            string FilterExpression = "(strMissionName +' '+ strStockCode +' '+ strDescription) LIKE '%" + txtSearch.Text + "%'";
            dtMissionsList = clsMissions.GetMissionsList(FilterExpression, "");
        }
        else
        {
            dtMissionsList = clsMissions.GetMissionsList();
        }

        Session["dtMissionsList"] = dtMissionsList;

        PopulateFormData();
        PopulateOrderItems();
    }

    protected void AddItemToOrderList(int iMissionID)
    {
        clsMissions clsMissions = new clsMissions(iMissionID);

        clsPhaseMissions clsPhaseMissions = new clsPhaseMissions();

        //clsPhaseMissions.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        //clsPhaseMissions.iAddedBy = clsUsers.iUserID;
        clsPhaseMissions.iPhaseID = Convert.ToInt32(Request.QueryString["iPhaseID"]);
        clsPhaseMissions.iMissionID = iMissionID;
        clsPhaseMissions.strTitle = clsMissions.strTitle;
        clsPhaseMissions.Update();
    }

    private void PopulateOrderItems()
    {
        //### Populate datagrid using clsMembersList object
        try
        {
            dtOrderItems = clsPhaseMissions.GetPhaseMissionsList("iPhaseID=" + Convert.ToInt32(Request.QueryString["iPhaseID"]), "");

            foreach(DataRow dr in dtOrderItems.Rows)
            {
                //dr;
            }
            Session["dtOrderItems"] = dtOrderItems;

            if (Session["dtOrderItems"] == null)
                dtOrderItems = clsPhaseMissions.GetPhaseMissionsList("iPhaseID=" + Convert.ToInt32(Request.QueryString["iPhaseID"]), "");
            else
                dtOrderItems = (DataTable)Session["dtOrderItems"];

            dgOrder.DataSource = dtOrderItems;

            //### Add this check to trap if the last item on the page is deleted
            if (Convert.ToInt16(dgOrder.CurrentPageIndex) < Convert.ToInt16(Session["ManualOrdersView_CurrentPageIndex"]))
            {
                dgOrder.CurrentPageIndex = 0;
            }
            else
            {
                if (Session["ManualOrdersView_CurrentPageIndex"] != null)
                {
                    dgOrder.CurrentPageIndex = (int)Session["ManualOrdersView_CurrentPageIndex"];
                }
            }

            dgOrder.DataBind();

        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgOrder_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //### Method is used when the page index is changed.
        try
        {
            Session["ManualOrdersView_CurrentPageIndex"] = e.NewPageIndex;
            dgOrder.CurrentPageIndex = e.NewPageIndex;

            PopulateFormData();
            PopulateOrderItems();
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgrGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //### Method is used when the page index is changed.
        try
        {
            Session["MissionsView_CurrentPageIndex"] = e.NewPageIndex;
            dgrGrid.CurrentPageIndex = e.NewPageIndex;

            PopulateFormData();
            PopulateOrderItems();
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgrGrid_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        //### Sort Method
        dtMissionsList = new DataTable();

        if (Session["dtMissionsList"] == null)
            dtMissionsList = clsMissions.GetMissionsList();
        else
            dtMissionsList = (DataTable)Session["dtMissionsList"];

        DataView dvTemp = dtMissionsList.DefaultView;

        dvTemp.Sort = e.SortExpression;
        dtMissionsList = dvTemp.ToTable();
        Session["dtMissionsList"] = dtMissionsList;

        PopulateFormData();
        PopulateOrderItems();
    }

    #endregion

    #region Missions FILTERING

    /// <summary>
    /// Enables the AutoCompleteExtender
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    /// 
    [WebMethod]
    [ScriptMethod]
    public static string[] FilterBusinessRule(string prefixText, int count)
    {
        string[] strReturnList = new string[] { };

        string FilterExpression = "(strMissionName +' '+ strStockCode +' '+ strDescription) LIKE '%" + prefixText + "%'";
        DataTable dtMissions = clsMissions.GetMissionsList(FilterExpression, "");
        List<string> glstMissions = new List<string>();

        if (dtMissions.Rows.Count > 0)
        {
            foreach (DataRow dtrMissions in dtMissions.Rows)
            {
                glstMissions.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem("" + dtrMissions["strMissionName"].ToString() +' '+"" + dtrMissions["strStockCode"].ToString() +' '+"" + dtrMissions["strDescription"].ToString(), dtrMissions["iMissionID"].ToString()));
            }
        }
        else
            glstMissions.Add("No Missions Available.");
        strReturnList = glstMissions.ToArray();
        return strReturnList;
    }

    #endregion
}
