
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text;

/// <summary>
/// Summary description for clsCustomColoursView
/// </summary>
public partial class CMS_clsCustomColoursView : System.Web.UI.Page
{
    #region CLASSES AND VARIABLES

    clsUsers clsUsers;
    DataTable dtCustomColoursList;

    List<clsCustomColours> glstCustomColours;

    #endregion

    #region EVENT MODULES

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

dgrGrid.ItemCreated += new DataGridItemEventHandler(clsCommonFunctions.dgrGrid_PagerPopulate);

        if (!Page.IsPostBack)
        {
            PopulateFormData();
        }
        else
        {
            if (Session["glstCustomColours"] == null)
            {
                glstCustomColours = new List<clsCustomColours>();
                Session["glstCustomColours"] = glstCustomColours;
            }
            else
                glstCustomColours = (List<clsCustomColours>)Session["glstCustomColours"];
        }
            PopulateFormData();

    }

    protected string EscapeLikeValue(string valueWithoutWildcards)
    {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < valueWithoutWildcards.Length; i++)
        {
            char c = valueWithoutWildcards[i];
            if (c == '*' || c == '%' || c == '[' || c == ']')
                sb.Append("[").Append(c).Append("]");
            else if (c == '\'')
                sb.Append("''");
            else
                sb.Append(c);
        }
        return sb.ToString();
    }

    protected void lnkbtnSearch_Click(object sender, EventArgs e)
    {
        string EscapedString = EscapeLikeValue(txtSearch.Text.ToString());
        string FilterExpression ="(strTopBarBG +' '+ strTopBarText +' '+ strSideBarHeaders +' '+ strSideBarHeaderText +' '+ strSideBarSubAreas +' '+ strSideBarSubAreaText +' '+ strMissionProgressBar +' '+ strPageHeaderText +' '+ strPhaseHeaderBG) LIKE '%" + EscapedString + "%'";
        DataTable dtCustomColoursList = clsCustomColours.GetCustomColoursList(FilterExpression, "");

        Session["dtCustomColoursList"] = dtCustomColoursList;

        PopulateFormData();
    }

    protected void lnkbtnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmCustomColoursAddModify.aspx");
    }

    #endregion

    #region DATA GRID METHODS

    private void PopulateFormData()
    {
        //### Populate datagrid using clsCustomColoursList object
        try
        {
            dtCustomColoursList = new DataTable();

            if (Session["dtCustomColoursList"] == null)
                dtCustomColoursList = clsCustomColours.GetCustomColoursList();
            else
                dtCustomColoursList = (DataTable)Session["dtCustomColoursList"];

            dgrGrid.DataSource = dtCustomColoursList;

            //### Add this check to trap if the last item on the page is deleted
            if (Convert.ToInt16(dgrGrid.CurrentPageIndex) < Convert.ToInt16(Session["CustomColoursView_CurrentPageIndex"]))
            {
                dgrGrid.CurrentPageIndex = 0;
            }
            else
            {
                if (Session["CustomColoursView_CurrentPageIndex"] != null)
                {
                    dgrGrid.CurrentPageIndex = (int)Session["CustomColoursView_CurrentPageIndex"];
                }
            }

            dgrGrid.DataBind();

        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void lnkDeleteItem_Click(object sender, EventArgs e)
    {
        int iCustomColourID = int.Parse((sender as LinkButton).CommandArgument);

        clsCustomColours.Delete(iCustomColourID);

        PopulateFormData();
    }

    protected void dgrGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //### Method is used when the page index is changed.
        try
        {
            Session["CustomColoursView_CurrentPageIndex"] = e.NewPageIndex;
            dgrGrid.CurrentPageIndex = e.NewPageIndex;
            PopulateFormData();
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgrGrid_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        //### Sort Method
        dtCustomColoursList = new DataTable();

        if (Session["dtCustomColoursList"] == null)
            dtCustomColoursList = clsCustomColours.GetCustomColoursList();
        else
            dtCustomColoursList = (DataTable)Session["dtCustomColoursList"];

        DataView dvTemp = dtCustomColoursList.DefaultView;

        dvTemp.Sort = e.SortExpression;
        dtCustomColoursList = dvTemp.ToTable();
        Session["dtCustomColoursList"] = dtCustomColoursList;

        PopulateFormData();
    }

    public void dgrGrid_PopEditLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkEditItem = (HyperLink)sender;
            DataGridItem dgrItemEdit = (DataGridItem)lnkEditItem.Parent.Parent;

            //### Get the iCustomColourID
            int iCustomColourID = 0;
            iCustomColourID = int.Parse(dgrItemEdit.Cells[0].Text);

            //### Add attributes to edit link
            lnkEditItem.Attributes.Add("href", "frmCustomColoursAddModify.aspx?action=edit&iCustomColourID=" + iCustomColourID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

     public void dgrGrid_PopDeleteLink(object sender, EventArgs e)
     {
          try
          {
             //### Add hyperlink to datagrid item
             HyperLink lnkDeleteItem = (HyperLink)sender;
             DataGridItem dgrItemDelete = (DataGridItem)lnkDeleteItem.Parent.Parent;

             //### Get the iCustomColourID
             int iCustomColourID = 0;
             iCustomColourID = int.Parse(dgrItemDelete.Cells[0].Text);

             //### Add attributes to delete link
             lnkDeleteItem.CssClass = "dgrDeleteLink";
             lnkDeleteItem.Attributes.Add("href", "frmCustomColoursView.aspx?action=delete&iCustomColourID=" + iCustomColourID);
           }
           catch (Exception ex)
           {
                 Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
           }
     }

    #endregion

    #region CustomColours FILTERING

    /// <summary>
    /// Enables the AutoCompleteExtender
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    /// 
    [WebMethod]
    [ScriptMethod]
    public static string[] FilterBusinessRule(string prefixText, int count)
    {
        string[] strReturnList = new string[] { };

        string FilterExpression = "(strTopBarBG +' '+ strTopBarText +' '+ strSideBarHeaders +' '+ strSideBarHeaderText +' '+ strSideBarSubAreas +' '+ strSideBarSubAreaText +' '+ strMissionProgressBar +' '+ strPageHeaderText +' '+ strPhaseHeaderBG) LIKE '%" + prefixText + "%'";
        DataTable dtCustomColours = clsCustomColours.GetCustomColoursList(FilterExpression, "");
        List<string> glstCustomColours = new List<string>();

        if (dtCustomColours.Rows.Count > 0)
        {
            foreach (DataRow dtrCustomColours in dtCustomColours.Rows)
            {
                glstCustomColours.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem("" + dtrCustomColours["strTopBarBG"].ToString() +' '+"" + dtrCustomColours["strTopBarText"].ToString() +' '+"" + dtrCustomColours["strSideBarHeaders"].ToString() +' '+"" + dtrCustomColours["strSideBarHeaderText"].ToString() +' '+"" + dtrCustomColours["strSideBarSubAreas"].ToString() +' '+"" + dtrCustomColours["strSideBarSubAreaText"].ToString() +' '+"" + dtrCustomColours["strMissionProgressBar"].ToString() +' '+"" + dtrCustomColours["strPageHeaderText"].ToString() +' '+"" + dtrCustomColours["strPhaseHeaderBG"].ToString(), dtrCustomColours["iCustomColourID"].ToString()));
            }
        }
        else
            glstCustomColours.Add("No CustomColours Available.");
        strReturnList = glstCustomColours.ToArray();
        return strReturnList;
    }

    #endregion
}
