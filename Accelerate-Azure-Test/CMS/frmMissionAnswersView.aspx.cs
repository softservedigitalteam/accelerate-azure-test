using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text;

/// <summary>
/// Summary description for clsMissionAnswersView
/// </summary>
public partial class CMS_clsMissionAnswersView : System.Web.UI.Page
{
    #region CLASSES AND VARIABLES

    clsUsers clsUsers;
    DataTable dtMissionAnswersList;

    List<clsMissionAnswers> glstMissionAnswers;

    #endregion

    #region EVENT MODULES

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

        dgrGrid.ItemCreated += new DataGridItemEventHandler(clsCommonFunctions.dgrGrid_PagerPopulate);

        if (!Page.IsPostBack)
        {
            Session["dtMissionAnswersList"] = null;
            PopulateFormData();
        }
        else
        {
            if (Session["glstMissionAnswers"] == null)
            {
                glstMissionAnswers = new List<clsMissionAnswers>();
                Session["glstMissionAnswers"] = glstMissionAnswers;
            }
            else
                glstMissionAnswers = (List<clsMissionAnswers>)Session["glstMissionAnswers"];
        }

        PopulateFormData();

    }

    protected string EscapeLikeValue(string valueWithoutWildcards)
    {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < valueWithoutWildcards.Length; i++)
        {
            char c = valueWithoutWildcards[i];
            if (c == '*' || c == '%' || c == '[' || c == ']')
                sb.Append("[").Append(c).Append("]");
            else if (c == '\'')
                sb.Append("''");
            else
                sb.Append(c);
        }
        return sb.ToString();
    }

    protected void lnkbtnSearch_Click(object sender, EventArgs e)
    {
        string EscapedString = EscapeLikeValue(txtSearch.Text.ToString());
        string FilterExpression ="(strAnswer) LIKE '%" + EscapedString + "%'";
        DataTable dtMissionAnswersList = clsMissionAnswers.GetMissionAnswersList(FilterExpression, "");

        Session["dtMissionAnswersList"] = dtMissionAnswersList;

        PopulateFormData();
    }

    protected void lnkbtnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmMissionAnswersAddModify.aspx");
    }

    #endregion

    #region DATA GRID METHODS

    private void PopulateFormData()
    {
        //### Populate datagrid using clsMissionAnswersList object
        try
        {
            dtMissionAnswersList = new DataTable();

            if (Session["dtMissionAnswersList"] == null)
                dtMissionAnswersList = clsMissionAnswers.GetMissionAnswersList();
            else
                dtMissionAnswersList = (DataTable)Session["dtMissionAnswersList"];

            dgrGrid.DataSource = dtMissionAnswersList;

            //### Add this check to trap if the last item on the page is deleted
            if (Convert.ToInt16(dgrGrid.CurrentPageIndex) < Convert.ToInt16(Session["MissionAnswersView_CurrentPageIndex"]))
            {
                dgrGrid.CurrentPageIndex = 0;
            }
            else
            {
                if (Session["MissionAnswersView_CurrentPageIndex"] != null)
                {
                    dgrGrid.CurrentPageIndex = (int)Session["MissionAnswersView_CurrentPageIndex"];
                }
            }

            dgrGrid.DataBind();

        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void lnkDeleteItem_Click(object sender, EventArgs e)
    {
        int iMissionAnswerID = int.Parse((sender as LinkButton).CommandArgument);

        clsMissionAnswers.Delete(iMissionAnswerID);

        DataTable dtMissionAnswersList = new DataTable();

        if (((txtSearch.Text == "SEARCH FOR A MISSION ANSWER") == false) && ((txtSearch.Text == "") == false))
        {
            string FilterExpression = "(strAnswer) LIKE '%" + txtSearch.Text + "%'";
            dtMissionAnswersList = clsMissionAnswers.GetMissionAnswersList(FilterExpression, "");
        }
        else
        {
            dtMissionAnswersList = clsMissionAnswers.GetMissionAnswersList();
        }

        Session["dtMissionAnswersList"] = dtMissionAnswersList;

        PopulateFormData();
    }

    protected void dgrGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //### Method is used when the page index is changed.
        try
        {
            Session["MissionAnswersView_CurrentPageIndex"] = e.NewPageIndex;
            dgrGrid.CurrentPageIndex = e.NewPageIndex;
            PopulateFormData();
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgrGrid_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        //### Sort Method
        dtMissionAnswersList = new DataTable();

        if (Session["dtMissionAnswersList"] == null)
            dtMissionAnswersList = clsMissionAnswers.GetMissionAnswersList();
        else
            dtMissionAnswersList = (DataTable)Session["dtMissionAnswersList"];

        DataView dvTemp = dtMissionAnswersList.DefaultView;

        dvTemp.Sort = e.SortExpression;
        dtMissionAnswersList = dvTemp.ToTable();
        Session["dtMissionAnswersList"] = dtMissionAnswersList;

        PopulateFormData();
    }

    //public void dgrGrid_PopCorrectImg(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        //### Add hyperlink to datagrid item
    //        Image imgCorrect = (Image)sender;
    //        DataGridItem dgrItemCorrect = (DataGridItem)imgCorrect.Parent.Parent;

    //        int iMissionAnswerID = 0;
    //        iMissionAnswerID = int.Parse(dgrItemCorrect.Cells[0].Text);

    //        clsMissionAnswers clsMissionAnswers = new clsMissionAnswers(iMissionAnswerID);

    //         clsMissionAnswers.GetMissionAnswersList();

    //        //CheckBox lblCorrect = (CheckBox)sender;
    //        //DataGridItem dgrItemCorrect = (DataGridItem)lblCorrect.Text;

    //        ////### Get the iMissionAnswerID
    //        //int iMissionAnswerID = 0;
    //        //iMissionAnswerID = int.Parse(dgrItemEdit.Cells[0].Text);

    //        ////### Add attributes to edit link
    //        ////lblCorrect.Attributes.Add("href", "frmMissionAnswersAddModify.aspx?action=edit&iMissionAnswerID=" + iMissionAnswerID);
    //    }
    //    catch (Exception ex)
    //    {
    //        Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
    //    }
    //}

    public void dgrGrid_PopEditLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkEditItem = (HyperLink)sender;
            DataGridItem dgrItemEdit = (DataGridItem)lnkEditItem.Parent.Parent;

            //### Get the iMissionAnswerID
            int iMissionAnswerID = 0;
            iMissionAnswerID = int.Parse(dgrItemEdit.Cells[0].Text);

            //### Add attributes to edit link
            lnkEditItem.Attributes.Add("href", "frmMissionAnswersAddModify.aspx?action=edit&iMissionAnswerID=" + iMissionAnswerID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

     public void dgrGrid_PopDeleteLink(object sender, EventArgs e)
     {
          try
          {
             //### Add hyperlink to datagrid item
             HyperLink lnkDeleteItem = (HyperLink)sender;
             DataGridItem dgrItemDelete = (DataGridItem)lnkDeleteItem.Parent.Parent;

             //### Get the iMissionAnswerID
             int iMissionAnswerID = 0;
             iMissionAnswerID = int.Parse(dgrItemDelete.Cells[0].Text);

             //### Add attributes to delete link
             lnkDeleteItem.CssClass = "dgrDeleteLink";
             lnkDeleteItem.Attributes.Add("href", "frmMissionAnswersView.aspx?action=delete&iMissionAnswerID=" + iMissionAnswerID);
           }
           catch (Exception ex)
           {
                 Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
           }
     }

    #endregion

    #region MissionAnswers FILTERING

    /// <summary>
    /// Enables the AutoCompleteExtender
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    /// 
    [WebMethod]
    [ScriptMethod]
    public static string[] FilterBusinessRule(string prefixText, int count)
    {
        string[] strReturnList = new string[] { };

        string FilterExpression = "(strAnswer) LIKE '%" + prefixText + "%'";
        DataTable dtMissionAnswers = clsMissionAnswers.GetMissionAnswersList(FilterExpression, "");
        List<string> glstMissionAnswers = new List<string>();

        if (dtMissionAnswers.Rows.Count > 0)
        {
            foreach (DataRow dtrMissionAnswers in dtMissionAnswers.Rows)
            {
                glstMissionAnswers.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem("" + dtrMissionAnswers["strAnswer"].ToString(), dtrMissionAnswers["iMissionAnswerID"].ToString()));
            }
        }
        else
            glstMissionAnswers.Add("No MissionAnswers Available.");
        strReturnList = glstMissionAnswers.ToArray();
        return strReturnList;
    }

    #endregion
}
