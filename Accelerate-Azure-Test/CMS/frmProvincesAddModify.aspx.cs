using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class CMS_frmProvincesAddModify : System.Web.UI.Page
{
    clsUsers clsUsers;
    clsProvinces clsProvinces;
    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

        if (!IsPostBack)
        {

            //### If the iProvinceID is passed through then we want to instantiate the object with that iProvinceID
            if ((Request.QueryString["iProvinceID"] != "") && (Request.QueryString["iProvinceID"] != null))
            {
                clsProvinces = new clsProvinces(Convert.ToInt32(Request.QueryString["iProvinceID"]));

                //### Populate the form
                popFormData();
            }
            else
            {
                clsProvinces = new clsProvinces();
            }
            Session["clsProvinces"] = clsProvinces;
        }
        else
        {
            clsProvinces = (clsProvinces)Session["clsProvinces"];
        }
    }

    protected void lnkbtnBack_Click(object sender, EventArgs e)
    {
        //### Go back to previous page
        Response.Redirect("frmProvincesView.aspx");
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        //### Validate registration process
        bool bCanSave = true;

       bCanSave = clsValidation.IsNullOrEmpty(txtTitle, bCanSave);

        if (bCanSave == true)
        {
            mandatoryDiv.Visible = false;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceHappy.png\" alt='' title=''/><div class=\"validationMessage\">Province added successfully</div></div>";
            SaveData();
        }
        else
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">Please fill out all mandatory fields - Province not added</div></div>";
        }
    }

    protected void lnkbtnClear_Click(object sender, EventArgs e)
    {
        mandatoryDiv.Visible = false;

        txtTitle.Text = "";
        clsValidation.SetValid(txtTitle);
    }

    #endregion 

    #region POPULATE DATA METHODS

    private void popFormData()
    {
         txtTitle.Text = clsProvinces.strTitle;
    }
    
    #endregion

    #region SAVE DATA METHODS

    private void SaveData()
    {
        //### Add / Update
        clsProvinces.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsProvinces.iAddedBy = clsUsers.iUserID;
        clsProvinces.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsProvinces.iEditedBy = clsUsers.iUserID;
        clsProvinces.strTitle = txtTitle.Text;

        clsProvinces.Update();

        Session["dtProvincesList"] = null;

        //### Go back to view page
        Response.Redirect("frmProvincesView.aspx");
    }

    #endregion
}