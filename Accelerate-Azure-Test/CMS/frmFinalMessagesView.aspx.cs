
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text;

/// <summary>
/// Summary description for clsFinalMessagesView
/// </summary>
public partial class CMS_clsFinalMessagesView : System.Web.UI.Page
{
    #region CLASSES AND VARIABLES

    clsUsers clsUsers;
    DataTable dtFinalMessagesList;

    List<clsFinalMessages> glstFinalMessages;

    #endregion

    #region EVENT MODULES

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

dgrGrid.ItemCreated += new DataGridItemEventHandler(clsCommonFunctions.dgrGrid_PagerPopulate);

        if (!Page.IsPostBack)
        {
            Session["dtFinalMessagesList"] = null;
            PopulateFormData();
        }
        else
        {
            if (Session["glstFinalMessages"] == null)
            {
                glstFinalMessages = new List<clsFinalMessages>();
                Session["glstFinalMessages"] = glstFinalMessages;
            }
            else
                glstFinalMessages = (List<clsFinalMessages>)Session["glstFinalMessages"];
        }
            PopulateFormData();

    }

    protected string EscapeLikeValue(string valueWithoutWildcards)
    {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < valueWithoutWildcards.Length; i++)
        {
            char c = valueWithoutWildcards[i];
            if (c == '*' || c == '%' || c == '[' || c == ']')
                sb.Append("[").Append(c).Append("]");
            else if (c == '\'')
                sb.Append("''");
            else
                sb.Append(c);
        }
        return sb.ToString();
    }

    protected void lnkbtnSearch_Click(object sender, EventArgs e)
    {
        string EscapedString = EscapeLikeValue(txtSearch.Text.ToString());
        string FilterExpression ="(strHeaderText +' '+ strParagraphText +' '+ strMasterImage) LIKE '%" + EscapedString + "%'";
        DataTable dtFinalMessagesList = clsFinalMessages.GetFinalMessagesList(FilterExpression, "");

        Session["dtFinalMessagesList"] = dtFinalMessagesList;

        PopulateFormData();
    }

    protected void lnkbtnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmFinalMessagesAddModify.aspx");
    }

    #endregion

    #region DATA GRID METHODS

    private void PopulateFormData()
    {
        //### Populate datagrid using clsFinalMessagesList object
        try
        {
            dtFinalMessagesList = new DataTable();

            if (Session["dtFinalMessagesList"] == null)
                dtFinalMessagesList = clsFinalMessages.GetFinalMessagesList();
            else
                dtFinalMessagesList = (DataTable)Session["dtFinalMessagesList"];

            dgrGrid.DataSource = dtFinalMessagesList;

            //### Add this check to trap if the last item on the page is deleted
            if (Convert.ToInt16(dgrGrid.CurrentPageIndex) < Convert.ToInt16(Session["FinalMessagesView_CurrentPageIndex"]))
            {
                dgrGrid.CurrentPageIndex = 0;
            }
            else
            {
                if (Session["FinalMessagesView_CurrentPageIndex"] != null)
                {
                    dgrGrid.CurrentPageIndex = (int)Session["FinalMessagesView_CurrentPageIndex"];
                }
            }

            dgrGrid.DataBind();

        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void lnkDeleteItem_Click(object sender, EventArgs e)
    {
        int iFinalMessageID = int.Parse((sender as LinkButton).CommandArgument);

        clsFinalMessages.Delete(iFinalMessageID);

        DataTable dtFinalMessagesList = new DataTable();

        if (((txtSearch.Text == "SEARCH FOR A FINAL MESSAGE") == false) && ((txtSearch.Text == "") == false))
        {
            string FilterExpression = "(strHeaderText +' '+ strParagraphText +' '+ strMasterImage) LIKE '%" + txtSearch.Text + "%'";
            dtFinalMessagesList = clsFinalMessages.GetFinalMessagesList(FilterExpression, "");
        }
        else
        {
            dtFinalMessagesList = clsFinalMessages.GetFinalMessagesList();
        }

        Session["dtFinalMessagesList"] = dtFinalMessagesList;

        PopulateFormData();
    }

    protected void dgrGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //### Method is used when the page index is changed.
        try
        {
            Session["FinalMessagesView_CurrentPageIndex"] = e.NewPageIndex;
            dgrGrid.CurrentPageIndex = e.NewPageIndex;
            PopulateFormData();
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgrGrid_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        //### Sort Method
        dtFinalMessagesList = new DataTable();

        if (Session["dtFinalMessagesList"] == null)
            dtFinalMessagesList = clsFinalMessages.GetFinalMessagesList();
        else
            dtFinalMessagesList = (DataTable)Session["dtFinalMessagesList"];

        DataView dvTemp = dtFinalMessagesList.DefaultView;

        dvTemp.Sort = e.SortExpression;
        dtFinalMessagesList = dvTemp.ToTable();
        Session["dtFinalMessagesList"] = dtFinalMessagesList;

        PopulateFormData();
    }

    public void dgrGrid_PopEditLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkEditItem = (HyperLink)sender;
            DataGridItem dgrItemEdit = (DataGridItem)lnkEditItem.Parent.Parent;

            //### Get the iFinalMessageID
            int iFinalMessageID = 0;
            iFinalMessageID = int.Parse(dgrItemEdit.Cells[0].Text);

            //### Add attributes to edit link
            lnkEditItem.Attributes.Add("href", "frmFinalMessagesAddModify.aspx?action=edit&iFinalMessageID=" + iFinalMessageID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

     public void dgrGrid_PopDeleteLink(object sender, EventArgs e)
     {
          try
          {
             //### Add hyperlink to datagrid item
             HyperLink lnkDeleteItem = (HyperLink)sender;
             DataGridItem dgrItemDelete = (DataGridItem)lnkDeleteItem.Parent.Parent;

             //### Get the iFinalMessageID
             int iFinalMessageID = 0;
             iFinalMessageID = int.Parse(dgrItemDelete.Cells[0].Text);

             //### Add attributes to delete link
             lnkDeleteItem.CssClass = "dgrDeleteLink";
             lnkDeleteItem.Attributes.Add("href", "frmFinalMessagesView.aspx?action=delete&iFinalMessageID=" + iFinalMessageID);
           }
           catch (Exception ex)
           {
                 Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
           }
     }

    #endregion

    #region FinalMessages FILTERING

    /// <summary>
    /// Enables the AutoCompleteExtender
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    /// 
    [WebMethod]
    [ScriptMethod]
    public static string[] FilterBusinessRule(string prefixText, int count)
    {
        string[] strReturnList = new string[] { };

        string FilterExpression = "(strHeaderText +' '+ strParagraphText +' '+ strMasterImage) LIKE '%" + prefixText + "%'";
        DataTable dtFinalMessages = clsFinalMessages.GetFinalMessagesList(FilterExpression, "");
        List<string> glstFinalMessages = new List<string>();

        if (dtFinalMessages.Rows.Count > 0)
        {
            foreach (DataRow dtrFinalMessages in dtFinalMessages.Rows)
            {
                glstFinalMessages.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem("" + dtrFinalMessages["strHeaderText"].ToString() +' '+"" + dtrFinalMessages["strParagraphText"].ToString() +' '+"" + dtrFinalMessages["strMasterImage"].ToString(), dtrFinalMessages["iFinalMessageID"].ToString()));
            }
        }
        else
            glstFinalMessages.Add("No FinalMessages Available.");
        strReturnList = glstFinalMessages.ToArray();
        return strReturnList;
    }

    #endregion
}
