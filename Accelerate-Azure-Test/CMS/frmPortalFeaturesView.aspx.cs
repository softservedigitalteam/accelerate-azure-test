
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text;

/// <summary>
/// Summary description for clsPortalFeaturesView
/// </summary>
public partial class CMS_clsPortalFeaturesView : System.Web.UI.Page
{
    #region CLASSES AND VARIABLES

    clsUsers clsUsers;
    DataTable dtPortalFeaturesList;

    List<clsPortalFeatures> glstPortalFeatures;

    #endregion

    #region EVENT MODULES

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

dgrGrid.ItemCreated += new DataGridItemEventHandler(clsCommonFunctions.dgrGrid_PagerPopulate);

        if (!Page.IsPostBack)
        {
            Session["dtPortalFeaturesList"] = null;
            PopulateFormData();
        }
        else
        {
            if (Session["glstPortalFeatures"] == null)
            {
                glstPortalFeatures = new List<clsPortalFeatures>();
                Session["glstPortalFeatures"] = glstPortalFeatures;
            }
            else
                glstPortalFeatures = (List<clsPortalFeatures>)Session["glstPortalFeatures"];
        }
            PopulateFormData();

    }

    protected string EscapeLikeValue(string valueWithoutWildcards)
    {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < valueWithoutWildcards.Length; i++)
        {
            char c = valueWithoutWildcards[i];
            if (c == '*' || c == '%' || c == '[' || c == ']')
                sb.Append("[").Append(c).Append("]");
            else if (c == '\'')
                sb.Append("''");
            else
                sb.Append(c);
        }
        return sb.ToString();
    }

    protected void lnkbtnSearch_Click(object sender, EventArgs e)
    {
        string EscapedString = EscapeLikeValue(txtSearch.Text.ToString());
        string FilterExpression ="(strTitle +' '+ strDescription +' '+ strYouTubeLink +' '+ strMasterImage) LIKE '%" + EscapedString + "%'";
        DataTable dtPortalFeaturesList = clsPortalFeatures.GetPortalFeaturesList(FilterExpression, "");

        Session["dtPortalFeaturesList"] = dtPortalFeaturesList;

        PopulateFormData();
    }

    protected void lnkbtnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmPortalFeaturesAddModify.aspx");
    }

    #endregion

    #region DATA GRID METHODS

    private void PopulateFormData()
    {
        //### Populate datagrid using clsPortalFeaturesList object
        try
        {
            dtPortalFeaturesList = new DataTable();

            if (Session["dtPortalFeaturesList"] == null)
                dtPortalFeaturesList = clsPortalFeatures.GetPortalFeaturesList("", "dtAdded DESC");
            else
                dtPortalFeaturesList = (DataTable)Session["dtPortalFeaturesList"];

            dgrGrid.DataSource = dtPortalFeaturesList;

            //### Add this check to trap if the last item on the page is deleted
            if (Convert.ToInt16(dgrGrid.CurrentPageIndex) < Convert.ToInt16(Session["PortalFeaturesView_CurrentPageIndex"]))
            {
                dgrGrid.CurrentPageIndex = 0;
            }
            else
            {
                if (Session["PortalFeaturesView_CurrentPageIndex"] != null)
                {
                    dgrGrid.CurrentPageIndex = (int)Session["PortalFeaturesView_CurrentPageIndex"];
                }
            }

            dgrGrid.DataBind();

        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void lnkDeleteItem_Click(object sender, EventArgs e)
    {
        int iPortalFeatureID = int.Parse((sender as LinkButton).CommandArgument);

        clsPortalFeatures.Delete(iPortalFeatureID);

        DataTable dtPortalFeaturesList = new DataTable();

        if (((txtSearch.Text == "SEARCH FOR A PORTAL FEATURE") == false) && ((txtSearch.Text == "") == false))
        {
            string FilterExpression = "(strTitle +' '+ strDescription +' '+ strYouTubeLink +' '+ strMasterImage) LIKE '%" + txtSearch.Text + "%'";
            dtPortalFeaturesList = clsPortalFeatures.GetPortalFeaturesList(FilterExpression, "");
        }
        else
        {
            dtPortalFeaturesList = clsPortalFeatures.GetPortalFeaturesList();
        }

        Session["dtPortalFeaturesList"] = dtPortalFeaturesList;

        PopulateFormData();
    }

    protected void dgrGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //### Method is used when the page index is changed.
        try
        {
            Session["PortalFeaturesView_CurrentPageIndex"] = e.NewPageIndex;
            dgrGrid.CurrentPageIndex = e.NewPageIndex;
            PopulateFormData();
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgrGrid_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        //### Sort Method
        dtPortalFeaturesList = new DataTable();

        if (Session["dtPortalFeaturesList"] == null)
            dtPortalFeaturesList = clsPortalFeatures.GetPortalFeaturesList();
        else
            dtPortalFeaturesList = (DataTable)Session["dtPortalFeaturesList"];

        DataView dvTemp = dtPortalFeaturesList.DefaultView;

        dvTemp.Sort = e.SortExpression;
        dtPortalFeaturesList = dvTemp.ToTable();
        Session["dtPortalFeaturesList"] = dtPortalFeaturesList;

        PopulateFormData();
    }

    public void dgrGrid_PopEditLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkEditItem = (HyperLink)sender;
            DataGridItem dgrItemEdit = (DataGridItem)lnkEditItem.Parent.Parent;

            //### Get the iPortalFeatureID
            int iPortalFeatureID = 0;
            iPortalFeatureID = int.Parse(dgrItemEdit.Cells[0].Text);

            //### Add attributes to edit link
            lnkEditItem.Attributes.Add("href", "frmPortalFeaturesAddModify.aspx?action=edit&iPortalFeatureID=" + iPortalFeatureID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

     public void dgrGrid_PopDeleteLink(object sender, EventArgs e)
     {
          try
          {
             //### Add hyperlink to datagrid item
             HyperLink lnkDeleteItem = (HyperLink)sender;
             DataGridItem dgrItemDelete = (DataGridItem)lnkDeleteItem.Parent.Parent;

             //### Get the iPortalFeatureID
             int iPortalFeatureID = 0;
             iPortalFeatureID = int.Parse(dgrItemDelete.Cells[0].Text);

             //### Add attributes to delete link
             lnkDeleteItem.CssClass = "dgrDeleteLink";
             lnkDeleteItem.Attributes.Add("href", "frmPortalFeaturesView.aspx?action=delete&iPortalFeatureID=" + iPortalFeatureID);
           }
           catch (Exception ex)
           {
                 Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
           }
     }

    #endregion

    #region PortalFeatures FILTERING

    /// <summary>
    /// Enables the AutoCompleteExtender
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    /// 
    [WebMethod]
    [ScriptMethod]
    public static string[] FilterBusinessRule(string prefixText, int count)
    {
        string[] strReturnList = new string[] { };

        string FilterExpression = "(strTitle +' '+ strDescription +' '+ strYouTubeLink +' '+ strMasterImage) LIKE '%" + prefixText + "%'";
        DataTable dtPortalFeatures = clsPortalFeatures.GetPortalFeaturesList(FilterExpression, "");
        List<string> glstPortalFeatures = new List<string>();

        if (dtPortalFeatures.Rows.Count > 0)
        {
            foreach (DataRow dtrPortalFeatures in dtPortalFeatures.Rows)
            {
                glstPortalFeatures.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem("" + dtrPortalFeatures["strTitle"].ToString() +' '+"" + dtrPortalFeatures["strDescription"].ToString() +' '+"" + dtrPortalFeatures["strYouTubeLink"].ToString() +' '+"" + dtrPortalFeatures["strMasterImage"].ToString(), dtrPortalFeatures["iPortalFeatureID"].ToString()));
            }
        }
        else
            glstPortalFeatures.Add("No PortalFeatures Available.");
        strReturnList = glstPortalFeatures.ToArray();
        return strReturnList;
    }

    #endregion
}
