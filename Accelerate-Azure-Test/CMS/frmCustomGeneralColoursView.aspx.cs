
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text;

/// <summary>
/// Summary description for clsCustomGeneralColoursViews
/// </summary>
public partial class CMS_clsCustomGeneralColoursView : System.Web.UI.Page
{
    #region CLASSES AND VARIABLES

    clsUsers clsUsers;
    DataTable dtCustomGeneralColoursList;

    List<clsCustomGeneralColours> glstCustomGeneralColours;

    #endregion

    #region EVENT MODULES

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

dgrGrid.ItemCreated += new DataGridItemEventHandler(clsCommonFunctions.dgrGrid_PagerPopulate);

        if (!Page.IsPostBack)
        {
            Session["dtCustomGeneralColoursList"] = null;
            PopulateFormData();
        }
            PopulateFormData();

    }

    protected string EscapeLikeValue(string valueWithoutWildcards)
    {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < valueWithoutWildcards.Length; i++)
        {
            char c = valueWithoutWildcards[i];
            if (c == '*' || c == '%' || c == '[' || c == ']')
                sb.Append("[").Append(c).Append("]");
            else if (c == '\'')
                sb.Append("''");
            else
                sb.Append(c);
        }
        return sb.ToString();
    }

    protected void lnkbtnSearch_Click(object sender, EventArgs e)
    {
        string EscapedString = EscapeLikeValue(txtSearch.Text.ToString());
        string FilterExpression ="(strTitle) LIKE '%" + EscapedString + "%'";
        DataTable dtCustomGeneralColoursList = clsCustomGeneralColours.GetCustomGeneralColoursList(FilterExpression, "");

        Session["dtCustomGeneralColoursList"] = dtCustomGeneralColoursList;

        PopulateFormData();
    }

    protected void lnkbtnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmCustomGeneralColoursAddModify.aspx");
    }

    #endregion

    #region DATA GRID METHODS

    private void PopulateFormData()
    {
        //### Populate datagrid using clsCustomGeneralColoursList object
        try
        {
            dtCustomGeneralColoursList = new DataTable();

            if (Session["dtCustomGeneralColoursList"] == null)
                dtCustomGeneralColoursList = clsCustomGeneralColours.GetCustomGeneralColoursList();
            else
                dtCustomGeneralColoursList = (DataTable)Session["dtCustomGeneralColoursList"];

            dgrGrid.DataSource = dtCustomGeneralColoursList;

            //### Add this check to trap if the last item on the page is deleted
            if (Convert.ToInt16(dgrGrid.CurrentPageIndex) < Convert.ToInt16(Session["CustomGeneralColoursView_CurrentPageIndex"]))
            {
                dgrGrid.CurrentPageIndex = 0;
            }
            else
            {
                if (Session["CustomGeneralColoursView_CurrentPageIndex"] != null)
                {
                    dgrGrid.CurrentPageIndex = (int)Session["CustomGeneralColoursView_CurrentPageIndex"];
                }
            }

            dgrGrid.DataBind();

        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void lnkDeleteItem_Click(object sender, EventArgs e)
    {
        int iCustomGeneralColourID = int.Parse((sender as LinkButton).CommandArgument);

        clsCustomGeneralColours.Delete(iCustomGeneralColourID);

        DataTable dtCustomGeneralColoursList = new DataTable();

        if (((txtSearch.Text == "SEARCH FOR A CUSTOM GENERAL COLOUR") == false) && ((txtSearch.Text == "") == false))
        {
            string FilterExpression = "(strTitle) LIKE '%" + txtSearch.Text + "%'";
            dtCustomGeneralColoursList = clsCustomGeneralColours.GetCustomGeneralColoursList(FilterExpression, "");
        }
        else
        {
            dtCustomGeneralColoursList = clsCustomGeneralColours.GetCustomGeneralColoursList();
        }

        Session["dtCustomGeneralColoursList"] = dtCustomGeneralColoursList;

        PopulateFormData();
    }

    protected void dgrGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //### Method is used when the page index is changed.
        try
        {
            Session["CustomGeneralColoursView_CurrentPageIndex"] = e.NewPageIndex;
            dgrGrid.CurrentPageIndex = e.NewPageIndex;
            PopulateFormData();
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgrGrid_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        //### Sort Method
        dtCustomGeneralColoursList = new DataTable();

        if (Session["dtCustomGeneralColoursList"] == null)
            dtCustomGeneralColoursList = clsCustomGeneralColours.GetCustomGeneralColoursList();
        else
            dtCustomGeneralColoursList = (DataTable)Session["dtCustomGeneralColoursList"];

        DataView dvTemp = dtCustomGeneralColoursList.DefaultView;

        dvTemp.Sort = e.SortExpression;
        dtCustomGeneralColoursList = dvTemp.ToTable();
        Session["dtCustomGeneralColoursList"] = dtCustomGeneralColoursList;

        PopulateFormData();
    }

    public void dgrGrid_PopEditLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkEditItem = (HyperLink)sender;
            DataGridItem dgrItemEdit = (DataGridItem)lnkEditItem.Parent.Parent;

            //### Get the iCustomGeneralColourID
            int iCustomGeneralColourID = 0;
            iCustomGeneralColourID = int.Parse(dgrItemEdit.Cells[0].Text);

            //### Add attributes to edit link
            lnkEditItem.Attributes.Add("href", "frmCustomGeneralColoursAddModify.aspx?action=edit&iCustomGeneralColourID=" + iCustomGeneralColourID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    #endregion

    #region CustomGeneralColours FILTERING

    /// <summary>
    /// Enables the AutoCompleteExtender
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    /// 
    [WebMethod]
    [ScriptMethod]
    public static string[] FilterBusinessRule(string prefixText, int count)
    {
        string[] strReturnList = new string[] { };

        string FilterExpression = "(strTitle) LIKE '%" + prefixText + "%'";
        DataTable dtCustomGeneralColours = clsCustomGeneralColours.GetCustomGeneralColoursList(FilterExpression, "");
        List<string> glstCustomGeneralColours = new List<string>();

        if (dtCustomGeneralColours.Rows.Count > 0)
        {
            foreach (DataRow dtrCustomGeneralColours in dtCustomGeneralColours.Rows)
            {
                glstCustomGeneralColours.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem("" + dtrCustomGeneralColours["strTitle"].ToString(), dtrCustomGeneralColours["iCustomGeneralColourID"].ToString()));
            }
        }
        else
            glstCustomGeneralColours.Add("No CustomGeneralColours Available.");
        strReturnList = glstCustomGeneralColours.ToArray();
        return strReturnList;
    }

    #endregion
}
