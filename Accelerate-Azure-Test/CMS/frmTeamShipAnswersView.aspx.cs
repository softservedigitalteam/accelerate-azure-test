
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text;

/// <summary>
/// Summary description for clsTeamShipAnswersView
/// </summary>
public partial class CMS_clsTeamShipAnswersView : System.Web.UI.Page
{
    #region CLASSES AND VARIABLES

    clsUsers clsUsers;
    DataTable dtTeamShipAnswersList;

    List<clsTeamShipAnswers> glstTeamShipAnswers;

    #endregion

    #region EVENT MODULES

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

dgrGrid.ItemCreated += new DataGridItemEventHandler(clsCommonFunctions.dgrGrid_PagerPopulate);

        if (!Page.IsPostBack)
        {
            Session["dtTeamShipAnswersList"] = null;
            PopulateFormData();
        }
        else
        {
            if (Session["glstTeamShipAnswers"] == null)
            {
                glstTeamShipAnswers = new List<clsTeamShipAnswers>();
                Session["glstTeamShipAnswers"] = glstTeamShipAnswers;
            }
            else
                glstTeamShipAnswers = (List<clsTeamShipAnswers>)Session["glstTeamShipAnswers"];
        }
            PopulateFormData();

    }

    protected string EscapeLikeValue(string valueWithoutWildcards)
    {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < valueWithoutWildcards.Length; i++)
        {
            char c = valueWithoutWildcards[i];
            if (c == '*' || c == '%' || c == '[' || c == ']')
                sb.Append("[").Append(c).Append("]");
            else if (c == '\'')
                sb.Append("''");
            else
                sb.Append(c);
        }
        return sb.ToString();
    }

    protected void lnkbtnSearch_Click(object sender, EventArgs e)
    {
        string EscapedString = EscapeLikeValue(txtSearch.Text.ToString());
        string FilterExpression ="(strNameAnswer +' '+ strTitleAnswer) LIKE '%" + EscapedString + "%'";
        DataTable dtTeamShipAnswersList = clsTeamShipAnswers.GetTeamShipAnswersList(FilterExpression, "");

        Session["dtTeamShipAnswersList"] = dtTeamShipAnswersList;

        PopulateFormData();
    }

    protected void lnkbtnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmTeamShipAnswersAddModify.aspx");
    }

    #endregion

    #region DATA GRID METHODS

    private void PopulateFormData()
    {
        //### Populate datagrid using clsTeamShipAnswersList object
        try
        {
            dtTeamShipAnswersList = new DataTable();

            if (Session["dtTeamShipAnswersList"] == null)
                dtTeamShipAnswersList = clsTeamShipAnswers.GetTeamShipAnswersList();
            else
                dtTeamShipAnswersList = (DataTable)Session["dtTeamShipAnswersList"];

            dgrGrid.DataSource = dtTeamShipAnswersList;

            //### Add this check to trap if the last item on the page is deleted
            if (Convert.ToInt16(dgrGrid.CurrentPageIndex) < Convert.ToInt16(Session["TeamShipAnswersView_CurrentPageIndex"]))
            {
                dgrGrid.CurrentPageIndex = 0;
            }
            else
            {
                if (Session["TeamShipAnswersView_CurrentPageIndex"] != null)
                {
                    dgrGrid.CurrentPageIndex = (int)Session["TeamShipAnswersView_CurrentPageIndex"];
                }
            }

            dgrGrid.DataBind();

        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void lnkDeleteItem_Click(object sender, EventArgs e)
    {
        int iTeamShipAnswerID = int.Parse((sender as LinkButton).CommandArgument);

        clsTeamShipAnswers.Delete(iTeamShipAnswerID);

        DataTable dtTeamShipAnswersList = new DataTable();

        if (((txtSearch.Text == "SEARCH FOR A TEAMSHIP ANSWER") == false) && ((txtSearch.Text == "") == false))
        {
            string FilterExpression = "(strNameAnswer +' '+ strTitleAnswer) LIKE '%" + txtSearch.Text + "%'";
            dtTeamShipAnswersList = clsTeamShipAnswers.GetTeamShipAnswersList(FilterExpression, "");
        }
        else
        {
            dtTeamShipAnswersList = clsTeamShipAnswers.GetTeamShipAnswersList();
        }

        Session["dtTeamShipAnswersList"] = dtTeamShipAnswersList;

        PopulateFormData();
    }

    protected void dgrGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //### Method is used when the page index is changed.
        try
        {
            Session["TeamShipAnswersView_CurrentPageIndex"] = e.NewPageIndex;
            dgrGrid.CurrentPageIndex = e.NewPageIndex;
            PopulateFormData();
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgrGrid_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        //### Sort Method
        dtTeamShipAnswersList = new DataTable();

        if (Session["dtTeamShipAnswersList"] == null)
            dtTeamShipAnswersList = clsTeamShipAnswers.GetTeamShipAnswersList();
        else
            dtTeamShipAnswersList = (DataTable)Session["dtTeamShipAnswersList"];

        DataView dvTemp = dtTeamShipAnswersList.DefaultView;

        dvTemp.Sort = e.SortExpression;
        dtTeamShipAnswersList = dvTemp.ToTable();
        Session["dtTeamShipAnswersList"] = dtTeamShipAnswersList;

        PopulateFormData();
    }

    public void dgrGrid_PopEditLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkEditItem = (HyperLink)sender;
            DataGridItem dgrItemEdit = (DataGridItem)lnkEditItem.Parent.Parent;

            //### Get the iTeamShipAnswerID
            int iTeamShipAnswerID = 0;
            iTeamShipAnswerID = int.Parse(dgrItemEdit.Cells[0].Text);

            //### Add attributes to edit link
            lnkEditItem.Attributes.Add("href", "frmTeamShipAnswersAddModify.aspx?action=edit&iTeamShipAnswerID=" + iTeamShipAnswerID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

     public void dgrGrid_PopDeleteLink(object sender, EventArgs e)
     {
          try
          {
             //### Add hyperlink to datagrid item
             HyperLink lnkDeleteItem = (HyperLink)sender;
             DataGridItem dgrItemDelete = (DataGridItem)lnkDeleteItem.Parent.Parent;

             //### Get the iTeamShipAnswerID
             int iTeamShipAnswerID = 0;
             iTeamShipAnswerID = int.Parse(dgrItemDelete.Cells[0].Text);

             //### Add attributes to delete link
             lnkDeleteItem.CssClass = "dgrDeleteLink";
             lnkDeleteItem.Attributes.Add("href", "frmTeamShipAnswersView.aspx?action=delete&iTeamShipAnswerID=" + iTeamShipAnswerID);
           }
           catch (Exception ex)
           {
                 Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
           }
     }

    #endregion

    #region TeamShipAnswers FILTERING

    /// <summary>
    /// Enables the AutoCompleteExtender
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    /// 
    [WebMethod]
    [ScriptMethod]
    public static string[] FilterBusinessRule(string prefixText, int count)
    {
        string[] strReturnList = new string[] { };

        string FilterExpression = "(strNameAnswer +' '+ strTitleAnswer) LIKE '%" + prefixText + "%'";
        DataTable dtTeamShipAnswers = clsTeamShipAnswers.GetTeamShipAnswersList(FilterExpression, "");
        List<string> glstTeamShipAnswers = new List<string>();

        if (dtTeamShipAnswers.Rows.Count > 0)
        {
            foreach (DataRow dtrTeamShipAnswers in dtTeamShipAnswers.Rows)
            {
                glstTeamShipAnswers.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem("" + dtrTeamShipAnswers["strNameAnswer"].ToString() +' '+"" + dtrTeamShipAnswers["strTitleAnswer"].ToString(), dtrTeamShipAnswers["iTeamShipAnswerID"].ToString()));
            }
        }
        else
            glstTeamShipAnswers.Add("No TeamShipAnswers Available.");
        strReturnList = glstTeamShipAnswers.ToArray();
        return strReturnList;
    }

    #endregion
}
