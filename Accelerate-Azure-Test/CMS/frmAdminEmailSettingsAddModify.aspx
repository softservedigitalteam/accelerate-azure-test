<%@ Page Title="Admin Email Settings" Language="C#" MasterPageFile="~/CMS/CMS.master" AutoEventWireup="true" Inherits="CMS_frmAdminEmailSettingsAddModify" Codebehind="frmAdminEmailSettingsAddModify.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div id="mandatoryDiv" class="mandatoryInvalidDiv" runat="server" visible="false">
        <asp:Label ID="lblValidationMessage" runat="server"></asp:Label>
    </div>

     <%--<div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">AdminEmailSettings:</div>
         <div class="fieldDiv">
             <asp:DropDownList ID="lstAdminEmailSettings" runat="server" CssClass="roundedCornerDropDownList" Visible="false" onblur="setValid(this, 0);"></asp:DropDownList>
         </div>
         <br class="clearingSpacer" />
     </div>--%>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Admin Title:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtAdminTitle" runat="server" CssClass="roundedCornerTextBox" onKeyUp="return SetMaxLength(this,100)" />
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Email1:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtEmail1" runat="server" CssClass="roundedCornerTextBox" onKeyUp="return SetMaxLength(this,100)" />
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Email2:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtEmail2" runat="server" CssClass="roundedCornerTextBox" onKeyUp="return SetMaxLength(this,100)" />
         </div>
         <br class="clearingSpacer" />
     </div>


    
    <div class="Line"></div>
    <div class="buttonsRightDiv">
        <asp:LinkButton ID="lnkbtnBack" runat="server" CssClass="backButton" onclick="lnkbtnBack_Click" OnClientClick='history.go(-1);return false;' />
        <asp:LinkButton ID="lnkbtnSave" runat="server" CssClass="saveButton" onclick="lnkbtnSave_Click" />
        <asp:LinkButton ID="lnkbtnClear" runat="server" CssClass="clearButton" onclick="lnkbtnClear_Click" />
    </div>
</asp:Content>
