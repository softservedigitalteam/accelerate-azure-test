
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text;

/// <summary>
/// Summary description for clsProvincesView
/// </summary>
public partial class CMS_clsProvincesView : System.Web.UI.Page
{
    #region CLASSES AND VARIABLES

    clsUsers clsUsers;
    DataTable dtProvincesList;

    List<clsProvinces> glstProvinces;

    #endregion

    #region EVENT MODULES

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

dgrGrid.ItemCreated += new DataGridItemEventHandler(clsCommonFunctions.dgrGrid_PagerPopulate);

        if (!Page.IsPostBack)
        {
            Session["dtProvincesList"] = null;
            PopulateFormData();
        }
        else
        {
            if (Session["glstProvinces"] == null)
            {
                glstProvinces = new List<clsProvinces>();
                Session["glstProvinces"] = glstProvinces;
            }
            else
                glstProvinces = (List<clsProvinces>)Session["glstProvinces"];
        }
            PopulateFormData();

    }

    protected string EscapeLikeValue(string valueWithoutWildcards)
    {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < valueWithoutWildcards.Length; i++)
        {
            char c = valueWithoutWildcards[i];
            if (c == '*' || c == '%' || c == '[' || c == ']')
                sb.Append("[").Append(c).Append("]");
            else if (c == '\'')
                sb.Append("''");
            else
                sb.Append(c);
        }
        return sb.ToString();
    }

    protected void lnkbtnSearch_Click(object sender, EventArgs e)
    {
        string EscapedString = EscapeLikeValue(txtSearch.Text.ToString());
        string FilterExpression ="(strTitle) LIKE '%" + EscapedString + "%'";
        DataTable dtProvincesList = clsProvinces.GetProvincesList(FilterExpression, "");

        Session["dtProvincesList"] = dtProvincesList;

        PopulateFormData();
    }

    protected void lnkbtnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmProvincesAddModify.aspx");
    }

    #endregion

    #region DATA GRID METHODS

    private void PopulateFormData()
    {
        //### Populate datagrid using clsProvincesList object
        try
        {
            dtProvincesList = new DataTable();

            if (Session["dtProvincesList"] == null)
                dtProvincesList = clsProvinces.GetProvincesList();
            else
                dtProvincesList = (DataTable)Session["dtProvincesList"];

            dgrGrid.DataSource = dtProvincesList;

            //### Add this check to trap if the last item on the page is deleted
            if (Convert.ToInt16(dgrGrid.CurrentPageIndex) < Convert.ToInt16(Session["ProvincesView_CurrentPageIndex"]))
            {
                dgrGrid.CurrentPageIndex = 0;
            }
            else
            {
                if (Session["ProvincesView_CurrentPageIndex"] != null)
                {
                    dgrGrid.CurrentPageIndex = (int)Session["ProvincesView_CurrentPageIndex"];
                }
            }

            dgrGrid.DataBind();

        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void lnkDeleteItem_Click(object sender, EventArgs e)
    {
        int iProvinceID = int.Parse((sender as LinkButton).CommandArgument);

        clsProvinces.Delete(iProvinceID);

        DataTable dtProvincesList = new DataTable();

        if (((txtSearch.Text == "SEARCH FOR A PROVINCE") == false) && ((txtSearch.Text == "") == false))
        {
            string FilterExpression = "(strTitle) LIKE '%" + txtSearch.Text + "%'";
            dtProvincesList = clsProvinces.GetProvincesList(FilterExpression, "");
        }
        else
        {
            dtProvincesList = clsProvinces.GetProvincesList();
        }

        Session["dtProvincesList"] = dtProvincesList;

        PopulateFormData();
    }

    protected void dgrGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //### Method is used when the page index is changed.
        try
        {
            Session["ProvincesView_CurrentPageIndex"] = e.NewPageIndex;
            dgrGrid.CurrentPageIndex = e.NewPageIndex;
            PopulateFormData();
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgrGrid_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        //### Sort Method
        dtProvincesList = new DataTable();

        if (Session["dtProvincesList"] == null)
            dtProvincesList = clsProvinces.GetProvincesList();
        else
            dtProvincesList = (DataTable)Session["dtProvincesList"];

        DataView dvTemp = dtProvincesList.DefaultView;

        dvTemp.Sort = e.SortExpression;
        dtProvincesList = dvTemp.ToTable();
        Session["dtProvincesList"] = dtProvincesList;

        PopulateFormData();
    }

    public void dgrGrid_PopEditLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkEditItem = (HyperLink)sender;
            DataGridItem dgrItemEdit = (DataGridItem)lnkEditItem.Parent.Parent;

            //### Get the iProvinceID
            int iProvinceID = 0;
            iProvinceID = int.Parse(dgrItemEdit.Cells[0].Text);

            //### Add attributes to edit link
            lnkEditItem.Attributes.Add("href", "frmProvincesAddModify.aspx?action=edit&iProvinceID=" + iProvinceID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

     public void dgrGrid_PopDeleteLink(object sender, EventArgs e)
     {
          try
          {
             //### Add hyperlink to datagrid item
             HyperLink lnkDeleteItem = (HyperLink)sender;
             DataGridItem dgrItemDelete = (DataGridItem)lnkDeleteItem.Parent.Parent;

             //### Get the iProvinceID
             int iProvinceID = 0;
             iProvinceID = int.Parse(dgrItemDelete.Cells[0].Text);

             //### Add attributes to delete link
             lnkDeleteItem.CssClass = "dgrDeleteLink";
             lnkDeleteItem.Attributes.Add("href", "frmProvincesView.aspx?action=delete&iProvinceID=" + iProvinceID);
           }
           catch (Exception ex)
           {
                 Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
           }
     }

    #endregion

    #region Provinces FILTERING

    /// <summary>
    /// Enables the AutoCompleteExtender
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    /// 
    [WebMethod]
    [ScriptMethod]
    public static string[] FilterBusinessRule(string prefixText, int count)
    {
        string[] strReturnList = new string[] { };

        string FilterExpression = "(strTitle) LIKE '%" + prefixText + "%'";
        DataTable dtProvinces = clsProvinces.GetProvincesList(FilterExpression, "");
        List<string> glstProvinces = new List<string>();

        if (dtProvinces.Rows.Count > 0)
        {
            foreach (DataRow dtrProvinces in dtProvinces.Rows)
            {
                glstProvinces.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem("" + dtrProvinces["strTitle"].ToString(), dtrProvinces["iProvinceID"].ToString()));
            }
        }
        else
            glstProvinces.Add("No Provinces Available.");
        strReturnList = glstProvinces.ToArray();
        return strReturnList;
    }

    #endregion
}
