using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Net.Mail;

public partial class CMS_frmCompaniesAddModify : System.Web.UI.Page
{
    clsUsers clsUsers;
    clsCompanies clsCompanies;

    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

        //### Determines if a javascript delete has been called
        //if (!String.IsNullOrEmpty(Page.Request["__EVENTARGUMENT"]) && (Page.Request["__EVENTARGUMENT"].Split(':')[0] == "iRemoveImages"))
        //    DeleteImages(Convert.ToInt32(Page.Request["__EVENTARGUMENT"].Split(':')[1]));

        if (!IsPostBack)
        {
            //### If the iCompanyID is passed through then we want to instantiate the object with that iCompanyID
            if ((Request.QueryString["iCompanyID"] != "") && (Request.QueryString["iCompanyID"] != null))
            {
                clsCompanies = new clsCompanies(Convert.ToInt32(Request.QueryString["iCompanyID"]));

                //### Populate the form
                popFormData();
            }
            else
            {
                clsCompanies = new clsCompanies();
            }
            Session["clsCompanies"] = clsCompanies;
        }
        else
        {
            //if (IsPostBack && FileUpload.PostedFile != null)
            //{
            //    if (FileUpload.PostedFile.FileName.Length > 0)
            //    {
            //        if (Session["lstImages"] == null)
            //        {
            //            lstImages = new List<string>();
            //            Session["lstImages"] = lstImages;
            //        }
            //        //### Check that they have ONLY HAVE MAX NUMBER OF Images in the datalist
            //        if (dlImages.Items.Count == iMaxImages)
            //        {
            //            mandatoryDiv.Visible = true;
            //            lblValidationMessage.Text = "You can only have " + iMaxImages.ToString() + " Images.";
            //            getList(lblUniquePath.Text);
            //        }
            //        else
            //        {
            //            mandatoryDiv.Visible = false;

            //            string strUniquePath;
            //            if (lblUniquePath.Text == "")
            //            {
            //                strUniquePath = GetUniquePath();
            //                lblUniquePath.Text = strUniquePath;
            //            }
            //            else
            //            {
            //                strUniquePath = lblUniquePath.Text;
            //            }
            //            UploadImages(strUniquePath);
            //            getList(strUniquePath);
            //        }
            //    }
            //}

            clsCompanies = (clsCompanies)Session["clsCompanies"];
        }
    }

    protected void lnkbtnBack_Click(object sender, EventArgs e)
    {
        //### Go back to previous page
        Response.Redirect("frmAccountUsersView.aspx");
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        //### Validate registration process
        bool bCanSave = true;

        bCanSave = clsValidation.IsNullOrEmpty(txtCompanyName, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtDescription, bCanSave);
        bCanSave = clsValidation.IsNullOrEmpty(txtTagLine, bCanSave);

        if (bCanSave == true)
        {
            mandatoryDiv.Visible = false;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceHappy.png\" alt='' title=''/><div class=\"validationMessage\">User added successfully</div></div>";
            SaveData();
        }
        else
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">Please fill out all mandatory fields - User not added</div></div>";
        }
    }

    protected void lnkbtnClear_Click(object sender, EventArgs e)
    {
        mandatoryDiv.Visible = false;

        txtCompanyName.Text = "";
        txtDescription.Text = "";
        txtTagLine.Text = "";
        //txtPrimaryColor.Text = "";
        //txtSecondaryColor.Text = "";
        //txtTertiaryColor.Text = "";
        txtPrimaryContact.Text = "";
        txtPrimaryEmail.Text = "";
        txtPrimaryTelephone.Text = "";
    }

    #endregion

    #region POPULATE DATA METHODS

    private void popFormData()
    {
        txtCompanyName.Text = clsCompanies.strTitle;
        txtTagLine.Text = clsCompanies.strTagLine;
        txtDescription.Text = clsCompanies.strDescription;
        //txtPrimaryColor.Text = clsCompanies.strPrimaryColor;
        //txtSecondaryColor.Text = clsCompanies.strSecondaryColor;
        //txtTertiaryColor.Text = clsCompanies.strTertiaryColor;
        txtPrimaryContact.Text = clsCompanies.strPrimaryContact;
        txtPrimaryEmail.Text = clsCompanies.strPrimaryEmail;
        txtPrimaryTelephone.Text = clsCompanies.strPrimaryTelephone;
    }

    #endregion

    #region SAVE DATA METHODS

    private void SaveData()
    {
        if (Session["clsCompanies"] == null)
        {
            if ((clsCommonFunctions.DoesRecordExist("tblCompanies", "strTitle='" + txtCompanyName.Text + "' AND bIsDeleted = 0") == true))
            {
                mandatoryDiv.Visible = true;
                lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">This Company already exist, please choose a new company name</div></div>";
            }
            else
            {
                try
                {
                    //### Add / Update
                    mandatoryDiv.Visible = false;

                    clsCompanies.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
                    clsCompanies.iAddedBy = clsUsers.iUserID;
                    clsCompanies.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
                    clsCompanies.iEditedBy = clsUsers.iUserID;
                    clsCompanies.strTitle = txtCompanyName.Text;
                    clsCompanies.strDescription = txtDescription.Text;
                    clsCompanies.strTagLine = txtTagLine.Text;
                    clsCompanies.strPrimaryColor = ""; /*txtPrimaryColor.Text;*/
                    clsCompanies.strSecondaryColor = "";/*txtSecondaryColor.Text;*/
                    clsCompanies.strTertiaryColor = "";/*txtTertiaryColor.Text;*/
                    clsCompanies.strPrimaryContact = txtPrimaryContact.Text;
                    clsCompanies.strPrimaryEmail = txtPrimaryEmail.Text;

                    clsConfigurations clsConfig = new clsConfigurations();

                    clsConfig.strEmail = txtPrimaryEmail.Text;
                    clsConfig.dtAdded = DateTime.Now;
                    clsConfig.iAddedBy = clsUsers.iUserID;

                    clsCompanies.strPrimaryTelephone = txtPrimaryTelephone.Text;

                    clsCompanies.Update();
                    clsConfig.Update();

                    Response.Redirect("frmCompaniesView.aspx");

                }
                catch
                {
                    Exception ex;
                }
            }
        }
        else
        {
            mandatoryDiv.Visible = false;

            clsCompanies.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            clsCompanies.iAddedBy = clsUsers.iUserID;
            clsCompanies.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            clsCompanies.iEditedBy = clsUsers.iUserID;
            clsCompanies.strTitle = txtCompanyName.Text;
            clsCompanies.strDescription = txtDescription.Text;
            clsCompanies.strTagLine = txtTagLine.Text;
            clsCompanies.strPrimaryColor = ""; /*txtPrimaryColor.Text;*/
            clsCompanies.strSecondaryColor = "";/*txtSecondaryColor.Text;*/
            clsCompanies.strTertiaryColor = "";/*txtTertiaryColor.Text;*/
            clsCompanies.strPrimaryContact = txtPrimaryContact.Text;
            clsCompanies.strPrimaryEmail = txtPrimaryEmail.Text;

            clsConfigurations clsConfig = new clsConfigurations();

            clsConfig.strEmail = txtPrimaryEmail.Text;
            clsConfig.dtAdded = DateTime.Now;
            clsConfig.iAddedBy = clsUsers.iUserID;

            clsCompanies.strPrimaryTelephone = txtPrimaryTelephone.Text;

            clsCompanies.Update();
            clsConfig.Update();

            Response.Redirect("frmCompaniesView.aspx");
        }
    }

    #endregion

//    #region IMAGE METHODS

//    List<string> lstImages;
//    List<string> lstImagesFileNames;
//    int iMaxImages = 1;
//    string strUniqueFullPath = AppDomain.CurrentDomain.BaseDirectory + "\\AccountUsers";

//    protected void btnUpload_Click(object sender, EventArgs e)
//    {
//        if (Session["lstImages"] == null)
//        {
//            lstImages = new List<string>();
//            Session["lstImages"] = lstImages;
//        }
//        //### Check that they have ONLY HAVE MAX NUMBER OF Images in the datalist
//        if (dlImages.Items.Count == iMaxImages)
//        {
//            mandatoryDiv.Visible = true;
//            lblValidationMessage.Text = "You can only have " + iMaxImages.ToString() + " Images.";
//            getList(lblUniquePath.Text);
//        }
//        else
//        {
//            mandatoryDiv.Visible = false;

//            string strUniquePath;
//            if (lblUniquePath.Text == "")
//            {
//                strUniquePath = GetUniquePath();
//                lblUniquePath.Text = strUniquePath;
//            }
//            else
//            {
//                strUniquePath = lblUniquePath.Text;
//            }
//            UploadImages(strUniquePath);
//            getList(strUniquePath);
//        }
//    }

//    private string GetUniquePath()
//    {
//        int iCount = 1;
//        //### First we need to get the path
//        while (System.IO.Directory.Exists(strUniqueFullPath + "\\AccountUsers" + iCount) == true)
//        {
//            iCount++;
//        }
//        return "AccountUsers" + iCount;
//    }

//    protected void UploadImages(String strUniquePath)
//    {
//        if (FileUpload.PostedFile.ContentLength > 0 && FileUpload.PostedFile.ContentLength < 1073741824)
//        {

//            //### Upload files to unique folder
//            string strUploadFileName = "";
//            strUploadFileName = System.IO.Path.GetFileName(FileUpload.PostedFile.FileName);
//            string strSaveLocation = "";
//            strSaveLocation = strUniqueFullPath + "\\" + strUniquePath + "\\" + strUploadFileName;

//            if (!System.IO.Directory.Exists(strUniqueFullPath + "\\" + strUniquePath))
//            {
//                System.IO.Directory.CreateDirectory(strUniqueFullPath + "\\" + strUniquePath);
//            }
//            FileUpload.PostedFile.SaveAs(strSaveLocation);

//            CopyAndResizePic(strSaveLocation);
//            getList(strUniqueFullPath + "\\" + strUniquePath);
//        }
//        else
//        {
//            lblValidationMessage.Text = "The file should be between 0 and 1Mb.";
//            lblUploadError.Text = "The file should be between 0 and 1Mb.";
//        }
//    }

//    private void CopyAndResizePic(String strFullPath)
//    {
//        try
//        {

//            String strFileName;
//            String strNewFilePath;

//            //### Main Images
//            String strLrgFileName;

//            strFileName = Path.GetFileName(strFullPath);
//            strNewFilePath = strFullPath.Replace(strFileName, "");
//            strNewFilePath = strNewFilePath + Path.GetFileNameWithoutExtension(strFullPath);
//            strNewFilePath = strNewFilePath + "_lrg";
//            strNewFilePath = strNewFilePath + Path.GetExtension(strFullPath);

//            File.Copy(strFullPath, strNewFilePath);
//            strLrgFileName = Path.GetFileName(strNewFilePath);

//            clsCommonFunctions.ResizeImage(strNewFilePath, strNewFilePath, 600, 600, false);

//            //### Thumbnail
//            String strSmlFileName;

//            strFileName = Path.GetFileName(strFullPath);
//            strNewFilePath = strFullPath.Replace(strFileName, "");
//            strNewFilePath = strNewFilePath + Path.GetFileNameWithoutExtension(strFullPath);
//            strNewFilePath = strNewFilePath + "_sml";
//            strNewFilePath = strNewFilePath + Path.GetExtension(strFullPath);

//            File.Copy(strFullPath, strNewFilePath);
//            strSmlFileName = Path.GetFileName(strNewFilePath);

//            clsCommonFunctions.ResizeImage(strNewFilePath, strNewFilePath, 140, 140, false);

//        }
//        catch (Exception ex) { }
//    }

//    public void getList(String strPathToFolder)
//    {
//        lstImages = new List<string>();
//        lstImagesFileNames = new List<string>();
//        try
//        {
//            string strPath = strPathToFolder;
//            string[] files = Directory.GetFiles(strUniqueFullPath + "\\" + strPath);

//            string iCompanyID = "";
//            if (!string.IsNullOrEmpty(Request.QueryString["iCompanyID"]))
//                iCompanyID = Request.QueryString["iCompanyID"];

//            int iImagesCount = 0;

//            foreach (string strName in files)
//            {
//                if (strName.IndexOf("_sml") != -1)
//                {
//                    string strHTMLImages = strName.Replace(AppDomain.CurrentDomain.BaseDirectory + "\\", "..\\");
//                    strHTMLImages = strHTMLImages.Replace("\\", "/");

//                    //### Generates a javascript postback for the delete method
//                    String strPostBack = Page.ClientScript.GetPostBackEventReference(this, "iRemoveImages:" + iImagesCount);

//                    lstImages.Add(@"<tr style='text-align: centre;'>
//                                        <td style='text-align: centre;'>
//                                            <a class='ImagesColorBox' href='" + strHTMLImages.Replace("_sml", "_lrg") + "'><img src='" + strHTMLImages + @"' alt='' title='' style='border: solid 5px #ffffff;' /></a><br /><br />" +
//                                            "<center><div class='buttonStyle' style='padding-top: 5px;'><a href=\"javascript:" + strPostBack + "\" style='color: #ffffff;'><div class='deleteButton'></div></a></div></center>" + @"
//                                        </td>
//                                    </tr>");
//                    lstImagesFileNames.Add(Path.GetFileName(strName).Replace("_sml", ""));
//                    iImagesCount++;
//                }
//            }
//            dlImages.DataSource = lstImages;
//            dlImages.DataBind();

//            Session["lstImages"] = lstImages;
//            Session["lstImagesFileNames"] = lstImagesFileNames;
//        }
//        catch (Exception ex) { }
//    }

//    private string GetMainImagePath(DataList dtlTarget)
//    {
//        string strReturn = "";

//        foreach (DataListItem dliTarget in dtlTarget.Items)
//        {
//            RadioButton rdbMainImage = (RadioButton)dliTarget.FindControl("rdbMainImage");
//            if (rdbMainImage.Checked)
//            {
//                lstImagesFileNames = (List<string>)Session["lstImagesFileNames"];
//                strReturn = lstImagesFileNames[dliTarget.ItemIndex];
//                break;
//            }
//        }
//        return strReturn;
//    }

//    private void DeleteImages(int iImagesIndex)
//    {

//        //### Deletes all Images related to the target Images.
//        lstImages = (List<string>)Session["lstImages"];
//        lstImagesFileNames = (List<string>)Session["lstImagesFileNames"];

//        lstImages.RemoveAt(iImagesIndex);
//        Session["lstImages"] = lstImages;

//        string[] files = Directory.GetFiles(strUniqueFullPath + "\\" + lblUniquePath.Text);
//        foreach (string file in files)
//        {
//            if (Path.GetFileName(file) == lstImagesFileNames[iImagesIndex].ToString())
//            {
//                //### Remove all Images
//                File.Delete(file);
//                File.Delete(file.Replace(Path.GetExtension(file), "_sml" + Path.GetExtension(file)));
//                File.Delete(file.Replace(Path.GetExtension(file), "_med" + Path.GetExtension(file)));
//                File.Delete(file.Replace(Path.GetExtension(file), "_lrg" + Path.GetExtension(file)));
//                break;
//            }
//        }
//        lstImagesFileNames.RemoveAt(iImagesIndex);
//        ViewState["lstImagesFileNames"] = lstImagesFileNames;
//        getList(lblUniquePath.Text);
//    }

//    #endregion

    #region REPORT SECTION

    //protected void SendReport()
    //{
    //    string strReportName = "Welcome";

    //    StringBuilder strbMailBuilder = new StringBuilder();

    //    strbMailBuilder.AppendLine("<!-- Start of textbanner -->");
    //    strbMailBuilder.AppendLine("<table width='100%' bgcolor='#fff' cellpadding='0' cellspacing='0' border='0' id='backgroundTable' movable=''>");
    //    strbMailBuilder.AppendLine("<tbody>");
    //    strbMailBuilder.AppendLine("<tr>");
    //    strbMailBuilder.AppendLine("<td>");
    //    strbMailBuilder.AppendLine("<table bgcolor='#ffffff' width='650' cellpadding='0' cellspacing='0' border='0' align='center' class='devicewidth' options=''>"); //style='border-left:1px solid #333; border-right:1px solid #333;'
    //    strbMailBuilder.AppendLine("<tbody>");
    //    strbMailBuilder.AppendLine("<!-- Spacing -->");
    //    strbMailBuilder.AppendLine("<tr>");
    //    strbMailBuilder.AppendLine("<td height='50'></td>");
    //    strbMailBuilder.AppendLine("</tr>");
    //    strbMailBuilder.AppendLine("<!-- End of Spacing -->");
    //    strbMailBuilder.AppendLine("<tr>");
    //    strbMailBuilder.AppendLine("<td>");
    //    strbMailBuilder.AppendLine("<table width='100%' cellspacing='0' cellpadding='0'>");
    //    strbMailBuilder.AppendLine("<tbody>");

    //    strbMailBuilder.AppendLine("<!-- Content -->");
    //    strbMailBuilder.AppendLine("<tr>");
    //    strbMailBuilder.AppendLine("<td width='60'></td>");
    //    strbMailBuilder.AppendLine("<td valign='top' style='font-family:Arial;font-size: 15px; color: #3b3b3b; text-align:left;line-height: 22px;' text=''>");

    //    strbMailBuilder.AppendLine("Hi " + txtFirstName.Text + ",<br /><br />");
    //    strbMailBuilder.AppendLine("Congratulation on joining Deloitte.<br/>As part of your onboarding experience, you will be introduced to Deloitte through an interactive, engaging gamified experience that is guaranteed tochallenge, engage and excite you.<br /><br/>");//strbMailBuilder.AppendLine("Congratulation on joining Deloitte Risk Advisory.As part of your onboarding experience, you will be introduced to Risk Advisory through an interactive, engaging <b>gamified</b> experience that is guaranteed tochallenge, engage and excite you.<br /><br/>");
    //    strbMailBuilder.AppendLine("Login to the Accelerate website using the following credentials to kick-off your first mission.<br /><br/>");//strbMailBuilder.AppendLine("Login to the Accelerate website using the following credentials to kick-off your first mission.<br /><br/>");

    //    strbMailBuilder.AppendLine("Username: " + txtEmailAddress.Text + "<br/>");
    //    strbMailBuilder.AppendLine("Password: " + txtEmailAddress.Text + "<br/>");
    //    strbMailBuilder.AppendLine("Link: <a href='" + System.Configuration.ConfigurationManager.AppSettings["WebRoot"] + "'>" + System.Configuration.ConfigurationManager.AppSettings["WebRoot"] + "</a><br/><br/>");

    //    strbMailBuilder.AppendLine("<h2>Why Onboarding?</h2>");//strbMailBuilder.AppendLine("<h2>Why Gamification?</h2>");
    //    strbMailBuilder.AppendLine("We are committed to you and your success at Deloitte, which is why we have created a tailor made solution that you help you get a jump start on your career!<br/>");//strbMailBuilder.AppendLine("We are committed to you and your success at Deloitte, which is why we have created a tailor made solution that you help you get a jump start on your career!Think happier, more engaged, integrated employees.<br/>");
    //    strbMailBuilder.AppendLine("<h2>What will you learn?</h2>");
    //    strbMailBuilder.AppendLine("By completing missions on the system, you will earn points and ultimately will get to know your Deloitte, your Consulting and your leadership better.<br/>");
    //    //strbMailBuilder.AppendLine("<i>*Completing all missions also earns you Challenger Series points, increasing your chances of winning iPads, gift vouchers and other great prizes.</i><br/>");

    //    strbMailBuilder.AppendLine("Learn, network, earn rewards.<br/>");
    //    strbMailBuilder.AppendLine("Regards,<br/>");
    //    strbMailBuilder.AppendLine("The Onboarding team<br/>");
    //    strbMailBuilder.AppendLine("0842809206");
    //    strbMailBuilder.AppendLine("</td>");
    //    strbMailBuilder.AppendLine("<td width='60'></td>");
    //    strbMailBuilder.AppendLine("</tr>");
    //    strbMailBuilder.AppendLine("<!-- End of Content -->");

    //    strbMailBuilder.AppendLine("</td>");
    //    strbMailBuilder.AppendLine("<td width='20'></td>");
    //    strbMailBuilder.AppendLine("</tr>");
    //    strbMailBuilder.AppendLine("</tbody>");
    //    strbMailBuilder.AppendLine("</table>");
    //    strbMailBuilder.AppendLine("</td>");
    //    strbMailBuilder.AppendLine("</tr>");
    //    strbMailBuilder.AppendLine("<!-- Spacing -->");
    //    strbMailBuilder.AppendLine("<tr>");
    //    strbMailBuilder.AppendLine("<td height='50'></td>");
    //    strbMailBuilder.AppendLine("</tr>");
    //    strbMailBuilder.AppendLine("<!-- End of Spacing -->");
    //    strbMailBuilder.AppendLine("</tbody>");
    //    strbMailBuilder.AppendLine("</table>");
    //    strbMailBuilder.AppendLine("</td>");
    //    strbMailBuilder.AppendLine("</tr>");
    //    strbMailBuilder.AppendLine("</tbody>");
    //    strbMailBuilder.AppendLine("</table>");
    //    strbMailBuilder.AppendLine("<!-- End of textbanner -->");

    //    Attachment[] empty = new Attachment[] { };

    //    try
    //    {
    //        emailComponent.SendMail("no-reply@consultingonboarding.co.za", txtEmailAddress.Text, "", "andrew@softservedigital.co.za", strReportName, strbMailBuilder.ToString(), "Heading", "Subheading", empty, true);
    //    }
    //    catch { }

    //    //### Redirect
    //    lblValidationMessage.Text = "<div class=\"validationImageCorrectLogin\"></div><div class=\"validationLabel\">Your email has been sent.</div>";
    //}

    #endregion
}
