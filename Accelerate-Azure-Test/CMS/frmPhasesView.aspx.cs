
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text;

/// <summary>
/// Summary description for clsPhasesView
/// </summary>
public partial class CMS_clsPhasesView : System.Web.UI.Page
{
    #region CLASSES AND VARIABLES

    clsUsers clsUsers;
    DataTable dtPhasesList;

    List<clsPhases> glstPhases;

    #endregion

    #region EVENT MODULES

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

dgrGrid.ItemCreated += new DataGridItemEventHandler(clsCommonFunctions.dgrGrid_PagerPopulate);

        if (!Page.IsPostBack)
        {
            Session["dtPhasesList"] = null;
            PopulateFormData();
        }
        else
        {
            if (Session["glstPhases"] == null)
            {
                glstPhases = new List<clsPhases>();
                Session["glstPhases"] = glstPhases;
            }
            else
                glstPhases = (List<clsPhases>)Session["glstPhases"];
        }
            PopulateFormData();

    }

    protected string EscapeLikeValue(string valueWithoutWildcards)
    {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < valueWithoutWildcards.Length; i++)
        {
            char c = valueWithoutWildcards[i];
            if (c == '*' || c == '%' || c == '[' || c == ']')
                sb.Append("[").Append(c).Append("]");
            else if (c == '\'')
                sb.Append("''");
            else
                sb.Append(c);
        }
        return sb.ToString();
    }

    protected void lnkbtnSearch_Click(object sender, EventArgs e)
    {
        string EscapedString = EscapeLikeValue(txtSearch.Text.ToString());
        string FilterExpression ="(strTitle +' '+ strMasterImage) LIKE '%" + EscapedString + "%'";
        DataTable dtPhasesList = clsPhases.GetPhasesList(FilterExpression, "");

        Session["dtPhasesList"] = dtPhasesList;

        PopulateFormData();
    }

    protected void lnkbtnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmPhasesAddModify.aspx");
    }

    #endregion

    #region DATA GRID METHODS

    private void PopulateFormData()
    {
        //### Populate datagrid using clsPhasesList object
        try
        {
            dtPhasesList = new DataTable();

            if (Session["dtPhasesList"] == null)
                dtPhasesList = clsPhases.GetPhasesList("", "dtAdded DESC");
            else
                dtPhasesList = (DataTable)Session["dtPhasesList"];

            dgrGrid.DataSource = dtPhasesList;

            //### Add this check to trap if the last item on the page is deleted
            if (Convert.ToInt16(dgrGrid.CurrentPageIndex) < Convert.ToInt16(Session["PhasesView_CurrentPageIndex"]))
            {
                dgrGrid.CurrentPageIndex = 0;
            }
            else
            {
                if (Session["PhasesView_CurrentPageIndex"] != null)
                {
                    dgrGrid.CurrentPageIndex = (int)Session["PhasesView_CurrentPageIndex"];
                }
            }

            dgrGrid.DataBind();

        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void lnkDeleteItem_Click(object sender, EventArgs e)
    {
        int iPhaseID = int.Parse((sender as LinkButton).CommandArgument);

        clsPhases.Delete(iPhaseID);

        DataTable dtPhasesList = new DataTable();

        if (((txtSearch.Text == "SEARCH FOR A PHASE") == false) && ((txtSearch.Text == "") == false))
        {
            string FilterExpression = "(strTitle +' '+ strMasterImage) LIKE '%" + txtSearch.Text + "%'";
            dtPhasesList = clsPhases.GetPhasesList(FilterExpression, "");
        }
        else
        {
            dtPhasesList = clsPhases.GetPhasesList();
        }

        Session["dtPhasesList"] = dtPhasesList;

        PopulateFormData();
    }

    protected void dgrGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //### Method is used when the page index is changed.
        try
        {
            Session["PhasesView_CurrentPageIndex"] = e.NewPageIndex;
            dgrGrid.CurrentPageIndex = e.NewPageIndex;
            PopulateFormData();
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgrGrid_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        //### Sort Method
        dtPhasesList = new DataTable();

        if (Session["dtPhasesList"] == null)
            dtPhasesList = clsPhases.GetPhasesList();
        else
            dtPhasesList = (DataTable)Session["dtPhasesList"];

        DataView dvTemp = dtPhasesList.DefaultView;

        dvTemp.Sort = e.SortExpression;
        dtPhasesList = dvTemp.ToTable();
        Session["dtPhasesList"] = dtPhasesList;

        PopulateFormData();
    }

    public void dgrGrid_PoplnkAddMissionsToShopLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkAddMissionsToShop = (HyperLink)sender;
            DataGridItem dgrItemAddMissions = (DataGridItem)lnkAddMissionsToShop.Parent.Parent;

            //### Get the iThemedShopID
            int iPhaseID = 0;
            iPhaseID = int.Parse(dgrItemAddMissions.Cells[0].Text);

            //### Add attributes to edit link
            lnkAddMissionsToShop.Attributes.Add("href", "frmPhaseMissionsView.aspx?iPhaseID=" + iPhaseID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    public void dgrGrid_PopEditLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkEditItem = (HyperLink)sender;
            DataGridItem dgrItemEdit = (DataGridItem)lnkEditItem.Parent.Parent;

            //### Get the iPhaseID
            int iPhaseID = 0;
            iPhaseID = int.Parse(dgrItemEdit.Cells[0].Text);

            //### Add attributes to edit link
            lnkEditItem.Attributes.Add("href", "frmPhasesAddModify.aspx?action=edit&iPhaseID=" + iPhaseID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

     public void dgrGrid_PopDeleteLink(object sender, EventArgs e)
     {
          try
          {
             //### Add hyperlink to datagrid item
             HyperLink lnkDeleteItem = (HyperLink)sender;
             DataGridItem dgrItemDelete = (DataGridItem)lnkDeleteItem.Parent.Parent;

             //### Get the iPhaseID
             int iPhaseID = 0;
             iPhaseID = int.Parse(dgrItemDelete.Cells[0].Text);

             //### Add attributes to delete link
             lnkDeleteItem.CssClass = "dgrDeleteLink";
             lnkDeleteItem.Attributes.Add("href", "frmPhasesView.aspx?action=delete&iPhaseID=" + iPhaseID);
           }
           catch (Exception ex)
           {
                 Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
           }
     }

    #endregion

    #region Phases FILTERING

    /// <summary>
    /// Enables the AutoCompleteExtender
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    /// 
    [WebMethod]
    [ScriptMethod]
    public static string[] FilterBusinessRule(string prefixText, int count)
    {
        string[] strReturnList = new string[] { };

        string FilterExpression = "(strTitle +' '+ strMasterImage) LIKE '%" + prefixText + "%'";
        DataTable dtPhases = clsPhases.GetPhasesList(FilterExpression, "");
        List<string> glstPhases = new List<string>();

        if (dtPhases.Rows.Count > 0)
        {
            foreach (DataRow dtrPhases in dtPhases.Rows)
            {
                glstPhases.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem("" + dtrPhases["strTitle"].ToString() +' '+"" + dtrPhases["strMasterImage"].ToString(), dtrPhases["iPhaseID"].ToString()));
            }
        }
        else
            glstPhases.Add("No Phases Available.");
        strReturnList = glstPhases.ToArray();
        return strReturnList;
    }

    #endregion
}
