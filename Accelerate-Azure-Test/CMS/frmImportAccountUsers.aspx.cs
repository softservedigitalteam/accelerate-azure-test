using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.Data.OleDb;
using System.Text;
using System.Net.Mail;
using System.Text.RegularExpressions;

public partial class CMS_frmImportAccountUsers : System.Web.UI.Page
{
    clsUsers clsUsers;
    clsAccountUsers clsAccountUsers;

    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

        if (!IsPostBack)
        {
            //### If the iAccountUserID is passed through then we want to instantiate the object with that iAccountUserID
            if ((Request.QueryString["iAccountUserID"] != "") && (Request.QueryString["iAccountUserID"] != null))
            {
                clsAccountUsers = new clsAccountUsers(Convert.ToInt32(Request.QueryString["iAccountUserID"]));

                //### Populate the form
            }
            else
            {
                clsAccountUsers = new clsAccountUsers();
            }
            Session["clsAccountUsers"] = clsAccountUsers;
        }
        else
        {
            clsAccountUsers = (clsAccountUsers)Session["clsAccountUsers"];
        }
        
    }

    protected void lnkbtnBack_Click(object sender, EventArgs e)
    {
        //### Go back to previous page
        Response.Redirect("frmInactiveAccountUsersView.aspx");
    }

    #endregion

    #region SAVE DATA METHODS

    private void SaveData()
    {
        Session["dtAccountUsersList"] = null;

        //### Go back to view page
        Response.Redirect("frmInactiveAccountUsersView.aspx");
    }

    #endregion

    protected void btnUpload_Click(object sender, EventArgs e)
    {
        if (FileUpload.HasFile)
        {
            string FileName = Path.GetFileName(FileUpload.PostedFile.FileName);
            string Extension = Path.GetExtension(FileUpload.PostedFile.FileName);
            string FolderPath = ConfigurationManager.AppSettings["FolderPath"];

            string FilePath = Server.MapPath(FolderPath + FileName);
            FileUpload.SaveAs(FilePath);
            Import_User(FilePath, Extension);
        }
    }

    private void Import_User(string FilePath, string Extension)
    {
        string conStr = "";
        int iNumberOfInactiveUsers = 0;
        switch (Extension)
        {
            case ".xls": //Excel 97-03
                conStr = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
                break;

            case ".xlsx": //Excel 07
                conStr = ConfigurationManager.ConnectionStrings["Excel07ConString"].ConnectionString;
                break;
        }

        conStr = String.Format(conStr, FilePath);
        OleDbConnection connExcel = new OleDbConnection(conStr);
        OleDbCommand cmdExcel = new OleDbCommand();
        OleDbDataAdapter oda = new OleDbDataAdapter();
        //DataTable dt = new DataTable();
        cmdExcel.Connection = connExcel;

        //Get the name of First Sheet
        connExcel.Open();
        DataTable dtExcelSchema;
        dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);

        int iNumberOfSheetCount = dtExcelSchema.Rows.Count;
        int iNumberOfColoumns = dtExcelSchema.Columns.Count;

        string SheetName = dtExcelSchema.Rows[iNumberOfSheetCount - 1]["TABLE_NAME"].ToString();

        //DataTable dtAllData = clsAccountUsers.GetAccountUsersList("iAccountUserID=" + clsAccountUsers.iAccountUserID, "");

        cmdExcel.CommandText = "SELECT * From [" + SheetName + "]";
        oda.SelectCommand = cmdExcel;
        oda.Fill(dtExcelSchema);
        dtExcelSchema.Rows.Count.Equals(1);
        string strEmail = "";

        foreach (DataRow dtr in dtExcelSchema.Rows)
        {
            strEmail = strEmail.ToString() + "<br/>" + dtr["F3"].ToString();

            try
            {
                clsInactiveAccountUsers clsInactiveAccountUsers = new clsInactiveAccountUsers();

                //removing white space from email address
                string strEmailAddress = dtr["F3"].ToString();
                strEmailAddress = strEmailAddress.Trim();
                strEmailAddress = Regex.Replace(strEmailAddress, @"\s", "");
                strEmailAddress = strEmailAddress.ToLower();

                //if (clsCommonFunctions.DoesRecordExist("tblAccountUsers", "strEmailAddress = '" + dtr["F3"].ToString() + "' AND bIsDeleted = 0") == true)
                //{
                //    mandatoryDiv.Visible = true;
                //    lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\"> These emails could not be added because they already on the system <br/>"  + strEmail +"</div></div>";
                //}
                if (clsCommonFunctions.DoesRecordExist("tblInactiveAccountUsers", "strEmailAddress = '" + strEmailAddress + "' AND bIsDeleted = 0") == true)
                {
                    mandatoryDiv.Visible = true;
                    lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\"> These emails could not be added because they already on the system <br/>" + strEmail + "</div></div>";
                }
                else
                {
                    //### Add / Update
                    mandatoryDiv.Visible = false;
                    if (dtr["F3"].ToString() != "Email Address" && dtr["F3"].ToString() != "")
                    {
                        clsInactiveAccountUsers.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
                        clsInactiveAccountUsers.iAddedBy = clsUsers.iUserID;
                        //clsInactiveAccountUsers.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
                        //clsInactiveAccountUsers.iEditedBy = clsUsers.iUserID;

                        clsInactiveAccountUsers.strFirstName = dtr["F1"].ToString();
                        clsInactiveAccountUsers.strSurname = dtr["F2"].ToString();

                        clsInactiveAccountUsers.strEmailAddress = strEmailAddress;

                        clsInactiveAccountUsers.strPhoneNumber = dtr["F4"].ToString();
                        clsInactiveAccountUsers.strJobTitle = dtr["F5"].ToString();
                        clsInactiveAccountUsers.iGroupID = 0;
                        clsInactiveAccountUsers.Update();
                        iNumberOfInactiveUsers++;
                    }

                    //if (clsAccountUsers.strEmailAddress == "")
                    //{
                    //    //### dONT Anything
                    //}
                    //else
                    //{
                        //## SAVE DATA METHODS
                        //clsAccountUsers.iGroupID = Convert.ToInt32(dtr["F4"]);
                        //clsAccountUsers.iCurrentPhaseID = Convert.ToInt32(dtr["F5"]);
                        //clsAccountUsers.iCurrentPoints = Convert.ToInt32(dtr["F6"]);
                        //clsAccountUsers.bHasCompletedProfileMission = false;
                        //clsAccountUsers.Update();

                        //## SEND REPORT
                        //SendReport(clsAccountUsers.strEmailAddress, clsAccountUsers.strFirstName);

                        #region ADDING STUFF
                        //#####################################

                        //### Add / Update

                        //mandatoryDiv.Visible = false;

                        //clsAccountUsers.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
                        //clsAccountUsers.iAddedBy = clsUsers.iUserID;
                        //clsAccountUsers.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
                        //clsAccountUsers.iEditedBy = clsUsers.iUserID;
                        //clsAccountUsers.strFirstName = txtFirstName.Text;
                        //clsAccountUsers.strSurname = txtSurname.Text;
                        //clsAccountUsers.strEmailAddress = txtEmailAddress.Text.ToLower();
                        //clsAccountUsers.iGroupID = Convert.ToInt32(lstGroup.SelectedValue.ToString());

                        //int iGroupID = Convert.ToInt32(lstGroup.SelectedValue.ToString());
                        //clsAccountUsers.strPhoneNumber = txtPhoneNumber.Text;

                        //if (bIsNewUser)
                        //    clsAccountUsers.bHasCompletedProfileMission = false;

                        ////string strHashPassword = clsCommonFunctions.GetMd5Sum("accelerate");
                        //string strHashPassword = clsCommonFunctions.GetMd5Sum(txtEmailAddress.Text.ToLower());
                        //clsAccountUsers.strPassword = strHashPassword;

                        ////### Images related items
                        //clsAccountUsers.strPathToImages = lblUniquePath.Text;
                        //clsAccountUsers.strMasterImage = "crop_" + GetMainImagePath(dlImages);

                        //string strFullPathToImage = clsAccountUsers.strPathToImages + clsAccountUsers.strMasterImage;
                        //clsAccountUsers.iCurrentMissionID = 0;
                        //clsAccountUsers.iCurrentPoints = 0;

                        //clsAccountUsers.Update();

                        //if (bIsNewUser)
                        //{
                        //    SendReport();
                        //}

                        //if (bIsNewUser)
                        //{
                        //    clsAccountUserTrackings clsAccountUserTrackings = new clsAccountUserTrackings();
                        //    clsAccountUserTrackings.dtAdded = DateTime.Now;
                        //    clsAccountUserTrackings.bHasEmailBeenSent = false;
                        //    clsAccountUserTrackings.iCurrentPhaseLevel = 1;
                        //    clsAccountUserTrackings.bIsPhase1Complete = false;
                        //    clsAccountUserTrackings.bIsPhase2Complete = false;
                        //    clsAccountUserTrackings.bIsPhase3Complete = false;

                        //    DataTable dtAllPhasesForThatGroup = clsGroupPhases.GetGroupPhasesList("iGroupID=" + clsAccountUsers.iGroupID, "");

                        //    int iCountID = 0;

                        //    foreach (DataRow dtrPhaseInGroup in dtAllPhasesForThatGroup.Rows)
                        //    {
                        //        ++iCountID;

                        //        int iCurrentPhaseID;

                        //        if (iCountID == 1)
                        //        {
                        //            iCurrentPhaseID = Convert.ToInt32(dtrPhaseInGroup["iPhaseID"]);
                        //            clsAccountUserTrackings.iPhase1ID = iCurrentPhaseID;
                        //            clsAccountUserTrackings.iCurrentPhaseID = iCurrentPhaseID;
                        //        }
                        //        else if (iCountID == 2)
                        //        {
                        //            iCurrentPhaseID = Convert.ToInt32(dtrPhaseInGroup["iPhaseID"]);
                        //            clsAccountUserTrackings.iPhase2ID = iCurrentPhaseID;
                        //        }
                        //        else if (iCountID == 3)
                        //        {
                        //            iCurrentPhaseID = Convert.ToInt32(dtrPhaseInGroup["iPhaseID"]);
                        //            clsAccountUserTrackings.iPhase3ID = iCurrentPhaseID;
                        //        }
                        //    }

                        //    DataTable dtPhase1Missions = clsPhaseMissions.GetPhaseMissionsList("iPhaseID=" + clsAccountUserTrackings.iPhase1ID, "");

                        //    foreach (DataRow dtrMissionsInPhase in dtPhase1Missions.Rows)
                        //    {
                        //        clsAccountUsers.iCurrentMissionID = Convert.ToInt32(dtrMissionsInPhase["iMissionID"]);
                        //        break;
                        //    }
                        //    //DataTable dtPhase2Missions = clsPhaseMissions.GetPhaseMissionsList("iPhaseID=" + clsAccountUserTrackings.iPhase2ID, "");
                        //    //DataTable dtPhase3Missions = clsPhaseMissions.GetPhaseMissionsList("iPhaseID=" + clsAccountUserTrackings.iPhase3ID, "");



                        //    //int iPhase1Total = dtPhase1Missions.Rows.Count;
                        //    //int iPhase2Total = dtPhase2Missions.Rows.Count;
                        //    //int iPhase3Total = dtPhase3Missions.Rows.Count;
                        //    //int iMissionTotal = iPhase1Total + iPhase2Total + iPhase3Total;

                        //    //clsAccountUserTrackings.iTotalMissionsAvailable = iMissionTotal;
                        //    //clsAccountUsers.Update();
                        //    clsAccountUserTrackings.Update();
                        //    //clsAccountUsers.iAccountUserTrackingID = clsAccountUserTrackings.iAccountUserTrackingID;
                        //    //clsAccountUsers.Update();
                        //    clsAccountUsers.iAccountUserTrackingID = clsAccountUserTrackings.iAccountUserTrackingID;
                        //    clsAccountUsers.iCurrentPhaseID = clsAccountUserTrackings.iCurrentPhaseID;
                        //    clsAccountUsers.Update();
                        //}

                        //#####################################
                        #endregion

                    //}
                }
            }
            catch { Exception ex; }
        }
        Session["iNumberOfInactiveUsers"] = iNumberOfInactiveUsers;
        connExcel.Close();
        Response.Redirect("frmInactiveAccountUsersView.aspx");
    }

    protected void SendReportX(string strEmailAddress, string strFirstName)
    {
        string strReportName = "Welcome";

        StringBuilder strbMailBuilder = new StringBuilder();

        strbMailBuilder.AppendLine("<!-- Start of textbanner -->");
        strbMailBuilder.AppendLine("<table width='100%' bgcolor='#fff' cellpadding='0' cellspacing='0' border='0' id='backgroundTable' movable=''>");
        strbMailBuilder.AppendLine("<tbody>");
        strbMailBuilder.AppendLine("<tr>");
        strbMailBuilder.AppendLine("<td>");
        strbMailBuilder.AppendLine("<table bgcolor='#ffffff' width='650' cellpadding='0' cellspacing='0' border='0' align='center' class='devicewidth' options=''>"); //style='border-left:1px solid #333; border-right:1px solid #333;'
        strbMailBuilder.AppendLine("<tbody>");
        strbMailBuilder.AppendLine("<!-- Spacing -->");
        strbMailBuilder.AppendLine("<tr>");
        strbMailBuilder.AppendLine("<td height='50'></td>");
        strbMailBuilder.AppendLine("</tr>");
        strbMailBuilder.AppendLine("<!-- End of Spacing -->");
        strbMailBuilder.AppendLine("<tr>");
        strbMailBuilder.AppendLine("<td>");
        strbMailBuilder.AppendLine("<table width='100%' cellspacing='0' cellpadding='0'>");
        strbMailBuilder.AppendLine("<tbody>");

        strbMailBuilder.AppendLine("<!-- Content -->");
        strbMailBuilder.AppendLine("<tr>");
        strbMailBuilder.AppendLine("<td width='60'></td>");
        strbMailBuilder.AppendLine("<td valign='top' style='font-family: Helvetica, Arial, sans-serif;font-size: 14px; color: #919191; text-align:left;line-height: 22px;' text=''>");
        strbMailBuilder.AppendLine("Hi " + strFirstName + ",<br />");
        strbMailBuilder.AppendLine("Here is your username and password:<br /><br/>");
        strbMailBuilder.AppendLine("Username: " + strEmailAddress + "<br/>");
        strbMailBuilder.AppendLine("Password: " + strEmailAddress + "<br/>");
        strbMailBuilder.AppendLine("<br/>");
        strbMailBuilder.AppendLine("<br/>");
        strbMailBuilder.AppendLine("</td>");
        strbMailBuilder.AppendLine("<td width='60'></td>");
        strbMailBuilder.AppendLine("</tr>");
        strbMailBuilder.AppendLine("<!-- End of Content -->");

        strbMailBuilder.AppendLine("</td>");
        strbMailBuilder.AppendLine("<td width='20'></td>");
        strbMailBuilder.AppendLine("</tr>");
        strbMailBuilder.AppendLine("</tbody>");
        strbMailBuilder.AppendLine("</table>");
        strbMailBuilder.AppendLine("</td>");
        strbMailBuilder.AppendLine("</tr>");
        strbMailBuilder.AppendLine("<!-- Spacing -->");
        strbMailBuilder.AppendLine("<tr>");
        strbMailBuilder.AppendLine("<td height='50'></td>");
        strbMailBuilder.AppendLine("</tr>");
        strbMailBuilder.AppendLine("<!-- End of Spacing -->");
        strbMailBuilder.AppendLine("</tbody>");
        strbMailBuilder.AppendLine("</table>");
        strbMailBuilder.AppendLine("</td>");
        strbMailBuilder.AppendLine("</tr>");
        strbMailBuilder.AppendLine("</tbody>");
        strbMailBuilder.AppendLine("</table>");
        strbMailBuilder.AppendLine("<!-- End of textbanner -->");

        Attachment[] empty = new Attachment[] { };

        try
        {
            emailComponent.SendMail("no-reply@raonboarding.co.za", strEmailAddress, "", "andrew@softservedigital.co.za", strReportName, strbMailBuilder.ToString(), "Heading", "Subheading", empty, true);
        }
        catch { }

        //### Redirect
        lblValidationMessage.Text = "<div class=\"validationImageCorrectLogin\"></div><div class=\"validationLabel\">Your email has been sent.</div>";
    }

    protected void SendReport(string strEmailAddress, string strFirstName)
    {
        string strReportName = "Welcome";

        StringBuilder strbMailBuilder = new StringBuilder();

        strbMailBuilder.AppendLine("<!-- Start of textbanner -->");
        strbMailBuilder.AppendLine("<table width='100%' bgcolor='#fff' cellpadding='0' cellspacing='0' border='0' id='backgroundTable' movable=''>");
        strbMailBuilder.AppendLine("<tbody>");
        strbMailBuilder.AppendLine("<tr>");
        strbMailBuilder.AppendLine("<td>");
        strbMailBuilder.AppendLine("<table bgcolor='#ffffff' width='650' cellpadding='0' cellspacing='0' border='0' align='center' class='devicewidth' options=''>"); //style='border-left:1px solid #333; border-right:1px solid #333;'
        strbMailBuilder.AppendLine("<tbody>");
        strbMailBuilder.AppendLine("<!-- Spacing -->");
        strbMailBuilder.AppendLine("<tr>");
        strbMailBuilder.AppendLine("<td height='50'></td>");
        strbMailBuilder.AppendLine("</tr>");
        strbMailBuilder.AppendLine("<!-- End of Spacing -->");
        strbMailBuilder.AppendLine("<tr>");
        strbMailBuilder.AppendLine("<td>");
        strbMailBuilder.AppendLine("<table width='100%' cellspacing='0' cellpadding='0'>");
        strbMailBuilder.AppendLine("<tbody>");

        strbMailBuilder.AppendLine("<!-- Content -->");
        strbMailBuilder.AppendLine("<tr>");
        strbMailBuilder.AppendLine("<td width='60'></td>");
        strbMailBuilder.AppendLine("<td valign='top' style='font-family:Arial;font-size: 15px; color: #3b3b3b; text-align:left;line-height: 22px;' text=''>");

        strbMailBuilder.AppendLine("Hi " + strFirstName + ",<br /><br />");
        strbMailBuilder.AppendLine("Congratulation on joining Deloitte Risk Advisory.As part of your onboarding experience, you will be introduced to Risk Advisory through an interactive, engaging <b>gamified</b> experience that is guaranteed tochallenge, engage and excite you.<br /><br/>");
        strbMailBuilder.AppendLine("Login to the Accelerate website using the following credentials to kick-off your first mission.<br /><br/>");

        strbMailBuilder.AppendLine("Username: " + strEmailAddress + "<br/>");
        strbMailBuilder.AppendLine("Password: " + strEmailAddress + "<br/>");
        strbMailBuilder.AppendLine("Link: <a href='" + System.Configuration.ConfigurationManager.AppSettings["WebRoot"] + "'>" + System.Configuration.ConfigurationManager.AppSettings["WebRoot"] + "</a><br/><br/>");

        strbMailBuilder.AppendLine("<h2>Why Gamification?</h2>");
        strbMailBuilder.AppendLine("We are committed to you and your success at Deloitte, which is why we have created a tailor made solution that you help you get a jump start on your career!Think happier, more engaged, integrated employees.<br/>");
        strbMailBuilder.AppendLine("<h2>What will you learn?</h2>");
        strbMailBuilder.AppendLine("By completing missions on the system, you will earn points and ultimately will get to know your Deloitte, your Risk Advisory and your leadership better.<br/>");
        strbMailBuilder.AppendLine("<i>*Completing all missions also earns you Challenger Series points, increasing your chances of winning iPads, gift vouchers and other great prizes.</i><br/><br/>");

        strbMailBuilder.AppendLine("Learn, network, earn rewards.<br/>");
        strbMailBuilder.AppendLine("Regards,<br/>");
        strbMailBuilder.AppendLine("The Onboarding team");
        strbMailBuilder.AppendLine("</td>");
        strbMailBuilder.AppendLine("<td width='60'></td>");
        strbMailBuilder.AppendLine("</tr>");
        strbMailBuilder.AppendLine("<!-- End of Content -->");

        strbMailBuilder.AppendLine("</td>");
        strbMailBuilder.AppendLine("<td width='20'></td>");
        strbMailBuilder.AppendLine("</tr>");
        strbMailBuilder.AppendLine("</tbody>");
        strbMailBuilder.AppendLine("</table>");
        strbMailBuilder.AppendLine("</td>");
        strbMailBuilder.AppendLine("</tr>");
        strbMailBuilder.AppendLine("<!-- Spacing -->");
        strbMailBuilder.AppendLine("<tr>");
        strbMailBuilder.AppendLine("<td height='50'></td>");
        strbMailBuilder.AppendLine("</tr>");
        strbMailBuilder.AppendLine("<!-- End of Spacing -->");
        strbMailBuilder.AppendLine("</tbody>");
        strbMailBuilder.AppendLine("</table>");
        strbMailBuilder.AppendLine("</td>");
        strbMailBuilder.AppendLine("</tr>");
        strbMailBuilder.AppendLine("</tbody>");
        strbMailBuilder.AppendLine("</table>");
        strbMailBuilder.AppendLine("<!-- End of textbanner -->");

        Attachment[] empty = new Attachment[] { };

        try
        {
            emailComponent.SendMail("no-reply@raonboarding.co.za", strEmailAddress, "", "andrew@softservedigital.co.za", strReportName, strbMailBuilder.ToString(), "Heading", "Subheading", empty, true);
        }
        catch { }

        //### Redirect
        lblValidationMessage.Text = "<div class=\"validationImageCorrectLogin\"></div><div class=\"validationLabel\">Your email has been sent.</div>";
    }
}
      