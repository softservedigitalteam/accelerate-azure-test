
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text;

/// <summary>
/// Summary description for clsTitlesView
/// </summary>
public partial class CMS_clsTitlesView : System.Web.UI.Page
{
    #region CLASSES AND VARIABLES

    clsUsers clsUsers;
    DataTable dtTitlesList;

    List<clsTitles> glstTitles;

    #endregion

    #region EVENT MODULES

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

dgrGrid.ItemCreated += new DataGridItemEventHandler(clsCommonFunctions.dgrGrid_PagerPopulate);

        if (!Page.IsPostBack)
        {
            Session["dtTitlesList"] = null;
            PopulateFormData();
        }
        else
        {
            if (Session["glstTitles"] == null)
            {
                glstTitles = new List<clsTitles>();
                Session["glstTitles"] = glstTitles;
            }
            else
                glstTitles = (List<clsTitles>)Session["glstTitles"];
        }
            PopulateFormData();

    }

    protected string EscapeLikeValue(string valueWithoutWildcards)
    {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < valueWithoutWildcards.Length; i++)
        {
            char c = valueWithoutWildcards[i];
            if (c == '*' || c == '%' || c == '[' || c == ']')
                sb.Append("[").Append(c).Append("]");
            else if (c == '\'')
                sb.Append("''");
            else
                sb.Append(c);
        }
        return sb.ToString();
    }

    protected void lnkbtnSearch_Click(object sender, EventArgs e)
    {
        string EscapedString = EscapeLikeValue(txtSearch.Text.ToString());
        string FilterExpression ="(strTitle) LIKE '%" + EscapedString + "%'";
        DataTable dtTitlesList = clsTitles.GetTitlesList(FilterExpression, "");

        Session["dtTitlesList"] = dtTitlesList;

        PopulateFormData();
    }

    protected void lnkbtnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmTitlesAddModify.aspx");
    }

    #endregion

    #region DATA GRID METHODS

    private void PopulateFormData()
    {
        //### Populate datagrid using clsTitlesList object
        try
        {
            dtTitlesList = new DataTable();

            if (Session["dtTitlesList"] == null)
                dtTitlesList = clsTitles.GetTitlesList();
            else
                dtTitlesList = (DataTable)Session["dtTitlesList"];

            dgrGrid.DataSource = dtTitlesList;

            //### Add this check to trap if the last item on the page is deleted
            if (Convert.ToInt16(dgrGrid.CurrentPageIndex) < Convert.ToInt16(Session["TitlesView_CurrentPageIndex"]))
            {
                dgrGrid.CurrentPageIndex = 0;
            }
            else
            {
                if (Session["TitlesView_CurrentPageIndex"] != null)
                {
                    dgrGrid.CurrentPageIndex = (int)Session["TitlesView_CurrentPageIndex"];
                }
            }

            dgrGrid.DataBind();

        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void lnkDeleteItem_Click(object sender, EventArgs e)
    {
        int iTitleID = int.Parse((sender as LinkButton).CommandArgument);

        clsTitles.Delete(iTitleID);

        DataTable dtTitlesList = new DataTable();

        if (((txtSearch.Text == "SEARCH FOR A TITLE") == false) && ((txtSearch.Text == "") == false))
        {
            string FilterExpression = "(strTitle) LIKE '%" + txtSearch.Text + "%'";
            dtTitlesList = clsTitles.GetTitlesList(FilterExpression, "");
        }
        else
        {
            dtTitlesList = clsTitles.GetTitlesList();
        }

        Session["dtTitlesList"] = dtTitlesList;

        PopulateFormData();
    }

    protected void dgrGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //### Method is used when the page index is changed.
        try
        {
            Session["TitlesView_CurrentPageIndex"] = e.NewPageIndex;
            dgrGrid.CurrentPageIndex = e.NewPageIndex;
            PopulateFormData();
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgrGrid_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        //### Sort Method
        dtTitlesList = new DataTable();

        if (Session["dtTitlesList"] == null)
            dtTitlesList = clsTitles.GetTitlesList();
        else
            dtTitlesList = (DataTable)Session["dtTitlesList"];

        DataView dvTemp = dtTitlesList.DefaultView;

        dvTemp.Sort = e.SortExpression;
        dtTitlesList = dvTemp.ToTable();
        Session["dtTitlesList"] = dtTitlesList;

        PopulateFormData();
    }

    public void dgrGrid_PopEditLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkEditItem = (HyperLink)sender;
            DataGridItem dgrItemEdit = (DataGridItem)lnkEditItem.Parent.Parent;

            //### Get the iTitleID
            int iTitleID = 0;
            iTitleID = int.Parse(dgrItemEdit.Cells[0].Text);

            //### Add attributes to edit link
            lnkEditItem.Attributes.Add("href", "frmTitlesAddModify.aspx?action=edit&iTitleID=" + iTitleID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

     public void dgrGrid_PopDeleteLink(object sender, EventArgs e)
     {
          try
          {
             //### Add hyperlink to datagrid item
             HyperLink lnkDeleteItem = (HyperLink)sender;
             DataGridItem dgrItemDelete = (DataGridItem)lnkDeleteItem.Parent.Parent;

             //### Get the iTitleID
             int iTitleID = 0;
             iTitleID = int.Parse(dgrItemDelete.Cells[0].Text);

             //### Add attributes to delete link
             lnkDeleteItem.CssClass = "dgrDeleteLink";
             lnkDeleteItem.Attributes.Add("href", "frmTitlesView.aspx?action=delete&iTitleID=" + iTitleID);
           }
           catch (Exception ex)
           {
                 Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
           }
     }

    #endregion

    #region Titles FILTERING

    /// <summary>
    /// Enables the AutoCompleteExtender
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    /// 
    [WebMethod]
    [ScriptMethod]
    public static string[] FilterBusinessRule(string prefixText, int count)
    {
        string[] strReturnList = new string[] { };

        string FilterExpression = "(strTitle) LIKE '%" + prefixText + "%'";
        DataTable dtTitles = clsTitles.GetTitlesList(FilterExpression, "");
        List<string> glstTitles = new List<string>();

        if (dtTitles.Rows.Count > 0)
        {
            foreach (DataRow dtrTitles in dtTitles.Rows)
            {
                glstTitles.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem("" + dtrTitles["strTitle"].ToString(), dtrTitles["iTitleID"].ToString()));
            }
        }
        else
            glstTitles.Add("No Titles Available.");
        strReturnList = glstTitles.ToArray();
        return strReturnList;
    }

    #endregion
}
