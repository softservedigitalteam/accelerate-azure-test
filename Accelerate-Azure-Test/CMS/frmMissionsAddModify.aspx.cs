using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class CMS_frmMissionsAddModify : System.Web.UI.Page
{
    clsUsers clsUsers;
    clsMissions clsMissions;
    clsMissionTypes clsMissionTypes;

    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

             //### Determines if a javascript delete has been called
             if (!String.IsNullOrEmpty(Page.Request["__EVENTARGUMENT"]) && (Page.Request["__EVENTARGUMENT"].Split(':')[0] == "iRemoveImages"))
             DeleteImages(Convert.ToInt32(Page.Request["__EVENTARGUMENT"].Split(':')[1]));

        if (!IsPostBack)
        {
             //popMissionStatus();
             popAchievement();
             popMissionType();
             popMissionVideos();
            popAdminEmailSettings();

            //### If the iMissionID is passed through then we want to instantiate the object with that iMissionID
            if ((Request.QueryString["iMissionID"] != "") && (Request.QueryString["iMissionID"] != null))
            {
                clsMissions = new clsMissions(Convert.ToInt32(Request.QueryString["iMissionID"]));

                //### Populate the form
                popFormData();
                
            }
            else
            {
                clsMissions = new clsMissions();
            }
            Session["clsMissions"] = clsMissions;
        }
        else
        {
            if (IsPostBack && FileUpload.PostedFile != null)
            {
                if (FileUpload.PostedFile.FileName.Length > 0)
                {
                    if (Session["lstImages"] == null)
                    {
                        lstImages = new List<string>();
                        Session["lstImages"] = lstImages;
                    }
                    //### Check that they have ONLY HAVE MAX NUMBER OF Images in the datalist
                    if (dlImages.Items.Count == iMaxImages)
                    {
                        mandatoryDiv.Visible = true;
                        lblValidationMessage.Text = "You can only have " + iMaxImages.ToString() + " Images.";
                        getList(lblUniquePath.Text);
                    }
                    else
                    {
                        mandatoryDiv.Visible = false;

                        string strUniquePath;
                        if (lblUniquePath.Text == "")
                        {
                            strUniquePath = GetUniquePath();
                            lblUniquePath.Text = strUniquePath;
                        }
                        else
                        {
                            strUniquePath = lblUniquePath.Text;
                        }
                        UploadImages(strUniquePath);
                        getList(strUniquePath);
                    }
                }
            }

            clsMissions = (clsMissions)Session["clsMissions"];
        }
    }

    protected void lnkbtnBack_Click(object sender, EventArgs e)
    {
        //### Go back to previous page
        Response.Redirect("frmMissionsView.aspx");
    }

    protected void lnkbtnSave_Click(object sender, EventArgs e)
    {
        //### Validate registration process
        bool bCanSave = true;

       //bCanSave = clsValidation.IsNullOrEmpty(lstMissionStatus, bCanSave);
       bCanSave = clsValidation.IsNullOrEmpty(lstMissionType, bCanSave);
       bCanSave = clsValidation.IsNullOrEmpty(lstAchievement, bCanSave);
       bCanSave = clsValidation.IsNullOrEmpty(txtBonusPoints, bCanSave);
       bCanSave = clsValidation.IsNullOrEmpty(txtTagLine, bCanSave);
       bCanSave = clsValidation.IsNullOrEmpty(txtTitle, bCanSave);
       //bCanSave = clsValidation.IsNullOrEmpty(txtPathToImages, bCanSave);
       //bCanSave = clsValidation.IsNullOrEmpty(txtMasterImage, bCanSave);
       bCanSave = clsValidation.IsNullOrEmpty(txtDescription, bCanSave);


       if (lblUniquePath.Text == "")
       {
           bCanSave = false;
           mandatoryDiv.Visible = true;
           lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">Please upload image and fill out all mandatory field - TeamShip not addeds</div></div>";

       }
       else
       {
           if (bCanSave == true)
           {
               mandatoryDiv.Visible = false;
               lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceHappy.png\" alt='' title=''/><div class=\"validationMessage\">Mission added successfully</div></div>";
               SaveData();
           }
           else
           {
               mandatoryDiv.Visible = true;
               lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><img src=\"images/Validation/imgFaceSad.png\" alt='' title=''/><div class=\"validationMessage\">Please fill out all mandatory fields - Mission not added</div></div>";
           }
       }
    }

    protected void lnkbtnClear_Click(object sender, EventArgs e)
    {
        mandatoryDiv.Visible = false;

        //lstMissionStatus.SelectedValue = "0";
        //clsValidation.SetValid(lstMissionStatus);
        lstAdminEmailSettings.SelectedValue = "0";
        clsValidation.SetValid(lstAdminEmailSettings);
        txtBonusPoints.Text = "";
        clsValidation.SetValid(txtBonusPoints);
        txtTitle.Text = "";
        clsValidation.SetValid(txtTitle);
        txtTagLine.Text = "";
        clsValidation.SetValid(txtTagLine);
        lstAchievement.SelectedValue = "0";
        clsValidation.SetValid(lstAchievement);
        txtDescription.Text = "";
        clsValidation.SetValid(txtDescription);
    }

    #endregion 

    #region POPULATE DATA METHODS

    private void popFormData()
    {
         //lstMissionStatus.SelectedValue = clsMissions.iMissionStatusID.ToString();
         txtBonusPoints.Text = clsMissions.iBonusPoints.ToString();
         txtTitle.Text = clsMissions.strTitle;
         txtTagLine.Text = clsMissions.strTagLine;
         txtTag.Text = clsMissions.strTag;
         txtLink.Text = clsMissions.strLink;
         txtLinkTitle.Text = clsMissions.strLinkTitle;
         
         

         //### Populates images
         if (!string.IsNullOrEmpty(clsMissions.strPathToImages))
         {
             lblUniquePath.Text = clsMissions.strPathToImages;
             getList(clsMissions.strPathToImages);
             //### Set Current Master Image
             List<string> lstImagesFileNames = (List<string>)Session["lstImagesFileNames"];
             try
             {
                 foreach (string strImageFileName in lstImagesFileNames)
                 {
                     if (strImageFileName == clsMissions.strMasterImage)
                     {
                         RadioButton rdbMasterImage = dlImages.Items[lstImagesFileNames.IndexOf(strImageFileName)].FindControl("rdbMainImage") as RadioButton;
                         rdbMasterImage.Checked = true;
                         break;
                     }
                 }
             }
             catch { }
         }

        if(clsMissions.iMissionVideoID != 0)
        {
            chkIsVideoMission.Checked = true;
            clsMissionVideos clsMissionVideo = new clsMissionVideos(clsMissions.iMissionVideoID);
            lblVideo.Visible = true;
            ddlVideoMission.Visible = true;
            ddlVideoMission.SelectedValue = clsMissionVideo.iMissionVideoID.ToString();
        }

         lstAchievement.SelectedValue = clsMissions.iAchievementID.ToString();
         txtDescription.Text = clsMissions.strDescription;
         lstMissionType.SelectedValue = clsMissions.iMissionTypeID.ToString();
        lstAdminEmailSettings.SelectedValue = clsMissions.iAdminEmailSettingsID.ToString();
        int selected = Convert.ToInt32(lstAdminEmailSettings.SelectedValue.ToString());
        if (selected != 0)
        {
            clsAdminEmailSettings clsAdminEmailSetting = new clsAdminEmailSettings(selected);
            txtEmail1.Text = clsAdminEmailSetting.strEmail1.ToString();
            txtEmail2.Text = clsAdminEmailSetting.strEmail2.ToString();
        }
        else
        {
            txtEmail1.Text = "";
            txtEmail2.Text = "";
        }
        //ddlVideoMission.SelectedValue = clsMissions.iMissionVideoID.ToString();
    }

    private void popAdminEmailSettings()
    {
        DataTable dtAdminEmailSettingssList = new DataTable();
        lstAdminEmailSettings.DataSource = clsAdminEmailSettings.GetAdminEmailSettingsList();

        //### Populates the drop down list with PK and TITLE;
        lstAdminEmailSettings.DataValueField = "iAdminEmailSettingsID";
        lstAdminEmailSettings.DataTextField = "strAdminTitle";

        //### Bind the data to the list;
        lstAdminEmailSettings.DataBind();

        //### Add default select option;
        lstAdminEmailSettings.Items.Insert(0, new ListItem("--Not Selected--", "0"));
    }

    //private void popMissionStatus()
    //{
    //     DataTable dtMissionStatussList = new DataTable();
    //     lstMissionStatus.DataSource = clsMissionStatuss.GetMissionStatussList();

    //     //### Populates the drop down list with PK and TITLE;
    //     lstMissionStatus.DataValueField = "iMissionStatusID";
    //     lstMissionStatus.DataTextField = "strTitle";

    //     //### Bind the data to the list;
    //     lstMissionStatus.DataBind();

    //     //### Add default select option;
    //     lstMissionStatus.Items.Insert(0, new ListItem("--Not Selected--", "0"));
    //}

    private void popMissionType()
    {
        DataTable dtMissionTypesList = new DataTable();
        lstMissionType.DataSource = clsMissionTypes.GetMissionTypesList();

        //### Populates the drop down list with PK and TITLE;
        lstMissionType.DataValueField = "iMissionTypeID";
        lstMissionType.DataTextField = "strTitle";
        
        //### Bind the data to the list;
        lstMissionType.DataBind();

        //### Add default select option;
        lstMissionType.Items.Insert(0, new ListItem("--Not Selected--", "0"));
    }
    private void popAchievement()
    {
         DataTable dtAchievementsList = new DataTable();
         lstAchievement.DataSource = clsAchievements.GetAchievementsList();

         //### Populates the drop down list with PK and TITLE;
         lstAchievement.DataValueField = "iAchievementID";
         lstAchievement.DataTextField = "strTitle";

         //### Bind the data to the list;
         lstAchievement.DataBind();

         //### Add default select option;
         lstAchievement.Items.Insert(0, new ListItem("--Not Selected--", "0"));
    }

    private void popMissionVideos()
    {
        DataTable dtMissionVideoList = new DataTable();
        ddlVideoMission.DataSource = clsMissionVideos.GetMissionVideosList();

        //### Populates the drop down list with PK and TITLE;
        ddlVideoMission.DataValueField = "iMissionVideoID";
        ddlVideoMission.DataTextField = "strVideoLink";

        //### Bind the data to the list;
        ddlVideoMission.DataBind();

        //### Add default select option;
        ddlVideoMission.Items.Insert(0, new ListItem("--Not Selected--", "0"));
    }
    
    #endregion

    #region SAVE DATA METHODS

    private void SaveData()
    {

        //### Add / Update
        clsMissions.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsMissions.iAddedBy = clsUsers.iUserID;
        clsMissions.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsMissions.iEditedBy = clsUsers.iUserID;
        clsMissions.strTag = txtTag.Text;
        //clsMissions.iMissionStatusID = Convert.ToInt32(lstMissionStatus.SelectedValue.ToString());
        clsMissions.iMissionTypeID = Convert.ToInt32(lstMissionType.SelectedValue.ToString());
        clsMissions.iBonusPoints = Convert.ToInt32(txtBonusPoints.Text);
        clsMissions.strLinkTitle = txtLinkTitle.Text;
        clsMissions.strLink = txtLink.Text;
        clsMissions.strTitle = txtTitle.Text;
        clsMissions.strTagLine = txtTagLine.Text;
        clsMissions.iMissionVideoID = Convert.ToInt32(ddlVideoMission.SelectedValue.ToString());

        //### Images related items
        clsMissions.strPathToImages = lblUniquePath.Text;
        clsMissions.strMasterImage = GetMainImagePath(dlImages);
        clsMissions.iAchievementID = Convert.ToInt32(lstAchievement.SelectedValue.ToString());
        clsMissions.iAdminEmailSettingsID = Convert.ToInt32(lstAdminEmailSettings.SelectedValue.ToString());
        clsMissions.strDescription = txtDescription.Text;
        //clsMissions.strSendEmailTo = txtSendEmailTo.Text;
        clsMissions.Update();

        Session["dtMissionsList"] = null;

        //### Go back to view page
        Response.Redirect("frmMissionsView.aspx");
    }

    #endregion

    #region IMAGE METHODS

    List<string> lstImages;
    List<string> lstImagesFileNames;
    int iMaxImages = 1;
    string strUniqueFullPath = AppDomain.CurrentDomain.BaseDirectory + "\\Missions";
    protected void btnUpload_Click(object sender, EventArgs e)
    {
        if (Session["lstImages"] == null)
        {
            lstImages = new List<string>();
            Session["lstImages"] = lstImages;
        }
        //### Check that they have ONLY HAVE MAX NUMBER OF Images in the datalist
        if (dlImages.Items.Count == iMaxImages)
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "You can only have " + iMaxImages.ToString() + " Images.";
            getList(lblUniquePath.Text);
        }
        else
        {
            mandatoryDiv.Visible = false;

            string strUniquePath;
            if (lblUniquePath.Text == "")
            {
                strUniquePath = GetUniquePath();
                lblUniquePath.Text = strUniquePath;
            }
            else
            {
                strUniquePath = lblUniquePath.Text;
            }
            UploadImages(strUniquePath);
            getList(strUniquePath);
        }
    }
    private string GetUniquePath()
    {
        int iCount = 1;
        //### First we need to get the path
        while (System.IO.Directory.Exists(strUniqueFullPath + "\\Missions" + iCount) == true)
        {
            iCount++;
        }
        return "Missions" + iCount;
    }

    protected void UploadImages(String strUniquePath)
    {
        if (FileUpload.PostedFile.ContentLength > 0 && FileUpload.PostedFile.ContentLength < 1073741824)
        {

            //### Upload files to unique folder
            string strUploadFileName = "";
            strUploadFileName = System.IO.Path.GetFileName(FileUpload.PostedFile.FileName);
            string strSaveLocation = "";
            strSaveLocation = strUniqueFullPath + "\\" + strUniquePath + "\\" + strUploadFileName;

            if (!System.IO.Directory.Exists(strUniqueFullPath + "\\" + strUniquePath))
            {
                System.IO.Directory.CreateDirectory(strUniqueFullPath + "\\" + strUniquePath);
            }
            FileUpload.PostedFile.SaveAs(strSaveLocation);

            CopyAndResizePic(strSaveLocation);
            getList(strUniqueFullPath + "\\" + strUniquePath);
        }
        else
        {
            lblValidationMessage.Text = "The file should be between 0 and 1Mb.";
            lblUploadError.Text = "The file should be between 0 and 1Mb.";
        }
    }

    private void CopyAndResizePic(String strFullPath)
    {
        try
        {

            String strFileName;
            String strNewFilePath;

            //### Main Images
            String strLrgFileName;

            strFileName = Path.GetFileName(strFullPath);
            strNewFilePath = strFullPath.Replace(strFileName, "");
            strNewFilePath = strNewFilePath + Path.GetFileNameWithoutExtension(strFullPath);
            strNewFilePath = strNewFilePath + "_lrg";
            strNewFilePath = strNewFilePath + Path.GetExtension(strFullPath);

            File.Copy(strFullPath, strNewFilePath);
            strLrgFileName = Path.GetFileName(strNewFilePath);

            clsCommonFunctions.ResizeImage(strNewFilePath, strNewFilePath, 600, 600, false);

            //### Thumbnail
            String strSmlFileName;

            strFileName = Path.GetFileName(strFullPath);
            strNewFilePath = strFullPath.Replace(strFileName, "");
            strNewFilePath = strNewFilePath + Path.GetFileNameWithoutExtension(strFullPath);
            strNewFilePath = strNewFilePath + "_sml";
            strNewFilePath = strNewFilePath + Path.GetExtension(strFullPath);

            File.Copy(strFullPath, strNewFilePath);
            strSmlFileName = Path.GetFileName(strNewFilePath);

            clsCommonFunctions.ResizeImage(strNewFilePath, strNewFilePath, 140, 140, false);
            
        }
        catch (Exception ex){}
    }

    public void getList(String strPathToFolder)
    {
        lstImages = new List<string>();
        lstImagesFileNames = new List<string>();
        try
        {
            string strPath = strPathToFolder;
            string[] files = Directory.GetFiles(strUniqueFullPath + "\\" + strPath);

            string iMissionID = "";
            if (!string.IsNullOrEmpty(Request.QueryString["iMissionID"]))
                iMissionID = Request.QueryString["iMissionID"];

            int iImagesCount = 0;

            foreach (string strName in files)
            {
                if (strName.IndexOf("_sml") != -1)
                {
                    string strHTMLImages = strName.Replace(AppDomain.CurrentDomain.BaseDirectory + "\\", "..\\");
                    strHTMLImages = strHTMLImages.Replace("\\", "/");

                    //### Generates a javascript postback for the delete method
                    String strPostBack = Page.ClientScript.GetPostBackEventReference(this, "iRemoveImages:" + iImagesCount);

                    lstImages.Add(@"<tr style='text-align: centre;'>
                                        <td style='text-align: centre;'>
                                            <a class='ImagesColorBox' href='" + strHTMLImages.Replace("_sml", "_lrg") + "'><img src='" + strHTMLImages + @"' alt='' title='' style='border: solid 5px #ffffff;' /></a><br /><br />" +
                                            "<center><div class='buttonStyle' style='padding-top: 5px;'><a href=\"javascript:" + strPostBack + "\" style='color: #ffffff;'><div class='deleteButton'></div></a></div></center>" + @"
                                        </td>
                                    </tr>");
                    lstImagesFileNames.Add(Path.GetFileName(strName).Replace("_sml", ""));
                    iImagesCount++;
                }
            }
            dlImages.DataSource = lstImages;
            dlImages.DataBind();

            Session["lstImages"] = lstImages;
            Session["lstImagesFileNames"] = lstImagesFileNames;
        }
        catch (Exception ex) {}
    }

    private string GetMainImagePath(DataList dtlTarget)
    {
        string strReturn = "";

        foreach (DataListItem dliTarget in dtlTarget.Items)
        {
            RadioButton rdbMainImage = (RadioButton)dliTarget.FindControl("rdbMainImage");
            if (rdbMainImage.Checked)
            {
                lstImagesFileNames = (List<string>)Session["lstImagesFileNames"];
                strReturn = lstImagesFileNames[dliTarget.ItemIndex];
                break;
            }
        }
        return strReturn;
    }

    private void DeleteImages(int iImagesIndex)
    {

        //### Deletes all Images related to the target Images.
        lstImages = (List<string>)Session["lstImages"];
        lstImagesFileNames = (List<string>)Session["lstImagesFileNames"];

        lstImages.RemoveAt(iImagesIndex);
        Session["lstImages"] = lstImages;

        string[] files = Directory.GetFiles(strUniqueFullPath + "\\" + lblUniquePath.Text);
        foreach (string file in files)
        {
            if (Path.GetFileName(file) == lstImagesFileNames[iImagesIndex].ToString())
            {
                //### Remove all Images
                File.Delete(file);
                File.Delete(file.Replace(Path.GetExtension(file), "_sml" + Path.GetExtension(file)));
                File.Delete(file.Replace(Path.GetExtension(file), "_med" + Path.GetExtension(file)));
                File.Delete(file.Replace(Path.GetExtension(file), "_lrg" + Path.GetExtension(file)));
                break;
            }
        }
        lstImagesFileNames.RemoveAt(iImagesIndex);
        ViewState["lstImagesFileNames"] = lstImagesFileNames;
        getList(lblUniquePath.Text);
    }

    #endregion

    protected void ddlVideoMission_SelectedIndexChanged(object sender, EventArgs e)
    {
        
    }
    protected void chkIsVideoMission_CheckedChanged(object sender, EventArgs e)
    {
        if (chkIsVideoMission.Checked)
        {
            lblVideo.Visible = true;
            ddlVideoMission.Visible = true;
        }
        else
        {
            lblVideo.Visible = false;
            ddlVideoMission.Visible = false;
            ddlVideoMission.SelectedIndex = 0;
        }
    }

    protected void lstAdminEmailSettings_SelectedIndexChanged(object sender, EventArgs e)
    {
        int selected = Convert.ToInt32(lstAdminEmailSettings.SelectedValue.ToString());
        if (selected != 0)
        {
            clsAdminEmailSettings clsAdminEmailSetting = new clsAdminEmailSettings(selected);
            txtEmail1.Text = clsAdminEmailSetting.strEmail1.ToString();
            txtEmail2.Text = clsAdminEmailSetting.strEmail2.ToString();
        }
        else
        {
            txtEmail1.Text = "";
            txtEmail2.Text = "";
        }
        
    }
}