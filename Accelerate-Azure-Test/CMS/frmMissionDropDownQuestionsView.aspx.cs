
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text;

/// <summary>
/// Summary description for clsMissionQuestionsView
/// </summary>
public partial class CMS_clsMissionDropDownQuestionsView : System.Web.UI.Page
{
    #region CLASSES AND VARIABLES

    clsUsers clsUsers;
    DataTable dtMissionDropDownQuestionsList;

    List<clsMissionDropDownQuestions> glstMissionDropDownQuestions;

    #endregion

    #region EVENT MODULES

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsUsers"] == null)
        {
            {
                //### Redirect back to login
                Response.Redirect("../CMSLogin.aspx");
            }
        }
        clsUsers = (clsUsers)Session["clsUsers"];

dgrGrid.ItemCreated += new DataGridItemEventHandler(clsCommonFunctions.dgrGrid_PagerPopulate);

        if (!Page.IsPostBack)
        {
            Session["dtMissionDropDownQuestionsList"] = null;
            PopulateFormData();
        }
        else
        {
            if (Session["glstMissionDropDownQuestions"] == null)
            {
                glstMissionDropDownQuestions = new List<clsMissionDropDownQuestions>();
                Session["glstMissionDropDownQuestions"] = glstMissionDropDownQuestions;
            }
            else
                glstMissionDropDownQuestions = (List<clsMissionDropDownQuestions>)Session["glstMissionDropDownQuestions"];
        }
            PopulateFormData();

    }

    protected string EscapeLikeValue(string valueWithoutWildcards)
    {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < valueWithoutWildcards.Length; i++)
        {
            char c = valueWithoutWildcards[i];
            if (c == '*' || c == '%' || c == '[' || c == ']')
                sb.Append("[").Append(c).Append("]");
            else if (c == '\'')
                sb.Append("''");
            else
                sb.Append(c);
        }
        return sb.ToString();
    }

    protected void lnkbtnSearch_Click(object sender, EventArgs e)
    {
        string EscapedString = EscapeLikeValue(txtSearch.Text.ToString());
        string FilterExpression ="(strQuestion) LIKE '%" + EscapedString + "%'";
        DataTable dtMissionDropDownQuestionsList = clsMissionDropDownQuestions.GetMissionQuestionsList(FilterExpression, "");

        Session["dtMissionDropDownQuestionsList"] = dtMissionDropDownQuestionsList;

        PopulateFormData();
    }

    protected void lnkbtnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmMissionDropDownQuestionsAddModify.aspx");
    }

    #endregion

    #region DATA GRID METHODS

    private void PopulateFormData()
    {
        //### Populate datagrid using clsMissionQuestionsList object
        try
        {
            dtMissionDropDownQuestionsList = new DataTable();

            if (Session["dtMissionDropDownQuestionsList"] == null)
                dtMissionDropDownQuestionsList = clsMissionDropDownQuestions.GetMissionQuestionsList("", "dtAdded DESC");
            else
                dtMissionDropDownQuestionsList = (DataTable)Session["dtMissionDropDownQuestionsList"];

            dgrGrid.DataSource = dtMissionDropDownQuestionsList;

            //### Add this check to trap if the last item on the page is deleted
            if (Convert.ToInt16(dgrGrid.CurrentPageIndex) < Convert.ToInt16(Session["MissionQuestionsView_CurrentPageIndex"]))
            {
                dgrGrid.CurrentPageIndex = 0;
            }
            else
            {
                if (Session["MissionQuestionsView_CurrentPageIndex"] != null)
                {
                    dgrGrid.CurrentPageIndex = (int)Session["MissionQuestionsView_CurrentPageIndex"];
                }
            }

            dgrGrid.DataBind();

        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void lnkDeleteItem_Click(object sender, EventArgs e)
    {
        int iMissionDropDownQuestionID = int.Parse((sender as LinkButton).CommandArgument);

        clsMissionDropDownQuestions.Delete(iMissionDropDownQuestionID);

        DataTable dtMissionDropDownQuestionsList = new DataTable();

        if (((txtSearch.Text == "SEARCH FOR A DROP DOWN MISSION ANSWER") == false) && ((txtSearch.Text == "") == false))
        {
            string FilterExpression = "(strQuestion) LIKE '%" + txtSearch.Text + "%'";
            dtMissionDropDownQuestionsList = clsMissionDropDownQuestions.GetMissionQuestionsList(FilterExpression, "");
        }
        else
        {
            dtMissionDropDownQuestionsList = clsMissionDropDownQuestions.GetMissionQuestionsList();
        }

        Session["dtMissionDropDownQuestionsList"] = dtMissionDropDownQuestionsList;

        PopulateFormData();
    }

    protected void dgrGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        //### Method is used when the page index is changed.
        try
        {
            Session["MissionQuestionsView_CurrentPageIndex"] = e.NewPageIndex;
            dgrGrid.CurrentPageIndex = e.NewPageIndex;
            PopulateFormData();
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

    protected void dgrGrid_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        //### Sort Method
        dtMissionDropDownQuestionsList = new DataTable();

        if (Session["dtMissionDropDownQuestionsList"] == null)
            dtMissionDropDownQuestionsList = clsMissionDropDownQuestions.GetMissionQuestionsList();
        else
            dtMissionDropDownQuestionsList = (DataTable)Session["dtMissionDropDownQuestionsList"];

        DataView dvTemp = dtMissionDropDownQuestionsList.DefaultView;

        dvTemp.Sort = e.SortExpression;
        dtMissionDropDownQuestionsList = dvTemp.ToTable();
        Session["dtMissionDropDownQuestionsList"] = dtMissionDropDownQuestionsList;

        PopulateFormData();
    }

    public void dgrGrid_PopEditLink(object sender, EventArgs e)
    {
        try
        {
            //### Add hyperlink to datagrid item
            HyperLink lnkEditItem = (HyperLink)sender;
            DataGridItem dgrItemEdit = (DataGridItem)lnkEditItem.Parent.Parent;

            //### Get the iMissionDropDownQuestionID
            int iMissionDropDownQuestionID = 0;
            iMissionDropDownQuestionID = int.Parse(dgrItemEdit.Cells[0].Text);

            //### Add attributes to edit link
            lnkEditItem.Attributes.Add("href", "frmMissionDropDownQuestionsAddModify.aspx?action=edit&iMissionDropDownQuestionID=" + iMissionDropDownQuestionID);
        }
        catch (Exception ex)
        {
            Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
        }
    }

     public void dgrGrid_PopDeleteLink(object sender, EventArgs e)
     {
          try
          {
             //### Add hyperlink to datagrid item
             HyperLink lnkDeleteItem = (HyperLink)sender;
             DataGridItem dgrItemDelete = (DataGridItem)lnkDeleteItem.Parent.Parent;

             //### Get the iMissionDropDownQuestionID
             int iMissionDropDownQuestionID = 0;
             iMissionDropDownQuestionID = int.Parse(dgrItemDelete.Cells[0].Text);

             //### Add attributes to delete link
             lnkDeleteItem.CssClass = "dgrDeleteLink";
             lnkDeleteItem.Attributes.Add("href", "frmMissionDropDownQuestionsView.aspx?action=delete&iMissionDropDownQuestionID=" + iMissionDropDownQuestionID);
           }
           catch (Exception ex)
           {
                 Response.Write(clsCommonFunctions.JSAlert(ex.Message.ToString()));
           }
     }

    #endregion

    #region MissionQuestions FILTERING

    /// <summary>
    /// Enables the AutoCompleteExtender
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    /// 
    [WebMethod]
    [ScriptMethod]
    public static string[] FilterBusinessRule(string prefixText, int count)
    {
        string[] strReturnList = new string[] { };

        string FilterExpression = "(strQuestion) LIKE '%" + prefixText + "%'";
        DataTable dtMissionDropDownQuestions = clsMissionDropDownQuestions.GetMissionQuestionsList(FilterExpression, "");
        List<string> glstMissionDropDownQuestions = new List<string>();

        if (dtMissionDropDownQuestions.Rows.Count > 0)
        {
            foreach (DataRow dtrMissionDropDownQuestions in dtMissionDropDownQuestions.Rows)
            {
                glstMissionDropDownQuestions.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem("" + dtrMissionDropDownQuestions["strQuestion"].ToString(), dtrMissionDropDownQuestions["iMissionDropDownQuestionID"].ToString()));
            }
        }
        else
            glstMissionDropDownQuestions.Add("No MissionQuestions Available.");
        strReturnList = glstMissionDropDownQuestions.ToArray();
        return strReturnList;
    }

    #endregion
}
