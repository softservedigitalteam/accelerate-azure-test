﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Accelerate.master" AutoEventWireup="true" Inherits="Accelerate_Achievements" Codebehind="Accelerate-Achievements.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Literal ID="litAchievementStages" runat="server"></asp:Literal>
    <div class="row-fluid">
        <div class="span2 PointsSection">
            <div style="width: 100%; font-size: 50px; font-weight: 300; text-align: center !important;">
                <asp:Literal runat="server" ID="litPoints"></asp:Literal>
            </div>
            <br />
            <br />
            <div style="width: 100%; font-size: 20px; font-weight: 500; font-weight: bold; text-align: left; margin-left: 15px;">POINTS</div>
        </div>
        <div class="span10">
            <div class="row-fluid">
                <div class="showcaseSection Black">My showcase</div>
            </div>
            <div class="well">
                <div class="row-fluid">
                    <asp:Repeater ID="rpAchievements" runat="server">
                        <ItemTemplate>

                            <div class="achievement" style="text-align: center; margin: 0 1.2%;">
                                <a href='<%#Eval ("strLink") %>'>
                                    <img onerror="this.src='images/imgAchievementEmpty.png'" src='<%#Eval ("FullPathForImage") %>' title='<%#Eval ("strTitle") %>' alt='<%#Eval ("strTitle") %>' width='70%' /></a>
                                <br />
                                <%#Eval ("strTitle") %>
                            </div>

                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

