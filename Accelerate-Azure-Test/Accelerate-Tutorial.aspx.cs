﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Accelerate_Tutorial : System.Web.UI.Page
{
    clsAccountUsers clsAccountUsers;

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsAccountUsers"] == null)
        {
            //### Redirect back to login
            Response.Redirect("Accelerate-Login.aspx");
        }
        clsAccountUsers = (clsAccountUsers)Session["clsAccountUsers"];
    }

    protected void btnProceed_Click(object sender, EventArgs e)
    {
        Response.Redirect("Accelerate-Profile-Mission.aspx");
    }
}