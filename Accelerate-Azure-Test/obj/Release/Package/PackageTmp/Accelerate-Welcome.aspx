﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Accelerate_Welcome_To" Codebehind="Accelerate-Welcome.aspx.cs" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Welcome to Deloitte Onboarding</title>
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <style>
            body {
                background: url(img/imgWelcome.png) no-repeat center center fixed;
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                background-size: cover;
            }
        </style>
        <div id="MainImage"></div>
        <asp:LinkButton runat="server" ID="btnSmallProceed" CssClass="btnAdvanceSmall" OnClick="btnSmallProceed_Click" Style="margin: 0; position: absolute; top: 50%; right: 20px; transform: translate(-50%, -50%);"><img id="btnAdvanceSmall" src="img/Start.png" /></asp:LinkButton>
    </form>
</body>
</html>
