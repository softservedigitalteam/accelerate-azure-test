﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Accelerate.master" AutoEventWireup="true" Inherits="_Default" Codebehind="Accelerate-HR.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
                <div class="infobox">
                    <div class="top-bar-alternative">
                        <h3>Mandatory Reading</h3>
                    </div>
                    <div class="infobox well-short" style="color:#000;background:#FFF">
                        <div style="padding-bottom:20px;">
                            <div style="margin-bottom:5px;"><b style="font-size:13px;">Your responibilities</b></div>
                            It is very important that you read the following carefully. It is mandatory to complete all requirements.
                            Non-compliance with firm policies will be subject to the firm’s disciplinary processes.
                            <br />
                        </div>
                        <div style="padding-bottom:20px;">
                            <div style="padding:4px 0px;"><b style="font-size:13px;">1. Mandatory eLearning</b></div>
                            Please note that upon joining the firm you are required to complete the eLearning displayed when clicking on the link below.  Please click on the name of the course to access it.
                            <br />
                            <a href="https://dlpdelc.deloitteresources.com/content.aspx?src=delus&vdr=saba&crscode=01343448" target="_blank" class="anchorAlt">https://dlpdelc.deloitteresources.com/content.aspx?src=delus&vdr=saba&crscode=01343448</a><br />
                        </div>
                        <div style="padding-bottom:20px;">
                            <div style="padding:4px 0px;"><b style="font-size:13px;">2. Independence</b></div>
                            All new hires should complete the joining independence declarations within one month of joining.<br />
                            You can find the link to your eLearning and Independence documentation via the link below.
                            <br />
                            <a href="http://independence.deloitte.co.za/" target="_blank" class="anchorAlt">http://independence.deloitte.co.za/</a><br />
                        </div>
                        <div style="padding-bottom:20px;">
                            <div style="padding:4px 0px;"><b style="font-size:13px;">3. HR Policies and Procedures</b></div>
                            Please formularise yourself with our HR policies and procedures.<br />
                            <a href="http://cambrient.deloitte.co.za/deloitte_hr/view/deloitte_hr/en/page121" target="_blank" class="anchorAlt">http://cambrient.deloitte.co.za/deloitte_hr/view/deloitte_hr/en/page121</a><br />
                        </div>
                    </div>
                </div>
</asp:Content>


