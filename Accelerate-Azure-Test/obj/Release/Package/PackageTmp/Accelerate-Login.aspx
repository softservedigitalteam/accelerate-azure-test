﻿<%@ Page Title="Accelerate Login" Language="C#" AutoEventWireup="true" Inherits="AccelerateLogin" Codebehind="Accelerate-Login.aspx.cs" %>

<!DOCTYPE html>
<!--[if IE 8]> 				 <html class="no-js lt-ie9" lang="en" > <![endif]-->
<!--[if gt IE 8]> <html class="no-js" lang="en" > <!--<![endif]-->

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Accelerate Login</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />

    <!-- Custom styles for this template -->
    <link href="css/login.css" rel="stylesheet" />
    <link href="css/animate-custom.css" rel="stylesheet" />
    <%--###--%>
    <link href="assets/open-sans/open-sans.css" rel="stylesheet" />
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
    <%--###--%>
    <%--<link href="assets/css/appfonts.css" rel="stylesheet" />--%>
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
    <link rel="icon" href="favicon.ico" type="image/x-icon" />
    <style>
        body {
            font-family: 'Open Sans' !important;
            background: url(img/imgLoginBG.jpg) no-repeat center center fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }
    </style>
    <asp:Literal ID="litStyleSheet" runat="server" />
</head>
<body>
    <form id="form1" runat="server">

        <asp:ScriptManager ID="smgLogin" runat="server" ScriptMode="Release" />
        <center>
            <asp:UpdatePanel ID="udpLogin" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel ID="panelLogin" runat="server" DefaultButton="lnkbtnLogin">
                
                <!-- start Login box -->
    	        <div class="container" id="login-block" style="">
    		        <div class="row">
			            <div class="col-sm-6 col-md-4 col-sm-offset-3 col-md-offset-4">
			               <div class="login-box clearfix animated flipInY">
			        	        <div class="login-logo">
			        		        <a href="#"><img src="img/imgLogo.png" /></a>
			        	        </div> 
			        	        <div class="login-form">
			        		        <!-- Start Error box -->
			        		        <div class="alert alert-danger hide" id="Alert" runat="server">
								          <button type="button" class="close" data-dismiss="alert"> &times;</button>
								          <h4><asp:Literal ID="litAlertHeading" runat="server"></asp:Literal>!</h4>
								          <asp:Literal ID="litValidationMessage" runat="server" />
							        </div> <!-- End Error box -->
                                    <div>
						   		         <asp:TextBox ID="txtUsername" runat="server" CssClass="txtUserNameBox" required placeholder="Username"></asp:TextBox>
						   		        </div> 
                                        <div id="divPassword" runat="server">
                                                <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" CssClass="txtPasswordBox" placeholder="Password"></asp:TextBox>
                                                &nbsp;
                                        </div>

                                    <div id="divRememberMe" runat="server" style="text-align:left;height: 55px;">
                                        <asp:CheckBox runat="server" id="chkRememberMe" Text="Remember me" CssClass="checkboxLogin"/>
                                        <br style="clear:both"/>
                                    </div>
                                    <div class="buttonHolder">
                                        <asp:Button ID="lnkbtnLogin" runat="server" CssClass="btn btn-white" Text="Login" OnClick="lnkbtnLogin_Click" />
                                        <asp:Button ID="lnkbtnSubmit" runat="server" CssClass="btn btn-white" Text="Submit" Visible="false" onClick="lnkbtnSubmit_Click" />
                                        <br class="clearfix" style="clear:both"/>
                                    </div>
							        <div class="login-links"> 
					                    <asp:LinkButton ID="lnkbtnForgottenPassword" runat="server" CssClass="linkGreyUnderlined" Text="Forgotten Password?" OnClick="lnkbtnForgottenPassword_Click" />
                                        <asp:LinkButton ID="lnkbtnBackToLogin" runat="server" CssClass="linkGreyUnderlined" Text="Back to login" OnClick="lnkbtnBackToLogin_Click" Visible="false" />
					                    <br />
					                    <a href="mailto:mishanth@stratagc.co.za?subject=Please add me to the system">
					                      Don't have an account?
					                    </a>
							        </div>      		
			        	        </div> 			        	
			               </div>

			            </div>
			        </div>
    	        </div>
      	        <!-- End Login box -->

                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
        </center>
        
        <script src="assets/js/modernizr.custom.js" type="text/javascript"></script>
        <script src="assets/js/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/jquery-1.9.1.min.js"><\/script>')</script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/placeholder-shim.min.js"></script>
        <script src="js/custom.js"></script>
    </form>
</body>
</html>
