﻿<%@ Page Title="Accelerate-Forum" Language="C#" MasterPageFile="~/Accelerate.master" AutoEventWireup="true" Inherits="Accelerate_Forum" Codebehind="Accelerate-Forum.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="infobox">
        <div class="top-bar-alternative">
            <h3>Forums</h3>
        </div>
        <div class="infobox well-short" style="color: #000; background: #FFF">
            <b style="font-size: 13px;">All Forum Topics</b>
            <br />
            <br />
            <asp:Literal ID="litForumTopics" runat="server"></asp:Literal>
        </div>
    </div>
</asp:Content>
