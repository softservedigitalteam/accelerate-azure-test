﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Accelerate.master" AutoEventWireup="true" Inherits="Accelerate_Current_Mission" Codebehind="Accelerate-Current-Mission.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <link rel="stylesheet" href="assets/REDIPS_drag/style.css" type="text/css" />
    <script type="text/javascript" src="assets/REDIPS_drag/header.js"></script>
    <script type="text/javascript" src="assets/REDIPS_drag/redips-drag-min.js"></script>
    <script type="text/javascript" src="assets/REDIPS_drag/script.js"></script>
    
    <script type="text/javascript">
        function displayModalPopup() {
            document.getElementById('<%= btnPopUpClick.ClientID %>').click();
        }
    </script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
            <asp:HiddenField ID="hfTeamshipAllCorrect" runat="server" />
            <asp:HiddenField ID="hfTeamshipIncorrect" runat="server" />
            <asp:HiddenField ID="hfTeamshipCorrect" runat="server" />
            <asp:HiddenField ID="hfTeamshipFullIDCount" runat="server" />

    <asp:UpdatePanel ID="UpdatePanel1" runat="server" EnableViewState="true">
        <ContentTemplate>
            <div class="infobox">
                <div class="row-fluid">
                    <div class="span2 PointsSection">
                        <div class="PointNumbersText">
                            <asp:Literal runat="server" ID="litPoints"></asp:Literal>
                        </div>
                        <br />
                        <br />
                        <div class="points-text-display">POINTS</div>
                    </div>
                    <div class="span10">
                        <div class="row-fluid">
                            <div class="showcaseSection Black">My showcase</div>
                            <asp:LinkButton runat="server" CssClass="phaseActive" OnClick="btnPhase1_Click" ID="btnPhase1">
                                <div class="showcaseSection Blue1"><span class='hidden-laptop'>Phase</span>
                                    1:
                                    <asp:Literal runat="server" ID="litPhase1Text"></asp:Literal>
                                </div>
                            </asp:LinkButton>
                            <asp:LinkButton runat="server" CssClass="phaseActive" ID="btnPhase2" OnClick="btnPhase2_Click" Enabled="false">
                                <div class="showcaseSection Blue2"><span class='hidden-laptop'>Phase</span>
                                    2:
                                    <asp:Literal runat="server" ID="litPhase2Text"></asp:Literal>
                                </div>
                            </asp:LinkButton>
                            <asp:LinkButton runat="server" CssClass="phaseActive" ID="btnPhase3" OnClick="btnPhase3_Click" Enabled="false">
                                <div class="showcaseSection Blue3"><span class='hidden-laptop'>Phase</span>
                                    3:
                                    <asp:Literal runat="server" ID="litPhase3Text"></asp:Literal>
                                </div>
                            </asp:LinkButton>
                        </div>
                        <div class="well">
                            <asp:Repeater ID="rpAchievements" runat="server">
                                <ItemTemplate>
                                    <%#Eval ("strNewRow") %>
                                    <div class="achievement" style="text-align: center; margin: 0 1.2%;">
                                        <a href='<%#Eval ("strLink") %>'>
                                            <img onerror="this.src='images/imgAchievementEmpty.png'" src='<%#Eval ("FullPathForImage") %>' title='<%#Eval ("strTitle") %>' alt='<%#Eval ("strTitle") %>' width='70%' /></a>
                                        <br />
                                        <%#Eval ("strTitle") %>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            <div class="infobox">
                
                <div class="top-bar-alternative">
                    <h3>Your Current Mission - Earn your badge here</h3>
                </div>
                <div class="well">
                    <asp:Literal ID="litCurrentMission" runat="server"></asp:Literal>
                    <div id="TopDiv" class="well-short" style="background: #80c342; padding-bottom: 12px;">
                        <div class="missionInformation" style="color: #000; font-size: 13px;">
                            <b>
                                <asp:Literal ID="litTitle" runat="server"></asp:Literal></b>
                        </div>
                        <br />
                        <asp:Literal ID="litBanner" runat="server"></asp:Literal>
                    </div>
                    <br />
                    <div class="well-short" style="background: #FFF; color: #000; padding-bottom: 12px;">
                        <b style="font-size: 13px;">Your Mission</b>
                        <br />
                        <br />
                        <asp:Literal ID="litDescription" runat="server"></asp:Literal>
                        <br />
                        <br />
                        <%--<asp:LinkButton ID="lnkbtnDisplayPDF" runat="server" Visible="false" OnClientClick="displayModalPopup()" CssClass="anchorAlt"></asp:LinkButton>--%>
                        <a id="lnkbtnDisplayPDF" runat="server" Visible="false" onclick="displayModalPopup();" class="anchorAlt"></a>
                        <asp:Literal ID="litVideo" runat="server"></asp:Literal>

                    </div>
                    <%--<div id="pdfDiv" runat="server" class="well-short" style="background: #FFF; color: #000; padding-bottom: 12px; display: none;">
                        <iframe style="width: 100%; height: 500px; border: none;" id="pdfFrameViewer" runat="server" />
                    </div>--%>

                    <br />
                    <div class="well-short" style="background: #FFF; color: #000; padding-bottom: 12px;">
                        <b style="font-size: 13px;">Your Task</b>
                        <br />
                        <br />

                        <asp:UpdatePanel ID="upQs" runat="server" EnableViewState="true">
                            <ContentTemplate>
                                <asp:PlaceHolder runat="server" ID="phAnswers">
                                    <asp:Repeater runat="server" ID='rpMisionQuestions' OnItemDataBound="rpMisionQuestions_ItemDataBound">
                                        <ItemTemplate>
                                            <%#Eval ("strQuestion") %><br />
                                            <br />
                                            <asp:TextBox runat="server" ID="txtQuestionAnswer" CommandArgument='<%#Eval ("iMissionQuestionID") %>' CssClass="textbox"></asp:TextBox><br />
                                            <br />
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </asp:PlaceHolder>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="click" />
                            </Triggers>
                        </asp:UpdatePanel>

                        <%--<asp:UpdatePanel ID="upQs2" runat="server" EnableViewState="true" UpdateMode="Always">
                            <ContentTemplate>
                                <asp:PlaceHolder runat="server" ID="phAnswers2">
                                    <asp:Repeater runat="server" ID='rpMissionQuestions2'>
                                        <ItemTemplate>
                                            <div style="float: left;">
                                                <img src='<%#Eval ("FullPathForImage") %>' width='70px' height='70px' />
                                            </div>
                                            <div style="float: left; padding-left: 20px; width: 75%">
                                                <asp:TextBox runat="server" ID="txtName" CommandArgument='<%#Eval ("iTeamShipID") %>' placeholder="Name" CssClass="textbox"></asp:TextBox>
                                                <asp:TextBox runat="server" ID="txtTitle" CommandArgument='<%#Eval ("iTeamShipID") %>' placeholder="Position" CssClass="textbox"></asp:TextBox>
                                            </div>
                                            <br style="clear: both" />
                                            <br style="clear: both" />
                                            <br style="clear: both" />
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </asp:PlaceHolder>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="click" />
                            </Triggers>
                        </asp:UpdatePanel>--%>

                        <asp:UpdatePanel ID="upQs3" runat="server" EnableViewState="True">
                            <ContentTemplate>
                                <asp:PlaceHolder runat="server" ID="PlaceHolder1">
                                    <asp:Repeater runat="server" ID='rpMissionDropDownQuestions'>
                                        <ItemTemplate>
                                            <br style="clear: both" />
                                            <div class="span3">
                                                <%#Eval ("strQuestion") %>
                                            </div>
                                            <div class="span3" style="float: left; margin-top: -6px;">
                                                <asp:DropDownList ID="lstQuestionAnswer" runat="server" CommandArgument='<%#Eval ("iMissionDropDownQuestionID") %>' CssClass="textbox" Enabled="true"></asp:DropDownList><br />
                                            </div>
                                            <br style="clear: both" />
                                            <br style="clear: both" />
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </asp:PlaceHolder>

                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="click" />
                            </Triggers>
                        </asp:UpdatePanel>

                        <asp:UpdatePanel ID="upQs4" runat="server" EnableViewState="true" UpdateMode="Always">
                            <ContentTemplate>
                                <asp:PlaceHolder runat="server" ID="PlaceHolder2">
                                    <div id="main-container">
                                        <div id="teamshipDragDropInfo"><asp:Literal ID="litLeadershipInfo" runat="server"></asp:Literal> </div>
                                        <div id="redips-drag">
                                            <div id="left">
                                                
                                                <table id="tblTeamshipInfo">
                                                    
                                                    <tbody>
                                                        <asp:Repeater runat="server" ID="rpTeamShipDragDropNames">
                                                            <ItemTemplate>
                                                                
                                                                <tr>
                                                                    
                                                                    <td class="redips-drag redips-single noborder" style="background-image:url('images/teamLeadershipBg.jpg'); background-position:center; width:75px; border:none;" id="cell<%#Eval ("iTeamShipID") %>">
                                                                       </td>
                                                                    <td class="redips-mark" style="padding-left:20px;">
                                                                        <%--<div style="background-color:#d7d8d6;">--%>
                                                                        <div id="teamInfoDiv<%#Eval ("iTeamShipID") %>" style="background-color:#e3e3e2; text-align:left; height:75px; display:table-cell; width:100%; vertical-align:middle;">
                                                                            <asp:TextBox ReadOnly="true" Enabled="false" Width="90%" runat="server" ID="txtName" CommandArgument='<%#Eval ("iTeamShipID") %>' placeholder="Name" CssClass="textbox" style="font-weight:bold; margin:0; background-color:none;"></asp:TextBox>
                                                                        <asp:TextBox ReadOnly="true" Enabled="false" Width="90%" runat="server" ID="txtTitle" CommandArgument='<%#Eval ("iTeamShipID") %>' placeholder="Position" CssClass="textbox" style="margin:0; background-color:none;"></asp:TextBox>
                                                                        </div>
                                                                        
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </tbody>
                                                </table>
                                                <br style="clear: both" />
                                            <br style="clear: both" />
                                            </div>
                                            <div id="right">
                                                
                                                <table id="tblTeamshipImages" >
                                                    
                                                    <tbody>
                                                        <asp:Repeater runat="server" ID='rpTeamShipDragDropImages'>
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td id="imageCell<%#Eval ("iTeamShipID") %>" class="dark redips-single" style="width: 75px; height: 75px;">
                                                                        <div class="redips-drag" id="image<%#Eval ("iTeamShipID") %>" style="width: 75px; height: 75px; border:none !important; border-style:none !important;" >
                                                                            <img src="<%#Eval ("FullPathForImage") %>" style="width: 75px; height: 75px;" />
                                                                        </div>
                                                                    </td>
                                                                    <%--<td class="light redips-only">
                                                                        <%#Eval ("litLeaderInfo") %>
                                                                    </td>--%>
                                                                </tr>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </tbody>
                                                </table>
                                                <br style="clear: both" />
                                            <br style="clear: both" />
                                            </div>
                                        </div>
                                    </div>
                                </asp:PlaceHolder>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="click" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="Green3 btn" OnClick="btnSave_Click" EnableViewState="true" />
                    </div>
                </div>
            </div>
            
            <asp:UpdateProgress ID="updateProgress" runat="server">
                <ProgressTemplate>
                    <div style="position: fixed; height: 100%; width: 100%; overflow: show; margin: auto; top: 0; left: 0; bottom: 0; right: 0; z-index: 9999999; background-color: #fff; opacity: 0.8;">
                        <center><asp:Image ID="imgUpdateProgress2" runat="server" ImageUrl="img/Loader.gif" AlternateText="Loading ..." ToolTip="Loading ..." style="opacity:1;position:fixed;top: 43%;left:50%"/></center>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </ContentTemplate>
    </asp:UpdatePanel>
    
   <%--##MODAL POPUP--%>
    <div class="infobox">

        <cc1:ModalPopupExtender ID="ModalPopupExtenderViewer" runat="server" TargetControlID="btnPopUpClick"
        PopupControlID="pnlView" BackgroundCssClass="modalBackground" CancelControlID="btnBack"
        RepositionMode="RepositionOnWindowResize" BehaviorID="ModalPopupExtenderViewerBID">
        </cc1:ModalPopupExtender>        
 
    <asp:Panel ID="pnlView" runat="server" HorizontalAlign="Center" Width="80%" style="display: none">
        <div>
            <div class="top-bar">
               <%--<h3>Deloitte</h3> --%>
                <%--<asp:LinkButton style="float:right; padding-top:5px;" runat="server" ID="btnCloseImages" CssClass="btnCloseDefaultImages" ForeColor="White" Font-Bold="true">X</asp:LinkButton>--%>
                <input id="btnExit" type="button" value="X" style="float:right; padding-top:5px; color:black;" onclick="closePopup();"/>
            </div>
            <div class="well">
                <iframe id="ifrmItemViewer" style="border: none; width:100%; height:80vh;" runat="server"/> 
                <div style="padding-right: 10px; float:left; padding-top:20px;">
            <%--<asp:LinkButton ID="btnBack" runat="server" CssClass="Green3 btn" Text="Back" AlternateText="Back" />--%>
            <input id="btnBack" type="button" value="Back" class="Green3 btn"/>
        </div>
           </div>
            
        </div>
    </asp:Panel>
        <input type="button" id="btnPopUpClick" runat="server" style="display: none" />
    </div>
    <%--##MODAL POPUP--%>

    <%--##MODAL POPUP--%>
         <asp:button id="Button1" runat="server" text="Button" style="display: none"/>
        <cc1:modalpopupextender id="ModalPopupExtender1" runat="server" 
	        cancelcontrolid="btnCancel" okcontrolid="btnOkay" 
	        targetcontrolid="Button1" popupcontrolid="Panel1" 
	        popupdraghandlecontrolid="PopupHeader" drag="true" 
	        backgroundcssclass="ModalPopupBG">
        </cc1:modalpopupextender>

        <asp:panel id="Panel1" style="display: none" runat="server">
	        <div class="HellowWorldPopup">
                        <div class="PopupHeader" id="PopupHeader">Header</div>
                        <div class="PopupBody">
                            <p>This is a simple modal dialog</p>
                        </div>
                        <div class="Controls">
                            <input id="btnOkay" type="button" value="Done" />
                            <input id="btnCancel" type="button" value="Cancel" />
		        </div>
                </div>
        </asp:panel>
   <%--##MODAL POPUP--%>

</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="ScrMang" runat="Server">
    <script type="text/javascript">
        function teamshipCorrectCount() {
            var correctCount = 0;
            var incorrectCount = "";
            var correctPlacementCount = "";
            var fullIDCount = "";
            for (var i = 0; i < tblTeamshipInfo.rows.length; i++)
            {
                var stringCellID = tblTeamshipInfo.rows[i].cells[0].id;

                stringCellID = stringCellID.substring(4, 6);
                var stringImageID;
                //var stringImageID = tblTeamshipInfo.rows[i].cells[1].children[0].childElementCount;
                if (tblTeamshipInfo.rows[i].cells[0].childElementCount != 0)
                {
                    stringImageID = tblTeamshipInfo.rows[i].cells[0].children[0].id;
                    stringImageID = stringImageID.substring(5, 7);
                }
                else
                {
                    stringImageID = "null";
                }
                //var stringImageID = tblTeamshipInfo.rows[i].cells[1].children[0].id;
                //alert(stringImageID);
                //stringImageID = stringImageID.substring(5, 7);
                
                if (fullIDCount != "") {
                    fullIDCount = fullIDCount + "," + stringCellID;
                }
                else {
                    fullIDCount = fullIDCount + stringCellID;
                }

                if (stringCellID == stringImageID)
                {
                    
                    correctCount = correctCount + 1;

                    if (correctPlacementCount != "")
                    {
                        correctPlacementCount = correctPlacementCount + "," + stringCellID;
                    }
                    else
                    {
                        correctPlacementCount = correctPlacementCount + stringCellID;
                    }
                }
                else
                {
                    if (incorrectCount != "")
                    {
                        incorrectCount = incorrectCount + "," + stringCellID;
                    }
                    else
                    {
                        incorrectCount = incorrectCount + stringCellID;
                    }
                }

                if (correctCount == tblTeamshipInfo.rows.length)
                {
                    document.getElementById('<%=hfTeamshipAllCorrect.ClientID%>').value = true;
                }
                else
                {
                    document.getElementById('<%=hfTeamshipAllCorrect.ClientID%>').value = false;
                    document.getElementById('<%=hfTeamshipIncorrect.ClientID%>').value = incorrectCount;
                    document.getElementById('<%=hfTeamshipCorrect.ClientID%>').value = correctPlacementCount;
                    document.getElementById('<%=hfTeamshipFullIDCount.ClientID%>').value = fullIDCount;
                }
                //alert('HELLO');
                //alert(correctCount);
            }
        }
    </script>

    <%--<script type="text/javascript">
        function setCorrectElements(correctElements) {
            var myArrayElements = "";
            myArrayElements = correctElements.split(',');

            for (var k = 0; k < myArrayElements.length; k++)
            {
                var cellID = "";
                var imageID = "";
                cellID = "cell" + myArrayElements[k];
                imageID = "image" + myArrayElements[k];
                for (var i = 0; i < tblTeamshipInfo.rows.length; i++)
                {
                    var stringCellID = "";
                    stringCellID = tblTeamshipInfo.rows[i].cells[1].id;
                    if (stringCellID == cellID)
                    {
                        for (var r = 0; r < tblTeamshipImages.rows.length; r++)
                        {
                            var stringImageID = "";
                            stringImageID = tblTeamshipImages.rows[r].cells[0].children[0].id;
                            alert(stringImageID);
                            if (stringImageID == imageID)
                            {
                                var stringImage = document.getElementById(stringImageID);
                                var strTableCell = document.getElementById(stringCellID);
                                alert(stringImage + " | " + strTableCell);
                                //strTableCell.appendChild(stringImage);
                            }
                        }
                    }
                }
            }
        }
    </script>--%>

    <script type="text/javascript">
        function setCorrectElements(correctElements) {
            var myArrayElements = "";
            myArrayElements = correctElements.split(',');

            for (var k = 0; k < myArrayElements.length; k++)
            {
                var cellID = "";
                var imageID = "";
                cellID = "cell" + myArrayElements[k];
                imageID = "image" + myArrayElements[k];
                for (var i = 0; i < tblTeamshipInfo.rows.length; i++)
                {
                    var stringCellID = "";
                    stringCellID = tblTeamshipInfo.rows[i].cells[0].id;
                    //Getting DivID
                    //stringDivID = tblTeamshipInfo.rows[i].cells[1].children[0].id;
                    if (stringCellID == cellID)
                    {
                        for (var r = 0; r < tblTeamshipImages.rows.length; r++)
                        {
                            var stringImageID = "";
                            stringImageID = tblTeamshipImages.rows[r].cells[0].children[0].id;
                            var stringImageCellID = "";
                            stringImageCellID = tblTeamshipImages.rows[r].cells[0].id;
                            //alert(stringImageID);
                            if (stringImageID == imageID)
                            {
                                
                                var stringDragImage = document.getElementById(stringImageID);
                                var strDropImageTableCell = document.getElementById(stringCellID);
                                var strDragImageTableCell = document.getElementById(stringImageCellID);
                                
                                var imageCopy = stringDragImage.cloneNode(true);
                                strDropImageTableCell.appendChild(imageCopy);
                                //alert(imageCopy);
                                //strTableCell.appendChild(imageCopy);
                                var div = document.createElement('div');
                                div.style.display = 'none';
                                strDragImageTableCell.replaceChild(div, stringDragImage);
                                //alert(stringImageID);
                            }
                        }
                    }
                }
            }
            for (var q = 0; q < tblTeamshipInfo.rows.length; q++)
            {
                var stringCheckCellID = tblTeamshipInfo.rows[q].cells[0].id;
                var getCheckCellID = document.getElementById(stringCheckCellID);
                if (getCheckCellID.childElementCount == 0)
                {
                    var stringGetInfoDivID = tblTeamshipInfo.rows[q].cells[1].children[0].id;
                    var getDivChildElement1ID = tblTeamshipInfo.rows[q].cells[1].children[0].children[0].id;
                    var getDivChildElement2ID = tblTeamshipInfo.rows[q].cells[1].children[0].children[1].id;

                    var GetInfoDiv = document.getElementById(stringGetInfoDivID);
                    var GetDivChildElement1 = document.getElementById(getDivChildElement1ID);
                    var GetDivChildElement2 = document.getElementById(getDivChildElement2ID);
                    GetInfoDiv.style.backgroundColor = '#e4babc';
                    GetDivChildElement1.style.backgroundColor = '#f09ca2';
                    GetDivChildElement2.style.backgroundColor = '#f09ca2';
                }
            }
        }
    </script>

    <script type="text/javascript">
        function sayHello()
        {
            alert("HELLO");
        }
        </script>

    <script type="text/javascript">
        function setMissionElements() {
            for (var i = 0; i < tblTeamshipInfo.rows.length; i++) {
                var stringCellID = "";
                stringCellID = tblTeamshipInfo.rows[i].cells[1].id;
                for (var r = 0; r < tblTeamshipImages.rows.length; r++) {
                    var stringImageID = "";
                    stringImageID = tblTeamshipImages.rows[r].cells[0].children[0].id;
                    var stringImageCellID = "";
                    stringImageCellID = tblTeamshipImages.rows[r].cells[0].id;

                    stringCellID = stringCellID.substring(4, 6);
                    stringImageID = stringImageID.substring(5, 7);
                    alert(stringImageID);
                    if (stringCellID == stringImageID) {

                        var stringDragImage = document.getElementById(stringImageID);
                        var strDropImageTableCell = document.getElementById(stringCellID);
                        var strDragImageTableCell = document.getElementById(stringImageCellID);

                        var imageCopy = stringDragImage.cloneNode(true);
                        strDropImageTableCell.appendChild(imageCopy);
                        alert(imageCopy);
                        //strTableCell.appendChild(imageCopy);
                        var div = document.createElement('div');
                        div.style.display = 'none';
                        strDragImageTableCell.replaceChild(div, stringDragImage);

                        //alert(stringImageID);

                    }
                }
            }
        }
    </script>
    
    <script type="text/javascript">
        function pageLoad() {
            var modalPopup = $find('ModalPopupExtenderViewerBID');
            modalPopup.add_shown(function () {
                modalPopup._backgroundElement.addEventListener("click", function () {
                    modalPopup.hide();
                });
            });
        };

        function closePopup() {
            var modalPopup = $find('ModalPopupExtenderViewerBID');
            modalPopup.hide();
        }
    </script>

    <script src='assets/js/jquery.hotkeys.js'></script>
    <script src='assets/js/calendar/fullcalendar.min.js'></script>
    <script src="assets/js/jquery-ui-1.10.2.custom.min.js"></script>
    <script src="assets/js/jquery.pajinate.js"></script>
    <script src="assets/js/jquery.prism.min.js"></script>
    <script src="assets/js/jquery.dataTables.min.js"></script>
    <script src="assets/js/charts/jquery.flot.time.js"></script>
    <script src="assets/js/charts/jquery.flot.pie.js"></script>
    <script src="assets/js/charts/jquery.flot.resize.js"></script>
    <asp:Literal ID="litBootstrap" runat="server"></asp:Literal>
    <script src="assets/js/bootstrap/bootstrap-wysiwyg.js"></script>
    <script src="assets/js/bootstrap/bootstrap-typeahead.js"></script>
    <script src="assets/js/jquery.easing.min.js"></script>
    <script src="assets/js/jquery.chosen.min.js"></script>
    <script src="assets/js/avocado-custom.js"></script>
    
</asp:Content>

