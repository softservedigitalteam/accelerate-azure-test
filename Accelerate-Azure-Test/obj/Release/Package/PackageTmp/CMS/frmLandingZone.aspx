﻿<%@ Page Title="Dashboard" Language="C#" MasterPageFile="~/CMS/CMS.master" AutoEventWireup="true" Inherits="CMS_frmLandingZone" Codebehind="frmLandingZone.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <!--Dashboard content-->
    <div class="dashboardContent">
        <asp:Literal ID="litReport" runat="server"></asp:Literal>
    </div>

</asp:Content>

