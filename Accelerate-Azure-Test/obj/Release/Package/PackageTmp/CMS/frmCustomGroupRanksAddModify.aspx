<%@ Page Title="Ranks" Language="C#" MasterPageFile="~/CMS/CMS.master" AutoEventWireup="true" Inherits="CMS_frmCustomGroupRanksAddModify" Codebehind="frmCustomGroupRanksAddModify.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div id="mandatoryDiv" class="mandatoryInvalidDiv" runat="server" visible="false">
        <asp:Label ID="lblValidationMessage" runat="server"></asp:Label>
    </div>

<%--    <div class="controlDiv">
        <div class="imageHolderCommonDiv">
            <div class="validationImageMandatory"></div>
        </div>
        <div class="labelDiv">Business Unit:</div>
        <div class="fieldDiv">
            <asp:DropDownList ID="lstGroup" runat="server" CssClass="roundedCornerDropDownList" onblur="setValid(this, 0);"></asp:DropDownList>
        </div>
        <br class="clearingSpacer" />
    </div>--%>

    <div class="controlDiv">
        <div class="imageHolderCommonDiv">
            <div class="validationImageMandatory"></div>
        </div>
        <div class="labelDiv">Rank Setting Title:</div>
        <div class="fieldDiv">
            <asp:TextBox ID="txtTag" runat="server" CssClass="roundedCornerTextBox" onKeyUp="return SetMaxLength(this,50)" onblur="setValid(this);" />
        </div>
        <br class="clearingSpacer" />
    </div>

    <br />
    <asp:UpdatePanel runat="server" ID="updRanks">
        <ContentTemplate>
    <div class="controlDiv">
        <div class="imageHolderCommonDiv">
            <div class="validationImageMandatory"></div>
        </div>
        <div class="labelDiv">Rank 1 Title:</div>
        <div class="fieldDiv">
            <asp:TextBox ID="txtRank1Title" runat="server" CssClass="roundedCornerTextBox" onKeyUp="return SetMaxLength(this,50)" onblur="setValid(this);" AutoPostBack="true" OnTextChanged="txtRank1Title_TextChanged" />
        </div>
        <br class="clearingSpacer" />
    </div>

    <div class="controlDiv">
        <div class="imageHolderCommonDiv">
            <div class="validationImageMandatory"></div>
        </div>
        <div class="labelDiv">Rank 1 Points:</div>
        <div class="fieldDiv">
            <asp:TextBox ID="txtRank1Points" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="jsIsNumeric(this)" onblur="jsIsNumeric(this)" AutoPostBack="true" OnTextChanged="txtRank1Points_TextChanged" /><asp:Label runat="server" ID="lblRank1" Visible="false"></asp:Label>
        </div>
        <br class="clearingSpacer" />
    </div>

    <br />

    <asp:Panel runat="server" ID="pnlRank2" Visible="false">
        <div class="controlDiv">
            <div class="imageHolderCommonDiv">
                <div class="validationImageMandatory"></div>
            </div>
            <div class="labelDiv">Rank 2 Title:</div>
            <div class="fieldDiv">
                <asp:TextBox ID="txtRank2Title" runat="server" CssClass="roundedCornerTextBox" onKeyUp="return SetMaxLength(this,50)" onblur="setValid(this);" AutoPostBack="true" OnTextChanged="txtRank2Title_TextChanged" />
            </div>
            <br class="clearingSpacer" />
        </div>


        <div class="controlDiv">
            <div class="imageHolderCommonDiv">
                <div class="validationImageMandatory"></div>
            </div>
            <div class="labelDiv">Rank 2 Points:</div>
            <div class="fieldDiv">
                <asp:TextBox ID="txtRank2Points" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="jsIsNumeric(this)" onblur="jsIsNumeric(this)" AutoPostBack="true" OnTextChanged="txtRank2Points_TextChanged" /><asp:Label runat="server" ID="lblRank2" Visible="false"></asp:Label>
            </div>
            <br class="clearingSpacer" />
        </div>


        <br />
    </asp:Panel>


    <asp:Panel runat="server" ID="pnlRank3" Visible="false">
        <div class="controlDiv">
            <div class="imageHolderCommonDiv">
                <div class="validationImageMandatory"></div>
            </div>
            <div class="labelDiv">Rank 3 Title:</div>
            <div class="fieldDiv">
                <asp:TextBox ID="txtRank3Title" runat="server" CssClass="roundedCornerTextBox" onKeyUp="return SetMaxLength(this,50)" onblur="setValid(this);" AutoPostBack="true" OnTextChanged="txtRank3Title_TextChanged" />
            </div>
            <br class="clearingSpacer" />
        </div>

        <div class="controlDiv">
            <div class="imageHolderCommonDiv">
                <div class="validationImageMandatory"></div>
            </div>
            <div class="labelDiv">Rank 3 Points:</div>
            <div class="fieldDiv">
                <asp:TextBox ID="txtRank3Points" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="jsIsNumeric(this)" onblur="jsIsNumeric(this)" AutoPostBack="true" OnTextChanged="txtRank3Points_TextChanged" /><asp:Label runat="server" ID="lblRank3" Visible="false"></asp:Label>
            </div>
            <br class="clearingSpacer" />
        </div>


        <br />
    </asp:Panel>


    <asp:Panel runat="server" ID="pnlRank4" Visible="false">
        <div class="controlDiv">
            <div class="imageHolderCommonDiv">
                <div class="validationImageMandatory"></div>
            </div>
            <div class="labelDiv">Rank 4 Title:</div>
            <div class="fieldDiv">
                <asp:TextBox ID="txtRank4Title" runat="server" CssClass="roundedCornerTextBox" onKeyUp="return SetMaxLength(this,50)" onblur="setValid(this);" AutoPostBack="true" OnTextChanged="txtRank4Title_TextChanged" />
            </div>
            <br class="clearingSpacer" />
        </div>

        <div class="controlDiv">
            <div class="imageHolderCommonDiv">
                <div class="validationImageMandatory"></div>
            </div>
            <div class="labelDiv">Rank 4 Points:</div>
            <div class="fieldDiv">
                <asp:TextBox ID="txtRank4Points" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="jsIsNumeric(this)" onblur="jsIsNumeric(this)" AutoPostBack="true" OnTextChanged="txtRank4Points_TextChanged" /><asp:Label runat="server" ID="lblRank4" Visible="false"></asp:Label>
            </div>
            <br class="clearingSpacer" />
        </div>


        <br />
    </asp:Panel>


    <asp:Panel runat="server" ID="pnlRank5" Visible="false">
        <div class="controlDiv">
            <div class="imageHolderCommonDiv">
                <div class="validationImageMandatory"></div>
            </div>
            <div class="labelDiv">Rank 5 Title:</div>
            <div class="fieldDiv">
                <asp:TextBox ID="txtRank5Title" runat="server" CssClass="roundedCornerTextBox" onKeyUp="return SetMaxLength(this,50)" onblur="setValid(this);" AutoPostBack="true" OnTextChanged="txtRank5Title_TextChanged" />
            </div>
            <br class="clearingSpacer" />
        </div>

        <div class="controlDiv">
            <div class="imageHolderCommonDiv">
                <div class="validationImageMandatory"></div>
            </div>
            <div class="labelDiv">Rank 5 Points:</div>
            <div class="fieldDiv">
                <asp:TextBox ID="txtRank5Points" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="jsIsNumeric(this)" onblur="jsIsNumeric(this)" AutoPostBack="true" OnTextChanged="txtRank5Points_TextChanged" /><asp:Label runat="server" ID="lblRank5" Visible="false"></asp:Label>
            </div>
            <br class="clearingSpacer" />
        </div>


        <br />
    </asp:Panel>


    <asp:Panel runat="server" ID="pnlRank6" Visible="false">
        <div class="controlDiv">
            <div class="imageHolderCommonDiv">
                <div class="validationImageMandatory"></div>
            </div>
            <div class="labelDiv">Rank 6 Title:</div>
            <div class="fieldDiv">
                <asp:TextBox ID="txtRank6Title" runat="server" CssClass="roundedCornerTextBox" onKeyUp="return SetMaxLength(this,50)" onblur="setValid(this);" AutoPostBack="true" OnTextChanged="txtRank6Title_TextChanged" />
            </div>
            <br class="clearingSpacer" />
        </div>

        <div class="controlDiv">
            <div class="imageHolderCommonDiv">
                <div class="validationImageMandatory"></div>
            </div>
            <div class="labelDiv">Rank 6 Points:</div>
            <div class="fieldDiv">
                <asp:TextBox ID="txtRank6Points" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="jsIsNumeric(this)" onblur="jsIsNumeric(this)" AutoPostBack="true" OnTextChanged="txtRank6Points_TextChanged" /><asp:Label runat="server" ID="lblRank6" Visible="false"></asp:Label>
            </div>
            <br class="clearingSpacer" />
        </div>


        <br />
    </asp:Panel>


    <asp:Panel runat="server" ID="pnlRank7" Visible="false">
        <div class="controlDiv">
            <div class="imageHolderCommonDiv">
                <div class="validationImageMandatory"></div>
            </div>
            <div class="labelDiv">Rank 7 Title:</div>
            <div class="fieldDiv">
                <asp:TextBox ID="txtRank7Title" runat="server" CssClass="roundedCornerTextBox" onKeyUp="return SetMaxLength(this,50)" onblur="setValid(this);" OnTextChanged="txtRank7Title_TextChanged" />
            </div>
            <br class="clearingSpacer" />
        </div>

        <div class="controlDiv">
            <div class="imageHolderCommonDiv">
                <div class="validationImageMandatory"></div>
            </div>
            <div class="labelDiv">Rank 7 Points:</div>
            <div class="fieldDiv">
                <asp:TextBox ID="txtRank7Points" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="jsIsNumeric(this)" onblur="jsIsNumeric(this)" OnTextChanged="txtRank7Points_TextChanged" /><asp:Label runat="server" ID="lblRank7" Visible="false"></asp:Label>
            </div>
            <br class="clearingSpacer" />
        </div>


        <br />
    </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="txtRank1Title" />
            <asp:PostBackTrigger ControlID="txtRank2Title" />
            <asp:PostBackTrigger ControlID="txtRank3Title" />
            <asp:PostBackTrigger ControlID="txtRank4Title" />
            <asp:PostBackTrigger ControlID="txtRank5Title" />
            <asp:PostBackTrigger ControlID="txtRank6Title" />
            <asp:PostBackTrigger ControlID="txtRank7Title" />
            <asp:PostBackTrigger ControlID="txtRank1Points" />
            <asp:PostBackTrigger ControlID="txtRank2Points" />
            <asp:PostBackTrigger ControlID="txtRank3Points" />
            <asp:PostBackTrigger ControlID="txtRank4Points" />
            <asp:PostBackTrigger ControlID="txtRank5Points" />
            <asp:PostBackTrigger ControlID="txtRank6Points" />
            <asp:PostBackTrigger ControlID="txtRank7Points" />
        </Triggers>
        </asp:UpdatePanel>


    <div class="Line"></div>
    <div class="buttonsRightDiv">
        <asp:LinkButton ID="lnkbtnBack" runat="server" CssClass="backButton" OnClick="lnkbtnBack_Click" OnClientClick='history.go(-1);return false;' />
        <asp:LinkButton ID="lnkbtnSave" runat="server" CssClass="saveButton" OnClick="lnkbtnSave_Click" />
        <asp:LinkButton ID="lnkbtnClear" runat="server" CssClass="clearButton" OnClick="lnkbtnClear_Click" />
    </div>
</asp:Content>
