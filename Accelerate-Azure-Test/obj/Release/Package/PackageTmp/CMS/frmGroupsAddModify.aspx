<%@ Page Title="Business Units" Language="C#" MasterPageFile="~/CMS/CMS.master" AutoEventWireup="true" Inherits="CMS_frmGroupsAddModify" Codebehind="frmGroupsAddModify.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" lang="javascript">
        function CheckOnOff(rdoId, gridName) {
            var rdo = document.getElementById(rdoId);
            /* Getting an array of all the INPUT controls on the form.*/
            var rdo = document.getElementById(rdoId);
            var all = document.getElementsByTagName("input");
            for (i = 0; i < all.length; i++) {
                /*Checking if it is a radio button, and also checking if the id of that radio button is different than rdoId */
                if (all[i].type == "radio" && all[i].id != rdo.id) {
                    var count = all[i].id.indexOf(gridName);
                    if (count != -1) {
                        all[i].checked = false;
                    }
                }
            }
            rdo.checked = true; /* Finally making the clicked radio button CHECKED */
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="mandatoryDiv" class="mandatoryInvalidDiv" runat="server" visible="false">
        <asp:Label ID="lblValidationMessage" runat="server"></asp:Label>
    </div>
    <div class="legend">
        <div class="legend-Master-With-Groups-Indicator"></div>
        <p class="legend-Text">Master Business Unit <br /><br /><i>(with a sub-business unit under it and without a parent)</i></p>
        <div class="legend-Sub-Group-Indicator"></div>
        <p class="legend-Text">Sub-Business Unit <br /><br /><i>(with a parent above it and a sub-business unit below it)</i></p>
        <div class="legend-Master-No-Sub-Groups-Indicator"></div>
        <p class="legend-Text">Root Business Unit <br /><br /><i>(no sub-business unit)</i></p>
        <p>Please note: Each Business Unit MUST have a Master!!
            <br style="clear:both;"/>
        </p>
    </div>
    <asp:UpdatePanel runat="server" ID="upTree">
        <ContentTemplate>
            <div class="controlDiv">
                <div class="imageHolderCommonDiv">
                    <div class="validationImageValid"></div>
                </div>
                <div class="labelDiv">
                    Business Unit:
                </div>
                <div class="fieldDiv">
                    <asp:TreeView ID="treeGroups" runat="server" ExpandImageUrl="images/imgPlus.png" CollapseImageUrl="images/imgMinus.png" CssClass="TreeView" RootNodeStyle-ForeColor="Green" ParentNodeStyle-ForeColor="OrangeRed">
                        <NodeStyle ForeColor="Black" CssClass="TreeviewNode" />
                        <SelectedNodeStyle CssClass="TreeviewSelected" />
                    </asp:TreeView>
                </div>
                <br class="clearingSpacer" />
            </div>
        </ContentTemplate>
        <Triggers>
                
        </Triggers>
    </asp:UpdatePanel>

    <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Welcome Message:</div>
         <div class="fieldDiv">
             <asp:DropDownList ID="lstWelcome" runat="server" CssClass="roundedCornerDropDownList" onblur="setValid(this, 0);"></asp:DropDownList>
         </div>
         <br class="clearingSpacer" />
     </div>

    <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Completion Message:</div>
         <div class="fieldDiv">
             <asp:DropDownList ID="lstComplete" runat="server" CssClass="roundedCornerDropDownList" onblur="setValid(this, 0);"></asp:DropDownList>
         </div>
         <br class="clearingSpacer" />
     </div>

    <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Color Scheme:</div>
         <div class="fieldDiv">
             <asp:DropDownList ID="lstTheme" runat="server" CssClass="roundedCornerDropDownList" onblur="setValid(this, 0);"></asp:DropDownList>
         </div>
         <br class="clearingSpacer" />
     </div>

    <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Company:</div>
         <div class="fieldDiv">
             <asp:DropDownList ID="lstCompany" runat="server" CssClass="roundedCornerDropDownList" onblur="setValid(this, 0);"></asp:DropDownList>
         </div>
         <br class="clearingSpacer" />
     </div>

    <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Rank Setting:</div>
         <div class="fieldDiv">
             <asp:DropDownList ID="lstRankSettings" runat="server" CssClass="roundedCornerDropDownList" onblur="setValid(this, 0);"></asp:DropDownList>
         </div>
         <br class="clearingSpacer" />
     </div>

    <div class="controlDiv">
        <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
        <div class="labelDiv">Title:</div>
        <div class="fieldDiv">
            <asp:TextBox ID="txtTitle" runat="server" CssClass="roundedCornerTextBoxMultiLine2" onKeyUp="return SetMaxLength(this,100)" onblur="setValid(this)" TextMode="MultiLine"/>
        </div>
        <br class="clearingSpacer" />
    </div>
    <div class="controlDiv">
        <div class="imageHolderCommonDiv">
            <p>
            </p>
        </div>
        <div class="labelDiv">
            Description:</div>
        <div class="fieldDiv">
            <asp:TextBox ID="txtDescription" runat="server" CssClass="roundedCornerTextBoxMultiLine5"
                onKeyUp="return SetMaxLength(this,2500)" TextMode="MultiLine" Rows="5" />
        </div>
        <br class="clearingSpacer" />
    </div>
    <%--<div class="controlDiv">
        <div class="imageHolderCommonDiv">
            <div class="validationImageMandatory">
            </div>
        </div>
        <div class="labelDiv">
            Color:</div>
        <div class="fieldDiv">
            <asp:TextBox ID="txtColor" runat="server" CssClass="roundedCornerTextBoxMultiLine2" onKeyUp="return SetMaxLength(this,100)" onblur="setValid(this)" TextMode="MultiLine"/>
        </div>
        <br class="clearingSpacer" />
    </div>--%>
    <%--<div class="controlDiv">
        <div class="imageHolderCommonDiv">
            <div class="validationImageMandatory">
            </div>
        </div>
        <div class="labelDiv">
            Images:</div>
        <div class="fieldDiv">
            <asp:FileUpload ID="FileUpload" runat="server" CssClass="roundedCornerTextBoxUpload"
                Style="float: left;" />
            <asp:LinkButton ID="btnUpload" runat="server" CssClass="uploadButton" OnClick="btnUpload_Click" />
            <br class="clearingSpacer" />
            <asp:Label ID="lblUniquePath" runat="Server" Visible="false"></asp:Label><br />
            <asp:UpdatePanel ID="udpImages" runat="server" ChildrenAsTriggers="true">
                <ContentTemplate>
                    <asp:DataList ID="dlImages" runat="server" RepeatColumns="3" ItemStyle-CssClass="listImages">
                        <ItemTemplate>
                            <table cellspacing="5">
                                <%# Container.DataItem %>
                            </table>
                            <div align="center" style="padding-top: 5px;">
                                <asp:RadioButton ID="rdbMainImage" runat="server" GroupName="MainImages" Text="Main Image" onclick="javascript:CheckOnOff(this.id,'dlImages');" /><br />
                            </div>
                        </ItemTemplate>
                    </asp:DataList>
                </ContentTemplate>
            </asp:UpdatePanel>
            <br />
            <b>
                <asp:Label ID="lblUploadError" runat="server"></asp:Label></b>
        </div>
        <br class="clearingSpacer" />
    </div>--%>

    <div class="Line"></div>

    <div class="buttonsRightDiv">
        <asp:LinkButton ID="lnkbtnBack" runat="server" CssClass="backButton" OnClick="lnkbtnBack_Click" />
        <asp:LinkButton ID="lnkbtnSave" runat="server" CssClass="saveButton" OnClick="lnkbtnSave_Click" />
        <asp:LinkButton ID="lnkbtnClear" runat="server" CssClass="clearButton" OnClick="lnkbtnClear_Click" />
        <asp:HiddenField ID="hdniGroupID" Value="0" runat="server" />
    </div>
</asp:Content>
