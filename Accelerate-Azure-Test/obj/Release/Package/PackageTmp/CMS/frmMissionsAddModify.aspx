<%@ Page Title="Missions" Language="C#" MasterPageFile="~/CMS/CMS.master" AutoEventWireup="true" Inherits="CMS_frmMissionsAddModify" Codebehind="frmMissionsAddModify.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript" lang="javascript">
     function CheckOnOff(rdoId,gridName)
     {
         var rdo = document.getElementById(rdoId);
         /* Getting an array of all the INPUT controls on the form.*/
         var rdo = document.getElementById(rdoId);
         var all = document.getElementsByTagName("input");
         for(i=0;i<all.length;i++)
         {
             /*Checking if it is a radio button, and also checking if the id of that radio button is different than rdoId */
             if(all[i].type=="radio" && all[i].id != rdo.id)
             {
                 var count=all[i].id.indexOf(gridName);
                 if(count!=-1)
                 {
                     all[i].checked=false;
                 }
             }
         }
     rdo.checked=true; /* Finally making the clicked radio button CHECKED */
     }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <asp:UpdatePanel ID="updMain" runat="server">
    <ContentTemplate>

    <div id="mandatoryDiv" class="mandatoryInvalidDiv" runat="server" visible="false">
        <asp:Label ID="lblValidationMessage" runat="server"></asp:Label>
    </div>

     <%--<div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">MissionStatus:</div>
         <div class="fieldDiv">
             <asp:DropDownList ID="lstMissionStatus" runat="server" CssClass="roundedCornerDropDownList" onblur="setValid(this, 0);"></asp:DropDownList>
         </div>
         <br class="clearingSpacer" />
     </div>--%>
        
     <div class="controlDiv">
          <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Mission Type:</div>
         <div class="fieldDiv">
             <asp:DropDownList ID="lstMissionType" runat="server" CssClass="roundedCornerDropDownList" ></asp:DropDownList>
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">BonusPoints:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtBonusPoints" runat="server" CssClass="roundedCornerTextBoxMini" onblur="jsIsNumeric(this)"/>
         </div>
         <br class="clearingSpacer" />
     </div>
     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Title:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtTitle" runat="server" CssClass="roundedCornerTextBoxMultiLine4" onKeyUp="return SetMaxLength(this,300)" TextMode="MultiLine" Rows="4" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

        <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Link Title:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtLinkTitle" runat="server" CssClass="roundedCornerTextBoxMultiLine4" onKeyUp="return SetMaxLength(this,300)" TextMode="MultiLine" Rows="4" />
         </div>
         <br class="clearingSpacer" />
     </div>

        <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Link:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtLink" runat="server" CssClass="roundedCornerTextBoxMultiLine4" onKeyUp="return SetMaxLength(this,1500)" TextMode="MultiLine" Rows="4" />
         </div>
         <br class="clearingSpacer" />
     </div>

        <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Tag:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtTag" runat="server" CssClass="roundedCornerTextBoxMultiLine5" onKeyUp="return SetMaxLength(this,300)" TextMode="MultiLine" Rows="4" />
         </div>
         <br class="clearingSpacer" />
     </div>

        <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Description:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtDescription" runat="server" CssClass="roundedCornerTextBoxMultiLine8" onKeyUp="return SetMaxLength(this,5000)" TextMode="MultiLine" Rows="8" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Instruction:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtTagLine" runat="server" CssClass="roundedCornerTextBoxMultiLine4" onKeyUp="return SetMaxLength(this,300)" TextMode="MultiLine" Rows="4" />
         </div>
         <br class="clearingSpacer" />
     </div>

        

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
 <div class="labelDiv">Images:<br />Minimum 150 x 150</div>
         <div class="fieldDiv">
             <asp:FileUpload ID="FileUpload" runat="server" CssClass="roundedCornerTextBoxUpload" style="float: left;" Width="100%"  onblur="setValid(this);" onchange="this.form.submit()"/>
            <%-- <div style="float:left;"><asp:LinkButton ID="btnUpload" runat="server" CssClass="uploadButton" onclick="btnUpload_Click" style="margin-left:5px;" /></div>--%>
             <asp:Label ID="lblUniquePath" runat="Server" Visible="false"></asp:Label><br style="clear:both"/>
             <asp:UpdatePanel ID="udpImages" runat="server" ChildrenAsTriggers="true">
                 <ContentTemplate>
                     <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                        <ProgressTemplate>
                            <asp:Image ID="imgLoader" runat="server" ImageUrl="images/imgLoader.GIF" />Loading. Please wait.
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                     <asp:DataList ID="dlImages" runat="server" RepeatColumns="3" ItemStyle-CssClass="listImages">
                         <ItemTemplate>
                             <table cellspacing="5">
                                 <%# Container.DataItem %>
                             </table>
                             <div align="center" style="padding-top: 5px; display: none;">
                                 <asp:RadioButton ID="rdbMainImage" runat="server" Checked="true" GroupName="MainImages" Text="Main Image" onclick="javascript:CheckOnOff(this.id,'dlImages');" /><br />
                             </div>
                         </ItemTemplate>
                     </asp:DataList>
                 </ContentTemplate>
             </asp:UpdatePanel><br />
             <b><asp:Label ID="lblUploadError" runat="server"></asp:Label></b>
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Achievement:</div>
         <div class="fieldDiv">
             <asp:DropDownList ID="lstAchievement" runat="server" CssClass="roundedCornerDropDownList" ></asp:DropDownList>
         </div>
         <br class="clearingSpacer" />
     </div>

        <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Send Email Q and A:</div>
         <div class="fieldDiv">
             <asp:DropDownList ID="lstAdminEmailSettings" runat="server" CssClass="roundedCornerDropDownList" OnSelectedIndexChanged="lstAdminEmailSettings_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
         </div>
         <br class="clearingSpacer" />
     </div>

        <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Main Email: <br /> (Email 1)</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtEmail1" runat="server" CssClass="roundedCornerTextBox" ReadOnly="true" onKeyUp="return SetMaxLength(this,100)" />
         </div>
         <br class="clearingSpacer" />
     </div>

        <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">CC Email: <br /> (Email 2)</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtEmail2" runat="server" CssClass="roundedCornerTextBox" ReadOnly="true" onKeyUp="return SetMaxLength(this,100)" />
         </div>
         <br class="clearingSpacer" />
     </div>

        <%--<asp:RegularExpressionValidator
        id="regEmail"
        ControlToValidate="txtSendEmailTo"
        Text="(Invalid email)"
        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
        Runat="server" />--%>

       <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Is Video Mission?</div>
         <div style="float: left;" class="fieldDiv">
             <asp:CheckBox ID="chkIsVideoMission" runat="server" AutoPostBack="true" EnableViewState="true" OnCheckedChanged="chkIsVideoMission_CheckedChanged"/>
         </div>
         <br class="clearingSpacer" />
     </div>

        <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv" runat="server" id="lblVideo" visible="false">Video Link:</div>
         <div class="fieldDiv">
             <asp:DropDownList ID="ddlVideoMission" runat="server" CssClass="roundedCornerDropDownList" OnSelectedIndexChanged="ddlVideoMission_SelectedIndexChanged" Visible="false"></asp:DropDownList>
         </div>
         <br class="clearingSpacer" />
     </div>
    
    <div class="Line"></div>
    <div class="buttonsRightDiv">
        <asp:LinkButton ID="lnkbtnBack" runat="server" CssClass="backButton" onclick="lnkbtnBack_Click" OnClientClick='history.go(-1);return false;' />
        <asp:LinkButton ID="lnkbtnSave" runat="server" CssClass="saveButton" onclick="lnkbtnSave_Click" />
        <asp:LinkButton ID="lnkbtnClear" runat="server" CssClass="clearButton" onclick="lnkbtnClear_Click" />
    </div>

         </ContentTemplate>
<%--         <Triggers>
             <asp:PostBackTrigger ControlID="btnUpload" />
         </Triggers>--%>
     </asp:UpdatePanel>
</asp:Content>
