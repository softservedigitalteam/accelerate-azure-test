<%@ Page Title="Add Features To Group" Language="C#" MasterPageFile="~/CMS/CMS.master" AutoEventWireup="true" Inherits="CMS_clsFeaturesView" Codebehind="frmGroupFeaturesView.aspx.cs" %>

<%@ Register TagPrefix="ajax" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script lang="javascript" type="text/javascript">
        //### Password check
        function clearText() {
            document.getElementById("<%=txtSearch.ClientID%>").value = ""
    }
    </script>
    <script type="text/javascript">
        function RefreshUpdatePanel(sender) {
            __doPostBack(sender, '');
        }
    </script>

    <!-- Event Manager for selection of item from auto-complete list -->
    <script lang="javascript" type="text/javascript">
        function ClientItemSelected(sender, e) {
            $get("<%=hfFeatureID.ClientID %>").value = e.get_value();
        $get('<%=lnkbtnSearch.ClientID %>').click();
    }

    function toggleDiv(divID) {
        $(divID).slideToggle();
    }
    </script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="currentPageTab" id="divPageName" style="width:200px; height:45px; margin-top:5px;" >
                                <asp:Label ID="lblGroupName" runat="server" ForeColor="#5a5a5a" Font-Bold="true"></asp:Label>
                            </div>
    <asp:Panel ID="pSearch" runat="server" DefaultButton="lnkbtnSearch">
        <div class="controlDiv" style="margin-top: 15px;">
            <div class="fieldSearchDiv">
                <asp:TextBox ID="txtSearch" runat="server" CssClass="roundedCornerTextBox" Text="SEARCH FOR A FEATURE TO ADD" onfocus="clearText();" />
                <ajax:AutoCompleteExtender ID="aceFeature" runat="server" TargetControlID="txtSearch" ServiceMethod="FilterBusinessRule"
                    EnableCaching="true" MinimumPrefixLength="1" CompletionInterval="0" CompletionSetCount="3"
                    CompletionListCssClass="popupCompletionList" CompletionListItemCssClass="popupCompletionListItem" CompletionListHighlightedItemCssClass="popupCompletionListItemHighlight"
                    OnClientItemSelected="ClientItemSelected" FirstRowSelected="true" />
                <asp:HiddenField ID="hfFeatureID" runat="server" Value="" />
            </div>
            <div class="buttonsSearchDiv">
                <asp:LinkButton ID="lnkbtnSearch" runat="server" CssClass="searchButton" OnClick="lnkbtnSearch_Click" /></div>
        </div>
        <br class="clearingSpacer" />
    </asp:Panel>

    <br />

    <asp:UpdatePanel ID="udpFeatureView" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="mandatoryDiv" class="mandatoryInvalidDiv" runat="server" visible="false">
                <asp:Label ID="lblValidationMessage" runat="server"></asp:Label>
            </div>
            <asp:Literal ID="litFeatureHeading" runat="server">ADD FEATURES FROM THIS TABLE TO THE CURRENT GROUP</asp:Literal><br />
            <br />
            <asp:DataGrid ID="dgrGrid" runat="server" AutoGenerateColumns="False" AllowSorting="true"
                AllowPaging="true" PageSize="15" PagerStyle-Mode="NumericPages" OnPageIndexChanged="dgrGrid_PageIndexChanged"
                OnSortCommand="dgrGrid_SortCommand" CellPadding="3" CellSpacing="0" PagerStyle-NextPageText="Next" PagerStyle-PrevPageText="Prev"
                HeaderStyle-CssClass="dgrHeader" ItemStyle-CssClass="dgrItem" AlternatingItemStyle-CssClass="dgrAltItem" PagerStyle-CssClass="dgrPg" CssClass="dgr">
                <Columns>
                    <asp:BoundColumn DataField="iPortalFeatureID" Visible="false" />
                    <asp:BoundColumn DataField="strTitle" SortExpression="strTitle" HeaderText="" HeaderStyle-CssClass="dgrHeaderLeft" HeaderStyle-HorizontalAlign="Left" />

                    <asp:TemplateColumn HeaderText="Add" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50px" HeaderStyle-CssClass="dgrHeaderRight">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkAddItem" runat="Server" CssClass="dgrLinkAdd" CommandArgument='<%# Eval("iPortalFeatureID") %>' OnClick="lnkAddItem_Click" />
                        </ItemTemplate>
                    </asp:TemplateColumn>

                </Columns>
            </asp:DataGrid>
            <br />
            <asp:Literal ID="litOrderHeading" runat="server">LIST OF FEATURES LINKED TO THE CURRENT GROUP</asp:Literal><br />
            <br />
            <asp:DataGrid ID="dgOrder" runat="server" AutoGenerateColumns="False" AllowSorting="true"
                AllowPaging="true" PageSize="15" PagerStyle-Mode="NumericPages" OnPageIndexChanged="dgOrder_PageIndexChanged" CellPadding="3" CellSpacing="0" PagerStyle-NextPageText="Next" PagerStyle-PrevPageText="Prev"
                HeaderStyle-CssClass="dgrHeader" ItemStyle-CssClass="dgrItem" AlternatingItemStyle-CssClass="dgrAltItem" PagerStyle-CssClass="dgrPg" CssClass="dgr">
                <Columns>
                    <asp:BoundColumn DataField="iPortalFeatureGroupsID" Visible="false" />
                    <asp:BoundColumn DataField="strTitle" SortExpression="strTitle" HeaderText="Feature Title" HeaderStyle-CssClass="dgrHeaderLeft" HeaderStyle-HorizontalAlign="Left" />

                    <asp:TemplateColumn HeaderText="Delete" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50px" HeaderStyle-CssClass="dgrHeaderRight">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkDeleteItem" runat="server" CssClass="dgrLinkDelete" CommandArgument='<%# Eval("iPortalFeatureGroupsID") %>' OnClick="lnkDeleteItem_Click" OnClientClick="return jsDeleteConfirm()" />
                        </ItemTemplate>
                    </asp:TemplateColumn>

                </Columns>
            </asp:DataGrid>

            <asp:Literal runat="server" ID="litTotal"></asp:Literal>


            <div class="buttonsRightDiv" style="clear: both; display: none;">
                <div class="Line"></div>
                <asp:LinkButton ID="lnkbtnSubmit" runat="server" CssClass="submitButton" OnClick="lnkbtnSubmit_Click" />
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
