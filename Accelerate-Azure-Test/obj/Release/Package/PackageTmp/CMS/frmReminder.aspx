﻿<%@ Page Title="Mission Reminder" Language="C#" MasterPageFile="~/CMS/CMS.master" AutoEventWireup="true" Inherits="CMS_Reminder" Codebehind="frmReminder.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="mandatoryDiv" class="mandatoryInvalidDiv" runat="server" visible="false">
        <asp:Label ID="lblValidationMessage" Text="" runat="server"></asp:Label>
    </div>

    <div class="controlDiv">
        
        
        <div class="fieldDiv">
        </div>
        <br class="clearingSpacer" />
    </div>
    
    <div runat="server" id="divUser" visible="false" class="controlDiv">
        <br class="clearingSpacer" />
    </div>
    
    <div class="Line"></div>
    <div class="buttonsRightDiv">
        <asp:Button ID="btnRemindUser" runat="server" Text="Run Mission Reminder" OnClick="btnRemindUser_Click"/>
    </div>
</asp:Content>

