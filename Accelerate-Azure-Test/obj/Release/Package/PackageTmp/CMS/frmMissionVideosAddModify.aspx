<%@ Page Title="Mission Videos" Language="C#" MasterPageFile="~/CMS/CMS.master" AutoEventWireup="true" Inherits="CMS_frmMissionVideosAddModify" Codebehind="frmMissionVideosAddModify.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<script type="text/javascript" lang="javascript">
     function CheckOnOff(rdoId,gridName)
     {
         var rdo = document.getElementById(rdoId);
         /* Getting an array of all the INPUT controls on the form.*/
         var rdo = document.getElementById(rdoId);
         var all = document.getElementsByTagName("input");
         for(i=0;i<all.length;i++)
         {
             /*Checking if it is a radio button, and also checking if the id of that radio button is different than rdoId */
             if(all[i].type=="radio" && all[i].id != rdo.id)
             {
                 var count=all[i].id.indexOf(gridName);
                 if(count!=-1)
                 {
                     all[i].checked=false;
                 }
             }
         }
     rdo.checked=true; /* Finally making the clicked radio button CHECKED */
     }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <asp:UpdatePanel ID="updMain" runat="server">
    <ContentTemplate>

    <div id="mandatoryDiv" class="mandatoryInvalidDiv" runat="server" visible="false">
        <asp:Label ID="lblValidationMessage" runat="server"></asp:Label>
    </div>

        <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Title:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtTitle" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,7)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Color:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtColor" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,7)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Video Link:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtVideoLink" runat="server" CssClass="roundedCornerTextBoxMultiLine8" onKeyUp="return SetMaxLength(this,1500)" TextMode="MultiLine" Rows="8" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">
             Video File:<br />
             (YouTube videos need to be the embeded link)<br />
             (Vimeo Videos normal link)
         </div>
         <div class="fieldDiv">
             <asp:FileUpload ID="FileUpload" runat="server" CssClass="roundedCornerTextBoxUpload" style="float: left;" Width="100%"  onblur="setValid(this);" onchange="this.form.submit()"/>
            <%-- <div style="float:left;"><asp:LinkButton ID="btnUpload" runat="server" CssClass="uploadButton" onclick="btnUpload_Click" style="margin-left:5px;" /></div>--%>
             <asp:Label ID="lblUniquePath" runat="Server" Visible="false"></asp:Label><br style="clear:both"/>
             <asp:UpdatePanel ID="udpVideo" runat="server" ChildrenAsTriggers="true" UpdateMode="Always">
                 <ContentTemplate>
                     <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                        <ProgressTemplate>
                            <asp:Image ID="imgLoader" runat="server" ImageUrl="images/imgLoader.gif" />Loading. Please wait.
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                     <asp:DataList ID="dlVideo" runat="server" ItemStyle-CssClass="listImages">
                         <ItemTemplate>
                             <table cellspacing="5">
                                 <%# Container.DataItem %>
                             </table>
                         </ItemTemplate>
                     </asp:DataList>
                 </ContentTemplate>
             </asp:UpdatePanel><br />
             <b><asp:Label ID="lblUploadError" runat="server"></asp:Label></b>
         </div>
         <br class="clearingSpacer" />
     </div>

         <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
 <div class="labelDiv">Images:<br />Minimum 150 x 150</div>
         <div class="fieldDiv">
             <asp:FileUpload ID="FileUpload1" runat="server" CssClass="roundedCornerTextBoxUpload" style="float: left;" Width="100%"  onblur="setValid(this);"  onchange="this.form.submit()"/>
            <%-- <div style="float:left;"><asp:LinkButton ID="btnImageUpload" runat="server" CssClass="uploadButton" onclick="btnImageUpload_Click" style="margin-left:5px;" /></div>--%>
             <asp:Label ID="lblUniqueImagePath" runat="Server" Visible="false"></asp:Label><br style="clear:both"/>
             <asp:UpdatePanel ID="udpImages" runat="server" ChildrenAsTriggers="true">
                 <ContentTemplate>
                     <asp:UpdateProgress ID="UpdateProgress2" runat="server">
                        <ProgressTemplate>
                            <asp:Image ID="imgLoader" runat="server" ImageUrl="images/imgLoader.gif" />Loading. Please wait.
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                     <asp:DataList ID="dlImages" runat="server" RepeatColumns="3" ItemStyle-CssClass="listImages">
                         <ItemTemplate>
                             <table cellspacing="5">
                                 <%# Container.DataItem %>
                             </table>
                             <div align="center" style="padding-top: 5px;">
                             </div>
                         </ItemTemplate>
                     </asp:DataList>
                 </ContentTemplate>
             </asp:UpdatePanel><br />
             <b><asp:Label ID="lblUploadError2" runat="server"></asp:Label></b>
         </div>
         <br class="clearingSpacer" />
     </div>
    
    <div class="Line"></div>
    <div class="buttonsRightDiv">
        <asp:LinkButton ID="lnkbtnBack" runat="server" CssClass="backButton" onclick="lnkbtnBack_Click" OnClientClick='history.go(-1);return false;' />
        <asp:LinkButton ID="lnkbtnSave" runat="server" CssClass="saveButton" onclick="lnkbtnSave_Click" />
        <asp:LinkButton ID="lnkbtnClear" runat="server" CssClass="clearButton" onclick="lnkbtnClear_Click" />
    </div>

         </ContentTemplate>
<%--         <Triggers>
             <asp:PostBackTrigger ControlID="btnUpload" />
         </Triggers>--%>
        <%--<Triggers>
             <asp:PostBackTrigger ControlID="btnImageUpload" />
         </Triggers>--%>
     </asp:UpdatePanel>
</asp:Content>
