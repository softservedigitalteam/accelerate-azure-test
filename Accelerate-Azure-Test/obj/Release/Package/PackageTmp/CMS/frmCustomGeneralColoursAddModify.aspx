<%@ Page Title="CustomGeneralColours" Language="C#" MasterPageFile="~/CMS/CMS.master" AutoEventWireup="true" Inherits="CMS_frmCustomGeneralColoursAddModify" Codebehind="frmCustomGeneralColoursAddModify.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div id="mandatoryDiv" class="mandatoryInvalidDiv" runat="server" visible="false">
        <asp:Label ID="lblValidationMessage" runat="server"></asp:Label>
    </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">CustomGeneralColour:</div>
         <%--<div class="fieldDiv">
             <asp:DropDownList ID="lstCustomGeneralColour" runat="server" CssClass="roundedCornerDropDownList" onblur="setValid(this, 0);"></asp:DropDownList>
         </div>--%>
         <br class="clearingSpacer" />
     </div>
     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Title:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtTitle" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

     <%--<div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Link:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtLink" runat="server" CssClass="roundedCornerTextBox" onKeyUp="return SetMaxLength(this,100)" />
         </div>
         <br class="clearingSpacer" />
     </div>--%>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Profile Background Colour:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtProfileBackgroundColour" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" />
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Profile Colour Text:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtProfileColourText" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" />
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Rank Background Colour:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtRankBackgroundColour" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" />
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Rank Text Colour:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtRankTextColour" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" />
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Progress Bar Section Background Colour:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtProgressBarSectionBackgroundColour" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" />
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Progress Bar Filler Colour:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtProgressBarFillerColour" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" />
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Information Bar Top Background Colour:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtInformationBarTopBackgroundColour" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" />
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Information Bar Top Text Colour:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtInformationBarTopTextColour" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" />
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Information Bar Background Colour:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtInformationBarBackgroundColour" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" />
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Information Bar Text Colour:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtInformationBarTextColour" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" />
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Tips Background Colour:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtTipsBackgroundColour" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" />
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Tips Text Colour:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtTipsTextColour" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" />
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Introduction Background Colour:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtIntroductionBackgroundColour" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" />
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Introduction Text Colour:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtIntroductionTextColour" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" />
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Mission Info Background Colour:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtMissionInfoBackgroundColour" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" />
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Mission Info Title Text Colour:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtMissionInfoTitleTextColour" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" />
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Mission Info Text Colour:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtMissionInfoTextColour" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" />
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Mission Info Points Colour:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtMissionInfoPointsColour" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" />
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">General Info Box Background Colour:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtGeneralInfoBoxBackgroundColour" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" />
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">General Info Box Text Colour:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtGeneralInfoBoxTextColour" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" />
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">General Info Box Header Background Colour:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtGeneralInfoBoxHeaderBackgroundColour" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" />
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">General Info Box Header Text Colour:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtGeneralInfoBoxHeaderTextColour" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" />
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Knowledge Base Block Colour1:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtKnowledgeBaseBlockColour1" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" />
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Knowledge Base Block Colour2:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtKnowledgeBaseBlockColour2" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" />
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Knowledge Base Block Colour3:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtKnowledgeBaseBlockColour3" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" />
         </div>
         <br class="clearingSpacer" />
     </div>

    <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Knowledge Base Block Text Colour1:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtKnowledgeBaseBlockTextColour1" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" />
         </div>
         <br class="clearingSpacer" />
     </div>

    <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Knowledge Base Block Text Colour2:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtKnowledgeBaseBlockTextColour2" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" />
         </div>
         <br class="clearingSpacer" />
     </div>

    <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Knowledge Base Block Text Colour3:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtKnowledgeBaseBlockTextColour3" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" />
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Knowledge Base Block Link Colour:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtKnowledgeBaseBlockLinkColour" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" />
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Mandatorye Learning Link Colour:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtMandatoryeLearningLinkColour" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" />
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Home Points Background Colour:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtHomePointsBackgroundColour" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" />
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Home Points Text Colour:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtHomePointsTextColour" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" />
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Showcase Background Colour:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtShowcaseBackgroundColour" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" />
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Showcase Text Colour:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtShowcaseTextColour" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" />
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Phase1 Background Colour:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtPhase1BackgroundColour" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" />
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Phase1 Text Colour:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtPhase1TextColour" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" />
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Phase2 Background Colour:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtPhase2BackgroundColour" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" />
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Phase2 Text Colour:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtPhase2TextColour" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" />
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Phase3 Background Colour:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtPhase3BackgroundColour" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" />
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Phase3 Text Colour:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtPhase3TextColour" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" />
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Content Box Background Colour:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtContentBoxBackgroundColour" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" />
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Content Box Text Colour:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtContentBoxTextColour" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" />
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Top Achievers Text Colour:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtTopAchieversTextColour" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" />
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Content Box Header Background Colour:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtContentBoxHeaderBackgroundColour" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" />
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Content Box Header Text Colour:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtContentBoxHeaderTextColour" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" />
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Missions Complete Text Colour:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtMissionsCompleteTextColour" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" />
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Button Background Colour:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtButtonBackgroundColour" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" />
         </div>
         <br class="clearingSpacer" />
     </div>

    <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Button Text Colour:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtButtonTextColour" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" />
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Navigation Bar Background Colour:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtNavigationBarBackgroundColour" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" />
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Navigation Bar Buttons Colour:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtNavigationBarButtonsColour" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" />
         </div>
         <br class="clearingSpacer" />
     </div>

    <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Profile Textbox Background Colour:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtProfileTextBoxBackgroundColour" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" />
         </div>
         <br class="clearingSpacer" />
     </div>

    <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Profile Textbox Text Colour:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtProfileTextBoxTextColour" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" />
         </div>
         <br class="clearingSpacer" />
     </div>

    <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Mission Textbox Text Colour:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtMissionTextBoxBackgroundColour" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" />
         </div>
         <br class="clearingSpacer" />
     </div>

    <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
         <div class="labelDiv">Mission Textbox Text Colour:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtMissionTextBoxTextColour" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,15)" />
         </div>
         <br class="clearingSpacer" />
     </div>
    
    <div class="Line"></div>
    <div class="buttonsRightDiv">
        <asp:LinkButton ID="lnkbtnBack" runat="server" CssClass="backButton" onclick="lnkbtnBack_Click" OnClientClick='history.go(-1);return false;' />
        <asp:LinkButton ID="lnkbtnSave" runat="server" CssClass="saveButton" onclick="lnkbtnSave_Click" />
        <asp:LinkButton ID="lnkbtnClear" runat="server" CssClass="clearButton" onclick="lnkbtnClear_Click" />
    </div>
</asp:Content>
