<%@ Page Title="Account Users" Language="C#" MasterPageFile="~/CMS/CMS.master" AutoEventWireup="true" Inherits="CMS_frmAccountUsersAddModify" Codebehind="frmAccountUsersAddModify.aspx.cs" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript" lang="javascript">
     function CheckOnOff(rdoId,gridName)
     {
         var rdo = document.getElementById(rdoId);
         /* Getting an array of all the INPUT controls on the form.*/
         var rdo = document.getElementById(rdoId);
         var all = document.getElementsByTagName("input");
         for(i=0;i<all.length;i++)
         {
             /*Checking if it is a radio button, and also checking if the id of that radio button is different than rdoId */
             if(all[i].type=="radio" && all[i].id != rdo.id)
             {
                 var count=all[i].id.indexOf(gridName);
                 if(count!=-1)
                 {
                     all[i].checked=false;
                 }
             }
         }
     rdo.checked=true; /* Finally making the clicked radio button CHECKED */
     }

</script>

    <link type="text/css" rel="Stylesheet" href="scripts/jquery.Jcrop.css" />
    <script type="text/javascript" src="scripts/jquery.Jcrop.min.js"></script>
    <script type="text/javascript" src="scripts/jquery.Jcrop.js"></script>
    <script language="javascript" type="text/javascript">
        function jCrop() {
            jQuery('#<%=imgCrop.ClientID%>').Jcrop({
                onSelect: storeCoords,
                aspectRatio: 1 / 1,
                onChange: storeCoords,
                onSelect: storeCoords,
                allowMove: true,
                minSize: [300, 300],                
            });
        }

        jQuery(window).load(jCrop);

        function storeCoords(c) {
            jQuery('#<%=X.ClientID%>').val(c.x);
            jQuery('#<%=Y.ClientID%>').val(c.y);
            jQuery('#<%=W.ClientID%>').val(c.w);
            jQuery('#<%=H.ClientID%>').val(c.h);
        };
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:UpdatePanel ID="updMain" runat="server">
    <ContentTemplate>

    <div id="mandatoryDiv" class="mandatoryInvalidDiv" runat="server" visible="false">
        <asp:Label ID="lblValidationMessage" runat="server"></asp:Label>
    </div>


    
     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">First Name:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtFirstName" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,50)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Surname:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtSurname" runat="server" CssClass="roundedCornerTextBoxMini" onKeyUp="return SetMaxLength(this,50)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Email Address:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtEmailAddress" runat="server" CssClass="roundedCornerTextBox" onKeyUp="return SetMaxLength(this,120);" onblur="setValid(this);" Style="text-transform: lowercase;"/>
         </div>
         <br class="clearingSpacer" />
     </div>

        <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Business Unit:</div>
         <div class="fieldDiv">
             <asp:DropDownList ID="lstGroup" runat="server" CssClass="roundedCornerDropDownList" onblur="setValid(this, 0);"></asp:DropDownList>
         </div>
         <br class="clearingSpacer" />
     </div>

    <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Phone Number:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtPhoneNumber" runat="server" CssClass="roundedCornerTextBox" onKeyUp="return SetMaxLength(this,15)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

        <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
         <div class="labelDiv">Business Unit:</div>
         <div class="fieldDiv">
             <asp:TextBox ID="txtJobTitle" runat="server" CssClass="roundedCornerTextBox" onKeyUp="return SetMaxLength(this,15)" onblur="setValid(this);"/>
         </div>
         <br class="clearingSpacer" />
     </div>

     <div class="controlDiv">
         <div class="imageHolderCommonDiv"><div class="dummyHolder"></div></div>
          <div class="labelDiv">Images:<br />Minimum 104 x 104</div>
         <div class="fieldDiv">
             <asp:FileUpload ID="FileUpload" runat="server" CssClass="roundedCornerTextBoxUpload" style="float: left;" Width="100%"  onblur="setValid(this);" onchange="this.form.submit()"/>
             <%--<div style="float:left;"><asp:LinkButton ID="btnUpload" runat="server" CssClass="uploadButton" onclick="btnUpload_Click" style="margin-left:5px;" /></div>--%>
             <asp:Label ID="lblUniquePath" runat="Server" Visible="false"></asp:Label><br style="clear:both"/>
             <asp:UpdatePanel ID="udpImages" runat="server" ChildrenAsTriggers="true">
                 <ContentTemplate>
                     <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                        <ProgressTemplate>
                            <asp:Image ID="imgLoader" runat="server" ImageUrl="images/imgLoader.gif" />Loading. Please wait.
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                     <asp:DataList ID="dlImages" runat="server" RepeatColumns="3" ItemStyle-CssClass="listImages">
                         <ItemTemplate>
                             <table cellspacing="5">
                                 <%# Container.DataItem %>
                             </table>
                             <div align="center" style="padding-top: 5px; display:none;">
                                 <asp:RadioButton ID="rdbMainImage" runat="server" Checked="true" GroupName="MainImages" Text="Main Image" onclick="javascript:CheckOnOff(this.id,'dlImages');" /><br />
                             </div>
                         </ItemTemplate>
                     </asp:DataList>
                 </ContentTemplate>
             </asp:UpdatePanel><br />
             <b><asp:Label ID="lblUploadError" runat="server"></asp:Label></b>
         </div>
         <br class="clearingSpacer" />
     </div>

    <div class="Line"></div>
    <div class="buttonsRightDiv">
        <asp:LinkButton ID="lnkbtnBack" runat="server" CssClass="backButton" onclick="lnkbtnBack_Click" OnClientClick='history.go(-1);return false;' />
        <asp:LinkButton ID="lnkbtnSave" runat="server" CssClass="saveButton" onclick="lnkbtnSave_Click" />
        <asp:LinkButton ID="lnkbtnClear" runat="server" CssClass="clearButton" onclick="lnkbtnClear_Click" />
    </div>

         </ContentTemplate>
<%--         <Triggers>
             <asp:PostBackTrigger ControlID="btnUpload" />
         </Triggers>--%>
     </asp:UpdatePanel>

     <asp:Panel ID="pnlCrop" runat="server" CssClass="PopUp">
    <div>
        Please crop the area you would like as a thumbnail.</div>
    <div style="margin-top: 10px;" align="center">
        <asp:Image ID="imgCrop" runat="server" />
    </div>
    <br />
    <asp:HiddenField ID="X" runat="server" />
    <asp:HiddenField ID="Y" runat="server" />
    <asp:HiddenField ID="W" runat="server" />
    <asp:HiddenField ID="H" runat="server" />
    <div>
        <img src="images/imgDivider.png" alt="" title="" />
    </div>
    <div class="buttonsRightDiv" style="padding-right:10px">
        <asp:LinkButton ID="btnCrop" runat="server" CssClass="cropButton" AlternateText="Crop" OnClick="btnCrop_Click" Style="margin-right: 5px;" />
        <asp:LinkButton ID="btnCancel" runat="server" CssClass="backButton" AlternateText="Back" />
    </div>
</asp:Panel>
<asp:Button ID="btnPopupCrop" runat="server" Style="display: none" />
<cc1:modalpopupextender ID="ModalPopupExtenderCrop" runat="server" TargetControlID="btnPopupCrop"
    PopupControlID="pnlCrop" BackgroundCssClass="modalBackground" CancelControlID="btnCancel"
    RepositionMode="RepositionOnWindowResize" Y="0" >        
</cc1:modalpopupextender>

</asp:Content>
