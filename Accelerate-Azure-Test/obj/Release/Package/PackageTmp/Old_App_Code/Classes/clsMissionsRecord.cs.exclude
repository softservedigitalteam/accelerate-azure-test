using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsMissions
/// </summary>
public class clsMissions
{
    #region MEMBER VARIABLES

        private int m_iMissionID;
        private DateTime m_dtAdded;
        private int m_iAddedBy;
        private DateTime m_dtEdited;
        private int m_iEditedBy;
        private int m_iMissionStatusID;
        private int m_iBonusPoints;
        private String m_strTitle;
        private String m_strTagLine;
        private String m_strPathToImages;
        private String m_strMasterImage;
        private String m_strPathToBadges;
        private String m_strMasterBadgeImage;
        private int m_iAchievementID;
        private String m_strDescription;
        private bool m_bIsDeleted;
        
    #endregion 

    #region PROPERTIES

        public int iMissionID
        {
            get
            {
                return m_iMissionID;
            }
        }

        public DateTime dtAdded
        {
            get
            {
                return m_dtAdded;
            }
            set
            {
                m_dtAdded = value;
            }
        }

        public int iAddedBy
        {
            get
            {
                return m_iAddedBy;
            }
            set
            {
                m_iAddedBy = value;
            }
        }

        public DateTime dtEdited
        {
            get
            {
                return m_dtEdited;
            }
            set
            {
                m_dtEdited = value;
            }
        }

        public int iEditedBy
        {
            get
            {
                return m_iEditedBy;
            }
            set
            {
                m_iEditedBy = value;
            }
        }

        public int iMissionStatusID
        {
            get
            {
                return m_iMissionStatusID;
            }
            set
            {
                m_iMissionStatusID = value;
            }
        }

        public int iBonusPoints
        {
            get
            {
                return m_iBonusPoints;
            }
            set
            {
                m_iBonusPoints = value;
            }
        }

        public String strTitle
        {
            get
            {
                return m_strTitle;
            }
            set
            {
                m_strTitle = value;
            }
        }

        public String strTagLine
        {
            get
            {
                return m_strTagLine;
            }
            set
            {
                m_strTagLine = value;
            }
        }

        public String strPathToImages
        {
            get
            {
                return m_strPathToImages;
            }
            set
            {
                m_strPathToImages = value;
            }
        }

        public String strMasterImage
        {
            get
            {
                return m_strMasterImage;
            }
            set
            {
                m_strMasterImage = value;
            }
        }

        public int iAchievementID
        {
            get
            {
                return m_iAchievementID;
            }
            set
            {
                m_iAchievementID = value;
            }
        }

        public String strDescription
        {
            get
            {
                return m_strDescription;
            }
            set
            {
                m_strDescription = value;
            }
        }

        public bool bIsDeleted
        {
            get
            {
                return m_bIsDeleted;
            }
            set
            {
                m_bIsDeleted = value;
            }
        }

    
    #endregion
    
    #region CONSTRUCTORS

    public clsMissions()
    {
        m_iMissionID = 0;
    }

    public clsMissions(int iMissionID)
    {
        m_iMissionID = iMissionID;
        GetData();
    }

    #endregion

    #region PUBLIC METHODS

        public virtual void Update()
        {
            try
            {
                if (iMissionID == 0)
                {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@dtAdded", m_dtAdded),
                        new SqlParameter("@iAddedBy", m_iAddedBy),
                        new SqlParameter("@iMissionStatusID", m_iMissionStatusID),
                        new SqlParameter("@iBonusPoints", m_iBonusPoints),
                        new SqlParameter("@strTitle", m_strTitle),
                        new SqlParameter("@strTagLine", m_strTagLine),
                        new SqlParameter("@strPathToImages", m_strPathToImages),
                        new SqlParameter("@strMasterImage", m_strMasterImage),
                        new SqlParameter("@iAchievementID", m_iAchievementID),
                        new SqlParameter("@strDescription", m_strDescription)                  
                  };

                  //### Add
                  m_iMissionID = (int)clsDataAccess.ExecuteScalar("spMissionsInsert", sqlParametersInsert);                    
                    }
                    else
                    {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iMissionID", m_iMissionID),
                         new SqlParameter("@dtEdited", m_dtEdited),
                         new SqlParameter("@iEditedBy", m_iEditedBy),
                         new SqlParameter("@iMissionStatusID", m_iMissionStatusID),
                         new SqlParameter("@iBonusPoints", m_iBonusPoints),
                         new SqlParameter("@strTitle", m_strTitle),
                         new SqlParameter("@strTagLine", m_strTagLine),
                         new SqlParameter("@strPathToImages", m_strPathToImages),
                         new SqlParameter("@strMasterImage", m_strMasterImage),
                         new SqlParameter("@iAchievementID", m_iAchievementID),
                         new SqlParameter("@strDescription", m_strDescription)
                    };
                    //### Update
                    clsDataAccess.Execute("spMissionsUpdate", sqlParametersUpdate);
                    }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Delete(int iMissionID)
        {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iMissionID", iMissionID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spMissionsDelete", sqlParameter);
        }

        public static DataTable GetMissionsList()
        {
            SqlParameter[] EmptySqlParameter = new SqlParameter[]{};
            return clsDataAccess.GetDataTable("spMissionsList", EmptySqlParameter);
        }
        public static DataTable GetMissionsList(string strFilterExpression, string strSortExpression)
        {
            DataView dvMissionsList = new DataView();

            SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
            dvMissionsList = clsDataAccess.GetDataView("spMissionsList", EmptySqlParameter);
            dvMissionsList.RowFilter = strFilterExpression;
            dvMissionsList.Sort = strSortExpression;

            return dvMissionsList.ToTable();
        }
    #endregion

    #region PROTECTED METHODS

        protected virtual void GetData()
        {
            try
                {
                    //### Populate
                    SqlParameter[] sqlParameter = new SqlParameter[] 
                        {
                            new SqlParameter("@iMissionID", m_iMissionID)
                        };
                DataRow drRecord = clsDataAccess.GetRecord("spMissionsGetRecord", sqlParameter);

                m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);
                m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);

                if (drRecord["dtEdited"] != DBNull.Value)
                   m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

             if (drRecord["iEditedBy"] != DBNull.Value)
               m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);

                m_iMissionStatusID = Convert.ToInt32(drRecord["iMissionStatusID"]);
                m_iBonusPoints = Convert.ToInt32(drRecord["iBonusPoints"]);
                m_strTitle = drRecord["strTitle"].ToString();
                m_strTagLine = drRecord["strTagLine"].ToString();
                m_strPathToImages = drRecord["strPathToImages"].ToString();
                m_strMasterImage = drRecord["strMasterImage"].ToString();
                m_iAchievementID = Convert.ToInt32(drRecord["iAchievementID"]);
                m_strDescription = drRecord["strDescription"].ToString();
                m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);
 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    #endregion
}