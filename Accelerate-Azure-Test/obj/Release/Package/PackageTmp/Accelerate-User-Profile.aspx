﻿<%@ Page Title="Accelerate - My Profile" Language="C#" MasterPageFile="~/Accelerate.master" AutoEventWireup="true" Inherits="Accelerate_User_Profile" Codebehind="Accelerate-User-Profile.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="infobox">
        <div class="top-bar-alternative">
            <h3>User Profile</h3>
        </div>
        <br class="clearfix" />
        <div style="float: left; width: 15%; padding: 0%; color: #333;">Name:</div>
        <div style="float: left; width: 85%; padding: 0%; text-align: left;">
            <p runat="server" style="color: #000000; font-size: 15px; text-transform: capitalize; text-decoration: none" id="txtFirstName" placeholder="Name"></p>
        </div>
        <div style="float: left; width: 15%; padding: 0%; color: #333;">Surname:</div>
        <div style="float: left; width: 85%; padding: 0%; text-align: left;">
            <p runat="server" style="color: #000000; font-size: 15px; text-transform: capitalize; text-decoration: none" id="txtSurname" placeholder="Name"></p>
        </div>

        <div style="float: left; width: 15%; padding: 0%; color: #333;">E-Mail Address:</div>
        <div style="float: left; width: 85%; padding: 0%; text-align: left;">
            <p runat="server" style="color: #000000; font-size: 15px; text-transform: lowercase; text-decoration: none" id="txtEmailAddress" placeholder="Name"></p>
        </div>
        <br class="clearingSpacer" />
    </div>
</asp:Content>
