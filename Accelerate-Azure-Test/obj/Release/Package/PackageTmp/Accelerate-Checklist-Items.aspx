﻿<%@ Page Title="Accelerate - Checklist Items" Language="C#" MasterPageFile="~/Accelerate.master" AutoEventWireup="true" Inherits="Accelerate_Checklist_Items" Codebehind="Accelerate-Checklist-Items.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <style type="text/css">
        .rightMenuContainer
        {
           margin-top: 72px !important;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="genericMainHeading" style="width: 131.1% !important;">
        <h2>Checklist Items</h2>
    </div>
    <div class="sectionContainerLeft innerBoxWithShadow margintopforty">

        <h2 class='genericHeading'>
            <asp:Literal runat="server" ID="litPortalHeading"></asp:Literal></h2>
        <div class='innerMissionText'>
            <div runat="server" id="divTemp" style='position: relative;'>

                <asp:Literal runat="server" ID="litPortalFeatures"></asp:Literal>


                <div class="percentageStartButton">
                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btnPortalFeature" OnClick="btnSave_Click" />
                    <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btnPortalFeatureBack" OnClientClick='history.go(-1);return false;' />
                </div>
            </div>
        </div>
    </div>

    <asp:HiddenField runat="server" ID="lblCheckboxes" />

    <script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>

    <script type="text/javascript">
        function loopForm() {
            var x = document.forms[0];
            var cbResults = 'Checkboxes: ';
            for (var i = 0; i < x.elements.length; i++) {
                if (x.elements[i].type == 'checkbox') {
                    if (x.elements[i].checked == true) {
                        if (cbResults == "Checkboxes: ") {
                            cbResults = x.elements[i].id + ':' + x.elements[i].value;
                        }
                        else {
                            cbResults += "|" + x.elements[i].id + ':' + x.elements[i].value;
                        }
                    }
                }
            }
            document.getElementById('<%= lblCheckboxes.ClientID %>').value = cbResults;
        }
    </script>

    <script type="text/javascript">
        function loopFormCheck() {
            var x = document.forms[0];
            var cbResults = document.getElementById('<%= lblCheckboxes.ClientID %>').value;
            for (var i = 0; i < x.elements.length; i++) {
                if (x.elements[i].type == 'checkbox') {
                    var items = cbResults.split("|");
                    $.each(items, function (index, value) {
                        if (x.elements[i].id + ":on" == value) {
                            x.elements[i].checked = true;
                        }
                    });
                }
            }
        }
    </script>

</asp:Content>

