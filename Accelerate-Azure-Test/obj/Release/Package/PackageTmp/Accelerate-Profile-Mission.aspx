﻿<%@ Page Title="Accelerate - Profile Mission" Language="C#" MasterPageFile="~/Accelerate.master" AutoEventWireup="true" Inherits="Accelerate_Profile_Mission" Codebehind="Accelerate-Profile-Mission.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    
    <%--<link href="css/base.css" rel="stylesheet" />
    <link href="css/skeleton.css" rel="stylesheet" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />--%>
    <%--<script src="js/jquery.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>--%>
    <%--<link href="CMS/css/styles.css" rel="stylesheet" />--%>

    <script type="text/javascript" lang="javascript">
        function CheckOnOff(rdoId, gridName) {
            var rdo = document.getElementById(rdoId);
            /* Getting an array of all the INPUT controls on the form.*/
            var rdo = document.getElementById(rdoId);
            var all = document.getElementsByTagName("input");
            for (i = 0; i < all.length; i++) {
                /*Checking if it is a radio button, and also checking if the id of that radio button is different than rdoId */
                if (all[i].type == "radio" && all[i].id != rdo.id) {
                    var count = all[i].id.indexOf(gridName);
                    if (count != -1) {
                        all[i].checked = false;
                    }
                }
            }
            rdo.checked = true;
            document.getElementById('<%= lblDefaultImages.ClientID %>').value = rdo.id + "|" + rdo.checked;
            /* Finally making the clicked radio button CHECKED */
        }
    </script>

    <script type="text/javascript">
        function showBrowseDialog() {
            document.getElementById('<%=FileUpload.ClientID%>').click();
        }
        function upload() {
            document.getElementById('<%= btnUpload.ClientID %>').click();
        }
    </script>
    <script src="CMS/scripts/jquery.min.js"></script>
    <script src="CMS/scripts/jquery.Jcrop.min.js"></script>
    <link rel="stylesheet" href="CMS/css/jquery.Jcrop.css" type="text/css" />
    <link href="assets/css/slim.min.css" rel="stylesheet"/>

    <%--<script lang="javascript" type="text/javascript">
        function jCrop() {
            jQuery('#<%=imgCrop.ClientID%>').Jcrop({
                onSelect: storeCoords,
                onChange: storeCoords,
                bgColor: 'black',
                bgOpacity: .4,
                aspectRatio: 1 / 1

            });
        }

        jQuery(window).load(jCrop);

        function storeCoords(c) {
            jQuery('#<%=X.ClientID%>').val(c.x);
            jQuery('#<%=Y.ClientID%>').val(c.y);
            jQuery('#<%=W.ClientID%>').val(c.w);
            jQuery('#<%=H.ClientID%>').val(c.h);
        };
    </script>--%>
    <script>

function getImageData(data, ready) {

    <%--var myHiddenX = document.getElementById('<%=X.ClientID%>');
    myHiddenX.value = data.actions.crop.x;
    var myHiddenY = document.getElementById('<%=Y.ClientID%>');
    myHiddenY.value = data.actions.crop.y;
    var myHiddenW = document.getElementById('<%=W.ClientID%>');
    myHiddenW.value = data.output.width;
    var myHiddenH = document.getElementById('<%=H.ClientID%>');
    myHiddenH.value = data.output.height;--%>
    
    var myHiddenX = document.getElementById('<%=X.ClientID%>');
    myHiddenX.value = data.actions.crop.x;
    var myHiddenX2 = document.getElementById('<%=HiddenX.ClientID%>');
    myHiddenX2.value = myHiddenX.value;

    var myHiddenY = document.getElementById('<%=Y.ClientID%>');
    myHiddenY.value = data.actions.crop.y;
    var myHiddenY2 = document.getElementById('<%=HiddenY.ClientID%>');
    myHiddenY2.value = myHiddenY.value;

    var myHiddenW = document.getElementById('<%=W.ClientID%>');
    myHiddenW.value = data.output.width;
    var myHiddenW2 = document.getElementById('<%=HiddenW.ClientID%>');
    myHiddenW2.value = myHiddenW.value;

    var myHiddenH = document.getElementById('<%=H.ClientID%>');
    myHiddenH.value = data.output.height;
    var myHiddenH2 = document.getElementById('<%=HiddenH.ClientID%>');
    myHiddenH2.value = myHiddenH.value;
    
    // continue saving the data
    ready(data);
}
</script>

   <%-- <script lang="javascript" type="text/javascript">
        
        var img = document.getElementById('imgCrop');
        img.onchange = function () {
            W = img.clientWidth;
            H = img.clientHeight;
        }
    </script>--%>

    <!-- SlidesJS Optional: If you'd like to use this design -->
    <style>
        .pnlPopup {
            top: 25%;
            left: 25%;
            height: auto;
            width: auto;
            border: 2px solid #333;
            background-color: #fff;
        }

        .btnSaveProfile {
            float: right !important;
            margin-right: 56px;
        }


        .btnClose {
            position: absolute;
            /*top: -10px;*/
            right: -26px;
            text-decoration: none;
            text-align: center;
            height: 20px;
            width: 20px;
            background: #fff;
            padding: 5px 5px 5px 5px;
            z-index: 1000;
        }

        .btnCloseDefaultImages {
            position: absolute;
            top: 5px;
            right: 5px;
            text-decoration: none;
            text-align: center;
            height: 10px;
            width: 10px;
            /*background: #fff;*/
            padding: 0px 5px 5px 5px;
            z-index: 1000;
        }

        #containerPop {
            margin: 10px 0px 0px 26px;
            width: 100%;
        }



        .btnChooseDefault {
            margin-left: 20.5px;
            float: left;
        }

        .lblProfileLabel {
            display: inline-block;
            width: 50px;
            margin-top: 5px;
            float: left;
            margin-left: 21px;
        }

        .lblProfilePictureLabel {
            display: inline-block;
            width: 50px;
            margin-top: 15px;
            float: left;
        }

        .dlImageStyle {
            margin-top: 69px;
        }

        .pnlLogin {
            display: block !important;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:HiddenField ID="HiddenX" runat="server" />
        <asp:HiddenField ID="HiddenY" runat="server" />
        <asp:HiddenField ID="HiddenW" runat="server" />
        <asp:HiddenField ID="HiddenH" runat="server" />
    
    <div class="infobox">
                <div class="top-bar">
                    <h3>Your current mission - Earn your badge here</h3>
                </div>
                <div class="well">
                    <asp:Literal ID="litCurrentMission" runat="server"></asp:Literal>
                    <div class="well-short" style="background: #80c342; padding-bottom: 12px;">
                        <div class="" style="color: #000; font-size: 12px;">
                            <b>
                                <asp:Literal ID="litTitle" runat="server"></asp:Literal></b>
                        </div>
                        <br />
                        <asp:Literal ID="litBanner" runat="server"></asp:Literal>
                    </div>
                    <br />
                    <div class="well-short" style="background: #FFF; color: #000;">
                        <b style="font-size: 12px;">Your Mission</b>
                        <br />
                        <br />
                        <div>Complete this mission to get your profile badge and start earning points.</div>
                        <br />
                        <asp:Literal ID="litDescription" runat="server"></asp:Literal>
                        <asp:Literal ID="litLink" runat="server"></asp:Literal>
                        <asp:Literal ID="litVideo" runat="server"></asp:Literal>

                    </div>
                    <br />
                    <div class="well-short" style="background: #FFF; color: #000;">
                        <b style="font-size: 12px;">Your Task</b>
                        <br />
                        <br />

                       <div class="controlDiv">
                    <img runat="server" id="imgCompleted" src="../images/imgComplete.png" style="display: none; position: absolute; top: 50px; right: 10px; width: 30px; height: 30px;" />
                    <div class="imageHolderCommonDiv">
                    </div>
                    <br />


                    <div class="fieldDiv" style="width: auto">
                        <br />
                        <center>
                    <input id="btnFileUpload" type="button" runat="server" class="Green3 btn"  style="width: 158px; float: left; height:51px;" onclick="showBrowseDialog()" />
                    <asp:FileUpload ID="FileUpload"  runat="server" CssClass="Green3 btn" Style="float: left;  display:none; margin-left: 25%" Width="350px" onblur="setValid(this);" onchange="upload()" />
                        <asp:Button ID="btnUpload" runat="server" CssClass="Green3 btn" OnClick="btnUpload_Click" Text="Upload" Style="margin-left: 5px; display:none" />
                         <%--<asp:FileUpload ID="FileUpload1" runat="server" CssClass="roundedCornerTextBoxUpload" Style="float: left;  display:none; margin-left: 25%" Width="350px" onblur="setValid(this);" onchange="this.form.submit()" />--%>
                                        <div style="float: left;">
                        <%--<asp:Button ID="btnUpload" runat="server" OnClick="btnUpload_Click" Text="Upload" Style="margin-left: 5px;" /><br />--%>
                    </div>
 
                          
                           
                        <h2 style="font-family: Arial; font-size: 13px; float: left; margin-left: 20px; margin-right: 35px">OR</h2>

                    <asp:Button ID="btnChooseDefault" CssClass="Green3 btn" runat="server" Style="float: left;" Text="Select Default" Width="160px" Height="51px" OnClick="btnChooseDefault_Click" />

                        </center>
                        <asp:Label ID="lblUniquePath" runat="Server" Visible="false"></asp:Label>
                        <br class="clr" />
                        <br />
                        <br />
                        <br />
                        <br />
                        <div>
                        </div>
                        <br />
                        <div style="position: absolute; top: 60px; right: 0px;">
                            <asp:UpdatePanel ID="udpImages" runat="server" ChildrenAsTriggers="true">
                                <ContentTemplate>
                                    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                                        <ProgressTemplate>
                                            <asp:Image ID="imgLoader" runat="server" ImageUrl="images/imgLoader.gif" />Loading. Please wait.
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>
                                    <asp:DataList ID="dlImages" runat="server" CssClass="dlImageStyle" RepeatColumns="3" ItemStyle-CssClass="listImages">
                                        <ItemTemplate>
                                            <table cellspacing="5" style="width: 260px;">
                                                <%# Container.DataItem %>
                                            </table>
                                            <div runat="server" visible="false" align="center" style="padding-top: 5px; display: none;">
                                                <asp:RadioButton ID="rdbMainImage" runat="server" GroupName="MainImages" Checked="true" Text="Main Image" onclick="javascript:CheckOnOff(this.id,'dlImages');" /><br />
                                            </div>
                                        </ItemTemplate>
                                    </asp:DataList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>

                        <%--<asp:Button runat="server" ID="btnSave" CssClass="Green3 btn" Width="80px" Text="Save" Visible="false" OnClick="btnSave_Click" />--%>
                        <%-- <asp:Button runat="server" ID="btnSave" CssClass="alignLeft margintopten btnSaveProfile" Visible="false" Width="80px" Text="Save"  OnClick="btnSave_Click" />--%>
                    </div>


                </div>
                    </div>
                </div>
            </div>
    <div class="infobox">
        <div class="row-fluid">

            <div class="span10">

                <div id="mandatoryDiv" class="mandatoryInvalidDiv" runat="server" visible="false">
                    <asp:Label ID="lblValidationMessage" runat="server"></asp:Label>
                </div>

            </div>
        </div>
    </div>

    <asp:LinkButton runat="server" ID="btnDefaultImages" Style="display: none">POP</asp:LinkButton>

    <div style="position: fixed; left: 0; top: 0; display:none;">

        <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDefaultImages"
            PopupControlID="pnlDefaultImages" BackgroundCssClass="modalBackground" CancelControlID="btnCloseImages"
            RepositionMode="RepositionOnWindowResize">
        </cc1:ModalPopupExtender>

        <asp:Panel ID="pnlDefaultImages" runat="server" HorizontalAlign="Center">
            <div class="sectionContainerLeft innerBoxWithShadow margintopten" style="background-color: #FFF;">
                <div class="top-bar">
                    <h3>Default Images</h3>
                </div>
                <div id="showcaseDiv" class="well">
                    <asp:UpdatePanel ID="updDefaultImages" runat="server" ChildrenAsTriggers="true" EnableViewState="true" UpdateMode="Conditional">
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnCloseImages" />
                        </Triggers>
                        <ContentTemplate>
                            <asp:LinkButton runat="server" ID="btnCloseImages" CssClass='btnCloseDefaultImages' OnClick="btnCloseImages_Click" ForeColor="White" Font-Bold="true">X</asp:LinkButton>

                            <asp:UpdateProgress ID="updImages" runat="server">
                                <ProgressTemplate>
                                    <asp:Image ID="imgDefaultLoader" runat="server" ImageUrl="images/imgLoader.gif" />Loading. Please wait.
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                            <asp:DataList ID="dlDefaultImages" runat="server" RepeatColumns="3" ItemStyle-CssClass="listImages">
                                <ItemTemplate>
                                    <table cellspacing="5">
                                        <%# Container.DataItem %>
                                    </table>
                                    <div align="center" style="padding-top: 5px;">
                                        <asp:RadioButton ID="rdbMainDefaultImage" runat="server" GroupName="MainImages" Text="Select this image?" onclick="javascript:CheckOnOff(this.id,'dlDefaultImages');" /><br />
                                    </div>
                                </ItemTemplate>
                            </asp:DataList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <center>
                <asp:Button runat="server" style="float:left" ID="btnSaveImage" CssClass="Green3 btn"  OnClick="btnSaveImage_Click" Text="Save" Width="80px" />
                </center>
                </div>
            </div>
        </asp:Panel>
    </div>
    <br />

    <asp:HiddenField runat="server" ID="lblDefaultImages" />

    <%--<div style="position:fixed; left:50% !important; top:10% !important;">--%>

        <cc1:ModalPopupExtender ID="ModalPopupExtenderCrop" runat="server" TargetControlID="btnPopupCrop"
        PopupControlID="pnlCrop" BackgroundCssClass="modalBackground" CancelControlID="btnCancel"
        RepositionMode="RepositionOnWindowResize" Y="0" BehaviorID="ModalPopupExtenderCropBID">
    </cc1:ModalPopupExtender>        
 
    <asp:Panel ID="pnlCrop" runat="server">
        <div class="top-bar">
           <h3>Crop</h3> 
        </div>
        
        <div class="well">
            <div style="float:left;">

    <div id="cropDiv" class="slim" 
        data-ratio="1:1"
        <%--data-size="900,900"--%>
        data-will-transform="getImageData">
        
        <%--<input type="file"/>--%>
        <asp:Image ID="imgCrop" runat="server" />

        <asp:HiddenField ID="X" runat="server" />
        <asp:HiddenField ID="Y" runat="server" />
        <asp:HiddenField ID="W" runat="server" />
        <asp:HiddenField ID="H" runat="server" />
    </div>
                
        <div>
        
            
            <%--<img src="CMS/images/imgDivider.png" alt="" title="" />--%>
            <hr style="height:1px;border:none;color:#333;background-color:transparent;width:600px" />
        </div>
        <div class="buttonsRightDiv" style="padding-right: 10px">
            <asp:LinkButton ID="btnCrop" runat="server" CssClass="Green3 btn" Text="Crop" AlternateText="Crop" OnClick="btnCrop_Click" />
            <asp:LinkButton ID="btnCancel" runat="server" CssClass="Green3 btn" Text="Back" AlternateText="Back" />
        </div>
            </div>
            <%--<div style="float:right; text-align:center; width:300px; padding-left:10px;">
                <h5>Please use the mouse cursor to select the area you want to crop. Ensure you click and drag the area you want to crop.</h5>
            </div>--%>

       </div>
        
            <asp:Literal ID="autoClick" runat="server"></asp:Literal>

    </asp:Panel>
        <asp:Button ID="btnPopupCrop" runat="server" Style="display: none" />
  <%--</div>--%>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ScrMang" runat="Server">
     
    <script type="text/javascript">
        

        //function pageLoad() {
        //    var popupCrop = $find('ModalPopupExtenderCropBID');
        //    popupCrop.add_shown(sayHello);

        //    var targetDiv = $('#cropDiv').find('.slim-btn.slim-btn-edit');
        //    //targetDiv.attr('style', 'display:none');
        //   // targetDiv.addClass('dd')
        //    alert(targetDiv.toString());
        //}
        function sayHello() {
            var cropButton = document.getElementsByClassName(".slim-btn.slim-btn-edit");
            for (var i = 0; i < cropButton.length; i++)
            {
                cropButton[i].click();
            }
            //document.querySelector('.slim-btn.slim-btn-edit').click();
            //$('button.slim-btn.slim-btn-edit').trigger('click');
            alert('fssf');
            //var targetDiv = $('#cropDiv');
            //targetDiv.attr('style', 'display:none');
            //$('button.slim-btn.slim-btn-edit').trigger('click');
            //var targetDiv = $('#cropDiv').find('button.slim-btn.slim-btn-edit').click();
            //var targetDiv2 = $('#cropDiv').find('.slim-btn.slim-btn-edit').click();
            //var targetDiv3 = $('#cropDiv').find('.slim-btn.slim-btn-edit').trigger('click');
            //var targetDiv4 = $('#cropDiv').find('.slim-btn.slim-btn-edit').trigger('click');
            ////targetDiv.trigger('click');
            //var elements = document.querySelectorAll(".slim-btn.slim-btn-edit button[type='button']")[0].click();
            ////buttonelement.click();
            //var button = jQuery(".slim-btn.slim-btn-edit").find("button").click();
            //var button2 = jQuery(".slim-btn.slim-btn-edit").find("button").trigger('click');
            
        }
    </script>
    <script src='assets/js/jquery.hotkeys.js'></script>
    <script src='assets/js/calendar/fullcalendar.min.js'></script>
    <script src="assets/js/jquery-ui-1.10.2.custom.min.js"></script>
    <script src="assets/js/jquery.pajinate.js"></script>
    <script src="assets/js/jquery.prism.min.js"></script>
    <script src="assets/js/jquery.dataTables.min.js"></script>
    <script src="assets/js/charts/jquery.flot.time.js"></script>
    <script src="assets/js/charts/jquery.flot.pie.js"></script>
    <script src="assets/js/charts/jquery.flot.resize.js"></script>
    <script src="assets/js/bootstrap/bootstrap-wysiwyg.js"></script>
    <script src="assets/js/bootstrap/bootstrap-typeahead.js"></script>
    <script src="assets/js/jquery.easing.min.js"></script>
    <script src="assets/js/jquery.chosen.min.js"></script>
    <script src="assets/js/avocado-custom.js"></script>
    <script src="assets/js/slim.kickstart.min.js"></script>
</asp:Content>

