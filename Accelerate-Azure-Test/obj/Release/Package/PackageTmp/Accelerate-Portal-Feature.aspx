﻿<%@ Page Title="Accelerate - Knowledge Base Feature" Language="C#" MasterPageFile="~/Accelerate.master" AutoEventWireup="true" Inherits="Accelerate_Portal_Feature" Codebehind="Accelerate-Portal-Feature.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="infobox">
        <div class="top-bar-alternative">
            <h3>Knowledge Base - (<asp:Literal runat="server" ID="litPortalHeading"></asp:Literal>)</h3>
        </div>

        <asp:Literal runat="server" ID="litPortalFeatures"></asp:Literal>

        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="Green3 btn" OnClick="btnSave_Click" style="display:none;"/>
        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="Green3 btn" OnClientClick='history.go(-1);return false;' />
    </div>
    <asp:HiddenField runat="server" ID="lblCheckboxes" />

    <script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>

    <script type="text/javascript">
        function loopForm() {
            var x = document.forms[0];
            var cbResults = 'Checkboxes: ';
            for (var i = 0; i < x.elements.length; i++) {
                if (x.elements[i].type == 'checkbox') {
                    if (x.elements[i].checked == true) {
                        if (cbResults == "Checkboxes: ") {
                            cbResults = x.elements[i].id + ':' + x.elements[i].value;
                        }
                        else {
                            cbResults += "|" + x.elements[i].id + ':' + x.elements[i].value;
                        }
                    }
                }
            }
            document.getElementById('<%= lblCheckboxes.ClientID %>').value = cbResults;
        }
    </script>
    <script type="text/javascript">
        function loopFormCheck() {
            var x = document.forms[0];
            var cbResults = document.getElementById('<%= lblCheckboxes.ClientID %>').value;
            for (var i = 0; i < x.elements.length; i++) {
                if (x.elements[i].type == 'checkbox') {
                    var items = cbResults.split("|");
                    $.each(items, function (index, value) {
                        if (x.elements[i].id + ":on" == value) {
                            x.elements[i].checked = true;
                        }
                    });
                }
            }
        }
    </script>

</asp:Content>

