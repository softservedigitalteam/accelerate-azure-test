﻿<%@ Page Title="Accelerate - Dashboard" Language="C#" MasterPageFile="~/Accelerate.master" AutoEventWireup="true" Inherits="Accelerate_Home" Codebehind="Accelerate-Home.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel runat="server" ID="updHome">
        <ContentTemplate>
            <div class="infobox">
                <video src="assets/videos/T&T_Launch_1185x540_optimised.mp4" controls="controls" style="width:100%;" poster="/img/videoPoster.png"></video>
                <br style="clear:both"/>
                <br style="clear:both"/>
                
                <div class="row-fluid">
                    <div class="span2 PointsSection">
                        <div class="PointNumbersText">
                            <asp:Literal runat="server" ID="litPoints"></asp:Literal>
                        </div>
                        <br />
                        <br />
                        <div class="points-text-display">POINTS</div>
                    </div>
                    <div class="span10">
                        <div class="row-fluid">
                            <div class="showcaseSection Black" style="font-weight:bold; font-size:12px;">My Showcase</div>
                            <asp:LinkButton runat="server" OnClick="btnPhase1_Click" ID="btnPhase1">
                                <div class="showcaseSection Blue1">
                                    <span class="hidden-laptop">Phase</span> 1:
                                    <asp:Literal runat="server" ID="litPhase1Text"></asp:Literal>
                                </div>
                            </asp:LinkButton>
                            <asp:LinkButton runat="server" ID="btnPhase2" OnClick="btnPhase2_Click" Enabled="false">
                                <div class="showcaseSection Grey" id="phase2Button" runat="server">
                                    <span class="hidden-laptop">Phase</span> 2:
                                    <asp:Literal runat="server" ID="litPhase2Text"></asp:Literal>
                                </div>
                            </asp:LinkButton>
                            <asp:LinkButton runat="server" ID="btnPhase3" OnClick="btnPhase3_Click" Enabled="false" Style="display: block;">
                                <div class="showcaseSection Grey" id="phase3Button" runat="server">
                                    <span class="hidden-laptop">Phase</span> 3:
                                    <asp:Literal runat="server" ID="litPhase3Text"></asp:Literal>
                                </div>
                            </asp:LinkButton>
                        </div>
                        <div class="well">
                            <asp:Repeater ID="rpAchievements" runat="server">
                                <ItemTemplate>
                                    <%#Eval ("strNewRow") %>
                                    <div class="achievement" style="text-align: center; margin: 0 1.2%;">
                                        <a href='<%#Eval ("strLink") %>'>
                                            <img onerror="this.src='images/imgAchievementEmpty.png'" src='<%#Eval ("FullPathForImage") %>' title='<%#Eval ("strTitle") %>' alt='<%#Eval ("strTitle") %>' width='70%' /></a>
                                        <br />
                                        <%#Eval ("strTitle") %>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            <div class="infobox" style="margin-top: 10px;">
                <div class="top-bar">
                    <h3>My Current Mission</h3>
                </div>
                <div class="well">
                    <asp:Literal ID="litCurrentMission" runat="server"></asp:Literal>
                </div>
            </div>
            <div class="infobox">
                <div class="top-bar">
                    <h3>My Business Unit</h3>
                </div>
                <div class="well">
                    <asp:Literal ID="litGroupMemberMain" runat="server"></asp:Literal>
                </div>
            </div>
            <asp:UpdateProgress ID="updateProgress" runat="server">
                <ProgressTemplate>
                    <div style="position: fixed; height: 100%; width: 100%; overflow: show; margin: auto; top: 0; left: 0; bottom: 0;right: 0; z-index: 9999999; background-color: #fff; opacity: 0.8;">
                        <center><asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="img/Loader.gif" AlternateText="Loading ..." ToolTip="Loading ..." style="opacity:1;position:fixed;top: 43%;"/></center>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </ContentTemplate>
        <Triggers>
        </Triggers>
    </asp:UpdatePanel>
    <asp:HiddenField ID="hdfAvailableAchiements" runat="server" />

</asp:Content>
