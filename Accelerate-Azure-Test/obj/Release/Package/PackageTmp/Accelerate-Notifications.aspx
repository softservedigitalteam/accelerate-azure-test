﻿<%@ Page Title="Accelerate-Notifications" Language="C#" MasterPageFile="~/Accelerate.master" AutoEventWireup="true" Inherits="Accelerate_Notifications" Codebehind="Accelerate-Notifications.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="infobox">
        <div class="top-bar-alternative">
            <h3>Notifications</h3>
        </div>
        <div class="infobox well-short" style="color: #000; background: #FFF">
            <b style="font-size: 13px;">All Notifications</b>
            <br />
            <br />
            <asp:Literal ID="litActivityFeed" runat="server"></asp:Literal>
        </div>
    </div>
</asp:Content>

