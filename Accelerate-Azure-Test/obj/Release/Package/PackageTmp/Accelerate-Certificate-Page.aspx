﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Accelerate.master" AutoEventWireup="true" Inherits="Accelerate_Certificate_Page" Codebehind="Accelerate-Certificate-Page.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript">
        function pageLoad() {
            var modalPopup = $find('ModalPopupExtenderViewerBID');
            modalPopup.add_shown(function () {
                modalPopup._backgroundElement.addEventListener("click", function () {
                    modalPopup.hide();
                });
            });
        };
    </script>


</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    
    <div class="well">
          <iframe style="border: none; width:100%; height:80vh;" id="ifrmCertViewer" runat="server"/> 
     </div>

    <asp:Button ID="btnContinue" runat="server" Text="Continue" CssClass="Green3 btn" OnClick="btnContinue_Click" />

    <%--##MODAL POPUP--%>
    <%--<div class="infobox">

        <cc1:ModalPopupExtender ID="ModalPopupExtenderViewer" runat="server" TargetControlID="btnClick"
        PopupControlID="pnlView" BackgroundCssClass="modalBackground" CancelControlID="btnCancel"
        RepositionMode="RepositionOnWindowResize" BehaviorID="ModalPopupExtenderViewerBID">
        </cc1:ModalPopupExtender>        
 
    <asp:Panel ID="pnlView" runat="server" HorizontalAlign="Center" Width="50%">
        <div>
            <div class="top-bar" >
               <h3>Deloitte Certificate</h3> 
                <asp:LinkButton style="float:right; padding-top:5px;" runat="server" ID="btnCloseImages" CssClass="btnCloseDefaultImages" ForeColor="White" Font-Bold="true">X</asp:LinkButton>
            </div>
            <div class="well">
                <iframe style="border: none; width:100%; height:80vh;" id="ifrmViewer" runat="server"/> 
           </div>
        </div>
    </asp:Panel>
                <br /><br /><br />
    </div>--%>

    <%--##MODAL POPUP--%>

    <%--##MODAL POPUP--%>
         <%--<asp:button id="Button1" runat="server" text="Button" style="display: none"/>
        <cc1:modalpopupextender id="ModalPopupExtender1" runat="server" 
	        cancelcontrolid="btnCancel" okcontrolid="btnOkay" 
	        targetcontrolid="Button1" popupcontrolid="Panel1" 
	        popupdraghandlecontrolid="PopupHeader" drag="true" 
	        backgroundcssclass="ModalPopupBG">
        </cc1:modalpopupextender>

        <asp:panel id="Panel1" style="display: none" runat="server">
	        <div class="HellowWorldPopup">
                        <div class="PopupHeader" id="PopupHeader">Header</div>
                        <div class="PopupBody">
                            <p>This is a simple modal dialog</p>
                        </div>
                        <div class="Controls">
                            <input id="btnOkay" type="button" value="Done" />
                            <input id="btnCancel" type="button" value="Cancel" />
		        </div>
                </div>
        </asp:panel>--%>
   <%--##MODAL POPUP--%>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScrMang" Runat="Server">
    
</asp:Content>

