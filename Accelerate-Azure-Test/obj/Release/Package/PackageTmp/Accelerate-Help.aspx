﻿<%@ Page Title="Help" Language="C#" MasterPageFile="~/Accelerate.master" AutoEventWireup="true" Inherits="Accelerate_Help" Codebehind="Accelerate-Help.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script src="js/jquery.js"></script>
    <link rel="stylesheet" href="css/font-awesome.min.css" />
    <!-- SlidesJS Optional: If you'd like to use this design -->
    <style>
        a.slidesjs-next,
        a.slidesjs-previous,
        a.slidesjs-play,
        a.slidesjs-stop {
            background-image: url(img/btns-next-prev.png);
            background-repeat: no-repeat;
            display: block;
            width: 12px;
            height: 18px;
            overflow: hidden;
            text-indent: -9999px;
            float: left;
            margin-right: 5px;
            margin-top: 5px;
        }

        a.slidesjs-next {
            margin-right: 10px;
            background-position: -12px 0;
        }

        a:hover.slidesjs-next {
            background-position: -12px -18px;
        }

        a.slidesjs-previous {
            background-position: 0 0;
        }

        a:hover.slidesjs-previous {
            background-position: 0 -18px;
        }

        a.slidesjs-play {
            width: 15px;
            background-position: -25px 0;
        }

        a:hover.slidesjs-play {
            background-position: -25px -18px;
        }

        a.slidesjs-stop {
            width: 18px;
            background-position: -41px 0;
        }

        a:hover.slidesjs-stop {
            background-position: -41px -18px;
        }

        .slidesjs-pagination {
            margin: 7px 0 0;
            float: right;
            list-style: none;
        }

            .slidesjs-pagination li {
                float: left;
                margin: 0 1px;
            }

                .slidesjs-pagination li a {
                    display: block;
                    width: 23px;
                    height: 0;
                    padding-top: 23px;
                    background-image: url(img/pagination.png);
                    background-position: 0 0;
                    float: left;
                    overflow: hidden;
                }
    </style>
    <!-- End SlidesJS Optional-->
    <!-- SlidesJS Required: These styles are required if you'd like a responsive slideshow -->
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="infobox">
        <div class="top-bar-alternative">
            <h3>Help</h3>
        </div>
        <br class="clearfix" />
        <!-- The container is used to define the width of the slideshow -->
        <center>
            <div id="slides">
                <img src="images/slide1.png" alt="Tutorial 1"/>
                <img src="images/slide2.png" alt="Tutorial 2"/>
                <img src="images/slide3.png" alt="Tutorial 3"/>
                <img src="images/slide4.png" alt="Tutorial 4"/>
                <img src="images/slide5.png" alt="Tutorial 5"/>
            </div>
        </center>
    </div>

    <script src="js/jquery-1.9.1.min.js"></script>
    <!-- SlidesJS Required: Link to jquery.slides.js -->
    <script src="js/jquery.slides.min.js"></script>
    <!-- SlidesJS Required: Initialize SlidesJS with a jQuery doc ready -->
    <script>
        $(function () {
            $('#slides').slidesjs({
                width: 784,
                height: 490,
                play: {
                    active: true,
                    auto: true,
                    interval: 6000,
                    swap: true
                }
            });
        });
    </script>
    
    <!-- End SlidesJS Required -->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScrMang" runat="Server">

        <script src='assets/js/jquery.hotkeys.js'></script>
        <script src='assets/js/calendar/fullcalendar.min.js'></script>
        <script src="assets/js/jquery-ui-1.10.2.custom.min.js"></script>
        <script src="assets/js/jquery.pajinate.js"></script>
        <script src="assets/js/jquery.prism.min.js"></script>
        <script src="assets/js/jquery.dataTables.min.js"></script>
        <script src="assets/js/charts/jquery.flot.time.js"></script>
        <script src="assets/js/charts/jquery.flot.pie.js"></script>
        <script src="assets/js/charts/jquery.flot.resize.js"></script>
        <script src="assets/js/bootstrap/bootstrap.min.js"></script>
        <script src="assets/js/bootstrap/bootstrap-wysiwyg.js"></script>
        <script src="assets/js/bootstrap/bootstrap-typeahead.js"></script>
        <script src="assets/js/jquery.easing.min.js"></script>
        <script src="assets/js/jquery.chosen.min.js"></script>
        <script src="assets/js/avocado-custom.js"></script>
</asp:Content>

