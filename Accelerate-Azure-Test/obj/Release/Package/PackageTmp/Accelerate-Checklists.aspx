﻿<%@ Page Title="Accelerate - Checklists" Language="C#" MasterPageFile="~/Accelerate.master" AutoEventWireup="true" Inherits="Accelerate_Checklists" Codebehind="Accelerate-Checklists.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .rightMenuContainer {
            position: relative;
        }

        @media (max-width: 1024px) {
            .span3{
                float: left;
                width: 30%;
            }

            .divKnowledgeBaseBottom {
                position: relative;
                top: -20px;
                margin-bottom: 260px;
                left: 92px;
            }

            .divKnowledgeBaseTop {
                width: 837px;
                float: left;
                left: 92px;
                position: relative;
            }
        }

        @media (max-width: 769px) {
            .divKnowledgeBaseBottom {
                position: relative;
                top: 0px;
                margin-bottom: 260px;
            }

            .divKnowledgeBaseTop {
                position: relative;
                left: -35px;
            }
            .span3
            {
                width: 33% !important;
                float: left !important;
            }
        }

        @media (max-width: 738px) {
            .divKnowledgeBaseBottom {
                position: relative;
                top: -10px;
                left: 90px;
                margin-bottom: 260px;
            }

            .divKnowledgeBaseTop {
                position: relative;
                left: -65px;
            }
        }

        
        @media (max-width: 668px) {
            .divKnowledgeBaseBottom {
                position: relative;
                top: 0px;
                left: 89px;
                margin-bottom: 0px;
            }

            .divKnowledgeBaseTop {
                position: relative;
                left: -81px;
            }
        }
        @media (max-width: 568px) {
            .divKnowledgeBaseBottom {
                position: relative;
                top: 0px;
                left: 87px;
                margin-bottom: 0px;
            }

            .divKnowledgeBaseTop {
                position: relative;
                left: -144px;
            }
        }

        @media (max-width: 481px) {
            .divKnowledgeBaseBottom {
                position: relative;
                top: 0px;
                left: 92px;
                margin-bottom: 0px;
            }

            .divKnowledgeBaseTop {
                position: relative;
                left: -200px;
            }
        }

        @media (max-width: 415px) {
            .divKnowledgeBaseBottom {
                position: relative;
                top: 0px;
                left: 93px;
                margin-bottom: 0px;
            }

            .divKnowledgeBaseTop {
                position: relative;
                left: -230px;
            }
        }

            

        @media (max-width: 376px) {
            .divKnowledgeBaseBottom {
                position: relative;
                top: 0px;
                left: 92px;
                margin-bottom: 0px;
            }

            .divKnowledgeBaseTop {
                position: relative;
                left: -246px;
            }
        }
        @media (max-width: 321px) {
            .divKnowledgeBaseBottom {
                position: relative;
                top: 0px;
                left: 90px;
                margin-bottom: 0px;
            }

            .divKnowledgeBaseTop {
                position: relative;
                left: -262px;
            }
        }

       

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="genericMainHeading">
        <h2>Checklists</h2>
    </div>
    <%--<br />
    <h4>Features</h4>--%>
    <br class="clr" />
    <asp:Literal runat="server" ID="litPortalFeatures"></asp:Literal>
    <br class="clr" />

</asp:Content>
