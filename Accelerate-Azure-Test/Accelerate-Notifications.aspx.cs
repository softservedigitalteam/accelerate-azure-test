﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Accelerate_Notifications : System.Web.UI.Page
{
    private clsAccountUsers clsAccountUsers;

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Logout User
        if (Request.QueryString["action"] == "syslogout")
        {
            //### Clear all seesion variables
            Session.Clear();
        }

        //### Check if session clsUser exists
        if (Session["clsAccountUsers"] == null)
        {
            //### Redirect back to login
            Response.Redirect("Accelerate-Login.aspx");
        }
        clsAccountUsers = (clsAccountUsers)Session["clsAccountUsers"];
        try
        {
            popNotifications();
        }
        catch(Exception ex)
        {
            Response.Redirect("Accelerate-Error404.aspx");
        }
    }

    protected void popNotifications()
    {
        DataTable dtNotifications = clsNotifications.GetNotificationsList("iAccountUserID=" + clsAccountUsers.iAccountUserID, "dtAdded DESC");

        int iCount = 0;
        int iMissionID = 0;
        //int iNotificationID;

        StringBuilder strbNotifications = new StringBuilder();

        foreach (DataRow dr in dtNotifications.Rows)
        {
            ++iCount;

            iMissionID = Convert.ToInt32(dr["iMissionID"]);

            clsMissions clsMissions = new clsMissions(iMissionID);

            //iNotificationID = Convert.ToInt32(dr["iNotificationID"].ToString());
            strbNotifications.AppendLine("<div style='padding-bottom: 20px;'>");
            strbNotifications.AppendLine("<b class='media-heading'>" + clsMissions.strTitle + "</b><br />");
            strbNotifications.AppendLine(dr["strMessage"].ToString());
            strbNotifications.AppendLine("</div>");

            if (iCount >= 10)
                break;
        }

        if (!(iCount == 0))
            litActivityFeed.Text = strbNotifications.ToString();
        else
            litActivityFeed.Text = "There are no notifications for you yet.";
    }
}