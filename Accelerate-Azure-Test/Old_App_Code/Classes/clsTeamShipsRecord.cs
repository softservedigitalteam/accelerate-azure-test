
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsTeamShips
/// </summary>
public class clsTeamShips
{
    #region MEMBER VARIABLES

        private int m_iTeamShipID;
        private DateTime m_dtAdded;
        private int m_iAddedBy;
        private DateTime m_dtEdited;
        private int m_iEditedBy;
        private int m_iMissionID;
        private String m_strName;
        private String m_strTitle;
        private String m_strPathToImages;
        private String m_strMasterImage;
        private bool m_bIsDisplayed;
        private bool m_bIsDeleted;
        
    #endregion 

    #region PROPERTIES

        public int iTeamShipID
        {
            get
            {
                return m_iTeamShipID;
            }
        }

        public DateTime dtAdded
        {
            get
            {
                return m_dtAdded;
            }
            set
            {
                m_dtAdded = value;
            }
        }

        public int iAddedBy
        {
            get
            {
                return m_iAddedBy;
            }
            set
            {
                m_iAddedBy = value;
            }
        }

        public DateTime dtEdited
        {
            get
            {
                return m_dtEdited;
            }
            set
            {
                m_dtEdited = value;
            }
        }

        public int iEditedBy
        {
            get
            {
                return m_iEditedBy;
            }
            set
            {
                m_iEditedBy = value;
            }
        }

        public int iMissionID
        {
            get
            {
                return m_iMissionID;
            }
            set
            {
                m_iMissionID = value;
            }
        }

        public String strName
        {
            get
            {
                return m_strName;
            }
            set
            {
                m_strName = value;
            }
        }

        public String strTitle
        {
            get
            {
                return m_strTitle;
            }
            set
            {
                m_strTitle = value;
            }
        }

        public String strPathToImages
        {
            get
            {
                return m_strPathToImages;
            }
            set
            {
                m_strPathToImages = value;
            }
        }

        public String strMasterImage
        {
            get
            {
                return m_strMasterImage;
            }
            set
            {
                m_strMasterImage = value;
            }
        }

        public bool bIsDisplayed
        {
            get
            {
                return m_bIsDisplayed;
            }
            set
            {
                m_bIsDisplayed = value;
            }
        }

        public bool bIsDeleted
        {
            get
            {
                return m_bIsDeleted;
            }
            set
            {
                m_bIsDeleted = value;
            }
        }

    
    #endregion
    
    #region CONSTRUCTORS

    public clsTeamShips()
    {
        m_iTeamShipID = 0;
    }

    public clsTeamShips(int iTeamShipID)
    {
        m_iTeamShipID = iTeamShipID;
        GetData();
    }

    #endregion

    #region PUBLIC METHODS

        public virtual void Update()
        {
            try
            {
                if (iTeamShipID == 0)
                {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@dtAdded", m_dtAdded),
                        new SqlParameter("@iAddedBy", m_iAddedBy),
                        new SqlParameter("@iMissionID", m_iMissionID),
                        new SqlParameter("@strName", m_strName),
                        new SqlParameter("@strTitle", m_strTitle),
                        new SqlParameter("@strPathToImages", m_strPathToImages),
                        new SqlParameter("@strMasterImage", m_strMasterImage),
                        new SqlParameter("@bIsDisplayed", m_bIsDisplayed)                  
                  };

                  //### Add
                  m_iTeamShipID = (int)clsDataAccess.ExecuteScalar("spTeamShipsInsert", sqlParametersInsert);                    
                    }
                    else
                    {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iTeamShipID", m_iTeamShipID),
                         new SqlParameter("@dtEdited", m_dtEdited),
                         new SqlParameter("@iEditedBy", m_iEditedBy),
                         new SqlParameter("@iMissionID", m_iMissionID),
                         new SqlParameter("@strName", m_strName),
                         new SqlParameter("@strTitle", m_strTitle),
                         new SqlParameter("@strPathToImages", m_strPathToImages),
                         new SqlParameter("@strMasterImage", m_strMasterImage),
                         new SqlParameter("@bIsDisplayed", m_bIsDisplayed)
                    };
                    //### Update
                    clsDataAccess.Execute("spTeamShipsUpdate", sqlParametersUpdate);
                    }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Delete(int iTeamShipID)
        {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iTeamShipID", iTeamShipID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spTeamShipsDelete", sqlParameter);
        }

        public static DataTable GetTeamShipsList()
        {
            SqlParameter[] EmptySqlParameter = new SqlParameter[]{};
            return clsDataAccess.GetDataTable("spTeamShipsList", EmptySqlParameter);
        }
        public static DataTable GetTeamShipsList(string strFilterExpression, string strSortExpression)
        {
            DataView dvTeamShipsList = new DataView();

            SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
            dvTeamShipsList = clsDataAccess.GetDataView("spTeamShipsList", EmptySqlParameter);
            dvTeamShipsList.RowFilter = strFilterExpression;
            dvTeamShipsList.Sort = strSortExpression;

            return dvTeamShipsList.ToTable();
        }
    #endregion

    #region PROTECTED METHODS

        protected virtual void GetData()
        {
            try
                {
                    //### Populate
                    SqlParameter[] sqlParameter = new SqlParameter[] 
                        {
                            new SqlParameter("@iTeamShipID", m_iTeamShipID)
                        };
                DataRow drRecord = clsDataAccess.GetRecord("spTeamShipsGetRecord", sqlParameter);

                m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);
                m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);

                if (drRecord["dtEdited"] != DBNull.Value)
                   m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

             if (drRecord["iEditedBy"] != DBNull.Value)
               m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);

                m_iMissionID = Convert.ToInt32(drRecord["iMissionID"]);
                m_strName = drRecord["strName"].ToString();
                m_strTitle = drRecord["strTitle"].ToString();
                m_strPathToImages = drRecord["strPathToImages"].ToString();
                m_strMasterImage = drRecord["strMasterImage"].ToString();
                m_bIsDisplayed = Convert.ToBoolean(drRecord["bIsDisplayed"]);
                m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);
 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    #endregion
}