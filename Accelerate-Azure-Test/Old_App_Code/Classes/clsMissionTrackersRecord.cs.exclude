
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsMissionTrackers
/// </summary>
public class clsMissionTrackers
{
    #region MEMBER VARIABLES

    private int m_iMissionTrackerID;
    private DateTime m_dtAdded;
    private int m_iAddedBy;
    private DateTime m_dtEdited;
    private int m_iEditedBy;
    private int m_iMissionID;
    private String m_strMissionName;
    private int m_iPhaseID;
    private int m_iAchievementID;
    private String m_strAchievementName;
    private bool m_bIsDeleted;

    #endregion

    #region PROPERTIES

    public int iMissionTrackerID
    {
        get
        {
            return m_iMissionTrackerID;
        }
    }

    public DateTime dtAdded
    {
        get
        {
            return m_dtAdded;
        }
        set
        {
            m_dtAdded = value;
        }
    }

    public int iAddedBy
    {
        get
        {
            return m_iAddedBy;
        }
        set
        {
            m_iAddedBy = value;
        }
    }

    public DateTime dtEdited
    {
        get
        {
            return m_dtEdited;
        }
        set
        {
            m_dtEdited = value;
        }
    }

    public int iEditedBy
    {
        get
        {
            return m_iEditedBy;
        }
        set
        {
            m_iEditedBy = value;
        }
    }

    public int iMissionID
    {
        get
        {
            return m_iMissionID;
        }
        set
        {
            m_iMissionID = value;
        }
    }

    public String strMissionName
    {
        get
        {
            return m_strMissionName;
        }
        set
        {
            m_strMissionName = value;
        }
    }

    public int iPhaseID
    {
        get
        {
            return m_iPhaseID;
        }
        set
        {
            m_iPhaseID = value;
        }
    }

    public int iAchievementID
    {
        get
        {
            return m_iAchievementID;
        }
        set
        {
            m_iAchievementID = value;
        }
    }

    public String strAchievementName
    {
        get
        {
            return m_strAchievementName;
        }
        set
        {
            m_strAchievementName = value;
        }
    }

    public bool bIsDeleted
    {
        get
        {
            return m_bIsDeleted;
        }
        set
        {
            m_bIsDeleted = value;
        }
    }


    #endregion

    #region CONSTRUCTORS

    public clsMissionTrackers()
    {
        m_iMissionTrackerID = 0;
    }

    public clsMissionTrackers(int iMissionTrackerID)
    {
        m_iMissionTrackerID = iMissionTrackerID;
        GetData();
    }

    #endregion

    #region PUBLIC METHODS

    public virtual void Update()
    {
        try
        {
            if (iMissionTrackerID == 0)
            {
                //### Assign values to the parameter list for each corresponding column in the DB
                SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@dtAdded", m_dtAdded),
                        new SqlParameter("@iAddedBy", m_iAddedBy),         
                        new SqlParameter("@iMissionID", m_iMissionID), 
                        new SqlParameter("@strMissionName", m_strMissionName), 
                        new SqlParameter("@iPhaseID", m_iPhaseID), 
                        new SqlParameter("@iAchievementID", m_iAchievementID), 
                        new SqlParameter("@strAchievementName", m_strAchievementName)
                  };

                //### Add
                m_iMissionTrackerID = (int)clsDataAccess.ExecuteScalar("spMissionTrackersInsert", sqlParametersInsert);
            }
            else
            {
                //### Assign values to the parameter list for each corresponding column in the DB
                SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iMissionTrackerID", m_iMissionTrackerID),
                         new SqlParameter("@dtEdited", m_dtEdited),
                         new SqlParameter("@iEditedBy", m_iEditedBy),
                         new SqlParameter("@iMissionID", m_iMissionID), 
                         new SqlParameter("@strMissionName", m_strMissionName), 
                         new SqlParameter("@iPhaseID", m_iPhaseID), 
                         new SqlParameter("@iAchievementID", m_iAchievementID), 
                         new SqlParameter("@strAchievementName", m_strAchievementName)
                    };
                //### Update
                clsDataAccess.Execute("spMissionTrackersUpdate", sqlParametersUpdate);
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public static void Delete(int iMissionTrackerID)
    {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iMissionTrackerID", iMissionTrackerID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spMissionTrackersDelete", sqlParameter);
    }

    public static DataTable GetMissionAnswersList()
    {
        SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
        return clsDataAccess.GetDataTable("spMissionTrackersList", EmptySqlParameter);
    }
    public static DataTable GetMissionAnswersList(string strFilterExpression, string strSortExpression)
    {
        DataView dvMissionAnswersList = new DataView();

        SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
        dvMissionAnswersList = clsDataAccess.GetDataView("spMissionTrackersList", EmptySqlParameter);
        dvMissionAnswersList.RowFilter = strFilterExpression;
        dvMissionAnswersList.Sort = strSortExpression;

        return dvMissionAnswersList.ToTable();
    }
    #endregion

    #region PROTECTED METHODS

    protected virtual void GetData()
    {
        try
        {
            //### Populate
            SqlParameter[] sqlParameter = new SqlParameter[] 
            {
                new SqlParameter("@iMissionTrackerID", m_iMissionTrackerID)
            };
            DataRow drRecord = clsDataAccess.GetRecord("spMissionTrackersGetRecord", sqlParameter);

            m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);
            m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);

            if (drRecord["dtEdited"] != DBNull.Value)
                m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

            if (drRecord["iEditedBy"] != DBNull.Value)
                m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);

            m_iMissionID = Convert.ToInt32(drRecord["iMissionID"]);
            m_strMissionName = drRecord["strMissionName"].ToString();
            m_iPhaseID = Convert.ToInt32(drRecord["iPhaseID"]);
            m_iAchievementID = Convert.ToInt32(drRecord["iAchievementID"]);
            m_strAchievementName = drRecord["strAchievementName"].ToString();

            m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    #endregion
}