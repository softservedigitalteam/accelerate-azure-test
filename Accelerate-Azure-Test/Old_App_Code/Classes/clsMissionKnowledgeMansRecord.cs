
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsMissionKnowledgeMans
/// </summary>
public class clsMissionKnowledgeMans
{
    #region MEMBER VARIABLES

        private int m_iMissionKnowledgeManID;
        private DateTime m_dtAdded;
        private int m_iAddedBy;
        private DateTime m_dtEdited;
        private int m_iEditedBy;
        private int m_iMissionID;
        private String m_strTitle;
        private String m_strPathToImages;
        private String m_strMasterImage;
        private bool m_bIsDeleted;
        
    #endregion 

    #region PROPERTIES

        public int iMissionKnowledgeManID
        {
            get
            {
                return m_iMissionKnowledgeManID;
            }
        }

        public DateTime dtAdded
        {
            get
            {
                return m_dtAdded;
            }
            set
            {
                m_dtAdded = value;
            }
        }

        public int iAddedBy
        {
            get
            {
                return m_iAddedBy;
            }
            set
            {
                m_iAddedBy = value;
            }
        }

        public DateTime dtEdited
        {
            get
            {
                return m_dtEdited;
            }
            set
            {
                m_dtEdited = value;
            }
        }

        public int iEditedBy
        {
            get
            {
                return m_iEditedBy;
            }
            set
            {
                m_iEditedBy = value;
            }
        }

        public int iMissionID
        {
            get
            {
                return m_iMissionID;
            }
            set
            {
                m_iMissionID = value;
            }
        }

        public String strTitle
        {
            get
            {
                return m_strTitle;
            }
            set
            {
                m_strTitle = value;
            }
        }

        public String strPathToImages
        {
            get
            {
                return m_strPathToImages;
            }
            set
            {
                m_strPathToImages = value;
            }
        }

        public String strMasterImage
        {
            get
            {
                return m_strMasterImage;
            }
            set
            {
                m_strMasterImage = value;
            }
        }

        public bool bIsDeleted
        {
            get
            {
                return m_bIsDeleted;
            }
            set
            {
                m_bIsDeleted = value;
            }
        }

    
    #endregion
    
    #region CONSTRUCTORS

    public clsMissionKnowledgeMans()
    {
        m_iMissionKnowledgeManID = 0;
    }

    public clsMissionKnowledgeMans(int iMissionKnowledgeManID)
    {
        m_iMissionKnowledgeManID = iMissionKnowledgeManID;
        GetData();
    }

    #endregion

    #region PUBLIC METHODS

        public virtual void Update()
        {
            try
            {
                if (iMissionKnowledgeManID == 0)
                {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@dtAdded", m_dtAdded),
                        new SqlParameter("@iAddedBy", m_iAddedBy),
                        new SqlParameter("@iMissionID", m_iMissionID),
                        new SqlParameter("@strTitle", m_strTitle),
                        new SqlParameter("@strPathToImages", m_strPathToImages),
                        new SqlParameter("@strMasterImage", m_strMasterImage)                  
                  };

                  //### Add
                  m_iMissionKnowledgeManID = (int)clsDataAccess.ExecuteScalar("spMissionKnowledgeMansInsert", sqlParametersInsert);                    
                    }
                    else
                    {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iMissionKnowledgeManID", m_iMissionKnowledgeManID),
                         new SqlParameter("@dtEdited", m_dtEdited),
                         new SqlParameter("@iEditedBy", m_iEditedBy),
                         new SqlParameter("@iMissionID", m_iMissionID),
                         new SqlParameter("@strTitle", m_strTitle),
                         new SqlParameter("@strPathToImages", m_strPathToImages),
                         new SqlParameter("@strMasterImage", m_strMasterImage)
                    };
                    //### Update
                    clsDataAccess.Execute("spMissionKnowledgeMansUpdate", sqlParametersUpdate);
                    }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Delete(int iMissionKnowledgeManID)
        {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iMissionKnowledgeManID", iMissionKnowledgeManID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spMissionKnowledgeMansDelete", sqlParameter);
        }

        public static DataTable GetMissionKnowledgeMansList()
        {
            SqlParameter[] EmptySqlParameter = new SqlParameter[]{};
            return clsDataAccess.GetDataTable("spMissionKnowledgeMansList", EmptySqlParameter);
        }
        public static DataTable GetMissionKnowledgeMansList(string strFilterExpression, string strSortExpression)
        {
            DataView dvMissionKnowledgeMansList = new DataView();

            SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
            dvMissionKnowledgeMansList = clsDataAccess.GetDataView("spMissionKnowledgeMansList", EmptySqlParameter);
            dvMissionKnowledgeMansList.RowFilter = strFilterExpression;
            dvMissionKnowledgeMansList.Sort = strSortExpression;

            return dvMissionKnowledgeMansList.ToTable();
        }
    #endregion

    #region PROTECTED METHODS

        protected virtual void GetData()
        {
            try
                {
                    //### Populate
                    SqlParameter[] sqlParameter = new SqlParameter[] 
                        {
                            new SqlParameter("@iMissionKnowledgeManID", m_iMissionKnowledgeManID)
                        };
                DataRow drRecord = clsDataAccess.GetRecord("spMissionKnowledgeMansGetRecord", sqlParameter);

                m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);
                m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);

                if (drRecord["dtEdited"] != DBNull.Value)
                   m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

             if (drRecord["iEditedBy"] != DBNull.Value)
               m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);

                m_iMissionID = Convert.ToInt32(drRecord["iMissionID"]);
                m_strTitle = drRecord["strTitle"].ToString();
                m_strPathToImages = drRecord["strPathToImages"].ToString();
                m_strMasterImage = drRecord["strMasterImage"].ToString();
                m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);
 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    #endregion
}