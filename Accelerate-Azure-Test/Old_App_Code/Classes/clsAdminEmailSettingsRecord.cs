
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsAdminEmailSettings
/// </summary>
public class clsAdminEmailSettings
{
    #region MEMBER VARIABLES

     private     int m_iAdminEmailSettingsID;
     private     string m_strAdminTitle;
     private     string m_strEmail1;
     private     string m_strEmail2;
    private bool m_bIsDeleted;

    #endregion

    #region PROPERTIES

    public int iAdminEmailSettingsID
    {
        get
        {
            return m_iAdminEmailSettingsID;
        }
    }

    public    string strAdminTitle
    {
        get
        {
            return m_strAdminTitle;
        }
        set
        {
            m_strAdminTitle = value;
        }
    }

    public    string strEmail1
    {
        get
        {
            return m_strEmail1;
        }
        set
        {
            m_strEmail1 = value;
        }
    }

    public    string strEmail2
    {
        get
        {
            return m_strEmail2;
        }
        set
        {
            m_strEmail2 = value;
        }
    }

    public bool bIsDeleted
    {
        get
        {
            return m_bIsDeleted;
        }
        set
        {
            m_bIsDeleted = value;
        }
    }


    #endregion

    #region CONSTRUCTORS

    public clsAdminEmailSettings()
    {
        m_iAdminEmailSettingsID = 0;
    }

    public clsAdminEmailSettings(int iAdminEmailSettingsID)
    {
        m_iAdminEmailSettingsID = iAdminEmailSettingsID;
        GetData();
    }

    #endregion

    #region PUBLIC METHODS

        public virtual void Update()
        {
            try
            {
                if (iAdminEmailSettingsID == 0)
                {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@strAdminTitle", m_strAdminTitle),
                        new SqlParameter("@strEmail1", m_strEmail1),
                        new SqlParameter("@strEmail2", m_strEmail2)                  
                  };

                  //### Add
                  m_iAdminEmailSettingsID = (int)clsDataAccess.ExecuteScalar("spAdminEmailSettingsInsert", sqlParametersInsert);                    
                    }
                    else
                    {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iAdminEmailSettingsID", m_iAdminEmailSettingsID),
                         new SqlParameter("@strAdminTitle", m_strAdminTitle),
                         new SqlParameter("@strEmail1", m_strEmail1),
                         new SqlParameter("@strEmail2", m_strEmail2)
          };
          //### Update
          clsDataAccess.Execute("spAdminEmailSettingsUpdate", sqlParametersUpdate);
                    }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Delete(int iAdminEmailSettingsID)
        {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iAdminEmailSettingsID", iAdminEmailSettingsID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spAdminEmailSettingsDelete", sqlParameter);
        }

        public static DataTable GetAdminEmailSettingsList()
        {
            SqlParameter[] EmptySqlParameter = new SqlParameter[]{};
            return clsDataAccess.GetDataTable("spAdminEmailSettingsList", EmptySqlParameter);
        }
        public static DataTable GetAdminEmailSettingsList(string strFilterExpression, string strSortExpression)
        {
            DataView dvAdminEmailSettingsList = new DataView();

            SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
            dvAdminEmailSettingsList = clsDataAccess.GetDataView("spAdminEmailSettingsList", EmptySqlParameter);
            dvAdminEmailSettingsList.RowFilter = strFilterExpression;
            dvAdminEmailSettingsList.Sort = strSortExpression;

            return dvAdminEmailSettingsList.ToTable();
        }
    #endregion

    #region PROTECTED METHODS

        protected virtual void GetData()
        {
            try
                {
                    //### Populate
                    SqlParameter[] sqlParameter = new SqlParameter[] 
                        {
                            new SqlParameter("@iAdminEmailSettingsID", m_iAdminEmailSettingsID)
                        };
                DataRow drRecord = clsDataAccess.GetRecord("spAdminEmailSettingsGetRecord", sqlParameter);

                   m_strAdminTitle = drRecord["strAdminTitle"].ToString();
                   m_strEmail1 = drRecord["strEmail1"].ToString();
                   m_strEmail2 = drRecord["strEmail2"].ToString();
                m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);

        }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    #endregion
}