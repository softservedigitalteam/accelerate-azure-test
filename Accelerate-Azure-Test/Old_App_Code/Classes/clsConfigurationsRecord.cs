
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsConfigurations
/// </summary>
public class clsConfigurations
{
    #region MEMBER VARIABLES

        private int m_iConfigurationID;
        private DateTime m_dtAdded;
        private int m_iAddedBy;
        private DateTime m_dtEdited;
        private int m_iEditedBy;
        private String m_strEmail;
        private bool m_bIsDeleted;
        
    #endregion 

    #region PROPERTIES

        public int iConfigurationID
        {
            get
            {
                return m_iConfigurationID;
            }
        }

        public DateTime dtAdded
        {
            get
            {
                return m_dtAdded;
            }
            set
            {
                m_dtAdded = value;
            }
        }

        public int iAddedBy
        {
            get
            {
                return m_iAddedBy;
            }
            set
            {
                m_iAddedBy = value;
            }
        }

        public DateTime dtEdited
        {
            get
            {
                return m_dtEdited;
            }
            set
            {
                m_dtEdited = value;
            }
        }

        public int iEditedBy
        {
            get
            {
                return m_iEditedBy;
            }
            set
            {
                m_iEditedBy = value;
            }
        }

        public String strEmail
        {
            get
            {
                return m_strEmail;
            }
            set
            {
                m_strEmail = value;
            }
        }

        public bool bIsDeleted
        {
            get
            {
                return m_bIsDeleted;
            }
            set
            {
                m_bIsDeleted = value;
            }
        }

    
    #endregion
    
    #region CONSTRUCTORS

    public clsConfigurations()
    {
        m_iConfigurationID = 0;
    }

    public clsConfigurations(int iConfigurationID)
    {
        m_iConfigurationID = iConfigurationID;
        GetData();
    }

    #endregion

    #region PUBLIC METHODS

        public virtual void Update()
        {
            try
            {
                if (iConfigurationID == 0)
                {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@dtAdded", m_dtAdded),
                        new SqlParameter("@iAddedBy", m_iAddedBy),
                        new SqlParameter("@strEmail", m_strEmail)       
                  };

                  //### Add
                    m_iConfigurationID = (int)clsDataAccess.ExecuteScalar("spConfigurationsInsert", sqlParametersInsert);                    
                    }
                    else
                    {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iConfigurationID", m_iConfigurationID),
                         new SqlParameter("@dtEdited", m_dtEdited),
                         new SqlParameter("@iEditedBy", m_iEditedBy),
                         new SqlParameter("@strEmail", m_strEmail)
                    };
                    //### Update
                    clsDataAccess.Execute("spConfigurationsUpdate", sqlParametersUpdate);
                    }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Delete(int iConfigurationID)
        {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iConfigurationID", iConfigurationID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spConfigurationsDelete", sqlParameter);
        }

        public static DataTable GetRanksList()
        {
            SqlParameter[] EmptySqlParameter = new SqlParameter[]{};
            return clsDataAccess.GetDataTable("spConfigurationsList", EmptySqlParameter);
        }

        public static DataTable GetRanksList(string strFilterExpression, string strSortExpression)
        {
            DataView dvRanksList = new DataView();

            SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
            dvRanksList = clsDataAccess.GetDataView("spConfigurationsList", EmptySqlParameter);
            dvRanksList.RowFilter = strFilterExpression;
            dvRanksList.Sort = strSortExpression;

            return dvRanksList.ToTable();
        }
    #endregion

    #region PROTECTED METHODS

        protected virtual void GetData()
        {
            try
                {
                    //### Populate
                    SqlParameter[] sqlParameter = new SqlParameter[] 
                        {
                            new SqlParameter("@iConfigurationID", m_iConfigurationID)
                        };
                    DataRow drRecord = clsDataAccess.GetRecord("spConfigurationsGetRecord", sqlParameter);

                m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);
                m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);

                if (drRecord["dtEdited"] != DBNull.Value)
                   m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

             if (drRecord["iEditedBy"] != DBNull.Value)
               m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);

                m_strEmail = drRecord["strEmail"].ToString();
                m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);
 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    #endregion
}