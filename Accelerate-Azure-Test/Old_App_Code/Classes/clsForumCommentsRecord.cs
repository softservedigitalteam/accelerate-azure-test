
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsForumComments
/// </summary>
public class clsForumComments
{
    #region MEMBER VARIABLES

        private int m_iForumCommentID;
        private DateTime m_dtAdded;
        private int m_iAddedBy;
        private DateTime m_dtEdited;
        private int m_iEditedBy;
        private int m_iForumID;
        private String m_strComment;
        private bool m_bIsDeleted;
        
    #endregion 

    #region PROPERTIES

        public int iForumCommentID
        {
            get
            {
                return m_iForumCommentID;
            }
        }

        public DateTime dtAdded
        {
            get
            {
                return m_dtAdded;
            }
            set
            {
                m_dtAdded = value;
            }
        }

        public int iAddedBy
        {
            get
            {
                return m_iAddedBy;
            }
            set
            {
                m_iAddedBy = value;
            }
        }

        public DateTime dtEdited
        {
            get
            {
                return m_dtEdited;
            }
            set
            {
                m_dtEdited = value;
            }
        }

        public int iEditedBy
        {
            get
            {
                return m_iEditedBy;
            }
            set
            {
                m_iEditedBy = value;
            }
        }

        public int iForumID
        {
            get
            {
                return m_iForumID;
            }
            set
            {
                m_iForumID = value;
            }
        }

        public String strComment
        {
            get
            {
                return m_strComment;
            }
            set
            {
                m_strComment = value;
            }
        }

        public bool bIsDeleted
        {
            get
            {
                return m_bIsDeleted;
            }
            set
            {
                m_bIsDeleted = value;
            }
        }

    
    #endregion
    
    #region CONSTRUCTORS

    public clsForumComments()
    {
        m_iForumCommentID = 0;
    }

    public clsForumComments(int iForumCommentID)
    {
        m_iForumCommentID = iForumCommentID;
        GetData();
    }

    #endregion

    #region PUBLIC METHODS

        public virtual void Update()
        {
            try
            {
                if (iForumCommentID == 0)
                {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@dtAdded", m_dtAdded),
                        new SqlParameter("@iAddedBy", m_iAddedBy),
                        new SqlParameter("@iForumID", m_iForumID),
                        new SqlParameter("@strComment", m_strComment)                  
                  };

                  //### Add
                  m_iForumCommentID = (int)clsDataAccess.ExecuteScalar("spForumCommentsInsert", sqlParametersInsert);                    
                    }
                    else
                    {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iForumCommentID", m_iForumCommentID),
                         new SqlParameter("@dtEdited", m_dtEdited),
                         new SqlParameter("@iEditedBy", m_iEditedBy),
                         new SqlParameter("@iForumID", m_iForumID),
                         new SqlParameter("@strComment", m_strComment)
                    };
                    //### Update
                    clsDataAccess.Execute("spForumCommentsUpdate", sqlParametersUpdate);
                    }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Delete(int iForumCommentID)
        {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iForumCommentID", iForumCommentID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spForumCommentsDelete", sqlParameter);
        }

        public static DataTable GetForumCommentsList()
        {
            SqlParameter[] EmptySqlParameter = new SqlParameter[]{};
            return clsDataAccess.GetDataTable("spForumCommentsList", EmptySqlParameter);
        }
        public static DataTable GetForumCommentsList(string strFilterExpression, string strSortExpression)
        {
            DataView dvForumCommentsList = new DataView();

            SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
            dvForumCommentsList = clsDataAccess.GetDataView("spForumCommentsList", EmptySqlParameter);
            dvForumCommentsList.RowFilter = strFilterExpression;
            dvForumCommentsList.Sort = strSortExpression;

            return dvForumCommentsList.ToTable();
        }
    #endregion

    #region PROTECTED METHODS

        protected virtual void GetData()
        {
            try
                {
                    //### Populate
                    SqlParameter[] sqlParameter = new SqlParameter[] 
                        {
                            new SqlParameter("@iForumCommentID", m_iForumCommentID)
                        };
                DataRow drRecord = clsDataAccess.GetRecord("spForumCommentsGetRecord", sqlParameter);

                m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);
                m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);

                if (drRecord["dtEdited"] != DBNull.Value)
                   m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

             if (drRecord["iEditedBy"] != DBNull.Value)
               m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);

                m_iForumID = Convert.ToInt32(drRecord["iForumID"]);
                m_strComment = drRecord["strComment"].ToString();
                m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);
 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    #endregion
}