using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsFinalMessages
/// </summary>
public class clsFinalMessages
{
    #region MEMBER VARIABLES

        private int m_iFinalMessageID;
        private DateTime m_dtAdded;
        private int m_iAddedBy;
        private DateTime m_dtEdited;
        private int m_iEditedBy;
        private String m_strHeaderText;
        private String m_strParagraphText;
        private String m_strPathToImages;
        private String m_strMasterImage;
        private bool m_bIsDeleted;
        
    #endregion 

    #region PROPERTIES

        public int iFinalMessageID
        {
            get
            {
                return m_iFinalMessageID;
            }
        }

        public DateTime dtAdded
        {
            get
            {
                return m_dtAdded;
            }
            set
            {
                m_dtAdded = value;
            }
        }

        public int iAddedBy
        {
            get
            {
                return m_iAddedBy;
            }
            set
            {
                m_iAddedBy = value;
            }
        }

        public DateTime dtEdited
        {
            get
            {
                return m_dtEdited;
            }
            set
            {
                m_dtEdited = value;
            }
        }

        public int iEditedBy
        {
            get
            {
                return m_iEditedBy;
            }
            set
            {
                m_iEditedBy = value;
            }
        }

        public String strHeaderText
        {
            get
            {
                return m_strHeaderText;
            }
            set
            {
                m_strHeaderText = value;
            }
        }

        public String strParagraphText
        {
            get
            {
                return m_strParagraphText;
            }
            set
            {
                m_strParagraphText = value;
            }
        }

        public String strPathToImages
        {
            get
            {
                return m_strPathToImages;
            }
            set
            {
                m_strPathToImages = value;
            }
        }

        public String strMasterImage
        {
            get
            {
                return m_strMasterImage;
            }
            set
            {
                m_strMasterImage = value;
            }
        }

        public bool bIsDeleted
        {
            get
            {
                return m_bIsDeleted;
            }
            set
            {
                m_bIsDeleted = value;
            }
        }

    
    #endregion
    
    #region CONSTRUCTORS

    public clsFinalMessages()
    {
        m_iFinalMessageID = 0;
    }

    public clsFinalMessages(int iFinalMessageID)
    {
        m_iFinalMessageID = iFinalMessageID;
        GetData();
    }

    #endregion

    #region PUBLIC METHODS

        public virtual void Update()
        {
            try
            {
                if (iFinalMessageID == 0)
                {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@dtAdded", m_dtAdded),
                        new SqlParameter("@iAddedBy", m_iAddedBy),
                        new SqlParameter("@strHeaderText", m_strHeaderText),
                        new SqlParameter("@strParagraphText", m_strParagraphText),
                        new SqlParameter("@strPathToImages", m_strPathToImages),
                        new SqlParameter("@strMasterImage", m_strMasterImage)                  
                    };

                    //### Add
                    m_iFinalMessageID = (int)clsDataAccess.ExecuteScalar("spFinalMessagesInsert", sqlParametersInsert); 
                   
                }
                else
                {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                            new SqlParameter("@iFinalMessageID", m_iFinalMessageID),
                            new SqlParameter("@dtEdited", m_dtEdited),
                            new SqlParameter("@iEditedBy", m_iEditedBy),
                            new SqlParameter("@strHeaderText", m_strHeaderText),
                            new SqlParameter("@strParagraphText", m_strParagraphText),
                            new SqlParameter("@strPathToImages", m_strPathToImages),
                            new SqlParameter("@strMasterImage", m_strMasterImage)
                    };
                    //### Update
                    clsDataAccess.Execute("spFinalMessagesUpdate", sqlParametersUpdate);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Delete(int iFinalMessageID)
        {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iFinalMessageID", iFinalMessageID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spFinalMessagesDelete", sqlParameter);
        }

        public static DataTable GetFinalMessagesList()
        {
            SqlParameter[] EmptySqlParameter = new SqlParameter[]{};
            return clsDataAccess.GetDataTable("spFinalMessagesList", EmptySqlParameter);
        }
        public static DataTable GetFinalMessagesList(string strFilterExpression, string strSortExpression)
        {
            DataView dvFinalMessagesList = new DataView();

            SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
            dvFinalMessagesList = clsDataAccess.GetDataView("spFinalMessagesList", EmptySqlParameter);
            dvFinalMessagesList.RowFilter = strFilterExpression;
            dvFinalMessagesList.Sort = strSortExpression;

            return dvFinalMessagesList.ToTable();
        }
    #endregion

    #region PROTECTED METHODS

        protected virtual void GetData()
        {
            try
                {
                //### Populate
                SqlParameter[] sqlParameter = new SqlParameter[] 
                {
                    new SqlParameter("@iFinalMessageID", m_iFinalMessageID)
                };
                DataRow drRecord = clsDataAccess.GetRecord("spFinalMessagesGetRecord", sqlParameter);

                m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);
                m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);

                if (drRecord["dtEdited"] != DBNull.Value)
                   m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

                if (drRecord["iEditedBy"] != DBNull.Value)
                    m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);

                m_strHeaderText = drRecord["strHeaderText"].ToString();
                m_strParagraphText = drRecord["strParagraphText"].ToString();
                m_strPathToImages = drRecord["strPathToImages"].ToString();
                m_strMasterImage = drRecord["strMasterImage"].ToString();
                m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);
 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    #endregion
}