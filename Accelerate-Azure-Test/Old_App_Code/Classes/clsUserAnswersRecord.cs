
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsUserAnswers
/// </summary>
public class clsUserAnswers
{
    #region MEMBER VARIABLES

        private int m_iUserAnswerID;
        private DateTime m_dtAdded;
        private int m_iAddedBy;
        private DateTime m_dtEdited;
        private int m_iEditedBy;
        private String m_strAnswer;
        private int m_iAccountUserID;
        private int m_iMissionQuestionID;
        private int m_iMissionID;
        private bool m_bIsDeleted;
        
    #endregion 

    #region PROPERTIES

        public int iUserAnswerID
        {
            get
            {
                return m_iUserAnswerID;
            }
        }

        public DateTime dtAdded
        {
            get
            {
                return m_dtAdded;
            }
            set
            {
                m_dtAdded = value;
            }
        }

        public int iAddedBy
        {
            get
            {
                return m_iAddedBy;
            }
            set
            {
                m_iAddedBy = value;
            }
        }

        public DateTime dtEdited
        {
            get
            {
                return m_dtEdited;
            }
            set
            {
                m_dtEdited = value;
            }
        }

        public int iEditedBy
        {
            get
            {
                return m_iEditedBy;
            }
            set
            {
                m_iEditedBy = value;
            }
        }

        public String strAnswer
        {
            get
            {
                return m_strAnswer;
            }
            set
            {
                m_strAnswer = value;
            }
        }

        public int iAccountUserID
        {
            get
            {
                return m_iAccountUserID;
            }
            set
            {
                m_iAccountUserID = value;
            }
        }

        public int iMissionQuestionID
        {
            get
            {
                return m_iMissionQuestionID;
            }
            set
            {
                m_iMissionQuestionID = value;
            }
        }

        public int iMissionID
        {
            get
            {
                return m_iMissionID;
            }
            set
            {
                m_iMissionID = value;
            }
        }

        public bool bIsDeleted
        {
            get
            {
                return m_bIsDeleted;
            }
            set
            {
                m_bIsDeleted = value;
            }
        }

    
    #endregion
    
    #region CONSTRUCTORS

    public clsUserAnswers()
    {
        m_iUserAnswerID = 0;
    }

    public clsUserAnswers(int iUserAnswerID)
    {
        m_iUserAnswerID = iUserAnswerID;
        GetData();
    }

    #endregion

    #region PUBLIC METHODS

        public virtual void Update()
        {
            try
            {
                if (iUserAnswerID == 0)
                {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@dtAdded", m_dtAdded),
                        new SqlParameter("@iAddedBy", m_iAddedBy),
                        new SqlParameter("@strAnswer", m_strAnswer),
                        new SqlParameter("@iAccountUserID", m_iAccountUserID),
                        new SqlParameter("@iMissionQuestionID", m_iMissionQuestionID),
                        new SqlParameter("@iMissionID", m_iMissionID)                  
                  };

                  //### Add
                  m_iUserAnswerID = (int)clsDataAccess.ExecuteScalar("spUserAnswersInsert", sqlParametersInsert);                    
                    }
                    else
                    {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iUserAnswerID", m_iUserAnswerID),
                         new SqlParameter("@dtEdited", m_dtEdited),
                         new SqlParameter("@iEditedBy", m_iEditedBy),
                         new SqlParameter("@strAnswer", m_strAnswer),
                         new SqlParameter("@iAccountUserID", m_iAccountUserID),
                         new SqlParameter("@iMissionQuestionID", m_iMissionQuestionID),
                         new SqlParameter("@iMissionID", m_iMissionID)
                    };
                    //### Update
                    clsDataAccess.Execute("spUserAnswersUpdate", sqlParametersUpdate);
                    }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Delete(int iUserAnswerID)
        {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iUserAnswerID", iUserAnswerID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spUserAnswersDelete", sqlParameter);
        }

        public static DataTable GetUserAnswersList()
        {
            SqlParameter[] EmptySqlParameter = new SqlParameter[]{};
            return clsDataAccess.GetDataTable("spUserAnswersList", EmptySqlParameter);
        }
        public static DataTable GetUserAnswersList(string strFilterExpression, string strSortExpression)
        {
            DataView dvUserAnswersList = new DataView();

            SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
            dvUserAnswersList = clsDataAccess.GetDataView("spUserAnswersList", EmptySqlParameter);
            dvUserAnswersList.RowFilter = strFilterExpression;
            dvUserAnswersList.Sort = strSortExpression;

            return dvUserAnswersList.ToTable();
        }
    #endregion

    #region PROTECTED METHODS

        protected virtual void GetData()
        {
            try
                {
                    //### Populate
                    SqlParameter[] sqlParameter = new SqlParameter[] 
                        {
                            new SqlParameter("@iUserAnswerID", m_iUserAnswerID)
                        };
                DataRow drRecord = clsDataAccess.GetRecord("spUserAnswersGetRecord", sqlParameter);

                m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);
                m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);

                if (drRecord["dtEdited"] != DBNull.Value)
                   m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

             if (drRecord["iEditedBy"] != DBNull.Value)
               m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);

                m_strAnswer = drRecord["strAnswer"].ToString();
                m_iAccountUserID = Convert.ToInt32(drRecord["iAccountUserID"]);
                m_iMissionQuestionID = Convert.ToInt32(drRecord["iMissionQuestionID"]);
                m_iMissionID = Convert.ToInt32(drRecord["iMissionID"]);
                m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);
 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    #endregion
}