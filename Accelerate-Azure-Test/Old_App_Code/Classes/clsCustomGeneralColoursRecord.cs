
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsCustomGeneralColours
/// </summary>
public class clsCustomGeneralColours
{
    #region MEMBER VARIABLES

     private     int m_iCustomGeneralColourID;
     private     DateTime m_dtAdded;
     private     int m_iAddedBy;
     private     DateTime m_dtEdited;
     private     int m_iEditedBy;
     private     string m_strTitle;
     private     string m_strLink;
     private     string m_strProfileBackgroundColour;
     private     string m_strProfileColourText;
     private     string m_strRankBackgroundColour;
     private     string m_strRankTextColour;
     private     string m_strProgressBarSectionBackgroundColour;
     private     string m_strProgressBarFillerColour;
     private     string m_strInformationBarTopBackgroundColour;
     private     string m_strInformationBarTopTextColour;
     private     string m_strInformationBarBackgroundColour;
     private     string m_strInformationBarTextColour;
     private     string m_strTipsBackgroundColour;
     private     string m_strTipsTextColour;
     private     string m_strIntroductionBackgroundColour;
     private     string m_strIntroductionTextColour;
     private     string m_strMissionInfoBackgroundColour;
     private     string m_strMissionInfoTitleTextColour;
     private     string m_strMissionInfoTextColour;
     private     string m_strMissionInfoPointsColour;
     private     string m_strGeneralInfoBoxBackgroundColour;
     private     string m_strGeneralInfoBoxTextColour;
     private     string m_strGeneralInfoBoxHeaderBackgroundColour;
     private     string m_strGeneralInfoBoxHeaderTextColour;
     private     string m_strKnowledgeBaseBlockColour1;
     private     string m_strKnowledgeBaseBlockColour2;
     private     string m_strKnowledgeBaseBlockColour3;
    private string m_strKnowledgeBaseBlockTextColour1;
    private string m_strKnowledgeBaseBlockTextColour2;
    private string m_strKnowledgeBaseBlockTextColour3;
    private     string m_strKnowledgeBaseBlockLinkColour;
     private     string m_strMandatoryeLearningLinkColour;
     private     string m_strHomePointsBackgroundColour;
     private     string m_strHomePointsTextColour;
     private     string m_strShowcaseBackgroundColour;
     private     string m_strShowcaseTextColour;
     private     string m_strPhase1BackgroundColour;
     private     string m_strPhase1TextColour;
     private     string m_strPhase2BackgroundColour;
     private     string m_strPhase2TextColour;
     private     string m_strPhase3BackgroundColour;
     private     string m_strPhase3TextColour;
     private     string m_strContentBoxBackgroundColour;
     private     string m_strContentBoxTextColour;
     private     string m_strTopAchieversTextColour;
     private     string m_strContentBoxHeaderBackgroundColour;
     private     string m_strContentBoxHeaderTextColour;
     private     string m_strMissionsCompleteTextColour;
     private     string m_strButtonBackgroundColour;
    private string m_strButtonTextColour;
     private     string m_strNavigationBarBackgroundColour;
     private     string m_strNavigationBarButtonsColour;
    private string m_strProfileTextBoxBackgroundColour;
    private string m_strProfileTextBoxTextColour;
    private string m_strMissionTextBoxBackgroundColour;
    private string m_strMissionTextBoxTextColour;
    private     bool m_bIsDeleted;
        
    #endregion 

    #region PROPERTIES

    public int iCustomGeneralColourID
    {
        get
        {
            return m_iCustomGeneralColourID;
        }
    }

    public    DateTime dtAdded
    {
        get
        {
            return m_dtAdded;
        }
        set
        {
            m_dtAdded = value;
        }
    }

    public    int iAddedBy
    {
        get
        {
            return m_iAddedBy;
        }
        set
        {
            m_iAddedBy = value;
        }
    }

    public    DateTime dtEdited
    {
        get
        {
            return m_dtEdited;
        }
        set
        {
            m_dtEdited = value;
        }
    }

    public    int iEditedBy
    {
        get
        {
            return m_iEditedBy;
        }
        set
        {
            m_iEditedBy = value;
        }
    }

    public    string strTitle
    {
        get
        {
            return m_strTitle;
        }
        set
        {
            m_strTitle = value;
        }
    }

    public    string strLink
    {
        get
        {
            return m_strLink;
        }
        set
        {
            m_strLink = value;
        }
    }

    public    string strProfileBackgroundColour
    {
        get
        {
            return m_strProfileBackgroundColour;
        }
        set
        {
            m_strProfileBackgroundColour = value;
        }
    }

    public    string strProfileColourText
    {
        get
        {
            return m_strProfileColourText;
        }
        set
        {
            m_strProfileColourText = value;
        }
    }

    public    string strRankBackgroundColour
    {
        get
        {
            return m_strRankBackgroundColour;
        }
        set
        {
            m_strRankBackgroundColour = value;
        }
    }

    public    string strRankTextColour
    {
        get
        {
            return m_strRankTextColour;
        }
        set
        {
            m_strRankTextColour = value;
        }
    }

    public    string strProgressBarSectionBackgroundColour
    {
        get
        {
            return m_strProgressBarSectionBackgroundColour;
        }
        set
        {
            m_strProgressBarSectionBackgroundColour = value;
        }
    }

    public    string strProgressBarFillerColour
    {
        get
        {
            return m_strProgressBarFillerColour;
        }
        set
        {
            m_strProgressBarFillerColour = value;
        }
    }

    public    string strInformationBarTopBackgroundColour
    {
        get
        {
            return m_strInformationBarTopBackgroundColour;
        }
        set
        {
            m_strInformationBarTopBackgroundColour = value;
        }
    }

    public    string strInformationBarTopTextColour
    {
        get
        {
            return m_strInformationBarTopTextColour;
        }
        set
        {
            m_strInformationBarTopTextColour = value;
        }
    }

    public    string strInformationBarBackgroundColour
    {
        get
        {
            return m_strInformationBarBackgroundColour;
        }
        set
        {
            m_strInformationBarBackgroundColour = value;
        }
    }

    public    string strInformationBarTextColour
    {
        get
        {
            return m_strInformationBarTextColour;
        }
        set
        {
            m_strInformationBarTextColour = value;
        }
    }

    public    string strTipsBackgroundColour
    {
        get
        {
            return m_strTipsBackgroundColour;
        }
        set
        {
            m_strTipsBackgroundColour = value;
        }
    }

    public    string strTipsTextColour
    {
        get
        {
            return m_strTipsTextColour;
        }
        set
        {
            m_strTipsTextColour = value;
        }
    }

    public    string strIntroductionBackgroundColour
    {
        get
        {
            return m_strIntroductionBackgroundColour;
        }
        set
        {
            m_strIntroductionBackgroundColour = value;
        }
    }

    public    string strIntroductionTextColour
    {
        get
        {
            return m_strIntroductionTextColour;
        }
        set
        {
            m_strIntroductionTextColour = value;
        }
    }

    public    string strMissionInfoBackgroundColour
    {
        get
        {
            return m_strMissionInfoBackgroundColour;
        }
        set
        {
            m_strMissionInfoBackgroundColour = value;
        }
    }

    public    string strMissionInfoTitleTextColour
    {
        get
        {
            return m_strMissionInfoTitleTextColour;
        }
        set
        {
            m_strMissionInfoTitleTextColour = value;
        }
    }

    public    string strMissionInfoTextColour
    {
        get
        {
            return m_strMissionInfoTextColour;
        }
        set
        {
            m_strMissionInfoTextColour = value;
        }
    }

    public    string strMissionInfoPointsColour
    {
        get
        {
            return m_strMissionInfoPointsColour;
        }
        set
        {
            m_strMissionInfoPointsColour = value;
        }
    }

    public    string strGeneralInfoBoxBackgroundColour
    {
        get
        {
            return m_strGeneralInfoBoxBackgroundColour;
        }
        set
        {
            m_strGeneralInfoBoxBackgroundColour = value;
        }
    }

    public    string strGeneralInfoBoxTextColour
    {
        get
        {
            return m_strGeneralInfoBoxTextColour;
        }
        set
        {
            m_strGeneralInfoBoxTextColour = value;
        }
    }

    public    string strGeneralInfoBoxHeaderBackgroundColour
    {
        get
        {
            return m_strGeneralInfoBoxHeaderBackgroundColour;
        }
        set
        {
            m_strGeneralInfoBoxHeaderBackgroundColour = value;
        }
    }

    public    string strGeneralInfoBoxHeaderTextColour
    {
        get
        {
            return m_strGeneralInfoBoxHeaderTextColour;
        }
        set
        {
            m_strGeneralInfoBoxHeaderTextColour = value;
        }
    }

    public    string strKnowledgeBaseBlockColour1
    {
        get
        {
            return m_strKnowledgeBaseBlockColour1;
        }
        set
        {
            m_strKnowledgeBaseBlockColour1 = value;
        }
    }

    public    string strKnowledgeBaseBlockColour2
    {
        get
        {
            return m_strKnowledgeBaseBlockColour2;
        }
        set
        {
            m_strKnowledgeBaseBlockColour2 = value;
        }
    }

    public    string strKnowledgeBaseBlockColour3
    {
        get
        {
            return m_strKnowledgeBaseBlockColour3;
        }
        set
        {
            m_strKnowledgeBaseBlockColour3 = value;
        }
    }
    
        public string strKnowledgeBaseBlockTextColour1
    {
        get
        {
            return m_strKnowledgeBaseBlockTextColour1;
        }
        set
        {
            m_strKnowledgeBaseBlockTextColour1 = value;
        }
    }

    public string strKnowledgeBaseBlockTextColour2
    {
        get
        {
            return m_strKnowledgeBaseBlockTextColour2;
        }
        set
        {
            m_strKnowledgeBaseBlockTextColour2 = value;
        }
    }

    public string strKnowledgeBaseBlockTextColour3
    {
        get
        {
            return m_strKnowledgeBaseBlockTextColour3;
        }
        set
        {
            m_strKnowledgeBaseBlockTextColour3 = value;
        }
    }
    public    string strKnowledgeBaseBlockLinkColour
    {
        get
        {
            return m_strKnowledgeBaseBlockLinkColour;
        }
        set
        {
            m_strKnowledgeBaseBlockLinkColour = value;
        }
    }

    public    string strMandatoryeLearningLinkColour
    {
        get
        {
            return m_strMandatoryeLearningLinkColour;
        }
        set
        {
            m_strMandatoryeLearningLinkColour = value;
        }
    }

    public    string strHomePointsBackgroundColour
    {
        get
        {
            return m_strHomePointsBackgroundColour;
        }
        set
        {
            m_strHomePointsBackgroundColour = value;
        }
    }

    public    string strHomePointsTextColour
    {
        get
        {
            return m_strHomePointsTextColour;
        }
        set
        {
            m_strHomePointsTextColour = value;
        }
    }

    public    string strShowcaseBackgroundColour
    {
        get
        {
            return m_strShowcaseBackgroundColour;
        }
        set
        {
            m_strShowcaseBackgroundColour = value;
        }
    }

    public    string strShowcaseTextColour
    {
        get
        {
            return m_strShowcaseTextColour;
        }
        set
        {
            m_strShowcaseTextColour = value;
        }
    }

    public    string strPhase1BackgroundColour
    {
        get
        {
            return m_strPhase1BackgroundColour;
        }
        set
        {
            m_strPhase1BackgroundColour = value;
        }
    }

    public    string strPhase1TextColour
    {
        get
        {
            return m_strPhase1TextColour;
        }
        set
        {
            m_strPhase1TextColour = value;
        }
    }

    public    string strPhase2BackgroundColour
    {
        get
        {
            return m_strPhase2BackgroundColour;
        }
        set
        {
            m_strPhase2BackgroundColour = value;
        }
    }

    public    string strPhase2TextColour
    {
        get
        {
            return m_strPhase2TextColour;
        }
        set
        {
            m_strPhase2TextColour = value;
        }
    }

    public    string strPhase3BackgroundColour
    {
        get
        {
            return m_strPhase3BackgroundColour;
        }
        set
        {
            m_strPhase3BackgroundColour = value;
        }
    }

    public    string strPhase3TextColour
    {
        get
        {
            return m_strPhase3TextColour;
        }
        set
        {
            m_strPhase3TextColour = value;
        }
    }

    public    string strContentBoxBackgroundColour
    {
        get
        {
            return m_strContentBoxBackgroundColour;
        }
        set
        {
            m_strContentBoxBackgroundColour = value;
        }
    }

    public    string strContentBoxTextColour
    {
        get
        {
            return m_strContentBoxTextColour;
        }
        set
        {
            m_strContentBoxTextColour = value;
        }
    }

    public    string strTopAchieversTextColour
    {
        get
        {
            return m_strTopAchieversTextColour;
        }
        set
        {
            m_strTopAchieversTextColour = value;
        }
    }

    public    string strContentBoxHeaderBackgroundColour
    {
        get
        {
            return m_strContentBoxHeaderBackgroundColour;
        }
        set
        {
            m_strContentBoxHeaderBackgroundColour = value;
        }
    }

    public    string strContentBoxHeaderTextColour
    {
        get
        {
            return m_strContentBoxHeaderTextColour;
        }
        set
        {
            m_strContentBoxHeaderTextColour = value;
        }
    }

    public    string strMissionsCompleteTextColour
    {
        get
        {
            return m_strMissionsCompleteTextColour;
        }
        set
        {
            m_strMissionsCompleteTextColour = value;
        }
    }

    public    string strButtonBackgroundColour
    {
        get
        {
            return m_strButtonBackgroundColour;
        }
        set
        {
            m_strButtonBackgroundColour = value;
        }
    }

    public string strButtonTextColour
    {
        get
        {
            return m_strButtonTextColour;
        }
        set
        {
            m_strButtonTextColour = value;
        }
    }

    public    string strNavigationBarBackgroundColour
    {
        get
        {
            return m_strNavigationBarBackgroundColour;
        }
        set
        {
            m_strNavigationBarBackgroundColour = value;
        }
    }
    
    public    string strNavigationBarButtonsColour 
    {
        get
        {
            return m_strNavigationBarButtonsColour;
        }
        set
        {
            m_strNavigationBarButtonsColour = value;
        }
    }

    public string strProfileTextBoxBackgroundColour
    {
        get
        {
            return m_strProfileTextBoxBackgroundColour;
        }
        set
        {
            m_strProfileTextBoxBackgroundColour = value;
        }
    }

    public string strProfileTextBoxTextColour
    {
        get
        {
            return m_strProfileTextBoxTextColour;
        }
        set
        {
            m_strProfileTextBoxTextColour = value;
        }
    }

    public string strMissionTextBoxBackgroundColour
    {
        get
        {
            return m_strMissionTextBoxBackgroundColour;
        }
        set
        {
            m_strMissionTextBoxBackgroundColour = value;
        }
    }

    public string strMissionTextBoxTextColour
    {
        get
        {
            return m_strMissionTextBoxTextColour;
        }
        set
        {
            m_strMissionTextBoxTextColour = value;
        }
    }

    public    bool bIsDeleted
    {
        get
        {
            return m_bIsDeleted;
        }
        set
        {
            m_bIsDeleted = value;
        }
    }

    
    #endregion
    
    #region CONSTRUCTORS

    public clsCustomGeneralColours()
    {
        m_iCustomGeneralColourID = 0;
    }

    public clsCustomGeneralColours(int iCustomGeneralColourID)
    {
        m_iCustomGeneralColourID = iCustomGeneralColourID;
        GetData();
    }

    #endregion

    #region PUBLIC METHODS

        public virtual void Update()
        {
            try
            {
                if (iCustomGeneralColourID == 0)
                {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@dtAdded", m_dtAdded),
                        new SqlParameter("@iAddedBy", m_iAddedBy),
                        new SqlParameter("@strTitle", m_strTitle),
                        new SqlParameter("@strLink", m_strLink),
                        new SqlParameter("@strProfileBackgroundColour", m_strProfileBackgroundColour),
                        new SqlParameter("@strProfileColourText", m_strProfileColourText),
                        new SqlParameter("@strRankBackgroundColour", m_strRankBackgroundColour),
                        new SqlParameter("@strRankTextColour", m_strRankTextColour),
                        new SqlParameter("@strProgressBarSectionBackgroundColour", m_strProgressBarSectionBackgroundColour),
                        new SqlParameter("@strProgressBarFillerColour", m_strProgressBarFillerColour),
                        new SqlParameter("@strInformationBarTopBackgroundColour", m_strInformationBarTopBackgroundColour),
                        new SqlParameter("@strInformationBarTopTextColour", m_strInformationBarTopTextColour),
                        new SqlParameter("@strInformationBarBackgroundColour", m_strInformationBarBackgroundColour),
                        new SqlParameter("@strInformationBarTextColour", m_strInformationBarTextColour),
                        new SqlParameter("@strTipsBackgroundColour", m_strTipsBackgroundColour),
                        new SqlParameter("@strTipsTextColour", m_strTipsTextColour),
                        new SqlParameter("@strIntroductionBackgroundColour", m_strIntroductionBackgroundColour),
                        new SqlParameter("@strIntroductionTextColour", m_strIntroductionTextColour),
                        new SqlParameter("@strMissionInfoBackgroundColour", m_strMissionInfoBackgroundColour),
                        new SqlParameter("@strMissionInfoTitleTextColour", m_strMissionInfoTitleTextColour),
                        new SqlParameter("@strMissionInfoTextColour", m_strMissionInfoTextColour),
                        new SqlParameter("@strMissionInfoPointsColour", m_strMissionInfoPointsColour),
                        new SqlParameter("@strGeneralInfoBoxBackgroundColour", m_strGeneralInfoBoxBackgroundColour),
                        new SqlParameter("@strGeneralInfoBoxTextColour", m_strGeneralInfoBoxTextColour),
                        new SqlParameter("@strGeneralInfoBoxHeaderBackgroundColour", m_strGeneralInfoBoxHeaderBackgroundColour),
                        new SqlParameter("@strGeneralInfoBoxHeaderTextColour", m_strGeneralInfoBoxHeaderTextColour),
                        new SqlParameter("@strKnowledgeBaseBlockColour1", m_strKnowledgeBaseBlockColour1),
                        new SqlParameter("@strKnowledgeBaseBlockColour2", m_strKnowledgeBaseBlockColour2),
                        new SqlParameter("@strKnowledgeBaseBlockColour3", m_strKnowledgeBaseBlockColour3),
                        new SqlParameter("@strKnowledgeBaseBlockTextColour1", m_strKnowledgeBaseBlockTextColour1),
                        new SqlParameter("@strKnowledgeBaseBlockTextColour2", m_strKnowledgeBaseBlockTextColour2),
                        new SqlParameter("@strKnowledgeBaseBlockTextColour3", m_strKnowledgeBaseBlockTextColour3),
                        new SqlParameter("@strKnowledgeBaseBlockLinkColour", m_strKnowledgeBaseBlockLinkColour),
                        new SqlParameter("@strMandatoryeLearningLinkColour", m_strMandatoryeLearningLinkColour),
                        new SqlParameter("@strHomePointsBackgroundColour", m_strHomePointsBackgroundColour),
                        new SqlParameter("@strHomePointsTextColour", m_strHomePointsTextColour),
                        new SqlParameter("@strShowcaseBackgroundColour", m_strShowcaseBackgroundColour),
                        new SqlParameter("@strShowcaseTextColour", m_strShowcaseTextColour),
                        new SqlParameter("@strPhase1BackgroundColour", m_strPhase1BackgroundColour),
                        new SqlParameter("@strPhase1TextColour", m_strPhase1TextColour),
                        new SqlParameter("@strPhase2BackgroundColour", m_strPhase2BackgroundColour),
                        new SqlParameter("@strPhase2TextColour", m_strPhase2TextColour),
                        new SqlParameter("@strPhase3BackgroundColour", m_strPhase3BackgroundColour),
                        new SqlParameter("@strPhase3TextColour", m_strPhase3TextColour),
                        new SqlParameter("@strContentBoxBackgroundColour", m_strContentBoxBackgroundColour),
                        new SqlParameter("@strContentBoxTextColour", m_strContentBoxTextColour),
                        new SqlParameter("@strTopAchieversTextColour", m_strTopAchieversTextColour),
                        new SqlParameter("@strContentBoxHeaderBackgroundColour", m_strContentBoxHeaderBackgroundColour),
                        new SqlParameter("@strContentBoxHeaderTextColour", m_strContentBoxHeaderTextColour),
                        new SqlParameter("@strMissionsCompleteTextColour", m_strMissionsCompleteTextColour),
                        new SqlParameter("@strButtonBackgroundColour", m_strButtonBackgroundColour),
                        new SqlParameter("@strButtonTextColour", m_strButtonTextColour),
                        new SqlParameter("@strNavigationBarBackgroundColour", m_strNavigationBarBackgroundColour),
                        new SqlParameter("@strNavigationBarButtonsColour", m_strNavigationBarButtonsColour),
                        new SqlParameter("@strProfileTextBoxBackgroundColour", m_strProfileTextBoxBackgroundColour),
                        new SqlParameter("@strProfileTextBoxTextColour", m_strProfileTextBoxTextColour),
                        new SqlParameter("@strMissionTextBoxBackgroundColour", m_strMissionTextBoxBackgroundColour),
                        new SqlParameter("@strMissionTextBoxTextColour", m_strMissionTextBoxTextColour)
                  };

                  //### Add
                  m_iCustomGeneralColourID = (int)clsDataAccess.ExecuteScalar("spCustomGeneralColoursInsert", sqlParametersInsert);                    
                    }
                    else
                    {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iCustomGeneralColourID", m_iCustomGeneralColourID),
                         new SqlParameter("@dtEdited", m_dtEdited),
                         new SqlParameter("@iEditedBy", m_iEditedBy),
                         new SqlParameter("@strTitle", m_strTitle),
                         new SqlParameter("@strLink", m_strLink),
                         new SqlParameter("@strProfileBackgroundColour", m_strProfileBackgroundColour),
                         new SqlParameter("@strProfileColourText", m_strProfileColourText),
                         new SqlParameter("@strRankBackgroundColour", m_strRankBackgroundColour),
                         new SqlParameter("@strRankTextColour", m_strRankTextColour),
                         new SqlParameter("@strProgressBarSectionBackgroundColour", m_strProgressBarSectionBackgroundColour),
                         new SqlParameter("@strProgressBarFillerColour", m_strProgressBarFillerColour),
                         new SqlParameter("@strInformationBarTopBackgroundColour", m_strInformationBarTopBackgroundColour),
                         new SqlParameter("@strInformationBarTopTextColour", m_strInformationBarTopTextColour),
                         new SqlParameter("@strInformationBarBackgroundColour", m_strInformationBarBackgroundColour),
                         new SqlParameter("@strInformationBarTextColour", m_strInformationBarTextColour),
                         new SqlParameter("@strTipsBackgroundColour", m_strTipsBackgroundColour),
                         new SqlParameter("@strTipsTextColour", m_strTipsTextColour),
                         new SqlParameter("@strIntroductionBackgroundColour", m_strIntroductionBackgroundColour),
                         new SqlParameter("@strIntroductionTextColour", m_strIntroductionTextColour),
                         new SqlParameter("@strMissionInfoBackgroundColour", m_strMissionInfoBackgroundColour),
                         new SqlParameter("@strMissionInfoTitleTextColour", m_strMissionInfoTitleTextColour),
                         new SqlParameter("@strMissionInfoTextColour", m_strMissionInfoTextColour),
                         new SqlParameter("@strMissionInfoPointsColour", m_strMissionInfoPointsColour),
                         new SqlParameter("@strGeneralInfoBoxBackgroundColour", m_strGeneralInfoBoxBackgroundColour),
                         new SqlParameter("@strGeneralInfoBoxTextColour", m_strGeneralInfoBoxTextColour),
                         new SqlParameter("@strGeneralInfoBoxHeaderBackgroundColour", m_strGeneralInfoBoxHeaderBackgroundColour),
                         new SqlParameter("@strGeneralInfoBoxHeaderTextColour", m_strGeneralInfoBoxHeaderTextColour),
                         new SqlParameter("@strKnowledgeBaseBlockColour1", m_strKnowledgeBaseBlockColour1),
                         new SqlParameter("@strKnowledgeBaseBlockColour2", m_strKnowledgeBaseBlockColour2),
                         new SqlParameter("@strKnowledgeBaseBlockColour3", m_strKnowledgeBaseBlockColour3),
                         new SqlParameter("@strKnowledgeBaseBlockTextColour1", m_strKnowledgeBaseBlockTextColour1),
                        new SqlParameter("@strKnowledgeBaseBlockTextColour2", m_strKnowledgeBaseBlockTextColour2),
                        new SqlParameter("@strKnowledgeBaseBlockTextColour3", m_strKnowledgeBaseBlockTextColour3),
                         new SqlParameter("@strKnowledgeBaseBlockLinkColour", m_strKnowledgeBaseBlockLinkColour),
                         new SqlParameter("@strMandatoryeLearningLinkColour", m_strMandatoryeLearningLinkColour),
                         new SqlParameter("@strHomePointsBackgroundColour", m_strHomePointsBackgroundColour),
                         new SqlParameter("@strHomePointsTextColour", m_strHomePointsTextColour),
                         new SqlParameter("@strShowcaseBackgroundColour", m_strShowcaseBackgroundColour),
                         new SqlParameter("@strShowcaseTextColour", m_strShowcaseTextColour),
                         new SqlParameter("@strPhase1BackgroundColour", m_strPhase1BackgroundColour),
                         new SqlParameter("@strPhase1TextColour", m_strPhase1TextColour),
                         new SqlParameter("@strPhase2BackgroundColour", m_strPhase2BackgroundColour),
                         new SqlParameter("@strPhase2TextColour", m_strPhase2TextColour),
                         new SqlParameter("@strPhase3BackgroundColour", m_strPhase3BackgroundColour),
                         new SqlParameter("@strPhase3TextColour", m_strPhase3TextColour),
                         new SqlParameter("@strContentBoxBackgroundColour", m_strContentBoxBackgroundColour),
                         new SqlParameter("@strContentBoxTextColour", m_strContentBoxTextColour),
                         new SqlParameter("@strTopAchieversTextColour", m_strTopAchieversTextColour),
                         new SqlParameter("@strContentBoxHeaderBackgroundColour", m_strContentBoxHeaderBackgroundColour),
                         new SqlParameter("@strContentBoxHeaderTextColour", m_strContentBoxHeaderTextColour),
                         new SqlParameter("@strMissionsCompleteTextColour", m_strMissionsCompleteTextColour),
                         new SqlParameter("@strButtonBackgroundColour", m_strButtonBackgroundColour),
                        new SqlParameter("@strButtonTextColour", m_strButtonTextColour),
                         new SqlParameter("@strNavigationBarBackgroundColour", m_strNavigationBarBackgroundColour),
                         new SqlParameter("@strNavigationBarButtonsColour", m_strNavigationBarButtonsColour),
                         new SqlParameter("@strProfileTextBoxBackgroundColour", m_strProfileTextBoxBackgroundColour),
                        new SqlParameter("@strProfileTextBoxTextColour", m_strProfileTextBoxTextColour),
                        new SqlParameter("@strMissionTextBoxBackgroundColour", m_strMissionTextBoxBackgroundColour),
                        new SqlParameter("@strMissionTextBoxTextColour", m_strMissionTextBoxTextColour)
          };
          //### Update
          clsDataAccess.Execute("spCustomGeneralColoursUpdate", sqlParametersUpdate);
                    }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Delete(int iCustomGeneralColourID)
        {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iCustomGeneralColourID", iCustomGeneralColourID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spCustomGeneralColoursDelete", sqlParameter);
        }

        public static DataTable GetCustomGeneralColoursList()
        {
            SqlParameter[] EmptySqlParameter = new SqlParameter[]{};
            return clsDataAccess.GetDataTable("spCustomGeneralColoursList", EmptySqlParameter);
        }
        public static DataTable GetCustomGeneralColoursList(string strFilterExpression, string strSortExpression)
        {
            DataView dvCustomGeneralColoursList = new DataView();

            SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
            dvCustomGeneralColoursList = clsDataAccess.GetDataView("spCustomGeneralColoursList", EmptySqlParameter);
            dvCustomGeneralColoursList.RowFilter = strFilterExpression;
            dvCustomGeneralColoursList.Sort = strSortExpression;

            return dvCustomGeneralColoursList.ToTable();
        }
    #endregion

    #region PROTECTED METHODS

        protected virtual void GetData()
        {
            try
                {
                    //### Populate
                    SqlParameter[] sqlParameter = new SqlParameter[] 
                        {
                            new SqlParameter("@iCustomGeneralColourID", m_iCustomGeneralColourID)
                        };
                DataRow drRecord = clsDataAccess.GetRecord("spCustomGeneralColoursGetRecord", sqlParameter);

               m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);
               m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);

           if (drRecord["dtEdited"] != DBNull.Value)
               m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

           if (drRecord["iEditedBy"] != DBNull.Value)
               m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);

                   m_strTitle = drRecord["strTitle"].ToString();
                   m_strLink = drRecord["strLink"].ToString();
                   m_strProfileBackgroundColour = drRecord["strProfileBackgroundColour"].ToString();
                   m_strProfileColourText = drRecord["strProfileColourText"].ToString();
                   m_strRankBackgroundColour = drRecord["strRankBackgroundColour"].ToString();
                   m_strRankTextColour = drRecord["strRankTextColour"].ToString();
                   m_strProgressBarSectionBackgroundColour = drRecord["strProgressBarSectionBackgroundColour"].ToString();
                   m_strProgressBarFillerColour = drRecord["strProgressBarFillerColour"].ToString();
                   m_strInformationBarTopBackgroundColour = drRecord["strInformationBarTopBackgroundColour"].ToString();
                   m_strInformationBarTopTextColour = drRecord["strInformationBarTopTextColour"].ToString();
                   m_strInformationBarBackgroundColour = drRecord["strInformationBarBackgroundColour"].ToString();
                   m_strInformationBarTextColour = drRecord["strInformationBarTextColour"].ToString();
                   m_strTipsBackgroundColour = drRecord["strTipsBackgroundColour"].ToString();
                   m_strTipsTextColour = drRecord["strTipsTextColour"].ToString();
                   m_strIntroductionBackgroundColour = drRecord["strIntroductionBackgroundColour"].ToString();
                   m_strIntroductionTextColour = drRecord["strIntroductionTextColour"].ToString();
                   m_strMissionInfoBackgroundColour = drRecord["strMissionInfoBackgroundColour"].ToString();
                   m_strMissionInfoTitleTextColour = drRecord["strMissionInfoTitleTextColour"].ToString();
                   m_strMissionInfoTextColour = drRecord["strMissionInfoTextColour"].ToString();
                   m_strMissionInfoPointsColour = drRecord["strMissionInfoPointsColour"].ToString();
                   m_strGeneralInfoBoxBackgroundColour = drRecord["strGeneralInfoBoxBackgroundColour"].ToString();
                   m_strGeneralInfoBoxTextColour = drRecord["strGeneralInfoBoxTextColour"].ToString();
                   m_strGeneralInfoBoxHeaderBackgroundColour = drRecord["strGeneralInfoBoxHeaderBackgroundColour"].ToString();
                   m_strGeneralInfoBoxHeaderTextColour = drRecord["strGeneralInfoBoxHeaderTextColour"].ToString();
                   m_strKnowledgeBaseBlockColour1 = drRecord["strKnowledgeBaseBlockColour1"].ToString();
                   m_strKnowledgeBaseBlockColour2 = drRecord["strKnowledgeBaseBlockColour2"].ToString(); 
                    m_strKnowledgeBaseBlockColour3 = drRecord["strKnowledgeBaseBlockColour3"].ToString();
            m_strKnowledgeBaseBlockTextColour1 = drRecord["strKnowledgeBaseBlockTextColour1"].ToString();
            m_strKnowledgeBaseBlockTextColour2 = drRecord["strKnowledgeBaseBlockTextColour2"].ToString();
            m_strKnowledgeBaseBlockTextColour3 = drRecord["strKnowledgeBaseBlockTextColour3"].ToString();
            m_strKnowledgeBaseBlockLinkColour = drRecord["strKnowledgeBaseBlockLinkColour"].ToString();
                   m_strMandatoryeLearningLinkColour = drRecord["strMandatoryeLearningLinkColour"].ToString();
                   m_strHomePointsBackgroundColour = drRecord["strHomePointsBackgroundColour"].ToString();
                   m_strHomePointsTextColour = drRecord["strHomePointsTextColour"].ToString();
                   m_strShowcaseBackgroundColour = drRecord["strShowcaseBackgroundColour"].ToString();
                   m_strShowcaseTextColour = drRecord["strShowcaseTextColour"].ToString();
                   m_strPhase1BackgroundColour = drRecord["strPhase1BackgroundColour"].ToString();
                   m_strPhase1TextColour = drRecord["strPhase1TextColour"].ToString();
                   m_strPhase2BackgroundColour = drRecord["strPhase2BackgroundColour"].ToString();
                   m_strPhase2TextColour = drRecord["strPhase2TextColour"].ToString();
                   m_strPhase3BackgroundColour = drRecord["strPhase3BackgroundColour"].ToString();
                   m_strPhase3TextColour = drRecord["strPhase3TextColour"].ToString();
                   m_strContentBoxBackgroundColour = drRecord["strContentBoxBackgroundColour"].ToString();
                   m_strContentBoxTextColour = drRecord["strContentBoxTextColour"].ToString();
                   m_strTopAchieversTextColour = drRecord["strTopAchieversTextColour"].ToString();
                   m_strContentBoxHeaderBackgroundColour = drRecord["strContentBoxHeaderBackgroundColour"].ToString();
                   m_strContentBoxHeaderTextColour = drRecord["strContentBoxHeaderTextColour"].ToString();
                   m_strMissionsCompleteTextColour = drRecord["strMissionsCompleteTextColour"].ToString();
            m_strButtonBackgroundColour = drRecord["strButtonBackgroundColour"].ToString();
            m_strButtonTextColour = drRecord["strButtonTextColour"].ToString();
            m_strNavigationBarBackgroundColour = drRecord["strNavigationBarBackgroundColour"].ToString(); 
                   m_strNavigationBarButtonsColour = drRecord["strNavigationBarButtonsColour"].ToString();
            m_strProfileTextBoxBackgroundColour = drRecord["strProfileTextBoxBackgroundColour"].ToString();
            m_strProfileTextBoxTextColour = drRecord["strProfileTextBoxTextColour"].ToString();
            m_strMissionTextBoxBackgroundColour = drRecord["strMissionTextBoxBackgroundColour"].ToString();
            m_strMissionTextBoxTextColour = drRecord["strMissionTextBoxTextColour"].ToString();
            m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);
 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    #endregion
}