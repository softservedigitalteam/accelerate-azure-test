
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsInactiveAccountUsers
/// </summary>
public class clsInactiveAccountUsers
{
    #region MEMBER VARIABLES

     private     int m_iInactiveAccountUserID;
     private     DateTime m_dtAdded;
     private     int m_iAddedBy;
     private     DateTime m_dtEdited;
     private     int m_iEditedBy;
     private     string m_strFirstName;
     private     string m_strSurname;
     private     string m_strEmailAddress;
     private     string m_strPhoneNumber;
     private     string m_strJobTitle;
     private     int m_iGroupID;
     private     bool m_bIsDeleted;
        
    #endregion 

    #region PROPERTIES

    public int iInactiveAccountUserID
    {
        get
        {
            return m_iInactiveAccountUserID;
        }
    }

    public    DateTime dtAdded
    {
        get
        {
            return m_dtAdded;
        }
        set
        {
            m_dtAdded = value;
        }
    }

    public    int iAddedBy
    {
        get
        {
            return m_iAddedBy;
        }
        set
        {
            m_iAddedBy = value;
        }
    }

    public    DateTime dtEdited
    {
        get
        {
            return m_dtEdited;
        }
        set
        {
            m_dtEdited = value;
        }
    }

    public    int iEditedBy
    {
        get
        {
            return m_iEditedBy;
        }
        set
        {
            m_iEditedBy = value;
        }
    }

    public    string strFirstName
    {
        get
        {
            return m_strFirstName;
        }
        set
        {
            m_strFirstName = value;
        }
    }

    public    string strSurname
    {
        get
        {
            return m_strSurname;
        }
        set
        {
            m_strSurname = value;
        }
    }

    public    string strEmailAddress
    {
        get
        {
            return m_strEmailAddress;
        }
        set
        {
            m_strEmailAddress = value;
        }
    }

    public    string strPhoneNumber
    {
        get
        {
            return m_strPhoneNumber;
        }
        set
        {
            m_strPhoneNumber = value;
        }
    }

    public    string strJobTitle
    {
        get
        {
            return m_strJobTitle;
        }
        set
        {
            m_strJobTitle = value;
        }
    }

    public    int iGroupID
    {
        get
        {
            return m_iGroupID;
        }
        set
        {
            m_iGroupID = value;
        }
    }

    public    bool bIsDeleted
    {
        get
        {
            return m_bIsDeleted;
        }
        set
        {
            m_bIsDeleted = value;
        }
    }

    
    #endregion
    
    #region CONSTRUCTORS

    public clsInactiveAccountUsers()
    {
        m_iInactiveAccountUserID = 0;
    }

    public clsInactiveAccountUsers(int iInactiveAccountUserID)
    {
        m_iInactiveAccountUserID = iInactiveAccountUserID;
        GetData();
    }

    #endregion

    #region PUBLIC METHODS

        public virtual void Update()
        {
            try
            {
                if (iInactiveAccountUserID == 0)
                {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@dtAdded", m_dtAdded),
                        new SqlParameter("@iAddedBy", m_iAddedBy),
                        new SqlParameter("@strFirstName", m_strFirstName),
                        new SqlParameter("@strSurname", m_strSurname),
                        new SqlParameter("@strEmailAddress", m_strEmailAddress),
                        new SqlParameter("@strPhoneNumber", m_strPhoneNumber),
                        new SqlParameter("@strJobTitle", m_strJobTitle),
                        new SqlParameter("@iGroupID", m_iGroupID)                  
                  };

                  //### Add
                  m_iInactiveAccountUserID = (int)clsDataAccess.ExecuteScalar("spInactiveAccountUsersInsert", sqlParametersInsert);                    
                    }
                    else
                    {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iInactiveAccountUserID", m_iInactiveAccountUserID),
                         new SqlParameter("@dtEdited", m_dtEdited),
                         new SqlParameter("@iEditedBy", m_iEditedBy),
                         new SqlParameter("@strFirstName", m_strFirstName),
                         new SqlParameter("@strSurname", m_strSurname),
                         new SqlParameter("@strEmailAddress", m_strEmailAddress),
                         new SqlParameter("@strPhoneNumber", m_strPhoneNumber),
                         new SqlParameter("@strJobTitle", m_strJobTitle),
                         new SqlParameter("@iGroupID", m_iGroupID)
          };
          //### Update
          clsDataAccess.Execute("spInactiveAccountUsersUpdate", sqlParametersUpdate);
                    }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Delete(int iInactiveAccountUserID)
        {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iInactiveAccountUserID", iInactiveAccountUserID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spInactiveAccountUsersDelete", sqlParameter);
        }

        public static DataTable GetInactiveAccountUsersList()
        {
            SqlParameter[] EmptySqlParameter = new SqlParameter[]{};
            return clsDataAccess.GetDataTable("spInactiveAccountUsersList", EmptySqlParameter);
        }
        public static DataTable GetInactiveAccountUsersList(string strFilterExpression, string strSortExpression)
        {
            DataView dvInactiveAccountUsersList = new DataView();

            SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
            dvInactiveAccountUsersList = clsDataAccess.GetDataView("spInactiveAccountUsersList", EmptySqlParameter);
            dvInactiveAccountUsersList.RowFilter = strFilterExpression;
            dvInactiveAccountUsersList.Sort = strSortExpression;

            return dvInactiveAccountUsersList.ToTable();
        }
    #endregion

    #region PROTECTED METHODS

        protected virtual void GetData()
        {
            try
                {
                    //### Populate
                    SqlParameter[] sqlParameter = new SqlParameter[] 
                        {
                            new SqlParameter("@iInactiveAccountUserID", m_iInactiveAccountUserID)
                        };
                DataRow drRecord = clsDataAccess.GetRecord("spInactiveAccountUsersGetRecord", sqlParameter);

               m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);
               m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);

           if (drRecord["dtEdited"] != DBNull.Value)
               m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

           if (drRecord["iEditedBy"] != DBNull.Value)
               m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);

                   m_strFirstName = drRecord["strFirstName"].ToString();
                   m_strSurname = drRecord["strSurname"].ToString();
                   m_strEmailAddress = drRecord["strEmailAddress"].ToString();
                   m_strPhoneNumber = drRecord["strPhoneNumber"].ToString();
                   m_strJobTitle = drRecord["strJobTitle"].ToString();
               m_iGroupID = Convert.ToInt32(drRecord["iGroupID"]);
                   m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);
 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    #endregion
}