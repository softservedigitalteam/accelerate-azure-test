using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsAccountUsers
/// </summary>
public class clsAccountUserTrackings
{
    #region MEMBER VARIABLES

    private int m_iAccountUserTrackingsID;
    private DateTime m_dtAdded;
    private DateTime m_dtEdited;
    private int m_iCurrentPhaseLevel;
    private bool m_bIsPhase1Complete;
    private bool m_bIsPhase2Complete;
    private bool m_bIsPhase3Complete;
    private int m_iPhase1ID;
    private int m_iPhase2ID;
    private int m_iPhase3ID;

    //private int m_iMissionsAvailableInCurrentPhase;
    //private int m_iMissionsRemainingInCurrentPhase;
    private int m_iCurrentPhaseID;
    private bool m_bHasEmailBeenSent;
    private bool m_bIsDeleted;

    #endregion

    #region PROPERTIES


    public int iAccountUserTrackingID
    {
        get
        {
            return m_iAccountUserTrackingsID;
        }
    }

    public DateTime dtAdded
    {
        get
        {
            return m_dtAdded;
        }
        set
        {
            m_dtAdded = value;
        }
    }

 

    public DateTime dtEdited
    {
        get
        {
            return m_dtEdited;
        }
        set
        {
            m_dtEdited = value;
        }
    }


    public int iCurrentPhaseLevel
    {
        get
        {
            return m_iCurrentPhaseLevel;
        }
        set
        {
            m_iCurrentPhaseLevel = value;
        }
    }

    public bool bIsPhase1Complete
    {
        get
        {
            return m_bIsPhase1Complete;
        }
        set
        {
            m_bIsPhase1Complete = value;
        }
    }

    public bool bIsPhase2Complete
    {
        get
        {
            return m_bIsPhase2Complete;
        }
        set
        {
            m_bIsPhase2Complete = value;
        }
    }

    public bool bIsPhase3Complete
    {
        get
        {
            return m_bIsPhase3Complete;
        }
        set
        {
            m_bIsPhase3Complete = value;
        }
    }

    //public int iMissionsAvailableInCurrentPhase
    //{
    //    get
    //    {
    //        return m_iMissionsAvailableInCurrentPhase;
    //    }
    //    set
    //    {
    //        m_iMissionsAvailableInCurrentPhase = value;
    //    }
    //}

    public int iPhase2ID
    {
        get
        {
            return m_iPhase2ID;
        }
        set
        {
            m_iPhase2ID = value;
        }
    }

    //public int iMissionsRemainingInCurrentPhase
    //{
    //    get
    //    {
    //        return m_iMissionsRemainingInCurrentPhase;
    //    }
    //    set
    //    {
    //        m_iMissionsRemainingInCurrentPhase = value;
    //    }
    //}

    public int iPhase1ID
    {
        get
        {
            return m_iPhase1ID;
        }
        set
        {
            m_iPhase1ID = value;
        }
    }

    public int iPhase3ID
    {
        get
        {
            return m_iPhase3ID;
        }
        set
        {
            m_iPhase3ID = value;
        }
    }

    public int iCurrentPhaseID
    {
        get
        {
            return m_iCurrentPhaseID;
        }
        set
        {
            m_iCurrentPhaseID = value;
        }
    }

    public bool bHasEmailBeenSent
    {
        get
        {
            return m_bHasEmailBeenSent;
        }
        set
        {
            m_bHasEmailBeenSent = value;
        }
    }

    public bool bIsDeleted
    {
        get
        {
            return m_bIsDeleted;
        }
        set
        {
            m_bIsDeleted = value;
        }
    }

    #endregion

    #region CONSTRUCTORS

    public clsAccountUserTrackings()
    {
        m_iAccountUserTrackingsID = 0;
    }

    public clsAccountUserTrackings(int iAccountUserTrackingID)
    {
        m_iAccountUserTrackingsID = iAccountUserTrackingID;
        GetData();
    }

    public clsAccountUserTrackings(int iAccountUserID, bool bIsByAccount)
    {
        m_iAccountUserTrackingsID = iAccountUserTrackingID;
        GetData();
    }


    //public clsTracking(string strUsername, string strPassword)
    //{
    //    //### Assign values to the parameter list for each corresponding column in the DB 
    //    SqlParameter[] sqlParametersInit = new SqlParameter[] 
    //    { 
    //        new SqlParameter("@strEmail", strUsername), 
    //        new SqlParameter("@strPassword", strPassword) 
    //    };

    //    DataRow drLoginRecord = clsDataAccess.GetRecord("spInitialiseAccountUser", sqlParametersInit);

    //    if (drLoginRecord != null)
    //    {
    //        m_iAccountUserTrackingsID = Convert.ToInt32(drLoginRecord["iAccountUserID"]);
    //        m_dtAdded = Convert.ToDateTime(drLoginRecord["dtAdded"]);
    //        m_iAddedBy = Convert.ToInt32(drLoginRecord["iAddedBy"]);

    //        if (drLoginRecord["dtEdited"] != DBNull.Value)
    //            m_dtEdited = Convert.ToDateTime(drLoginRecord["dtEdited"]);

    //        if (drLoginRecord["iEditedBy"] != DBNull.Value)
    //            m_iEditedBy = Convert.ToInt32(drLoginRecord["iEditedBy"]);

    //        m_iAccountUserID = Convert.ToInt32(drLoginRecord["iTitleID"]);
    //        m_iCurrentPhaseID = Convert.ToInt32(drLoginRecord["iCurrentPhaseID"]);
    //        m_iMissionsAvailableInCurrentPhase = Convert.ToInt32(drLoginRecord["iMissionsAvailableInCurrentPhase"]);
    //        m_iMissionsRemainingInCurrentPhase = Convert.ToInt32(drLoginRecord["iMissionsRemainingInCurrentPhase"]);
    //        m_iPhase1ID = Convert.ToInt32(drLoginRecord["iPhase1ID"]);
    //        m_iPhase2ID = Convert.ToInt32(drLoginRecord["iPhase2ID"]);
    //        m_iPhase3ID = Convert.ToInt32(drLoginRecord["iPhase3ID"]);
    //        m_bIsPhase1Complete = Convert.ToBoolean(drLoginRecord["bIsPhase1Complete"]);
    //        m_bIsPhase2Complete = Convert.ToBoolean(drLoginRecord["bIsPhase2Complete"]);
    //        m_bIsPhase3Complete = Convert.ToBoolean(drLoginRecord["bIsPhase3Complete"]);
    //        m_dtCompleted = Convert.ToDateTime(drLoginRecord["dtCompleted"]);
    //        m_bHasEmailBeenSent = Convert.ToBoolean(drLoginRecord["bHasCompletedProfileMission"]);
    //        m_bIsDeleted = Convert.ToBoolean(drLoginRecord["bIsDeleted"]);
    //    }
    //    else
    //        throw new Exception("Invalid User Login Credentials");
    //}

    #endregion

    #region PUBLIC METHODS

    public virtual void Update()
    {
       
            if (iAccountUserTrackingID == 0)
            {
                //### Assign values to the parameter list for each corresponding column in the DB
                SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@dtAdded", m_dtAdded),
                        
                        //new SqlParameter("@iMissionsAvailableInCurrentPhase", m_iMissionsAvailableInCurrentPhase),
                        //new SqlParameter("@iMissionsRemainingInCurrentPhase", m_iMissionsRemainingInCurrentPhase),
                        new SqlParameter("@iCurrentPhaseLevel", m_iCurrentPhaseLevel),
                        new SqlParameter("@bIsPhase1Complete", m_bIsPhase1Complete),
                        new SqlParameter("@bIsPhase2Complete", m_bIsPhase2Complete),
                        new SqlParameter("@bIsPhase3Complete", m_bIsPhase3Complete),
                        new SqlParameter("@iCurrentPhaseID", m_iCurrentPhaseID),
                        new SqlParameter("@iPhase1ID", m_iPhase1ID),
                        new SqlParameter("@iPhase2ID", m_iPhase2ID),
                        new SqlParameter("@iPhase3ID", m_iPhase3ID),
                        new SqlParameter("@bHasEmailBeenSent", m_bHasEmailBeenSent)         
                  };

                //### Add
                m_iAccountUserTrackingsID = (int)clsDataAccess.ExecuteScalar("spAccountUserTrackingsInsert", sqlParametersInsert);
            }
            else
            {
                //### Assign values to the parameter list for each corresponding column in the DB
                SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                        new SqlParameter("@iAccountUserTrackingID", m_iAccountUserTrackingsID),
                        new SqlParameter("@dtEdited", m_dtEdited),
                        new SqlParameter("@iCurrentPhaseID", m_iCurrentPhaseID),
                        //new SqlParameter("@iMissionsAvailableInCurrentPhase", m_iMissionsAvailableInCurrentPhase),
                        //new SqlParameter("@iMissionsRemainingInCurrentPhase", m_iMissionsRemainingInCurrentPhase),
                        new SqlParameter("@iCurrentPhaseLevel", m_iCurrentPhaseLevel),
                        new SqlParameter("@iPhase1ID", m_iPhase1ID),
                        new SqlParameter("@iPhase2ID", m_iPhase2ID),
                        new SqlParameter("@iPhase3ID", m_iPhase3ID),
                        new SqlParameter("@bIsPhase1Complete", m_bIsPhase1Complete),
                        new SqlParameter("@bIsPhase2Complete", m_bIsPhase2Complete),
                        new SqlParameter("@bIsPhase3Complete", m_bIsPhase3Complete),
                        new SqlParameter("@bHasEmailBeenSent", m_bHasEmailBeenSent)  
                    };
                //### Update
                clsDataAccess.Execute("spAccountUserTrackingsUpdate", sqlParametersUpdate);
            }
        
    }

    public static void Delete(int iAccountUserTrackingID)
    {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iAccountUserTrackingID", iAccountUserTrackingID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spAccountUserTrackingsDelete", sqlParameter);
    }

    //public static void ProfileMisionComplete(int iAccountUserID)
    //{
    //    //### Populate
    //    SqlParameter[] sqlParameter = new SqlParameter[]
    //    {
    //        new SqlParameter("@iAccountUserID", iAccountUserID)
    //    };
    //    //### Executes switches boolean sp
    //    clsDataAccess.Execute("spAccountUsersCompleteProfile", sqlParameter);
    //}

    public static DataTable GetAccountUserTrackingsList()
    {
        SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
        return clsDataAccess.GetDataTable("spAccountUserTrackingsList", EmptySqlParameter);
    }

    public static DataTable GetAccountUserTrackingsList(string strFilterExpression, string strSortExpression)
    {
        DataView dvAccountUsersList = new DataView();

        SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
        dvAccountUsersList = clsDataAccess.GetDataView("spAccountUserTrackingsList", EmptySqlParameter);
        dvAccountUsersList.RowFilter = strFilterExpression;
        dvAccountUsersList.Sort = strSortExpression;

        return dvAccountUsersList.ToTable();
    }
    #endregion

    #region PROTECTED METHODS

    protected virtual void GetData()
    {
        
            //### Populate
            SqlParameter[] sqlParameter = new SqlParameter[] 
            {
                new SqlParameter("@iAccountUserTrackingID", m_iAccountUserTrackingsID)
            };
            DataRow drRecord = clsDataAccess.GetRecord("spAccountUserTrackingsGetRecord", sqlParameter);

            m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);

            if (drRecord["dtEdited"] != DBNull.Value)
                m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

            m_iCurrentPhaseID = Convert.ToInt32(drRecord["iCurrentPhaseID"]);
            //m_iMissionsAvailableInCurrentPhase = Convert.ToInt32(drRecord["iMissionsAvailableInCurrentPhase"]);
            //m_iMissionsRemainingInCurrentPhase = Convert.ToInt32(drRecord["iMissionsRemainingInCurrentPhase"]);
            m_iCurrentPhaseLevel = Convert.ToInt32(drRecord["iCurrentPhaseLevel"]);
            m_iPhase1ID = Convert.ToInt32(drRecord["iPhase1ID"]);
            m_iPhase2ID = Convert.ToInt32(drRecord["iPhase2ID"]);
            m_iPhase3ID = Convert.ToInt32(drRecord["iPhase3ID"]);
            m_bIsPhase1Complete = Convert.ToBoolean(drRecord["bIsPhase1Complete"]);
            m_bIsPhase2Complete = Convert.ToBoolean(drRecord["bIsPhase2Complete"]);
            m_bIsPhase3Complete = Convert.ToBoolean(drRecord["bIsPhase3Complete"]);
            m_bHasEmailBeenSent = Convert.ToBoolean(drRecord["bHasEmailBeenSent"]);
            m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);

 
    }
    //protected virtual void GetTrackingDataByAccountUser()
    //{
        
    //        //### Populate
    //        SqlParameter[] sqlParameter = new SqlParameter[] 
    //        {
    //            new SqlParameter("@iAccountUserID", m_iAccountUserID)
    //        };
    //        DataRow drRecord = clsDataAccess.GetRecord("spAccountUserTrackingsGetRecord", sqlParameter);

    //        m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);

    //        if (drRecord["dtEdited"] != DBNull.Value)
    //            m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);


    //        m_iAccountUserTrackingsID = Convert.ToInt32(drRecord["iAccountUserTrackingsID"]);
    //        m_iCurrentPhaseID = Convert.ToInt32(drRecord["iCurrentPhaseID"]);
    //        //m_iMissionsAvailableInCurrentPhase = Convert.ToInt32(drRecord["iMissionsAvailableInCurrentPhase"]);
    //        //m_iMissionsRemainingInCurrentPhase = Convert.ToInt32(drRecord["iMissionsRemainingInCurrentPhase"]);
    //        m_iPhase1ID = Convert.ToInt32(drRecord["iPhase1ID"]);
    //        m_iPhase2ID = Convert.ToInt32(drRecord["iPhase2ID"]);
    //        m_iPhase3ID = Convert.ToInt32(drRecord["iPhase3ID"]);
    //        m_bIsPhase1Complete = Convert.ToBoolean(drRecord["bIsPhase1Complete"]);
    //        m_bIsPhase2Complete = Convert.ToBoolean(drRecord["bIsPhase2Complete"]);
    //        m_bIsPhase3Complete = Convert.ToBoolean(drRecord["bIsPhase3Complete"]);
    //        m_bHasEmailBeenSent = Convert.ToBoolean(drRecord["bHasEmailBeenSent"]);
    //        m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);

    //}
    #endregion
}