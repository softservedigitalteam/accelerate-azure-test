
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsCompletedMissions
/// </summary>
public class clsCompletedMissions
{
    #region MEMBER VARIABLES

        private int m_iCompletedMissionID;
        private DateTime m_dtAdded;
        private int m_iAddedBy;
        private DateTime m_dtEdited;
        private int m_iEditedBy;
        private int m_iAccountUserID;
        private int m_iMissionID;
        private bool m_bIsDeleted;
        
    #endregion 

    #region PROPERTIES

        public int iCompletedMissionID
        {
            get
            {
                return m_iCompletedMissionID;
            }
        }

        public DateTime dtAdded
        {
            get
            {
                return m_dtAdded;
            }
            set
            {
                m_dtAdded = value;
            }
        }

        public int iAddedBy
        {
            get
            {
                return m_iAddedBy;
            }
            set
            {
                m_iAddedBy = value;
            }
        }

        public DateTime dtEdited
        {
            get
            {
                return m_dtEdited;
            }
            set
            {
                m_dtEdited = value;
            }
        }

        public int iEditedBy
        {
            get
            {
                return m_iEditedBy;
            }
            set
            {
                m_iEditedBy = value;
            }
        }

        public int iAccountUserID
        {
            get
            {
                return m_iAccountUserID;
            }
            set
            {
                m_iAccountUserID = value;
            }
        }

        public int iMissionID
        {
            get
            {
                return m_iMissionID;
            }
            set
            {
                m_iMissionID = value;
            }
        }

        public bool bIsDeleted
        {
            get
            {
                return m_bIsDeleted;
            }
            set
            {
                m_bIsDeleted = value;
            }
        }

    
    #endregion
    
    #region CONSTRUCTORS

    public clsCompletedMissions()
    {
        m_iCompletedMissionID = 0;
    }

    public clsCompletedMissions(int iCompletedMissionID)
    {
        m_iCompletedMissionID = iCompletedMissionID;
        GetData();
    }

    #endregion

    #region PUBLIC METHODS

        public virtual void Update()
        {
            try
            {
                if (iCompletedMissionID == 0)
                {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@dtAdded", m_dtAdded),
                        new SqlParameter("@iAddedBy", m_iAddedBy),
                        new SqlParameter("@iAccountUserID", m_iAccountUserID),
                        new SqlParameter("@iMissionID", m_iMissionID)                  
                  };

                  //### Add
                  m_iCompletedMissionID = (int)clsDataAccess.ExecuteScalar("spCompletedMissionsInsert", sqlParametersInsert);                    
                    }
                    else
                    {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iCompletedMissionID", m_iCompletedMissionID),
                         new SqlParameter("@dtEdited", m_dtEdited),
                         new SqlParameter("@iEditedBy", m_iEditedBy),
                         new SqlParameter("@iAccountUserID", m_iAccountUserID),
                         new SqlParameter("@iMissionID", m_iMissionID)
                    };
                    //### Update
                    clsDataAccess.Execute("spCompletedMissionsUpdate", sqlParametersUpdate);
                    }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Delete(int iCompletedMissionID)
        {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iCompletedMissionID", iCompletedMissionID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spCompletedMissionsDelete", sqlParameter);
        }

        public static DataTable GetCompletedMissionsList()
        {
            SqlParameter[] EmptySqlParameter = new SqlParameter[]{};
            return clsDataAccess.GetDataTable("spCompletedMissionsList", EmptySqlParameter);
        }
        public static DataTable GetCompletedMissionsList(string strFilterExpression, string strSortExpression)
        {
            DataView dvCompletedMissionsList = new DataView();

            SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
            dvCompletedMissionsList = clsDataAccess.GetDataView("spCompletedMissionsList", EmptySqlParameter);
            dvCompletedMissionsList.RowFilter = strFilterExpression;
            dvCompletedMissionsList.Sort = strSortExpression;

            return dvCompletedMissionsList.ToTable();
        }
    #endregion

    #region PROTECTED METHODS

        protected virtual void GetData()
        {
            try
                {
                    //### Populate
                    SqlParameter[] sqlParameter = new SqlParameter[] 
                        {
                            new SqlParameter("@iCompletedMissionID", m_iCompletedMissionID)
                        };
                DataRow drRecord = clsDataAccess.GetRecord("spCompletedMissionsGetRecord", sqlParameter);

                m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);
                m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);

                if (drRecord["dtEdited"] != DBNull.Value)
                   m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

             if (drRecord["iEditedBy"] != DBNull.Value)
               m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);

                m_iAccountUserID = Convert.ToInt32(drRecord["iAccountUserID"]);
                m_iMissionID = Convert.ToInt32(drRecord["iMissionID"]);
                m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);
 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    #endregion
}