
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsKnowledgeManagements
/// </summary>
public class clsKnowledgeManagements
{
    #region MEMBER VARIABLES

        private int m_iKnowledgeManagementID;
        private DateTime m_dtAdded;
        private int m_iAddedBy;
        private DateTime m_dtEdited;
        private int m_iEditedBy;
        private String m_strFileSize;
        private String m_strTitle;
        private String m_strDescription;
        private String m_strPathToKnowledgeManagement;
        private bool m_bIsDeleted;
        
    #endregion 

    #region PROPERTIES

        public int iKnowledgeManagementID
        {
            get
            {
                return m_iKnowledgeManagementID;
            }
        }

        public DateTime dtAdded
        {
            get
            {
                return m_dtAdded;
            }
            set
            {
                m_dtAdded = value;
            }
        }

        public int iAddedBy
        {
            get
            {
                return m_iAddedBy;
            }
            set
            {
                m_iAddedBy = value;
            }
        }

        public DateTime dtEdited
        {
            get
            {
                return m_dtEdited;
            }
            set
            {
                m_dtEdited = value;
            }
        }

        public int iEditedBy
        {
            get
            {
                return m_iEditedBy;
            }
            set
            {
                m_iEditedBy = value;
            }
        }

        public String strFileSize
        {
            get
            {
                return m_strFileSize;
            }
            set
            {
                m_strFileSize = value;
            }
        }

        public String strTitle
        {
            get
            {
                return m_strTitle;
            }
            set
            {
                m_strTitle = value;
            }
        }

        public String strDescription
        {
            get
            {
                return m_strDescription;
            }
            set
            {
                m_strDescription = value;
            }
        }

        public String strPathToKnowledgeManagement
        {
            get
            {
                return m_strPathToKnowledgeManagement;
            }
            set
            {
                m_strPathToKnowledgeManagement = value;
            }
        }

        public bool bIsDeleted
        {
            get
            {
                return m_bIsDeleted;
            }
            set
            {
                m_bIsDeleted = value;
            }
        }

    
    #endregion
    
    #region CONSTRUCTORS

    public clsKnowledgeManagements()
    {
        m_iKnowledgeManagementID = 0;
    }

    public clsKnowledgeManagements(int iKnowledgeManagementID)
    {
        m_iKnowledgeManagementID = iKnowledgeManagementID;
        GetData();
    }

    #endregion

    #region PUBLIC METHODS

        public virtual void Update()
        {
            try
            {
                if (iKnowledgeManagementID == 0)
                {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@dtAdded", m_dtAdded),
                        new SqlParameter("@iAddedBy", m_iAddedBy),
                        new SqlParameter("@strFileSize", m_strFileSize),
                        new SqlParameter("@strTitle", m_strTitle),
                        new SqlParameter("@strDescription", m_strDescription),
                        new SqlParameter("@strPathToKnowledgeManagement", m_strPathToKnowledgeManagement)                  
                  };

                  //### Add
                  m_iKnowledgeManagementID = (int)clsDataAccess.ExecuteScalar("spKnowledgeManagementsInsert", sqlParametersInsert);                    
                    }
                    else
                    {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iKnowledgeManagementID", m_iKnowledgeManagementID),
                         new SqlParameter("@dtEdited", m_dtEdited),
                         new SqlParameter("@iEditedBy", m_iEditedBy),
                         new SqlParameter("@strFileSize", m_strFileSize),
                         new SqlParameter("@strTitle", m_strTitle),
                         new SqlParameter("@strDescription", m_strDescription),
                         new SqlParameter("@strPathToKnowledgeManagement", m_strPathToKnowledgeManagement)
                    };
                    //### Update
                    clsDataAccess.Execute("spKnowledgeManagementsUpdate", sqlParametersUpdate);
                    }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Delete(int iKnowledgeManagementID)
        {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iKnowledgeManagementID", iKnowledgeManagementID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spKnowledgeManagementsDelete", sqlParameter);
        }

        public static DataTable GetKnowledgeManagementsList()
        {
            SqlParameter[] EmptySqlParameter = new SqlParameter[]{};
            return clsDataAccess.GetDataTable("spKnowledgeManagementsList", EmptySqlParameter);
        }
        public static DataTable GetKnowledgeManagementsList(string strFilterExpression, string strSortExpression)
        {
            DataView dvKnowledgeManagementsList = new DataView();

            SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
            dvKnowledgeManagementsList = clsDataAccess.GetDataView("spKnowledgeManagementsList", EmptySqlParameter);
            dvKnowledgeManagementsList.RowFilter = strFilterExpression;
            dvKnowledgeManagementsList.Sort = strSortExpression;

            return dvKnowledgeManagementsList.ToTable();
        }
    #endregion

    #region PROTECTED METHODS

        protected virtual void GetData()
        {
            try
                {
                    //### Populate
                    SqlParameter[] sqlParameter = new SqlParameter[] 
                        {
                            new SqlParameter("@iKnowledgeManagementID", m_iKnowledgeManagementID)
                        };
                DataRow drRecord = clsDataAccess.GetRecord("spKnowledgeManagementsGetRecord", sqlParameter);

                m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);
                m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);

                if (drRecord["dtEdited"] != DBNull.Value)
                   m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

             if (drRecord["iEditedBy"] != DBNull.Value)
               m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);

                m_strFileSize = drRecord["strFileSize"].ToString();
                m_strTitle = drRecord["strTitle"].ToString();
                m_strDescription = drRecord["strDescription"].ToString();
                m_strPathToKnowledgeManagement = drRecord["strPathToKnowledgeManagement"].ToString();
                m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);
 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    #endregion
}