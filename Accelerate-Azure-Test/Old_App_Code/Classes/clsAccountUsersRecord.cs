using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsAccountUsers
/// </summary>
public class clsAccountUsers
{
    #region MEMBER VARIABLES

    private int m_iAccountUserID;
    private DateTime m_dtAdded;
    private int m_iAddedBy;
    private DateTime m_dtEdited;
    private int m_iEditedBy;
    private int m_iAccountUserTrackingID;
    private String m_strFirstName;
    private String m_strSurname;
    private String m_strEmailAddress;
    private string m_strPhoneNumber;
    private String m_strPassword;
    private string m_strJobTitle;
    private int m_iGroupID;
    private int m_iCurrentPhaseID;
    private int m_iCurrentMissionID;
    private int m_iCurrentPoints;
    private String m_strPathToImages;
    private String m_strMasterImage;
    private bool m_bHasCompletedProfileMission;
    private bool m_bIsDeleted;

    #endregion

    #region PROPERTIES

    public int iAccountUserID
    {
        get
        {
            return m_iAccountUserID;
        }
    }

    public DateTime dtAdded
    {
        get
        {
            return m_dtAdded;
        }
        set
        {
            m_dtAdded = value;
        }
    }

    public int iAddedBy
    {
        get
        {
            return m_iAddedBy;
        }
        set
        {
            m_iAddedBy = value;
        }
    }

    public DateTime dtEdited
    {
        get
        {
            return m_dtEdited;
        }
        set
        {
            m_dtEdited = value;
        }
    }

    public int iEditedBy
    {
        get
        {
            return m_iEditedBy;
        }
        set
        {
            m_iEditedBy = value;
        }
    }

    public String strFirstName
    {
        get
        {
            return m_strFirstName;
        }
        set
        {
            m_strFirstName = value;
        }
    }

    public String strSurname
    {
        get
        {
            return m_strSurname;
        }
        set
        {
            m_strSurname = value;
        }
    }

    public String strEmailAddress
    {
        get
        {
            return m_strEmailAddress;
        }
        set
        {
            m_strEmailAddress = value;
        }

    }
    
    public String strPhoneNumber
    {
        get
        {
            return m_strPhoneNumber;
        }
        set
        {
            m_strPhoneNumber = value;
        }
    }

    public String strPassword
    {
        get
        {
            return m_strPassword;
        }
        set
        {
            m_strPassword = value;
        }
    }

    public String strJobTitle
    {
        get
        {
            return m_strJobTitle;
        }
        set
        {
            m_strJobTitle = value;
        }
    }

    public int iGroupID
    {
        get
        {
            return m_iGroupID;
        }
        set
        {
            m_iGroupID = value;
        }
    }

    public int iAccountUserTrackingID
    {
        get
        {
            return m_iAccountUserTrackingID;
        }
        set
        {
            m_iAccountUserTrackingID = value;
        }
    }

    public int iCurrentPhaseID
    {
        get
        {
            return m_iCurrentPhaseID;
        }
        set
        {
            m_iCurrentPhaseID = value;
        }
    }

    public int iCurrentMissionID
    {
        get
        {
            return m_iCurrentMissionID;
        }
        set
        {
            m_iCurrentMissionID = value;
        }
    }

    public int iCurrentPoints
    {
        get
        {
            return m_iCurrentPoints;
        }
        set
        {
            m_iCurrentPoints = value;
        }
    }

    public String strPathToImages
    {
        get
        {
            return m_strPathToImages;
        }
        set
        {
            m_strPathToImages = value;
        }
    }

    public String strMasterImage
    {
        get
        {
            return m_strMasterImage;
        }
        set
        {
            m_strMasterImage = value;
        }
    }

    public bool bHasCompletedProfileMission
    {
        get
        {
            return m_bHasCompletedProfileMission;
        }
        set
        {
            m_bHasCompletedProfileMission = value;
        }
    }

    public bool bIsDeleted
    {
        get
        {
            return m_bIsDeleted;
        }
        set
        {
            m_bIsDeleted = value;
        }
    }

    #endregion

    #region CONSTRUCTORS

    public clsAccountUsers()
    {
        m_iAccountUserID = 0;
    }

    public clsAccountUsers(int iAccountUserID)
    {
        m_iAccountUserID = iAccountUserID;
        GetData();
    }

    public clsAccountUsers(string strUsername, string strPassword)
    {
        //### Assign values to the parameter list for each corresponding column in the DB 
        SqlParameter[] sqlParametersInit = new SqlParameter[] 
        { 
            new SqlParameter("@strEmail", strUsername), 
            new SqlParameter("@strPassword", strPassword) 
        };

        DataRow drLoginRecord = clsDataAccess.GetRecord("spInitialiseAccountUser", sqlParametersInit);

        if (drLoginRecord != null)
        {
            m_iAccountUserID = Convert.ToInt32(drLoginRecord["iAccountUserID"]);
            m_dtAdded = Convert.ToDateTime(drLoginRecord["dtAdded"]);
            m_iAddedBy = Convert.ToInt32(drLoginRecord["iAddedBy"]);

            if (drLoginRecord["dtEdited"] != DBNull.Value)
                m_dtEdited = Convert.ToDateTime(drLoginRecord["dtEdited"]);

            if (drLoginRecord["iEditedBy"] != DBNull.Value)
                m_iEditedBy = Convert.ToInt32(drLoginRecord["iEditedBy"]);

            m_strFirstName = drLoginRecord["strFirstName"].ToString();
            m_strSurname = drLoginRecord["strSurname"].ToString(); 
             m_strPassword = drLoginRecord["strPassword"].ToString();
            m_strJobTitle = drLoginRecord["strJobTitle"].ToString();
            m_iGroupID = Convert.ToInt32(drLoginRecord["iGroupID"]);
            m_iAccountUserTrackingID = Convert.ToInt32(drLoginRecord["iAccountUserTrackingID"]);
            m_iCurrentPhaseID = Convert.ToInt32(drLoginRecord["iCurrentPhaseID"]);
            m_iCurrentMissionID = Convert.ToInt32(drLoginRecord["iCurrentMissionID"]);
            m_iCurrentPoints = Convert.ToInt32(drLoginRecord["iCurrentPoints"]);
            m_strEmailAddress = drLoginRecord["strEmailAddress"].ToString();
            m_strPhoneNumber = drLoginRecord["strPhoneNumber"].ToString();
            m_strPathToImages = drLoginRecord["strPathToImages"].ToString();
            m_strMasterImage = drLoginRecord["strMasterImage"].ToString();
            m_bHasCompletedProfileMission = Convert.ToBoolean(drLoginRecord["bHasCompletedProfileMission"]);

            m_bIsDeleted = Convert.ToBoolean(drLoginRecord["bIsDeleted"]);
        }
        else
            throw new Exception("Invalid User Login Credentials");
    }

    #endregion

    #region PUBLIC METHODS

    public virtual void Update()
    {
        
            if (iAccountUserID == 0)
            {
                //### Assign values to the parameter list for each corresponding column in the DB
                SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                {
                    new SqlParameter("@dtAdded", m_dtAdded),
                    new SqlParameter("@iAddedBy", m_iAddedBy),
                    new SqlParameter("@strFirstName", m_strFirstName),
                    new SqlParameter("@strSurname", m_strSurname),
                    new SqlParameter("@strEmailAddress", m_strEmailAddress),
                    new SqlParameter("@strPhoneNumber", m_strPhoneNumber),
                    new SqlParameter("@strPassword", m_strPassword),
                    new SqlParameter("@strJobTitle", m_strJobTitle),
                    new SqlParameter("@iGroupID", m_iGroupID),
                    new SqlParameter("@iAccountUserTrackingID", m_iAccountUserTrackingID),
                    new SqlParameter("@iCurrentPhaseID", m_iCurrentPhaseID),
                    new SqlParameter("@iCurrentMissionID", m_iCurrentMissionID),
                    new SqlParameter("@iCurrentPoints", m_iCurrentPoints),
                    new SqlParameter("@strPathToImages", m_strPathToImages),
                    new SqlParameter("@strMasterImage", m_strMasterImage),
                    new SqlParameter("@bHasCompletedProfileMission", m_bHasCompletedProfileMission)
                };

                //### Add
                m_iAccountUserID = (int)clsDataAccess.ExecuteScalar("spAccountUsersInsert", sqlParametersInsert);
            }
            else
            {
                //### Assign values to the parameter list for each corresponding column in the DB
                SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iAccountUserID", m_iAccountUserID),
                         new SqlParameter("@dtEdited", m_dtEdited),
                         new SqlParameter("@iEditedBy", m_iEditedBy),
                         new SqlParameter("@strFirstName", m_strFirstName),
                         new SqlParameter("@strSurname", m_strSurname),
                         new SqlParameter("@strEmailAddress", m_strEmailAddress),
                         new SqlParameter("@strPhoneNumber", m_strPhoneNumber),
                         new SqlParameter("@strPassword", m_strPassword),
                         new SqlParameter("@strJobTitle", m_strJobTitle),
                         new SqlParameter("@iGroupID", m_iGroupID),
                         new SqlParameter("@iAccountUserTrackingID", m_iAccountUserTrackingID),
                         new SqlParameter("@iCurrentPhaseID", m_iCurrentPhaseID),
                         new SqlParameter("@iCurrentMissionID", m_iCurrentMissionID),
                         new SqlParameter("@iCurrentPoints", m_iCurrentPoints),
                         new SqlParameter("@strPathToImages", m_strPathToImages),
                         new SqlParameter("@strMasterImage", m_strMasterImage),
                         new SqlParameter("@bHasCompletedProfileMission", m_bHasCompletedProfileMission)
                    };
                //### Update
                clsDataAccess.Execute("spAccountUsersUpdate", sqlParametersUpdate);
            }
       
    }

    public static void Delete(int iAccountUserID)
    {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iAccountUserID", iAccountUserID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spAccountUsersDelete", sqlParameter);
    }

    public static void ProfileMisionComplete(int iAccountUserID)
    {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iAccountUserID", iAccountUserID)
        };
        //### Executes switches boolean sp
        clsDataAccess.Execute("spAccountUsersCompleteProfile", sqlParameter);
    }

    public static DataTable GetAccountUsersList()
    {
        SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
        return clsDataAccess.GetDataTable("spAccountUsersList", EmptySqlParameter);
    }
    public static DataTable GetTop5AccountUsersList(int iGroupID)
    {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iGroupID", iGroupID)
        };
        return clsDataAccess.GetDataTable("spAccountUsersListTop5", sqlParameter);
    }
    public static DataTable GetAccountUsersListWithGroupName()
    {
        SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
        return clsDataAccess.GetDataTable("spAccountUsersListWithGroupName", EmptySqlParameter);
    }

    public static DataTable GetAccountUsersListWithGroupName(string strSortExpression)
    {
        
        DataView dvAccountUsersList = new DataView();
        SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
        dvAccountUsersList = clsDataAccess.GetDataView("spAccountUsersListWithGroupName", EmptySqlParameter);
        dvAccountUsersList.Sort = strSortExpression;

        return dvAccountUsersList.ToTable();
    }

    public static DataTable GetAccountUsersList(string strFilterExpression, string strSortExpression)
    {
        DataView dvAccountUsersList = new DataView();

        SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
        dvAccountUsersList = clsDataAccess.GetDataView("spAccountUsersList", EmptySqlParameter);
        dvAccountUsersList.RowFilter = strFilterExpression;
        dvAccountUsersList.Sort = strSortExpression;

        return dvAccountUsersList.ToTable();
    }
    #endregion

    #region PROTECTED METHODS

    protected virtual void GetData()
    {
        
            //### Populate
            SqlParameter[] sqlParameter = new SqlParameter[] 
            {
                new SqlParameter("@iAccountUserID", m_iAccountUserID)
            };
            DataRow drRecord = clsDataAccess.GetRecord("spAccountUsersGetRecord", sqlParameter);

            m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);
            m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);

            if (drRecord["dtEdited"] != DBNull.Value)
                m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

            if (drRecord["iEditedBy"] != DBNull.Value)
                m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);
            
            m_strFirstName = drRecord["strFirstName"].ToString();
            m_strSurname = drRecord["strSurname"].ToString();
            m_strEmailAddress = drRecord["strEmailAddress"].ToString();
            m_strPhoneNumber = drRecord["strPhoneNumber"].ToString(); 
             m_strPassword = drRecord["strPassword"].ToString();
        m_strJobTitle = drRecord["strJobTitle"].ToString();
        m_iGroupID = Convert.ToInt32(drRecord["iGroupID"]);
            m_iAccountUserTrackingID = Convert.ToInt32(drRecord["iAccountUserTrackingID"]);
            m_iCurrentPhaseID = Convert.ToInt32(drRecord["iCurrentPhaseID"]);
            m_iCurrentMissionID = Convert.ToInt32(drRecord["iCurrentMissionID"]);
            m_iCurrentPoints = Convert.ToInt32(drRecord["iCurrentPoints"]);
            m_strPathToImages = drRecord["strPathToImages"].ToString();
            m_strMasterImage = drRecord["strMasterImage"].ToString();
            m_bHasCompletedProfileMission= Convert.ToBoolean(drRecord["bHasCompletedProfileMission"]);
            m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);

        
    }
    #endregion
}