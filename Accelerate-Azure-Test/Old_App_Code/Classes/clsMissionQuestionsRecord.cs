
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsMissionQuestions
/// </summary>
public class clsMissionQuestions
{
    #region MEMBER VARIABLES

        private int m_iMissionQuestionID;
        private DateTime m_dtAdded;
        private int m_iAddedBy;
        private DateTime m_dtEdited;
        private int m_iEditedBy;
        private int m_iMissionID;
        private String m_strQuestion;
        private bool m_bIsText;
        private bool m_bIsDropDownList;
        private bool m_bIsMultipleChoice;
        private bool m_bIsTeamStructure;
    private bool m_bIsCertificateQuestion;
        private bool m_bIsDeleted;
        
    #endregion 

    #region PROPERTIES

        public int iMissionQuestionID
        {
            get
            {
                return m_iMissionQuestionID;
            }
        }

        public DateTime dtAdded
        {
            get
            {
                return m_dtAdded;
            }
            set
            {
                m_dtAdded = value;
            }
        }

        public int iAddedBy
        {
            get
            {
                return m_iAddedBy;
            }
            set
            {
                m_iAddedBy = value;
            }
        }

        public DateTime dtEdited
        {
            get
            {
                return m_dtEdited;
            }
            set
            {
                m_dtEdited = value;
            }
        }

        public int iEditedBy
        {
            get
            {
                return m_iEditedBy;
            }
            set
            {
                m_iEditedBy = value;
            }
        }

        public int iMissionID
        {
            get
            {
                return m_iMissionID;
            }
            set
            {
                m_iMissionID = value;
            }
        }

        public String strQuestion
        {
            get
            {
                return m_strQuestion;
            }
            set
            {
                m_strQuestion = value;
            }
        }

        public bool bIsText
        {
            get
            {
                return m_bIsText;
            }
            set
            {
                m_bIsText = value;
            }
        }

        public bool bIsDropDownList
        {
            get
            {
                return m_bIsDropDownList;
            }
            set
            {
                m_bIsDropDownList = value;
            }
        }

        public bool bIsMultipleChoice
        {
            get
            {
                return m_bIsMultipleChoice;
            }
            set
            {
                m_bIsMultipleChoice = value;
            }
        }

        public bool bIsTeamStructure
        {
            get
            {
                return m_bIsTeamStructure;
            }
            set
            {
                m_bIsTeamStructure = value;
            }
        }

    public bool bIsCertificateQuestion
    {
        get
        {
            return m_bIsCertificateQuestion;
        }
        set
        {
            m_bIsCertificateQuestion = value;
        }
    }


        public bool bIsDeleted
        {
            get
            {
                return m_bIsDeleted;
            }
            set
            {
                m_bIsDeleted = value;
            }
        }

    
    #endregion
    
    #region CONSTRUCTORS

    public clsMissionQuestions()
    {
        m_iMissionQuestionID = 0;
    }

    public clsMissionQuestions(int iMissionQuestionID)
    {
        m_iMissionQuestionID = iMissionQuestionID;
        GetData();
    }

    #endregion

    #region PUBLIC METHODS

        public virtual void Update()
        {
            try
            {
                if (iMissionQuestionID == 0)
                {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@dtAdded", m_dtAdded),
                        new SqlParameter("@iAddedBy", m_iAddedBy),
                        new SqlParameter("@iMissionID", m_iMissionID),
                        new SqlParameter("@strQuestion", m_strQuestion),
                        new SqlParameter("@bIsText", m_bIsText),
                        new SqlParameter("@bIsDropDownList", m_bIsDropDownList),
                        new SqlParameter("@bIsMultipleChoice", m_bIsMultipleChoice),
                        new SqlParameter("@bIsTeamStructure", m_bIsTeamStructure),
                        new SqlParameter("@bIsCertificateQuestion", m_bIsCertificateQuestion)
                        
                  };

                  //### Add
                  m_iMissionQuestionID = (int)clsDataAccess.ExecuteScalar("spMissionQuestionsInsert", sqlParametersInsert);                    
                    }
                    else
                    {
                    //### Assign values to the parameter list for each corresponding column in the DB
                    SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iMissionQuestionID", m_iMissionQuestionID),
                         new SqlParameter("@dtEdited", m_dtEdited),
                         new SqlParameter("@iEditedBy", m_iEditedBy),
                         new SqlParameter("@iMissionID", m_iMissionID),
                         new SqlParameter("@strQuestion", m_strQuestion),
                         new SqlParameter("@bIsText", m_bIsText),
                         new SqlParameter("@bIsDropDownList", m_bIsDropDownList),
                         new SqlParameter("@bIsMultipleChoice", m_bIsMultipleChoice),
                         new SqlParameter("@bIsTeamStructure", m_bIsTeamStructure),
                         new SqlParameter("@bIsCertificateQuestion", m_bIsCertificateQuestion)
                    };
                    //### Update
                    clsDataAccess.Execute("spMissionQuestionsUpdate", sqlParametersUpdate);
                    }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Delete(int iMissionQuestionID)
        {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iMissionQuestionID", iMissionQuestionID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spMissionQuestionsDelete", sqlParameter);
        }

        public static DataTable GetMissionQuestionsList()
        {
            SqlParameter[] EmptySqlParameter = new SqlParameter[]{};
            return clsDataAccess.GetDataTable("spMissionQuestionsList", EmptySqlParameter);
        }
        public static DataTable GetMissionQuestionsList(string strFilterExpression, string strSortExpression)
        {
            DataView dvMissionQuestionsList = new DataView();

            SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
            dvMissionQuestionsList = clsDataAccess.GetDataView("spMissionQuestionsList", EmptySqlParameter);
            dvMissionQuestionsList.RowFilter = strFilterExpression;
            dvMissionQuestionsList.Sort = strSortExpression;

            return dvMissionQuestionsList.ToTable();
        }
    #endregion

    #region PROTECTED METHODS

        protected virtual void GetData()
        {
            try
                {
                    //### Populate
                    SqlParameter[] sqlParameter = new SqlParameter[] 
                        {
                            new SqlParameter("@iMissionQuestionID", m_iMissionQuestionID)
                        };
                DataRow drRecord = clsDataAccess.GetRecord("spMissionQuestionsGetRecord", sqlParameter);

                m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);
                m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);

                if (drRecord["dtEdited"] != DBNull.Value)
                   m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

             if (drRecord["iEditedBy"] != DBNull.Value)
                m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);

                m_iMissionID = Convert.ToInt32(drRecord["iMissionID"]);
                m_strQuestion = drRecord["strQuestion"].ToString();
                m_bIsText = Convert.ToBoolean(drRecord["bIsText"]);
                m_bIsDropDownList = Convert.ToBoolean(drRecord["bIsDropDownList"]);
                m_bIsMultipleChoice = Convert.ToBoolean(drRecord["bIsMultipleChoice"]);
                m_bIsTeamStructure = Convert.ToBoolean(drRecord["bIsTeamStructure"]);
            m_bIsCertificateQuestion = Convert.ToBoolean(drRecord["bIsCertificateQuestion"]);
                m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    #endregion
}