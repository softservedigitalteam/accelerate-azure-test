
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsCustomColours
/// </summary>
public class clsCustomColours
{
    #region MEMBER VARIABLES

    private int m_iCustomColourID;
    private DateTime m_dtAdded;
    private int m_iAddedBy;
    private DateTime m_dtEdited;
    private int m_iEditedBy;
    private String m_strMissionHeaderText;
    private String m_strAchievementRankHeaderText;
    private String m_strAchievementRankText;
    private String m_strUserNameColour;
    private String m_strCongratulationsBackground;
    private String m_strAchievementImageContainer;
    private String m_strEmailHeaderBanner;
    private String m_strProfileHeaderSection;
    private String m_strCurrentRank;
    private String m_strNextRank;
    private String m_strAchievementText;
    private String m_strTopProgressBar;
    private String m_strTopBarBG;
    private String m_strTopBarText;
    private String m_strTopBarHoverBG;
    private String m_strTopBarHoverText;
    private String m_strSideBarHeaders;
    private String m_strSideBarHeaderText;
    private String m_strSideBarSubAreas;
    private String m_strSideBarSubAreaText;
    private String m_strNotificationAreaBorder;
    private String m_strNotificationHeaderText;
    private String m_strNotificationSubText;
    private String m_strMissionProgressBar;
    private String m_strPageHeaderText;
    private String m_strPhaseHeaderBG;
    private String m_strPhaseHeaderText;
    private String m_strTitle;
    private String m_strLink;
    private bool m_bIsDeleted;

    #endregion

    #region PROPERTIES

    public int iCustomColourID
    {
        get
        {
            return m_iCustomColourID;
        }
    }

    public DateTime dtAdded
    {
        get
        {
            return m_dtAdded;
        }
        set
        {
            m_dtAdded = value;
        }
    }

    public int iAddedBy
    {
        get
        {
            return m_iAddedBy;
        }
        set
        {
            m_iAddedBy = value;
        }
    }

    public DateTime dtEdited
    {
        get
        {
            return m_dtEdited;
        }
        set
        {
            m_dtEdited = value;
        }
    }

    public int iEditedBy
    {
        get
        {
            return m_iEditedBy;
        }
        set
        {
            m_iEditedBy = value;
        }
    }

    public String strMissionHeaderText
    {
        get
        {
            return m_strMissionHeaderText;
        }
        set
        {
            m_strMissionHeaderText = value;
        }
    }

    public String strAchievementRankHeaderText
    {
        get
        {
            return m_strAchievementRankHeaderText;
        }
        set
        {
            m_strAchievementRankHeaderText = value;
        }
    }

    public String strAchievementRankText
    {
        get
        {
            return m_strAchievementRankText;
        }
        set
        {
            m_strAchievementRankText = value;
        }
    }

    public String strUserNameColour
    {
        get
        {
            return m_strUserNameColour;
        }
        set
        {
            m_strUserNameColour = value;
        }
    }

    public String strCongratulationsBackground
    {
        get
        {
            return m_strCongratulationsBackground;
        }
        set
        {
            m_strCongratulationsBackground = value;
        }
    }

    public String strAchievementImageContainer
    {
        get
        {
            return m_strAchievementImageContainer;
        }
        set
        {
            m_strAchievementImageContainer = value;
        }
    }

    public String strEmailHeaderBanner
    {
        get
        {
            return m_strEmailHeaderBanner;
        }
        set
        {
            m_strEmailHeaderBanner = value;
        }
    }

    public String strTopBarBG
    {
        get
        {
            return m_strTopBarBG;
        }
        set
        {
            m_strTopBarBG = value;
        }
    }

    public String strCurrentRank
    {
        get
        {
            return m_strCurrentRank;
        }
        set
        {
            m_strCurrentRank = value;
        }
    }

    public String strNextRank
    {
        get
        {
            return m_strNextRank;
        }
        set
        {
            m_strNextRank = value;
        }
    }

    public String strAchievementText
    {
        get
        {
            return m_strAchievementText;
        }
        set
        {
            m_strAchievementText = value;
        }
    }

    public String strTopProgressBar
    {
        get
        {
            return m_strTopProgressBar;
        }
        set
        {
            m_strTopProgressBar = value;
        }
    }

    public String strProfileHeaderSection
    {
        get
        {
            return m_strProfileHeaderSection;
        }
        set
        {
            m_strProfileHeaderSection = value;
        }
    }

    public String strTopBarText
    {
        get
        {
            return m_strTopBarText;
        }
        set
        {
            m_strTopBarText = value;
        }
    }

    public String strTopBarHoverBG
    {
        get
        {
            return m_strTopBarHoverBG;
        }
        set
        {
            m_strTopBarHoverBG = value;
        }
    }

    public String strTopBarHoverText
    {
        get
        {
            return m_strTopBarHoverText;
        }
        set
        {
            m_strTopBarHoverText = value;
        }
    }

    public String strSideBarHeaders
    {
        get
        {
            return m_strSideBarHeaders;
        }
        set
        {
            m_strSideBarHeaders = value;
        }
    }

    public String strSideBarHeaderText
    {
        get
        {
            return m_strSideBarHeaderText;
        }
        set
        {
            m_strSideBarHeaderText = value;
        }
    }

    public String strSideBarSubAreas
    {
        get
        {
            return m_strSideBarSubAreas;
        }
        set
        {
            m_strSideBarSubAreas = value;
        }
    }

    public String strSideBarSubAreaText
    {
        get
        {
            return m_strSideBarSubAreaText;
        }
        set
        {
            m_strSideBarSubAreaText = value;
        }
    }

    public String strNotificationAreaBorder
    {
        get
        {
            return m_strNotificationAreaBorder;
        }
        set
        {
            m_strNotificationAreaBorder = value;
        }
    }

    public String strNotificationHeaderText
    {
        get
        {
            return m_strNotificationHeaderText;
        }
        set
        {
            m_strNotificationHeaderText = value;
        }
    }

    public String strNotificationSubText
    {
        get
        {
            return m_strNotificationSubText;
        }
        set
        {
            m_strNotificationSubText = value;
        }
    }

    public String strMissionProgressBar
    {
        get
        {
            return m_strMissionProgressBar;
        }
        set
        {
            m_strMissionProgressBar = value;
        }
    }

    public String strPageHeaderText
    {
        get
        {
            return m_strPageHeaderText;
        }
        set
        {
            m_strPageHeaderText = value;
        }
    }

    public String strPhaseHeaderBG
    {
        get
        {
            return m_strPhaseHeaderBG;
        }
        set
        {
            m_strPhaseHeaderBG = value;
        }
    }

    public String strPhaseHeaderText
    {
        get
        {
            return m_strPhaseHeaderText;
        }
        set
        {
            m_strPhaseHeaderText = value;
        }
    }

    public String strTitle
    {
        get
        {
            return m_strTitle;
        }
        set
        {
            m_strTitle = value;
        }
    }

    public String strLink
    {
        get
        {
            return m_strLink;
        }
        set
        {
            m_strLink = value;
        }
    }

    public bool bIsDeleted
    {
        get
        {
            return m_bIsDeleted;
        }
        set
        {
            m_bIsDeleted = value;
        }
    }


    #endregion

    #region CONSTRUCTORS

    public clsCustomColours()
    {
        m_iCustomColourID = 0;
    }

    public clsCustomColours(int iCustomColourID)
    {
        m_iCustomColourID = iCustomColourID;
        GetData();
    }

    #endregion

    #region PUBLIC METHODS

    public virtual void Update()
    {
        try
        {
            if (iCustomColourID == 0)
            {
                //### Assign values to the parameter list for each corresponding column in the DB
                SqlParameter[] sqlParametersInsert = new SqlParameter[] 
                    {
                        new SqlParameter("@dtAdded", m_dtAdded),
                        new SqlParameter("@iAddedBy", m_iAddedBy),
                        new SqlParameter("@strMissionHeaderText", m_strMissionHeaderText),
                        new SqlParameter("@strAchievementRankHeaderText", m_strAchievementRankHeaderText),
                        new SqlParameter("@strAchievementRankText", m_strAchievementRankText),
                        new SqlParameter("@strUserNameColour", m_strUserNameColour),
                        new SqlParameter("@strCongratulationsBackground", m_strCongratulationsBackground),
                        new SqlParameter("@strAchievementImageContainer", m_strAchievementImageContainer),
                        new SqlParameter("@strEmailHeaderBanner", m_strEmailHeaderBanner),
                        new SqlParameter("@strProfileHeaderSection", m_strProfileHeaderSection),
                        new SqlParameter("@strTopBarBG", m_strTopBarBG),
                        new SqlParameter("@strCurrentRank", m_strCurrentRank),
                        new SqlParameter("@strNextRank", m_strNextRank),
                        new SqlParameter("@strAchievementText", m_strAchievementText),
                        new SqlParameter("@strTopProgressBar", m_strTopProgressBar),
                        new SqlParameter("@strTopBarText", m_strTopBarText),
                        new SqlParameter("@strTopBarHoverBG", m_strTopBarHoverBG),
                        new SqlParameter("@strTopBarHoverText", m_strTopBarHoverText),
                        new SqlParameter("@strSideBarHeaders", m_strSideBarHeaders),
                        new SqlParameter("@strSideBarHeaderText", m_strSideBarHeaderText),
                        new SqlParameter("@strSideBarSubAreas", m_strSideBarSubAreas),
                        new SqlParameter("@strSideBarSubAreaText", m_strSideBarSubAreaText),
                        new SqlParameter("@strNotificationAreaBorder", m_strNotificationAreaBorder),
                        new SqlParameter("@strNotificationHeaderText", m_strNotificationHeaderText),
                        new SqlParameter("@strNotificationSubText", m_strNotificationSubText),
                        new SqlParameter("@strMissionProgressBar", m_strMissionProgressBar),
                        new SqlParameter("@strPageHeaderText", m_strPageHeaderText),
                        new SqlParameter("@strPhaseHeaderBG", m_strPhaseHeaderBG),
                        new SqlParameter("@strPhaseHeaderText", m_strPhaseHeaderText),
                        new SqlParameter("@strTitle", m_strTitle),
                        new SqlParameter("@strLink", m_strLink)
                                        
                  };

                //### Add
                m_iCustomColourID = (int)clsDataAccess.ExecuteScalar("spCustomColoursInsert", sqlParametersInsert);
            }
            else
            {
                //### Assign values to the parameter list for each corresponding column in the DB
                SqlParameter[] sqlParametersUpdate = new SqlParameter[] 
                    {
                         new SqlParameter("@iCustomColourID", m_iCustomColourID),
                         new SqlParameter("@dtEdited", m_dtEdited),
                         new SqlParameter("@iEditedBy", m_iEditedBy),
                         new SqlParameter("@strMissionHeaderText", m_strMissionHeaderText),
                         new SqlParameter("@strAchievementRankHeaderText", m_strAchievementRankHeaderText),
                         new SqlParameter("@strAchievementRankText", m_strAchievementRankText),
                         new SqlParameter("@strUserNameColour", m_strUserNameColour),
                         new SqlParameter("@strEmailHeaderBanner", m_strEmailHeaderBanner),
                         new SqlParameter("@strCongratulationsBackground", m_strCongratulationsBackground),
                         new SqlParameter("@strAchievementImageContainer", m_strAchievementImageContainer),
                         new SqlParameter("@strProfileHeaderSection", m_strProfileHeaderSection),
                         new SqlParameter("@strTopBarBG", m_strTopBarBG),
                         new SqlParameter("@strCurrentRank", m_strCurrentRank),
                         new SqlParameter("@strNextRank", m_strNextRank),
                         new SqlParameter("@strAchievementText", m_strAchievementText),
                         new SqlParameter("@strTopProgressBar", m_strTopProgressBar),
                         new SqlParameter("@strTopBarText", m_strTopBarText),
                         new SqlParameter("@strTopBarHoverBG", m_strTopBarHoverBG),
                         new SqlParameter("@strTopBarHoverText", m_strTopBarHoverText),
                         new SqlParameter("@strSideBarHeaders", m_strSideBarHeaders),
                         new SqlParameter("@strSideBarHeaderText", m_strSideBarHeaderText),
                         new SqlParameter("@strSideBarSubAreas", m_strSideBarSubAreas),
                         new SqlParameter("@strSideBarSubAreaText", m_strSideBarSubAreaText),
                         new SqlParameter("@strNotificationAreaBorder", m_strNotificationAreaBorder),
                         new SqlParameter("@strNotificationHeaderText", m_strNotificationHeaderText),
                         new SqlParameter("@strNotificationSubText", m_strNotificationSubText),
                         new SqlParameter("@strMissionProgressBar", m_strMissionProgressBar),
                         new SqlParameter("@strPageHeaderText", m_strPageHeaderText),
                         new SqlParameter("@strPhaseHeaderBG", m_strPhaseHeaderBG),
                         new SqlParameter("@strPhaseHeaderText", m_strPhaseHeaderText),
                         new SqlParameter("@strTitle", m_strTitle),
                         new SqlParameter("@strLink", m_strLink)
                    };
                //### Update
                clsDataAccess.Execute("spCustomColoursUpdate", sqlParametersUpdate);
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public static void Delete(int iCustomColourID)
    {
        //### Populate
        SqlParameter[] sqlParameter = new SqlParameter[]
        {
            new SqlParameter("@iCustomColourID", iCustomColourID)
        };
        //### Executes delete sp
        clsDataAccess.Execute("spCustomColoursDelete", sqlParameter);
    }

    public static DataTable GetCustomColoursList()
    {
        SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
        return clsDataAccess.GetDataTable("spCustomColoursList", EmptySqlParameter);
    }

    public static DataTable GetCustomColoursList(string strFilterExpression, string strSortExpression)
    {
        DataView dvCustomColoursList = new DataView();

        SqlParameter[] EmptySqlParameter = new SqlParameter[] { };
        dvCustomColoursList = clsDataAccess.GetDataView("spCustomColoursList", EmptySqlParameter);
        dvCustomColoursList.RowFilter = strFilterExpression;
        dvCustomColoursList.Sort = strSortExpression;

        return dvCustomColoursList.ToTable();
    }
    #endregion

    #region PROTECTED METHODS

    protected virtual void GetData()
    {
        try
        {
            //### Populate
            SqlParameter[] sqlParameter = new SqlParameter[] 
                        {
                            new SqlParameter("@iCustomColourID", m_iCustomColourID)
                        };
            DataRow drRecord = clsDataAccess.GetRecord("spCustomColoursGetRecord", sqlParameter);

            m_dtAdded = Convert.ToDateTime(drRecord["dtAdded"]);
            m_iAddedBy = Convert.ToInt32(drRecord["iAddedBy"]);

            if (drRecord["dtEdited"] != DBNull.Value)
                m_dtEdited = Convert.ToDateTime(drRecord["dtEdited"]);

            if (drRecord["iEditedBy"] != DBNull.Value)
                m_iEditedBy = Convert.ToInt32(drRecord["iEditedBy"]);

            m_strMissionHeaderText = drRecord["strMissionHeaderText"].ToString();
            m_strAchievementRankHeaderText = drRecord["strAchievementRankHeaderText"].ToString();
            m_strAchievementRankText = drRecord["strAchievementRankText"].ToString();
            m_strUserNameColour = drRecord["strUserNameColour"].ToString();
            m_strCongratulationsBackground = drRecord["strCongratulationsBackground"].ToString();
            m_strAchievementImageContainer = drRecord["strAchievementImageContainer"].ToString();
            m_strEmailHeaderBanner = drRecord["strEmailHeaderBanner"].ToString();
            m_strProfileHeaderSection = drRecord["strProfileHeaderSection"].ToString();
            m_strTopBarBG = drRecord["strTopBarBG"].ToString();
            m_strCurrentRank = drRecord["strCurrentRank"].ToString();
            m_strNextRank = drRecord["strNextRank"].ToString();
            m_strAchievementText = drRecord["strAchievementText"].ToString();
            m_strTopProgressBar = drRecord["strTopProgressBar"].ToString();
            m_strTopBarText = drRecord["strTopBarText"].ToString();
            m_strTopBarHoverBG = drRecord["strTopBarHoverBG"].ToString();
            m_strTopBarHoverText = drRecord["strTopBarHoverText"].ToString();
            m_strSideBarHeaders = drRecord["strSideBarHeaders"].ToString();
            m_strSideBarHeaderText = drRecord["strSideBarHeaderText"].ToString();
            m_strSideBarSubAreas = drRecord["strSideBarSubAreas"].ToString();
            m_strSideBarSubAreaText = drRecord["strSideBarSubAreaText"].ToString();
            m_strNotificationAreaBorder = drRecord["strNotificationAreaBorder"].ToString();
            m_strNotificationHeaderText = drRecord["strNotificationHeaderText"].ToString();
            m_strNotificationSubText = drRecord["strNotificationSubText"].ToString();
            m_strMissionProgressBar = drRecord["strMissionProgressBar"].ToString();
            m_strPageHeaderText = drRecord["strPageHeaderText"].ToString();
            m_strPhaseHeaderBG = drRecord["strPhaseHeaderBG"].ToString();
            m_strPhaseHeaderText = drRecord["strPhaseHeaderText"].ToString();
            m_strTitle = drRecord["strTitle"].ToString();
            m_strLink = drRecord["strLink"].ToString();
            m_bIsDeleted = Convert.ToBoolean(drRecord["bIsDeleted"]);

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    #endregion
}