﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Configuration;
using System.IO;
using System.Text;
using System.Web.Hosting;
using System.Web.UI;

/// <summary>
/// Summary description for clsSiteCommon
/// </summary>
public class clsSiteCommon
{
    #region CONSTRUCTOR

    private clsSiteCommon()
    {
    }

    /// <summary>
    /// Checks to see if the iEmployeeID cookie is set and active. If it is not active, method redirects to the login page, with the encrypted form of the branch id.
    /// </summary>
    //public static void DoLoggedInCheck()
    //{
    //    //### Check logged in
    //    if (HttpContext.Current.Request.Cookies["strEmail"] == null || string.IsNullOrEmpty(HttpContext.Current.Request.Cookies["strEmail"].Value))
    //    {
    //        HttpContext.Current.Response.Redirect("frmLogin.aspx");
    //    }
    //    else
    //    {
    //        HttpContext.Current.Session["clsAccountHolders"] = new clsAccountHolders(HttpContext.Current.Request.Cookies["strEmail"].Value, HttpContext.Current.Request.Cookies["strPassword"].Value);

    //        clsAccountHolders clsAccountHolders = (clsAccountHolders)HttpContext.Current.Session["clsAccountHolders"];

    //        //### Populate that users Access Rights
    //        clsAccessList clsAccessList = new clsAccessList(clsAccountHolders);
    //        clsAccessList.FilterExpression = "(iEmployeeID =" + clsAccountHolders.iUserID + ")";

    //        clsAccountHolders.DicAccessRights = new Dictionary<int, clsAccountHolders.AccessRights>();

    //        foreach (DataRow dataRowAccessItem in clsAccessList.GetRecords().ToTable().Rows)
    //        {
    //            //Creates the struct and sets its values
    //            clsAccountHolders.AccessRights structAccessRights = new clsAccountHolders.AccessRights(Convert.ToBoolean(dataRowAccessItem["bCreate"]), Convert.ToBoolean(dataRowAccessItem["bManage"]), Convert.ToBoolean(dataRowAccessItem["bIsReadable"]));
    //            clsAccountHolders.DicAccessRights.Add(Convert.ToInt32(dataRowAccessItem["iModuleID"]), structAccessRights);
    //        }
    //    }
    //}

    #endregion

    //#region CONTACT FORM

    //public static void ShowContactForm(Page Page, String strFormTitle, String strContactForm, int iClientID)
    //{
    //    Page.Session["ContactForm"] = strContactForm;

    //    try
    //    {
    //        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "ShowContactForm" + DateTime.Now.ToString("hh_mm_ss"),
    //            "$(document).ready(function() {" +
    //            "    $.ajaxSetup ({cache: false});" +
    //            "    $.colorbox({ returnFocus:true, href: 'frmContactsAddModify.aspx?iClientID=" + iClientID + "', opacity: 0.7, overlayClose:false, fixed:true, width:'610px', height:'650px', title:'<span class=\"\">" + strFormTitle + "</span>'  });" +
    //            "});", true);
    //    }
    //    catch { }
    //}

    //public static void ShowDomainForm(Page Page, String strFormTitle, String strDomainForm, int iHostID)
    //{
    //    Page.Session["DomainForm"] = strDomainForm;

    //    try
    //    {
    //        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "ShowDomainForm" + DateTime.Now.ToString("hh_mm_ss"),
    //            "$(document).ready(function() {" +
    //            "    $.ajaxSetup ({cache: false});" +
    //            "    $.colorbox({ returnFocus:true, href: 'frmDomainAddModify.aspx?iHostID=" + iHostID + "', opacity: 0.7, overlayClose:false, fixed:true, width:'610px', height:'650px', title:'<span class=\"\">" + strFormTitle + "</span>'  });" +
    //            "});", true);
    //    }
    //    catch { }
    //}

    //public static void ShowFullMessageForm(Page Page, String strFormTitle, String strContactForm, int iMessageID)
    //{
    //    Page.Session["ContactForm"] = strContactForm;

    //    try
    //    {
    //        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "ShowFullMessageForm" + DateTime.Now.ToString("hh_mm_ss"),
    //            "$(document).ready(function() {" +
    //            "    $.ajaxSetup ({cache: false});" +
    //            "    $.colorbox({ returnFocus:true, href: 'frmFullMessage.aspx?iMessageID=" + iMessageID + "', opacity: 0.7, overlayClose:false, fixed:true, width:'610px', height:'300px', title:'<span class=\"\">" + strFormTitle + "</span>'  });" +
    //            "});", true);
    //    }
    //    catch { }
    //}

    ////public static void ShowContactForm(Page Page, String strFormTitle, String strContactForm, int iClientID, int iContactID)
    ////{
    ////    Page.Session["ContactForm"] = strContactForm;

    ////    try
    ////    {
    ////        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "ShowContactForm",
    ////            "$(document).ready(function() {" +
    ////            "    $.ajaxSetup ({cache: false});" +
    ////            "    $.colorbox({ href: 'frmContactsAddModify.aspx?iClientID=" + iClientID + "&iContactID=" + iContactID + "', opacity: 0.7, overlayClose:false, fixed:true, width:'610px', height:'650px', title:'<span class=\"\">" + strFormTitle + "</span>'  });" +
    ////            "});", true);
    ////    }
    ////    catch { }
    ////}

    //public static void ShowContactForm(Page Page)
    //{
    //    ShowContactForm(Page, " Contact Form", "", 0);
    //}

    //public static void ShowDomainForm(Page Page)
    //{
    //    ShowContactForm(Page, " Domain Form", "Yes", 0);
    //}

    //#endregion

    #region ERROR MESSAGE

    public static void ShowAlertMessage(Page Page, String strErrorTitle, String strErrorMessage)
    {
        Page.Session["ErrorMessage"] = strErrorMessage;

        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "ShowAlertMessage" + DateTime.Now.ToString("hh_mm_ss"),
            "$(document).ready(function() {" +
            "    $.ajaxSetup ({cache: false});" +
            "    $.colorbox({ returnFocus:true, href: 'frmErrorMessage.aspx', opacity: 0.7, overlayClose:false, fixed:true, width:'510px', height:'170px', title:'<span class=\"ErrorBoxTitle\">" + strErrorTitle + "</span>'  });" +
            "});", true);
    }

    public static void ShowAlertMessage(Page Page)
    {
        ShowAlertMessage(Page, "Required Fields", "Sorry, not all required fields have been entered.<br />Please complete the form.");
    }

    public static void ShowAlertMessageDate(Page Page, String strErrorTitle, String strErrorMessage)
    {
        Page.Session["ErrorMessage"] = strErrorMessage;

        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "ShowAlertMessage" + DateTime.Now.ToString("hh_mm_ss"),
            "$(document).ready(function() {" +
            "    $.ajaxSetup ({cache: false});" +
            "    $.colorbox({ returnFocus:true, href: 'frmErrorMessage.aspx', opacity: 0.7, overlayClose:false, fixed:true, width:'510px', height:'170px', title:'<span class=\"ErrorBoxTitle\">" + strErrorTitle + "</span>'  });" +
            "});", true);
    }

    public static void ShowAlertMessageDate(Page Page)
    {
        ShowAlertMessageDate(Page, "Date Fields", "Sorry, there is a problem with the date fields that you have entered.");
    }

    public static void ShowAlertMessageRecordExists(Page Page, String strErrorTitle, String strErrorMessage)
    {
        Page.Session["ErrorMessage"] = strErrorMessage;

        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "ShowAlertMessage" + DateTime.Now.ToString("hh_mm_ss"),
            "$(document).ready(function() {" +
            "    $.ajaxSetup ({cache: false});" +
            "    $.colorbox({ returnFocus:true, href: 'frmErrorMessage.aspx', opacity: 0.7, overlayClose:false, fixed:true, width:'510px', height:'170px', title:'<span class=\"ErrorBoxTitle\">" + strErrorTitle + "</span>'  });" +
            "});", true);
    }

    public static void ShowAlertMessageRecordExists(Page Page)
    {
        ShowAlertMessageRecordExists(Page, "Record Exists", "Sorry, this user has already been added to the system.");
    }

    #endregion


    //public static void SendCompanyQuestionaire(int iMissionID, int iAccountUserID, string strAnswer)
    //{
    //    try
    //    {
    //        clsAccountUsers clsAccountUsers = new clsAccountUsers(iAccountUserID);
    //        clsMissions clsMissions = new clsMissions(iMissionID);

    //        string strAboutCompany = clsCompanyQuestionnaires.strCompanyProfile;
    //        string strService = clsCompanyQuestionnaires.strProductOrService;
    //        string strTargetAudience = clsCompanyQuestionnaires.strTargetAudience;
    //        string strCompetitors = clsCompanyQuestionnaires.strCompetitors;
    //        string strSetApart = clsCompanyQuestionnaires.strDifferentiateCompetition;

    //        string strQuote = File.ReadAllText(HttpContext.Current.Server.MapPath("~/templates/ClientQuestionnaire.html"))
    //        .Replace("{AboutCompany}", strAboutCompany.ToString())
    //        .Replace("{Service}", strService.ToString())
    //        .Replace("{TargetAudience}", strTargetAudience.ToString())
    //        .Replace("{Competitors}", strCompetitors.ToString())
    //        .Replace("{SetApart}", strSetApart.ToString());

    //        var htmlContent = strQuote;
    //        var htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter();
    //        var pdfBytes = htmlToPdf.GeneratePdf(htmlContent);
    //        // create an API client instance
    //        //pdfcrowd.Client client = new pdfcrowd.Client("SoftserveDigital", "add44351b2e10f4b9924b27ce03b5108");

    //        //// convert a web page and write the generated PDF to a memory stream
    //        //MemoryStream Stream = new MemoryStream();
    //        //client.convertURI(strQuote, Stream);

    //        //// set HTTP response headers
    //        //Response.Clear();
    //        //Response.AddHeader("Content-Type", "application/pdf");
    //        //Response.AddHeader("Cache-Control", "max-age=0");
    //        //Response.AddHeader("Accept-Ranges", "none");
    //        //Response.AddHeader("Content-Disposition", "attachment; filename=survey.pdf");

    //        string strQuoteTempPath = AppDomain.CurrentDomain.BaseDirectory + "\\PDF\\ClientQuestionnaire\\" + clsClients.strPrimaryContactName + clsCompanyQuestionnaires.iCompanyQuestionnaireID + ".pdf";

    //        using (FileStream fstream = new FileStream(strQuoteTempPath, FileMode.OpenOrCreate))
    //        {
    //            fstream.Write(pdfBytes, 0, pdfBytes.Length);
    //        }

    //        // send the generated PDF
    //        //Stream.WriteTo(Response.OutputStream);
    //        //Stream.Close();
    //        //    Response.Flush();
    //        //    Response.End();

    //        //if (HttpContext.Current.Session["clsClients"] != null)
    //        //{

    //        //string strQuote = File.ReadAllText(HttpContext.Current.Server.MapPath("~/templates/ClientQuestionnaire.html"))
    //        //.Replace("{AboutCompany}", strAboutCompany.ToString())
    //        //.Replace("{Service}", strService.ToString())
    //        //.Replace("{TargetAudience}", strTargetAudience.ToString())
    //        //.Replace("{Competitors}", strCompetitors.ToString())
    //        //.Replace("{SetApart}", strSetApart.ToString());

    //        //string strQuoteTempPath = AppDomain.CurrentDomain.BaseDirectory + "\\PDF\\ClientQuestionnaire\\" + clsClients.strPrimaryContactName + clsCompanyQuestionnaires.iCompanyQuestionnaireID + ".pdf";

    //        //try
    //        //{
    //        //    byte[] buffer = clsWkHtmlToPdf.HtmlToPdf(strQuote, HttpContext.Current.Server.MapPath("~/templates"));

    //        //    using (FileStream fstream = new FileStream(strQuoteTempPath, FileMode.OpenOrCreate))
    //        //    {
    //        //        fstream.Write(buffer, 0, buffer.Length);
    //        //    }
    //        //}
    //        //catch (Exception ex)
    //        //{
    //        //    throw ex;
    //        //}

    //        //### Send Ivoice mail

    //        string strFrom = "noreply@softservedigital.co.za";
    //        string strTo = clsClients.strPrimaryContactEmail;
    //        string strSubject = "Softserve Digital Development - Client Questionnaire";

    //        StringBuilder strContent = new StringBuilder();
    //        System.Net.Mail.Attachment[] attachments = new System.Net.Mail.Attachment[1];
    //        System.Net.Mail.Attachment attachment = new System.Net.Mail.Attachment(strQuoteTempPath);
    //        attachments[0] = attachment;

    //        strContent.AppendLine("<!-- Start of textbanner -->");
    //        strContent.AppendLine("<table width='100%' bgcolor='#fff' cellpadding='0' cellspacing='0' border='0' id='backgroundTable' movable=''>");
    //        strContent.AppendLine("<tbody>");
    //        strContent.AppendLine("<tr>");
    //        strContent.AppendLine("<td>");
    //        strContent.AppendLine("<table bgcolor='#ffffff' width='650' cellpadding='0' cellspacing='0' border='0' align='center' class='devicewidth' options=''>"); //style='border-left:1px solid #333; border-right:1px solid #333;'
    //        strContent.AppendLine("<tbody>");
    //        strContent.AppendLine("<!-- Spacing -->");
    //        strContent.AppendLine("<tr>");
    //        strContent.AppendLine("<td height='50'></td>");
    //        strContent.AppendLine("</tr>");
    //        strContent.AppendLine("<!-- End of Spacing -->");
    //        strContent.AppendLine("<tr>");
    //        strContent.AppendLine("<td>");
    //        strContent.AppendLine("<table width='100%' cellspacing='0' cellpadding='0'>");
    //        strContent.AppendLine("<tbody>");

    //        strContent.AppendLine("<!-- Content -->");
    //        strContent.AppendLine("<tr>");
    //        strContent.AppendLine("<td width='60'></td>");
    //        strContent.AppendLine("<td valign='top' style='font-family:Arial;font-size: 15px; color: #3b3b3b; text-align:left;line-height: 22px;' text=''>");
    //        strContent.AppendLine("Dear " + clsClients.strPrimaryContactName + ",<br /><br />");
    //        strContent.AppendLine("Please see your answers on the attached document.<br /><br/>");
    //        //strContent.AppendLine("Please see the attached document.");
    //        strContent.AppendLine("</td>");
    //        strContent.AppendLine("<td width='60'></td>");
    //        strContent.AppendLine("</tr>");
    //        strContent.AppendLine("<!-- End of Content -->");

    //        strContent.AppendLine("</td>");
    //        strContent.AppendLine("<td width='20'></td>");
    //        strContent.AppendLine("</tr>");
    //        strContent.AppendLine("</tbody>");
    //        strContent.AppendLine("</table>");
    //        strContent.AppendLine("</td>");
    //        strContent.AppendLine("</tr>");
    //        strContent.AppendLine("<!-- Spacing -->");
    //        strContent.AppendLine("<tr>");
    //        strContent.AppendLine("<td height='50'></td>");
    //        strContent.AppendLine("</tr>");
    //        strContent.AppendLine("<!-- End of Spacing -->");
    //        strContent.AppendLine("</tbody>");
    //        strContent.AppendLine("</table>");
    //        strContent.AppendLine("</td>");
    //        strContent.AppendLine("</tr>");
    //        strContent.AppendLine("</tbody>");
    //        strContent.AppendLine("</table>");
    //        strContent.AppendLine("<!-- End of textbanner -->");

    //        //emailComponent.SendMail(strFrom, strTo, "andrew@softservedigital.co.za", "hello@softservedigital.co.za", strSubject, strContent.ToString(), attachments, true);
    //        emailComponent.SendMail(strFrom, strTo, "hello@softservedigital.co.za", "", strSubject, strContent.ToString(), attachments, true);

    //        //### Admin Mail
    //        strContent.Clear();

    //        strContent.AppendLine("<!-- Start of textbanner -->");
    //        strContent.AppendLine("<table width='100%' bgcolor='#fff' cellpadding='0' cellspacing='0' border='0' id='backgroundTable' movable=''>");
    //        strContent.AppendLine("<tbody>");
    //        strContent.AppendLine("<tr>");
    //        strContent.AppendLine("<td>");
    //        strContent.AppendLine("<table bgcolor='#ffffff' width='650' cellpadding='0' cellspacing='0' border='0' align='center' class='devicewidth' options=''>"); //style='border-left:1px solid #333; border-right:1px solid #333;'
    //        strContent.AppendLine("<tbody>");
    //        strContent.AppendLine("<!-- Spacing -->");
    //        strContent.AppendLine("<tr>");
    //        strContent.AppendLine("<td height='50'></td>");
    //        strContent.AppendLine("</tr>");
    //        strContent.AppendLine("<!-- End of Spacing -->");
    //        strContent.AppendLine("<tr>");
    //        strContent.AppendLine("<td>");
    //        strContent.AppendLine("<table width='100%' cellspacing='0' cellpadding='0'>");
    //        strContent.AppendLine("<tbody>");

    //        strContent.AppendLine("<!-- Content -->");
    //        strContent.AppendLine("<tr>");
    //        strContent.AppendLine("<td width='60'></td>");
    //        strContent.AppendLine("<td valign='top' style='font-family:Arial;font-size: 15px; color: #3b3b3b; text-align:left;line-height: 22px;' text=''>");
    //        strContent.AppendLine("Dear admin,<br /><br />");
    //        strContent.AppendLine(clsClients.strPrimaryContactName + " has completed a Questionnaire<br /><br/>");
    //        strContent.AppendLine("Please see their answers on the attached document.");
    //        //strContent.AppendLine("Please see the attached document.");
    //        strContent.AppendLine("</td>");
    //        strContent.AppendLine("<td width='60'></td>");
    //        strContent.AppendLine("</tr>");
    //        strContent.AppendLine("<!-- End of Content -->");

    //        strContent.AppendLine("</td>");
    //        strContent.AppendLine("<td width='20'></td>");
    //        strContent.AppendLine("</tr>");
    //        strContent.AppendLine("</tbody>");
    //        strContent.AppendLine("</table>");
    //        strContent.AppendLine("</td>");
    //        strContent.AppendLine("</tr>");
    //        strContent.AppendLine("<!-- Spacing -->");
    //        strContent.AppendLine("<tr>");
    //        strContent.AppendLine("<td height='50'></td>");
    //        strContent.AppendLine("</tr>");
    //        strContent.AppendLine("<!-- End of Spacing -->");
    //        strContent.AppendLine("</tbody>");
    //        strContent.AppendLine("</table>");
    //        strContent.AppendLine("</td>");
    //        strContent.AppendLine("</tr>");
    //        strContent.AppendLine("</tbody>");
    //        strContent.AppendLine("</table>");
    //        strContent.AppendLine("<!-- End of textbanner -->");

    //        attachments = new System.Net.Mail.Attachment[1];
    //        attachment = new System.Net.Mail.Attachment(strQuoteTempPath);
    //        attachments[0] = attachment;
    //        emailComponent.SendMail(strFrom, "andrew@softservedigital.co.za", "hello@softservedigital.co.za", "hello@softservedigital.co.za", strSubject, strContent.ToString(), attachments, true);
    //    }
    //    //}
    //    catch
    //    {
    //        Exception ex;
    //    }
    //}
    //public static void CreateCertificateForEmail(int iAccountUsersID)
    //{
    //    try
    //    {
    //        clsAccountUsers clsAccountUsers = new clsAccountUsers(iAccountUsersID);

    //        string strName = clsAccountUsers.strFirstName + " " + clsAccountUsers.strSurname;

    //        string strCertificate = File.ReadAllText(HttpContext.Current.Server.MapPath("~/templates/CompleteCertificate.html"))
    //        .Replace("{AccountUserName}", strName.ToString());

    //        string strQuoteTempPath = AppDomain.CurrentDomain.BaseDirectory + "\\PDF\\CompleteCertificate\\" + clsAccountUsers.strSurname + "-" + clsAccountUsers.strFirstName + ".pdf";

    //        var htmlContent = strCertificate;
    //        var htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter();
    //        var pdfBytes = htmlToPdf.GeneratePdf(htmlContent);

    //        using (FileStream fstream = new FileStream(strQuoteTempPath, FileMode.OpenOrCreate))
    //        {
    //            fstream.Write(pdfBytes, 0, pdfBytes.Length);
    //        }

    //        StringBuilder strContent = new StringBuilder();
    //        System.Net.Mail.Attachment[] attachments = new System.Net.Mail.Attachment[1];
    //        System.Net.Mail.Attachment attachment = new System.Net.Mail.Attachment(strQuoteTempPath);
    //        attachments[0] = attachment;

    //        //emailComponent.SendMail(strFrom, strTo, "andrew@softservedigital.co.za", "hello@softservedigital.co.za", strSubject, strContent.ToString(), attachments, true);
    //        emailComponent.SendMail(strFrom, strTo, "hello@softservedigital.co.za", "", strSubject, strContent.ToString(), attachments, true);

    //        //### Admin Mail
    //        strContent.Clear();
           
    //    }
    //    catch (Exception ex)
    //    {
    //        throw ex;
    //    }
    //}

}