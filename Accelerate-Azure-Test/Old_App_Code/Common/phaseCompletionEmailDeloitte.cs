﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Configuration;
using System.Text;

/// <summary>
/// Summary description for emailComponent
/// </summary>
public class PhaseCompletionEmailDeloitte
{
    public PhaseCompletionEmailDeloitte()
    {
        //
        // TODO: Add constructor logic here
        //
    }


    #region Mail Methods

    public static void SendMail(string strFrom, string strTo, string strCC, string strBCC, string strSubject, string strContent, string strHeading, string strSubheading, Attachment[] atcAttatchedFiles, bool bUseGenericHTML)
    {
        //### Create an instance of the MailMessage class 
        using (MailMessage myMail = new MailMessage())
        {
            //### Set the subject            
            myMail.Subject = strSubject;

            //### Send To / From / BCC
            myMail.From = new MailAddress(strFrom);
            myMail.To.Add(strTo);

            //myMail.Bcc.Add("hello@softservedigital.co.za");

            //### Add Attachments to message object
            if (atcAttatchedFiles != null)
            {
                foreach (Attachment atcAttachedFile in atcAttatchedFiles)
                {
                    myMail.Attachments.Add(atcAttachedFile);
                }
            }

            //### Assign the content to the mail body
            myMail.IsBodyHtml = true;

            string strHtmlBody = "";

            if (bUseGenericHTML == true)
                strHtmlBody = SendMailGenericHTML(strContent, strSubject, strHeading, strSubheading);
            else
                strHtmlBody += strContent;

            //clsMissions clsMission = new clsMissions(1);

            //string strImage = getList(clsMission.strPathToImages);

            //strImage = strImage.Replace("../", AppDomain.CurrentDomain.BaseDirectory + "\\");

            string strPathToLogo = AppDomain.CurrentDomain.BaseDirectory + @"\images\DeloitteEmailIamge.png";
            string strDeloitteEmailBanner = AppDomain.CurrentDomain.BaseDirectory + @"\images\DeloitteEmailBanner.png";
            //string strPathToGetInTouchImage = AppDomain.CurrentDomain.BaseDirectory + @"\images\getintouch.png";
            string strFacebookEmailLinkThumb = AppDomain.CurrentDomain.BaseDirectory + @"\images\eFacebookEmailLinkThumb.png";
            string strTwitterEmailLinkThumb = AppDomain.CurrentDomain.BaseDirectory + @"\images\eTwitterEmailLinkThumb.png";
            string strLinkedInEmailLinkThumb = AppDomain.CurrentDomain.BaseDirectory + @"\images\eLinkedInEmailLinkThumb.png";
            string strGooglePEmailLinkThumb = AppDomain.CurrentDomain.BaseDirectory + @"\images\eGooglePEmailLinkThumb.png";
            string strPinInEmailLinkThumb = AppDomain.CurrentDomain.BaseDirectory + @"\images\ePinInEmailLinkThumb.png";
            string strMailEmailLinkThumb = AppDomain.CurrentDomain.BaseDirectory + @"\images\eMailEmailLinkThumb.png";


            AlternateView HTMLEmail = AlternateView.CreateAlternateViewFromString(strHtmlBody, null, "text/html");
            LinkedResource MyImage = new LinkedResource(strPathToLogo);
            LinkedResource DeloitteEmailBanner = new LinkedResource(strDeloitteEmailBanner);
            //LinkedResource GetInTouchImage = new LinkedResource(strPathToGetInTouchImage);
            LinkedResource FacebookEmailLinkThumb = new LinkedResource(strFacebookEmailLinkThumb);
            LinkedResource TwitterEmailLinkThumb = new LinkedResource(strTwitterEmailLinkThumb);
            LinkedResource LinkedInEmailLinkThumb = new LinkedResource(strLinkedInEmailLinkThumb);
            LinkedResource GooglePEmailLinkThumb = new LinkedResource(strGooglePEmailLinkThumb);
            LinkedResource PinInEmailLinkThumb = new LinkedResource(strPinInEmailLinkThumb);
            LinkedResource MailEmailLinkThumb = new LinkedResource(strMailEmailLinkThumb);

            //### <img src="cid:InlineImageID" />
            MyImage.ContentId = "logo";
            DeloitteEmailBanner.ContentId = "DeloitteEmailBanner";
            //GetInTouchImage.ContentId = "getintouch";
            FacebookEmailLinkThumb.ContentId = "FacebookEmailLinkThumb";
            TwitterEmailLinkThumb.ContentId = "TwitterEmailLinkThumb";
            LinkedInEmailLinkThumb.ContentId = "LinkedInEmailLinkThumb";
            GooglePEmailLinkThumb.ContentId = "GooglePEmailLinkThumb";
            PinInEmailLinkThumb.ContentId = "PinInEmailLinkThumb";
            MailEmailLinkThumb.ContentId = "MailEmailLinkThumb";

            //### Add this linked resource to HTML view
            HTMLEmail.LinkedResources.Add(MyImage);
            HTMLEmail.LinkedResources.Add(DeloitteEmailBanner);
            //HTMLEmail.LinkedResources.Add(GetInTouchImage);
            HTMLEmail.LinkedResources.Add(FacebookEmailLinkThumb);
            HTMLEmail.LinkedResources.Add(TwitterEmailLinkThumb);
            HTMLEmail.LinkedResources.Add(LinkedInEmailLinkThumb);
            HTMLEmail.LinkedResources.Add(GooglePEmailLinkThumb);
            HTMLEmail.LinkedResources.Add(PinInEmailLinkThumb);
            HTMLEmail.LinkedResources.Add(MailEmailLinkThumb);

            myMail.AlternateViews.Add(HTMLEmail);

            //SmtpClient emailClient = new SmtpClient("mail.softservedigital.co.za");
            //emailClient.Port = 587;
            //emailClient.Credentials = new System.Net.NetworkCredential("test@softservedigital.co.za", "12345%$#@!AAA");

            //### Password protected

            //emailClient.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["Credientials"], ConfigurationManager.AppSettings["Password"]);
            //emailClient.Credentials = new System.Net.NetworkCredential("noreply@softservedigital.co.za", "N0R3PLY123!");

            //string strCredientials = ;

            //SmtpClient emailClient = new SmtpClient(ConfigurationManager.AppSettings["SMTP"]);
            //emailClient.Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]);

            //if (strCredientials != null && strCredientials != "")
            //{
            //    string strUsername = "";
            //    string strPassword = "";
            //    int iCount = 0;
            //    List<string> lstCredentials = strCredientials.Split(';').ToList();
            //    foreach (string s in lstCredentials)
            //    {
            //        if (iCount == 0)
            //        {
            //            strUsername = s;
            //            int iLocation = strUsername.LastIndexOf('=');
            //            strUsername = strUsername.Remove(0, iLocation + 1);
            //        }
            //        else
            //        {
            //            strPassword = s;
            //            int iLocation = strPassword.LastIndexOf('=');
            //            strPassword = strPassword.Remove(0, iLocation + 1);
            //        }

            //        iCount++;
            //    }
            //    emailClient.Credentials = new System.Net.NetworkCredential(strUsername, strPassword);
            //}

            //### ORIGINAL ###
            //SmtpClient emailClient = new SmtpClient(ConfigurationManager.AppSettings["SMTP"]);
            //emailClient.Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]);

            //### LOCAL LIVE VERSION TESTING ###
            SmtpClient emailClient = new SmtpClient(ConfigurationManager.AppSettings["SMTP"]);
            emailClient.Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]);
            emailClient.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["EmailAddress"], ConfigurationManager.AppSettings["EmailPassword"]);

            //### TEST ###
            //SmtpClient emailClient = new SmtpClient("41.185.13.123");
            //emailClient.Port = 25;
            //emailClient.Credentials = new System.Net.NetworkCredential("noreply@softservedigital.co.za", "N0r3ply123#");

            //### Now, to send the message, use the Send method of the SmtpMail class 
            emailClient.Send(myMail);
            myMail.Dispose();
        }
    }

    private static String SendMailGenericHTML(String strContent, String strSubject, String strHeading, String strSubheading)
    {
        //### This is a generic email function that accepts the strFrom, strTo, strSubject and strContent
        //### This will simplify the management of email sending

        //### Mail settings for the template
        //string strPathToLogo = "";
        //string strURL = "" + System.Configuration.ConfigurationManager.AppSettings["WebRoot"] + "";
        //string strClientName = "The accelerate Online Team";
        //string strColor = "#003264";

        StringBuilder strbMailBuilder = new StringBuilder();
        DateTime dateTime = DateTime.Now;

        //strbMailBuilder.AppendLine("<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\"><title>" + strSubject + "</title></head><body style=\"background-color:#f6f5f5;color: " + strColor + "\">");
        //strbMailBuilder.AppendLine("<center>");

        ////### This code is added here as it is generic throughout email sending
        //strbMailBuilder.AppendLine("<table cellpadding=\"2\" cellspacing=\"2\" style=\"font-family:Arial; font-size:12px; width:600px;\">");
        //strbMailBuilder.AppendLine("<tr><td><img src=\"cid:logo\" alt=\"" + strClientName + "\" title=\"" + strClientName + "\" /></td></tr>");
        //strbMailBuilder.AppendLine("<tr><td style=\"background:" + strColor + "; color:#f6f5f5; font-weight:bold; padding:10px; text-align: center;\">" + strSubject + "</td></tr>");
        //strbMailBuilder.AppendLine("<tr><td style=\"width: 100%; background-color: " + strColor + "; padding: 3px;\"></td></tr>");
        //strbMailBuilder.AppendLine("<tr><td>");
        //strbMailBuilder.AppendLine(strContent);
        //strbMailBuilder.AppendLine("</td></tr>");
        //strbMailBuilder.AppendLine("<tr><td style=\"width: 100%; background-color: " + strColor + " padding: 3px;\"></td></tr>");
        //strbMailBuilder.AppendLine("<tr><td style=\"font-family:Arial; font-size:14px;\">Yours in Roses,<br /><b><a href=\"" + strURL + "\" style=\"text-decoration:none;color:#000;\">" + strClientName + "</a></b></td></tr>");

        //strbMailBuilder.AppendLine("</table>");
        //strbMailBuilder.AppendLine("</center>");
        //strbMailBuilder.AppendLine("</body></html>");

        strbMailBuilder.AppendLine("<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\"><title>" + strSubject + "</title>");

        strbMailBuilder.AppendLine("<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>");
        strbMailBuilder.AppendLine("<style type='text/css'>");
        strbMailBuilder.AppendLine("/* Client-specific Styles */");
        strbMailBuilder.AppendLine("#outlook a {padding:0;} /* Force Outlook to provide a 'view in browser' menu link. */");
        strbMailBuilder.AppendLine("body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;}");
        strbMailBuilder.AppendLine("/* Prevent Webkit and Windows Mobile platforms from changing default font sizes, while not breaking desktop design. */");
        strbMailBuilder.AppendLine(".ExternalClass {width:100%;} /* Force Hotmail to display emails at full width */");
        strbMailBuilder.AppendLine(".ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;} /* Force Hotmail to display normal line spacing.  More on that: http://www.emailonacid.com/forum/viewthread/43/ */");
        strbMailBuilder.AppendLine("#backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important;}");
        strbMailBuilder.AppendLine("img {outline:none; text-decoration:none;border:none; -ms-interpolation-mode: bicubic;}");
        strbMailBuilder.AppendLine("a img {border:none;}");
        strbMailBuilder.AppendLine(".image_fix {display:block;}");
        strbMailBuilder.AppendLine("p {margin: 0px 0px !important;}");
        strbMailBuilder.AppendLine("h1, h2, h3, h4, h5, h6 {color: #6c7480 !important;}");
        strbMailBuilder.AppendLine("h1 a, h2 a, h3 a, h4 a, h5 a, h6 a {color: #33cc66 !important;}");
        strbMailBuilder.AppendLine("h1 a:active, h2 a:active,  h3 a:active, h4 a:active, h5 a:active, h6 a:active {");
        strbMailBuilder.AppendLine("color: red !important; ");
        strbMailBuilder.AppendLine("}");
        strbMailBuilder.AppendLine("h1 a:visited, h2 a:visited,  h3 a:visited, h4 a:visited, h5 a:visited, h6 a:visited {");
        strbMailBuilder.AppendLine("color: purple !important; ");
        strbMailBuilder.AppendLine("}");
        strbMailBuilder.AppendLine("table td {border-collapse: collapse;}");
        strbMailBuilder.AppendLine("table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }");
        strbMailBuilder.AppendLine("a {color: #33cc66;text-decoration: none;text-decoration:none!important;}");
        strbMailBuilder.AppendLine("/*STYLES*/");
        strbMailBuilder.AppendLine("table[class=full] { width: 100%; clear: both; }");
        strbMailBuilder.AppendLine("table[class=button] {");
        strbMailBuilder.AppendLine("border-width:1px!important;border-style:solid!important;");
        strbMailBuilder.AppendLine("border-top-width: 1px!important;");
        strbMailBuilder.AppendLine("border-right-width: 1px!important;");
        strbMailBuilder.AppendLine("border-bottom-width: 1px!important;");
        strbMailBuilder.AppendLine("border-left-width: 1px!important;");
        strbMailBuilder.AppendLine("}");
        strbMailBuilder.AppendLine("/*IPAD STYLES*/");
        strbMailBuilder.AppendLine("@media only screen and (max-width: 640px) {");
        strbMailBuilder.AppendLine("a[href^='tel'], a[href^='sms'] {");
        strbMailBuilder.AppendLine("text-decoration: none;");
        strbMailBuilder.AppendLine("color: #33cc66; /* or whatever your want */");
        strbMailBuilder.AppendLine("pointer-events: none;");
        strbMailBuilder.AppendLine("cursor: default;");
        strbMailBuilder.AppendLine("}");
        strbMailBuilder.AppendLine(".mobile_link a[href^='tel'], .mobile_link a[href^='sms'] {");
        strbMailBuilder.AppendLine("text-decoration: default;");
        strbMailBuilder.AppendLine("color: #33cc66 !important;");
        strbMailBuilder.AppendLine("pointer-events: auto;");
        strbMailBuilder.AppendLine("cursor: default;");
        strbMailBuilder.AppendLine("}");
        strbMailBuilder.AppendLine("table[class=devicewidth] {width: 440px!important;text-align:center!important;}");
        strbMailBuilder.AppendLine("td[class=text-Center] {width: 100%;text-align: center!important;clear: both;}");
        strbMailBuilder.AppendLine("td[class=menu] {width: 100%;height:40px!important;text-align: center!important;clear: both;}");
        strbMailBuilder.AppendLine("img[class=banner] {width: 440px!important;height:191px!important;}");
        strbMailBuilder.AppendLine("td[class=w20]{width:10px!important}");
        strbMailBuilder.AppendLine("table[class=image-banner]{width:440px!important;height: 73px!important}");
        strbMailBuilder.AppendLine("td[class=image-banner]{width:440px!important;height: 73px!important}");
        strbMailBuilder.AppendLine("img[class=image-banner]{width:440px!important;height: 73px!important}");
        strbMailBuilder.AppendLine("table[class=emhide]{display:none!important;}");
        strbMailBuilder.AppendLine("}");
        strbMailBuilder.AppendLine("/*IPHONE STYLES*/");
        strbMailBuilder.AppendLine("@media only screen and (max-width: 480px) {");
        strbMailBuilder.AppendLine("a[href^='tel'], a[href^='sms'] {");
        strbMailBuilder.AppendLine("text-decoration: none;");
        strbMailBuilder.AppendLine("color: #33cc66; /* or whatever your want */");
        strbMailBuilder.AppendLine("pointer-events: none;");
        strbMailBuilder.AppendLine("cursor: default;");
        strbMailBuilder.AppendLine("}");
        strbMailBuilder.AppendLine(".mobile_link a[href^='tel'], .mobile_link a[href^='sms'] {");
        strbMailBuilder.AppendLine("text-decoration: default;");
        strbMailBuilder.AppendLine("color: #33cc66 !important; ");
        strbMailBuilder.AppendLine("pointer-events: auto;");
        strbMailBuilder.AppendLine("cursor: default;");
        strbMailBuilder.AppendLine("}");
        strbMailBuilder.AppendLine("table[class=devicewidth] {width: 280px!important;text-align:center!important;}");
        strbMailBuilder.AppendLine("td[class=text-Center] {width: 100%;text-align: center!important;clear: both;}");
        strbMailBuilder.AppendLine("td[class=menu] {width: 100%;height:40px!important;text-align: center!important;clear: both;}");
        strbMailBuilder.AppendLine("img[class=banner] {width: 280px!important;height:121px!important;}");
        strbMailBuilder.AppendLine("td[class=w20]{width:10px!important}");
        strbMailBuilder.AppendLine("table[class=image-banner]{width:280px!important;height: 47px!important}");
        strbMailBuilder.AppendLine("td[class=image-banner]{width:280px!important;height: 47px!important}");
        strbMailBuilder.AppendLine("img[class=image-banner]{width:280px!important;height: 47px!important}");
        strbMailBuilder.AppendLine("table[class=emhide]{display:none!important;}");
        strbMailBuilder.AppendLine("}");
        //strbMailBuilder.AppendLine(".arrow-down {width: 0; height: 0; border-left: 20px solid transparent; border-right: 20px solid transparent; border-top: 20px solid #2d2d2d;");
        //strbMailBuilder.AppendLine("}");
        strbMailBuilder.AppendLine("</style>");
        strbMailBuilder.AppendLine("</head>");

        strbMailBuilder.AppendLine("<body style='-webkit-user-select: none;'>");

        strbMailBuilder.AppendLine("<div class='canvas ui-sortable'>");

        strbMailBuilder.AppendLine("<!-- Start of Email -->");
        strbMailBuilder.AppendLine("<table width='100%' bgcolor='#f2f2f2' cellpadding='0' cellspacing='0' border='0' id='backgroundTable' movable=''>");
        strbMailBuilder.AppendLine("<tbody>");
        strbMailBuilder.AppendLine("<tr>");
        strbMailBuilder.AppendLine("<td>");

        strbMailBuilder.AppendLine("<!-- Start of Content -->");

        strbMailBuilder.AppendLine("<!-- logo -->");
        strbMailBuilder.AppendLine("<table width='100%' bgcolor='#000000' cellpadding='0' cellspacing='0' border='0'  border='none' class='devicewidth' options=''>");
        strbMailBuilder.AppendLine("<tbody>");
        strbMailBuilder.AppendLine("<tr>");
        strbMailBuilder.AppendLine("<td>");

        strbMailBuilder.AppendLine("<table width='650' bgcolor='#000000' cellpadding='0' cellspacing='0' border='0'  border='none' align='center' class='devicewidth' options=''>");
        strbMailBuilder.AppendLine("<tbody>");
        strbMailBuilder.AppendLine("<tr>");
        strbMailBuilder.AppendLine("<td>");

        strbMailBuilder.AppendLine("<table width='650' bgcolor='#000000' cellpadding='0' cellspacing='0' border='0' align='left' class='devicewidth'>");
        strbMailBuilder.AppendLine("<tbody>");
        strbMailBuilder.AppendLine("<tr>");
        strbMailBuilder.AppendLine("<td height='65'>");
        strbMailBuilder.AppendLine("<img src=\"cid:DeloitteEmailBanner\" alt='logo' border='0' style='display:block; border:none; outline:none; text-decoration:none;'/>");
        strbMailBuilder.AppendLine("</td>");
        strbMailBuilder.AppendLine("</tr>");
        strbMailBuilder.AppendLine("</tbody>");
        strbMailBuilder.AppendLine("</table>");

        strbMailBuilder.AppendLine("</td>");
        strbMailBuilder.AppendLine("</tr>");
        strbMailBuilder.AppendLine("</tbody>");
        strbMailBuilder.AppendLine("</table>");

        strbMailBuilder.AppendLine("</td>");
        strbMailBuilder.AppendLine("</tr>");
        strbMailBuilder.AppendLine("</tbody>");
        strbMailBuilder.AppendLine("</table>");
        strbMailBuilder.AppendLine("<!-- End of logo -->");

        strbMailBuilder.AppendLine("<!-- Spacing -->");
        strbMailBuilder.AppendLine("<table width='100%' height='20' cellpadding='0' cellspacing='0' border='0'  border='none' align='center' class='devicewidth' options=''>");
        strbMailBuilder.AppendLine("<tbody>");
        strbMailBuilder.AppendLine("<tr>");
        strbMailBuilder.AppendLine("<td></td>");
        strbMailBuilder.AppendLine("</tr>");
        strbMailBuilder.AppendLine("</tbody>");
        strbMailBuilder.AppendLine("</table>");
        strbMailBuilder.AppendLine("<!-- Spacing -->");

        strbMailBuilder.AppendLine("<!-- Start of Note -->");
        strbMailBuilder.AppendLine("<table width='650' cellpadding='0' cellspacing='0' border='0'  border='none' align='center' class='devicewidth' options=''>");
        strbMailBuilder.AppendLine("<tbody>");
        strbMailBuilder.AppendLine("<tr>");
        strbMailBuilder.AppendLine("<td>");


        strbMailBuilder.AppendLine("<table width='650' cellpadding='0' cellspacing='0' border='0' align='left' class='devicewidth'>");
        strbMailBuilder.AppendLine("<tbody>");
        strbMailBuilder.AppendLine("<tr>");
        strbMailBuilder.AppendLine("<td>");
        strbMailBuilder.AppendLine("<br/><p style='color:gray; font-family:Verdana; font-size:11px;'> South Africa  |  Talent & Transformation  |  " + DateTime.Now.ToString("dd MMMM yyyy") + " </p><br/><br/>");
        strbMailBuilder.AppendLine("</td>");
        strbMailBuilder.AppendLine("</tr>");
        strbMailBuilder.AppendLine("</tbody>");
        strbMailBuilder.AppendLine("</table>");


        strbMailBuilder.AppendLine("</td>");
        strbMailBuilder.AppendLine("</tr>");
        strbMailBuilder.AppendLine("</tbody>");
        strbMailBuilder.AppendLine("</table>");
        strbMailBuilder.AppendLine("<!-- End of Note -->");

        strbMailBuilder.AppendLine("<!-- Spacing -->");
        strbMailBuilder.AppendLine("<table width='100%' height='40' cellpadding='0' cellspacing='0' border='0'  border='none' align='center' class='devicewidth' options=''>");
        strbMailBuilder.AppendLine("<tbody>");
        strbMailBuilder.AppendLine("<tr>");
        strbMailBuilder.AppendLine("<td></td>");
        strbMailBuilder.AppendLine("</tr>");
        strbMailBuilder.AppendLine("</tbody>");
        strbMailBuilder.AppendLine("</table>");
        strbMailBuilder.AppendLine("<!-- Spacing -->");

        strbMailBuilder.AppendLine("<!-- Start of 2nd Banner -->");
        strbMailBuilder.AppendLine("<table width='650' cellpadding='0' cellspacing='0' border='0'  border='none' align='center' options=''>");
        strbMailBuilder.AppendLine("<tbody>");
        strbMailBuilder.AppendLine("<tr>");
        strbMailBuilder.AppendLine("<td>");


        strbMailBuilder.AppendLine("<table width='650' cellpadding='0' cellspacing='0' border='0' align='left'>");
        strbMailBuilder.AppendLine("<tbody>");
        strbMailBuilder.AppendLine("<tr>");
        strbMailBuilder.AppendLine("<td>");
        strbMailBuilder.AppendLine("<img src=\"cid:logo\" width='650' height='240' alt='logo' border='0' style='display:block; border:none; outline:none; text-decoration:none;'/>");
        strbMailBuilder.AppendLine("</td>");
        strbMailBuilder.AppendLine("</tr>");
        strbMailBuilder.AppendLine("</tbody>");
        strbMailBuilder.AppendLine("</table>");


        strbMailBuilder.AppendLine("</td>");
        strbMailBuilder.AppendLine("</tr>");
        strbMailBuilder.AppendLine("</tbody>");
        strbMailBuilder.AppendLine("</table>");
        strbMailBuilder.AppendLine("<!-- End of 2nd Banner -->");

        strbMailBuilder.AppendLine("<!-- Spacing -->");
        //strbMailBuilder.AppendLine("<table width='100%' height='20' cellpadding='0' cellspacing='0' border='0'  border='none' align='center' class='devicewidth' options=''>");
        //strbMailBuilder.AppendLine("<tbody>");
        //strbMailBuilder.AppendLine("<tr>");
        //strbMailBuilder.AppendLine("<td></td>");
        //strbMailBuilder.AppendLine("</tr>");
        //strbMailBuilder.AppendLine("</tbody>");
        //strbMailBuilder.AppendLine("</table>");
        strbMailBuilder.AppendLine("<!-- Spacing -->");

        strbMailBuilder.AppendLine("<!-- Start of Text -->");
        strbMailBuilder.AppendLine("<table width='650' cellpadding='0' cellspacing='0' style='border-top: 2px solid #f2f2f2;' align='center' class='devicewidth' options=''>");
        strbMailBuilder.AppendLine("<tbody>");
        strbMailBuilder.AppendLine("<tr>");
        strbMailBuilder.AppendLine("<td>");


        strbMailBuilder.AppendLine("<table width='650' bgcolor='#ffffff' cellpadding='0' cellspacing='0' border='0' align='left' class='devicewidth'>");
        strbMailBuilder.AppendLine("<tbody>");
        strbMailBuilder.AppendLine("<tr>");
        strbMailBuilder.AppendLine("<td>");
        strbMailBuilder.AppendLine(strContent);
        strbMailBuilder.AppendLine("</td>");
        strbMailBuilder.AppendLine("</tr>");
        strbMailBuilder.AppendLine("</tbody>");
        strbMailBuilder.AppendLine("</table>");


        strbMailBuilder.AppendLine("</td>");
        strbMailBuilder.AppendLine("</tr>");
        strbMailBuilder.AppendLine("</tbody>");
        strbMailBuilder.AppendLine("</table>");
        strbMailBuilder.AppendLine("<!-- End of Text -->");

        strbMailBuilder.AppendLine("<!-- Spacing -->");
        strbMailBuilder.AppendLine("<table width='100%' height='20' bgcolor='#f2f2f2' cellpadding='0' cellspacing='0' border='0'  border='none' align='center' class='devicewidth' options=''>");
        strbMailBuilder.AppendLine("<tbody>");
        strbMailBuilder.AppendLine("<tr>");
        strbMailBuilder.AppendLine("<td></td>");
        strbMailBuilder.AppendLine("</tr>");
        strbMailBuilder.AppendLine("</tbody>");
        strbMailBuilder.AppendLine("</table>");
        strbMailBuilder.AppendLine("<!-- Spacing -->");

        strbMailBuilder.AppendLine("<!-- Start of Social Media -->");
        strbMailBuilder.AppendLine("<table width='650' bgcolor='#ffffff' cellpadding='0' cellspacing='0' border='0'  border='none' align='center' class='devicewidth' options=''>");
        strbMailBuilder.AppendLine("<tbody>");
        strbMailBuilder.AppendLine("<tr height='50'>");
        strbMailBuilder.AppendLine("<td>");


        strbMailBuilder.AppendLine("<table width='400' bgcolor='#ffffff' cellpadding='0' cellspacing='0' border='0' align='center' class='devicewidth'>");
        strbMailBuilder.AppendLine("<tbody>");
        strbMailBuilder.AppendLine("<tr style='column-span:6'>");

        strbMailBuilder.AppendLine("<td style='text-align:center;'>");
        strbMailBuilder.AppendLine("<a href='https://www.facebook.com/deloitte'><img src=\"cid:FacebookEmailLinkThumb\" width='32' height='32' alt='Facebook' /></a>");
        strbMailBuilder.AppendLine("</td>");
        strbMailBuilder.AppendLine("<td style='text-align:center;'>");
        strbMailBuilder.AppendLine("<a href='https://twitter.com/Deloitte'><img src=\"cid:TwitterEmailLinkThumb\" width='32' height='32' alt='Twitter' /></a>");
        strbMailBuilder.AppendLine("</td>");
        strbMailBuilder.AppendLine("<td style='text-align:center;'>");
        strbMailBuilder.AppendLine("<a href='http://www.linkedin.com/company/deloitte'><img src=\"cid:LinkedInEmailLinkThumb\" width='32' height='32' alt='LinkedIn' /></a>");
        strbMailBuilder.AppendLine("</td>");
        strbMailBuilder.AppendLine("<td style='text-align:center;'>");
        strbMailBuilder.AppendLine("<a href='https://plus.google.com/+Deloitte/posts'><img src=\"cid:GooglePEmailLinkThumb\" width='32' height='32' alt='Google' /></a>");
        strbMailBuilder.AppendLine("</td>");
        strbMailBuilder.AppendLine("<td style='text-align:center;'>");
        strbMailBuilder.AppendLine("<a href='http://www.pinterest.com/deloitte/'><img src=\"cid:PinInEmailLinkThumb\" width='32' height='32' alt='PinInterest' /></a>");
        strbMailBuilder.AppendLine("</td>");
        strbMailBuilder.AppendLine("<td style='text-align:center;'>");
        strbMailBuilder.AppendLine("<a href='#'><img src=\"cid:MailEmailLinkThumb\" width='32' height='32' alt='Mail' /></a>");
        strbMailBuilder.AppendLine("</td>");

        strbMailBuilder.AppendLine("</tr>");
        strbMailBuilder.AppendLine("</tbody>");
        strbMailBuilder.AppendLine("</table>");


        strbMailBuilder.AppendLine("</td>");
        strbMailBuilder.AppendLine("</tr>");
        strbMailBuilder.AppendLine("</tbody>");
        strbMailBuilder.AppendLine("</table>");
        strbMailBuilder.AppendLine("<!-- End of Social Media -->");

        strbMailBuilder.AppendLine("<!-- Spacing -->");
        strbMailBuilder.AppendLine("<table width='100%' height='20' bgcolor='#f2f2f2' cellpadding='0' cellspacing='0' border='0'  border='none' align='center' class='devicewidth' options=''>");
        strbMailBuilder.AppendLine("<tbody>");
        strbMailBuilder.AppendLine("<tr>");
        strbMailBuilder.AppendLine("<td></td>");
        strbMailBuilder.AppendLine("</tr>");
        strbMailBuilder.AppendLine("</tbody>");
        strbMailBuilder.AppendLine("</table>");
        strbMailBuilder.AppendLine("<!-- Spacing -->");

        strbMailBuilder.AppendLine("<!-- Start of Footer -->");
        strbMailBuilder.AppendLine("<table width='650' cellpadding='0' bgcolor='#ffffff' cellspacing='0' border='0'  border='none' align='center' class='devicewidth' options=''>");
        strbMailBuilder.AppendLine("<tbody>");
        strbMailBuilder.AppendLine("<tr>");
        strbMailBuilder.AppendLine("<td>");


        strbMailBuilder.AppendLine("<table width='650' bgcolor='#ffffff' cellpadding='0' cellspacing='0' border='0' align='center' class='devicewidth'>");
        strbMailBuilder.AppendLine("<tbody>");
        strbMailBuilder.AppendLine("<tr>");
        strbMailBuilder.AppendLine("<td>");
        strbMailBuilder.AppendLine("<table width='650' bgcolor='#ffffff' cellpadding='30' cellspacing='0' border='0'  border='none' align='center' class='devicewidth' options=''>"); //style='border-left:1px solid #333; border-right:1px solid #333;'
        strbMailBuilder.AppendLine("<tbody>");
        strbMailBuilder.AppendLine("<tr>");
        strbMailBuilder.AppendLine("<td valign='top' style='color:gray; font-family:Verdana; font-size:10px; text-align:left;' text=''>");
        strbMailBuilder.AppendLine("Deloitte refers to one or more of Deloitte Touche Tohmatsu Limited, a UK private ");
        strbMailBuilder.AppendLine("company limited by guarantee (DTTL), its network of member firms and their related ");
        strbMailBuilder.AppendLine("entities. DTTL and each of its member firms are legally separate and independent ");
        strbMailBuilder.AppendLine("entities. DTTL (also referred to as “Deloitte Global”) does not provide services to ");
        strbMailBuilder.AppendLine("clients. Please see www.deloitte.com/about for a more detailed description of DTTL and its member firms.<br/><br/>");

        strbMailBuilder.AppendLine("This communication is for internal distribution and use only among personnel of ");
        strbMailBuilder.AppendLine("Deloitte Touche Tohmatsu Limited, its member firms, and their related entities ");
        strbMailBuilder.AppendLine("(collectively, the “Deloitte network”). None of the Deloitte network shall be responsible ");
        strbMailBuilder.AppendLine("for any loss whatsoever sustained by any person who relies on this communication.<br/><br/>");

        strbMailBuilder.AppendLine("© 2016. For information, contact Deloitte Touche Tohmatsu Limited<br/><br/>");

        strbMailBuilder.AppendLine("<p style='color:black; font-family:Verdana; font-size:9px;'>To no longer receive emails about this topic please send a return email to the sender with the word “Unsubscribe” in the subject line.</p>");

        strbMailBuilder.AppendLine("</td>");
        strbMailBuilder.AppendLine("</tr>");
        strbMailBuilder.AppendLine("</tbody>");
        strbMailBuilder.AppendLine("</table>");
        strbMailBuilder.AppendLine("</td>");
        strbMailBuilder.AppendLine("</tr>");
        strbMailBuilder.AppendLine("</tbody>");
        strbMailBuilder.AppendLine("</table>");


        strbMailBuilder.AppendLine("</td>");
        strbMailBuilder.AppendLine("</tr>");
        strbMailBuilder.AppendLine("</tbody>");
        strbMailBuilder.AppendLine("</table>");
        strbMailBuilder.AppendLine("<!-- End of Footer -->");

        strbMailBuilder.AppendLine("<!-- Spacing -->");
        strbMailBuilder.AppendLine("<table width='100%' height='10' bgcolor='#f2f2f2' cellpadding='0' cellspacing='0' border='0'  border='none' align='center' class='devicewidth' options=''>");
        strbMailBuilder.AppendLine("<tbody>");
        strbMailBuilder.AppendLine("<tr>");
        strbMailBuilder.AppendLine("<td></td>");
        strbMailBuilder.AppendLine("</tr>");
        strbMailBuilder.AppendLine("</tbody>");
        strbMailBuilder.AppendLine("</table>");
        strbMailBuilder.AppendLine("<!-- Spacing -->");

        strbMailBuilder.AppendLine("<!-- End of Content -->");


        strbMailBuilder.AppendLine("</td>");
        strbMailBuilder.AppendLine("</tr>");
        strbMailBuilder.AppendLine("</tbody>");
        strbMailBuilder.AppendLine("</table>");

        strbMailBuilder.AppendLine("<!-- End of eMail -->");

        strbMailBuilder.AppendLine("</div>");

        strbMailBuilder.AppendLine("</body>");
        strbMailBuilder.AppendLine("</html>");

        return strbMailBuilder.ToString();
    }

    #endregion
}