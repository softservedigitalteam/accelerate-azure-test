﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Accelerate_Video_Missions : System.Web.UI.Page
{
    DataTable dtMissionVideos = new DataTable();
    clsAccountUsers clsAccountUsers = new clsAccountUsers();

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsAccountUsers"] == null)
        {
            //### Redirect back to login
            Response.Redirect("Accelerate-Login.aspx");
        }
        clsAccountUsers = (clsAccountUsers)Session["clsAccountUsers"];

        if (!IsPostBack)
        {
            try
            {
                popPhases();
            }
            catch (Exception ex)
            {
                Response.Redirect("Accelerate-Error404.aspx");
            }
        }
    }

    protected void PopulateMissions(string strPhaseList)
    {
        if (!string.IsNullOrEmpty(strPhaseList))
        {
            DataTable dtMissionListOfVideoMissions = clsMissions.GetMissionsList();
            dtMissionListOfVideoMissions.Clear();

            StringBuilder strbMissionLiteral = new StringBuilder();
            int iCurrentPhaseID;

            char[] splitchar = { ',' };

            string[] strPhaseID = strPhaseList.Split(splitchar);

            foreach (string ID in strPhaseID)
            {
                iCurrentPhaseID = Convert.ToInt32(ID.ToString());
                //### GET MISSIONS FOR CURRENT PHASE
                DataTable dtMissionList = getAllMissionsForCurrentUserPhase(iCurrentPhaseID);

                int iCount = 1;
                StringBuilder strbVideoBullet = new StringBuilder();
                StringBuilder strbVideoPages = new StringBuilder();

                foreach (DataRow dtrMissionItems in dtMissionList.Rows)
                {
                    bool bVideoUpload = false;

                    int iMissionID = Convert.ToInt32(dtrMissionItems["iMissionID"]);

                    clsMissions clsMission = new clsMissions(iMissionID);

                    int iMissionTypeID = clsMission.iMissionTypeID;
                    int iXValue = 300;
                    int iYValue = 400;
                    int iCurrentPosition;
                    int iTotalBullets = dtMissionList.Rows.Count;

                    if ((clsMission.iMissionVideoID != null) && (clsMission.iMissionVideoID != 0))
                    {
                        iCurrentPosition = (100 * iCount);
                        iXValue = iCurrentPosition + iXValue;

                        DataTable dtMissionVideo = clsMissionVideos.GetMissionVideosList("iMissionVideoID = " + clsMission.iMissionVideoID, "");

                        string strColor = "";
                        string strVideoLink = "";

                        foreach (DataRow dr in dtMissionVideo.Rows)
                        {
                            strColor = dr["strColor"].ToString();
                            string temp = dr["strPathToVideo"].ToString();

                            if (temp != null && temp != "")
                            {
                                bVideoUpload = true;
                                strVideoLink = getList(dr["strPathToVideo"].ToString());
                            }
                            else
                            {
                                strVideoLink = dr["strVideoLink"].ToString();
                            }
                        }

                        if (!bIsMissionComplete(clsMission.iMissionID))
                        {

                            strbVideoBullet.AppendLine("<a href='Accelerate-Current-Mission.aspx?iMissionID=" + iMissionID + "' id='btnGoToMission" + iCount + "' class='tp-caption customin tp-resizeme rs-parallaxlevel-0 bullet'");
                            strbVideoBullet.AppendLine("data-x='" + iXValue + "'");
                            strbVideoBullet.AppendLine("data-y='" + iYValue + "'");
                            strbVideoBullet.AppendLine("data-customin='x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;'");
                            strbVideoBullet.AppendLine("data-speed='500'");
                            strbVideoBullet.AppendLine("data-start='2900'");
                            strbVideoBullet.AppendLine("data-easing='Power3.easeInOut'");
                            strbVideoBullet.AppendLine("data-splitin='none'");
                            strbVideoBullet.AppendLine("data-splitout='none'");
                            strbVideoBullet.AppendLine("data-elementdelay='0.1'");
                            strbVideoBullet.AppendLine("data-endelementdelay='0.1'");
                            strbVideoBullet.AppendLine("style='background-color: " + strColor + ";'>");


                            //////////////////////////////////////////////////////////////////////////////////////
                            //////////////////////////////////////////////////////////////////////////////////////////


                            strbVideoPages.AppendLine("<li data-transition='slideleft' data-slotamount='" + (iCount + 1) + "' data-masterspeed='2000'>");
                            strbVideoPages.AppendLine("<!-- MAIN IMAGE -->");
                            strbVideoPages.AppendLine("<img src='images/dummy.png' alt='citybg' data-lazyload='images/citybg.jpg' data-bgposition='center top' data-bgfit='cover' data-bgrepeat='no-repeat'>");
                            strbVideoPages.AppendLine("<!-- LAYERS -->");
                            strbVideoPages.AppendLine("<!-- BUTTONS FOR BACK AND MISSIONS START -->");
                            strbVideoPages.AppendLine("<!--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////-->");
                            strbVideoPages.AppendLine("<!--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////-->");
                            strbVideoPages.AppendLine("<div id='showStart" + iCount + "' class='tp-caption customin tp-resizeme rs-parallaxlevel-0'");
                            strbVideoPages.AppendLine("data-customin='x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;'");
                            strbVideoPages.AppendLine("data-speed='500'");
                            strbVideoPages.AppendLine("data-start='2900'");
                            strbVideoPages.AppendLine("data-x='10'");
                            strbVideoPages.AppendLine("data-y='10'");
                            strbVideoPages.AppendLine("data-easing='Power3.easeInOut'");
                            strbVideoPages.AppendLine("data-splitin='none'");
                            strbVideoPages.AppendLine("data-splitout='none'");
                            strbVideoPages.AppendLine("data-elementdelay='0.1'");
                            strbVideoPages.AppendLine("data-endelementdelay='0.1'");
                            strbVideoPages.AppendLine("style='z-index: 12; max-width: auto; max-height: auto; white-space: nowrap;'>");
                            strbVideoPages.AppendLine("<a href='#' class='largeredbtn'>&nbsp &nbsp &nbsp Go Back</a>");
                            strbVideoPages.AppendLine("</div>");

                            strbVideoPages.AppendLine("<div class='tp-caption arrowicon customin rs-parallaxlevel-0'");
                            strbVideoPages.AppendLine("data-customin='x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;'");
                            strbVideoPages.AppendLine("data-speed='300'");
                            strbVideoPages.AppendLine("data-start='3200'");
                            strbVideoPages.AppendLine("data-x='32'");
                            strbVideoPages.AppendLine("data-y='34'");
                            strbVideoPages.AppendLine("data-customin='x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;'");
                            strbVideoPages.AppendLine("data-easing='Power3.easeInOut'");
                            strbVideoPages.AppendLine("data-elementdelay='0.1'");
                            strbVideoPages.AppendLine("data-endelementdelay='0.1'");
                            strbVideoPages.AppendLine("data-linktoslide='next'");
                            strbVideoPages.AppendLine("style='z-index: 13;'>");
                            strbVideoPages.AppendLine("<div class=' rs-slideloop' data-easing='Power3.easeInOut'");
                            strbVideoPages.AppendLine("data-speed='0.5'");
                            strbVideoPages.AppendLine("data-xs='5'");
                            strbVideoPages.AppendLine("data-xe='-5'");
                            strbVideoPages.AppendLine("data-ys='0'");
                            strbVideoPages.AppendLine("data-ye='0'>");
                            strbVideoPages.AppendLine("<img src='images/dummy.png' alt='' data-ww='18' data-hh='11' data-lazyload='images/doublearrow1.png'>");
                            strbVideoPages.AppendLine("</div>");
                            strbVideoPages.AppendLine("</div>");

                            strbVideoPages.AppendLine("<div id='goToMission" + iCount + "' class='tp-caption customin tp-resizeme rs-parallaxlevel-0'");
                            strbVideoPages.AppendLine("data-customin='x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;'");
                            strbVideoPages.AppendLine("data-speed='500'");
                            strbVideoPages.AppendLine("data-start='2900'");
                            strbVideoPages.AppendLine("data-x='916'");
                            strbVideoPages.AppendLine("data-y='630'");
                            strbVideoPages.AppendLine("data-splitin='none'");
                            strbVideoPages.AppendLine("data-splitout='none'");
                            strbVideoPages.AppendLine("data-elementdelay='0.1'");
                            strbVideoPages.AppendLine("data-endelementdelay='0.1'");
                            strbVideoPages.AppendLine("style='z-index: 12; max-width: auto; max-height: auto; white-space: nowrap;'>");
                            strbVideoPages.AppendLine("<a href='Accelerate-Current-Mission.aspx?iMissionID=" + iMissionID + "' id='btnGoToMission" + iCount + "' runat='server' class='largeredbtn'>Go To Mission</a>");
                            strbVideoPages.AppendLine("</div>");

                            strbVideoPages.AppendLine("<div class='tp-caption arrowicon customin rs-parallaxlevel-0'");
                            strbVideoPages.AppendLine("data-customin='x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;'");
                            strbVideoPages.AppendLine("data-speed='300'");
                            strbVideoPages.AppendLine("data-start='3200'");
                            strbVideoPages.AppendLine("data-x='1110'");
                            strbVideoPages.AppendLine("data-y='655'");
                            strbVideoPages.AppendLine("data-easing='Power3.easeInOut'");
                            strbVideoPages.AppendLine("data-elementdelay='0.1'");
                            strbVideoPages.AppendLine("data-endelementdelay='0.1'");
                            strbVideoPages.AppendLine("data-linktoslide='next'");
                            strbVideoPages.AppendLine("style='z-index: 13;'>");
                            strbVideoPages.AppendLine("<div class=' rs-slideloop' data-easing='Power3.easeInOut'");
                            strbVideoPages.AppendLine(" data-speed='0.5'");
                            strbVideoPages.AppendLine("data-xs='-5'");
                            strbVideoPages.AppendLine("data-xe='5'");
                            strbVideoPages.AppendLine("data-ys='0'");
                            strbVideoPages.AppendLine("data-ye='0'>");
                            strbVideoPages.AppendLine("<img src='images/dummy.png' alt='' data-ww='18' data-hh='11' data-lazyload='images/doublearrow2.png'>");
                            strbVideoPages.AppendLine("</div>");
                            strbVideoPages.AppendLine("</div>");

                            strbVideoPages.AppendLine("<div class='tp-caption tp-fade fadeout rs-parallaxlevel-0'");
                            strbVideoPages.AppendLine(" data-x='78'");
                            strbVideoPages.AppendLine(" data-y='103'");
                            strbVideoPages.AppendLine(" data-speed='2000'");
                            strbVideoPages.AppendLine("data-start='1450'");
                            strbVideoPages.AppendLine(" data-easing='Power3.easeInOut'");
                            strbVideoPages.AppendLine("data-elementdelay='0.1'");
                            strbVideoPages.AppendLine(" data-endelementdelay='0.1'");
                            strbVideoPages.AppendLine(" data-endspeed='600'");
                            strbVideoPages.AppendLine(" data-autoplay='false'");
                            strbVideoPages.AppendLine(" data-autoplayonlyfirsttime='false'");
                            strbVideoPages.AppendLine(" style='z-index: 15;'>");

                            if (bVideoUpload)
                            {
                                strbVideoPages.AppendLine("<video controls='' autostart='false' name='media'>");
                                strbVideoPages.AppendLine("<source src='" + strVideoLink + "' type='video/mp4' width='625' height='467'></video>");
                            }
                            else
                            {
                                strbVideoPages.AppendLine("<iframe id='ifrmMissionVideo' src='" + strVideoLink + "' runat='server' width='625' height='467' style='width: 625px; height: 467px;'></iframe>");
                            }
                        }
                        else
                        {
                            strbVideoBullet.AppendLine("<a href='Accelerate-Current-Mission.aspx?iMissionID=" + iMissionID + "' id='btnGoToMission" + iCount + "' class='tp-caption customin tp-resizeme rs-parallaxlevel-0 bullet'");
                            strbVideoBullet.AppendLine("data-x='" + iXValue + "'");
                            strbVideoBullet.AppendLine("data-y='" + iYValue + "'");
                            strbVideoBullet.AppendLine("data-customin='x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;'");
                            strbVideoBullet.AppendLine("data-speed='500'");
                            strbVideoBullet.AppendLine("data-start='2900'");
                            strbVideoBullet.AppendLine("data-easing='Power3.easeInOut'");
                            strbVideoBullet.AppendLine("data-splitin='none'");
                            strbVideoBullet.AppendLine("data-splitout='none'");
                            strbVideoBullet.AppendLine("data-elementdelay='0.1'");
                            strbVideoBullet.AppendLine("data-endelementdelay='0.1'");
                            strbVideoBullet.AppendLine("><img src='images/imgComplete2.png'/> </a>");


                            //////////////////////////////////////////////////////////////////////////////////////
                            //////////////////////////////////////////////////////////////////////////////////////////


                            strbVideoPages.AppendLine("<li data-transition='slideleft' data-slotamount='" + (iCount + 1) + "' data-masterspeed='2000'>");
                            strbVideoPages.AppendLine("<!-- MAIN IMAGE -->");
                            strbVideoPages.AppendLine("<img src='images/dummy.png' alt='citybg' data-lazyload='images/citybg.jpg' data-bgposition='center top' data-bgfit='cover' data-bgrepeat='no-repeat'>");
                            strbVideoPages.AppendLine("<!-- LAYERS -->");
                            strbVideoPages.AppendLine("<!-- BUTTONS FOR BACK AND MISSIONS START -->");
                            strbVideoPages.AppendLine("<!--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////-->");
                            strbVideoPages.AppendLine("<!--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////-->");
                            strbVideoPages.AppendLine("<div id='showStart" + iCount + "' class='tp-caption customin tp-resizeme rs-parallaxlevel-0'");
                            strbVideoPages.AppendLine("data-customin='x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;'");
                            strbVideoPages.AppendLine("data-speed='500'");
                            strbVideoPages.AppendLine("data-start='2900'");
                            strbVideoPages.AppendLine("data-x='10'");
                            strbVideoPages.AppendLine("data-y='10'");
                            strbVideoPages.AppendLine("data-easing='Power3.easeInOut'");
                            strbVideoPages.AppendLine("data-splitin='none'");
                            strbVideoPages.AppendLine("data-splitout='none'");
                            strbVideoPages.AppendLine("data-elementdelay='0.1'");
                            strbVideoPages.AppendLine("data-endelementdelay='0.1'");
                            strbVideoPages.AppendLine("style='z-index: 12; max-width: auto; max-height: auto; white-space: nowrap;'>");
                            strbVideoPages.AppendLine("<a href='#' class='largeredbtn'>&nbsp &nbsp &nbsp Go Back</a>");
                            strbVideoPages.AppendLine("</div>");

                            strbVideoPages.AppendLine("<div class='tp-caption arrowicon customin rs-parallaxlevel-0'");
                            strbVideoPages.AppendLine("data-customin='x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;'");
                            strbVideoPages.AppendLine("data-speed='300'");
                            strbVideoPages.AppendLine("data-start='3200'");
                            strbVideoPages.AppendLine("data-x='32'");
                            strbVideoPages.AppendLine("data-y='34'");
                            strbVideoPages.AppendLine("data-customin='x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;'");
                            strbVideoPages.AppendLine("data-easing='Power3.easeInOut'");
                            strbVideoPages.AppendLine("data-elementdelay='0.1'");
                            strbVideoPages.AppendLine("data-endelementdelay='0.1'");
                            strbVideoPages.AppendLine("data-linktoslide='next'");
                            strbVideoPages.AppendLine("style='z-index: 13;'>");
                            strbVideoPages.AppendLine("<div class=' rs-slideloop' data-easing='Power3.easeInOut'");
                            strbVideoPages.AppendLine("data-speed='0.5'");
                            strbVideoPages.AppendLine("data-xs='5'");
                            strbVideoPages.AppendLine("data-xe='-5'");
                            strbVideoPages.AppendLine("data-ys='0'");
                            strbVideoPages.AppendLine("data-ye='0'>");
                            strbVideoPages.AppendLine("<img src='images/dummy.png' alt='' data-ww='18' data-hh='11' data-lazyload='images/doublearrow1.png'>");
                            strbVideoPages.AppendLine("</div>");
                            strbVideoPages.AppendLine("</div>");

                            strbVideoPages.AppendLine("<div id='goToMission" + iCount + "' class='tp-caption customin tp-resizeme rs-parallaxlevel-0'");
                            strbVideoPages.AppendLine("data-customin='x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;'");
                            strbVideoPages.AppendLine("data-speed='500'");
                            strbVideoPages.AppendLine("data-start='2900'");
                            strbVideoPages.AppendLine("data-x='916'");
                            strbVideoPages.AppendLine("data-y='630'");
                            strbVideoPages.AppendLine("data-splitin='none'");
                            strbVideoPages.AppendLine("data-splitout='none'");
                            strbVideoPages.AppendLine("data-elementdelay='0.1'");
                            strbVideoPages.AppendLine("data-endelementdelay='0.1'");
                            strbVideoPages.AppendLine("style='z-index: 12; max-width: auto; max-height: auto; white-space: nowrap;'>");
                            strbVideoPages.AppendLine("<a href='Accelerate-Current-Mission.aspx?iMissionID=" + iMissionID + "' id='btnGoToMission" + iCount + "' runat='server' class='largeredbtn'>Go To Mission</a>");
                            strbVideoPages.AppendLine("</div>");

                            strbVideoPages.AppendLine("<div class='tp-caption arrowicon customin rs-parallaxlevel-0'");
                            strbVideoPages.AppendLine("data-customin='x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;'");
                            strbVideoPages.AppendLine("data-speed='300'");
                            strbVideoPages.AppendLine("data-start='3200'");
                            strbVideoPages.AppendLine("data-x='1110'");
                            strbVideoPages.AppendLine("data-y='655'");
                            strbVideoPages.AppendLine("data-easing='Power3.easeInOut'");
                            strbVideoPages.AppendLine("data-elementdelay='0.1'");
                            strbVideoPages.AppendLine("data-endelementdelay='0.1'");
                            strbVideoPages.AppendLine("data-linktoslide='next'");
                            strbVideoPages.AppendLine("style='z-index: 13;'>");
                            strbVideoPages.AppendLine("<div class=' rs-slideloop' data-easing='Power3.easeInOut'");
                            strbVideoPages.AppendLine(" data-speed='0.5'");
                            strbVideoPages.AppendLine("data-xs='-5'");
                            strbVideoPages.AppendLine("data-xe='5'");
                            strbVideoPages.AppendLine("data-ys='0'");
                            strbVideoPages.AppendLine("data-ye='0'>");
                            strbVideoPages.AppendLine("<img src='images/dummy.png' alt='' data-ww='18' data-hh='11' data-lazyload='images/doublearrow2.png'>");
                            strbVideoPages.AppendLine("</div>");
                            strbVideoPages.AppendLine("</div>");

                            strbVideoPages.AppendLine("<div class='tp-caption tp-fade fadeout rs-parallaxlevel-0'");
                            strbVideoPages.AppendLine(" data-x='78'");
                            strbVideoPages.AppendLine(" data-y='103'");
                            strbVideoPages.AppendLine(" data-speed='2000'");
                            strbVideoPages.AppendLine("data-start='1450'");
                            strbVideoPages.AppendLine(" data-easing='Power3.easeInOut'");
                            strbVideoPages.AppendLine("data-elementdelay='0.1'");
                            strbVideoPages.AppendLine(" data-endelementdelay='0.1'");
                            strbVideoPages.AppendLine(" data-endspeed='600'");
                            strbVideoPages.AppendLine(" data-autoplay='false'");
                            strbVideoPages.AppendLine(" data-autoplayonlyfirsttime='false'");
                            strbVideoPages.AppendLine(" style='z-index: 15;'>");

                            if (bVideoUpload)
                            {
                                strbVideoPages.AppendLine("<video controls='' autostart='false' name='media'>");
                                strbVideoPages.AppendLine("<source src='" + strVideoLink + "' type='video/mp4' width='625' height='467'></video>");
                            }
                            else
                            {
                                strbVideoPages.AppendLine("<iframe id='ifrmMissionVideo' src='" + strVideoLink + "' runat='server' width='625' height='467' style='width: 625px; height: 467px;'></iframe>");
                            }
                        }
                        strbVideoPages.AppendLine("</div>");
                        strbVideoPages.AppendLine("</li>");

                        iCount++;
                    }
                }
                if ((strbVideoPages.Length > 0) && (strbVideoBullet.Length > 0))
                {
                    litMissionVideoBullets.Text = strbVideoBullet.ToString();
                    litMissionPages.Text = strbVideoPages.ToString();
                }
            }
        }
    }

    protected DataTable getAllPhasesForCurrentUserGroup(int iCurrentGroup)
    {
        DataTable dtPhaseList = clsGroupPhases.GetGroupPhasesList("iGroupID=" + iCurrentGroup, ""); //+ " AND iAccountUserID=" + clsAccountUsers.iAccountUserID

        //int iTotalPhases = dtPhaseList.Rows.Count;

        return dtPhaseList;
    }

    //### Get the Missions from that Phase
    protected DataTable getAllMissionsForCurrentUserPhase(int iCurrentPhase)
    {
        DataTable dtMissionList = clsPhaseMissions.GetPhaseMissionsList("iPhaseID=" + iCurrentPhase, ""); //+ " AND iAccountUserID=" + clsAccountUsers.iAccountUserID

        int iTotalMissions = dtMissionList.Rows.Count;

        return dtMissionList;
    }

    protected void popPhases()
    {
        clsPhases clsCurrentPhase = new clsPhases();
        DataTable dtPhase = getAllPhasesForCurrentUserGroup(clsAccountUsers.iGroupID);

        //string FullPathForImage;
        string strTitle;
        string strPhaseList = "";

        int iCount = 0;
        DateTime dtCurrentDate = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        DateTime dtStartTime = new DateTime();
        DateTime dtEndTime = new DateTime();

        foreach (DataRow dtrCurrentPhase in dtPhase.Rows)
        {
            ++iCount;

            int iPhaseID = Convert.ToInt32(dtrCurrentPhase["iPhaseID"].ToString());
            clsCurrentPhase = new clsPhases(iPhaseID);

            dtStartTime = Convert.ToDateTime(clsCurrentPhase.dtStartDate); //Convert.ToDateTime(dtrCurrentPhase["dtStartDate"]);
            dtEndTime = Convert.ToDateTime(clsCurrentPhase.dtEndDate);//Convert.ToDateTime(dtrCurrentPhase["dtEndDate"]);

            if ((dtStartTime <= dtCurrentDate && dtEndTime >= dtCurrentDate))
            {
                if (!string.IsNullOrEmpty(strPhaseList))
                {
                    strPhaseList = strPhaseList + ",";
                }
                else
                { }

                strPhaseList = strPhaseList + iPhaseID;
            }
        }

        PopulateMissions(strPhaseList);
    }

    protected bool bIsMissionComplete(int iCurrentMissionID)
    {
        bool bIsMissionComplete = false;

        DataTable dtMissionQuestions = clsMissionQuestions.GetMissionQuestionsList("iMissionID=" + iCurrentMissionID, "");
        int iTotalQuestionCount = dtMissionQuestions.Rows.Count;

        DataTable dtCompletedAnswersToQuestions = clsUserAnswers.GetUserAnswersList("iMissionID=" + iCurrentMissionID + " AND iAccountUserID=" + clsAccountUsers.iAccountUserID, "");
        int iTotalAnsweredCount = dtCompletedAnswersToQuestions.Rows.Count;
        int iCurrentPercentage = 0;

        //### Check if undefined
        if ((iTotalQuestionCount != 0))
        {
            double dblCurrentPercentage = Convert.ToDouble(iTotalAnsweredCount) / Convert.ToDouble(iTotalQuestionCount);
            iCurrentPercentage = Convert.ToInt32(dblCurrentPercentage * 200);
        }

        if (iCurrentPercentage == 200)
            bIsMissionComplete = true;

        return bIsMissionComplete;
    }

    #region VIDEO METHODS

    List<string> lstVideo;
    List<string> lstVideoFileNames;
    string strUniqueFullPath = AppDomain.CurrentDomain.BaseDirectory + "\\MissionVideos";

    public string getList(String strPathToFolder)
    {
        lstVideo = new List<string>();
        lstVideoFileNames = new List<string>();

        string strPath = strPathToFolder;
        string[] files = Directory.GetFiles(strUniqueFullPath + "\\" + strPath);

        string iMissionVideoID = "";
        if (!string.IsNullOrEmpty(Request.QueryString["iMissionVideoID"]))
            iMissionVideoID = Request.QueryString["iMissionVideoID"];

        string strVideoFile = "";
        foreach (string strName in files)
        {

            //string strHTMLVideo = strName.Replace(AppDomain.CurrentDomain.BaseDirectory + "\\", "..\\");

            //string strHTMLVideo = strName;
            string strHTMLVideo = strName.Replace(AppDomain.CurrentDomain.BaseDirectory + "\\", System.Configuration.ConfigurationManager.AppSettings["WebRoot"]);
            strHTMLVideo = strHTMLVideo.Replace("\\", "/");
            strVideoFile = strHTMLVideo;
            break;
        }

        return strVideoFile;
    }

    #endregion

}