﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Net.Mail;

public partial class Accelerate_My_Email_Settings : System.Web.UI.Page
{
    clsUsers clsUsers;
    clsAccountUsers clsAccountUsers;
    clsEmailSettings clsEmailSettings;

    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["action"] == "syslogout")
        {
            //### Clear all seesion variables
            Session.Clear();
        }

        //### Check if session clsUser exists
        if (Session["clsAccountUsers"] == null)
        {
            //### Redirect back to login
            Response.Redirect("Accelerate-Login.aspx");
        }
        try
        {
            clsAccountUsers = (clsAccountUsers)Session["clsAccountUsers"];
        }
        catch(Exception ex)
        {
            Response.Redirect("Accelerate-Error404.aspx");
        }
       
        ScriptManager.RegisterStartupScript(this, GetType(), "myFunction", "populateSwitch();", true);

        //popProfile(clsAccountUsers.iCurrentPoints);
        int iEmailSettingID = 0;
        try
        {
            DataTable dtEmailSettings = clsEmailSettings.GetSettingsList("iAccountUserID=" + clsAccountUsers.iAccountUserID, "");
            foreach (DataRow dtrEmailSettings in dtEmailSettings.Rows)
            {
                iEmailSettingID = Convert.ToInt32(dtrEmailSettings["iEmailSettingID"].ToString());
            }
        }
        catch
        {
            Response.Redirect("Accelerate-Error404.aspx");
        }

        if (iEmailSettingID != 0)
        {
            try
            {
                clsEmailSettings = new clsEmailSettings(iEmailSettingID);
            }
            catch(Exception ex)
            {
                Response.Redirect("Accelerate-Error404.aspx");
            }
        }
   
        else
        {
            clsEmailSettings = new clsEmailSettings();
            clsEmailSettings.bEmailNotification = cbNotification.Checked;
            clsEmailSettings.bEmailMissions = cbMission.Checked;
            clsEmailSettings.bEmailReminders = cbReminders.Checked;
            try
            {
                SaveData();
            }
            catch(Exception ex)
            {
                Response.Redirect("Accelerate-Error404.aspx");
            }
        }

        if (!IsPostBack)
        {
            popFormData();
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        //### Validate registration process
        bool bCanSave = true;

        //bCanSave = clsValidation.IsNullOrEmpty(txtFirstName, bCanSave);
        //bCanSave = clsValidation.IsNullOrEmpty(txtSurname, bCanSave);
        //bCanSave = clsValidation.IsNullOrEmpty(txtEmailAddress, bCanSave);
        //bCanSave = clsValidation.IsNullOrEmpty(txtPhoneNumber, bCanSave);

        if (bCanSave == true)
        {
            mandatoryDiv.Visible = false;
            try
            {
                SaveData();
            }
            catch(Exception ex)
            {
                Response.Redirect("Accelerate-Error404.aspx");
            }
            Response.Redirect("~/Accelerate-My-Profile.aspx");
        }
        else
        {
            mandatoryDiv.Visible = true;
            lblValidationMessage.Text = "<div class=\"madatoryPaddingDiv\"><div class=\"validationMessage\">Please fill out all the fields</div></div>";
        }
        //### Validate registration process
    }

    #endregion

    #region POPULATE DATA METHODS

    public void popFormData()
    {
        cbMission.Checked = Convert.ToBoolean(clsEmailSettings.bEmailMissions);
        cbNotification.Checked = Convert.ToBoolean(clsEmailSettings.bEmailNotification);
        cbReminders.Checked = Convert.ToBoolean(clsEmailSettings.bEmailReminders);
    }

    protected void popProfile(int iCurrentPoints)
    {
        string strFullName = clsAccountUsers.strFirstName + " " + clsAccountUsers.strSurname.Substring(0, 1);
        string strFullPath = "";
        string strFileName = "";

        //### Populates images
        if (!string.IsNullOrEmpty(clsAccountUsers.strMasterImage))
        {
            strFullPath = clsAccountUsers.strPathToImages;
            if (!string.IsNullOrEmpty(clsAccountUsers.strMasterImage))
            {
                strFileName = clsAccountUsers.strMasterImage;
                strFullPath = "AccountUsers/" + clsAccountUsers.strPathToImages + "/" + Path.GetFileNameWithoutExtension(strFileName) + Path.GetExtension(strFileName);
            }
        }
        else
            strFullPath = "images/no-image.png";

        StringBuilder strbAccountUser = new StringBuilder();

        strbAccountUser.AppendLine("<div class='homeHeaderContainer'>");
        strbAccountUser.AppendLine("<div class='headerImgContainer'><img src='" + strFullPath + "' width='100px' height='100px'/></div>");
        strbAccountUser.AppendLine("<div class='headerContentContainer'>");
        strbAccountUser.AppendLine("<h1 class='ProfileHead'>" + strFullName + "</h1>");
        strbAccountUser.AppendLine("<div class='progressContainerProfile'> ");
        strbAccountUser.AppendLine("<span class='white'>Current rank and points</span>");
        int iCurrentRank = iRankID(iCurrentPoints);
        clsRanks clsCurrentRank = new clsRanks(iCurrentRank);
        clsRanks clsNextRank = new clsRanks();

        if (iRankCount() > iCurrentRank)
        {
            clsNextRank = new clsRanks(iCurrentRank + 1);
        }

        strbAccountUser.AppendLine("<p class='green'>" + clsCurrentRank.strTitle + ": " + iCurrentPoints + "</p>");

        //###   Bootstrap Progress Bar
        strbAccountUser.AppendLine("<div class='progress progress-striped active'>");
        strbAccountUser.AppendLine("<div class='progress-bar' role='progressbar' aria-valuenow='45' aria-valuemin='0' aria-valuemax='100' style='width: " + iCurrentPercentage(iCurrentPoints, clsCurrentRank.iPointLevel) / 2 + "%'>");
        strbAccountUser.AppendLine("<span class='sr-only'>" + iCurrentPercentage(iCurrentPoints, clsCurrentRank.iPointLevel) / 2 + "% Complete</span>");
        strbAccountUser.AppendLine("</div>");
        strbAccountUser.AppendLine("</div>");

        //###   Custom Progress Bar
        //strbAccountUser.AppendLine("<div class='headerProgressBarEmpty'>");
        //strbAccountUser.AppendLine("<div class='percentageHeaderProgressBar' style='width:" + iCurrentPercentage(iCurrentPoints, clsCurrentRank.iPointLevel) + "px'>&nbsp;</div>");
        //strbAccountUser.AppendLine("</div>");
        strbAccountUser.AppendLine("</div>");
        strbAccountUser.AppendLine("<div class='progressContainerProfile'>");
        strbAccountUser.AppendLine("<span class='white'>Next rank and points</span>");

        if (iRankCount() > iCurrentRank)
        {
            strbAccountUser.AppendLine("<p class='orange'>" + clsNextRank.strTitle + ": " + (clsNextRank.iPointLevel - clsCurrentRank.iPointLevel) + " MORE</p>");
        }
        else
        {
            strbAccountUser.AppendLine("<p class='orange'>Well Done! You're at the top!</p>");
        }

        strbAccountUser.AppendLine("</div>");
        strbAccountUser.AppendLine("</div>");
        strbAccountUser.AppendLine("<br class='clr'/>");
        strbAccountUser.AppendLine("</div>");

        litProfile.Text = strbAccountUser.ToString();
    }

    #endregion

    #region ACTION METHODS

    protected int iRankID(int iCurrentPoints)
    {
        DataTable dtRank = clsRanks.GetRanksList("", "");

        int iRankID = 1;
        int iPointLevel;

        int iCount = 0;

        foreach (DataRow dtrRank in dtRank.Rows)
        {
            ++iCount;

            iRankID = Convert.ToInt32(dtrRank["iRankID"].ToString());
            iPointLevel = Convert.ToInt32(dtrRank["iPointLevel"].ToString());

            if (iCurrentPoints < iPointLevel)
            {
                break;
            }
        }

        return iRankID;
    }

    protected int iRankCount()
    {
        DataTable dtRank = clsRanks.GetRanksList();

        int iRowCount = dtRank.Rows.Count;

        return iRowCount;
    }

    protected int iCurrentPercentage(int iCurrentPoints, int iTotalPoints)
    {
        double dblCurrentPercentage = Convert.ToDouble(iCurrentPoints) / Convert.ToDouble(iTotalPoints);

        int iCurrentPercentage = Convert.ToInt32(dblCurrentPercentage * 200);

        return iCurrentPercentage;
    }

    //### Returns Answers Completed Percentage for current mission
    protected int iCurrentPercentage(int iCurrentMissionID)
    {
        DataTable dtMissionQuestions = clsMissionQuestions.GetMissionQuestionsList("iMissionID=" + iCurrentMissionID, "");
        int iTotalQuestionCount = dtMissionQuestions.Rows.Count;

        DataTable dtCompletedAnswersToQuestions = clsUserAnswers.GetUserAnswersList("iMissionID=" + iCurrentMissionID + " AND iAccountUserID=" + clsAccountUsers.iAccountUserID, "");
        int iTotalAnsweredCount = dtCompletedAnswersToQuestions.Rows.Count;
        int iCurrentPercentage = 0;

        //### Check if undefined
        if ((iTotalQuestionCount != 0))
        {
            double dblCurrentPercentage = Convert.ToDouble(iTotalAnsweredCount) / Convert.ToDouble(iTotalQuestionCount);
            iCurrentPercentage = Convert.ToInt32(dblCurrentPercentage * 200);
        }

        return iCurrentPercentage;
    }

    //### Returns Answers Completed Percentage for Teamship mission
    protected int iCurrentPercentageTeamship(int iCurrentMissionID)
    {
        DataTable dtMissionQuestions = clsTeamShips.GetTeamShipsList("iMissionID=" + iCurrentMissionID, "");
        int iTotalQuestionCount = dtMissionQuestions.Rows.Count;

        DataTable dtCompletedAnswersToQuestions = clsTeamShipAnswers.GetTeamShipAnswersList("iMissionID=" + iCurrentMissionID + " AND iAddedby=" + clsAccountUsers.iAccountUserID, "");
        int iTotalAnsweredCount = dtCompletedAnswersToQuestions.Rows.Count;
        int iCurrentPercentage = 0;

        //### Check if undefined
        if ((iTotalQuestionCount != 0))
        {
            double dblCurrentPercentage = Convert.ToDouble(iTotalAnsweredCount) / Convert.ToDouble(iTotalQuestionCount);
            iCurrentPercentage = Convert.ToInt32(dblCurrentPercentage * 200);
        }

        return iCurrentPercentage;
    }
    #endregion

    #region SAVE DATA METHODS

    private void SaveData()
    {
        clsEmailSettings.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsEmailSettings.iAddedBy = clsAccountUsers.iAccountUserID;
        clsEmailSettings.dtEdited = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        clsEmailSettings.iEditedBy = clsAccountUsers.iAccountUserID;

        clsEmailSettings.bEmailMissions = cbMission.Checked;
        clsEmailSettings.bEmailReminders = cbReminders.Checked;
        clsEmailSettings.bEmailNotification = cbNotification.Checked;
        clsEmailSettings.iAccountUserID = clsAccountUsers.iAccountUserID;

        clsEmailSettings.Update();
    }

    #endregion
}