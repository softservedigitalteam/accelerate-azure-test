﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Accelerate_Certificate_Page : System.Web.UI.Page
{
    clsAccountUsers clsAccountUsers;
    clsMissions clsMissions;
    public clsAccountUserTrackings clsTracking;
    protected void Page_Load(object sender, EventArgs e)
    {
        //### Logout User
        if (Request.QueryString["action"] == "syslogout")
        {
            //### Clear all seesion variables
            Session.Clear();
        }

        //### Check if session clsUser exists
        if (Session["clsAccountUsers"] == null)
        {
            //### Redirect back to login
            Response.Redirect("Accelerate-Login.aspx");
        }
        try
        {
            clsAccountUsers = (clsAccountUsers)Session["clsAccountUsers"];
            clsTracking = new clsAccountUserTrackings(clsAccountUsers.iAccountUserTrackingID);
            int iGroupID = clsAccountUsers.iGroupID;
            clsGroups clsGroup = new clsGroups(iGroupID);
        }
        catch (Exception ex)
        {
            Response.Redirect("Accelerate-Error404.aspx");
        }
        popCertificate();
    }

    protected void popCertificate()
    {
        string strPath = AppDomain.CurrentDomain.BaseDirectory + "\\PDF\\CompleteCertificate\\" + clsAccountUsers.iAccountUserID.ToString() + "-" + clsAccountUsers.strSurname + "-" + clsAccountUsers.strFirstName + ".pdf";
        string pdfPath = "PDF/CompleteCertificate/" + clsAccountUsers.iAccountUserID.ToString() + "-" +  clsAccountUsers.strSurname + "-" + clsAccountUsers.strFirstName + ".pdf#zoom=75";
        if (File.Exists(strPath))
        {
            if (strPath.Contains(".pdf"))
            {
                //strPath = strPath.Replace("\\", "/");
                ifrmCertViewer.Attributes["src"] = pdfPath;
                //pdfFrameViewer.Attributes["src"] = "http://www.pitt.edu/~kmram/132/lectures/registers+counters.pdf";
            }
        }
        else
        {
            Response.Redirect("Accelerate-Completed.aspx");
        }
    }

    protected void btnContinue_Click(object sender, EventArgs e)
    {
        Response.Redirect("Accelerate-Home.aspx");
    }

    protected void btnClick_Click(object sender, EventArgs e)
    {
        //ModalPopupExtenderViewer.Show();
    }
}