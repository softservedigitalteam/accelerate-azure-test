﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Accelerate.master" AutoEventWireup="true" Inherits="Accelerate_Privacy" Codebehind="Accelerate-Privacy.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div>
<b>ACCELERATE ONBOARDING SOFTWARE</b><br />
<br />
<b>PRIVACY & COOKIE POLICY</b><br />
<br />
Effective Date: 1 December 2014<br />
<br />
We collect certain information through our website, located at <a style="color:#000;" href="accelerate.za.deloitte.com">accelerate.za.deloitte.com</a> (our “Website”). 
This page (this “Privacy Policy”) lays out our policies and procedures surrounding the collection and handling of any such 
information that identifies an individual user or that could be used to contact or locate him or her 
(“Personally Identifiable Information” or “PII”).<br />
This Privacy Policy applies only our Website. It does not apply to any third party site or service linked to our Website 
or recommended or referred by our Website or by our staff. And it does not apply to any other website or online service operated 
by our company, or to any of our offline activities.<br />
All collected information is in compliance with the South Africa the Protection of Personal Information Act (POPI).<br />
“Personal Information” shall have the meaning set out in the current version of the Protection of Personal Information Act (“POPI”) 
or such other legislation as may become applicable to the protection information in South Africa.<br />
<br />
<b>What does this privacy policy cover?</b><br />
<br />
This privacy policy is to inform you regarding the use of your personal information which is collected during your visit to our website.
<br />
<br />
We may need to provide, collect, use, store or process your Personal Information. You hereby authorises such collection, use, 
storage and processing where the need arises.<br />
<br />
Such Personal information shall be provided, collected, used, stored or processed<br />
•	in compliance with POPI;<br />
•	as is necessary for the purposes of the Terms and Conditions<br />
We shall comply with the security and information protection obligations equivalent to those imposed on them in terms of POPI 
and other applicable data protection legislation, and failing such legislation, they shall take, implement and maintain all such 
technical and organisational security procedures and measures necessary or appropriate to preserve the security and confidentiality 
of the Personal Information in its possession and to protect such Personal Information against unauthorised or unlawful disclosure, 
access or processing, accidental loss, destruction or damage.<br />
<br />
<b>Amendment of this Privacy Policy</b><br />
<br />
We may change this Privacy Policy at any time by posting a new version on this page or on a successor page. The new version will 
become effective on the date it’s posted, which will be listed at the top of the page as the new Effective Date.<br />
<br />
<b>Cookies policy</b><br />
<br />
<b>What is a cookie?</b><br />
Cookies are small data files that your browser places on your computer or device.  Cookies help your browser navigate a website and 
the cookies themselves cannot collect any information stored on your computer or your files.<br />
When a server uses a web browser to read cookies they can help a website deliver a more user-friendly service. 
To protect your privacy, your browser only gives a website access to the cookies it has already sent to you.<br />
<br />
<b>Why do we use cookies?</b><br />
We use cookies to learn more about the way you interact with our content and help us to improve your experience when visiting our website.<br />
Cookies remember the type of browser you use and which additional browser software you have installed. They also remember your preferences, 
such as language and region, which remain as your default settings when you revisit the website. Cookies also allow you to rate pages 
and fill in comment forms.<br />
Some of the cookies we use are session cookies and only last until you close your browser, others are persistent cookies which 
are stored on your computer for longer.<br />
<br />
<b>How are third party cookies used?</b><br />
For some of the functions within our websites we use third party suppliers, for example, when you visit a page with videos 
embedded from or links to YouTube. These videos or links (and any other content from third party suppliers) may contain third party 
cookies and you may wish to consult the policies of these third party websites for information regarding their use of cookies.<br />
<br />
<b>How do I reject and delete cookies?</b><br />
We will not use cookies to collect personally identifiable information about you. However, should you wish to do so, you can choose to 
reject or block the cookies set by Shell or the websites of any third party suppliers by changing your browser 
settings – see the Help function within your browser for further details. Please note that most browsers automatically accept 
cookies so if you do not wish cookies to be used you may need to actively delete or block the cookies.<br />
For information on the use of cookies in mobile phone browsers and for details on how to reject or delete such cookies, 
please refer to your handset manual.<br />
Note, however, that if you reject the use of cookies you will still be able to visit our website but some of the functions may 
not work correctly.<br />
<br />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScrMang" Runat="Server">
</asp:Content>

