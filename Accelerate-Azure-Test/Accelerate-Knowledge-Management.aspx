<%@ Page Title="Knowledge Management" Language="C#" MasterPageFile="~/Accelerate.master" AutoEventWireup="true" Inherits="Accelerate_Knowledge_Management" Codebehind="Accelerate-Knowledge-Management.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<script type="text/javascript" lang="javascript">
     function CheckOnOff(rdoId,gridName)
     {
         var rdo = KnowledgeManagement.getElementById(rdoId);
         /* Getting an array of all the INPUT controls on the form.*/
         var rdo = KnowledgeManagement.getElementById(rdoId);
         var all = KnowledgeManagement.getElementsByTagName("input");
         for(i=0;i<all.length;i++)
         {
             /*Checking if it is a radio button, and also checking if the id of that radio button is different than rdoId */
             if(all[i].type=="radio" && all[i].id != rdo.id)
             {
                 var count=all[i].id.indexOf(gridName);
                 if(count!=-1)
                 {
                     all[i].checked=false;
                 }
             }
         }
     rdo.checked=true; /* Finally making the clicked radio button CHECKED */
     }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:UpdatePanel ID="updMain" runat="server">
    <ContentTemplate>

         <div class="sectionContainerLeft innerBoxWithShadow margintopten">
            <h2 class="genericHeading">Knowledge Management</h2>
            <div style="margin:5px;">
                 <div class="imageHolderCommonDiv"><div class="validationImageMandatory"></div></div>
                 <div class="labelDiv">Knowledge Management Upload:</div>
                 <div class="fieldDiv" style="position:relative;">
                     <asp:FileUpload ID="FileUpload" runat="server" CssClass="roundedCornerTextBoxUpload" style="float: left;" />
                     <div style="float:left;"><asp:LinkButton ID="btnUpload" runat="server" CssClass="btnStart StartButtonPosition" onclick="btnUpload_Click" style="margin-left:5px;" Text="Upload" /></div>
                     <br class="clearingSpacer" />
                     <asp:Label ID="lblUniquePath" runat="Server" Visible="false"></asp:Label><br /><asp:Label ID="lblFileSize" runat="Server" Visible="false"></asp:Label>
                     <asp:UpdatePanel ID="udpKnowledgeManagement" runat="server" ChildrenAsTriggers="true">
                         <ContentTemplate>
                             <asp:DataList ID="dlKnowledgeManagement" runat="server" RepeatColumns="3" ItemStyle-CssClass="listImages">
                                 <ItemTemplate>
                                     <table cellspacing="5">
                                         <%# Container.DataItem %>
                                     </table>
                                     <div align="center" style="padding-top: 5px;">
                                         <asp:RadioButton ID="rdbMainImage" runat="server" GroupName="MainKnowledgeManagement" Text="Main Image" onclick="javascript:CheckOnOff(this.id,'dlKnowledgeManagement');" Visible="false" /><br />
                                     </div>
                                 </ItemTemplate>
                             </asp:DataList>
                         </ContentTemplate>
                     </asp:UpdatePanel><br />
                     <b><asp:Label ID="lblUploadError" runat="server"></asp:Label></b>
                 </div>
                 <br class="clearingSpacer" />
            </div>
         </div>

        <div class="sectionContainerLeft innerBoxWithShadow margintopforty" style="position:relative;">
            <asp:LinkButton ID="lnkbtnSave" runat="server" CssClass="btnStart StartButtonPosition" onclick="lnkbtnSave_Click" Text="Save"/>
        </div>

         </ContentTemplate>
         <Triggers>
             <asp:PostBackTrigger ControlID="btnUpload" />
         </Triggers>
     </asp:UpdatePanel>
</asp:Content>
