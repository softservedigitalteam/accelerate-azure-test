﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.IO;
using System.Configuration;

public partial class Accelerate_Portal : System.Web.UI.Page
{
    clsAccountUsers clsAccountUsers;
    protected void Page_Load(object sender, EventArgs e)
    {
        //### Logout User
        if (Request.QueryString["action"] == "syslogout")
        {
            //### Clear all seesion variables
            Session.Clear();
        }

        //### Check if session clsUser exists
        if (Session["clsAccountUsers"] == null)
        {
            //### Redirect back to login
            Response.Redirect("Accelerate-Login.aspx");
        }
        clsAccountUsers = (clsAccountUsers)Session["clsAccountUsers"];
        try
        {
            popPortalDocuments();
        }
        catch(Exception ex)
        {
            Response.Redirect("Accelerate-Error404.aspx");
        }
    }


    protected void popPortalDocuments()
    {
        clsPortalFeatures clsPortalFeatures = new clsPortalFeatures();

        int iGroupID = clsAccountUsers.iGroupID;

        DataTable dtFeatureGroups = clsGroupFeatures.GetGroupFeaturesList("iGroupID = " + iGroupID, "");

        List<int> lstFeatureIDs = new List<int>();

        foreach (DataRow dr in dtFeatureGroups.Rows)
        {
            lstFeatureIDs.Add(Convert.ToInt32(dr["iPortalFeatureID"]));
        }

        DataTable dtFeatures = clsPortalFeatures.GetPortalFeaturesList("", "");

        StringBuilder strbPortalDoc = new StringBuilder();

        int iCount = 0;
        int iTotalCount = 0;

        dtFeatures.Columns.Add("FullPathForImage");

        foreach (int i in lstFeatureIDs)
        {
            foreach (DataRow dtrFeatures in dtFeatures.Rows)
            {
                if (Convert.ToInt32(dtrFeatures["iPortalFeatureID"]) == i)
                {
                    ++iCount;
                    ++iTotalCount;

                    if ((dtrFeatures["strMasterImage"].ToString() == "") || (dtrFeatures["strMasterImage"] == null))
                    {
                        dtrFeatures["FullPathForImage"] = "images/icon.png";
                    }
                    else
                        dtrFeatures["FullPathForImage"] = "PortalFeatures/" + dtrFeatures["strPathToImages"] + "/" + Path.GetFileNameWithoutExtension(dtrFeatures["strMasterImage"].ToString()) + "" + Path.GetExtension(dtrFeatures["strMasterImage"].ToString());

                    switch (iCount)
	                {
	                    case 1:
                        strbPortalDoc.AppendLine("<div class='row-fluid'>");
		                strbPortalDoc.AppendLine("<div class='Colour2 KnowledgeBaseBlock'>");
		                break;
	                    case 2:
		                strbPortalDoc.AppendLine("<div class='Colour3 KnowledgeBaseBlock'>");
		                break;
                        case 3:
		                strbPortalDoc.AppendLine("<div class='Colour1 KnowledgeBaseBlock'>");
		                break;
                            case 4:
		                strbPortalDoc.AppendLine("<div class='Colour2 KnowledgeBaseBlock'>");
		                break;
                            case 5:
                        strbPortalDoc.AppendLine("<div class='row-fluid'>");
		                strbPortalDoc.AppendLine("<div class='Colour3 KnowledgeBaseBlock'>");
		                break;
                            case 6:
		                strbPortalDoc.AppendLine("<div class='Colour1 KnowledgeBaseBlock'>");
		                break;
                            case 7:
		                strbPortalDoc.AppendLine("<div class='Colour2 KnowledgeBaseBlock'>");
		                break;
                            case 8:
		                strbPortalDoc.AppendLine("<div class='Colour3 KnowledgeBaseBlock'>");
		                break;
	                }      
                    
                    strbPortalDoc.AppendLine("<b class='media-heading'>"+dtrFeatures["strTitle"]+"</b><br />");

                    string tempstring = dtrFeatures["strDescription"].ToString();
                    
                    //Cut string if description is to long
                    if (tempstring.Length < 150)
                        strbPortalDoc.AppendLine(dtrFeatures["strDescription"].ToString() + "<br />");
                    else
                        strbPortalDoc.AppendLine(dtrFeatures["strDescription"].ToString().Substring(0, 150) + "...<br />");

                    strbPortalDoc.AppendLine("<a href='Accelerate-Portal-Feature.aspx?iFeatureID=" + dtrFeatures["iPortalFeatureID"].ToString() + "'>READ MORE</a><br />");
                    strbPortalDoc.AppendLine("</div>"); //Block div end

                    if (iCount % 4 == 0)
                    {
                        if (iCount == 8)
                        {
                            iCount = 0;
                        }

                        strbPortalDoc.AppendLine("</div>"); //row-fluid div end
                        break;
                    }
                    else
                    {
                        if (iTotalCount == dtFeatures.Rows.Count)
                        {
                            strbPortalDoc.AppendLine("</div>"); //info well div end

                        }
                    }
                }             
            }
        }

        strbPortalDoc.AppendLine("<br style='clear:both' />");
        strbPortalDoc.AppendLine("</div>");

        litPortalFeatures.Text = strbPortalDoc.ToString();
    }


}