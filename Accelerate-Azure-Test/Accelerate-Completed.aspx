﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Accelerate_Completed" Codebehind="Accelerate-Completed.aspx.cs" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Literal runat="server" ID="litImage"></asp:Literal>
        <div id="MainImage"></div>
        <asp:LinkButton runat="server" ID="btnSmallProceed" CssClass="btnAdvanceSmall" OnClick="btnSmallProceed_Click" Style="margin: 0; position: absolute; top: 50%; right: 20px; transform: translate(-50%, -50%);"><img id="btnAdvanceSmall" src="img/Start.png" /></asp:LinkButton>
        <asp:LinkButton runat="server" ID="btnProceedLarge" CssClass="btnAdvanceSmall" OnClick="btnProceedLarge_Click" Style="display: none;"><img id="btnAdvanceLarge" src="images/btnContinueWelcome.png" /></asp:LinkButton>
    </form>
</body>
</html>
