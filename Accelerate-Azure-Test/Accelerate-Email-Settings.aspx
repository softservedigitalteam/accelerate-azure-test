﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Accelerate.master" Inherits="Accelerate_Email_Settings" Codebehind="Accelerate-Email-Settings.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="css/base.css" rel="stylesheet" />
    <link href="css/skeleton.css" rel="stylesheet" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Literal ID="litProfile" runat="server"></asp:Literal>
    <center><div class="arrow-down"></div></center>
    <div class="sectionContainerLeft innerBoxWithShadow margintopten">
        <h2 class="genericHeading">Email Settings</h2>
        <div class="checkBoxList">
            <br class="clr" />
                <div class="onoffswitch">
                    
                Mission Reminders<input type="checkbox" runat="server" name="onoffswitch" class="onoffswitch-checkbox" id="rdbMissions" checked="checked"/>
                    <%--<asp:CheckBox ID="rdbMissions" runat="server" name="onoffswitch" CssClass="onoffswitch-checkbox"  checked="true" />--%>
                <label class="onoffswitch-label" for="myonoffswitch">
        <span class="onoffswitch-inner"></span>
        <span class="onoffswitch-switch"></span>
    </label>
</div>
                <div class="onoffswitch">
    Forum Messages<input  runat="server" type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="rdbForums" checked="checked"/>
    <label class="onoffswitch-label" for="myonoffswitch">
        <span class="onoffswitch-inner"></span>
        <span class="onoffswitch-switch"></span>
    </label>
</div>
                <div class="onoffswitch">
    Phase Reminders<input type="checkbox" runat="server" name="onoffswitch" class="onoffswitch-checkbox" id="rdbPhase" checked="checked"/>
    <label class="onoffswitch-label" for="myonoffswitch">
        <span class="onoffswitch-inner"></span>
        <span class="onoffswitch-switch"></span>
    </label>
</div>
            <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-white" Text="Save" Visible="True" onClick="btnSubmit_Click" />
            <br class="clr" />

        </div>
    </div>
    <div class="sectionContainerLeft innerBoxWithShadow margintopforty">
        <h2 class="genericHeading">My Group</h2>
        <div class="homeGroupImages">
            <asp:Literal ID="litGroupMemberMain" runat="server"></asp:Literal>
        </div>
    </div>
    <asp:HiddenField ID="hdfAvailableAchiements" runat="server" />
    <script type="text/javascript">
        function balanceSectionsHome() {
            var lhs = $('#leftHandSideSection').height();
            var rhs = $('#leftHandSideSection').height();
            if (lhs > rhs) {
                $('#rightHandSideSection').css({ 'height': lhs + 'px' });
                $('#leftHandSideSection').css({ 'height': lhs + 'px' });
            }
            else {
                $('#rightHandSideSection').css({ 'height': rhs + 'px' });
                $('#leftHandSideSection').css({ 'height': rhs + 'px' });
            }
        }
    </script>
</asp:Content>

