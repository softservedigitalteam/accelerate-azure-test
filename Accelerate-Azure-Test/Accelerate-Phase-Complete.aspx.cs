﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Accelerate_Phase_Complete : System.Web.UI.Page
{
    clsAccountUsers clsAccountUsers;
    clsAccountUserTrackings clsAccountUserTracking;

    protected void Page_Load(object sender, EventArgs e)
    {
                //### Logout User
        if (Request.QueryString["action"] == "syslogout")
        {
            //### Clear all seesion variables
            Session.Clear();
        }

        //### Check if session clsUser exists
        if (Session["clsAccountUsers"] == null)
        {
            //### Redirect back to login
            Response.Redirect("Accelerate-Login.aspx");
        }
        clsAccountUsers = (clsAccountUsers)Session["clsAccountUsers"];
        try
        {
            clsAccountUserTracking = new clsAccountUserTrackings(clsAccountUsers.iAccountUserTrackingID);
        }
        catch(Exception ex)
        {
            Response.Redirect("Accelerate-Error404.aspx");
        }
    }

    protected void btnContinue_Click(object sender, EventArgs e)
    {
        Response.Redirect("Accelerate-Home.aspx");
    }
}