﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Text;
using System.Drawing;

public partial class Accelerate_Missions : System.Web.UI.Page
{
    clsAccountUsers clsAccountUsers;
    clsAccountUserTrackings iAccountUserTracking;
    DataTable dtPhase1Missions;
    DataTable dtPhase2Missions;
    DataTable dtPhase3Missions;
    //DataTable dtPhase;
    int iMissionID;

    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Logout User
        if (Request.QueryString["action"] == "syslogout")
        {
            //### Clear all seesion variables
            Session.Clear();
        }

        //### Check if session clsUser exists
        if (Session["clsAccountUsers"] == null)
        {
            //### Redirect back to login
            Response.Redirect("Accelerate-Login.aspx");
        }
        try
        {
            clsAccountUsers = (clsAccountUsers)Session["clsAccountUsers"];
            iAccountUserTracking = new clsAccountUserTrackings(clsAccountUsers.iAccountUserTrackingID);

            //Caching
            //if (Cache["dtPhase"] == null)
            //{
            //    dtPhase = getAllPhasesForCurrentUserGroup(clsAccountUsers.iGroupID);
            //    Cache.Add("dtPhase", dtPhase, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, 5, 0), System.Web.Caching.CacheItemPriority.Default, null);
            //}
            //else
            //{
            //    dtPhase = (DataTable)Cache["dtPhase"];
            //}
            cacheAllPhaseMissions();

            //### Populates the Welcome Message
            popPhases();
        }
        catch (Exception ex)
        {
            Response.Redirect("Accelerate-Error404.aspx");
        }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Balance", "balanceSectionsMission()", true);

        //if (!IsPostBack)
        //{
        //}
        //if (Cache["dtPhase1Missions"] != null)
        //    Page.ClientScript.RegisterStartupScript(this.GetType(), "scriptkey", "<script>alert('Hello');</script>");
    }

    protected void cacheAllPhaseMissions()
    {
        if (Cache["dtPhase1Missions"] == null)
        {
            dtPhase1Missions = clsPhaseMissions.GetPhaseMissionsList("iPhaseID=" + iAccountUserTracking.iPhase1ID, "");
            Cache.Add("dtPhase1Missions", dtPhase1Missions, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, 5, 0), System.Web.Caching.CacheItemPriority.Default, null);
        }
        else
        {
            dtPhase1Missions = (DataTable)Cache["dtPhase1Missions"];
        }

        if (Cache["dtPhase2Missions"] == null)
        {
            dtPhase2Missions = clsPhaseMissions.GetPhaseMissionsList("iPhaseID=" + iAccountUserTracking.iPhase2ID, "");
            Cache.Add("dtPhase2Missions", dtPhase2Missions, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, 5, 0), System.Web.Caching.CacheItemPriority.Default, null);
        }
        else
        {
            dtPhase2Missions = (DataTable)Cache["dtPhase2Missions"];
        }

        if (Cache["dtPhase3Missions"] == null)
        {
            dtPhase3Missions = clsPhaseMissions.GetPhaseMissionsList("iPhaseID=" + iAccountUserTracking.iPhase3ID, "");
            Cache.Add("dtPhase3Missions", dtPhase3Missions, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, 5, 0), System.Web.Caching.CacheItemPriority.Default, null);
        }
        else
        {
            dtPhase3Missions = (DataTable)Cache["dtPhase3Missions"];
        }
    }

    #endregion

    #region ACTION FUNCTIONS

    //### Get the Phases for that Group
    protected DataTable getAllPhasesForCurrentUserGroup(int iCurrentGroup)
    {
        DataTable dtPhaseList = clsGroupPhases.GetGroupPhasesList("iGroupID=" + iCurrentGroup, ""); //+ " AND iAccountUserID=" + clsAccountUsers.iAccountUserID

        //int iTotalPhases = dtPhaseList.Rows.Count;

        return dtPhaseList;
    }

    //### Get the Missions from that Phase
    protected DataTable getAllMissionsForCurrentUserPhase(int iCurrentPhase)
    {
        DataTable dtMissionList = clsPhaseMissions.GetPhaseMissionsList("iPhaseID=" + iCurrentPhase, ""); //+ " AND iAccountUserID=" + clsAccountUsers.iAccountUserID

        //int iTotalMissions = dtMissionList.Rows.Count;

        return dtMissionList;
    }

    //### Returns Current Percentage - used for...
    protected int iCurrentPercentage(int iCurrentPoints, int iTotalPoints)
    {
        double dblCurrentPercentage = Convert.ToDouble(iCurrentPoints) / Convert.ToDouble(iTotalPoints);

        int iCurrentPercentage = Convert.ToInt32(dblCurrentPercentage * 200);

        return iCurrentPercentage;
    }

    //### Returns Answers Completed Percentage for current mission
    protected int iCurrentPercentage(int iCurrentMissionID)
    {
        DataTable dtMissionQuestions = clsMissionQuestions.GetMissionQuestionsList("iMissionID=" + iCurrentMissionID, "");
        int iTotalQuestionCount = dtMissionQuestions.Rows.Count;

        DataTable dtCompletedAnswersToQuestions = clsUserAnswers.GetUserAnswersList("iMissionID=" + iCurrentMissionID + " AND iAccountUserID=" + clsAccountUsers.iAccountUserID, "");
        int iTotalAnsweredCount = dtCompletedAnswersToQuestions.Rows.Count;
        int iCurrentPercentage = 0;

        //### Check if undefined
        if ((iTotalQuestionCount != 0))
        {
            double dblCurrentPercentage = Convert.ToDouble(iTotalAnsweredCount) / Convert.ToDouble(iTotalQuestionCount);
            iCurrentPercentage = Convert.ToInt32(dblCurrentPercentage * 200);
        }

        return iCurrentPercentage;
    }

    //### Returns Answers Completed Percentage for Teamship mission
    protected int iCurrentPercentageTeamship(int iCurrentMissionID)
    {
        DataTable dtMissionQuestions = clsTeamShips.GetTeamShipsList("iMissionID=" + iCurrentMissionID, "");
        int iTotalQuestionCount = dtMissionQuestions.Rows.Count;

        DataTable dtCompletedAnswersToQuestions = clsTeamShipAnswers.GetTeamShipAnswersList("iMissionID=" + iCurrentMissionID + " AND iAddedby=" + clsAccountUsers.iAccountUserID, "");
        int iTotalAnsweredCount = dtCompletedAnswersToQuestions.Rows.Count;
        int iCurrentPercentage = 0;

        //### Check if undefined
        if ((iTotalQuestionCount != 0))
        {
            double dblCurrentPercentage = Convert.ToDouble(iTotalAnsweredCount) / Convert.ToDouble(iTotalQuestionCount);
            iCurrentPercentage = Convert.ToInt32(dblCurrentPercentage * 200);
        }

        return iCurrentPercentage;
    }

    protected string strCurrentRank(int iCurrentPoints)
    {
        DataTable dtRank = clsRanks.GetRanksList("", "");

        int iRankID = 1;
        int iPointLevel;
        string strCurrentRank = "Noob";

        int iCount = 0;

        foreach (DataRow dtrRank in dtRank.Rows)
        {
            ++iCount;

            iRankID = Convert.ToInt32(dtrRank["iRankID"].ToString());
            iPointLevel = Convert.ToInt32(dtrRank["iPointLevel"].ToString());
            strCurrentRank = dtrRank["strTitle"].ToString();

            if (iCurrentPoints < iPointLevel)
            {
                break;
            }
        }

        return strCurrentRank;
    }

    //### Returns Current Percentage
    protected int iCurrentPercentageFromCurrentMission(int iCurrentPoints, int iTotalPoints)
    {
        double dblCurrentPercentage = Convert.ToDouble(iCurrentPoints) / Convert.ToDouble(iTotalPoints);

        int iCurrentPercentage = Convert.ToInt32(dblCurrentPercentage * 200);

        return iCurrentPercentage;
    }

    protected string strCurrentRankFromCurrentMission(int iCurrentPoints)
    {
        DataTable dtRank = clsRanks.GetRanksList("", "");

        int iRankID = 1;
        int iPointLevel;
        string strCurrentRank = "Noob";

        int iCount = 0;

        foreach (DataRow dtrRank in dtRank.Rows)
        {
            ++iCount;

            iRankID = Convert.ToInt32(dtrRank["iRankID"].ToString());
            iPointLevel = Convert.ToInt32(dtrRank["iPointLevel"].ToString());
            strCurrentRank = dtrRank["strTitle"].ToString();

            if (iCurrentPoints < iPointLevel)
            {
                break;
            }
        }

        return strCurrentRank;
    }

    //### Get the total number of  answers
    protected int iTotalQuestions(int iCurrentMissionID)
    {
        DataTable dtMissionQuestions = clsMissionQuestions.GetMissionQuestionsList("iMissionID=" + iCurrentMissionID, "");

        int iCurrentQs = dtMissionQuestions.Rows.Count;

        return iCurrentQs;
    }

    //### Get the total number of questions answered
    protected int iTotalQuestionsAnswered(int iCurrentMissionID)
    {
        DataTable dtMissionQuestionsAnswered = clsUserAnswers.GetUserAnswersList("iMissionID=" + iCurrentMissionID + " AND iAccountUserID=" + clsAccountUsers.iAccountUserID, "");

        int iTotalQuestionsAnswered = dtMissionQuestionsAnswered.Rows.Count;

        return iTotalQuestionsAnswered;
    }

    //### Get the total number of  answers
    protected int iTotalTeamshipQuestions(int iCurrentMissionID)
    {
        DataTable dtMissionQuestions = clsTeamShips.GetTeamShipsList("iMissionID=" + iCurrentMissionID, "");

        int iCurrentQs = dtMissionQuestions.Rows.Count;

        return iCurrentQs;
    }

    //### Get the total number of questions answered
    protected int iTotalTeamshipQuestionsAnswered(int iCurrentMissionID)
    {
        DataTable dtMissionQuestionsAnswered = clsTeamShipAnswers.GetTeamShipAnswersList("iMissionID=" + iCurrentMissionID + " AND iAddedBy=" + clsAccountUsers.iAccountUserID, "");

        int iTotalQuestionsAnswered = dtMissionQuestionsAnswered.Rows.Count;

        return iTotalQuestionsAnswered;
    }

    //### Returns Current Rank ID
    protected int iRankID(int iCurrentPoints)
    {
        DataTable dtRank = clsRanks.GetRanksList("", "");

        int iRankID = 1;
        int iPointLevel;

        int iCount = 0;

        foreach (DataRow dtrRank in dtRank.Rows)
        {
            ++iCount;

            iRankID = Convert.ToInt32(dtrRank["iRankID"].ToString());
            iPointLevel = Convert.ToInt32(dtrRank["iPointLevel"].ToString());

            if (iCurrentPoints < iPointLevel)
            {
                break;
            }
        }

        return iRankID;
    }

    //### Returns Rank Total
    protected int iRankCount()
    {
        DataTable dtRank = clsRanks.GetRanksList();

        int iRowCount = dtRank.Rows.Count;

        return iRowCount;
    }

    //### Returns if the question has a correct answer
    private bool bDoesACorrectAnswerExist(int iMissionQuestionID)
    {
        bool bIsThereAnAnswer;

        DataTable dtAnswers = clsMissionAnswers.GetMissionAnswersList("iMissionQuestionID=" + iMissionQuestionID + "AND bIsCorrectAnswer=" + true, "");

        if ((dtAnswers.Rows.Count) > 0)
        {
            bIsThereAnAnswer = true;
        }
        else
            bIsThereAnAnswer = false;

        return bIsThereAnAnswer;
    }

    //### Returns if the teamship question has a correct answer
    //private bool bDoesACorrectAnswerExistForTeamShip(int iTeamshipID)
    //{
    //    bool bIsThereAnAnswer;

    //    DataTable dtAnswers = clsTeamShipAnswers.GetTeamShipAnswersList("iTeamShipID=" + iTeamshipID, "");

    //    if ((dtAnswers.Rows.Count) > 0)
    //    {
    //        bIsThereAnAnswer = true;
    //    }
    //    else
    //        bIsThereAnAnswer = false;

    //    return bIsThereAnAnswer;
    //}

    //### Returns the correct answer

    private string strAnswerValue(int iMissionQuestionID)
    {
        string strAnswer = "Not Set";

        DataTable dtAnswers = clsMissionAnswers.GetMissionAnswersList("iMissionQuestionID=" + iMissionQuestionID + "AND bIsCorrectAnswer=" + true, "");

        foreach (DataRow dtrAnswers in dtAnswers.Rows)
        {
            strAnswer = dtrAnswers["strAnswer"].ToString();
        }

        return strAnswer;
    }

    //### Returns the correct name answer
    private string strTeamNameAnswerValue(int iTeamShipID)
    {
        string strAnswer = "Not Set";

        DataTable dtAnswers = clsTeamShips.GetTeamShipsList("iTeamShipID=" + iTeamShipID, "");

        foreach (DataRow dtrAnswers in dtAnswers.Rows)
        {
            strAnswer = dtrAnswers["strName"].ToString();
        }

        return strAnswer;
    }

    //### Returns the correct title answer
    private string strTeamTitleAnswerValue(int iTeamShipID)
    {
        string strAnswer = "Not Set";

        DataTable dtAnswers = clsTeamShips.GetTeamShipsList("iTeamShipID=" + iTeamShipID, "");

        foreach (DataRow dtrAnswers in dtAnswers.Rows)
        {
            strAnswer = dtrAnswers["strTitle"].ToString();
        }

        return strAnswer;
    }

    #endregion

    #region POPULATION METHODS

    protected void popPhases()
    {
        DataTable dtPhase = clsGroupPhases.GetGroupPhasesList("iGroupID=" + clsAccountUsers.iGroupID, "");//clsPhases.GetPhasesList("iGroupID=", "");
        int iPhasesAvailable = canWeUnlockNextPhase();

        clsPhases clsCurrentPhase = new clsPhases();
        dtPhase = getAllPhasesForCurrentUserGroup(clsAccountUsers.iGroupID);

        dtPhase.Columns.Add("FullPathForImage");
        dtPhase.Columns.Add("strPhaseTitle");
        dtPhase.Columns.Add("strStyle");

        //string FullPathForImage;
        string strTitle;
        string strPhaseList="";

        int iCount = 0;
        DateTime dtCurrentDate = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        DateTime dtStartTime = new DateTime();
        DateTime dtEndTime = new DateTime();

        foreach (DataRow dtrCurrentPhase in dtPhase.Rows)
        {
            ++iCount;

            int iPhaseID = Convert.ToInt32(dtrCurrentPhase["iPhaseID"].ToString());
            clsCurrentPhase = new clsPhases(iPhaseID);

            strTitle = clsCurrentPhase.strTitle;//dtrCurrentPhase["strTitle"].ToString();
            dtrCurrentPhase["strPhaseTitle"] = strTitle;
            dtStartTime = Convert.ToDateTime(clsCurrentPhase.dtStartDate); //Convert.ToDateTime(dtrCurrentPhase["dtStartDate"]);
            dtEndTime = Convert.ToDateTime(clsCurrentPhase.dtEndDate);//Convert.ToDateTime(dtrCurrentPhase["dtEndDate"]);

            if ((clsCurrentPhase.strMasterImage.ToString() == "") || (clsCurrentPhase.strMasterImage == null))
            {
                dtrCurrentPhase["FullPathForImage"] = "images/imgNoImage.png";
            }
            else
            {
                dtrCurrentPhase["FullPathForImage"] = "Phases/" + clsCurrentPhase.strPathToImages + "/" + Path.GetFileNameWithoutExtension(clsCurrentPhase.strMasterImage.ToString()) + "" + Path.GetExtension(clsCurrentPhase.strMasterImage.ToString());
            }

            if ((dtStartTime <= dtCurrentDate && dtEndTime >= dtCurrentDate))
            {
                if (iPhasesAvailable >= iCount)
                {
                    if (!string.IsNullOrEmpty(strPhaseList))
                    {
                        strPhaseList = strPhaseList + ",";
                    }
                    //else
                    //{ }

                    strPhaseList = strPhaseList + iPhaseID;

                    dtrCurrentPhase["strStyle"] = "opacity:1";
                }
                else
                {
                    dtrCurrentPhase["strStyle"] = "opacity:0.3";
                }
            }
            else
            {
                dtrCurrentPhase["strStyle"] = "opacity:0.3";
            }

            //popMissions(strPhaseList);
        }

        popMissions(strPhaseList);

        rpPhases.DataSource = dtPhase;
        rpPhases.DataBind();
    }

    private int canWeUnlockNextPhase() //###return 1 2 or 3 depending what should be available
    {
        int iPhaseCount = 0;
        //iAccountUserTracking = new clsAccountUserTrackings(clsAccountUsers.iAccountUserTrackingID);

        if ((iAccountUserTracking.bIsPhase1Complete == true) && (iAccountUserTracking.bIsPhase2Complete == false) && (iAccountUserTracking.bIsPhase3Complete == false))
        {
            iPhaseCount = 2;
        }
        else if ((iAccountUserTracking.bIsPhase1Complete == true) && (iAccountUserTracking.bIsPhase2Complete == true) && ((iAccountUserTracking.bIsPhase3Complete == true) || (iAccountUserTracking.bIsPhase3Complete == false)))
        {
            iPhaseCount = 3;
        }
        else
        {
            iPhaseCount = 1;
        }
        //int iGroupID = clsAccountUsers.iGroupID;
        //clsGroups clsGroups = new clsGroups(clsAccountUsers.iGroupID);
        //DataTable dtGroupPhases = clsGroupPhases.GetGroupPhasesList("iGroupID=" + iGroupID, "");
        //int iPhaseID = 1;
        

        //foreach (DataRow dtrGroupPhasesList in dtGroupPhases.Rows)
        //{

        
        //    iPhaseID = Convert.ToInt32(dtrGroupPhasesList["iPhaseID"]);
        //    DataTable dtPhaseMissions = clsPhaseMissions.GetPhaseMissionsList("iPhaseID=" + iPhaseID, "");

        //    bool bIsThisPhaseComplete = true;

        //    //break;
        //    int iMissionID = 0;
        //    foreach (DataRow dtrPhaseMissionsList in dtPhaseMissions.Rows)
        //    {
        //        iMissionID = Convert.ToInt32(dtrPhaseMissionsList["iMissionID"]);

        //        clsMissions clsMissions = new clsMissions(iMissionID);
        //        int iMissionTypeID = clsMissions.iMissionTypeID;

        //        if(iMissionTypeID==1)
        //        {
        //            if(!bIsTextMissionComplete(iMissionID))
        //                bIsThisPhaseComplete = false;
        //        }
        //        else if(iMissionTypeID==2)
        //        {
        //            if (!bIsLeadershipMissionComplete(iMissionID))
        //                bIsThisPhaseComplete = false;
        //        }
        //    }

        //    if(bIsThisPhaseComplete)
        //        ++iPhaseCount;
        //}

        return iPhaseCount;
    }

    protected bool bIsTextMissionComplete(int iCurrentMissionID)
    {
        bool bIsTextMissionComplete = false;

        DataTable dtMissionQuestions = clsMissionQuestions.GetMissionQuestionsList("iMissionID=" + iCurrentMissionID, "");
        int iTotalQuestionCount = dtMissionQuestions.Rows.Count;

        DataTable dtCompletedAnswersToQuestions = clsUserAnswers.GetUserAnswersList("iMissionID=" + iCurrentMissionID + " AND iAccountUserID=" + clsAccountUsers.iAccountUserID, "");
        int iTotalAnsweredCount = dtCompletedAnswersToQuestions.Rows.Count;
        int iCurrentPercentage = 0;

        //### Check if undefined
        if ((iTotalQuestionCount != 0))
        {
            double dblCurrentPercentage = Convert.ToDouble(iTotalAnsweredCount) / Convert.ToDouble(iTotalQuestionCount);
            iCurrentPercentage = Convert.ToInt32(dblCurrentPercentage * 200);
        }

        if (iCurrentPercentage == 200)
            bIsTextMissionComplete = true;

        return bIsTextMissionComplete;
    }

    //### Returns Answers Completed Percentage for Teamship mission
    protected bool bIsLeadershipMissionComplete(int iCurrentMissionID)
    {
        bool bIsLeadershipMissionComplete = false;

        DataTable dtMissionQuestions = clsTeamShips.GetTeamShipsList("iMissionID=" + iCurrentMissionID, "");
        int iTotalQuestionCount = dtMissionQuestions.Rows.Count;

        DataTable dtCompletedAnswersToQuestions = clsTeamShipAnswers.GetTeamShipAnswersList("iMissionID=" + iCurrentMissionID + " AND iAddedby=" + clsAccountUsers.iAccountUserID, "");
        int iTotalAnsweredCount = dtCompletedAnswersToQuestions.Rows.Count;
        int iCurrentPercentage = 0;

        //### Check if undefined
        if ((iTotalQuestionCount != 0))
        {
            double dblCurrentPercentage = Convert.ToDouble(iTotalAnsweredCount) / Convert.ToDouble(iTotalQuestionCount);
            iCurrentPercentage = Convert.ToInt32(dblCurrentPercentage * 200);
        }

        if (iCurrentPercentage == 200)
            bIsLeadershipMissionComplete = true;

        return bIsLeadershipMissionComplete;
    }

    protected void popMissions(int iPhaseID)
    {
        StringBuilder strbMissionLiteral = new StringBuilder();

        //### GET MISSIONS FOR CURRENT PHASE
        DataTable dtMissionList = getAllMissionsForCurrentUserPhase(iPhaseID);

        foreach (DataRow dtrMissionItems in dtMissionList.Rows)
        {
            int iMissionID = Convert.ToInt32(dtrMissionItems["iMissionID"]);

            clsMissions clsMissions = new clsMissions(iMissionID);
            int iMissionTypeID = clsMissions.iMissionTypeID;

            if (iMissionTypeID == 1)
            {
                strbMissionLiteral.Append(popCurrentMissionList(iMissionID, iMissionTypeID));
            }
            else if (iMissionTypeID == 2)
            {
                strbMissionLiteral.Append(popCurrentMissionList(iMissionID, iMissionTypeID));
            }

            litMissionsList.Text = strbMissionLiteral.ToString();
        }
    }

    //### For a list of Phases
    protected void popMissions(string strPhaseList)
    {
        
        if (!string.IsNullOrEmpty(strPhaseList))
        {
            StringBuilder strbMissionLiteral = new StringBuilder();
            int iCurrentPhaseID;
            int phaseCount = 0;
            DataTable dtMissionList = new DataTable();

            char[] splitchar = { ',' };

            string[] strPhaseID = strPhaseList.Split(splitchar);
            phaseCount = strPhaseID.Count();
            
            //foreach (string ID in strPhaseID)
            //{
                //iCurrentPhaseID = Convert.ToInt32(ID.ToString());
                //### GET MISSIONS FOR CURRENT PHASE
            if (phaseCount == 2)
            {
                dtMissionList.Merge(dtPhase1Missions, true);
                dtMissionList.Merge(dtPhase2Missions, true);
            }
            else if (phaseCount == 3)
            {
                dtMissionList.Merge(dtPhase1Missions, true);
                dtMissionList.Merge(dtPhase2Missions, true);
                dtMissionList.Merge(dtPhase3Missions, true);
            }
            else
            {
                dtMissionList.Merge(dtPhase1Missions, true);
            }
               // DataTable dtMissionList = getAllMissionsForCurrentUserPhase(iCurrentPhaseID);
                

                foreach (DataRow dtrMissionItems in dtMissionList.Rows)
                {
                    int iMissionID = Convert.ToInt32(dtrMissionItems["iMissionID"]);

                    clsMissions clsMissions = new clsMissions(iMissionID);
                    int iMissionTypeID = clsMissions.iMissionTypeID;

                    if (iMissionTypeID == 1)
                    {
                        strbMissionLiteral.Append(popCurrentMissionList(iMissionID, iMissionTypeID));
                    }
                    else if (iMissionTypeID == 2)
                    {
                        strbMissionLiteral.Append(popCurrentMissionList(iMissionID, iMissionTypeID));
                    }
                }
        	//}

            litMissionsList.Text = strbMissionLiteral.ToString();
        }
    }

    protected void popPercentageBar(int iCurrentPoints, int iTotalPoints)
    {
        StringBuilder strbCurrentMission = new StringBuilder();
        strbCurrentMission.AppendLine("<div class='percentageGreen' style='width:" + iCurrentPercentage(iCurrentPoints, iTotalPoints) + "px'></div>");
    }

    protected void popPercentageBar(int iCurrentPercent)
    {
        StringBuilder strbCurrentMission = new StringBuilder();
        strbCurrentMission.AppendLine("<div class='percentageGreen' style='width:" + iCurrentPercentage(iCurrentPercent) + "px'></div>");
    }

    protected string popMissionProgressBar(int iCurrentPoints, int iTotalPoints, int iCurrentMissionID)
    {
        StringBuilder strbCurrentMission = new StringBuilder();

        int iDoublePercent = iCurrentPercentage(iCurrentPoints, iTotalPoints);

        int iPercent = 0;
        if (iDoublePercent != 0)
        {
            iPercent = iDoublePercent / 2;
        }

        strbCurrentMission.AppendLine("<div class='progressBarEmpty'>");
        strbCurrentMission.AppendLine("<div class='percentageBlue' style='width:" + iDoublePercent + "px' title='" + iPercent + " percent complete'></div>");
        strbCurrentMission.AppendLine("</div>");


        return strbCurrentMission.ToString();
    }

    protected string popMissionProgressBarPercent(int iCurrentDoublePercent, int iCurrentMissionID)
    {
        StringBuilder strbCurrentMission = new StringBuilder();

        int iDoublePercent = iCurrentDoublePercent;
        int iPercent = 0;

        if (iDoublePercent != 0)
        {
            iPercent = iDoublePercent / 2;
        }

        strbCurrentMission.AppendLine("<div class='progressBarEmpty'>");
        strbCurrentMission.AppendLine("<div class='percentageBlue' style='width:" + iDoublePercent + "px' title='" + iPercent + " percent complete'></div>");
        strbCurrentMission.AppendLine("</div>");

        return strbCurrentMission.ToString();
    }

    protected void popCurrentMissionInfoFromCurrentMission(int iCurrentMissionID)
    {
        clsMissions clsMissions = new clsMissions(iCurrentMissionID);

        StringBuilder strbCurrentMission = new StringBuilder();

        strbCurrentMission.AppendLine("<h3 class='missionSubHeading'>");
        strbCurrentMission.AppendLine("Mission");
        strbCurrentMission.AppendLine("</h3>");
        strbCurrentMission.AppendLine("<div class='missionInner-Active'>" + clsMissions.strDescription.Replace("\n", "<br />") + "<br />");

        clsAchievements clsAchievements = new clsAchievements(clsMissions.iAchievementID);
        string strFullPathForImage = "Achievements/" + clsAchievements.strPathToImages + "/" + Path.GetFileNameWithoutExtension(clsAchievements.strMasterImage) + "" + Path.GetExtension(clsAchievements.strMasterImage);
        strbCurrentMission.AppendLine("<br/ ><center><img src='" + strFullPathForImage + "' title='" + clsAchievements.strTitle + "' alt='" + clsAchievements.strTitle + "' width='65px' height='65px' /></center></div>");

        //litCurrentMissionFromCurrentMission.Text = strbCurrentMission.ToString();

        StringBuilder strbTagAndImage = new StringBuilder();

        string strFullPathForMissionImage = "Missions/" + clsMissions.strPathToImages + "/" + Path.GetFileNameWithoutExtension(clsMissions.strMasterImage) + "" + Path.GetExtension(clsMissions.strMasterImage);

        strbTagAndImage.AppendLine("<div class='tagLine'>" + clsMissions.strTagLine + "</div><div class='imageRight'><img src='" + strFullPathForMissionImage + "' title='" + clsMissions.strTitle + "' alt='" + clsMissions.strTitle + "' width='80px' height='80px' /></div>");

        //litTagAndImage.Text = strbTagAndImage.ToString();

        if (iCurrentMissionID == 1)
        {
            //litCurrentMissionFromCurrentMission.Text = strbCurrentMission.ToString();
            ///litTagAndImage.Text = strbTagAndImage.ToString();
        }
        if (iCurrentMissionID == 2)
        {
            //litCurrentMissionFromCurrentMission2.Text = strbCurrentMission.ToString();
            //litTagAndImage2.Text = strbTagAndImage.ToString();
        }

        //if (iCurrentMissionID == 5)
        //{
        //    litCurrentMissionFromCurrentMission5.Text = strbCurrentMission.ToString();
        //    litTagAndImage5.Text = strbTagAndImage.ToString();
        //}
        //if (iCurrentMissionID == 6)
        //{
        //    litCurrentMissionFromCurrentMission6.Text = strbCurrentMission.ToString();
        //    litTagAndImage6.Text = strbTagAndImage.ToString();
        //}
    }

    /// <summary>
    /// POP Mission Questions DYNAMICALLY
    /// </summary>
    /// <param name="iCurrentMissionID"></param>
    protected void popMissionQuestions(int iCurrentMissionID)
    {
        DataTable dtMission = clsMissionQuestions.GetMissionQuestionsList("iMissionID=" + iCurrentMissionID, "");

        dtMission.Columns.Add("strAnswer");

        string strQuestion;
        int iMissionQuestionID;

        int iCount = 0;

        foreach (DataRow dtrAccountMember in dtMission.Rows)
        {
            ++iCount;

            iMissionQuestionID = Convert.ToInt32(dtrAccountMember["iMissionQuestionID"].ToString());
            strQuestion = dtrAccountMember["strQuestion"].ToString();
        }

        //if (iCurrentMissionID == 1)
        //{
        //    rpMisionQuestions.DataSource = dtMission;
        //    rpMisionQuestions.DataBind();
        //}
    }

    /// <summary>
    /// ### POP TEAMSHIP QUESTIONS DYNAMICALLY
    /// </summary>
    /// <param name="iCurrentMissionID"></param>
    protected void popTeamShips(int iCurrentMissionID)
    {
        DataTable dtMission = clsTeamShips.GetTeamShipsList("iMissionID=" + iCurrentMissionID, "");

        dtMission.Columns.Add("strNameAnswer");
        dtMission.Columns.Add("strTitleAnswer");
        dtMission.Columns.Add("FullPathForImage");

        string strName;
        string strTitle;
        int iTeamShipID;

        int iCount = 0;

        foreach (DataRow dtrAccountMember in dtMission.Rows)
        {
            ++iCount;

            iTeamShipID = Convert.ToInt32(dtrAccountMember["iTeamShipID"].ToString());
            strName = dtrAccountMember["strName"].ToString();
            strTitle = dtrAccountMember["strTitle"].ToString();

            clsTeamShips clsCurrentTeamShips = new clsTeamShips(iTeamShipID);

            if ((clsCurrentTeamShips.strMasterImage == "") || (clsCurrentTeamShips.strMasterImage == null))
            {
                dtrAccountMember["FullPathForImage"] = "images/imgNoImage.png";
            }
            else
            {
                dtrAccountMember["FullPathForImage"] = "/TeamShips/" + clsCurrentTeamShips.strPathToImages + "/" + Path.GetFileNameWithoutExtension(clsCurrentTeamShips.strMasterImage) + "" + Path.GetExtension(clsCurrentTeamShips.strMasterImage);
            }
        }

        if (iCurrentMissionID == 2)
        {
            //rpMissionQuestions2.DataSource = dtMission;
            //rpMissionQuestions2.DataBind();
        }
    }


    protected string popCurrentMissionInfo(int iCurrentMissionID)
    {
        string strCurrentMission;

        DataTable dtMissionList = getAllMissionsForCurrentUserPhase(iCurrentMissionID);

        clsMissions clsMissions = new clsMissions(iCurrentMissionID);

        StringBuilder strbCurrentMission = new StringBuilder();

        strbCurrentMission.AppendLine("<div class='missionContainer'>");
        strbCurrentMission.AppendLine("<h4 class='missionHeading black'><strong>MISSION: " + clsMissions.strTitle.ToUpper() + "<span>POINTS: " + clsMissions.iBonusPoints + "</span></strong></h4>");

        if (clsMissions.strDescription.Length >= 250)
            strbCurrentMission.AppendLine("<p class='currentMissionText'>" + clsMissions.strDescription.Substring(0, 250) + "...</p>");
        else
            strbCurrentMission.AppendLine("<p class='currentMissionText'>" + clsMissions.strDescription + "...</p>");

        strbCurrentMission.AppendLine("</div>");

        strCurrentMission = strbCurrentMission.ToString();

        return strCurrentMission;
    }

    protected string popCurrentMissionList(int iMissionID, int iMissionTypeID)
    {
        bool bMissionComplete = false;

        string strCurrentMissionList;
        StringBuilder strbCurrentMissionList = new StringBuilder();

        strbCurrentMissionList.AppendLine("<div class='missionsDiv' style='position: relative;border-bottom: solid 4px #cfcfcf;'>");
        strbCurrentMissionList.AppendLine(popCurrentMissionInfo(iMissionID));
        strbCurrentMissionList.AppendLine("<div class='currentMissionProgressContainer'>");
        strbCurrentMissionList.AppendLine("<div class='progressContainer black'>");
        strbCurrentMissionList.AppendLine("<p>My Progress:</p>");

        if (iMissionTypeID == 1)
        {
            strbCurrentMissionList.AppendLine(popMissionProgressBarPercent(iCurrentPercentage(iMissionID), iMissionID));
            if ((iCurrentPercentage(iMissionID) != 200))
            {
                strbCurrentMissionList.AppendLine("<a href='Accelerate-Current-Mission.aspx?iMissionID=" + iMissionID + "'" + " class='btnStart StartButtonPositionCurrent'>Start</a>"); //id='btnStart'
                bMissionComplete = false;
            }
            else
            {
                strbCurrentMissionList.AppendLine("<a href='Accelerate-Current-Mission.aspx?iMissionID=" + iMissionID + "'" + " class='btnStart2 StartButtonPositionCurrent'><img src='images/imgComplete.png' /></a>");
                //strbCurrentMissionList.AppendLine("<a href='Accelerate-Current-Mission.aspx?iMissionID=" + iMissionID + "'" + " class='btnStart StartButtonPositionCurrent'>View</a>");
                bMissionComplete = true;
            }
        }
        else if (iMissionTypeID == 2)
        { 
            strbCurrentMissionList.AppendLine(popMissionProgressBarPercent(iCurrentPercentageTeamship(iMissionID), iMissionID));

            if (iCurrentPercentageTeamship(iMissionID) != 200)
            {
                strbCurrentMissionList.AppendLine("<a href='Accelerate-Current-Mission.aspx?iMissionID=" + iMissionID + "'" + " class='btnStart StartButtonPositionCurrent'>Start</a>");
                bMissionComplete = false;
            }
            else
            {
                strbCurrentMissionList.AppendLine("<a href='Accelerate-Current-Mission.aspx?iMissionID=" + iMissionID + "'" + " class='btnStart2 StartButtonPositionCurrent'><img src='images/imgComplete.png' /></a>");
                //strbCurrentMissionList.AppendLine("<a href='Accelerate-Current-Mission.aspx?iMissionID=" + iMissionID + "'" + " class='btnStart StartButtonPositionCurrent'>View</a>");
                bMissionComplete = true;
            }
        }

        //if (iCurrentPercentage(iMissionID) != 200)
        //    strbCurrentMissionList.AppendLine("<a href='Accelerate-Current-Mission.aspx?iMissionID=" + iMissionID + "'" + " class='btnStart StartButtonPositionCurrent'>Start</a>"); //id='btnStart'
        //else strbCurrentMissionList.AppendLine("<a href='Accelerate-Current-Mission.aspx?iMissionID=" + iMissionID + "'" + " class='btnStart StartButtonPositionCurrent'>View</a>");

        //strbCurrentMissionList.AppendLine("<a href='Accelerate-Current-Mission.aspx?iMissionID=" + iMissionID + "'" + "id='btnStart' class='btnStart StartButtonPositionCurrent'>Start</a>");
        strbCurrentMissionList.AppendLine("</div>");

        //if (bMissionComplete)
        //{
        //    strbCurrentMissionList.AppendLine("<div class='progressContainer'>");
        //    strbCurrentMissionList.AppendLine("<img src='img/img-Mission-Complete.png' style='position:absolute;right:100px;top:-50px;'/>");
        //    strbCurrentMissionList.AppendLine("</div>");
        //}

        strbCurrentMissionList.AppendLine("<br/>");
        strbCurrentMissionList.AppendLine("<br/>");
        strbCurrentMissionList.AppendLine("<br/>");
        //strbCurrentMissionList.AppendLine("<div class='missionContainer-Active'></div>");
        strbCurrentMissionList.AppendLine("</div>");
        strbCurrentMissionList.AppendLine("</div>");

        strCurrentMissionList = strbCurrentMissionList.ToString();
        return strCurrentMissionList;
    }

    protected void popCurrentMission()
    {
        //int iCurrentMissionID = clsAccountUsers.iTitleID;
        
        //Just for testing 'iCurrentMissionID

        int iCurrentMissionID = 0;
        clsMissions clsCurrentMission = new clsMissions(iCurrentMissionID);
        StringBuilder strbCurrentMission = new StringBuilder();

        strbCurrentMission.AppendLine("<p class='currentMissionHeader'>" + clsCurrentMission.strTitle + "</p>");
        strbCurrentMission.AppendLine("<p class='currentMissionText'>" + clsCurrentMission.strDescription.Substring(0, 200) + "...</p>");
        strbCurrentMission.AppendLine("<div class='currentMissionProgressContainer'>");
        strbCurrentMission.AppendLine("<div class='progressContainer'>");
        strbCurrentMission.AppendLine("<span>" + strCurrentRank(clsAccountUsers.iCurrentPoints) + "</span>");
        strbCurrentMission.AppendLine("<p>My progress</p>");
        strbCurrentMission.AppendLine("<div class='progressBarEmpty'>");
        strbCurrentMission.AppendLine("<div class='percentageBlue' style='width:" + iCurrentPercentage(2, 3) + "px'></div>");
        strbCurrentMission.AppendLine("</div>");
        strbCurrentMission.AppendLine("</div><br class='clr' /><br /></div>");

        //litCurrentMission.Text = strbCurrentMission.ToString();
    }

    #endregion

    #region SAVE METHODS

    protected void saveMissionAndLoadNext(int iCurrentMissionID)
    {
        string strMissionTitle = "";
        int iMissionPoints = 0;

        DataTable dtMissions = clsMissions.GetMissionsList();

        foreach (DataRow dr in dtMissions.Rows)
        {
            strMissionTitle = dr["strTitle"].ToString();
            iMissionPoints = Convert.ToInt32(dr["iBonusPoints"].ToString());
            break;
        }

        int iMissionCount = dtMissions.Rows.Count;

        bool bAlreadyExists = clsCommonFunctions.DoesRecordExist("tblNotifications", "iMissionID='" + iCurrentMissionID + "' AND iAccountUserID=" + clsAccountUsers.iAccountUserID + " AND bIsDeleted=0");

        if (!bAlreadyExists)
        {
            clsNotifications clsNotifications = new clsNotifications();

            //### Add / Update
            clsNotifications.dtAdded = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            clsNotifications.iAddedBy = -10;
            clsNotifications.iAccountUserID = Convert.ToInt32(clsAccountUsers.iAccountUserID);
            clsNotifications.iGroupID = Convert.ToInt32(clsAccountUsers.iGroupID);
            clsNotifications.iMissionID = iCurrentMissionID;
            clsNotifications.strMessage = clsAccountUsers.strFirstName + " has just completed the mission: " + strMissionTitle + "";

            clsNotifications.Update();

            clsAccountUsers.iCurrentPoints = clsAccountUsers.iCurrentPoints + iMissionPoints;

            clsMissions clsCurrentMission = new clsMissions(iCurrentMissionID);

            bool bUserAchievementExists = clsCommonFunctions.DoesRecordExist("tblUserAchievements", "iAchievementID='" + clsCurrentMission.iAchievementID + "' AND iAccountUserID=" + clsAccountUsers.iAccountUserID + " AND bIsDeleted=0");

            if (bUserAchievementExists)
            {
                clsUserAchievements clsUserAchievements = new clsUserAchievements();
                clsUserAchievements.iAccountUserID = clsAccountUsers.iAccountUserID;
                clsUserAchievements.iAchievementID = clsCurrentMission.iAchievementID;
                clsUserAchievements.Update();
            }

            //### Increase Mission Level
            //if (iMissionCount > iCurrentMissionID)
            //{
            //    clsAccountUsers.iTitleID = iCurrentMissionID + 1;
            //}

            //clsAccountUsers.Update();

            //Session["clsAccountUsers"] = clsAccountUsers;
        }
    }

    protected void btnStart_Click(object sender, EventArgs e)
    {
        //Response.Redirect("Accelerate-Current-Mission.aspx");
    }


    #endregion
}