﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Net.Mail;

public partial class Accelerate_User_Profile : System.Web.UI.Page
{
    clsAccountUsers clsAccountUsers;
    clsAccountUsers clsAccountUsersBeingViewesd; 

    #region EVENT METHODS

    protected void Page_Load(object sender, EventArgs e)
    {
        //### Check if session clsUser exists
        if (Session["clsAccountUsers"] == null)
        {
            //### Redirect back to login
            Response.Redirect("Accelerate-Login.aspx");
        }
        clsAccountUsers = (clsAccountUsers)Session["clsAccountUsers"];

        if (Request.QueryString["action"] == "syslogout")
        {
            //### Clear all seesion variables
            Session.Clear();
        }

        //### If the iAccountUserID is passed through then we want to instantiate the object with that iAccountUserID
        if ((Request.QueryString["iAccountUserID"] != "") && (Request.QueryString["iAccountUserID"] != null))
        {
            try
            {
                clsAccountUsersBeingViewesd = new clsAccountUsers(Convert.ToInt32(Request.QueryString["iAccountUserID"]));

                //### Populate the form
                popFormData();
                string strAchievementsList = popAvailablePhasesMissionsAchievements();
                DataTable dtAchievementsToDisplay = getAchievementsToDisplayDT(strAchievementsList);
                string strCompletedBadgeIDList = popCompletedAchievements(clsAccountUsersBeingViewesd.iAccountUserID);
                popAllAchievements(strCompletedBadgeIDList, dtAchievementsToDisplay, clsAccountUsersBeingViewesd.iAccountUserID);
            }
            catch (Exception ex)
            {
                Response.Redirect("Accelerate-Error404.aspx");
            }
            //popAchievementStages();
        }

        if (!IsPostBack)
        {
            try
            {
                popFormData();
            }
            catch (Exception ex)
            {
                Response.Redirect("Accelerate-Error404.aspx");
            }
        }
        else
        {
            clsAccountUsers = (clsAccountUsers)Session["clsAccountUsers"];
        }

    }

    protected string strCurrentRank(int iCurrentPoints)
    {
        DataTable dtRank = clsRanks.GetRanksList("", "");

        int iRankID = 1;
        int iPointLevel;
        string strCurrentRank = "Noob";

        int iCount = 0;

        foreach (DataRow dtrRank in dtRank.Rows)
        {
            ++iCount;

            iRankID = Convert.ToInt32(dtrRank["iRankID"].ToString());
            iPointLevel = Convert.ToInt32(dtrRank["iPointLevel"].ToString());
            strCurrentRank = dtrRank["strTitle"].ToString();

            if (iCurrentPoints < iPointLevel)
            {
                break;
            }
        }

        return strCurrentRank;
    }

    //### Get the Missions from that Phase
    protected DataTable getAllMissionsForCurrentUserPhase(int iCurrentPhase)
    {
        DataTable dtMissionList = clsPhaseMissions.GetPhaseMissionsList("iPhaseID=" + iCurrentPhase, ""); //+ " AND iAccountUserID=" + clsAccountUsers.iAccountUserID

        int iTotalMissions = dtMissionList.Rows.Count;

        return dtMissionList;
    }

    protected DataTable getAllPhasesForCurrentUserGroup(int iCurrentGroup)
    {
        DataTable dtPhaseList = clsGroupPhases.GetGroupPhasesList("iGroupID=" + iCurrentGroup, ""); //+ " AND iAccountUserID=" + clsAccountUsers.iAccountUserID

        //int iTotalPhases = dtPhaseList.Rows.Count;

        return dtPhaseList;
    }

    protected string getAchievementList(int iPhaseID)
    {
        //StringBuilder strbAchievementList = new StringBuilder();
        string strAchievements = "";

        //### GET MISSIONS FOR CURRENT PHASE
        DataTable dtMissionList = getAllMissionsForCurrentUserPhase(iPhaseID);

        foreach (DataRow dtrMissionItems in dtMissionList.Rows)
        {
            int iMissionID = Convert.ToInt32(dtrMissionItems["iMissionID"]);

            clsMissions clsMissions = new clsMissions(iMissionID);
            int iMissionTypeID = clsMissions.iMissionTypeID;

            if (strAchievements != "")
                strAchievements += ",";

            strAchievements += clsMissions.iAchievementID;
        }

        return strAchievements;
    }

    protected string popCompletedAchievements(int iAccelerateUserID)
    {
        DataTable dtUserAchievement = clsUserAchievements.GetUserAchievementsList("iAccountUserID=" + iAccelerateUserID, "");

        string strListOfIDs = "";
        int iAchievementID;
        int iCount = 0;

        foreach (DataRow dtrAccountMember in dtUserAchievement.Rows)
        {
            ++iCount;

            iAchievementID = Convert.ToInt32(dtrAccountMember["iAchievementID"].ToString());
            strListOfIDs += iAchievementID;

            //clsAchievements clsAchievements = new clsAchievements(iAchievementID);

            if (dtUserAchievement.Rows.Count != iCount)
                strListOfIDs += ",";
        }

        return strListOfIDs;
    }

    protected void popAllAchievements(string strAlreadyDisplayedBadges, DataTable dtAllPossibleBadgesList, int iAccelerateUserID)
    {
        string[] idsList = strAlreadyDisplayedBadges.Split(',');
        int iCount = 0;
        dtAllPossibleBadgesList.Columns.Add("iMissionID");
        dtAllPossibleBadgesList.Columns.Add("strLink");

        foreach (DataRow dtrAccountMember in dtAllPossibleBadgesList.Rows)
        {
            ++iCount;
            int iAchievementID = Convert.ToInt32(dtrAccountMember["iAchievementID"].ToString());

            //### Gets the MissionID and adds it in a new datable for the href
            DataTable dtCurrentMission = clsMissions.GetMissionsList("iAchievementID=" + iAchievementID, "");
            foreach (DataRow dtrMissionList in dtCurrentMission.Rows)
            {
                int iMissionID = Convert.ToInt32(dtrMissionList["iMissionID"]);
                dtrAccountMember["iMissionID"] = iMissionID;
                clsAchievements clsCurrentAchievement = new clsAchievements(iAchievementID);

                foreach (DataRow dtrPossibleAchievements in dtAllPossibleBadgesList.Rows)
                {
                    if (Convert.ToInt16(dtrPossibleAchievements["iAchievementID"].ToString()) == iAchievementID)
                    {
                        foreach (string strID in idsList)
                        {
                            if (strID != "")
                            {
                                if (Convert.ToInt16(strID) == iAchievementID)
                                {

                                    if ((clsCurrentAchievement.strMasterImage == "") || (clsCurrentAchievement.strMasterImage == null))
                                    {
                                        dtrAccountMember["FullPathForImage"] = "images/imgNoImage.png";
                                    }
                                    else
                                    {
                                        dtrAccountMember["FullPathForImage"] = "Achievements/" + clsCurrentAchievement.strPathToImages + "/" + Path.GetFileNameWithoutExtension(clsCurrentAchievement.strMasterImage) + "" + Path.GetExtension(clsCurrentAchievement.strMasterImage);
                                        break;
                                    }
                                }
                                else
                                {
                                    dtrAccountMember["FullPathForImage"] = "images/imgAchievementEmpty.png";
                                }
                            }
                            else
                            {
                                dtrAccountMember["FullPathForImage"] = "images/imgAchievementEmpty.png";
                            }


                            if (iMissionID != 1)
                                dtrAccountMember["strLink"] = "Accelerate-Current-Mission.aspx?iMissionID=" + iMissionID;
                            else
                                dtrAccountMember["strLink"] = "#";
                        }
                    }
                }
            }
        }

        //rpAchievements.DataSource = dtAllPossibleBadgesList;
        //rpAchievements.DataBind();
    }

    protected DataTable getDtAllPossibleAchievements(int iAccountUserID)
    {
        string strListOfAllPossibleAchievements = "";
        int iCount = 0;
        int iCountGroupPhases = 0;

        DataTable dtAchievement = clsAchievements.GetAchievementsList();
        dtAchievement.Columns.Add("FullPathForImage");

        DataTable dtPossibleAchievements = new DataTable();
        dtPossibleAchievements = dtAchievement.Clone();
        dtPossibleAchievements.Clear();

        clsAccountUsers clsCurrentAccountUser = new clsAccountUsers(iAccountUserID);
        int iGroupID = clsCurrentAccountUser.iGroupID;

        DataTable dtGroupPhases = clsGroupPhases.GetGroupPhasesList("iGroupID=" + iGroupID, "");

        foreach (DataRow dtrGrouphases in dtGroupPhases.Rows)
        {
            iCountGroupPhases++;

            int iPhaseID = Convert.ToInt32(dtrGrouphases["iPhaseID"]);
            DataTable dtPhaseMission = clsPhaseMissions.GetPhaseMissionsList("iPhaseID=" + iPhaseID, "");

            foreach (DataRow dtrPhaseMission in dtPhaseMission.Rows)
            {
                ++iCount;

                int iMissionID = Convert.ToInt32(dtrPhaseMission["iMissionID"]);
                clsMissions clsMissions = new clsMissions(iMissionID);
                int iAchievementID = clsMissions.iAchievementID;
                //strListOfAllPossibleAchievements += iAchievementID;

                foreach (DataRow dtrAvailableAchievements in dtAchievement.Rows)
                {
                    if (iAchievementID == Convert.ToInt32(dtrAvailableAchievements["iAchievementID"].ToString()))
                        dtPossibleAchievements.Rows.Add(dtrAvailableAchievements.ItemArray);
                }
            }
        }

        dtPossibleAchievements.DefaultView.Sort = "strTitle ASC";
        dtPossibleAchievements = dtPossibleAchievements.DefaultView.ToTable();

        return dtPossibleAchievements;
    }

    protected string popAchievements(int iAccelerateUserID)
    {
        DataTable dtUserAchievement = clsUserAchievements.GetUserAchievementsList("iAccountUserID=" + iAccelerateUserID, "");

        string strListOfIDs = "";
        int iAchievementID;
        int iCount = 0;

        foreach (DataRow dtrAccountMember in dtUserAchievement.Rows)
        {
            ++iCount;

            iAchievementID = Convert.ToInt32(dtrAccountMember["iAchievementID"].ToString());

            strListOfIDs += iAchievementID;

            clsAchievements clsAchievements = new clsAchievements(iAchievementID);

            if (dtUserAchievement.Rows.Count != iCount)
                strListOfIDs += ",";
        }

        return strListOfIDs;
    }

    protected DataTable getAchievementsToDisplayDT(string strAchievementsList)
    {
        DataTable dtAchievement = clsAchievements.GetAchievementsList();
        dtAchievement.Columns.Add("FullPathForImage");

        DataTable dtAchievementsToDisplay = new DataTable();
        dtAchievementsToDisplay = dtAchievement.Clone();
        dtAchievementsToDisplay.Clear();

        string[] AchievementIDs = strAchievementsList.Split(',');

        foreach (string ID in AchievementIDs)
        {
            foreach (DataRow dtrAvailableAchievements in dtAchievement.Rows)
            {
                if (Convert.ToInt32(ID) == Convert.ToInt32(dtrAvailableAchievements["iAchievementID"].ToString()))
                    dtAchievementsToDisplay.Rows.Add(dtrAvailableAchievements.ItemArray);
            }
        }

        return dtAchievementsToDisplay;
    }

    protected string popAvailablePhasesMissionsAchievements()
    {
        int iPhasesAvailable = canWeUnlockNextPhase();

        clsPhases clsCurrentPhase = new clsPhases();
        DataTable dtPhase = getAllPhasesForCurrentUserGroup(clsAccountUsersBeingViewesd.iGroupID);

        int iCount = 0;
        string strMissions = "";
        string strAchievementsList = "";

        DateTime dtCurrentDate = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
        DateTime dtStartTime = new DateTime();
        DateTime dtEndTime = new DateTime();

        foreach (DataRow dtrCurrentPhase in dtPhase.Rows)
        {
            ++iCount;

            int iPhaseID = Convert.ToInt32(dtrCurrentPhase["iPhaseID"].ToString());
            clsCurrentPhase = new clsPhases(iPhaseID);

            dtStartTime = Convert.ToDateTime(clsCurrentPhase.dtStartDate);
            dtEndTime = Convert.ToDateTime(clsCurrentPhase.dtEndDate);

            if (dtStartTime <= dtCurrentDate && dtEndTime >= dtCurrentDate)
            {
                if (iPhasesAvailable >= iCount)
                {
                    if (strAchievementsList != "")
                        strAchievementsList += ",";

                    //### Get list of possible achievements
                    strAchievementsList = strAchievementsList.ToString() + getAchievementList(iPhaseID);
                }
            }
        }

        return strAchievementsList;
    }

    private int canWeUnlockNextPhase() //###return 1 2 or 3 depending what should be available
    {
        int iGroupID = clsAccountUsers.iGroupID;
        clsGroups clsGroups = new clsGroups(clsAccountUsers.iGroupID);
        DataTable dtGroupPhases = clsGroupPhases.GetGroupPhasesList("iGroupID=" + iGroupID, "");
        int iPhaseID = 1;
        int iPhaseCount = 1;

        foreach (DataRow dtrGroupPhasesList in dtGroupPhases.Rows)
        {
            iPhaseID = Convert.ToInt32(dtrGroupPhasesList["iPhaseID"]);
            DataTable dtPhaseMissions = clsPhaseMissions.GetPhaseMissionsList("iPhaseID=" + iPhaseID, "");

            bool bIsThisPhaseComplete = true;

            //break;
            int iMissionID = 0;
            foreach (DataRow dtrPhaseMissionsList in dtPhaseMissions.Rows)
            {
                iMissionID = Convert.ToInt32(dtrPhaseMissionsList["iMissionID"]);

                clsMissions clsMissions = new clsMissions(iMissionID);
                int iMissionTypeID = clsMissions.iMissionTypeID;

                if (iMissionTypeID == 1)
                {
                    if (!bIsTextMissionComplete(iMissionID))
                        bIsThisPhaseComplete = false;
                }
                else if (iMissionTypeID == 2)
                {
                    if (!bIsLeadershipMissionComplete(iMissionID))
                        bIsThisPhaseComplete = false;
                }
            }

            if (bIsThisPhaseComplete)
                ++iPhaseCount;
        }

        return iPhaseCount;
    }

    protected void popAchievementStages()
    {
        StringBuilder strbAchievementStages = new StringBuilder();
        strbAchievementStages.AppendLine("<div class='achievementStageContainer'>");
        strbAchievementStages.AppendLine("<div class='ovalAchiementStage'>");
        strbAchievementStages.AppendLine("Current Status<br />");
        strbAchievementStages.AppendLine("<span>" + strCurrentRank(clsAccountUsersBeingViewesd.iCurrentPoints) + "</span>");
        strbAchievementStages.AppendLine("</div>");

        int iCurrentRank = iRankID(clsAccountUsersBeingViewesd.iCurrentPoints);
        clsRanks clsCurrentRank = new clsRanks(iCurrentRank);
        clsRanks clsNextRank = new clsRanks();
        clsRanks clsFinalRank = new clsRanks();

        if (iRankCount() > iCurrentRank)
        {
            clsNextRank = new clsRanks(iCurrentRank + 1);
            clsFinalRank = new clsRanks(iRankCount());
        }

        strbAchievementStages.AppendLine("<div class='ovalAchiementStage marginleftfifty'>");
        strbAchievementStages.AppendLine("Next Status<br />");
        strbAchievementStages.AppendLine("<span>" + clsNextRank.strTitle + "</span>");
        strbAchievementStages.AppendLine("</div>");
        strbAchievementStages.AppendLine("<div class='ovalAchiementStage marginleftfifty'>");
        strbAchievementStages.AppendLine("Final Status<br />");
        strbAchievementStages.AppendLine("<span>" + clsFinalRank.strTitle + "</span>");
        strbAchievementStages.AppendLine("</div></div>");
    }

    //### Returns if Mission is Complete for Text mission
    protected bool bIsTextMissionComplete(int iCurrentMissionID)
    {
        bool bIsTextMissionComplete = false;

        DataTable dtMissionQuestions = clsMissionQuestions.GetMissionQuestionsList("iMissionID=" + iCurrentMissionID, "");
        int iTotalQuestionCount = dtMissionQuestions.Rows.Count;

        DataTable dtCompletedAnswersToQuestions = clsUserAnswers.GetUserAnswersList("iMissionID=" + iCurrentMissionID + " AND iAccountUserID=" + clsAccountUsers.iAccountUserID, "");
        int iTotalAnsweredCount = dtCompletedAnswersToQuestions.Rows.Count;
        int iCurrentPercentage = 0;

        //### Check if undefined
        if ((iTotalQuestionCount != 0))
        {
            double dblCurrentPercentage = Convert.ToDouble(iTotalAnsweredCount) / Convert.ToDouble(iTotalQuestionCount);
            iCurrentPercentage = Convert.ToInt32(dblCurrentPercentage * 200);
        }

        if (iCurrentPercentage == 200)
            bIsTextMissionComplete = true;

        return bIsTextMissionComplete;
    }

    //### Returns if Mission is Complete for Teamship mission
    protected bool bIsLeadershipMissionComplete(int iCurrentMissionID)
    {
        bool bIsLeadershipMissionComplete = false;

        DataTable dtMissionQuestions = clsTeamShips.GetTeamShipsList("iMissionID=" + iCurrentMissionID, "");
        int iTotalQuestionCount = dtMissionQuestions.Rows.Count;

        DataTable dtCompletedAnswersToQuestions = clsTeamShipAnswers.GetTeamShipAnswersList("iMissionID=" + iCurrentMissionID + " AND iAddedby=" + clsAccountUsers.iAccountUserID, "");
        int iTotalAnsweredCount = dtCompletedAnswersToQuestions.Rows.Count;
        int iCurrentPercentage = 0;

        //### Check if undefined
        if ((iTotalQuestionCount != 0))
        {
            double dblCurrentPercentage = Convert.ToDouble(iTotalAnsweredCount) / Convert.ToDouble(iTotalQuestionCount);
            iCurrentPercentage = Convert.ToInt32(dblCurrentPercentage * 200);
        }

        if (iCurrentPercentage == 200)
            bIsLeadershipMissionComplete = true;

        return bIsLeadershipMissionComplete;
    }

    #endregion

    #region POPULATE DATA METHODS

    private void popFormData()
    {
        txtFirstName.InnerText = clsAccountUsersBeingViewesd.strFirstName;
        txtSurname.InnerText = clsAccountUsersBeingViewesd.strSurname;
        txtEmailAddress.InnerText =  clsAccountUsersBeingViewesd.strEmailAddress;
    }

    #endregion

    #region ACTION METHODS

    protected int iRankID(int iCurrentPoints)
    {
        DataTable dtRank = clsRanks.GetRanksList("", "");

        int iRankID = 1;
        int iPointLevel;

        int iCount = 0;

        foreach (DataRow dtrRank in dtRank.Rows)
        {
            ++iCount;

            iRankID = Convert.ToInt32(dtrRank["iRankID"].ToString());
            iPointLevel = Convert.ToInt32(dtrRank["iPointLevel"].ToString());

            if (iCurrentPoints < iPointLevel)
            {
                break;
            }
        }

        return iRankID;
    }

    protected int iRankCount()
    {
        DataTable dtRank = clsRanks.GetRanksList();

        int iRowCount = dtRank.Rows.Count;

        return iRowCount;
    }

    #endregion
}